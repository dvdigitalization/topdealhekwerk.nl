<?php
/*
Title:		Standard sitemap generator
File: 		page.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization

SQL:		INSERT INTO `digi_settings` (`setting`, `setting_set`, `setting_value`, `setting_descr`, `digi_protected`, `type_yes_no`,
			 `type_int`) VALUES
			('SITEMAP_REFRESH_RATE', 'CMS', '1', 'Sitemap wordt ververst om de zoveel dagen', 0, 0, 1),
			('SITEMAP_LAST_UPDATED', 'CMS', '0', 'Laatste keer dat sitemap ververst werd', 1, 0, 1);

*/

//HTML name, page title, page set
$html_name 		= 	'sitemap.html';
$page_title		=	'Sitemap';
$page_set		=	'sitemap';

require_once('inc/site_header.php');
$crumbs[$html_name] = $page_title;

//What to do
$do_pages				=	true;

//Database tables
$db_table_pages 		= 	DIGI_DB_PREFIX.'pages_'.$_lang;

//Types
$types					=	array(
	'page'			=>	'Pagina\'s',
	'maincats'		=>	'Nieuws',
);

//Manual pages
$manual_pages	=	array(
	'sitemap.html' 	=> 	'Sitemap'
);

//The image url
$img_sep = "<img src='gfx/cms/system/sitemap.gif' style='margin-top: 3px;' align='absmiddle' />";

//Update the sitemaps after a certain amount of time has elapsed
if (((time() - $cfg['SITEMAP_LAST_UPDATED']) > $cfg['SITEMAP_REFRESH_RATE'] * 86400) or !file_exists('sitemap.xml')) {

	//Last modified and URL's array initialization
	$last_mod 	= 	date('Y')."-".date('m')."-".date('d');
	$urls 		= 	array();

	//Website pages
	if ($do_pages) {
		$res = eq("SELECT file_name,html_name,id,title FROM $db_table_pages WHERE active = '1' AND needs_authorization = '0';");
		while ($page = mfo($res)) {
			$loc 	=	$page->file_name ? $page->file_name : ($page->html_name ? $page->html_name : 'page.php?page_id='.$page->id);
			$urls[] = "\n\t<url>\n\t\t<loc>$_link$loc</loc>\n\t\t<lastmod>$last_mod</lastmod>\n\t\t<changefreq>monthly</changefreq>\n\t\t<priority>0.8</priority>\n\t\t<title><![CDATA[$page->title]]></title>\n\t\t<type>page</type>\n\t\t<level>0</level>\n\t</url>";
		}
	}

	//Manual pages
	if (count($manual_pages)) {
		foreach ($manual_pages as $loc => $title) {
			$urls[] = "\n\t<url>\n\t\t<loc>$_link$loc</loc>\n\t\t<lastmod>$last_mod</lastmod>\n\t\t<changefreq>monthly</changefreq>\n\t\t<priority>0.8</priority>\n\t\t<title><![CDATA[$title]]></title>\n\t\t<type>page</type>\n\t\t<level>0</level>\n\t</url>";
		}
	}

	//Main categories
	$res = eq("SELECT id,menu_name FROM $db_table_cats WHERE active='1' AND sub_cat_of='0' AND menu_name <> '';");
	while ($maincat = mfo($res)) {


		$res_c = eq("SELECT id,menu_name FROM $db_table_cats WHERE active='1' AND sub_cat_of='$maincat->id' AND menu_name <> '';");
		while ($cat = mfo($res_c)) {
			$loc = "categorie-".$cat->id."-".rewriteSafe($cat->menu_name).".html";
			$urls[] = "\n\t<url>\n\t\t<loc>$_link$loc</loc>\n\t\t<lastmod>$last_mod</lastmod>\n\t\t<changefreq>monthly</changefreq>\n\t\t<priority>0.5</priority>\n\t\t<title><![CDATA[$cat->menu_name]]></title>\n\t\t<type><![CDATA[$maincat->menu_name]]></type>\n\t\t<level>0</level>\n\t</url>";
		}
	}

	//Write XML
	$sitemap_xml = "<?xml version='1.0' encoding='UTF-8'?>\n<urlset xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'>".implode("",$urls)."\n</urlset>";
	file_put_contents('sitemap.xml', $sitemap_xml);

	//Update refresh timestamp
	upRec('digi_settings',"setting='SITEMAP_LAST_UPDATED'", 'setting_value', time());
}

//Read XML
$xml 	= 	isset($sitemap_xml) ? simplexml_load_string($sitemap_xml) : simplexml_load_file('sitemap.xml');
$links 	= 	array();

//Loop URL's
foreach ($xml->url as $url) {

	//Get type and level
	$type 	= 	(string) 	$url->type;
	$level	=	(int)		$url->level;

	if (!isset($links[$type])) {
		$links[$type] = array();
	}

	$url_real			= 	str_replace($_link,'',$url->loc);
	$links[$type][] 	= 	str_repeat('&nbsp; &nbsp; ',$level)."$img_sep <a href='$url_real' title='$url->title'>$url->title</a>";
}

//Initial contents
$page_contents		=	"<h1>Sitemap</h1>";

//Loop links
foreach ($links as $type => $urls) {
	if (!isset($types[$type])) {
		$types[$type] = $type;
	}
	$page_contents .= "<p><br/><b>".$types[$type]."</b></p><p>".implode("\n<br/>",$urls)."</p>";
}

//Output
echo $page_contents;

require_once('inc/site_footer.php');
?>