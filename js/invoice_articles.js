/*
Title:		Invoice articles JS functions
File: 		js/invoice_articles.js
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

function setnew_article(invoice_id, row) {
				
	var text 	=	makeQuerySafe(document.getElementById('article_text_'+row).value);
	var amount 	=	document.getElementById('amount_'+row).value;
	var unit 	=	document.getElementById('article_unit_'+row).value;
	var price 	=	document.getElementById('price_'+row).value;
	var vat 	=	document.getElementById('vat_level_'+row).value;
	
	if (amount == '') amount = 1;
				
	ajax_query = 'add_article=1&invoice_id='+invoice_id+'&text='+text+'&amount='+amount+'&unit='+unit+'&price='+price+'&vat_level='+vat+'&row='+row;
	sendHttpRequest(file,'update_page',0,'added_article');			
}

function del_article(invoice_id, article_id) {

	//innerHTML('status_field', 'Artikel wordt verwijderd...');
	ajax_query = 'delete_article=1&invoice_id='+invoice_id+'&article_id='+article_id;
	sendHttpRequest(file,'update_page',0,'deleted_article');				
}

function update_page(res, task) {
 	
 	if (res) {
		if (task == 'added_article') {
			innerHTML('invoice_articles', res);
			
			setValue('article_text_0');
			setValue('amount_0');
			setValue('article_unit_0');
			setValue('price_0');
			setValue('vat_level_0');
			
		}
		else if (task == 'deleted_article') {
			innerHTML('invoice_articles', res);														
		}
	}
}