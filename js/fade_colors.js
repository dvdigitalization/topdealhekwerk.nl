/*
Title:		Functions to fade color/background of an element from one color to another
File: 		js/fade_colors.js
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization

Usage:		function fadeIn(obj_id){
				clearTimeout(tid);
				fade(obj_id, 255,255,255, 170,212,255, 1, 20, 0, 0);
			}
			function fadeOut(obj_id){
				clearTimeout(tid);
				fade(obj_id, 170,212,255, 255,255,255, 1, 20, 0, 0);
			}
*/

//Hex number support
hexa = new makeArray(16);
for(var i = 0; i < 10; i++) hexa[i] = i;
hexa[10]='a'; hexa[11]='b'; hexa[12]='c';
hexa[13]='d'; hexa[14]='e'; hexa[15]='f';

function hex(i){
	if (i < 0) return '00';
	else if (i >255) return 'ff';
	else return '' + hexa[Math.floor(i/16)] + hexa[i%16];
}

//Set color/background with given RGB value
function setbgColor(obj_id, r, g, b){
	var hr = hex(r); var hg = hex(g); var hb = hex(b);
	setStyle(obj_id, 'background', '#'+hr+hg+hb);
}
function setColor(obj_id, r, g, b){
	var hr = hex(r); var hg = hex(g); var hb = hex(b);
	setStyle(obj_id, 'color', '#'+hr+hg+hb);
}

//Color/background fade function
var tid;
function fade(obj_id, sr, sg, sb, er, eg, eb, bgcolor, step, interval, i) {
	if (i <= step){
		if (bgcolor == 1) {
			setbgColor(obj_id,
				Math.floor(sr * ((step-i)/step) + er * (i/step)),
				Math.floor(sg * ((step-i)/step) + eg * (i/step)),
				Math.floor(sb * ((step-i)/step) + eb * (i/step)));
		}
		else {
			setColor(obj_id,
				Math.floor(sr * ((step-i)/step) + er * (i/step)),
				Math.floor(sg * ((step-i)/step) + eg * (i/step)),
				Math.floor(sb * ((step-i)/step) + eb * (i/step)));			
		}
		i++;
		tid = setTimeout('fade("'+obj_id+'",'+sr+','+sg+','+sb+','+er+','+eg+','+eb+','+bgcolor+','+step+','+interval+','+i+')', interval);
	}
}