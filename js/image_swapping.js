/*
Title:		Functions to easily pre-load and swap images (ie menu buttons)
File: 		js/image_swapping.js
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

//Locatie afbeeldingen map
var	img_folder	= 	'gfx/navigation/';

//Afbeelding naam definities
var id_prefix	=	'b_';
var	name_prefix	=	'';
var	name_suffix	=	'.jpg';
var	name_on		=	'_2';
var	name_off	=	'_1';
var	names		=	new Array('overons','diensten','onswerk','offerte','contact',);

//Afbeeldingen arrays
var	buttons_on  =	new Array();
var buttons_off	=	new Array();

//Pre loaden afbeeldingen
for (i = 0; i < names.length; i++) {
	
	var name = names[i];
	
	buttons_on[name] 		= 	new Image();
	buttons_on[name].src	=	img_folder + name_prefix + name + name_on + name_suffix;
	
	buttons_off[name] 		= 	new Image();
	buttons_off[name].src	=	img_folder + name_prefix + name + name_off + name_suffix;
	
}

//Mouseover and mouseout functions
function showOn(name) {
	var obj = getElementById(id_prefix+name);
	obj.src = buttons_on[name].src;
}

function showOff(name) {
	var obj = getElementById(id_prefix+name);
	obj.src = buttons_off[name].src;
}