/*
Title:		Global JavaScript functions
File: 		js/globals.js
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

//Dollar function
function $() {
	var elements = new Array();
	for (var i = 0; i < arguments.length; i++) {
		var element = arguments[i];
		if (typeof element == 'string')
			element = document.getElementById(element);
		if (arguments.length == 1)
			return element;
		elements.push(element);
	}
	return elements;
}

//Time2date
function time2date(unixtime) {
	var mydate = new Date(unixtime*1000);
	var day    = mydate.getDate();
	var month  = mydate.getMonth() + 1;
	var year   = mydate.getFullYear();

	if (day.toString().length == 1) { 
		day = '0' + day;
	}
	
	if (month.toString().length == 1) { 
		month = '0' + month;
	}
	return day + '/' + month + '/' + year;
}

//Date2time
function date2time(date_string) {
	
	var pieces = date_string.split('/');
	var mydate = new Date();
	
	mydate.setDate(pieces[0]);
	mydate.setMonth(pieces[1]);
	mydate.setFullYear(pieces[2]);
	
	return Math.round(mydate.getTime()/1000);
}

//Set the value of an object
function setValue(obj_id, value) {
	if (document.getElementById(obj_id)) {
		if (value == null) value = '';
		document.getElementById(obj_id).value = value;
	}
	else alert('Object with ID "'+obj_id+'" does not exist. Could not set value.');
}

//Set the style of an object
function setStyle(obj_id, style, value) {
	if (document.getElementById(obj_id)) {
		document.getElementById(obj_id).style[style] = value;
	}
	else alert('Object with ID "'+obj_id+'" does not exist. Could not set style.');
}

//Focus on an object
function setFocus(obj_id) {
	if (document.getElementById(obj_id)) {
		document.getElementById(obj_id).focus();
	}
	else alert('Object with ID "'+obj_id+'" does not exist. Could not set focus.');
}

//Function to change the inner HTML of an object.
function innerHTML(obj_id, txt) {
 	if (document.getElementById(obj_id)) {
		document.getElementById(obj_id).innerHTML = txt;
	}
	else alert('Object with ID "'+obj_id+'" does not exist. Could not set inner HTML.');
}

//Function to set the visibility of an object
function setVisibility(obj_id, visible) {
	if (document.getElementById(obj_id)) {
		var element = document.getElementById(obj_id);
		if (visible == true) {
			element.style.display = '';
		}
		else {
			element.style.display = 'none';
		}
	}
	else alert('Object with ID "'+obj_id+'" does not exist. Could not set visibility.');
}

//Toggle visibility
function toggleVisibility(obj_id, set_display) {
	if (set_display == null) set_display = '';
	if (document.getElementById(obj_id)) {
		var element = document.getElementById(obj_id);
		element.style.display = (element.style.display != 'none' ? 'none' : set_display );
	}
	else alert('Object with ID "'+obj_id+'" does not exist. Could not toggle visibility.');
}

//Function to make an array
function makeArray(n) {
	this.length = n;
	for(var i = 1; i <= n; i++)
	this[i] = 0;
	return this;
}

//As the PHP function in_array()
function in_array(needle, haystack) {
	for (i = 0; i < haystack.length; i++) {
		if (needle == haystack[i]) return true;
	}
	return false;
}

//Check if a number is an integer
function isInt(number) {
    var modulus = number % 1;
    if (modulus == 0) 	return true;
    else 				return false;
}

//Force the value of an input field to be a number
function force_number(obj) {
	if(obj.value.indexOf(',')) 				obj.value = obj.value.replace(',','.');
	if(isNaN(obj.value) || obj.value == '') obj.value = 0;
	if(obj.value != 0 && obj.value < 0.001 && obj.value > -0.001) obj.value = 0;
	if(obj.value.indexOf('.')) 				obj.value = obj.value.replace('.',',');
}

//Force the value of an input field to be a positive integer
function force_positive_integer(obj, default_value) {
 	var value = parseInt(obj.value);
	if(isNaN(value) || obj.value == '') 	obj.value = default_value;
	else if (value < 0)						obj.value = value - (2*value);
	else 									obj.value = value;
}

//As the parseAmount() function defined in globals.php
function parseAmount(amount, sign) {
	
	amount = amount / 100;
	var amount_str = amount + '';
	
	pos = amount_str.indexOf('.');
	if (pos > -1) {
		var ending = amount_str.substring(pos+1);
		if (ending.length == 1) {
			amount_str += '0';
		}
	}
	else {
		amount_str += '.00';
	}
	
	amount_str = amount_str.replace('.',',');
	
	if (sign == true) 	return '&euro;' + amount_str;
	else				return amount_str;
}

//Round function for more decimals
function round(value, digits) {
	var tmp = Math.pow(10,digits);	
	return Math.round(value*tmp)/tmp
}

//Function to generate a print version popup
function printVersion(width, height) {
	if (!width)  width  = 600;
	if (!height) height = 600;
	var this_url 	= 	document.location.href;
	var sep			=	(this_url.indexOf('?') > 0) ? '&' : '?';
	window.open(this_url+sep+'print','_blank','location=no,menubar=no,status=no,toolbar=no,scrollbars=1,width='+width+',height='+height);
}

//Function to color the background of an object
function colorBg(obj_id) {
	if (document.getElementById(obj_id)) {
		setStyle(obj_id, 'background', '#ffaa88');
		document.getElementById(obj_id).onkeypress = function() {
			setStyle(obj_id, 'background', '#ffffff');
		}
		document.getElementById(obj_id).onblur = function() {
			setStyle(obj_id, 'background', '#ffffff');
		}
	}
	else alert('Object with ID "'+obj_id+'" does not exist. Could not color the background.');
}

//Function to color the background of an object if empty
function colorBgIfEmpty(obj_id) {
	if (document.getElementById(obj_id)) {	
		if(!document.getElementById(obj_id).value) colorBg(obj_id);
	}
	else alert('Object with ID "'+obj_id+'" does not exist. Could not apply colorBgIfEmpty.');
}

//Function to expand/contract an element with the given contents (must be named expand_div_XX)
var showing = new Array();
function evalExpand(id, contents) {
	
	if (showing[id] != 1) {
		document.getElementById('expand_div_'+id).innerHTML = contents;
		showing[id] = 1;
	} else {
		document.getElementById('expand_div_'+id).innerHTML = '';
		showing[id] = 0;
	}
}

//Function to get the value of a checked radio button
function getCheckedValue(form_name, radio_name) {
	radioObj = document.forms[form_name].elements[radio_name];
	if(!radioObj)
		return "";
	var radioLength = radioObj.length;
	if(radioLength == undefined)
		if(radioObj.checked)
			return radioObj.value;
		else
			return "";
	for(var i = 0; i < radioLength; i++) {
		if(radioObj[i].checked) {
			return radioObj[i].value;
		}
	}
	return "";
}

//Function to check the radio button of a specific value
function setCheckedValue(form_name, radio_name) {
	radioObj = document.forms[form_name].elements[radio_name];
	if(!radioObj)
		return;
	var radioLength = radioObj.length;
	if(radioLength == undefined) {
		radioObj.checked = (radioObj.value == newValue.toString());
		return;
	}
	for(var i = 0; i < radioLength; i++) {
		radioObj[i].checked = false;
		if(radioObj[i].value == newValue.toString()) {
			radioObj[i].checked = true;
		}
	}
}

//Functions to mark/un-mark all checkboxes of a certain identifier on a page
function markAll(identifier) {
	if (identifier == null) identifier = 'm_';	
	var i = 1;
	while (document.getElementById(identifier+i)) {
		document.getElementById(identifier+i).checked = true;
		i++;	
	}
}

function markNone(identifier) {
	if (identifier == null) identifier = 'm_';	
	var i = 1;
	while (document.getElementById(identifier+i)) {
		document.getElementById(identifier+i).checked = false;
		i++;	
	}			
}

var marked_all = false;
function toggleMarkAll(identifier) {
	if (identifier == null) identifier = 'm_';
	if (marked_all == true) {
		markNone(identifier);
		marked_all = false;
	}
	else {
		markAll(identifier);
		marked_all = true;
	}
}

function getMarkedValues(identifier) {
	if (identifier == null) identifier = 'm_';
	var marked 	= 	new Array();
	var i 		= 	1;
	var j		= 	0;
	while (document.getElementById(identifier+i)) {
		if (document.getElementById(identifier+i).checked == true) {
			marked[j] = document.getElementById(identifier+i).value;
			j++;
		}
		i++;	
	}
	return marked;
}

function sendWithMarked(url, extra_query, query_identifier, identifier) {
	if (identifier == null) 	identifier = 'm_';
	var marked 		= 	getMarkedValues(identifier);
	var url_query 	= 	'?'+extra_query+'&multiple=1';
	for(j=0;j<marked.length;j++) {
		url_query+= '&'+query_identifier+'[]='+marked[j];
	}
	document.location.href = url+url_query;
}