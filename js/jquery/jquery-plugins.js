/**
 * Common plugins to improve jQUery's usability
 *
 * @package 	DigiCMS v3.x
 * @file		js/jquery/jquery_plugins.js
 * @version		3.00
 * @author		Digitalization
 * @email		info@digitalization.nl
 * @link		http://www.digitalization.nl/
 * @copyright	All code copyright 2008,2009 by Digitalization
 */

/* COLOR FIX: change if(h.state==0) to if(h.state==0||h.start.constructor!=Array||h.end.constructor!=Array) */

/* Dutch (UTF-8) initialisation for the jQuery UI date picker plugin. */
jQuery(function($){
	$.datepicker.regional['nl'] = {clearText: 'Wissen', clearStatus: '',
		closeText: 'Sluiten', closeStatus: '',
		prevText: '&lt;Terug', prevStatus: '',
		nextText: 'Volgende&gt;', nextStatus: '',
		currentText: 'Vandaag', currentStatus: '',
		monthNames: ['Januari','Februari','Maart','April','Mei','Juni',
		'Juli','Augustus','September','Oktober','November','December'],
		monthNamesShort: ['Jan','Feb','Mrt','Apr','Mei','Jun',
		'Jul','Aug','Sep','Okt','Nov','Dec'],
		monthStatus: '', yearStatus: '',
		weekHeader: 'Wk', weekStatus: '',
		dayNames: ['Zondag','Maandag','Dinsdag','Woensdag','Donderdag','Vrijdag','Zaterdag'],
		dayNamesShort: ['Zon','Maa','Din','Woe','Don','Vri','Zat'],
		dayNamesMin: ['Zo','Ma','Di','Wo','Do','Vr','Za'],
		dayStatus: 'DD', dateStatus: 'D, M d',
		dateFormat: 'dd-mm-yy', firstDay: 0,
		initStatus: '', isRTL: false};
});

/*
 * Jonathan Howard
 *
 * jQuery Pause
 * version 0.2
 *
 * Requires: jQuery 1.0 (tested with svn as of 7/20/2006)
 *
 * Feel free to do whatever you'd like with this, just please give credit where
 * credit is due.
 *
 * pause() will hold everything in the queue for a given number of milliseconds,
 * or 1000 milliseconds if none is given.
 *
 * unpause() will clear the queue of everything of a given type, or 'fx' if no
 * type is given.
 */

(function($){
	$.fn.pause = function(milli,type) {
		milli = milli || 1000;
		type = type || "fx";
		return this.queue(type,function(){
			var self = this;
			setTimeout(function(){
				$.dequeue(self);
			},milli);
		});
	};

	$.fn.clearQueue = $.fn.unpause = function(type) {
		return this.each(function(){
			type = type || "fx";
			if(this.queue && this.queue[type]) {
				this.queue[type].length = 0;
			}
		});
	};
})(jQuery);

/*
 * jQuery FxQueues 2.0.3
 * Copyright 2009 Luciano German Panaro <contact@decodeuri.com>
 * Released under the MIT and GPL licenses.
 */

(function($){
  var fxQueue = function(queueName) {
    return {
      name: queueName,

      isFxQueue: true,

      paused: false,

      playing: null,

      shouldStart: function() {
          return (this.playing == null || !this.paused);
      },

      pause: function() {
          if (!this.playing) { return false; }
          ((this.playing.isScope)? this.playing: this.playing.elem).stop();
          this.paused = true;
          return true;
      },

      stop: function() {
          if (!this.playing) { return false; }
          ((this.playing.isScope)? this.playing: this.playing.elem).stop();
          this.playing = null;
          this.paused = false;
          this.length = 0;
          return true;
      },

      start: function() {
          // if a dequeued fn was paused
          if (this.playing && this.paused) {
              this.playing();
              this.paused = false;
              return true;
          // or if the queue has not started
          } else if (this.length && !this.playing) {
              this.playing = this[0];
              $(document).dequeue(this.name);
              return true;
          }
          return false;
      },

      getScope: function( scopeName ) {
          if (this.playing && this.playing.isScope && this.playing.called == scopeName) {
              return this.playing;
          }

          for (var i = 0; i < this.length; i++) {
              if (this[i].isScope && this[i].called == scopeName) {
                  return this[i];
              }
          }
          return false;
      },

      dequeue: function( caller ) {
          // Do nothing if queue is not playing anything
          if (!this.playing) {
            return false;
          }

          if (this.playing.isScope) {

              var queueItems = this.playing.items;
              // Find the actual element in scope's items
              for ( var i=0; i < queueItems.length; i++) {
                  if ( caller == queueItems[i].elem[0] && !queueItems[i].finished ) {
                      queueItems[i].finished = true;
                      this.playing.finishedItems++;
                  }
              }

              // Do not dequeue if scope is not finished
              if (this.playing.finishedItems < queueItems.length) {
                  return false;
              }

          // Dequeue just once for every selection
          } else if (this.playing.elem && this.playing.elem[0] != caller) {
              return false;
          }

          var queue = this;

          setTimeout(function() {
              queue.playing = queue[0];
              $(document).dequeue(queue.name);
          }, this.playing.postDelay);

          return true;
      }

    };

  };

  var fxScope = function ( scopeName ) {
      var newScope = function() {
          for (var i=0; i < newScope.items.length; i++) {
              newScope.items[i]();
          }
      };
      newScope.called = scopeName;
      newScope.isScope = true;
      newScope.finishedItems = 0;
      newScope.stop = function() {
          for (var i=0; i < newScope.items.length; i++) {
              newScope.items[i].elem.stop();
          }
      };
      newScope.items = [];
      return newScope;
  };

  // We need to overload the default animate method
  var _animate = $.fn.animate;

  $.fn.animate = function( props, speed, easing, callback ) {
      if (!this.length) {
          return this;
      }

      var options = (typeof speed == "object")? speed: $.speed(speed, easing, callback);

      // Load in the default options
      var opts = $.extend({
          queue: "fx",
          position: "end",
          limit: -1,
          preDelay: 0,
          postDelay: 0,
          complete: null
      }, options );

      // Let normal animations just pass through
      if ( !opts.queue || opts.queue == "fx" ) {
          return _animate.apply( this, arguments );
      }

      // Get the name of the queue
      var queueName = opts.queue;

      // Get the effect queue (A global queue is centered on 'document')
      var queue = $(document).queue( opts.queue );

      // Extend the queue object if it's new.
      if ( !queue.isFxQueue ) {
          $.extend(queue, fxQueue(queueName));
      }

      // We're overriding the default queueing behavior
      opts.queue = false;

      // The animation to queue
      var fn = function() {
          opts.complete = function() {
              queue.dequeue(this);
              if ( $.isFunction(fn.users_complete) ) {
                  return fn.users_complete.apply(this, arguments);
              }
          };
          setTimeout(function() {
              fn.elem.animate( props, opts );
        	}, fn.preDelay);
      };
      fn.elem = this;
      fn.preDelay = opts.preDelay || 0;
      fn.postDelay = opts.postDelay || 0;
      fn.users_complete = speed.complete || callback; //Do not use the one generated by $.speed

      // If scope exists, just add the animation and return
      var scope = queue.getScope( opts.scope );
      if ( scope ) {
          scope.items.push( fn );
          // Start the animation if the scope is already being played
          if (queue.playing == scope) {
              fn();
          }
          return this;
      }

      // Restrict the animation to a specifically sized queue
      if ( opts.limit < 0 || queue.length < opts.limit) {

          var add = null; //What we are going to add into the queue
          if ( opts.scope ) {
              add = fxScope( opts.scope );
              add.items.push(fn);
          } else {
              add = fn;
          }

          if ( opts.position == "end" ) {
            queue.push( add );
          } else if ( opts.position == "front" ) {
            queue.splice( 1, 0, add );
          }

          if ( queue.shouldStart() ) {
            queue.start();
          }

          return this;
      }
  };

  // A simple global fx queue getter
  $.extend({
      fxqueue: function(queueName) {
          return $(document).queue( queueName );
      }
	});

})(jQuery);


/**
 * jQuery custom checkboxes
 *
 * Copyright (c) 2008 Khavilo Dmitry (http://widowmaker.kiev.ua/checkbox/)
 * Licensed under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 *
 * @version 1.3.0 Beta 1
 * @author Khavilo Dmitry
 * @mailto wm.morgun@gmail.com
**/

(function($){
	/* Little trick to remove event bubbling that causes events recursion */
	var CB = function(e)
	{
		if (!e) var e = window.event;
		e.cancelBubble = true;
		if (e.stopPropagation) e.stopPropagation();
	};

	$.fn.checkbox = function(options) {
		/* IE6 background flicker fix */
		try	{ document.execCommand('BackgroundImageCache', false, true);	} catch (e) {}

		/* Default settings */
		var settings = {
			cls: 'safari-box',  /* checkbox  */
			empty: 'images/system/transparent.gif'  /* checkbox  */
		};

		/* Processing settings */
		settings = $.extend(settings, options || {});

		/* Adds check/uncheck & disable/enable events */
		var addEvents = function(object)
		{
			var checked = object.checked;
			var disabled = object.disabled;
			var $object = $(object);

			if ( object.stateInterval )
				clearInterval(object.stateInterval);

			object.stateInterval = setInterval(
				function()
				{
					if ( object.disabled != disabled )
						$object.trigger( (disabled = !!object.disabled) ? 'disable' : 'enable');
					if ( object.checked != checked )
						$object.trigger( (checked = !!object.checked) ? 'check' : 'uncheck');
				},
				10 /* in miliseconds. Low numbers this can decrease performance on slow computers, high will increase responce time */
			);
			return $object;
		};
		//try { console.log(this); } catch(e) {}

		/* Wrapping all passed elements */
		return this.each(function()
		{
			var ch = this; /* Reference to DOM Element*/
			var $ch = addEvents(ch); /* Adds custom events and returns, jQuery enclosed object */

			/* Removing wrapper if already applied  */
			if (ch.wrapper) ch.wrapper.remove();

			/* Creating wrapper for checkbox and assigning "hover" event */
			ch.wrapper = $('<span class="' + settings.cls + '"><span class="mark"><img src="' + settings.empty + '" /></span></span>');
			ch.wrapperInner = ch.wrapper.children('span:eq(0)');
			ch.wrapper.hover(
				function(e) { ch.wrapperInner.addClass(settings.cls + '-hover');CB(e); },
				function(e) { ch.wrapperInner.removeClass(settings.cls + '-hover');CB(e); }
			);

			/* Wrapping checkbox */
			$ch.css({position: 'absolute', zIndex: -1, visibility: 'hidden'}).after(ch.wrapper);

			/* Ttying to find "our" label */
			var label = false;
			if ($ch.attr('id'))
			{
				label = $('label[for='+$ch.attr('id')+']');
				if (!label.length) label = false;
			}
			if (!label)
			{
				/* Trying to utilize "closest()" from jQuery 1.3+ */
				label = $ch.closest ? $ch.closest('label') : $ch.parents('label:eq(0)');
				if (!label.length) label = false;
			}
			/* Labe found, applying event hanlers */
			if (label)
			{
				label.hover(
					function(e) { ch.wrapper.trigger('mouseover', [e]); },
					function(e) { ch.wrapper.trigger('mouseout', [e]); }
				);
				label.click(function(e) { $ch.trigger('click',[e]); CB(e); return false;});
			}
			ch.wrapper.click(function(e) { $ch.trigger('click',[e]); CB(e); return false;});
			$ch.click(function(e) { CB(e); });
			$ch.bind('disable', function() { ch.wrapperInner.addClass(settings.cls+'-disabled');}).bind('enable', function() { ch.wrapperInner.removeClass(settings.cls+'-disabled');});
			$ch.bind('check', function() { ch.wrapper.addClass(settings.cls+'-checked' );}).bind('uncheck', function() { ch.wrapper.removeClass(settings.cls+'-checked' );});

			/* Disable image drag-n-drop for IE */
			$('img', ch.wrapper).bind('dragstart', function () {return false;}).bind('mousedown', function () {return false;});

			/* Firefox antiselection hack */
			if ( window.getSelection )
				ch.wrapper.css('MozUserSelect', 'none');

			/* Applying checkbox state */
			if ( ch.checked )
				ch.wrapper.addClass(settings.cls + '-checked');
			if ( ch.disabled )
				ch.wrapperInner.addClass(settings.cls + '-disabled');
		});
	}
})(jQuery);