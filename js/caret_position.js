/*
Title:		Functions to manipulate the caret position inside a text box
File: 		js/caret_position.js
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

function CaretPosition() {
	var start = null;
	var end = null;
}

function getCaretPosition(oField) {
 
	// Initialise the CaretPosition object
	var oCaretPos = new CaretPosition();
	
	// IE support
	if(document.selection) {
		// Focus on the text box
		oField.focus();
		
		// This returns us an object containing
		// information about the currently selected text
		var oSel = document.selection.createRange();
		
		// Find out the length of the selected text
		// (you'll see why below)
		var selectionLength = oSel.text.length;
		
		// Move the selection start to 0 position.
		//
		// This is where it gets interesting, and this is why
		// some have claimed you can't get the caret positions
		// in IE.
		//
		// IE has no 'selectionStart' or 'selectionEnd' property,
		// so we can not get or set this value directly. We can
		// only move the caret positions relative to where they
		// currently are (this should make more sense when you read
		// the next line of code).
		//
		// Note, that even though we have moved the start
		// position on our object in memory, this is not reflected
		// in the browser until we call oSel.select() (which we're
		// not going to do here).
		//
		// Also note, the start position will never be a negative
		// number, no matter how far we try to move it back.
		oSel.moveStart ('character', -oField.value.length);
		
		// This is where it should start to make sense. We now know
		// our start caret position is the length of the currently
		// selected text minus the original selection length
		// (think about it).
		oCaretPos.start = oSel.text.length - selectionLength;
		
		// Since the start of the selection is at the start of the
		// text, we know that the length of the selection is also
		// the index of the end caret position.
		oCaretPos.end = oSel.text.length;
	}
	
	// Firefox support
	else if(oField.selectionStart || oField.selectionStart == '0') {
		// This is a whole lot easier in Firefox
		oCaretPos.start = oField.selectionStart;
		oCaretPos.end = oField.selectionEnd;
	}
	
	// Return results
	return (oCaretPos);
}

function setCaretPosition(oField, iCaretStart, iCaretEnd) {

	// IE Support
	if (document.selection) {
		// Focus on the text box
		oField.focus();
		
		// This returns us an object containing
		// information about the currently selected text
		var oSel = document.selection.createRange();
		
		// Since we don't know where the caret positions
		// currently are (see comments in getCaretPosition() for
		// further information on this), move them to position 0.
		//
		// Note, the caret positions will never be a negative
		// number, no matter how far we try to move them back.
		oSel.moveStart ('character', -oField.value.length);
		oSel.moveEnd ('character', -oField.value.length);
		
		// Now we know the caret positions are at index 0, move
		// them forward to the desired position (move end caret
		// position first - actually not sure if moving start
		// caret position first affects the end caret position -
		// it might).
		//
		// Note, we allow for a null end caret position and just
		// default it to the same as the start caret position.
		if(iCaretEnd != null)
		oSel.moveEnd ('character', iCaretEnd);
		else
		oSel.moveEnd ('character', iCaretStart);
		
		oSel.moveStart ('character', iCaretStart);
		
		// Everything thus far has just been on our object in
		// memory - this line actually updates the browser
		oSel.select();
	}
	
	// Firefox support
	else if(oField.selectionStart || oField.selectionStart == '0') {

		oField.selectionStart = iCaretStart;
		
		if(iCaretEnd != null)
		oField.selectionEnd = iCaretEnd;
		else
		oField.selectionEnd = iCaretStart;

		oField.focus();
	}
}

function insertAtCursor(oField, sValue) {
	//IE support
	if (document.selection) {
		oField.focus();
		
		//in effect we are creating a text range with zero
		//length at the cursor location and replacing it
		//with myValue
		sel = document.selection.createRange();
		sel.text = sValue;
	}
	
	//Mozilla/firefox/Netscape 7+ support
	else if (oField.selectionStart || oField.selectionStart == '0') {
	
		//Here we get the start and end points of the
		//selection. Then we create substrings up to the
		//start of the selection and from the end point
		//of the selection to the end of the field value.
		//Then we concatenate the first substring, myValue,
		//and the second substring to get the new value.
		var startPos = oField.selectionStart;
		var endPos = oField.selectionEnd;
		oField.value = oField.value.substring(0, startPos)+ sValue+ oField.value.substring(endPos, oField.value.length);
		} else {
		oField.value += sValue;
	}
} 