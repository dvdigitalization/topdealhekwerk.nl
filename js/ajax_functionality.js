/*
Title:		AJAX functionality functions
File: 		js/ajax_functionality.js
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

var ajax_query = null;

function innerHTML(obj, txt) {
 	if (document.getElementById(obj)) {
		document.getElementById(obj).innerHTML = txt;
	}
}

function sendHttpRequest(url,callbackFunc,respXml,task) {
	var X = initAjaxObject(url,callbackFunc,respXml,task);
	if (ajax_query) {
		ajax_query += '&ajax=1';
		X.send(ajax_query);
	}
}

function initAjaxObject(url, callbackFunc, respXml, task) {
	var X = null;

	var msxmlhttp = new Array(
				'Msxml2.XMLHTTP.5.0',
				'Msxml2.XMLHTTP.4.0',
				'Msxml2.XMLHTTP.3.0',
				'Msxml2.XMLHTTP',
				'Microsoft.XMLHTTP');

	for (var i = 0; i < msxmlhttp.length; i++) {
		try {
			X = new ActiveXObject(msxmlhttp[i]);
		}
		catch (e) {
			X = null;
		}
	}

	if (!X && typeof XMLHttpRequest != 'undefined') {
		try {
			X = new XMLHttpRequest();
		}
		catch(e){
			alert('AJAX isnt supported by your browser! Pity...');
			return false;
		}
	}

	X.onreadystatechange = function() {
		if(X.readyState == 4) {
			if(X.status == 200){
				respXml ? eval(callbackFunc+'(X.responseXML, task)') : eval(callbackFunc+'(X.responseText, task)');
			}
		}
	}

	X.open('POST',url,true);
	X.setRequestHeader('Method', 'POST ' + url + ' HTTP/1.1');
	X.setRequestHeader('Content-Type','application/x-www-form-urlencoded; charset=UTF-8');

	return X;
}

function makeQuerySafe(string) {
	string = encodeURIComponent(string);
	string = Url.encode(string);
	return string;
}

//URL encode / decode tool - http://www.webtoolkit.info
var Url = {

	// public method for url encoding
	encode : function (string) {
		return escape(this._utf8_encode(string));
	},

	// public method for url decoding
	decode : function (string) {
		return this._utf8_decode(unescape(string));
	},

	// private method for UTF-8 encoding
	_utf8_encode : function (string) {
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";

		for (var n = 0; n < string.length; n++) {

			var c = string.charCodeAt(n);

			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}

		}

		return utftext;
	},

	// private method for UTF-8 decoding
	_utf8_decode : function (utftext) {
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;

		while ( i < utftext.length ) {

			c = utftext.charCodeAt(i);

			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			}
			else if((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i+1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			}
			else {
				c2 = utftext.charCodeAt(i+1);
				c3 = utftext.charCodeAt(i+2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}

		}

		return string;
	}
}