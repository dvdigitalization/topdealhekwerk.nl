/*
Title:		Functions to manipulate focus/blur behavior of input elements by JS
File: 		js/focus_blur.js
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization

Comments:	This is now done with CSS in CMS v1.7 and up.
*/

function addFocusBlurBehaviour() {
	var input, textarea, select;

	var inputs = document.getElementsByTagName('input');
	for (var i = 0; (input = inputs[i]); i++) {
		
		if (input.className != 'input_button') {
			addEvent(input, 'focus', focusStyleEvent);
			addEvent(input, 'blur', blurStyleEvent);	
		}
	}
	
	var textareas = document.getElementsByTagName('textarea');
	for (var i = 0; (textarea = textareas[i]); i++) {
		addEvent(textarea, 'focus', focusStyleEvent);
		addEvent(textarea, 'blur', blurStyleEvent);
	}
	
	var selects = document.getElementsByTagName('select');
	for (var i = 0; (select = selects[i]); i++) {
		addEvent(select, 'focus', focusStyleEvent);
		addEvent(select, 'blur', blurStyleEvent);
	}	
}

function addEvent(obj, evType, fn){
	if (obj.addEventListener){
		obj.addEventListener(evType, fn, true);
		return true;
	} else if (obj.attachEvent){
		var r = obj.attachEvent("on"+evType, fn);
		return r;
	} else {
		return false;
	}
}

function getEventSource(e) {
	
	if (typeof e == 'undefined') {
		var e = window.event;
	}
	
	var source;
	
	if (typeof e.target != 'undefined') {
		source = e.target;
	} else if (typeof e.srcElement != 'undefined') {
		source = e.srcElement;
	} else {
		source = null;
	}
	
	return source;
}

function focusStyleEvent(e) {
 	if (source = getEventSource(e)) {
	 	source.style['background'] = '#94d744';
	}
}

function blurStyleEvent(e) {
 	if (source = getEventSource(e)) {
	 	source.style['background'] = '#b9ed7b';
	} 
}

function focusStyleObj(obj) {
 	obj.style['background'] = '#ffffff';
}

function blurStyleObj(obj) {
 	obj.style['background'] = '#f5f5f5';
}