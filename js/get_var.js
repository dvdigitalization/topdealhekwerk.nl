/*
Title:		Function to let javascript read $_GET variables
File: 		js/get_var.js
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

function get_var(name) {
	get_string = document.location.search;         
	return_value = '';
	
	do { //This loop is made to catch all instances of any get variable.
	name_index = get_string.indexOf(name + '=');
	
	if(name_index != -1)
	  {
	  get_string = get_string.substr(name_index + name.length + 1, get_string.length - name_index);
	  
	  end_of_value = get_string.indexOf('&');
	  if(end_of_value != -1)                
	    value = get_string.substr(0, end_of_value);                
	  else                
	    value = get_string;                
	    
	  if(return_value == '' || value == '')
	     return_value += value;
	  else
	     return_value += ', ' + value;
	  }
	} while(name_index != -1)
	
	//Restores all the blank spaces.
	space = return_value.indexOf('+');
	while(space != -1)
	  { 
	  return_value = return_value.substr(0, space) + ' ' + 
	  return_value.substr(space + 1, return_value.length);
					 
	  space = return_value.indexOf('+');
	  }
	
	return(return_value);        
}