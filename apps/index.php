<?php
/*
Title:		Index redirect away from forbidden boo boo no access directories
File: 		index.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl	
*/

header('Location: ../index.php');
exit;
?>