/*
 * For FCKeditor 2.3
 *
 *
 * File Name: nl.js
 * 	Dutch language file for the youtube plugin.
 *
 * File Authors:
 * 		Digitalization
 */

FCKLang['YouTubeTip']			= 'YouTube video toevoegen/wijzigen' ;
FCKLang['DlgYouTubeTitle']		= 'YouTube Video' ;
FCKLang['DlgYouTubeCode']		= '"Voeg de URL in van de YouTube video."' ;
FCKLang['DlgYouTubeSecurity']	= 'Ongeldige URL.' ;
FCKLang['DlgYouTubeURL']	    = 'URL' ;
FCKLang['DlgYouTubeWidth']	    = 'Breedte' ;
FCKLang['DlgYouTubeHeight']	    = 'Hoogte' ;
FCKLang['DlgYouTubeQuality']    = 'Kwaliteit' ;
FCKLang['DlgYouTubeLow']	    = 'Laag' ;
FCKLang['DlgYouTubeHigh']	    = 'Hoog (indien beschikbaar)' ;
