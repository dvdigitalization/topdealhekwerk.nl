//Register the related commands
FCKCommands.RegisterCommand('Custom_Tags', new FCKDialogCommand(FCKLang['CustomTagsTitle'], FCKLang['CustomTagsTitle'], FCKConfig.PluginsPath + 'customtags/customtags.html', 600, 300));

//Create the toolbar button
var oCustomTags = new FCKToolbarButton('Custom_Tags', FCKLang['CustomTags'],FCKLang['CustomTagsTitle'],FCK_TOOLBARITEM_ICONTEXT,true,true,37);
oCustomTags.IconPath = FCKConfig.PluginsPath + 'customtags/customtags.gif';
FCKToolbarItems.RegisterItem('Custom_Tags', oCustomTags);
