<?php
/*
Title:		Standard contact page with form
File: 		contact.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

/******************************************************************************************
 * Settings
 ***/

//Page sets
$page_set 		= 	array('contact','spam');
$html_name		=	'contact.html';

//Required files
require_once('inc/site_header.php');

//Crumb
$crumbs[$html_name] = 'Contact';

//Initial values
$show_form				=	true;
$fields_missing			=	false;
$form_html				=	'';

//Settings
$required_fields		=	array('first_name','last_name','address','postal_code','city','email','phone_number','question');
$fields_to_mail			=	array('first_name','last_name','address','postal_code','city','email','phone_number','question');
$mail_to				=	explode(';',$cfg['CONTACT_FORM_EMAIL']);

/******************************************************************************************
 * Form processing
 ***/

if (isset($_POST['send_form'])) {

	//Clean recipients
	foreach ($mail_to as $key => $recipient) {
		evalTrim($mail_to[$key]);
		if (empty($mail_to[$key])) {
			unset($mail_to[$key]);
		}
	}

	//Data cleansing
	foreach (array_keys($_POST) as $key) {
		evalAll($_POST[$key]);
	}

	//Load spam shields
	$spam_check_string = $_POST['question'];
	require(SITE_PATH_PREFIX.'inc/spamshields.php');

	//Missing fields check
	foreach ($required_fields as $required_field) {
		if (empty($_POST[$required_field])) {
			$fields_missing = true;
			break;
		}
	}

	//Check recipients
	if (!count($mail_to)) {
		$error 		= 'Email adres niet ingesteld! Stel dit via het CMS in voordat u het contactformulier gebruikt.';
		$show_form	= true;
	}

	//IP blocked
	elseif ($ip_blocked) {
		$error 		= IP_IS_BLOCKED;
		$show_form 	= false;
	}

	//Very bad word found
	elseif ($very_bad_word_found) {
	 	$error 		= str_replace('[BAD_WORD]',$very_bad_word_found, BAD_WORD).'<br/><br/>'.NO_TOLERANCE_WORD;
		$show_form 	= false;
	}

	//Bad word found
	elseif ($bad_word_found) {
		$error 		= str_replace('[BAD_WORD]',$bad_word_found, BAD_WORD);
		$show_form 	= true;
	}

	//Check for required fields
	elseif ($fields_missing) {
		$error 		= CF_FIELDS_MISSING;
		$show_form 	= true;
	}

	//Email validation
	elseif (in_array('email',$required_fields) and !$_POST['email'] = validateEmail($_POST['email'])) {
		$error 		= CF_INVALID_EMAIL;
		$show_form 	= true;
	}

	//All ok
	else {

		//Set the email message
		$cf_message = CF_EMAIL_TEXT.'<br/>';

		foreach ($fields_to_mail as $field_to_mail) {
			$cf_message .= '<br/><b>'.ucfirst($field_to_mail).':</b> '.nl2br($_POST[$field_to_mail]);
		}

		//Send the email
		require_once(SITE_PATH_PREFIX.'inc/sendmail.php');
		sendMail($mail_to, CF_EMAIL_TITLE, $cf_message);

		$msg		= CF_RECEIVED;
		$show_form 	= false;
	}
}

/******************************************************************************************
 * Page contents
 ***/

//Error?
$error 	= isset($error) ? "<p>&nbsp;</p><p class='error_message'>$error</p>" : '';
$msg 	= isset($msg) 	? "<p>&nbsp;</p><p class='message'>$msg</p>" : '';

//Do we show the form?
if ($show_form)  {

	$form_html = "
	<p>&nbsp;</p>
	<div class='table'>
		<form name='contact_form' method='post' action='$_file'>
		<table width='100%' cellpadding='0' cellspacing='0' border='0'>
			<tr>
				<td width='140' height='22' align='left' valign='middle'>Voornaam:</td>
				<td width='15' align='left' valign='middle'>*</td>
				<td align='left' valign='middle'><input type='text' name='first_name' value='".request('first_name')."' class='formMed' /></td>
			</tr>
			<tr>
				<td width='140' height='22' align='left' valign='middle'>Achternaam:</td>
				<td width='15' align='left' valign='middle'>*</td>
				<td align='left' valign='middle'><input type='text' name='last_name' value='".request('last_name')."' class='formMed' /></td>
			</tr>
			<tr>
				<td width='140' height='22' align='left' valign='middle'>Adres:</td>
				<td width='15' align='left' valign='middle'>*</td>
				<td align='left' valign='middle'><input type='text' name='address' value='".request('address')."' class='formMed' /></td>
			</tr>
			<tr>
				<td width='140' height='22' align='left' valign='middle'>Postcode:</td>
				<td width='15' align='left' valign='middle'>*</td>
				<td align='left' valign='middle'><input type='text' name='postal_code' value='".request('postal_code')."' class='formMed' /></td>
			</tr>
			<tr>
				<td width='140' height='22' align='left' valign='middle'>Plaats:</td>
				<td width='15' align='left' valign='middle'>*</td>
				<td align='left' valign='middle'><input type='text' name='city' value='".request('city')."' class='formMed' /></td>
			</tr>
			<tr>
				<td width='140' height='22' align='left' valign='middle'>E-mail:</td>
				<td width='15' align='left' valign='middle'>*</td>
				<td align='left' valign='middle'><input type='text' name='email' value='".request('email')."' class='formMed' /></td>
			</tr>
			<tr>
				<td width='140' height='22' align='left' valign='middle'>Telefoonnummer:</td>
				<td width='15' align='left' valign='middle'>*</td>
				<td align='left' valign='middle'><input type='text' name='phone_number' value='".request('phone_number')."' class='formMed' /></td>
			</tr>
			<tr>
				<td width='140' height='22' align='left' valign='top' style='padding:3px 0 0;'>Stel uw vraag:</td>
				<td width='15' align='left' valign='top' style='padding:3px 0 0;'>*</td>
				<td align='left' valign='middle'><textarea name='question' class='formLarge' rows='' cols=''>".request('question')."</textarea></td>
			</tr>
			<tr>
				<td width='140' height='22' align='left' valign='middle'></td>
				<td width='15' align='left' valign='middle'>&nbsp;</td>
				<td align='left' valign='middle'>* Verplichte velden</td>
			</tr>
			<tr>
				<td height='22' align='left' valign='bottom' colspan='3'>
					<div class='divider'>
						<div class='verstuurBtn'><input type='submit' name='send_form' class='verstuurBtn' value=''/></div>
					</div>
				</td>
			</tr>
		</table>
		</form>
	</div>
	";
}

//Main HTML contents
$page_contents .= "
	$error
	$msg
	$form_html
";

//Output
echo $page_contents;

require_once('inc/site_footer.php');
?>