<?php
/*
Title:		Checkout
File: 		checkout.php
Version: 	v2.20
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2009 by Digitalization
*/

/******************************************************************************************
 * Settings
 ***/

//Page set and title
$page_set 	=  	array('Kassa');
$page_image	=	'gfx/layout/sfeer/sfeer_gaas.jpg';
$page_title	=	'Kassa';

//Required files
require_once('inc/site_header.php');

//Database tables
$db_table_customers		=	DIGI_DB_PREFIX.'topdeal_customers';
$db_table_orders		=	DIGI_DB_PREFIX.'topdeal_orders';
$template				=	'static/checkout.html';
$sidebar				=	'';

//File definitions
$_file					=	$cfg['USE_REWRITE_ENGINE'] ? 'kassa.html' 				: $_file;
$file_cart				=	$cfg['USE_REWRITE_ENGINE'] ? 'winkelwagen.html' 		: 'shopping-cart.php';
$file_success			=	$cfg['USE_REWRITE_ENGINE'] ? 'betaling-voltooid.html' 	: 'payment-completed.php';
$file_conditions		=	'leveringsvoorwaarden.html';

//Payment settings
$bpe_server				=	'https://payment.buckaroo.nl/gateway/ideal_payment.asp';
$bpe_merchant			=	'K6rKsQzNQK';
$bpe_secret_key			=	'M77ASweP';
$bpe_testmode			=	false;
$bpe_success			=	$_link.'betaling-voltooid.html';
$bpe_fail				=	$_link.'betaling-mislukt.html';

/******************************************************************************************
 * Startup stuff
 ***/

//Load cart from session
if (!isset($_SESSION['cart']) or !$_SESSION['cart'] or !is_array($_SESSION['cart']) or isset($_GET['clear_cart'])) {
	$_SESSION['cart'] = array();
}
$cart =& $_SESSION['cart'];

//If no items, send the customer to the shopping bag
if (!count($cart)) {
	header('Location: '.$file_cart);
	exit;
}

//Clean POST variables
foreach (array_keys($_POST) as $key) {
	evalAll($_POST[$key]);
}

/******************************************************************************************
 * Read order from database, if order ID given. The customer will try to pay again.
 ***/

if (isset($_SESSION['order']) and $order = $_SESSION['order'] and is_array($order)) {

	//Put customer variables in $_GET so that the form can be pre-completed
	$customer_fields = array(
		'title','first_name','prefix','last_name','address','postal_code','city','country','email','phone','mobile','newsletter'
	);
	foreach ($customer_fields as $field) {
		$_GET[$field] = $order['customer'][$field];
	}

	//Delivery variables
	$delivery_fields = array(
		'address','postal_code','city','country'
	);
	foreach ($delivery_fields as $field) {
		$_GET['d_'.$field] = $order['delivery'][$field];
	}

	//Order details
	$_GET['remarks'] 			= 	$order['remarks'];
	$_GET['delivery_date'] 		= 	$order['delivery_date'];
	$_GET['payment_method'] 	= 	$order['payment_method'];
	$_GET['delivery_address']	=	$order['diff_delivery'];
}

/******************************************************************************************
 * Process order form
 ***/

if (isset($_POST['submit_form'])) {

	//Required fields
	$required_fields = array(
		'title','first_name','last_name','address','postal_code','city','country','email','phone','delivery_date'
	);

	//Delivery address = more required fields
	if (isset($_POST['delivery_address'])) {
		$required_fields = array_merge($required_fields, array('d_address','d_postal_code','d_city','d_country'));
	}

	//Missing fields check
	$fields_missing = false;
	$missing = array();
	foreach ($required_fields as $required_field) {
		if (!$_POST[$required_field]) {
			$fields_missing = true;
			$missing[$required_field] = true;
		}
	}

	//Check for missing fields
	if ($fields_missing) {
		$error 	= 	CHECKOUT_FIELDS_MISSING;
	}

	//Email validation
	elseif (in_array('email',$required_fields) and !$_POST['email'] = validateEmail($_POST['email'])) {
		$error 	= 	CHECKOUT_INVALID_EMAIL;
		$missing['email'] = true;
	}

	//Agreement check
	elseif (!isset($_POST['agreement'])) {
		$error 	= 	CHECKOUT_AGREEMENT;
		$missing['agreement'] = true;
	}

	//All good
	else {

		//Customer data
		$customer = array(
				'title'			=>	$_POST['title'],
				'first_name'	=>	$_POST['first_name'],
				'prefix'		=>	$_POST['prefix'],
				'last_name'		=>	$_POST['last_name'],
				'email'			=>	$_POST['email'],
				'address'		=>	$_POST['address'],
				'postal_code'	=>	$_POST['postal_code'],
				'city'			=>	$_POST['city'],
				'country'		=>	$_POST['country'],
				'phone'			=>	$_POST['phone'],
				'mobile'		=>	$_POST['mobile'],
				'newsletter'	=>	isset($_POST['newsletter']) ? 1 : 0
		);

		//Delivery address data specified?
		if (isset($_POST['delivery_address'])) {
			$delivery = array(
				'address'		=>	$_POST['d_address'],
				'postal_code'	=>	$_POST['d_postal_code'],
				'city'			=>	$_POST['d_city'],
				'country'		=>	$_POST['d_country'],
			);
		}

		//Just use the customer data
		else {
			$delivery = $customer;
		}

		//Check if customer already exists maybe. Match on email address for now
		if ($customer_id = isDoublePost($db_table_customers, array('email'), $customer['email'])) {
			upRec($db_table_customers, $customer_id,	array(	'title','first_name','prefix','last_name','email','address','postal_code',
																'city','country','phone','mobile','newsletter'),
														array(	$customer['title'],$customer['first_name'],$customer['prefix'],
																$customer['last_name'],$customer['email'],$customer['address'],
																$customer['postal_code'],$customer['city'],$customer['country'],
																$customer['phone'],$customer['mobile'],$customer['newsletter']));
		}

		//Insert the customer
		else {
			$customer_id = insRec($db_table_customers, 	array(	'title','first_name','prefix','last_name','email','address','postal_code',
																'city','country','phone','mobile','newsletter'),
														array(	$customer['title'],$customer['first_name'],$customer['prefix'],
																$customer['last_name'],$customer['email'],$customer['address'],
																$customer['postal_code'],$customer['city'],$customer['country'],
																$customer['phone'],$customer['mobile'],$customer['newsletter']));
		}

		//Total price
		list ($sub_total, $delivery_costs, $discount, $vat, $total, $discount_info) = get_cart_amounts();

		//Payment type
		if ($payment_method = $_POST['payment_method'] and ($payment_method == 50 or $payment_method == 100)) {

			if ($payment_method == 50) {
				$to_pay 			= (int) floor($total/2);
			}
			elseif ($payment_method == 100) {
				$to_pay 			= (int) $total;
			}

			$pay_info 			= $payment_method == 50 ? "iDEAL (50% vooraf betalen en 50% overmaken na levering)" : "iDEAL (100% vooraf betalen)";
			$payment_link		= $bpe_server;
			$extra_info 		= BETALING_DOORNAARIDEAL;
		}
		elseif ($payment_method == 'rembours') {
			$total				+= (int) $cfg['REMBOURS_COSTS'];
			$to_pay 			= (int) $total;
			$pay_info 			= "rembours betaling";
			$payment_link		= 'betaling-voltooid.html';
			$extra_info 		= BETALING_BETAALBIJLEVERING;
		}

		//Make the order object
		$order = array(
			'customer'		=>	$customer,
			'delivery'		=>	$delivery,
			'cart'			=>	$cart,
			'diff_delivery'	=>	isset($_POST['delivery_address']) ? 1 : 0,
			'remarks'		=>	$_POST['remarks'],
			'delivery_date'	=>	$_POST['delivery_date'],
			'order_date'	=>	date('d-m-Y'),
			'order_total'	=>	$total,
			'payment_method'=>	$_POST['payment_method'],
			'still_to_pay'	=>	($payment_method == 'rembours') ? $total : $total-$to_pay
		);

		//Save to session
		$_SESSION['order'] = $order;

		//Insert the serialized order
		$order_id = insRec($db_table_orders, 	array('customer_id','total_price','payment_status','payment_method','order_serialized'),
												array($customer_id, $total, 0, $_POST['payment_method'], serialize($order)));

		//Make the invoice nr
		$invoice_nr			=	sprintf('F%05d', $order_id);

		//Generate the signature
		$bpe_signature		=	md5($bpe_merchant.$invoice_nr.$to_pay.'EUR'.($bpe_testmode ? 1 : 0).$bpe_secret_key);

		//Set the fields
		$fields = array('BPE_Merchant'		=>	$bpe_merchant,
						'BPE_Amount'		=>	$to_pay,
						'BPE_Currency'		=>	'EUR',
						'BPE_Description'	=>	'Uw Topdeal Hekwerk bestelling',
						'BPE_Reference'		=>	$order_id,
						'BPE_Language'		=>	'NL',
						'BPE_Mode'			=>	($bpe_testmode ? 1 : 0),
						'BPE_Signature2'	=>	$bpe_signature,
						'BPE_Invoice'		=>	$invoice_nr,
						'BPE_Return_Success'=>	$bpe_success,
						'BPE_Return_Reject'	=>	$bpe_fail,
						'BPE_Return_Error'	=>	$bpe_fail,
						'BPE_Return_Method'	=>	'POST');
		$fields = implode_special("\n", $fields, "<input type='hidden' name='[KEY]' value='[VALUE]' />");

		//Pay info
		$pay_info = str_replace(array('<p>','</p>','[BETALING]','[BEDRAG]'),  array('','', $pay_info, parseAmount($to_pay, '&euro; ')),  BETALEN_INFO);
		$pay_info .= "<br/><br/>".$extra_info;

		//Page contents
		$page_contents 	= "
			<div class='blok'>
				<h1>Kassa</h1>
				<hr/>
			</div>
			<div class='blok'>
				<h2 class='table'>Betaling</h2>
				<form name='payment_form' method='post' action='$payment_link'>
				<input type='hidden' name='order_id' value='$order_id'/>
				$fields
				<div class='table'>
					<table width='100%' cellpadding='0' cellspacing='0' border='0'>
						<tr>
							<td height='22' align='left' valign='middle'>$pay_info</td>
						</tr>
						<tr>
							<td height='22' align='left' valign='middle'>&nbsp;</td>
						</tr>
						<tr>
							<td height='22' align='left' valign='bottom'>

								<div class='divider'>
									<div class='verstuurBtn'><input type='submit' name='submit_form' value='' /></div>
								</div>
							</td>
						</tr>
					</table>
				</div>
				</form>
			</div>
		";
	}
}

/******************************************************************************************
 * Determine delivery days
 ***/

$min_days = 3;
foreach ($cart as $item) {
	if ($delivery = (int) $item['delivery'] and $delivery > $min_days) {
		$min_days = $delivery;
	}
}
$datepicker_min_date = '+'.$min_days.'d';

/******************************************************************************************
 * Form
 ***/

if (!isset($order_id)) {

	//Error
	$error = isset($error) ? "<div class='blok'><p class='error_message'>$error</p></div>" : '';

	//Get cart amounts
	list ($sub_total, $delivery, $discount, $vat, $total, $discount_info) = get_cart_amounts();

	//Default delivery date
	$default_delivery_date = date('d-m-Y', next_non_weekend_day(time()+$min_days*86400));

	//Page contents
	$page_contents 	= "
		<div class='blok'>
			<h1>Kassa</h1>
			<hr/>
		</div>
		$error
		<div class='blok'>
			<h2 class='table'>Uw gegevens</h2>
			<form name='order_form' method='post' action='$_file'>
			<input type='hidden' name='no_header' value='1' />
			<div class='table'>
				<table width='100%' cellpadding='0' cellspacing='0' border='0'>
					<tr>
						<td width='140' height='22' align='left' valign='middle' ".(isset($missing['title']) ? "class='foutmelding'" : '').">Aanhef:</td>

						<td width='15' align='left' valign='middle'>*</td>
						<td align='left' valign='middle'>
							<table cellpadding='0' cellspacing='0' border='0'>
								<tr>
									<td width='25' height='22' align='left' valign='middle'><input type='radio' value='1' name='title' ".((request('title') == 1 or !request('title')) ? "checked='checked'" : '')." /></td>
									<td width='25' align='left' valign='middle'>Dhr.</td>
									<td width='25' align='left' valign='middle'><input type='radio' name='title' value='2' ".((request('title') == 2) ? "checked='checked'" : '')."/></td>
									<td align='left' valign='middle'>Mevr.</td>

								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td width='140' height='22' align='left' valign='middle'".(isset($missing['first_name']) ? "class='foutmelding'" : '').">Voornaam:</td>
						<td width='15' align='left' valign='middle'>*</td>
						<td align='left' valign='middle'><input type='text' name='first_name'  value='".request('first_name')."' class='formMed' /></td>

					</tr>
					<tr>
						<td width='140' height='22' align='left' valign='middle'".(isset($missing['prefix']) ? "class='foutmelding'" : '').">Tussenvoegsel:</td>
						<td width='15' align='left' valign='middle'>&nbsp;</td>
						<td align='left' valign='middle'><input type='text' name='prefix'  value='".request('prefix')."' class='formMed' /></td>
					</tr>
					<tr>
						<td width='140' height='22' align='left' valign='middle'".(isset($missing['last_name']) ? "class='foutmelding'" : '').">Achternaam:</td>

						<td width='15' align='left' valign='middle'>*</td>
						<td align='left' valign='middle'><input type='text' name='last_name' value='".request('last_name')."' class='formMed' /></td>
					</tr>
					<tr>
						<td width='140' height='22' align='left' valign='middle'".(isset($missing['address']) ? "class='foutmelding'" : '').">Adres en huisnummer:</td>
						<td width='15' align='left' valign='middle'>*</td>
						<td align='left' valign='middle'><input type='text' name='address'  value='".request('address')."' class='formMed' /></td>

					</tr>
					<tr>
						<td width='140' height='22' align='left' valign='middle'".(isset($missing['postal_code']) ? "class='foutmelding'" : '').">Postcode:</td>
						<td width='15' align='left' valign='middle'>*</td>
						<td align='left' valign='middle'><input type='text' name='postal_code'  value='".request('postal_code')."' class='formMed' /></td>
					</tr>
					<tr>
						<td width='140' height='22' align='left' valign='middle'".(isset($missing['city']) ? "class='foutmelding'" : '').">Woonplaats:</td>

						<td width='15' align='left' valign='middle'>*</td>
						<td align='left' valign='middle'><input type='text' name='city'  value='".request('city')."' class='formMed' /></td>
					</tr>
					<tr>
						<td width='140' height='22' align='left' valign='middle'".(isset($missing['country']) ? "class='foutmelding'" : '').">Land:</td>
						<td width='15' align='left' valign='middle'>*</td>
						<td align='left' valign='middle'>
							".makeSelectBox('country', array(124 => 'Nederland', 17 => 'Belgi&euml;', 64 => 'Duitsland'), request('country', 124), 0, "class='selectMed'")."
						</td>
					</tr>
					<tr>
						<td width='140' height='22' align='left' valign='middle'".(isset($missing['phone']) ? "class='foutmelding'" : '').">Telefoonnummer:</td>

						<td width='15' align='left' valign='middle'>*</td>
						<td align='left' valign='middle'><input type='text' name='phone'  value='".request('phone')."' class='formMed' /></td>
					</tr>
					<tr>
						<td width='140' height='22' align='left' valign='middle'".(isset($missing['mobile']) ? "class='foutmelding'" : '').">Mobiel nummer:</td>
						<td width='15' align='left' valign='middle'>&nbsp;</td>
						<td align='left' valign='middle'><input type='text' name='mobile'  value='".request('mobile')."' class='formMed' /></td>
					</tr>

					<tr>
						<td width='140' height='22' align='left' valign='middle'".(isset($missing['email']) ? "class='foutmelding'" : '').">E-mail:</td>
						<td width='15' align='left' valign='middle'>*</td>
						<td align='left' valign='middle'><input type='text' name='email'  value='".request('email')."' class='formMed' /></td>
					</tr>
				</table>
			</div>
			<div class='table'>

				<table width='100%' cellpadding='0' cellspacing='0' border='0'>
					<tr>
						<td width='25' height='22' align='left' valign='middle'><input type='checkbox' name='delivery_address'  ".(request('delivery_address') ? "checked='checked'" : "'")." onclick='if (this.checked == true) {displayDiv(\"Verzendadres\", 1);} else {displayDiv(\"Verzendadres\", 0);}' id='delivery_address' /></td>
						<td align='left' valign='middle'>Ik wil graag dat de bestelling op een ander adres wordt bezorgd.</td>
					</tr>
				</table>
			</div>
			<div class='table' id='Verzendadres' ".(request('delivery_address') ? "" : "style='display:none;'").">

				<table width='100%' cellpadding='0' cellspacing='0' border='0'>
					<tr>
						<td width='140' height='22' align='left' valign='middle'".(isset($missing['d_address']) ? "class='foutmelding'" : '').">Adres en huisnummer:</td>
						<td width='15' align='left' valign='middle'>*</td>
						<td align='left' valign='middle'><input type='text' name='d_address' value='".request('d_address')."' class='formMed' /></td>
					</tr>
					<tr>
						<td width='140' height='22' align='left' valign='middle'".(isset($missing['d_postal_code']) ? "class='foutmelding'" : '').">Postcode:</td>

						<td width='15' align='left' valign='middle'>*</td>
						<td align='left' valign='middle'><input type='text' name='d_postal_code' value='".request('d_postal_code')."' class='formMed' /></td>
					</tr>
					<tr>
						<td width='140' height='22' align='left' valign='middle' ".(isset($missing['d_city']) ? "class='foutmelding'" : '').">Woonplaats:</td>
						<td width='15' align='left' valign='middle'>*</td>
						<td align='left' valign='middle'><input type='text' name='d_city' value='".request('d_city')."' class='formMed' /></td>

					</tr>
					<tr>
						<td width='140' height='22' align='left' valign='middle' ".(isset($missing['d_country']) ? "class='foutmelding'" : '').">Land:</td>
						<td width='15' align='left' valign='middle'>*</td>
						<td align='left' valign='middle'>
							".makeSelectBox('d_country', array(124 => 'Nederland', 17 => 'Belgi&euml;', 64 => 'Duitsland'), request('d_country', 124), 0, "class='selectMed'")."
						</td>
					</tr>
				</table>
				<script type='text/javascript'>
					if ($('input#delivery_address').attr('checked') == true) {
						displayDiv(\"Verzendadres\", 1);
					}
				</script>
			</div>

			<div class='table'>
				<table width='100%' cellpadding='0' cellspacing='0' border='0'>
					<tr>
						<td>Opmerkingen bij uw bestelling:<br/>
						<textarea name='remarks' style='width: 322px; height: 100px;'>".request('remarks')."</textarea></td>
					</tr>
				</table>
			</div>

			<h2 class='table'>Gewenste betaalmethode</h2>

			<div class='table'>
				<table width='100%' cellpadding='0' cellspacing='0' border='0'>
					<tr>
						<td height='22' align='left' valign='middle' colspan='2'>Het totaalbedrag van uw bestelling bedraagt <strong>".parseAmount($total, '&euro; ')."</strong>. Kies uw gewenste betaalmethode:</td>
					</tr>
					<tr>
						<td width='25' height='22' align='left' valign='middle'><input type='radio' name='payment_method' ".((($pm = request('payment_method')) == 100 or !$pm) ? "checked='checked'" : '')." value='100' /></td>
						<td align='left' valign='middle'>iDEAL (100% vooraf betalen met iDEAL)</td>
					</tr>
					<tr>
						<td width='25' height='22' align='left' valign='middle'><input type='radio' name='payment_method' value='50' ".($pm == 50 ? "checked='checked'" : '')."/></td>
						<td align='left' valign='middle'>iDEAL (50% vooraf betalen en 50% contant bij levering)</td>
					</tr>
					<tr>
						<td width='25' height='22' align='left' valign='middle'><input type='radio' name='payment_method' value='rembours' ".($pm == 'rembours' ? "checked='checked'" : '')."/></td>
						<td align='left' valign='middle'>Rembours (betalen bij levering, extra kosten: ".parseAmount($cfg['REMBOURS_COSTS'], 1).")</td>
					</tr>
				</table>
			</div>
			<h2 class='table'>Gewenste afleverdatum</h2>
			<div class='table'>
				<table width='100%' cellpadding='0' cellspacing='0' border='0'>
					<tr>
						<td width='140' height='22' align='left' valign='middle'".(isset($missing['delivery_date']) ? "class='foutmelding'" : '').">Kies een datum:</td>
						<td width='15' align='left' valign='middle'>*</td>
						<td width='150'align='left' valign='middle'><input type='text' readonly='readonly' name='delivery_date' id='delivery_date' class='datepicker formMed' value='".request('delivery_date', $default_delivery_date)."' style='width: 110px;'/></td>
						<td align='left' valign='top'>
							<div class='info' id='infoDatum' style='display:none;'>
								".str_replace('[DAGEN]', $min_days, INFO_DELIVERY_DATE)."
								<div class='sluiten'><a href='#infoDatum' onclick='displayDiv(\"infoDatum\", 0);return false'>Sluit dit venster</a></div>
							</div>
							&nbsp; <a onclick='displayDiv(\"infoDatum\", 1); return false;' name='infoDatum' class='infoIcon'><img src='gfx/layout/nav/info.gif' alt='Meer informatie' title='Meer informatie' style='margin:3px 0 0;' /></a>
						</td>
					</tr>
				</table>

			</div>
			<div class='table'>
				<table width='100%' cellpadding='0' cellspacing='0' border='0'>
					<tr>
						<td width='25' height='22' align='left' valign='middle'><div><input type='checkbox' name='agreement' ".(request('agreement') ? "checked='checked'" : '')."/></div></td>
						<td align='left' valign='middle' colspan='5' ".(isset($missing['agreement']) ? "class='foutmelding'" : '').">Ik ga akkoord met de <a href='http://www.topdealhekwerk.nl/leveringsvoorwaarden.html' target='_blank'>algemene voorwaarden.</a> *</td>
					</tr>

				</table>
				<table width='100%' cellpadding='0' cellspacing='0' border='0'>
					<tr>
						<td width='25' height='22' align='left' valign='middle'><input type='checkbox' name='newsletter' ".(request('newsletter') ? "checked='checked'" : '')." /></td>
						<td align='left' valign='middle' colspan='5'>Houd mij op de hoogte van de acties van Topdealplaza!</td>
					</tr>
				</table>

			</div>
			<div class='table'>
				<table width='100%' cellpadding='0' cellspacing='0' border='0'>
					<tr>
						<td height='22' align='left' valign='middle'>* Verplichte velden</td>
					</tr>
					<tr>
						<td height='22' align='left' valign='bottom'>

							<div class='divider'>
								<div class='verstuurBtn'><input type='submit' name='submit_form' value='' /></div>
							</div>
						</td>
					</tr>
				</table>
			</div>
			</form>
		</div>
	";

	$sidebar = "
	<div class='prodHorzPriceBestellen'>
		<h4 style='color: white;'>Totaalprijs</h4>
		<h5>".parseAmount($total, '&euro; ')."</h5>
	</div>
	";
}

//Output
echo $page_contents;

require_once('inc/site_footer.php');
?>