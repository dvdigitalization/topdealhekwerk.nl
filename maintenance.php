<?php
/*
Title:		Standard maintenance notice page
File: 		maintenance.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

require_once('inc/site_ajax_header.php');

echo "
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html>

<head>
	<meta http-equiv='content-type' content='text/html; charset=utf-8' />
	<title>$site_name in onderhoud</title>
</head>

<body style='background: black; color: white; font-family: Verdana; font-size: 11px;'>

<br/><br/><br/><br/><img src='gfx/site/digi/maintenance.gif' style='float: left; margin-right: 20px;'/> <h1 style='letter-spacing: 5px;'>$site_name in onderhoud</h1>
<br/>Deze website is momenteel in onderhoud door <b>$cfg[TECH_NAME]</b>.
<br/>Naar verwachting zal de website weer beschikbaar zijn op:

<br/><br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>$cfg[MAINTENANCE_END_TIME]</b>

</body>
</html>
";
?>