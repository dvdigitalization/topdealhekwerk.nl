<?php
/*
Title:		Global functions for CMS only
File: 		inc/globals_cms.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

//Change a configuration setting externally
function changeSetting($setting, $value) {
	global $cfg;
	upRec(DIGI_DB_PREFIX.'settings',"setting = '$setting'",'setting_value',$value);
	$cfg[$setting] = $value;
}

//Check if a connection with the Digi server can be established
function canConnect($host = '194.171.50.215', $port = 80) {
	if (function_exists('socket_create')) {
		$protocol 	=	defined('SOL_TCP') ? SOL_TCP : @getprotobyname('tcp');
		$socket		=	@socket_create(AF_INET, SOCK_STREAM, $protocol);
		$connect	=	@socket_connect($socket, $host, $port);
		@socket_close($socket);
		return $connect;
	}
	elseif (function_exists('fsockopen')) {
		$connect 	= 	@fsockopen($host, $port);
		@fclose($connect);
		return $connect;
	}
	elseif (function_exists('curl_init')) {
		$ch = @curl_init();
		@curl_setopt($ch, CURLOPT_URL, $host);
		@curl_setopt($ch, CURLOPT_HEADER, 0);
		@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$connect = @curl_exec($ch);
		@curl_close($ch);
		return $connect;
	}
	else {
		return false;
	}
}

//Check to see if user has been browsing back
function noBrowse() {

	global $_file;

	if (!isset($_SESSION['rs']) or !is_array($_SESSION['rs'])) {
		$_SESSION['rs'] = array();
	}

	if (!isset($_GET['rs'])) {
		trigger_error("Random string not given in $_file: ".print_r($_GET,true), E_USER_WARNING);
		return false;
	}

	if (!isset($_SESSION['rs'][$_GET['rs']])) {
		$_SESSION['rs'][$_GET['rs']] = 1;
		return true;
	}
	return false;
}

//To fix the sort order, when deleting an item from an ordered table
function fixSortOrder($db_table, $sort_field, $sort_order, $extra_where = '') {
	
	$sort_order++;
	if ($extra_where) 	$where 	= 	' AND '.$extra_where;
	else				$where	=	'';
	
	$res = eq("SELECT id FROM $db_table WHERE $sort_field = '$sort_order' $where;");
	if (mysql_num_rows($res)) {
		$myo = mfo($res);
		upRec($db_table, $myo->id, $sort_field, $sort_order - 1);
		fixSortOrder($db_table, $sort_field, $sort_order, $extra_where);
	}
}

//Fix sort order in general (fix gaps, fix all zeroes, etc)
function fixWholeSortOrder($db_table, $sort_field, $extra_where = '', $sort_method = 'ASC') {
	
	if ($extra_where) 	$where 	= 	'WHERE '.$extra_where;
	else				$where	=	'';	
	
	$sort_order = 0;
	
	$res = eq("SELECT id FROM $db_table $where ORDER BY $sort_field $sort_method;");
	while ($myo = mfo($res)) {
		$sort_order++;
		upRec($db_table,$myo->id,$sort_field,$sort_order);
	}
}

//Log a CMS activity
function cmsLog($activity_text, $repeat_filter = false, $login_id = 0) {
	
	global $cfg;
	
	//Set the ID
	$login_id = $login_id ? (int) $login_id : (int) $_SESSION['login_id'];
	
	//Check if we need to log, and if we have an id for a user
	if ($cfg['ACTIVITY_LOGGING'] and $login_id and $login_id > 1) {

		//Check if we need to filter repeating entries of the same event
		if ($repeat_filter) {
		 
		 	//Isset check
		 	if (!isset($_SESSION['last_event_log']))	$_SESSION['last_event_log'] = '';
		 
		 	//If the last event is the same as this one, dont log
			if ($_SESSION['last_event_log'] == md5($activity_text)) return false;
			
			//Save this event as last event in session
			$_SESSION['last_event_log'] = md5($activity_text);
		}

		//Get the time, and increase by one second if duplicate to what we last logged
		$time = time();
		if (isset($_SESSION['last_event_log_time']) and $_SESSION['last_event_log_time'] == $time) $time++;

		//Insert entry to database
		insRec(DIGI_DB_PREFIX.'activity_log',
			array('auth_user','timestamp','ip_address','activity_text'),
			array($login_id, mysql_time($time) ,$_SERVER['REMOTE_ADDR'],$activity_text));
			
		//Save the timestamp of this last event log
		$_SESSION['last_event_log_time'] 	= $time;
	}
}

//Salt a password for saving in database
function dbPass($password, $salt) {
	return sha1($password.$salt);
}

//Compare a password against a salted db password
function comparePass($db_pass, $given_pass, $salt) {
	return (sha1($given_pass.$salt) == $db_pass) ? true : false;
}

// *** Privilleges check function
function checkPriv($priv) {

	global $s_privs, $s_admin, $_file;
		
	$priv_id = (int) $priv;
	if ($priv_id == 0) {
		echo "*** UN-INITIATED PRIV $priv WAS CALLED FROM $_file. ***";
		exit;
	}

	if ($s_admin) 	return true;
	else			return isset($s_privs[$priv]);
}

//Return a link to the faq
function getFaqLink($topic_id) {
 
 	global $cfg;
 	
	if ($cfg['SHOW_FAQ_LINKS']) {	
		return "&nbsp; (<a href='cms_faq.php?topic=$topic_id' title='Help onderwerp bekijken'>?</a>)";	
	}
	else return '';
}
?>