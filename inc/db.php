<?php
/*
Title:		Database handling functions
File: 		inc/db.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Executes query and returns mysql result
function eq($query) {
	global $DB;
	$result = $DB->eq($query);
	return $result;
}

// *** Finds the number of rows
function mnr($res) {
	return mysql_num_rows($res);
}

// *** Returns mysql array from valid mysql result
function mfa($res) {

	$myr = @mysql_fetch_array($res);

	if (is_array($myr)) {
		return $myr;
	}

	return null;
}

// *** Returns mysql object from valid mysql result
function mfo($res) {

	$myo = @mysql_fetch_object($res);

	if (is_object($myo)) {
		return $myo;
	}

	return null;
}

// *** Make a value safe for database insertion
function dbSafe($value) {

	//Empty values
 	if ($value === '') {
		return "''";
	}
	//Integers
	else if (is_int($value)) {
		$value = (int) $value;
		return "'".$value."'";
	}
	//Floats
	else if (is_float($value)) {
		$value = (float) $value;
		return $value;
	}
	//Special string
	else if ($value == '[NOW()]') {
		return 'NOW()';
	}
	//Strings and other data
	else {
		$value = trim($value);
		$value = mysql_real_escape_string($value);
		return "'".$value."'";
	}
}

// *** Make a value safe for LIKE searching
function likeSafe($value) {
	return addcslashes(mysql_real_escape_string($value), "%_");
}

// *** Count results in database with optional WHERE clause
function countResults($table, $where = '', $group = '', $column = 'id', $join = '') {
	if ($where) $where = (substr($where, 0, 6) == 'WHERE ') 	? $where : 'WHERE '.$where;
	if ($group) $group = (substr($group, 0, 9) == 'GROUP BY ') 	? $group : 'GROUP BY '.$group;
	if ($join)	$join  = (substr($join, 0, 10) == 'LEFT JOIN ') 	? $join : 'LEFT JOIN '.$join;
	$res = eq("SELECT COUNT($column) FROM $table $join $where $group;");
	return (is_resource($res) and mysql_num_rows($res)) ?
		(mysql_num_rows($res) > 1 ? mysql_num_rows($res) : mysql_result($res,0)) : 0;
}


// *** Inserts an record and returns insert ID. Supports automatic retrieval of $_POST vars
function insRec($table, $fields, $values = '') {

	//See if $props is an array
	if (is_array($fields)) {
		foreach($fields as $key => $field) {
			if (is_array($values)) 	$value = $values[$key];
			else 					$value = $_POST[$field];
			$query[] = "$field = ".dbSafe($value);
		}
	}

	//If not, see if there is a single string value given
	elseif (is_string($fields)) {
		$query[] = "$fields = ".dbSafe($values);
	}

	//If we have query elements, build the SET query
	if(is_array($query)) $query = "SET ".implode(", ",$query);

	//Otherwise we will just do an insertRecord with empty values
	else {
		trigger_error('No query elements were found to insert. Will use default values.', E_USER_NOTICE);
		unset($query);
	}

	if(eq("INSERT INTO $table $query;")) return mysql_insert_id();
	else return false;
}

// *** Update record, either by ID or by a custom where clause
function upRec($table, $where, $fields, $values = '') {

	// *** Null
	if (!$where) {
		return false;
	}

	// *** Markter for all elements
	elseif ($where == '&all') {
		$where_query = '';
	}

	// *** If the parameter is an array, assume an array of id's
	else if (is_array($where)) {
		$where_query = array();
		foreach ($where as $id) {
			$id = (int) $id;
			if ($id != 0) {
				$where_query[]	=	"id = '$id'";
			}
		}
		$where_query = 'WHERE '.implode(" OR ", $where_query);
	}

	// *** If the where parameter is integer, assumed it is the id
	else if (is_numeric($where)) {
		$id = (int) $where;
		$where_query = "WHERE id = '$id'";
	}

	// *** Otherwise, we assume it is a valid where clause
	else {
		$where_query = 'WHERE '.$where;
	}

	// *** If we are getting an array of fields
	if (is_array($fields)) {
		foreach($fields as $key => $field) {
			if (is_array($values)) 	$value = $values[$key];
			else 					$value = $_POST[$field];
			$query[] = "$field = ".dbSafe($value);
		}
	}

	//If we are getting just one value
	elseif (is_string($fields)) {
		$query[] = "$fields = ".dbSafe($values);
	}

	//If we have elements to update, make the query
	if(is_array($query)) $query = implode(", ",$query);

	//Otherwise trigger a notice. No update will be performed, return 0.
	else {
		trigger_error("No query elements were found to update. No update will be performed.", E_USER_NOTICE);
		return 0;
	}

	if(eq("UPDATE $table SET $query $where_query;")) return mysql_affected_rows();
	else return false;
}

// *** Delete an record from the database, either by ID or by a custom where clause
function delRec($table, $where) {

	// *** Null
	if (!$where) {
		return false;
	}

	// *** Markter for all elements
	elseif ($where == '&all') {
		$where_query = '';
	}

	// *** If the parameter is an array, assume an array of id's
	else if (is_array($where)) {
		$where_query = array();
		foreach ($where as $id) {
			$id = (int) $id;
			if ($id != 0) {
				$where_query[]	=	"id = '$id'";
			}
		}
		$where_query = 'WHERE '.implode(" OR ", $where_query);
	}

	// *** If the where parameter is integer, assumed it is the id
	else if (is_numeric($where)) {
		$id = (int) $where;
		$where_query = "WHERE id = '$id'";
	}

	// *** Otherwise, we assume it is a valid where clause
	else {
		$where_query = 'WHERE '.$where;
	}

	if (eq("DELETE FROM $table $where_query;")) return mysql_affected_rows();
	else return false;
}

// *** Simple select queries simplification function
function getMyo($db_table, $where, $fields, $order = '', $order_type = '', $limit = '') {

	//If we are getting an array
	if (is_array($fields)) {
		$field = implode(',', $fields);
	}

	//If we are getting just one value
	elseif (is_string($fields)) {
		$field = $fields;
	}

	//If the where parameter is integer, assumed it is the id
	if (is_numeric($where)) $where = (int) $where;
	$where = (is_int($where)) ? "id = '$where'" : $where;
	if ($where) $where = "WHERE $where";

	//Order clause
	if ($order) $order = "ORDER BY $order $order_type";

	//Execute query
	$res = eq("SELECT $field FROM $db_table $where $order LIMIT 1;");
	$myo = mfo($res);

	//If we are getting an array
	if (is_array($fields)) {
		return $myo;
	}

	//If we are getting just one value
	elseif (is_string($fields) and isset($myo->$field)) {
		return $myo->$field;
	}

	//Return empty
	else {
		return '';
	}
}

// *** Returns an array of the given key/value combination
function makeArray($db_table, $key_field, $value_field, $where = '', $order = '', $order_type = '', $limit = '', $zero_value = false) {

	//Where, order and limit clauses
	if ($where) $where = "WHERE $where";
	if ($order) $order = "ORDER BY $order $order_type";
	if ($limit) $limit = "LIMIT $limit";

	$array = array();
	$res = eq("SELECT $key_field AS key_,$value_field AS value_ FROM $db_table $where $order $limit;");
	while ($myo = mfo($res)) {
		$array[$myo->key_] = $myo->value_;
	}
	return $zero_value ? (array(0 => '') + $array) : $array;
}

// *** Check for double posts
function isDoublePost($db_table, $fields, $values, $extra_where = '') {

	//See if $fields is an array
	if (is_array($fields)) {
		foreach($fields as $key => $field) {
			if (is_array($values)) 	$value = $values[$key];
			else 					$value = $_POST[$field];
			$query[] = "$field = ".dbSafe($value);
		}
	}

	//If not, see if there is a single string value given
	elseif (is_string($fields)) {
		if (!is_array($values)) $value = $values;
		else 					$value = $_POST[$fields];
		$query[] = "$fields = ".dbSafe($value);
	}

	//If we have elements to update, make the query
	if(is_array($query)) $query = implode(" AND ",$query);

	//Otherwise trigger a notice. No update will be performed, return 0.
	else {
		trigger_error("No query elements were found to check against.", E_USER_NOTICE);
		return 0;
	}

	$res = eq("SELECT id FROM $db_table WHERE $query $extra_where;");
	if (mysql_num_rows($res)) {
		$myo = mfo($res);
		return $myo->id;
	}
	return 0;
}

// *** Increment a field by one
function incrementField($db_table, $where, $field, $by = 1) {

	//By how much?
	$by = (int) $by;

	//If the where parameter is integer, assumed it is the id
	if (is_numeric($where)) $where = (int) $where;
	$where = (is_int($where)) ? "id = '$where'" : $where;
	if ($where) $where = "WHERE $where";

	eq("UPDATE $db_table SET $field = $field + $by $where;");
}
?>