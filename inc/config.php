<?php
/*
Title:		Global configuration file
File: 		inc/config.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** General settings
$charset		=	'utf-8';					//Strongly recommended to keep at utf-8
$chmod			=	0777;						//Chmodding regime for the site
$absolute_path	=	'';							//With trailing slash, leave empty if unsure. For example /var/www/customer/

// *** Front-end settings
$site_name		=	'Topdeal Hekwerk';					//The website name
$template		=	'default.html';				//Default website template

// *** CMS settings
$site_name_cms	=	'Topdeal Hekwerk';					//The customer specific CMS name
$cms_lang		=	'NL';						//The default CMS language
$cms_folder		=	'digicms';					//The CMS subfolder location
$private_folder	=	'';							//Private folder (absolute)
$template_cms	=	'digi_cms_20x.html';		//The template to use for the CMS
$template_login	=	'digi_cms_20x_login.html';	//The template to use for login
$cms_name		=	'Digitalization CMS';		//The CMS base name
$cms_title		=	'Digitalization CMS';		//The CMS browser title

// *** CMS menu layout definition
$menu = array(
	'MENU_LABEL_MANAGEMENT'	=>	array('MOD_SETTINGS','MOD_AUTH_USERS','MOD_BACKUP'),
	'MENU_LABEL_WEBSHOP'	=>	array('MOD_TOPDEAL_CUSTOMERS','MOD_OFFERTES','MOD_TOPDEAL_CATS','MOD_TOPDEAL_PRODS','MOD_DISCOUNT_CODES','MOD_TOPDEAL_DISCOUNTS','MOD_HOLIDAYS'),
	'MENU_LABEL_WEBSITE'	=>	array('MOD_PAGES','MOD_MENU','MOD_FILES','MOD_PAGE_CONTENTS'),
	'MENU_LABEL_MAILING'	=>	array('MOD_MAILER','MOD_NEWSLETTERS','MOD_TOPDEAL_SUBSCR'),
	'MENU_LABEL_TOOLS'		=>	array('MOD_BAD_WORDS','MOD_BLOCKED_IPS','MOD_AUTO_UPDATE','MOD_FAQ')
);

?>