<?php
/*
Title:		JPEG thumbnail stream output file
File: 		inc/makethumb.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

//Get extension
$parts = explode('.', $_GET['file']);
if (is_array($parts) and count($parts) > 1) {
	$file_extension = strtolower(end($parts));
}
else {
	$file_extension = '';
}

//Get file
if (isset($_GET['user_image'])) {
	$file = '../user_files/image/'.$_GET['file'];
}
else {
	$file = '../'.$_GET['file'];
}

//Check if file exists and if a valid image
if (file_exists($file) and in_array($file_extension,array('jpg','jpeg','gif','png'))) {
	
	//Get dimensions
	list($width, $height) = getimagesize($file);

	//Set constraints
	$max_width 	= 	(int) $_GET['maxwidth'];
	$max_height	=	(int) $_GET['maxheight'];

	if (!$max_height and !$max_width)	$max_height	=	200;
	if (!$max_width)	$max_width	=	$max_height;	
	if (!$max_height)	$max_height	=	$max_width;

	if ($width > $height) {
	    $new_width 	= $max_width;
	    $percent 	= $new_width/$width;
	    $new_height = $height * $percent;
	}
	elseif ($width < $height) {
	    $new_height = $max_height;
	    $percent 	= $new_height/$height;
	    $new_width 	= $width * $percent;
	}
	else {
		$new_height = $max_height;
		$new_width 	= $max_width;
	}
	
	//Set proper resampling function
	switch($file_extension) {
		case 'gif':
			$sampling_function	=	'imagecreatefromgif';
			break;
		case 'png':
			$sampling_function	=	'imagecreatefrompng';
			break;		
		case 'jpg':
		case 'jpeg':
		default:
			$sampling_function	=	'imagecreatefromjpeg';
			break;			
	}

	//Resample
	$image_p 	= imagecreatetruecolor($new_width, $new_height);
	$image 		= $sampling_function($file);
	imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
	
	//Output
	header('Content-type: image/jpeg');
	imagejpeg($image_p, null, 100);
}
?>