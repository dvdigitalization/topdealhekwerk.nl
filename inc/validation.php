<?php
/*
Title:		Input validation files
File: 		validation.php
Version: 	v2.03
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

/******************************************************************************************
 * Data stripping functions
 ***/

//Removes <br/> tags
function stripBRs($data) {
	return preg_replace('!<br.*>!iU', '', $data );
}

//Removes new lines and carriage returns
function stripNewlines($data, $replacement = '') {
  	return preg_replace("/(\r\n|\n|\r)/", $replacement, $data);
}

//Removes tabs
function stripTabs($data) {
  	return str_replace("\t", "", $data);
}

//Remove spaces
function stripSpaces($data) {
	return str_replace(' ','',$data);
}

//Removes <p></p> tags
function strip_p_tags($data) {
	$data = str_replace('<p>', '', $data );
	return str_replace('</p>', '', $data );
}

/******************************************************************************************
 * Data evaluation functions
 ***/

//Check if we need to do a stripslashes()
function evalStripSlashes(&$value, $return = false) {
	if (get_magic_quotes_gpc()) {
		if (is_array($value)) {
			foreach ($value as $key => $real_value) {
				//$value[$key]	=	stripslashes($real_value);
				$value[$key] = evalStripSlashes($real_value, true);
			}
		}
		else	$value = stripslashes($value);
	}
	if ($return) return $value;
}

//This function applies trim() to $value
function evalTrim(&$value, $return = false) {
	if (is_array($value)) {
		foreach ($value as &$real_value) {
			evalTrim($real_value);
		}
	}
	else {
		$value = trim($value);
	}
	if ($return) return $value;
}

//This function applies htmlspecialchars() to $value
function evalHTML(&$value, $return = false) {
	if (is_array($value)) {
		foreach ($value as &$real_value) {
			evalHTML($real_value);
		}
	}
	else	$value = htmlspecialchars($value, ENT_QUOTES);
	if ($return) return $value;
}

//This function applied utf8_decode() to $value
function evalUTF8(&$value, $return = false) {
	if (is_array($value)) {
		foreach ($value as &$real_value) {
			evalUTF8($real_value);
		}
	}
	else	$value = utf8_decode($value);
	if ($return) return $value;
}

function detectUTF8($string) {
    return preg_match('%(?:
    [\xC2-\xDF][\x80-\xBF]
    |\xE0[\xA0-\xBF][\x80-\xBF]
    |[\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}
    |\xED[\x80-\x9F][\x80-\xBF]
    |\xF0[\x90-\xBF][\x80-\xBF]{2}
    |[\xF1-\xF3][\x80-\xBF]{3}
    |\xF4[\x80-\x8F][\x80-\xBF]{2}
    )+%xs', $string);
}

//New version, should make all external data UTF-8 ok
function eval_utf8($string) {

	//If the string is UTF-8...
	if (detectUTF8($string.' ')) {

		//Presence of these characters means no decoding is needed
		if (strstr($string, '�') or
			strstr($string, '�') or
			strstr($string, '�') or
			strstr($string, '�') or
			strstr($string, '�') or
			strstr($string, '�') or
			strstr($string, '�') or
			strstr($string, '�') or
			strstr($string, '�') or
			strstr($string, '�') or
			strstr($string, '�') or
			strstr($string, '�') or
			strstr($string, '�') or
			strstr($string, '�') or
			strstr($string, '�')) {
			$return = $string;
		}

		//Decode the UTF-8
		else {
			$return	= utf8_decode($string);
		}
	}

	//Encode to UTF-8
	else {
		$return = utf8_encode($string);
	}

	//Return
	return $return;
}

//Apply all evaluative functions on $value (PARAM ORDER AND DEFAULTS CHANGED SINCE 2.00, pay attention!)
function evalAll(&$value, $eval_html = true, $eval_trim = true, $eval_stripslashes = true, $eval_utf8 = false,  $return = false) {
	if ($eval_stripslashes)	evalStripSlashes($value);
	if ($eval_trim)			evalTrim($value);
	if ($eval_utf8)			evalUTF8($value);
	if ($eval_html)			evalHTML($value);
	if ($return) 			return $value;
}

/******************************************************************************************
 * New data evaluation functions
 ***/

function decodeSpecialHTML($value) {
	return preg_replace_callback('!&amp;#((?:[0-9]+)|(?:x(?[0-9A-F]+)));?!i','decodeSpecialHTML_callback', $value);
}

function decodeSpecialHTML_callback($matches) {

	//Determines if the numeric entity is decimal or hexadecimal
	if (!is_int($matches[1]{0})) {
		$val	=	'0'.$matches[1] + 0;
	}
	else {
		$val	=	(int) $matches[1];
	}

	//Non-ASCII, convert to valid entity
	if ($val > 255) {
		return '&#'.$val.';';
	}

	//Simple characters, convert to literal form
	if (($val >= 65 and $val <= 90) or	//A-Z
		($val >= 97 and $val <= 122) or	//a-z
		($val >= 48 and $val <= 57)) {	//0-9

		return chr($val);
	}

	//Leave the rest as it is
	return $matches[0];
}

/******************************************************************************************
 * Data validation functions
 ***/

//Validates integers
function validateInt($int) {

	//Cast the value into an integer and return it
	return (int) $int;
}

//Validate array values on integer
function validateArrayInt($array, $remove_zeroes = true) {
	$ints = array();
	foreach ($array as $int) {
		$int = (int) $int;
		if ($int > 0 or $remove_zeroes == true) $ints[] = $int;
	}
	return $ints;
}

//Validates integers
function validateFloat($float) {

	//Replace comma's by dots, in case the user used those as decimal separator
	$float 	=	str_replace(',','.',$float);

	//Cast it into a float, and return it
	return (float) $float;
}

//Validate number
function validateNumber($number, $lower_bound = false, $upper_bound = false, $integer = false) {

	//Cast into integer or float
	if ($integer) {
		$number = 	(int) $number;
	}
	else {
		$number =	str_replace(',','.',$number);
		$number = 	(float) $number;
	}

	//Check lower bound
	if ($lower_bound !== false and $number < $lower_bound) {
		$number = $lower_bound;
	}

	//Check upper bound
	if ($upper_bound !== false and $number > $upper_bound) {
		$number = $upper_bound;
	}

	//Return
	return $number;
}

//Validate a simple string, for example passwords, usernames
function validateSimpleString($string, $min_length = false, $max_length = false) {

	//Form the regular expression
	$regexp = "\"^[a-zA-Z0-9\ _-]*$\"";

	//Validate against input
	if (preg_match($regexp, $string)) {
		if (($min_length !== false and strlen($string) < $min_length) or
			($max_length !== false and strlen($string) > $max_length)) {
			return '';
		}
		return $string;
	}
	return '';
}

//Validate a string
function validateString($string, $min_length = false, $max_length = false) {

	//Form the regular expression
	$regexp = '"^[a-zA-Z0-9\ \\\/|\$\^\+`~#={}%&@!\?\(\)\[\]*_.,<>;\":/\'-]*$"';

	//Validate against input
	if (preg_match($regexp, $string)) {
		if (($min_length !== false and strlen($string) < $min_length) or
			($max_length !== false and strlen($string) > $max_length)) {
			return '';
		}
		return $string;
	}
	return '';
}

//Validate a date
function validateDate($date, $european = true) {

	$date = str_replace('-','/',$date);

	if ($european) {

		//Form the regular expression
		$regexp1 = "\"^[0-9][0-9]/[0-9][0-9]/[1-2][0-9][0-9][0-9]$\"";

		if (!(preg_match($regexp1, $date))) {
			return '01/01/1970';
		}
		else {
			return $date;
		}
	}
	else return date('d/m/Y', strtotime($date));
}

//Validate email
function validateEmail($email, $strict = false, $utf8 = false) {

	//Trim data
	$email = trim($email);

	//Utf-8 data
	if ($utf8) 	$utf8 = 'u';
	else		$utf8 = '';

	//Regex
	$regex = $strict	?
      	'/^([.0-9a-z_-]+)@(([0-9a-z-]+\.)+[0-9a-z]{2,4})$/i'.$utf8 :
       	'/^([*+!.&#$�\'\\%\/0-9a-z^_`{}=?~:-]+)@(([0-9a-z-]+\.)+[0-9a-z]{2,4})$/i'.$utf8;

	//Validate
	if(!empty($email) and preg_match($regex, $email)) {
		return $email;
	}
	else {
		return '';
	}
}

//Validate an url
function validateURL($url) {

	//Trim data
	$url = trim($url);

	if (!empty($url)) {

		//Parse the url
		if ($url_data = parse_url($url) and $url_data['scheme'] and $url_data['hostname']) {
			$url = $url_data['scheme'].'://'.$url_data['hostname'].$url_data['path'];
			return $url;
		}
	}
	return false;
}

//Validate IP address
function validateIP($ip) {
	$ip = long2ip(ip2long($ip));
	return ($ip == '0.0.0.0') ? '' : $ip;
}

//Validate IP address with wildcard
function validateIPwildcard($ip) {

	$regex = "/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(\*|(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(\*|(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(\*|(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))))$/";

	//Validate
	if(!empty($ip) and preg_match($regex, $ip)) {
		return $ip;
	}
	else {
		return '';
	}
}

//Validate a file as an image
function validateImage($url, $delete_invalid = false) {

	if (file_exists($url) and is_readable($url)) {
		if (getimagesize($url)) {
			return true;
		}
		else {
			if ($delete_invalid) unlink($url);
		}
	}
	return false;
}

//Validate filesize of an uploaded file
function validateFileSize($url_after_upload, $file_field) {
	if (!file_exists($url_after_upload) or filesize($url_after_upload) != $_FILES[$file_field]['size']) {
		return false;
	}
	else {
		return true;
	}
}

/******************************************************************************************
 * Data length validation
 ***/

//Check if an input string doesn't exceed a maximum length
function validateLength($string, $allowed_length) {
	if (!empty($data) and isset($string[$allowed_length+1])) {
		return false;
	}
	else {
		return true;
	}
}

/******************************************************************************************
 * Data restriction
 ***/

//Check if data falls into a predefined set of allowed values
function validateAllowedValue($value, $allowed_values) {
	if (empty($value) or !in_array($value, $allowed_values)) {
		return false;
	}
	else {
		return true;
	}
}

/******************************************************************************************
 * Serialized data encoding / decoding / validation
 ***/

//Serialize data with a hash for validation
function serializeHashed($data, $hash_appendix = '') {

	//Serialize the data
	$serialized_data 	= 	serialize($data);

	//Create a hash key based on the serialized data and a secret key
	$hash				=	hash('sha1', $serialized_data.$hash_appendix);

	//Return the serialized data with hash key prepended
	return $hash.$serialized_data;
}

//Check if serialized data matches validates with the correct hash
function unserializeHashed($serialized_data, $hash_appendix = '') {

	//Split the key and data
	$hash				=	substr($serialized_data, 0, 40);
	$serialized_data	=	substr($serialized_data, 40);

	//Validate the key and return unserialized data if valid
	if ($hash != hash('sha1', $serialized_data.$hash_appendix)) {
		return false;
	}
	else {
		return unserialize($serialized_data);
	}
}

/******************************************************************************************
 * Data obtaining
 ***/

//Get a users IP address
function getUserIP() {

	if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		$ips = explode(', ', $_SERVER['HTTP_X_FORWARDED_FOR']);
		$ip = validateIP(array_pop($ips));
	}
	else {
		$ip = $_SERVER['REMOTE_ADDR'];
	}

	return $ip;
}

//Get either post or get values, avoiding the need for $_REQUEST
function request($key, $value_if_blank = '') {
	if (isset($_POST[$key]))	return	$_POST[$key];
	if (isset($_GET[$key]))		return	$_GET[$key];
	return $value_if_blank;
}
?>