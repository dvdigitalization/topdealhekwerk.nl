<?php
/*
Title:		Global functions
File: 		inc/globals.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

/******************************************************************************************
 * Functions check
 ***/

//Check if stripos() exists, if not make it. Support for PHP < 5
if (!function_exists('stripos')) {
	function stripos($str, $needle, $offset = 0) {
		return strpos(strtolower($str), strtolower($needle), $offset);
	}
}

//Check if str_ireplace() exists, if not make it. Support for PHP < 5
if (!function_exists('str_ireplace')) {
	function str_ireplace($search, $replace, $subject) {
		$search = '/'.preg_quote($search, '/').'/i';
		return preg_replace($search, $replace, $subject);
	}
}

//Check if hash function exists
if(!function_exists('hash')) {
	function hash($algo, $data, $raw_output = 0) {
		if($algo == 'md5') 	return(md5($data,  $raw_output));
		if($algo == 'sha1') return(sha1($data, $raw_output));
	}
}

/******************************************************************************************
 * Added functionality
 ***/

//Map an objects vars onto another object
function map_object(&$main_object, $mapping_object) {

	if (!is_object($main_object) or !is_object($mapping_object)) {
		return;
	}
	foreach ($mapping_object as $key => $value) {
		$main_object->$key = $value;
	}
}

//Swap two variables
function swap(&$a, &$b) {
	$c = $a;
	$a = $b;
	$b = $c;
}

//A special version of implode, to format the pieces in a specific way
function implode_special($glue, $pieces, $format) {
	$pieces_special	= array();
	foreach ($pieces as $key => $value) {
		$pieces_special[] = str_replace('[KEY]',$key, str_replace('[VALUE]',$value, $format));
	}
	return implode($glue, $pieces_special);
}

//Swap the values of an array, if key is present in a replacement values array
function swap_array_values($array_source, $array_replacements) {
	foreach ($array_source as $key => $value) {
		if (isset($array_replacements[$key])) {
			$array_source[$key] = $array_replacements[$key];
		}
	}
	return $array_source;
}

//An upgraded version of array_fill
function array_fill_manual($start, $end, $increment = 1) {

	//Infinite loop protection
	if (($start < $end and $increment < 0) or ($start > $end and $increment > 0) or $increment == 0) {
		trigger_error('Invalid input received for array_fill_manual()');
		return array();
	}

	$array = array();
	for ($a = $start; $a <= $end; $a += $increment) {
		$array[$a] = $a;
	}
	return $array;
}

//A simple function to add a blank element in the beginning of an array
function array_add_blank($array) {
	return array(0 => '') + $array;
}

//A simple function to retrieve the first key of an array
function array_first_key($array) {
	$tmp = array_keys($array);
	return $tmp[0];
}

//Print_r function with pre-tags
function print_pre_r($contents, $return = false) {
	$contents = '<pre>'.print_r($contents, true).'</pre>';
	if ($return) {
		return $contents;
	}
	else {
		echo $contents;
	}
}

/******************************************************************************************
 * HTML/JS manipulation functions
 ***/

//This function reverses html entities
function reverseHtmlentities($mixed) {
  	$htmltable = get_html_translation_table(HTML_ENTITIES);
   	foreach($htmltable as $key => $value) {
       	$mixed = str_replace(addslashes($value),$key,$mixed);
   	}
   	return str_replace("&#039;","'",$mixed);
}

//Fixes (HTML) input for use in JS functions, adding slashes and then replacing linebreaks by '+\n'
function fixForJS($html, $extra_fix = false) {

	if ($extra_fix) {
		$html = str_replace("'", '"', $html);
		$html = str_replace("&quot;", '"', $html);
	}
	$html = addslashes($html);
	$html = preg_replace("/(\r\n|\n|\r)/", " ", $html);
	return $html;
}

//Generates code for outputting an image or flash file, based on url (extension)
function image_or_flash($url, $extra = '', $flash_width = '', $flash_height = '', $flash_loop = 'true', $flash_vars = '') {

	//Check file validity
	if (!file_exists($url)) {
		return '';
	}

	//Try to determine size
	if (!$image_size = getimagesize($url)) {
		list ($width, $height) = $image_size;
	}
	else {
		$width = $height = '';
	}

	//Determine extension
	$ext = getFileExtension($url);

	//Image
	if (in_array($ext, array('jpg','jpeg','png','gif'))) {
		return "<img src='$url' border='0px' ".($width ? "width='{$width}px'":'')." ".($height ? "height='{$height}px'":'')." alt='' $extra/>";
	}

	//Flash
	else {
		return "
			<embed
			  src='$url'
			  width='$flash_width'
			  height='$flash_height'
			  allowscriptaccess='always'
			  allowfullscreen='true'
			  menu='true'
			  play='true'
 			  loop='$flash_loop'
			  flashvars='$flash_vars'
			  pluginspage='http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash'
			  type='application/x-shockwave-flash'
			  $extra
			/>
		";
	}
}

//Remove <p> tags, handy sometimes for FCK texts
function remove_p_tags($text) {
	$text = str_replace('<p>', '', $text);
	$text = str_replace('</p>', '', $text);
	return $text;
}

/******************************************************************************************
 * String generation/manipulation functions
 ***/

//Limit a string to a certain amount of chars
function limitString($string, $max_length, $replace='...') {
	if (strlen($string) > $max_length) {
		$string = substr($string, 0, $max_length).$replace;
	}
	return $string;
}

//Clean a string
function cleanString($string) {
	$clean_string = '';
    for ($i=0; $i<strlen($string); $i++) {
        if (preg_match('([0-9a-z])', $string[$i])) {
            $clean_string .= $string[$i];
        }
    }
    return $clean_string;
}

//Makes a random password with $numchar amount of characters
function randomPass($numchar) {
  	$string = str_shuffle('abcdefghjklmnpqrstuvwxyz23456789');
   	return substr($string,0,$numchar);
}

//Makes a random number with $numchar amount of numbers
function randomNumber($numchar) {
  	$string = str_shuffle('1234567890123456789012345678901234567890');
   	return substr($string,0,$numchar);
}

//Makes a random string with $numchar amount of letters
function randomString($numchar, $uppercase = false) {
  	$string = str_shuffle('abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz');
  	$string = substr($string,0,$numchar);
   	return $uppercase ? strtoupper($string) : $string;
}

/******************************************************************************************
 * Filesystem / file functions
 ***/

//Make a string filename safe
function filenameSafe($filename, $no_space = true, $to_lower_case = true) {

	$temp = basename($filename);

    //Lower case
    if ($to_lower_case) {
    	$temp = strtolower($temp);
	}

    //Replace spaces with an underscore
    if ($no_space) {
		$temp = str_replace(' ', '_', $temp);
	}

	//Fix & sign
	$temp = str_replace('&', 'en', $temp);

    //Loop through string and remove illegal characters
    $result = '';
    for ($i=0; $i<strlen($temp); $i++) {
        if (preg_match('([0-9a-zA-Z_\.-])', $temp[$i])) {
            $result = $result . $temp[$i];
        }
    }

	//Remove trailing underscores
	while (substr($result,-1) == '_') {
		$result = substr($result,0,strlen($result)-1);
	}

    //Return filename
    return $result;
}

//Make a string redirect engine URL safe
function rewriteSafe($string) {
	if (defined('CMS_PATH_PREFIX')) {
		require_once(CMS_PATH_PREFIX.'inc/functions/normalize_chars.php');
	}
	else {
		require_once('inc/functions/normalize_chars.php');
	}
	$string = normalize_chars($string);
	$string = str_replace('.', '',fileNameSafe(str_replace(' ','-',reverseHTMLentities($string))));
	$string = preg_replace('/\-+/','-',$string);

	return $string;
}

//Get the extension of a filename
function getFileExtension($filename, $with_dot = false) {

	$parts = explode('.', $filename);
	if (is_array($parts) and count($parts) > 1) {
		return ($with_dot ? '.' : '').strtolower(end($parts));
	}
    else return '';
}

//Standard file upload processing
function uploadFile($field_name, $destination_folder, $file_name = '', $chmod = null, $force_image = false, $overwrite = true,
					$allowed_extensions = '', $no_space = true, $to_lower_case = true, $dont_append_extension = false) {

	//Global vars
	global $_sep, $_error, $_lang;

	//Define error definitions for front-end
	if (defined('SITE_PATH_PREFIX') and !defined('DIGI_ERR_UPLOAD_SIZE')) {
		if ($_lang == 'NL') {
			define('DIGI_ERR_UPLOAD_SIZE',				'Het bestand is te groot. Dit mag maximaal [SIZE] zijn.');
			define('DIGI_ERR_UPLOAD_NO_FILE',			'Het bestand is ongeldig.');
			define('DIGI_ERR_UPLOAD_NO_TMP',			'Er kan geen tijdelijke map gevonden worden om het bestand naar te uploaden.');
			define('DIGI_ERR_UPLOAD_PARTIAL',			'Het bestand is niet helemaal geupload.');
			define('DIGI_ERR_UPLOAD_CANTWRITE',			'Het bestand kon niet naar de tijdelijke map gekopieerd worden.');
			define('DIGI_ERR_UPLOAD_NO_IMG',			'Het bestand moet een afbeelding zijn.');
			define('DIGI_ERR_UPLOAD_FILENAME',			'Er is geen geldige bestandsnaam gevonden.');
			define('DIGI_ERR_UPLOAD_EXISTS',			'Er bestaat al een bestand met deze naam.');
			define('DIGI_ERR_UPLOAD_DEST_DIR',			'De doel map [DIR] bestaat niet of is niet bruikbaar.');
			define('DIGI_ERR_UPLOAD_MOVING',			'Het bestand kon niet naar de doel map [DIR] gekopieerd worden.');
			define('DIGI_ERR_UPLOAD_EXT',				'Het bestand moet een van de volgende types zijn: [EXTENSIONS]');
			define('DIGI_ERR_UPLOAD_FIELD_NAME',		'Het veld [FIELD] is geen geldig bestands type veld.');
			define('DIGI_ERR_UPLOAD_HACK',				'Er wordt mogelijk met het CMS geknoeid.');
		}
		else {
			define('DIGI_ERR_UPLOAD_SIZE',				'The file is too large. Maximum file size is [SIZE].');
			define('DIGI_ERR_UPLOAD_NO_FILE',			'The file is invalid.');
			define('DIGI_ERR_UPLOAD_NO_TMP',			'There is no temporary folder to upload files to.');
			define('DIGI_ERR_UPLOAD_PARTIAL',			'The file was not uploaded completely.');
			define('DIGI_ERR_UPLOAD_CANTWRITE',			'The file could not be copied to the temporary folder.');
			define('DIGI_ERR_UPLOAD_NO_IMG',			'The file has to be an image.');
			define('DIGI_ERR_UPLOAD_FILENAME',			'No valid filename could be found.');
			define('DIGI_ERR_UPLOAD_EXISTS',			'A file with this filename already exists.');
			define('DIGI_ERR_UPLOAD_DEST_DIR',			'The target folder [DIR] does not exist or is not usable.');
			define('DIGI_ERR_UPLOAD_MOVING',			'The file could not be copied to the target folder [DIR].');
			define('DIGI_ERR_UPLOAD_EXT',				'The file must be one of the following types: [EXTENSIONS]');
			define('DIGI_ERR_UPLOAD_FIELD_NAME',		'The field [FIELD] is not a valid file type field.');
			define('DIGI_ERR_UPLOAD_HACK',				'There is possibly being tampered with the CMS.');
		}
	}

	//Check if the field is ok
	if (!isset($_FILES[$field_name])) {
		$_error = str_replace('[FIELD]', $field_name, DIGI_ERR_UPLOAD_FIELD_NAME);
		return false;
	}

	//Error check
	if (($error = $_FILES[$field_name]['error']) != 0) {

		if ($error == UPLOAD_ERR_INI_SIZE) {
			$max_upload_bytes		=	returnBytes(ini_get('upload_max_filesize'));
			$max_upload_size		=	byte2kb($max_upload_bytes);
			$_error = str_replace('[SIZE]', $max_upload_size, DIGI_ERR_UPLOAD_SIZE);
		}
		elseif ($error == UPLOAD_ERR_FORM_SIZE) {
			$max_upload_bytes		=	$_POST['MAX_FILE_SIZE'];
			$max_upload_size		=	byte2kb($max_upload_bytes);
			$_error = str_replace('[SIZE]', $max_upload_size, DIGI_ERR_UPLOAD_SIZE);
		}
		elseif ($error == UPLOAD_ERR_NO_FILE) {
			$_error = DIGI_ERR_UPLOAD_NO_FILE;
		}
		elseif ($error == UPLOAD_ERR_PARTIAL) {
			$_error = DIGI_ERR_UPLOAD_PARTIAL;
		}
		elseif ($error == UPLOAD_ERR_CANT_WRITE) {
			$_error = str_replace('[DIR]', $destination_folder, DIGI_ERR_UPLOAD_CANTWRITE);
		}
		elseif ($error == UPLOAD_ERR_NO_TMP_DIR) {
			$_error = DIGI_ERR_UPLOAD_NO_TMP;
		}

		if (file_exists($_FILES[$field_name]['tmp_name'])) {
			unlink($_FILES[$field_name]['tmp_name']);
		}
		return false;
	}

	//Check if tmp file exists
	if (!is_file($_FILES[$field_name]['tmp_name'])) {
		$_error = DIGI_ERR_UPLOAD_NO_FILE;
		return false;
	}

	//Check validity of file
	if (!is_uploaded_file($_FILES[$field_name]['tmp_name'])) {
		$_error = DIGI_ERR_UPLOAD_HACK;
		return false;
	}

	//Size mismatch?
	if (($file_size = filesize($_FILES[$field_name]['tmp_name'])) != $_FILES[$field_name]['size']) {
		$_error = DIGI_ERR_UPLOAD_PARTIAL;
		unlink($_FILES[$field_name]['tmp_name']);
		return false;
	}

	//Get the filename extension (of the uploaded file)
	$file_extension = getFileExtension($_FILES[$field_name]['name']);

	//Extension check
	if (!empty($allowed_extensions)) {

		if (is_array($allowed_extensions) and !in_array($file_extension, $allowed_extensions)) {
			$_error = str_replace('[EXTENSIONS]', implode(', ', $allowed_extensions), DIGI_ERR_UPLOAD_EXT);
			unlink($_FILES[$field_name]['tmp_name']);
			return false;
		}
		else if	(is_string($allowed_extensions) and $file_extension != $allowed_extensions) {
			$_error = str_replace('[EXTENSIONS]', $allowed_extensions, DIGI_ERR_UPLOAD_EXT);
			unlink($_FILES[$field_name]['tmp_name']);
			return false;
		}
	}

	//Forced image?
	if ($force_image and getimagesize($_FILES[$field_name]['tmp_name']) === false) {
		$_error = DIGI_ERR_UPLOAD_NO_IMG;
		unlink($_FILES[$field_name]['tmp_name']);
		return false;
	}

	//Filename check
	if (empty($file_name)) {
		if (empty($_FILES[$field_name]['name'])) {
			$_error = DIGI_ERR_UPLOAD_FILENAME;
			unlink($_FILES[$field_name]['tmp_name']);
			return false;
		}
		else $file_name = $_FILES[$field_name]['name'];
	}
	else {
		if (!$dont_append_extension) {
			$file_name .= '.'.$file_extension;
		}
	}

	//Check destination folder
	if (!is_dir($destination_folder)) {
		$_error = str_replace('[DIR]', $destination_folder, DIGI_ERR_UPLOAD_DEST_DIR);
		unlink($_FILES[$field_name]['tmp_name']);
		return false;
	}

	//Set filename and file URL
	$file_name 	= 	filenameSafe($file_name, $no_space, $to_lower_case);
	$file_url 	= 	$destination_folder.$_sep.$file_name;

	//Overwrite if exists?
	if (file_exists($file_url) and !$overwrite) {
		$_error = DIGI_ERR_UPLOAD_EXISTS;
		unlink($_FILES[$field_name]['tmp_name']);
		return false;
	}

	if (@move_uploaded_file($_FILES[$field_name]['tmp_name'], $file_url)) {
		if ($chmod) chmod($file_url, $chmod);
		return array($file_name, '.'.$file_extension, $file_size);
	}
	else {
		$_error = str_replace('[DIR]', $destination_folder, DIGI_ERR_UPLOAD_MOVING);
		unlink($_FILES[$field_name]['tmp_name']);
		return false;
	}
}

//Read directory and return files
function readDirectory($dir, $recursive = true, $include_directories = false, $use_filenames_as_index = false) {

	global $_sep;

	//Not a directory?
	if (!is_dir($dir)) {
		return array();
	}

	//Open handler and initialize array
	if (!$fh = @opendir($dir)) {
		return array();
	}
	$files 	= array();

	//Read the directory
	while (false !== ($file = readdir($fh))) {

		//Skip the . and .. files
		if ($file == '.' or $file == '..' or $file == '.svn') {
			continue;
		}

		//File
		if (is_file($dir.$_sep.$file) or (is_dir($dir.$_sep.$file) and $include_directories)) {
			if ($use_filenames_as_index) {
				$files[$file] = $file;
			}
			else {
				$files[] = $file;
			}
		}

		//Directory recursivity
		if ($recursive and is_dir($dir.$_sep.$file)) {

			//Read files from this subdirectory, and make temporary array
			$files_in_subdir 	= 	readDirectory($dir.$_sep.$file, true, $include_directories, $use_filenames_as_index);
			$files_to_merge		=	array();

			//Loop over all read files
			foreach ($files_in_subdir as $file_in_subdir) {

				//Add to temp array
				if ($use_filenames_as_index) {
					$key = $file_in_subdir 	= 	$file.$_sep.$file_in_subdir;
					$files_to_merge[$key] 	= 	$file_in_subdir;
				}
				else {
					$files_to_merge[]		= 	$file.$_sep.$file_in_subdir;
				}
			}

			//Merge temp array with files array
			$files = array_merge($files, $files_to_merge);
		}
	}

	//Close the handler, sort the files while maintaing index
	closedir($fh);
	asort($files);
	return $files;
}

//Delete all contents from a directory (and if desired remove the directory as well)
function rmdir_files($dir, $clean_only = false) {

	global $_sep;

	if(is_dir($dir) and !is_link($dir)) {

		$dir 	= (substr($dir, -1) != $_sep) ? $dir.$_sep : $dir;
		$dir_h 	= opendir($dir);

		while($file = readdir($dir_h)) {
			if($file != '.' and $file != '..' and  $file != '.svn') {
				if(!is_dir($dir.$file)) {
					@unlink($dir.$file);
				}
				else {
					rmdir_files($dir.$file);
				}
			}
		}

		closedir($dir_h);
		if (!$clean_only) {
			@rmdir($dir);
		}
	}
}

//Copy a folder recursively
function copy_files($source, $target) {

	if (is_dir($source)) {

		if (!is_dir($target)) {
			mkdir($target);
	   	}

	    $dh = dir($source);

	    while (false !== ($element = $dh->read())) {
	        if ($element == '.' or $element == '..' or $element == '.svn') {
	            continue;
	        }

	        $to_copy = $source.'/'.$element;
	        if (is_dir($to_copy)) {
	            copy_files($to_copy, $target.'/'.$element);
	            continue;
	        }
	        copy($to_copy, $target.'/'.$element );
	    }

	    $dh->close();
	}
	else {
	    copy($source, $target);
	}
}

//Folder existence check
function checkFolder($dir, $supress_message = false) {

	global $_sep, $chmod, $Message;

	if (!is_dir($dir)) {
		if (@mkdir($dir)) {
			if ($chmod) chmod($dir, $chmod);
			if (!$supress_message) {
				if (defined('DIGI_ERR_FOLDER_NONEXIST_CREATED')) {
					$err = DIGI_ERR_FOLDER_NONEXIST_CREATED;
				}
				else {
					$err = 'Map bestond niet maar is aangemaakt!';
				}
				if (defined('CMS_PATH_PREFIX')) {
					$Message->set($err.'<br/><br/>'.getcwd().$_sep.$dir, WARNING);
				}
			}
			return true;
		}
		else {
			if (!$supress_message) {
				if (defined('DIGI_ERR_FOLDER_CREATE_FAILED')) {
					$err = DIGI_ERR_FOLDER_CREATE_FAILED;
				}
				else {
					$err = 'Map bestond niet maar is aangemaakt!';
				}
				if (defined('CMS_PATH_PREFIX')) {
					$Message->set($err.'<br/><br/>'.getcwd().$_sep.$dir, CRITICAL);
				}
			}
			return false;
		}
	}
	return true;
}

//Returns the filename only, excluding subdirs. Defaults to the SCRIPT_NAME location
function filename($path = '') {
 	if (!$path) {
		$path = htmlspecialchars($_SERVER['SCRIPT_NAME']);
	}
	return basename($path);
}

//Returns the pathname only
function pathname() {
	$path = substr($_SERVER['SCRIPT_NAME'],0,strlen($_SERVER['SCRIPT_NAME']) - strlen(filename()));
	return $path;
}

//Returns the server and pathname
function serverAndPath() {
	$path = substr($_SERVER['SCRIPT_NAME'],0,strlen($_SERVER['SCRIPT_NAME']) - strlen(filename()));
	return $_SERVER['SERVER_NAME'].$path;
}

//Returns gigabyte, megabyte or kilobyte value from byte value
function byte2kb($bytes) {

	$bytes = (int) $bytes;

	if ($bytes >= 1073741824)	return round($bytes/1073741824,2).' Gb';
 	else if ($bytes >= 1048576)	return round($bytes/1048576,2).' Mb';
 	else if ($bytes >= 1024)  	return round($bytes/1024,0).' kb';
 	else						return $bytes.' b';
}

//Returns bytes from a string filesize value
function returnBytes($val) {

	$val = trim($val);
    $last = strtolower($val{strlen($val)-1});
    switch($last) {
        case 'g':
            $val *= 1024;
        case 'm':
            $val *= 1024;
        case 'k':
            $val *= 1024;
    }
    return $val;
}

//Get the disk usage for a folder
function diskUsage($d, $depth = NULL, $byte2kb = false, $min_threshold = 0) {

	if(is_file($d))	return filesize($d);

	if($d[strlen($d)-1] != "\\" or $d[strlen($d)-1] != '/')
	$d .= '/';

	$dh = @opendir($d);
	if(!$dh)
	return 0;

	$usage = 0;
	while($e = readdir($dh)) {
		if($e != '.' && $e != '..' && $e != '.svn') {
			$usage += diskUsage($d.$e, isset($depth) ? $depth - 1 : NULL);
		}
	}
	closedir($dh);

	if ($min_threshold > 0 and $usage < $min_threshold) $usage = 0;
	return $byte2kb ? byte2kb($usage) : $usage;
}

//Write contents to a file
function write_to_file($file_url, $contents, $append = false) {

	//Set the mode to append or write
	$mode = $append ? 'a' : 'w';

	//Try to open file
	if (!$fh = @fopen($file_url,$mode)) {
		return false;
	}

	//Try to write to file
	if (@fwrite($fh, $contents) === false) {
		@fclose($fh);
		return false;
	}

	@fclose($fh);
	return true;
}

/******************************************************************************************
 * Mark-up functions
 ***/

//Return a nice formatted ID
function niceId($id, $leading_zeroes = 0, $prefix = '', $suffix = '') {
	if ($leading_zeroes > 0) {
		return sprintf($prefix.'%0'.$leading_zeroes.'d'.$suffix, $id);
	}
	else {
		return $prefix.$id.$suffix;
	}
}

//Parse integer monetary amounts into readable format
function parseAmount($amount, $sign = 0) {

	//Input should be integer, take into account limit of integer
	if ($amount < 2147483647) {
		$amount = (int) $amount;
	}
	else {
		$amount = 1 * $amount / 1;
	}

	//Divide and round
	$amount = round($amount/100,2);

	//Fix accuracy bug
	if ($amount == '-0.00') $amount = 0;

	//Format amount
	$amount = number_format($amount, 2, ',', '.');

	//Return
	return ($sign) ? (is_string($sign) ? $sign.$amount : '&euro;'.$amount) : $amount;
}

//Convert input amounts into integer database amounts
function dbAmount($amount) {
	if (strstr($amount,'.') and strstr($amount,',')) {
		$amount = str_replace('.','',$amount);
	}
	return round(100*validateFloat($amount));
}

//Ceil a database amount properly
function ceilAmount($amount) {
	return 100 * ceil($amount/100);
}

//Floor a database amount properly
function floorAmount($amount) {
	return 100 * floor($amount/100);
}

//Round a database amount properly
function roundAmount($amount) {
	return 100 * round($amount/100);
}

/******************************************************************************************
 * Time and date functions
 ***/

//Check if a timestamp is today
function isToday($timestamp) {
	if (	date('d')	==	date('d', $timestamp)	and
			date('m')	==	date('m', $timestamp)	and
			date('Y')	==	date('Y', $timestamp)
		) return true;
	return false;
}

//Return an elaborate timestamp based on the current language
function time2dateLong($timestamp) {

	global $MONTH_ARRAY;

	//Check if we have a mysql timestamp
 	$parts = explode(' ', $timestamp);
 	if (count($parts) > 1) {
		$parts 	= 	explode('-', $parts[0]);
		$day	=	$parts[2] + 0;
		$month	=	$parts[1] + 0;
		$year	=	$parts[0];
	}

	//Otherwise the input should be an integer
 	else {
	 	$timestamp 	= 	(int) $timestamp;
		$day		=	date('j', $timestamp);
		$month		=	date('n', $timestamp);
		$year		=	date('Y', $timestamp);
	}

 	//Return formatted date
	return $day.' '.$MONTH_ARRAY[$month].' '.$year;
}

//Convert a timestamp into a readable date format
function time2date($timestamp) {

 	//Check if we have a mysql timestamp
 	$parts = explode(' ', $timestamp);
 	if (count($parts) > 1) {
		$parts = explode('-', $parts[0]);
		return $parts[2].'/'.$parts[1].'/'.$parts[0];
	}

 	//Otherwise the input should be an integer
 	$timestamp = (int) $timestamp;

 	//Return formatted date
	return date('d/m/Y', $timestamp);
}

//Convert a given date to a timestamp, supports both european and US style input
function date2time($date, $european = true) {
	$date = validateDate($date, $european);
	$date = explode('/', $date);
	$date = $date[1].'/'.$date[0].'/'.$date[2];
	return strtotime($date);
}

//Convert unix timestamp to mysql timestamp
function mysql_time($timestamp = '') {
	if ($timestamp === '') $timestamp = time();
	return date('Y-m-d H:i:s', $timestamp);
}

//Convert mysql timestamp to unix timestamp
function unix_time($timestamp = '') {
	$parts = explode(' ', $timestamp);
 	if (count($parts) > 1) {
		$parts_date = explode('-', $parts[0]);
		$parts_time = explode(':', $parts[1]);
		return mktime($parts_time[0],$parts_time[1],$parts_time[2],$parts_date[1],$parts_date[2],$parts_date[0]);
	}
	return time();
}

/******************************************************************************************
 * Form element generation functions
 ***/

//Check a checkbox if needed
function check($field, $array = false) {
	if ($array and isset($_POST[$field]) and is_array($_POST[$field])) {
		return $_POST[$field];
	}
	elseif ($array and isset($_GET[$field]) and is_array($_GET[$field])) {
		return $_GET[$field];
	}
	else if (!$array and (isset($_POST[$field]) or isset($_GET[$field]))) {
		return "checked='checked'";
	}
	return $array ? array() : '';
}

//Make a selectbox
function makeSelectbox($name, $array, $selected = 0, $width = 0, $extra = '', $function = '', $extra_style = '') {

	//Check if $array is an array
	if (!is_array($array)) {
		trigger_error("While trying to make a SELECT box ($name), no valid array was received for the options.",E_USER_WARNING);
		return null;
	}

	//Width needs to be an integer
	$width = (int) $width;

	//Set the width
	if ($width != 0)	$style = "style='width: {$width}px; $extra_style'";
	else				$style = '';

	if (!$style and $extra_style) $style = "style='$extra_style'";

	//Beginning html
  	$html = "\n<select name='$name' id='$name' $style $extra>";

	//Loop through the array with options
	foreach ($array as $option_value => $option_name) {

		//The is_numeric() check is required because when a string is compared to an integer, the string will become 0...
		if(is_array($selected) AND in_array($option_value, $selected)) {
 			$sel = "selected='selected'";
		}
 		if(is_numeric($option_value) and is_numeric($selected) and $option_value == $selected) {
		 	$sel = "selected='selected'";
		}
		elseif (is_string($option_value) and is_string($selected) and $option_value == $selected) {
			$sel = "selected='selected'";
		}
 		else $sel = '';

  		//Apply function?
 		if ($function) {
			eval('$option_name = '.$function);
		}

		$html .= "\n\t<option value='$option_value' $sel>$option_name</option>";
	}

	//Finishing html
	$html .= "\n</select>\n";

	return $html;
}

//Make a list of radio inputs
function makeRadioList($name, $array, $checked = null, $brs = true, $extra = '', $function = '', $style = '') {

	//Check if $array is an array
	if (!is_array($array)) {
		trigger_error("While trying to make a RADIO list ($name), no valid array was received for the options.",E_USER_WARNING);
		return null;
	}

	//The amount of BR tags needs to be an integer
	$brs = (int) $brs;

	//Set the style
	if ($style) $style = "style='$style'";

	//Make array
	$radios = array();

	//Loop through the array with options
	foreach ($array as $option_value => $option_name) {

		//Ignore empty option names
		if (!$option_name) {
			continue;
		}

		//The is_numeric() check is required because when a string is compared to an integer, the string will become 0...
 		if(is_numeric($option_value) and is_numeric($checked) and $option_value == $checked) {
		 	$check = "checked='checked'";
		}
		elseif (is_string($option_value) and is_string($checked) and $option_value == $checked) {
			$check = "checked='checked'";
		}
 		else $check = '';

  		//Apply function?
 		if ($function) {
			eval('$option_name = '.$function);
		}

		$radios[] = "\n\t<input type='radio' name='$name' value='$option_value' $check $style $extra/> $option_name";
	}

	//Apply BRs or just spaces?
	if ($brs)	$glue = str_repeat('<br/>', $brs);
	else		$glue = ' &nbsp; ';

	//Finishing the html
	$html = "\n".implode($glue, $radios)."\n";

	return $html;
}

//Make a list of radio inputs
function makeCheckBoxList($name, $array, $checked = array(), $brs = true, $extra = '', $function = '', $style = '', $mark_all = false, $identifier = 'm_') {

	//Check if $array is an array
	if (!is_array($array)) {
		trigger_error("While trying to make a CHECKBOX list ($name), no valid array was received for the options.",E_USER_WARNING);
		return null;
	}

	//The amount of BR tags needs to be an integer
	$brs = (int) $brs;

	//Set the style
	if ($style) $style = "style='$style'";

	//Make array
	$checkboxes = array();
	$id			= 1;

	//Loop through the array with options
	foreach ($array as $option_value => $option_name) {

		//Check the checked checkboxes
 		if ($mark_all) {
			$check = "checked='checked'";
		}
		elseif(is_array($checked) and (in_array($option_value, $checked) or (isset($checked[0]) and $checked[0] == 'ALL'))) {
		 	$check = "checked='checked'";
		}
		elseif(is_numeric($option_value) and is_numeric($checked) and $option_value == $checked) {
		 	$check = "checked='checked'";
		}
		elseif (is_string($option_value) and is_string($checked) and $option_value == $checked) {
			$check = "checked='checked'";
		}
 		else $check = '';

  		//Apply function?
 		if ($function) {
			eval('$option_name = '.$function);
		}

		$checkboxes[] = "\n\t<input type='checkbox' id='$identifier$id' name='".$name."[]' value='$option_value' $check $style $extra/> $option_name";
		$id++;
	}

	//Apply BRs or just spaces?
	if ($brs)	$glue = str_repeat('<br/>', $brs);
	else		$glue = ' &nbsp; ';

	//Finishing the html
	$html = "\n".implode($glue, $checkboxes)."\n";

	return $html;
}

/******************************************************************************************
 * Extranet functions
 ***/

// *** Function to return a salted password (for extranet users only)
if (!function_exists('dbSaltSite')) {
	function dbSaltSite($password) {
		$salt			=	'fW7rsWl8fWjgOu9x3MeRbKhKm4f3cUa1M';
		$salt_prefix	=	'2kEpWs8b';
		return sha1($salt_prefix.$password.$salt);
	}
}

/******************************************************************************************
 * Mail queuer
 ***/

function queue_mail($to, $subject, $message, $from = '', $attachment_name = '', $attachment_url = '',
					$is_html = false, $use_template = false, $override_from = false) {


	global $cfg, $_link;

	//From determination
	if (!$from) {
		//If the from field was not specified, we use the site default
		$from 		= 	reverseHtmlEntities($cfg['SITE_EMAIL_NAME']).' <'.$cfg['SITE_EMAIL_ADDRESS'].'>';
	}
	elseif (!$override_from) {
		//If we want dont want to totally override the from field, we assume we received just an email addres and use the site name
		$from		=	reverseHtmlEntities($cfg['SITE_EMAIL_NAME']).' <'.$from.'>';
	}
	else {
		//If we do want to override, we just use the value we got
		$from		=	$from;
	}

	//Do we intend to use a template?
	if (!$is_html and $use_template) {
		if (defined('CMS_PATH_PREFIX') and file_exists(CMS_PATH_PREFIX.'template/email_template.html')) {

			//Get layout from template and make replacements
			$email_html = file_get_contents(CMS_PATH_PREFIX.'template/email_template.html');
			$email_html = str_replace('[SUBJECT]',	$subject,	$email_html);
			$email_html = str_replace('[LINK]',		$_link,		$email_html);
			$message 	= str_replace('[MESSAGE]',	$message,	$email_html);

		}
		elseif (defined('SITE_PATH_PREFIX') and file_exists(SITE_PATH_PREFIX.'template/email_template.html')) {

			//Get layout from template and make replacements
			$email_html = file_get_contents(SITE_PATH_PREFIX.'template/email_template.html');
			$email_html = str_replace('[SUBJECT]',	$subject,	$email_html);
			$email_html = str_replace('[LINK]',		$_link,		$email_html);
			$message 	= str_replace('[MESSAGE]',	$message,	$email_html);
		}
		else {
			$use_template = false;
		}
	}

	//Default email layout if no HTML received and no template usage specified (or if template failed to load)
 	if (!$is_html and !$use_template) {
		$message = "
			<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
			<html xmlns='http://www.w3.org/1999/xhtml'>
			<head>
			<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
			<style type='text/css'>
			body,p,td{font-family:verdana;font-size: 11px;color:#000000;}
			</style>
			</head>
			<body style='font-family:verdana;font-size: 11px;color:#000000;'>
			$message
			</body>
			</html>
		";
	}

	insRec(DIGI_DB_PREFIX."mails2go", 	array('sender', 'recipient', 'subject', 'body', 'is_html'),
										array($from, $to, $subject, $message, 1));
}

/******************************************************************************************
 * Debugging functions
 ***/

//Dump all existing vars to the screen
function allVarDump() {
	echo "<pre>".print_r($GLOBALS,1)."</pre>";
}

?>