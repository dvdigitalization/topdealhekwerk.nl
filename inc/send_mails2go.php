<?php
/*
Title:		Send mails 2 go system
File: 		inc/send_mails2go.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Required sendmail file
if (defined('CMS_PATH_PREFIX') and file_exists(CMS_PATH_PREFIX.'inc/sendmail.php')) {
	require_once(CMS_PATH_PREFIX.'inc/sendmail.php');
}
elseif (defined('SITE_PATH_PREFIX') and file_exists(SITE_PATH_PREFIX.'inc/sendmail.php')) {
	require_once(SITE_PATH_PREFIX.'inc/sendmail.php');
}
else {
	echo '*** SENDMAIL SCRIPT COULD NOT BE FOUND ***';
	exit;
}

//Check if we need to send first
$mails_sent = 0;
$res = eq("SELECT id FROM ".DIGI_DB_PREFIX."mails2go LIMIT 1;");
if ($cfg['AUTO_SENDING'] and mysql_num_rows($res) and (time() - $cfg['LAST_BATCH_SENT']) > $cfg['BATCH_INTERVAL']) {

	//Wait a little while for another possible script which is sending to finish
	usleep(100000);
	
	//Check again to be sure
	$res = eq("SELECT setting_value FROM ".DIGI_DB_PREFIX."settings WHERE setting = 'LAST_BATCH_SENT';");
	$myo = mfo($res);
	$cfg['LAST_BATCH_SENT'] = $myo->setting_value;
	
	//Do we need to send
	if ((time() - $cfg['LAST_BATCH_SENT']) > $cfg['BATCH_INTERVAL']) {
		
		//Update last batch sent immediately to lock the sending process for other scripts
		upRec(DIGI_DB_PREFIX.'settings', "setting = 'LAST_BATCH_SENT'", 'setting_value', time());

		//Lock the table		
		eq("LOCK TABLE ".DIGI_DB_PREFIX."mails2go WRITE;");
		
		//Fetch a batch of emails
		for ($s = 1; $s <= $cfg['MAX_PER_BATCH']; $s++) {
			
			//Read the next email to send
			$res = eq("	SELECT id, sender, recipient, subject, body, attachment_names, attachment_urls, is_html
						FROM ".DIGI_DB_PREFIX."mails2go ORDER BY queued LIMIT 1;");
			
			//If there is one to send...
			if (mysql_num_rows($res)) {
				
				$myo = mfo($res);
				
				//Send it, delete it, sleep 0.05 sec
				sendMail(	$myo->recipient,$myo->subject,$myo->body,$myo->sender,
							explode(";",$myo->attachment_names), explode(";",$myo->attachment_urls), $myo->is_html);
				delRec(DIGI_DB_PREFIX.'mails2go', $myo->id);
				$mails_sent++;
				usleep(50000);	
			}
		}
		
		//Unlock table again
		eq("UNLOCK TABLES;");		
	}
}
?>