<?php
/*
Title:		Frequently used array definitions (NL)
File: 		inc/arrays_NL.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Array with weekday numbers / names in it
$WEEKDAY_ARRAY = array (   	0 => 'Zondag',
							1 => 'Maandag',
                        	2 => 'Dinsdag',
                        	3 => 'Woensdag',
                        	4 => 'Donderdag',
                        	5 => 'Vrijdag',
                        	6 => 'Zaterdag',
                        	7 => 'Zondag'
);

// *** Array with month numbers / names in it
$MONTH_ARRAY = array (   	1 => 'Januari',
                        	2 => 'Februari',
                        	3 => 'Maart',
                        	4 => 'April',
                        	5 => 'Mei',
                        	6 => 'Juni',
                        	7 => 'Juli',
                        	8 => 'Augustus',
                        	9 => 'September',
                        	10 => 'Oktober',
                        	11 => 'November',
                        	12 => 'December'
);
                                	
// *** Array with month numbers / names in it, short format
$MONTH_SHORT_ARRAY = array (	1 => 'Jan',
                        		2 => 'Feb',
                        		3 => 'Mar',
	                        	4 => 'Apr',
	                        	5 => 'Mei',
	                        	6 => 'Jun',
	                        	7 => 'Jul',
	                        	8 => 'Aug',
	                        	9 => 'Sep',
	                        	10 => 'Okt',
	                        	11 => 'Nov',
	                        	12 => 'Dec'
);                                	

// *** Array with years in it
$YEAR_ARRAY = array (   2005 => 2005,
                    	2006 => 2006,
                    	2007 => 2007,
                    	2008 => 2008,
                    	2009 => 2009,
                    	2010 => 2010,
                    	2011 => 2011,
                    	2012 => 2012,
                    	2013 => 2013,
                    	2014 => 2014,
                    	2015 => 2015
);

// *** Hours array
for ($h = 6; $h <= 29; $h++) {
	for ($m = 0; $m <=45; $m+=15) {
		if ($h >= 24) 	$h_p = $h - 24;
		else			$h_p = $h;
		$hour = sprintf('%02d',$h_p).':'.sprintf('%02d',$m);
		$HOUR_ARRAY[$hour] = $hour;
	}
}
                                	
// *** Array with week nr's in it
for ($w = 1; $w <= 52; $w++) {
	$WEEK_ARRAY[$w] = $w;
}

// *** Array with sex definitions in it
$SEX_ARRAY = array(		0	=>	'',
						1 	=> 'Dhr. ',
						2	=> 'Mevr. '
					);

// *** Different type of sex definitions
$SEX_ARRAY_2 = array(	0	=>	'',
						1 	=> 'heer ',
						2	=> 'mevrouw '
					);

// *** Yes/No array
$YESNO_ARRAY = array( 	1	=> 'Ja',
						0	=> 'Nee'
					);	

// *** Regions array for delivery costs etc.
$REGION_ARRAY = array(
						1	=>	'Nederland',
						2	=>	'Europa',
						3	=>	'Wereld'
);	

// *** Languages array
$LANGUAGES_ARRAY = array(
						'NL'	=>	'nederlands',
						'EN'	=>	'engels',
						'FR'	=>	'frans'
);	
					
// *** Countries array
$COUNTRY_ARRAY = array();
$res = eq("SELECT id,name_NL FROM ".DIGI_DB_PREFIX."countries ORDER BY name_NL ASC;");
while ($myo = mfo($res)) {
	$COUNTRY_ARRAY[$myo->id] = $myo->name_NL;
}		
?>