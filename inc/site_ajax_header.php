<?php
/*
Title:		AJAX header construct
File: 		inc/site_ajax_header.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Error displaying
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_DEPRECATED);

// *** Output buffering
ob_start();
register_shutdown_function('ob_end_flush');

// *** Database table prefix
define('DIGI_DB_PREFIX',	'digi_');

// *** Testmode
$test_servers	=	array(	'dev.digitalization.nl','pub.digitalization.nl','archive.digitalization.nl','cms.digitalization.nl',
							'admin.digitalization.nl','192.168.10.1','194.171.50.215','localhost');
$testmode 		= 	in_array($_SERVER['HTTP_HOST'], $test_servers);

// *** Initial settings (backwards compatible with version 2.0x)
if (file_exists('inc/config.php')) {
	require_once('inc/config.php');
}
else {
	require_once('inc/site_config.php');
}

// *** Private folder
if (isset($private_folder) and !empty($private_folder)) {

	if (substr($private_folder, -1) != '/') {
		$private_folder .= '/';
	}
	if (substr($private_folder, 0, 1) != '/') {
		$private_folder = '/'.$private_folder;
	}
	if (!is_dir($private_folder)) {
		$private_folder = '';
	}
}
else {
	$private_folder = '';
}

// *** Absolute path definition
if ($_SERVER['HTTP_HOST'] == '194.171.50.215')	define('SITE_PATH_PREFIX',	'');
else											define('SITE_PATH_PREFIX',	(!empty($absolute_path) ? $absolute_path : ''));

// *** Session functionality
require_once(SITE_PATH_PREFIX.'classes/class_Session.php');

// *** Create new session object
$SESSION 	= 	new Session();

// *** UTF8 / ISO-8859-1 character support?
if (!isset($_GET['no_header']) and !isset($_POST['no_header'])) {
	header('Content-Type: text/html; charset='.$charset);
}

// *** Database settings
if ($private_folder and file_exists($private_folder.'db_settings.php')) {
	require_once($private_folder.'db_settings.php');
}
elseif (file_exists(SITE_PATH_PREFIX.'inc/db_settings.php')) {
	require_once(SITE_PATH_PREFIX.'inc/db_settings.php');
}
elseif (file_exists(SITE_PATH_PREFIX.'inc/style.php')) {
	require_once(SITE_PATH_PREFIX.'inc/style.php');
}
else {
	echo 'Cant find any database settings file @_@';
	exit;
}

// *** Required files for database functionality
require_once(SITE_PATH_PREFIX.'inc/db.php');
require_once(SITE_PATH_PREFIX.'classes/class_DB.php');

// *** Create database connection object
$DB = new DB(dbHost, dbUser, dbPass, dbName);

// *** Global and validation functions
require_once(SITE_PATH_PREFIX.'inc/validation.php');
require_once(SITE_PATH_PREFIX.'inc/globals.php');

// *** UTF-8 MySQL support
if ($charset == 'utf-8') {
	eq("SET NAMES 'utf8';");
}

// *** Settings
$res = eq("SELECT setting, setting_value FROM ".DIGI_DB_PREFIX."settings;");
while ($myo = mfo($res)) {
	$cfg[$myo->setting] = $myo->setting_value;
}

// *** Digi error handling
require_once(SITE_PATH_PREFIX.'inc/error_handling.php');

// *** Installed languages
$installed_languages	=	explode(';', $cfg['INSTALLED_LANGUAGES']);

// *** Language selection
if (isset($_GET['lang']) and $lang = strtoupper($_GET['lang']) and in_array($lang, $installed_languages)) {
	$_SESSION['lang']	=	$lang;
}
if (!isset($_SESSION['lang'])) {
	$_SESSION['lang'] 	= 	$installed_languages[0];
}

// *** System variables
$_file 		= 	basename(htmlspecialchars($_SERVER['SCRIPT_NAME']));
$_path 		= 	substr(htmlspecialchars($_SERVER['SCRIPT_NAME']),0,strlen($_SERVER['SCRIPT_NAME'])-strlen($_file));
$_server	= 	$_SERVER['SERVER_NAME'];
$_link		=	'http://'.$_server.$_path;
$_file_abs	=	$_link.$_file;
$_sep  		= 	DIRECTORY_SEPARATOR;
$_ip		= 	getUserIP();
$_lang 		= 	$_SESSION['lang'];

// *** Sha encoding hashes
$sha1_appendix	=	'xGk32j5037dSfI';
$sha1_hash		=	'UchYeW91Jzk4i0';

// *** Maintenance mode override
if (isset($_GET['no_maintenance'])) {
	$_SESSION['no_maintenance'] = 1;
}

// *** Maintenance mode? Redirect
if ($cfg['IN_MAINTENANCE'] and $_file != 'maintenance.php' and !isset($_SESSION['no_maintenance'])) {
	header('Location: '.SITE_PATH_PREFIX.'maintenance.php');
	exit;
}

// *** Default language, extra check
if (empty($_lang) or !in_array($_lang, $installed_languages)) {
	$_lang = $installed_languages[0];
	$_SESSION['lang']	=	$_lang;
}

// *** Timezone
date_default_timezone_set($cfg['TIMEZONE']);

// *** Set the installed modules
$installed_modules 	= 	explode(';', $cfg['INSTALLED_MODULES']);
$installed_modules 	= 	array_flip($installed_modules);

// *** These modules needs a definition for usage here
define('MOD_SITE_STATISTICS',	92);
define('MOD_AFFILIATES',		36);
define('MOD_PAGES',				7);
define('MOD_MENU',				9);

// *** Hitcounter
if (isset($installed_modules[MOD_SITE_STATISTICS]) and file_exists(SITE_PATH_PREFIX.'classes/class_Hitcounter.php')) {
	require_once(SITE_PATH_PREFIX.'classes/class_Hitcounter.php');
	$Hitcounter = new Hitcounter();
}

// *** Affiliate registration
if (isset($installed_modules[MOD_AFFILIATES]) and file_exists(SITE_PATH_PREFIX.'classes/class_Affiliate.php')) {
	require_once(SITE_PATH_PREFIX.'classes/class_Affiliate.php');
	$Affiliate = new Affiliate();
}

// *** Array definitions
require_once(SITE_PATH_PREFIX.'inc/arrays_'.$_lang.'.php');

// *** Site specific definitions
if (file_exists(SITE_PATH_PREFIX.'inc/common/def_site_specific.php')) {
	require_once(SITE_PATH_PREFIX.'inc/common/def_site_specific.php');
}
else {
	require_once(SITE_PATH_PREFIX.'inc/site_specific_definitions.php');
}

// *** Initial values
$is_authorized	=	false;
$s_id = $g_id	=	'';
$menu_html		=	'';
$login_box		=	'';
?>