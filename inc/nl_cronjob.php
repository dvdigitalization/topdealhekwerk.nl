<?php

/**
 * Topdeal newsletter signups / signoffs cronjob script (DISABLED, now via muskito mail)
 *
 * @file		inc/nl_cronjob.php
 * @version		2.20
 * @author		Digitalization
 * @email		info@digitalization.nl
 * @link		http://www.digitalization.nl/
 * @copyright	All code copyright 2008,2009 by Digitalization
 */
/*
// *** Change working directory
chdir('../');

// *** Basic functionality
require('inc/site_ajax_header.php');

// *** Configuration
$test_mode				=	false;
$db_table				=	DIGI_DB_PREFIX.'newsletter_subscribers';
$post_host				=	'pg.jebentonline.nl';
$post_page				=	'/?methodname=core.newsletter.manager';
$post_port				=	443;
$sign_up_hidden_fields	=	array(	'u'			=>	'14069-1-0',
									'action'	=>	'add',
									'path'		=>	'/website-aanmeldingen/',
									'returnurl'	=>	str_replace('inc/','',$_link).'aangemeld.html');
$sign_off_hidden_fields	=	array(	'u'			=>	'14069-1-0',
									'action'	=>	'out',
									'path'		=>	'',
									'returnurl'	=>	str_replace('inc/','',$_link).'afgemeld.html');

// *** Titles array
$titles					=	array(	1 => 'Dhr.', 2 => 'Mevr.');*/

/*/ *** To test locally, uncomment the following
$post_host 	= 	$_server;
$post_port	=	80;
$post_page 	= 	$_path.'nl_cronjob_tester.php';//*/

// *** Activates cronjob
/*if (isset($_GET['process'])) {

	//Echo
	echo "Running the cron-job script<br/>";

	//See if there are items to process (take 10 at once max)
	$res = eq("SELECT id, first_name, last_name, title, email, newsletter FROM $db_table WHERE to_process = '1' LIMIT 10;");

	//Items?
	if (mnr($res)) {

		//Open socket to division
		$fp = fsockopen((($post_port == 443) ? 'ssl://' : '').$post_host, $post_port, $errno, $errstr, 10);

		//Do we have a socket?
		if ($fp) {

			//Output
			echo "Socket opened to $post_host<br/>";

			//Loop the items
			while ($myo = mfo($res)) {

				//Set the correct fields
				if ($myo->newsletter) {
					$fields = array('voornaam' 		=> 	$myo->first_name,
									'achternaam'	=>	$myo->last_name,
									'aanhef'		=>	$titles[$myo->title],
									'email'			=>	$myo->email) + $sign_up_hidden_fields;
				}
				else {
					$fields = $sign_off_hidden_fields;
				}

				//Output
				echo "Found entry: $myo->last_name, to ".($myo->newsletter ? 'sign-up' : 'sign-off')."<br/>";

				//Concatenate and urlencode them
				foreach ($fields as $key => &$field) {
					$field = $key.'='.urlencode($field);
				}
				$fields = implode('&', $fields);

				//Perform a post on the signup page
				$post  = "POST ".$post_page." ".($post_port == 80 ? 'HTTP' : 'HTTPS')."/1.1\r\n";
				$post .= "Host: ".$post_host."\r\n";
				$post .= "Content-Type: application/x-www-form-urlencoded\r\n";
				$post .= "Content-Length: ".strlen($fields)."\r\n\r\n";
				$post .= $fields."\r\n";

				//Output
				echo "Going to post to $post_host$post_page, contents:<br/><pre>";
				echo $post;
				echo "</pre>Response:<br/><pre>";

				//Output the contents to the socked
				fwrite($fp, $post);
			    while (!feof($fp)) {
			        echo fgets($fp, 128);
			    }
			    fclose($fp);

			    //Sleep a little while to not overload the server
				usleep(10000);

				//Do not process this entry another time
				if (!$test_mode) {
					upRec($db_table, $myo->id, 'to_process', 0);
				}

				//For testing, only do this once
				echo "</pre>";
				if ($test_mode) {
					break;
				}
			}
		}
	}
	else {
		echo "Nothing to do right now";
	}
}*/

?>