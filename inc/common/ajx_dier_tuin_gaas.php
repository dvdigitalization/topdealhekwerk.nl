<?php

/******************************************************************************************
 * Dier en tuin gaas AJAX handler
 ***/

//Basic functionality
chdir('../../');
require_once('inc/site_ajax_header.php');

//Configuration
require_once('inc/common/cfg_dier_tuin_gaas.php');

//Vat
$vat_multiplier			=	(100 + $cfg['PRODUCTS_STANDARD_VAT'])/100;

//Settings
$gfx_folder				=	'gfx'.$_sep.'categories';
$cat_prefix				=	'cat_';
$prod_prefix			=	'product_';
$thumb_prefix			=	'thumb_';
$cat_re_prefix			=	'categorie';
$prod_re_prefix			=	'product';

//Get lowest factor to use for prices
$factor					=	getMyo('digi_topdeal_discounts', "", 'multiplier', 'multiplier', 'ASC', 1);

//Database tables
$db_table_cats			=	DIGI_DB_PREFIX.'topdeal_categories';
$db_table_products		=	DIGI_DB_PREFIX.'topdeal_products';
$db_table_colors		=	DIGI_DB_PREFIX.'topdeal_colors';
$db_table_props			=	DIGI_DB_PREFIX.'topdeal_properties';
$db_table_prop_values	=	DIGI_DB_PREFIX.'topdeal_prop_values';

//Get available maze sizes
if (isset($_GET['maze']) and
	isset($_GET['height']) and isset($_GET['height']) and $height = (int) $_GET['height'] and
	isset($_GET['category_id']) and $category_id = (int) $_GET['category_id']) {

	//Read available maze sizes
	$available_mazes = array();
	$maze_prop = getMyo($db_table_props, "category_id = '$category_id' AND property LIKE 'Maas%'", 'id');
	$res_m = eq("	SELECT property_value AS maze FROM $db_table_prop_values
					WHERE property_id = '$maze_prop' AND product_id IN (
						SELECT id FROM $db_table_products WHERE category_id = '$category_id' AND height = '$height'
					)
					ORDER BY property_value ASC;");
	while ($maze = mfo($res_m)) {
		$available_mazes[$maze->maze] = $maze->maze;
	}
	asort($available_mazes);

	//One maze? Make it the initial maze
	if (count($available_mazes) == 1) {
		$key = key($available_mazes);
	}
	else {
		$key = 0;
	}

	//Add option
	$available_mazes = array(0 => 'Maak uw keuze') + $available_mazes;

	//Maze selector
	$maze_selector = makeSelectBox(		'maze', $available_mazes, $key, 0,
										"class='selectMed' onchange='calc_price(\"Maze changed\");'");
	echo $maze_selector;
	exit;
}

//Get available gaas colors
if (isset($_GET['colors']) and
	isset($_GET['height']) and isset($_GET['height']) and $height = (int) $_GET['height'] and
	isset($_GET['category_id']) and $category_id = (int) $_GET['category_id']) {

	//Get available colors
	$available_colors = array();
	$res_c = eq("	SELECT $db_table_colors.name, $db_table_colors.id FROM $db_table_colors
					LEFT JOIN $db_table_products ON ($db_table_colors.id = color_id)
					WHERE $db_table_products.active = '1' AND category_id = '$category_id' AND height = '$height'
					GROUP BY color_id
					ORDER BY $db_table_colors.name ASC
	");
	while ($color = mfo($res_c)) {
		$available_colors[$color->id] = $color->name;
	}

	$color_selector = '';
	unset($checked);

	$colors = array();
	$c = 0;
	foreach ($available_colors as $color_id => $color_name) {
		$checked = isset($checked) ? '' : "checked='checked'";
		$c++;
		$available_colors[$color_id] = "
			<div class='kleurstaal'>
				<label for='kleur_$c'>
					<img src='gfx/colors/color_$color_id.jpg' alt='$color_name' title='$color_name' />
					<input type='radio' name='color' id='kleur_$c' $checked value='$color_id'/> <span>$color_name</span>
				</label>
			</div>
		";
	}
	$color_selector = implode("\n",$available_colors);

	echo $color_selector;
	exit;
}
elseif (isset($_GET['colors'])) {
	echo "Kies eerste de hoogte en het maas";
	exit;
}

?>