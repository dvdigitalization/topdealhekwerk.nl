<?php

/******************************************************************************************
 * Accessoires formulier
 ***/

$form_contents = "
	<script type='text/javascript'>

		//Initialize vars
		var	amount	=	1;

		//Initial HTML of total price block
		var initial	=	$('#price_block').html();

		//Read form vars
		function read_vars() {

			//Read vars
			amount	=	$('#amount').val();
		}

		//Calculate the total price
		function calc_price(why) {

			//Read the latest vars
			read_vars();

			//alert('Calculating...' + why);

			//Send the price request
			$.get(	'ajaxhond.php?ajax_calc_price&category_id=$category_id&amount=' + amount,
				function(html) {
					$('#price_block').html(html);
					$('#price_block_2').html(html);
				}, 'html'
			);
		}
	</script>
";

?>