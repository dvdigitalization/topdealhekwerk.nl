<?php

/******************************************************************************************
 * Paneelsystemen configuration
 ***/

//Definitions
define('BRACES_PER_POLE',				3);
define('CLIPS_PER_CORNER',				3);
define('CONNECTIONS_PER_CONNECTION',	3);

//Show the ruler
$show_ruler = true;

//Gate categories
$gate_categories			=	array(
	//Usage: product category => array(of gate categories),
	2	=>	array(30,80),
	3	=>	array(32)
);

//Gate height map
$gate_height_map = array(
	//Usage: gate category => array(height fence => height gate)

	//Bekafor classic enkel
	30	=>	array(
		630		=>	1030,		//Cat 2 -- Bekafor classic
		1030	=>	1030,
		1230	=>	1230,
		1530	=>	1530,
		1730	=>	1730,
		2030	=>	2030
	),

	//Bekafor classic dubbel
	80	=>	array(
		630		=>	1030,		//Cat 2 -- Bekafor classic
		1030	=>	1030,
		1230	=>	1230,
		1530	=>	1530,
		1730	=>	1730,
		2030	=>	2030
	),

	//Bekafor prestige enkel
	32	=>	array(
		900		=>	1100,		//Cat 3 -- Bekafor prestige
		1100	=>	1100,
		1300	=>	1300,
		1500	=>	1500
	)
);

//Categories
$braces_category		= 	8;		//Beugels
$clips_category 		= 	9;		//Hoekclips
$connection_category 	= 	10;		//Muurbevestigingen
$foot_category			=	11;		//Voetstuk, voor gebruik met "palen in voetstuk"

//Poles
$pole_categories	=	array(
	2	=>	4,						//Palen te betonneren
	3	=>	6
);

//Foot poles
$footpole_categories	=	array(
	2	=>	5,						//Palen in voetstuk
	3	=>	7
);

//Poles
$poles_options = array(
	0	=>	'Geen bevestigingssysteem.',
	1	=>	'Bekafor paal met beugels.',
	2	=>	'Bekafor paal met beugels voor voetstuk.'
);

?>