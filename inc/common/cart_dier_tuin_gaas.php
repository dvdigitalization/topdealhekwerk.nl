<?php

/******************************************************************************************
 * Cart filler for dier_tuin_gaas
 ***/

//Get the required parameters
$length = 	(int) request('length');
$height	=	(int) request('height');
$maze	=	request('maze');
$color	=	(int) request('color');

//Replace , with . for length
$length = str_replace(',','.',$length);

//Identifier (based on the unique properties combination)
$identifier = md5($category_id.$height.$length.$maze.$color);

//Price determination
$res_p = eq("	SELECT price,sale_price,on_sale,step_size,normal_price FROM $db_table_products
				LEFT JOIN $db_table_prop_values ON (product_id = $db_table_products.id)
				WHERE active = '1' AND height = '$height' AND color_id = '$color' AND category_id = '$category_id'
				AND property_value = '$maze'
				ORDER BY price ASC LIMIT 1;");
$product = mfo($res_p);
if ($product) {
	$price 				= 	$product->on_sale ? $product->sale_price : $product->price;
	$normal_price		= 	$product->normal_price;
	$step				=	$product->step_size;
}

else {
	$dont_add = true;
	return;
}

//Price is per roll, so determine the amount of rolls needed based on the length input
$price 			= $price * ($rolls = ceil($length/$step));
$normal_price 	= $normal_price * ($rolls = ceil($length/$step));

//Build the description
$description = array(	'Lengte gaas'		=>	$length.' m',
						'Rollen benodigd'	=>	$rolls.' stuks ('.$step.' m/rol)',
						'Hoogte gaas'		=>	$height.' mm',
						'Maas'				=>	$maze.' mm',
						'Kleur gaas'		=>	getMyo($db_table_colors, $color, 'name'));

//Item
$item = array(	'amount'		=>	1, //Always one, cause it is the whole package that is the product
				'price'			=>	$price,
				'description'	=>	$description);

if (isset($_GET['ajax_calc_price'])) {
	echo ajax_price_block($price, $normal_price);
	exit;
}
?>