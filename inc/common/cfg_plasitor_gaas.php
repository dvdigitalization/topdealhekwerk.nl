<?php

/******************************************************************************************
 * Plasitor gaas configuration
 ***/

//Definitions
define('M_PER_ROLL',				25);
define('M_BWIRE_PER_ROLL',			100);
define('M_WIRE_PER_ROLL',			100);

//Show the ruler
$show_ruler = true;

//Gate categories
$gate_categories			=	array(
	//Usage: product category => array(of gate categories),
	26	=>	array(28,29,34,35)
);

//Gate height map
$gate_height_map = array(
	//Usage: gate category => array(height fence => height gate)

	//Fortinet enkel
	28	=>	array(
		1000	=>	950,		//Cat 26 -- Plasitor gaas
		1250	=>	1150,
		1500	=>	1450,
		1750	=>	1750,
		1800	=>	1750,
		2000	=>	1950,
		2500	=>	0			//No matching gates
	),

	//Fortinet dubbel
	29	=>	array(
		1000	=>	950,		//Cat 26 -- Plasitor gaas
		1250	=>	1150,
		1500	=>	1450,
		1750	=>	1750,
		1800	=>	1750,
		2000	=>	1950,
		2500	=>	0			//No matching gates
	),

	//Robusta enkel
	34	=>	array(
		1000	=>	950,		//Cat 26 -- Plasitor gaas
		1250	=>	1150,
		1500	=>	1450,
		1750	=>	1750,
		1800	=>	1750,
		2000	=>	1950,
		2500	=>	0			//No matching gates
	),

	//Robusta dubbel
	35	=>	array(
		1000	=>	950,		//Cat 26 -- Plasitor gaas
		1250	=>	1150,
		1500	=>	1450,
		1750	=>	1750,
		1800	=>	1750,
		2000	=>	1950,
		2500	=>	0			//No matching gates
	)
);

//Pole categories
$pole_categories			=	array(
	//Usage: product category => array(of pole categories),
	26	=>	array(73)
);

//Pole height map
$pole_height_map = array(
	//Usage: pole category => array(height fence => height pole)

	//Plasitor palen
	73	=>	array(
		1000	=>	1850,		//Cat 26 -- Plasitor gaas
		1250	=>	2050,
		1500	=>	2300,
		1800	=>	2500,
		2000	=>	2800,
		2500	=>	0			//No matching pole
	)
);

//Accessoires categories
$tubes_category 			= 	74;
$mofs_category 				= 	75;
$heads_category 			= 	76;
$wires_category 			= 	58;
$bwires_category 			= 	57;
$wspanners_category			= 	51;
$ibraces_category 			=	77;
$eputubes_category 			= 	78;
$boutmoer_category 			= 	79;
$polecaps_category			= 	83;

?>