<?php

/**
 * Mail contents - Toegangspoorten
 */

$contents = "
	<!-- Offerte Toegangspoorten -->
	<tr>
		<td height='15'></td>
	</tr>
	<tr>
		<td align='left' valign='top'>
			<table width='100%' cellpadding='0' cellspacing='0' border='0'>
				<tr>
					<td align='left' valign='middle' style='padding:3px 0 0; font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000; font-weight:bold;'>$category->name</td>
				</tr>
				<tr>
					<td height='5'></td>
				</tr>
				<tr>
					<td height='5' style='border-top:1px solid #d7d7d7; font-size:1px;'>&nbsp;</td>
				</tr>
			</table>
			<table width='100%' cellpadding='0' cellspacing='0' border='0'>
				<tr>
					<td align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000; font-weight:bold;' colspan='2'>Uw wensen</td>
				</tr>
				<tr>
					<td height='5' colspan='2'></td>
				</tr>
				<tr>
					<td width='250' align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000;'>Aantal stuks</td>
					<td align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000;'>$_POST[pieces]</td>
				</tr>
				<tr>
					<td width='250' align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000;'>Breedte poort</td>
					<td align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000;'>$_POST[width] m</td>
				</tr>
				<tr>
					<td width='250' align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000;'>Hoogte poort</td>
					<td align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000;'>$_POST[height] mm</td>
				</tr>
				<tr>
					<td height='5' colspan='2'></td>
				</tr>
				<tr>
					<td height='5' style='border-top:1px solid #d7d7d7; font-size:1px;' colspan='2'>&nbsp;</td>
				</tr>
			</table>
			<table width='100%' cellpadding='0' cellspacing='0' border='0'>
				<tr>
					<td align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000; font-weight:bold;' colspan='2'>Kleur van de poort</td>
				</tr>
				<tr>
					<td height='5' colspan='2'></td>
				</tr>
				<tr>
					<td width='250' align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000;'>".getMyo($db_table_colors,$_POST['color'],'name')."</td>
					<td align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000;'>&nbsp;</td>
				</tr>
				<tr>
					<td height='5' colspan='2'></td>
				</tr>
				<tr>
					<td height='5' style='border-top:1px solid #d7d7d7; font-size:1px;' colspan='2'>&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	<!-- Einde Offerte Toegangspoorten -->
";

?>