<?php

/******************************************************************************************
 * Gaas AJAX handler
 ***/

//Basic functionality
chdir('../../');
require_once('inc/site_ajax_header.php');

//Configuration
require_once('inc/common/cfg_gaas.php');
require_once('classes/class_Gaas.php');

//Vat
$vat_multiplier			=	(100 + $cfg['PRODUCTS_STANDARD_VAT'])/100;

//Settings
$gfx_folder				=	'gfx'.$_sep.'categories';
$cat_prefix				=	'cat_';
$prod_prefix			=	'product_';
$thumb_prefix			=	'thumb_';
$cat_re_prefix			=	'categorie';
$prod_re_prefix			=	'product';

//Get lowest factor to use for prices
$factor					=	getMyo('digi_topdeal_discounts', "", 'multiplier', 'multiplier', 'ASC', 1);

//Database tables
$db_table_cats			=	DIGI_DB_PREFIX.'topdeal_categories';
$db_table_products		=	DIGI_DB_PREFIX.'topdeal_products';
$db_table_colors		=	DIGI_DB_PREFIX.'topdeal_colors';
$db_table_props			=	DIGI_DB_PREFIX.'topdeal_properties';
$db_table_prop_values	=	DIGI_DB_PREFIX.'topdeal_prop_values';


//Get available gates for a certain side
if (isset($_GET['gates']) and
	isset($_GET['side']) and $side = (int) $_GET['side'] and
	isset($_GET['height']) and $height = (int) $_GET['height'] and
	isset($_GET['category_id']) and $category_id = (int) $_GET['category_id']) {

	$Gaas = new Gaas($category_id);
	$Gaas->set_height($height);
	echo $Gaas->get_gates_html($side);
	exit;
}

//Re-make gate category HTML for given width
if (isset($_GET['gate_width']) and
	isset($_GET['side']) and $side = (int) $_GET['side'] and
	isset($_GET['width']) and $width = (int) $_GET['width'] and
	isset($_GET['height']) and $height = (int) $_GET['height'] and
	isset($_GET['gate_category_id']) and $gate_category_id = (int) $_GET['gate_category_id']) {

	$Gaas = new Gaas($category_id);
	$Gaas->set_height($height);
	echo $Gaas->get_gate_html($side, $gate_category_id, $width, false);
	exit;
}

//Get available gaas colors
if (isset($_GET['colors']) and
	isset($_GET['height']) and $height = (int) $_GET['height'] and
	isset($_GET['category_id']) and $category_id = (int) $_GET['category_id']) {

	$Gaas = new Gaas($category_id);
	$Gaas->set_height($height);
	echo $Gaas->get_colors_html();
	exit;
}

//Get available pople colors
if (isset($_GET['poles']) and
	isset($_GET['height']) and $height = (int) $_GET['height'] and
	isset($_GET['color_id']) and $color_id = (int) $_GET['color_id'] and
	isset($_GET['category_id']) and $category_id = (int) $_GET['category_id']) {

	$Gaas = new Gaas($category_id);
	$Gaas->set_height($height);
	$Gaas->set_color($color_id);

	echo $Gaas->get_poles_html();
	exit;
}

//Get available gate colors
if (isset($_GET['gate_colors']) and
	isset($_GET['gate_category_id']) and $gate_category_id = (int) $_GET['gate_category_id'] and
	isset($_GET['gate_id']) and $gate_id = (int) $_GET['gate_id'] and
	isset($_GET['side']) and $side = (int) $_GET['side']) {

	$Gaas = new Gaas($category_id);
	echo $Gaas->get_gate_colors_html($side, $gate_category_id, $gate_id);
	exit;
}

//Update price
if (isset($_GET['update_price'])) {

	//Get possible required parameters
	$height				=	(int) request('height');
	$color				=	(int) request('color');
	$poles 				= 	request('poles');
	$nr_corners 		= 	(int) request('corners');
	$nr_sides 			= 	(int) request('sides');

	$Gaas = new Gaas($category_id);
	echo $Gaas->update_price($height, $color, $poles, $nr_corners, $nr_sides);
	exit;
}
?>