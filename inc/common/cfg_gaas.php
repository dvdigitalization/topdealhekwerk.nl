<?php

/******************************************************************************************
 * Gaas configuration
 ***/

//Definitions
//define('CLIPS_CATEGORY_ID',			39); //groen 39, grijs 42 TODO grijs moet apart dus -.-
define('CLIPS_PER_POLE',			6);


$cliptang_category = 44;

//Clips categories
$clips_categories = array(
	1			=>	39,
	2			=>	42
);

//Show the ruler
$show_ruler = true;

//Gate categories
$gate_categories			=	array(
	//Usage: product category => array(of gate categories),
	14	=>	array(33),
	15	=>	array(28,29),
	16	=>	array(33),
	17	=>	array(33),
	18	=>	array(33),
	19	=>	array(28,29),
	24	=>	array()
);

//Gate height map
$gate_height_map = array(
	//Usage: gate category => array(height fence => height gate)

	//Fortinet enkel
	28	=>	array(
		1020	=>	950,		//Cat 15 - Pentanet protect
		1220	=>	1150,
		1520	=>	1450,
		1830	=>	1750,
		2030	=>	1950,

		1500	=>	1450,		//Cat 19 -- Fortinet medium
		1800	=>	1750,
		2010	=>	1950,
		2510	=>	0			//No suitable gates for this height!
	),

	//Fortinet dubbel
	29	=>	array(
		1020	=>	950,		//Cat 15 - Pentanet protect
		1220	=>	1150,
		1520	=>	1450,
		1830	=>	1750,
		2030	=>	1950,

		1500	=>	1450,		//Cat 19 -- Fortinet medium
		1800	=>	1750,
		2010	=>	1950,
		2510	=>	0			//No suitable gates for this height!
	),

	//Bekafor garden enkel
	33	=>	array(
		410		=>	1000,		//Cat 14 -- Luxanet
		660		=>	1000,
		910		=>	1000,

		610		=>	1000,		//Cat 16 & 17 -- Pentanet family & garden
		810		=>	1000,
		1020	=>	1000,
		1220	=>	1200,
		1520	=>	1500,
		1830	=>	1800,
		2030	=>	1800,

		600		=>	1000,		//Cat 18 -- Pentanet light
		800		=>	1000,
		1000	=>	1000,
		1200	=>	1200,
		1500	=>	1500
	)
);

//Pole categories
$pole_categories			=	array(
	//Usage: product category => array(of pole categories),
	14	=>	array(37),
	15	=>	array(40),
	16	=>	array(40),
	17	=>	array(37),
	18	=>	array(37),
	19	=>	array(40),
	24	=>	array(),
);

//Pole height map
$pole_height_map = array(
	//Usage: pole category => array(height fence => height pole)

	//Bekaclip P palen
	37	=>	array(
		410		=>	1200,		//Cat 14 -- Luxanet
		660		=>	1200,
		910		=>	1200,

		610		=>	1200,		//Cat 17 -- Pentanet garden
		810		=>	1500,
		1020	=>	1500,
		1220	=>	1700,
		1520	=>	2000,

		600		=>	1200,		//Cat 18 -- Pentanet light
		800		=>	1500,
		1000	=>	1500,
		1200	=>	1700,
		1500	=>	2000
	),

	//Bekaclip palen
	40	=> array(
		1020	=>	1500,		//Cat 15 - Pentanet protect
		1220	=>	1700,
		1520	=>	2000,
		1830	=>	2300,
		2030	=>	2500,

		610		=>	1100,		//Cat 16 -- Pentanet family
		810		=>	1300,

		1500	=>	2000,		//Cat 19 -- Fortinet medium
		1800	=>	2300,
		2010	=>	2500,
		2510	=>	0			//No suitable poles for this height!
	)
);

?>