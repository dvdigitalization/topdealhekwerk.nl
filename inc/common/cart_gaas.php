<?php

/******************************************************************************************
 * Cart filler for gaas
 ***/

//Configuration
require_once('inc/common/cfg_gaas.php');

//Get the required parameters
$height				=	(int) request('height');
$color				=	(int) request('color');
$poles 				= 	request('poles');
$nr_corners 		= 	(int) request('corners');
$nr_sides 			= 	(int) request('sides');

//Identifier (based on the unique properties combination)
$identifier = md5($category_id.$height.$color);

//Price determination
$res_p = eq("	SELECT price,sale_price,on_sale,step_size,normal_price FROM $db_table_products
				WHERE height='$height'  AND color_id='$color'  AND active = '1' AND category_id = '$category_id' LIMIT 1;");
if ($product = mfo($res_p)) {
	$price 			= 	$product->on_sale ? $product->sale_price : $product->price;
	$normal_price	= 	$product->normal_price;
	$step			=	$product->step_size;
}

else {
	$dont_add = true;
	return;
}

//Build the description
$description = array(	'Lengte gaas'		=>	'',
						'Rollen benodigd'	=>	'',
						'Hoogte gaas'		=>	$height.' mm',
						'Kleur gaas'		=>	getMyo($db_table_colors, $color, 'name'),
						'Bevestiginggaas'	=>	'',
						'Aantal palen'		=>	'',
						'Installatie'		=>	array(	0	=>	array('Aantal hoeken'				=>	$nr_corners),
														1	=>	array('Aantal zijdes'				=>	$nr_sides)
												)
);

//For each side, get the number of gates, or just the length
$side_lengths 			= request('side_lengths');
$side_gates 			= request('side_gates');
$side_gates_amounts		= request('side_gates_amounts');
$side_gates_widths 		= request('side_gates_widths');
$side_gates_colors 		= request('side_gates_colors');
$price_gate 			= 0;
$normal_price_gate 		= 0;
$total_length 			= 0;

for ($i = 1; $i <= $nr_sides; $i++) {

	//Did we select gates
	if (isset($side_gates[$i]) and count($side_gates[$i]) >= 1) {

		//Start building the description
		$description['Zijde '.$i] = array(	0 => array('Toegangspoorten'	=>	'ja'));

		//Algoritm to get the data of each gate
		$gates 		= array();
		$lengths 	= array();
		for ($j = 0; $j < count($side_gates[$i]); $j++) {

			//No amount selected for this gate?
			if ($side_gates_amounts[$i][$j] < 1) {
				continue;
			}

			$gate_id 	= $side_gates[$i][$j];
			$amount 	= $side_gates_amounts[$i][$j];
			$width 		= $side_gates_widths[$i][$j];
			$color 		= $side_gates_colors[$i][$j];

			$res_g = eq("	SELECT 		price, sale_price, on_sale, normal_price FROM $db_table_products
							WHERE 		id='$gate_id' AND active='1'
			;");

			if (mnr($res_g)) {
				$gate = mfo($res_g);
				$price_gate += ($gate->on_sale ? $gate->sale_price : $gate->price) *$amount;
				$normal_price_gate += $gate->normal_price * $amount;
			}

			$description['Zijde '.$i][$j+1]['Breedte poort']	= $width.' mm';
			$description['Zijde '.$i][$j+1]['Kleur poort']		= getMyo($db_table_colors, $color, 'name');
			$description['Zijde '.$i][$j+1]['Type poort']		= getMyo($db_table_cats, getMyo($db_table_products, $gate_id, 'category_id'), 'name');
			$description['Zijde '.$i][$j+1]['Aantal']			= $amount;
		}

		for ($k = 0; $k < count($side_lengths[$i]); $k++) {
			
			//Replace , with .
			$side_lengths[$i][$k] = str_replace(',','.',$side_lengths[$i][$k]);

			//$description['Zijde '.$i]['Lengte '.$k] 	= $side_lengths[$i][$k];
			$description['Zijde '.$i][0]['Lengte '.($k > 0 ? $k : '')] = 	$side_lengths[$i][$k].' m';
			
			//Update total length
			$total_length += $side_lengths[$i][$k];
		}
	}

	//No gates selected, just take the length of this side
	else {
		
		//Replace , with .
		$side_lengths[$i][0] = str_replace(',','.',$side_lengths[$i][0]);

		//Start building the description
		$description['Zijde '.$i] = array(	0	=> array(	'Toegangspoorten'	=>	'Nee',
															'Lengte'			=>	$side_lengths[$i][0].' m'
														)
		);
		//Update total length
		$total_length += $side_lengths[$i][0];
	}
}

//Determine the number of rolls needed
$rolls = ceil($total_length / $step);

//If a pole has been selected
$price_pole 		= 0;
$normal_price_pole 	= 0;
$needed_poles 		= 0;
$pole_name 			= '';
if ($poles) {

	$pole_id = (int) request('pole');
	$res_p = eq("	SELECT 		price, sale_price, on_sale, category_id, normal_price FROM $db_table_products
					WHERE 		id='$pole_id' AND active='1'
	;");

	$price_pole = 0;
	if (mnr($res_p)) {
		$pole 				= mfo($res_p);
		$price_pole 		= $pole->on_sale ? $pole->sale_price : $pole->price;
		$normal_price_pole 	= $pole->normal_price;
		$needed_poles 		= ceil(($total_length / 2.5) + 1);
		$price_pole 		= $needed_poles * $price_pole;
		$normal_price_pole 	= $needed_poles * $normal_price_pole;

		//Add description
		$description['Aantal palen']		= $needed_poles;
		$description['Bevestiginggaas'] = getMyo($db_table_cats, getMyo($db_table_products, $pole_id, 'category_id'), 'name');

		//Needed poles
		$description['Accessoires'][]	= array('Aantal bevestigingspalen' => $needed_poles);
	}

	//Poles have been selected so we also need to include the cliptang :')
	if ($cliptang = getMyo($db_table_products, "category_id='$cliptang_category'", array('price','sale_price','on_sale','normal_price'))) {
		$description['Accessoires'] = array(	array('Cliptang'	=>	'1 stuk'));

		//Update the price
		$price_pole += ($cliptang->on_sale ? $cliptang->sale_price : $cliptang->price);
		$normal_price_pole += $cliptang->normal_price;
	}

	//Add the price of the clips to the pole price
	if (isset($clips_categories[$color]) and $clips = getMyo($db_table_products, "category_id='".$clips_categories[$color]."' AND height='$height'", array('price','sale_price','on_sale','step_size','normal_price'))) {

		//How many bags do we need? this equals the amount of poles x the amount of clips per pole / the amount of clips per bag. step_size is clips_per_pole/clips_per_bag
		$needed_bags 	= ceil($needed_poles * $clips->step_size);

		//Price of each bag
		$price_per_bag 			= $clips->on_sale ? $clips->sale_price : $clips->price;
		$normal_price_per_bag 	= $clips->normal_price;

		//Update price
		$price_pole += $price_per_bag * $needed_bags;
		$normal_price_pole += $normal_price_per_bag * $needed_bags;

		$description['Accessoires'][]	= array('Aantal zakjes clips' => $needed_bags);
	}
}

//Update some description fields
$description['Lengte gaas'] 		= $total_length.' m';
$description['Rollen benodigd'] 	= $rolls.' stuks ('.$step.' m/rol)';

//Price is per roll
$price 			= ($price * $rolls) + $price_pole + $price_gate;
$normal_price 	= ($normal_price * $rolls) + $normal_price_pole + $normal_price_gate;

//Item
$item = array(	'amount'		=>	1, //ik denk gewoon altijd 1 laten, want het is toch een maatwerk pakket
				'price'			=>	$price,
				'description'	=>	$description);

if (isset($_GET['ajax_calc_price'])) {
	echo ajax_price_block($price, $normal_price);
	//echo $price;
	exit;
}
?>