<?php

/******************************************************************************************
 * Panelen AJAX handler
 ***/

//Basic functionality
chdir('../../');
require_once('inc/site_ajax_header.php');

//Configuration
require_once('inc/common/cfg_paneelsystemen.php');
require_once('classes/class_Paneel.php');

//Vat
$vat_multiplier			=	(100 + $cfg['PRODUCTS_STANDARD_VAT'])/100;

//Settings
$gfx_folder				=	'gfx'.$_sep.'categories';
$cat_prefix				=	'cat_';
$prod_prefix			=	'product_';
$thumb_prefix			=	'thumb_';
$cat_re_prefix			=	'categorie';
$prod_re_prefix			=	'product';

//Get lowest factor to use for prices
$factor					=	getMyo('digi_topdeal_discounts', "", 'multiplier', 'multiplier', 'ASC', 1);

//Database tables
$db_table_cats			=	DIGI_DB_PREFIX.'topdeal_categories';
$db_table_products		=	DIGI_DB_PREFIX.'topdeal_products';
$db_table_colors		=	DIGI_DB_PREFIX.'topdeal_colors';
$db_table_props			=	DIGI_DB_PREFIX.'topdeal_properties';
$db_table_prop_values	=	DIGI_DB_PREFIX.'topdeal_prop_values';

//Get available gates for a certain side
if (isset($_GET['gates']) and
	isset($_GET['side']) and $side = (int) $_GET['side'] and
	isset($_GET['height']) and $height = (int) $_GET['height'] and
	isset($_GET['color']) and $color = (int) $_GET['color'] and
	isset($_GET['category_id']) and $category_id = (int) $_GET['category_id']) {

	$Paneel = new Paneel($category_id);
	$Paneel->set_height($height);
	$Paneel->set_color($color);
	echo $Paneel->get_gates_html($side);
	exit;
}

//Re-make gate category HTML for given width
if (isset($_GET['gate_width']) and
	isset($_GET['side']) and $side = (int) $_GET['side'] and
	isset($_GET['width']) and $width = (int) $_GET['width'] and
	isset($_GET['color']) and $color = (int) $_GET['color'] and
	isset($_GET['height']) and $height = (int) $_GET['height'] and
	isset($_GET['gate_category_id']) and $gate_category_id = (int) $_GET['gate_category_id']) {

	$Paneel = new Paneel($category_id);
	$Paneel->set_height($height);
	$Paneel->set_color($color);
	echo $Paneel->get_gate_html($side, $gate_category_id, $width, false);
	exit;
}

//Get available gaas colors
if (isset($_GET['colors']) and
	isset($_GET['height']) and $height = (int) $_GET['height'] and
	isset($_GET['category_id']) and $category_id = (int) $_GET['category_id']) {

	$Paneel = new Paneel($category_id);
	$Paneel->set_height($height);
	echo $Paneel->get_colors_html();
	exit;
}

//Get available pole options
if (isset($_GET['poles']) and
	isset($_GET['height']) and $height = (int) $_GET['height'] and
	isset($_GET['category_id']) and $category_id = (int) $_GET['category_id']) {

	$Paneel = new Paneel($category_id);
	$Paneel->set_height($height);
	echo $Paneel->get_poles_html();
	exit;
}

//Get available gate colors
if (isset($_GET['gate_colors']) and
	isset($_GET['gate_id']) and $gate_id = (int) $_GET['gate_id'] and
	isset($_GET['side']) and $side = (int) $_GET['side']) {

	$Paneel = new Paneel($category_id);
	echo $Paneel->get_gate_colors_html($side, $gate_id);
	exit;
}

//Update price
if (isset($_GET['update_price'])) {

	//Get possible required parameters
	$height				=	(int) request('height');
	$color				=	(int) request('color');
	$poles 				= 	request('poles');
	$nr_corners 		= 	(int) request('corners');
	$nr_sides 			= 	(int) request('sides');

	$Paneel = new Paneel($category_id);
	echo $Paneel->update_price($height, $color, $poles, $nr_corners, $nr_sides);
	exit;
}
?>