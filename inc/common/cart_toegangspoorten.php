<?php

/******************************************************************************************
 * Cart filler for toegangspoorten
 ***/

//Get the required parameters
$height	=	(int) request('height');
$width	=	(int) request('width');
$color	=	(int) request('color');
$amount	=	(int) request('pieces');
$amount = 	$amount > 0 ? $amount : 1;

//Identifier (based on the unique properties combination)
$identifier = md5($category_id.$height.$width.$color);

//Price determination
$res_p = eq("	SELECT price,sale_price,on_sale,normal_price FROM $db_table_products
				WHERE height='$height' AND width='$width' AND color_id='$color'  AND active = '1' AND category_id = '$category_id' LIMIT 1;");
$product = mfo($res_p);
if ($product) {
	$price 			= $product->on_sale ? $product->sale_price : $product->price;
	$normal_price 	= $product->normal_price;
}
else {
	$dont_add = true;
	return;
}

//Build the description
$description = array(	'Aantal'		=> $amount.' stuk(s)',
						'Breedte poort'	=>	$width.' mm',
						'Hoogte poort'	=>	$height.' mm',
						'Kleur poort'	=>	getMyo($db_table_colors, $color, 'name'));

//Item
$item = array(	'amount'		=>	$amount,
				'price'			=>	$price,
				'description'	=>	$description);

if (isset($_GET['ajax_calc_price'])) {
	if ($amount == 0) $amount = 1;
	echo ajax_price_block($price*$amount, $normal_price*$amount);
	exit;
}

?>