<?php

/******************************************************************************************
 * Gaas formulier
 ***/

//Configuration
require_once('inc/common/cfg_dier_tuin_gaas.php');

//AJAX page
$ajax_page							=	'inc/common/ajx_dier_tuin_gaas.php';

//Text elements
$info['INFO_GAAS_LENGTH']			=	INFO_GAAS_LENGTH;
$info['INFO_GAAS_HEIGHT']			=	INFO_GAAS_HEIGHT;
$info['INFO_GAAS_MAAS']				=	INFO_GAAS_MAAS;
$info['COLOR_GAAS_CHOOSE_FIRST']	=	COLOR_GAAS_CHOOSE_FIRST;

/******************************************************************************************
 * Height selection
 ***/

//Read available heights
$available_heights = array(0 => 'Maak uw keuze');
$res_h = eq("SELECT height FROM $db_table_products WHERE category_id = '$category_id' GROUP BY height;");
while ($height = mfo($res_h)) {
	$available_heights[$height->height] = (int) $height->height;
}
asort($available_heights);

//Height selector
$height_selector = makeSelectBox(	'height', $available_heights, request('height',$vk_height), 0,
									"class='selectMed' onchange='height_changed();'");

/******************************************************************************************
 * Maze selection
 ***/

//Maze selector
$available_mazes = array(0 => 'Kies eerst de hoogte');
$maze_selector = makeSelectBox(		'maze', $available_mazes, request('maze',0), 0,
									"class='selectMed' onchange='calc_price(\"Maze changed\");'");
$maze_selector_js = fixForJS($maze_selector);

/******************************************************************************************
 * Scripts
 ***/

$scripts = "
	<script type='text/javascript'>
	<!--

		//Initialize vars
		var length	=	0;
		var	height	=	0;
		var color	=	0;
		var maze	=	0;

		//Initial HTML of total price block
		var initial	=	$('#price_block').html();

		//Read form vars
		function read_vars() {

			//Read vars
			length	=	$('#length').val();
			height	=	$('#height').val();
			maze	=	$('#maze').val();
			color	=	0;

			//No length? Pretend 1 meter
			if (length == 0) {
				length = 1;
			}

			//Read color
			if ($('input[name=\"color\"]').length) {
				color	=	$('input[name=\"color\"]:checked').val();
			}
		}

		//Initialization
		function initialize() {

			//Read vars
			read_vars();

			//Do we have a height?
			if (height > 0 && maze == 0) {
				height_changed();
			}
		}

		//Calculate the total price
		function calc_price(why) {

			//Read the latest vars
			read_vars();

			//alert('Calculating...' + why);

			//We need these vars minimally
			if (height == 0 || length == 0 || color == 0 || maze == 0) {
				$('#price_block').html(initial);
				$('#price_block_2').html(initial);
				return;
			}

			//Send the price rquest
			$.get(	'ajaxhond.php?ajax_calc_price&category_id=$category_id&height=' + height + '&length=' + length + '&color=' + color
					+ '&maze=' + maze,
				function(html) {

					$('#price_block').html(html);
					$('#price_block_2').html(html);
				}, 'html'
			);
		}

		//Is called when the height changes
		function height_changed() {

			//Read the latest vars
			read_vars();

			//Get colors
			get_colors();

			//Get maze
			get_maze();
		}

		function get_colors() {

			//No height?
			if (height == 0) {
				$('#color_select').html(\"$info[COLOR_GAAS_CHOOSE_FIRST]\");
				return;
			}

			//Height request
			$.get('$ajax_page?colors&category_id=$category_id&height='+height,
				function(html) {
					$('#color_select').html(html);
					//calc_price(); not needed here, cause it is always done in conjunction with get_maze();
				}, 'html'
			);
		}

		function get_maze() {

			//No height?
			if (height == 0) {
				$('maze_select').innerHTML = \"$maze_selector_js\";
				return;
			}

			//Maze request
			$.get('$ajax_page?maze&category_id=$category_id&height='+height,
				function(html) {
					$('#maze_select').html(html);
					calc_price(\"Got maze\");
				}, 'html'
			);
		}
	-->
	</script>
";

/******************************************************************************************
 * Form contents
 ***/

$missing['length'] 	= isset($missing['length']) ? "class='foutmelding'" : '';
$missing['height'] 	= isset($missing['height']) ? "class='foutmelding'" : '';
$missing['maze'] 	= isset($missing['maze']) ? "class='foutmelding'" : '';

$form_contents = <<<FORM
	$scripts
	<div class="blok"><img src="gfx/content/txt/stel_uw_gaas_samen.gif" alt="Stel uw gaas samen!" title="Stel uw gaas samen!" /></div>
	$error
	<div class="blok">
		<h2>Uw wensen</h2>
		<div class="table">
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="140" height="22" align="left" valign="middle" $missing[length]>Lengte gaas:</td>
					<td width="15" align="left" valign="middle">*</td>
					<td width="180" align="left" valign="middle"><input type="text" id="length" onkeyup="calc_price('length_changed');" name="length" class="formMed" value='$vk_length' /></td>
					<td align="left" valign="top">
						<p style="padding:4px 10px 0 0; float:left;">m</p>
						<div class="info" id="gaasLengte" onmouseover="displayDiv('gaasLengte', 1);" onmouseout="displayDiv('gaasLengte', 0);" style="display:none;">
							$info[INFO_GAAS_LENGTH]
						</div>
						<a onmouseover="displayDiv('gaasLengte', 1); return false;" onmouseout="displayDiv('gaasLengte', 0);" class="infoIcon"><img src="gfx/layout/nav/info.gif" alt="" title="" style="margin:3px 0 0;" /></a>
					</td>
				</tr>
				<tr>
					<td width="140" height="22" align="left" valign="middle" $missing[height]>Hoogte gaas:</td>
					<td width="15" align="left" valign="middle">*</td>
					<td width="180" align="left" valign="middle">
						$height_selector
					</td>
					<td align="left" valign="top">
						<p style="padding:4px 10px 0 0; float:left;">mm</p>
						<div class="info" id="gaasHoogte" onmouseover="displayDiv('gaasHoogte', 1);" onmouseout="displayDiv('gaasHoogte', 0);" style="display:none;">
							$info[INFO_GAAS_HEIGHT]
						</div>
						<a onmouseover="displayDiv('gaasHoogte', 1); return false;" onmouseout="displayDiv('gaasHoogte', 0);" class="infoIcon"><img src="gfx/layout/nav/info.gif" alt="" title="" style="margin:3px 0 0;" /></a>
					</td>
				</tr>
				<tr>
					<td width="140" height="22" align="left" valign="middle" $missing[maze]>Maas:</td>
					<td width="15" align="left" valign="middle">*</td>
					<td width="180" align="left" valign="middle" id='maze_select'>
						$maze_selector
					</td>
					<td align="left" valign="top">
						<p style="padding:4px 10px 0 0; float:left;">mm</p>
						<div class="info" id="maasInfo" onmouseover="displayDiv('maasInfo', 1);" onmouseout="displayDiv('maasInfo', 0);" style="display:none;">
							$info[INFO_GAAS_MAAS]
						</div>
						<a onmouseover="displayDiv('maasInfo', 1); return false;" onmouseout="displayDiv('maasInfo', 0);" class="infoIcon"><img src="gfx/layout/nav/info.gif" alt="" title="" style="margin:3px 0 0;" /></a>
					</td>
				</tr>
			</table>
		</div>
		<div class="subtableDivider">&nbsp;</div>
	</div>
	<div class='blok'>
		<h2>Kleur van het gaas</h2>
		<div class='table' id='color_select'>Kies eerst de hoogte en het maas</div>
		<div class='subtableDivider'>&nbsp;</div>
	</div>
	<script type='text/javascript'>
		initialize();
	</script>

FORM;

?>