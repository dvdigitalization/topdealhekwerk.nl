<?php

/******************************************************************************************
 * Cart filler for gaas
 ***/

//Configuration
require_once('inc/common/cfg_dubbelstaaf.php');

//Get the required parameters
$height				=	(int) request('height');
$color				=	(int) request('color');
$poles 				= 	(int) request('poles');
$nr_connections 	= 	(int) request('connections');
$nr_corners 		= 	(int) request('corners');
$nr_sides 			= 	(int) request('sides');

//Price determination
$res_p = eq("	SELECT price,sale_price,on_sale,step_size,normal_price FROM $db_table_products
				WHERE height='$height'  AND color_id='$color'  AND active = '1' AND category_id = '$category_id' LIMIT 1;");
if ($product = mfo($res_p)) {
	$price 			= 	$product->on_sale ? $product->sale_price : $product->price;
	$normal_price 	= 	$product->normal_price;
	$step			=	$product->step_size;
}

else {
	$dont_add = true;
	return;
}

//For each side, get the number of gates, or just the length
$side_lengths 			= request('side_lengths');
$side_gates 			= request('side_gates');
$side_gates_amounts		= request('side_gates_amounts');
$side_gates_widths 		= request('side_gates_widths');
$price_gate 			= 0;
$normal_price_gate 		= 0;
$total_length 			= 0;

//Build the description
$description = array(	'Lengte hekwerk'		=>	'',
						'Aantal panelen'		=>	'',
						'Hoogte hekwerk'		=>	$height.' mm',
						'Kleur hekwerk'			=>	getMyo($db_table_colors, $color, 'name'),
						'Verankering palen'		=>	$poles_options[$poles],
						'Aantal palen'			=>	'',

						'Installatie'			=>	array(	0	=>	array('Aantal muurbevestigingen'	=>	$nr_connections),
															1	=>	array('Aantal hoeken'				=>	$nr_corners),
															2	=>	array('Aantal zijdes'				=>	$nr_sides)
												)
);

for ($i = 1; $i <= $nr_sides; $i++) {

	//Did we select gates
	if (isset($side_gates[$i]) and count($side_gates[$i]) > 0) {

		//Start building the description
		$description['Zijde '.$i] = array(	0 => array('Toegangspoorten'	=>	'ja'));

		//Algoritm to get the data of each gate
		$gates 		= array();
		$lengths 	= array();
		for ($j = 0; $j < count($side_gates[$i]); $j++) {

			//No amount selected for this gate?
			if ($side_gates_amounts[$i][$j] < 1) {
				continue;
			}

			$gate_id 	= $side_gates[$i][$j]; 				//This is actually the id of the gate category, edit: apparently not, it is the id of the product
			$amount 	= $side_gates_amounts[$i][$j];
			$width 		= $side_gates_widths[$i][$j];

			$res_g = eq("	SELECT 		price, sale_price, on_sale, normal_price FROM $db_table_products
							WHERE 		id='$gate_id' AND active='1'
			;");

			if (mnr($res_g)) {
				$gate = mfo($res_g);
				$price_gate += ($gate->on_sale ? $gate->sale_price : $gate->price) *$amount;
				$normal_price_gate += $gate->normal_price * $amount;
			}

			$description['Zijde '.$i][$j+1]['Breedte poort']	= $width.' mm';
			$description['Zijde '.$i][$j+1]['Type poort']		= getMyo($db_table_cats, getMyo($db_table_products, $gate_id, 'category_id'), 'name');
			$description['Zijde '.$i][$j+1]['Aantal']			= $amount;
		}

		for ($k = 0; $k < count($side_lengths[$i]); $k++) {
			
			//Replace , with .
			$side_lengths[$i][$k] = str_replace(',','.',$side_lengths[$i][$k]);

			//$description['Zijde '.$i]['Lengte '.$k] 	= $side_lengths[$i][$k];
			$description['Zijde '.$i][0]['Lengte '.($k > 0 ? $k : '')] = 	$side_lengths[$i][$k].' m';

			//Update total length
			$total_length += $side_lengths[$i][$k];
		}
	}

	//No gates selected, just take the length of this side
	else {
		
		//Replace , with .
		$side_lengths[$i][0] = str_replace(',','.',$side_lengths[$i][0]);

		//Start building the description
		$description['Zijde '.$i] = array(	0	=> array(	'Toegangspoorten'	=>	'Nee',
															'Lengte hekwerk'	=>	$side_lengths[$i][0].' m'
														)
		);

		//Update total length
		$total_length += $side_lengths[$i][0];
	}
}

//Determine the number of rolls needed
$panels = ceil($total_length / $step);

$description['Lengte hekwerk'] 	= $total_length.' m';
$description['Aantal panelen']  = $panels.' stuks';

$nr_poles 				= 0;
$price_pole 			= 0;
$price_braces			= 0;

$normal_price_pole 		= 0;
$normal_price_braces 	= 0;

$price_acc 				= 0;
$normal_price_acc 		= 0;

$needed_boutenmoer 		= 0;
$needed_endbraces  		= 0;
$needed_midbraces  		= 0;

//Poles
if ($poles) {

	$nr_poles 		= ceil($panels + 1);
	$pole_category 	= $pole_categories[$category_id];
	$pole_height 	= $pole_height_map[$pole_category][$height];

	$description['Aantal palen']	= $nr_poles.' stuks';

	//Get poles from db
	$res = eq("SELECT id, price, sale_price, on_sale, normal_price FROM $db_table_products WHERE category_id='$pole_category' AND height='$pole_height' AND color_id='$color' AND active='1';");
	if (mnr($res)) {

		$pole = mfo($res);
		$price_pole = $pole->on_sale ? $pole->sale_price : $pole->price;
		$normal_price_pole = $pole->normal_price;
	}

	$price_pole = $nr_poles * $price_pole;
	$normal_price_pole = $nr_poles * $normal_price_pole;

	//Get braces
	$price_braces = 0;
	
	//Mid braces
	$needed_midbraces = ($nr_poles - $nr_corners - 2) * $mid_braces_multipliers[$height];
	if ($mid_brace = getMyo($db_table_products, "category_id='$mid_braces_category'", array('price','sale_price','on_sale','normal_price','step_size'))) {
		$tmp_price = $mid_brace->on_sale ? $mid_brace->sale_price : $mid_brace->price;
		$needed_midbraces_sets = $needed_midbraces / $mid_brace->step_size;
		$price_braces += $needed_midbraces_sets * $tmp_price;
		$normal_price_braces += $needed_midbraces_sets * $mid_brace->normal_price;
	}

	//$needed_endbraces = ($end_braces_multipliers[$height] + $nr_corners) * 3;
	$needed_endbraces = (($end_braces_multipliers[$height]/2) * $nr_corners) + $end_braces_multipliers[$height];
	if ($end_brace = getMyo($db_table_products, "category_id='$end_braces_category'", array('price','sale_price','on_sale','normal_price','step_size'))) {
		$tmp_price = $end_brace->on_sale ? $end_brace->sale_price : $end_brace->price;
		$needed_endbraces_sets = $needed_endbraces / $end_brace->step_size;
		$price_braces += $needed_endbraces_sets * $tmp_price;
		$normal_price_braces += $needed_endbraces_sets * $end_brace->normal_price;
	}
	
	$needed_boutenmoer  = $needed_midbraces + $needed_endbraces;
	if ($boutenmoer = getMyo($db_table_products, "category_id='$boutmoer_category'", array('price','sale_price','on_sale','normal_price','step_size'))) {
		$tmp_price = $boutenmoer->on_sale ? $boutenmoer->sale_price : $boutenmoer->price;
		$needed_boutenmoer_sets = $needed_boutenmoer / $boutenmoer->step_size;
		$price_acc += $needed_boutenmoer_sets * $tmp_price;
		$normal_price_acc += $needed_boutenmoer_sets * $boutenmoer->normal_price;
	}
}

//Get connections
$price_connections 			= 0;
$normal_price_connections 	= 0;
$res = eq("SELECT id, price, sale_price, on_sale, step_size, normal_price FROM $db_table_products WHERE category_id='$connection_category' AND height='$height' AND active='1';");
if (mnr($res)) {
	$connection 				= mfo($res);
	$price_connections 			= $connection->on_sale ? $connection->sale_price : $connection->price;
	$normal_price_connections 	= $connection->normal_price;
	$nr_connections 			= ceil($nr_connections * $connection->step_size); //Step size includes the nr of connections per connection
	$price_connections 			= $price_connections * $nr_connections;
	$normal_price_connections 	= $normal_price_connections * $nr_connections;
}

//Needed polecaps
$needed_polecaps 	= $nr_poles;
if ($polecap = getMyo($db_table_products, "category_id='$polecaps_category'", array('price','sale_price','on_sale','normal_price','step_size'))) {
	$tmp_price = $polecap->on_sale ? $polecap->sale_price : $polecap->price;
	$needed_polecaps_sets = $needed_polecaps / $polecap->step_size;
	$price_acc += $needed_polecaps_sets * $tmp_price;
	$normal_price_acc += $needed_polecaps_sets * $polecap->normal_price;
}

//Update some description fields
$description['Benodigde accessoires'] = array(	0	=>	array(	'Aantal bevestigingspalen'		=>	$nr_poles,																
																'Aantal tussenbeugels'			=>	$needed_midbraces,
																'Aantal eindbeugels'			=>	$needed_endbraces,
																'Bout + moer'					=>	$needed_boutenmoer,
																'Aantal paaldoppen'				=>	$needed_polecaps,
																'Aantal muurbevestigingen'		=>	$nr_connections
														),
);

//Price is per panel, so determine the amount of rolls needed based on the length input
$price 			= $price * $panels;
$normal_price 	= $normal_price * $panels;

//echo "<br/>$price<br/>$price_gate<br/>$price_pole<br/>$price_foots<br/>$price_braces<br/>$price_clips<br/>$price_connections";
//print_pre_r($description);
//exit;

$price 			= $price 		+ $price_gate 			+ $price_pole 				+ $price_braces 		 	+ $price_connections			+ $price_acc;
$normal_price 	= $normal_price + $normal_price_gate 	+ $normal_price_pole 	 	+ $normal_price_braces 	 	+ $normal_price_connections		+ $normal_price_acc;

//Item
$item = array(	'amount'		=>	1, //ik denk gewoon altijd 1 laten, want het is toch een maatwerk pakket
				'price'			=>	$price,
				'description'	=>	$description);


//Identifier (based on the unique properties combination)
$identifier = md5($category_id.$height.$color.$total_length);

if (isset($_GET['ajax_calc_price'])) {
	echo ajax_price_block($price, $normal_price);
	exit;
}
?>