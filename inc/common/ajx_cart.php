<?php

/******************************************************************************************
 * Cart AJAX handler
 ***/

//Basic functionality
chdir('../../');
require_once('inc/site_ajax_header.php');

/******************************************************************************************
 * Load cart contents from session
 ***/

if (!isset($_SESSION['cart']) or !$_SESSION['cart'] or !is_array($_SESSION['cart']) or isset($_GET['clear_cart'])) {
	$_SESSION['cart'] = array();
}
$cart =& $_SESSION['cart'];

// *** Update amount
if (isset($_GET['update_price']) and isset($_GET['amount']) and $identifier = $_GET['update_price'] and isset($cart[$identifier])) {

	//New amount
	$amount = (int) $_GET['amount'];

	//Null or negative amount?
	if ($amount < 1) {
		unset($cart[$identifier]);
		unset($_SESSION['cart'][$identifier]);
	}

	//Regular amount
	else {
		$cart[$identifier]['amount'] = $amount;
	}

	//Get cart amounts
	list ($sub_total, $delivery, $discount, $vat, $total, $discount_code, $discount_info, $factor, $item_prices) = get_cart_amounts();

	//Parse prices
	foreach ($item_prices as &$price) {
		$price = parseAmount($price, '&euro; ');
	}

	//Output XML
	header('content-type: text/xml');
	echo "<?xml version='1.0' encoding='utf-8'?>
<xml>
<sub_total><![CDATA[".parseAmount($sub_total,'&euro; ')."]]></sub_total>
<delivery><![CDATA[".parseAmount($delivery,'&euro; ')."]]></delivery>
<discount><![CDATA[- ".parseAmount($discount,'&euro; ')."]]></discount>
<vat><![CDATA[".parseAmount($vat,'&euro; ')."]]></vat>
<total><![CDATA[".parseAmount($total,'&euro; ')."]]></total>
<item_prices>".implode_special("\t",$item_prices, "<[KEY]><![CDATA[[VALUE]]]></[KEY]>")."</item_prices>
</xml>";
}

// *** Discount code
if (isset($_GET['discount_code'])) {

	//No code given? Clear.
	if (!$discount_code = $_GET['discount_code']) {
		$_SESSION['discount_code'] = $discount_code = '';
		$discount = 0;
	}

	//Validate the discount code
	else {
		evalAll($discount_code);
		$discount_code 	= 	strtoupper($discount_code);
	}

	//Get cart amounts
	list ($sub_total, $delivery, $discount, $vat, $total, $discount_code, $discount_info, $factor, $item_prices) = get_cart_amounts($discount_code);

	//Save code to session
	$discount_code = $discount ? $discount_code : '';
	$_SESSION['discount_code'] = $discount_code;

	//Output XML
	header('content-type: text/xml');
	echo "<?xml version='1.0' encoding='utf-8'?>
<xml>
<discount_code><![CDATA[".$discount_code."]]></discount_code>
<discount_info><![CDATA[".$discount_info."]]></discount_info>
<sub_total><![CDATA[".parseAmount($sub_total,'&euro; ')."]]></sub_total>
<delivery><![CDATA[".parseAmount($delivery,'&euro; ')."]]></delivery>
<discount><![CDATA[- ".parseAmount($discount,'&euro; ')."]]></discount>
<vat><![CDATA[".parseAmount($vat,'&euro; ')."]]></vat>
<total><![CDATA[".parseAmount($total,'&euro; ')."]]></total>
</xml>";
}

?>