<?php

/******************************************************************************************
 * Plasitor gaas formulier
 ***/

//Configuration
require_once('inc/common/cfg_plasitor_gaas.php');

//AJAX page
$ajax_page					=	'inc/common/ajx_plasitor_gaas.php';

//Select the right gate/pole categories
$gate_categories = isset($gate_categories[$category_id]) ? $gate_categories[$category_id] : array();
$pole_categories = isset($pole_categories[$category_id]) ? $pole_categories[$category_id] : array();

//Load class
require('classes/class_Gaas.php');
$Gaas = new Gaas($category_id);

/******************************************************************************************
 * Scripts
 ***/

$script = "
	<script type='text/javascript'>

		//Initialize vars
		var	height	=	0;
		var color	=	0;
		var corners	=	0;
		var sides	=	0;
		var poles	=	0;
		var pole	=	0;

		var needed_heads 		= 0;
		var needed_poles 		= 0;
		var needed_tubes		= 0;
		var needed_mofs 		= 0;
		var needed_wire			= 0;
		var needed_bwire		= 0;
		var needed_wspanners	= 0;
		var needed_ibraces		= 0;
		var needed_eputubes		= 0;
		var needed_boutenmoer	= 0;
		var needed_polecaps		= 0;

		//Initialize side props
		var side_lengths 		=	'';
		var side_gates 			=	'';
		var side_gates_amounts 	=	'';
		var side_gates_widths 	=	'';
		var side_gates_colors 	=	'';

		//Accessories appendix for ajax request
		var acc = '';

		//Initial HTML of total price block
		var initial	=	$('#price_block').html();

		//Read form vars
		function read_vars() {

			//Read vars
			height	=	$('#height').val();
			corners	=	$('#corners').val();
			sides	=	$('#sides').val();
			poles	=	$('input[name=\"poles\"]:checked').val();
			pole	=	0;
			color	=	0;

			needed_heads 		= $('#needed_heads').val();
			needed_poles 		= $('#needed_poles').val();
			needed_tubes 		= $('#needed_tubes').val();
			needed_mofs 		= $('#needed_mofs').val();
			needed_wire 		= $('#needed_wire').val();
			needed_bwire 		= $('#needed_bwire').val();
			needed_wspanners 	= $('#needed_wspanners').val();
			needed_ibraces 		= $('#needed_ibraces').val();
			needed_eputubes 	= $('#needed_eputubes').val();
			needed_boutenmoer 	= $('#needed_boutenmoer').val();
			needed_polecaps 	= $('#needed_polecaps').val();
			
			//Read side lengths
			side_lengths = '';
			pretended = false;
			$('input.side_lengths').each(function() {

				//At least pretend we have one side...
				val = $(this).val();
				if (val == '' && pretended == false) {
					pretended = true;
					val = 1;
				}
				side_lengths += $(this).attr('name') + '=' + val + '&';
			});

			//Read side gates
			side_gates = '';
			$('input.side_gates').each(function() {
				side_gates += $(this).attr('name') + '=' + $(this).val() + '&';
			});

			//Read side gate amounts
			side_gates_amounts = '';
			$('input.side_gates_amounts').each(function() {
				side_gates_amounts += $(this).attr('name') + '=' + $(this).val() + '&';
			});

			//Read side gate widths
			side_gates_widths = '';
			$('select.side_gates_widths').each(function() {
				side_gates += $(this).attr('name') + '=' + $(this).val() + '&';
			});

			//side_gates_widths  ????

			//Read side gate colors
			side_gates_colors = '';
			$('input.side_gates_colors').each(function() {
				side_gates_colors += $(this).attr('name') + '=' + $(this).val() + '&';
			});

			//No sides? Pretend 1 side
			if (sides == 0) {
				sides = 1;
			}

			//Read color
			if ($('input[name=\"color\"]').length) {
				color	=	$('input[name=\"color\"]:checked').val();
				get_poles();
			}

			//Read pole
			if ($('input[name=\"pole\"]').length) {
				pole	=	$('input[name=\"pole\"]:checked').val();
			}
		}

		//Initialization
		function initialize() {

			//Read vars
			read_vars();

			//Do we have a height?
			if (height > 0) {
				changed_height();
			}

			//Do we have corners?
			if (corners > 0) {
				changed_corners();
			}

			//Show the pole selector fix
			if (get_radio_checked_value('poles') == 1) {
				displayDiv('pole_selector', 1);
			}
		}

		//Calculate the total price
		function calc_price(why) {

			//Read the latest vars
			read_vars();

			//alert('Calculating...' + why);

			//We need these vars minimally
			if (height == 0 || color == 0) {
				$('#price_block').html(initial);
				$('#price_block_2').html(initial);
				return;
			}

			//prompt('', 'ajaxhond.php?ajax_calc_price&category_id=$category_id&height=' + height + '&color=' + color + '&corners=' + corners + '&sides=' + sides + '&poles=' + poles + '&pole=' + pole + &needed_poles=' + needed_poles + '&' + side_lengths + side_gates + side_gates_amounts + side_gates_widths + side_gates_colors);

			acc = '';

			acc += '&needed_heads=' + needed_heads + '&needed_tubes=' + needed_tubes + '&needed_mofs=' + needed_mofs;
			acc += '&needed_wire=' + needed_wire + '&needed_bwire=' + needed_bwire + '&needed_wspanners=' + needed_wspanners + '&needed_ibraces=' + needed_ibraces;
			acc += '&needed_eputubes=' + needed_eputubes + '&needed_boutenmoer=' + needed_boutenmoer + '&needed_polecaps=' + needed_polecaps;

			//Send the price request
			$.get(	'ajaxhond.php?ajax_calc_price&category_id=$category_id&height=' + height + '&color=' + color + '&corners=' + corners + '&sides=' + sides + '&poles=' + poles + '&pole=' + pole + '&needed_poles=' + needed_poles + '&' + side_lengths + side_gates + side_gates_amounts + side_gates_widths + side_gates_colors + acc,
				function(html) {
					$('#price_block').html(html);
					$('#price_block_2').html(html);
				}, 'html'
			);

			//KEKEKE
			$.get('$ajax_page?calc_price_accessoires&category_id=$category_id&height=' + height + '&color=' + color,
				function(html) {
					$('#price_plasitor_gaas').html(html);
				}, 'html'
			);
		}

		function update_plasitor() {

			//Read the latest vars
			read_vars();

			nr_sides = $('.side_lengths').size();

			total_length = 0;
			$('.side_lengths').each(function() {
				total_length += this.value*1;
			});

			//Calculate needed rolls
			nr_rolls = total_length / ".(M_PER_ROLL).";

			nr_rolls = Math.ceil(nr_rolls);

			$('#nr_rolls').val(nr_rolls);

			//Calculate the price of the selected pole
			pole = $('input.pole:checked').val();
			if (pole > 0) {
				$.get('$ajax_page?calc_pole_price=' + pole,
					function(html) {
						$('#price_pole').html(html);
					}, 'html'
				);
			}

			//Calculate needed poles
			poles = get_radio_checked_value('poles');

			//poles = $('#poles:checked').val();
			if (poles == 1) {
				needed_poles = Math.ceil(total_length / 3);
				$('#needed_poles').val(needed_poles);

				//Calculate needed couple heads XD
				needed_heads = needed_poles - 2 - corners;
				if (needed_heads < 0) needed_heads = 0;
				$('#needed_heads').val(needed_heads);


				// *** WARNING !!! PROCEED READING FOLLOWING CODE WITH EXTREME CAUTION!!! XD XD XD *** //

				//Calculate needed tubes
				needed_tubes = Math.ceil(total_length / 6);
				$('#needed_tubes').val(needed_tubes);

				//Calculate needed couple mofs :')
				needed_mofs = needed_tubes - 1;
				if (needed_mofs < 0) needed_mofs = 0;
				$('#needed_mofs').val(needed_mofs);

				//Calculate needed spanwire XD
				needed_wire = Math.ceil((total_length * 3)/".M_WIRE_PER_ROLL.");
				$('#needed_wire').val(needed_wire);

				//Calculate needed bindwire XD XD XD
				needed_bwire = Math.ceil(total_length / (4 * ".M_BWIRE_PER_ROLL."));
				$('#needed_bwire').val(needed_bwire);

				//Calculate needed wirespanners XD XD XD ROFL!
				needed_wspanners = Math.ceil(3 + (corners * 3));
				$('#needed_wspanners').val(needed_wspanners);

				//Calculate needed inox braces :')
				needed_ibraces = Math.ceil(2 + (corners * 2));
				$('#needed_ibraces').val(needed_ibraces);

	   			//Calculate needed endpieces uppertubes
	   			needed_eputubes = Math.ceil(2 + (corners * 2));
	   			$('#needed_eputubes').val(needed_eputubes);

	   			//Calculate needed bout + moer
	   			needed_boutenmoer = Math.ceil(2 + (corners * 2));
	   			$('#needed_boutenmoer').val(needed_boutenmoer);

	   			//Calculate needed polecaps
	   			needed_polecaps = 2 + corners*1;
	   			$('#needed_polecaps').val(needed_polecaps);
   			}

			else {
				$('#needed_poles').val(0);
				$('#needed_heads').val(0);
				$('#needed_tubes').val(0);
				$('#needed_mofs').val(0);
				$('#needed_wire').val(0);
				$('#needed_bwire').val(0);
				$('#needed_wspanners').val(0);
				$('#needed_ibraces').val(0);
				$('#needed_eputubes').val(0);
				$('#needed_boutenmoer').val(0);
				$('#needed_polecaps').val(0);
			}
		}

		//Helper function to get the checked value of a radio button
		function get_radio_checked_value(radio_name) {
			return $('input[name='+radio_name+']:checked').val();
		}

		//This function is called when the height is altered
		function changed_height() {

			//Read the latest vars
			read_vars();

			//Check for allowed colors
			get_colors();

			//Check for poles
			get_poles();

			//Toggle the sides again
			toggle_sides();
			
			//Calc price
			calc_price('Changed height');
		}

		//This function is called when the amount of corners is changed
		function changed_corners(corners) {

			//Get the sides selector
			get_sides(corners);

			//Calculate price
			calc_price('Corners changed');
		}

		//This function is called when the amount of sides is changed
		function changed_sides(sides) {

			//Toggle the sides
			toggle_sides(sides);

			//Calculate price
			calc_price('Sides changed');
		}

		//Has gates toggled for a certain side
		function changed_has_gate(side, has_gate) {

			if (has_gate == 1) {

				//Show the div
				displayDiv('side_gates_'+side, 1);

				//Get the height
				var height = $('select#height').val();

				if (height == 0) {
					$('#side_gates_'+side).html('<h3>Zijde '+side+': Kies een poort</h3><p>Kies eerst een hoogte</p>');
					return;
				}

				//AJAX request to get the gates HTML for this side and the selected height
				$.get('$ajax_page?gates&side=' + side + '&category_id=$category_id&height=' + height,
					function(html) {
						$('#side_gates_'+side).html(html);
						calc_price('Changed has gate');
					}, \"html\"
				);
			}
			else {
				displayDiv('side_gates_'+side, 0);

				//Cleare the amount of gates values
				$('input.gate_amount_'+side).each(function() {
					$(this).val('');
				});
				changed_gate_amount(side);
				calc_price('Changed has gate');
			}
		}

		//Gate width for a certain side / gate category has changed
		function changed_gate_width(side, gate_category_id, width) {

			//Get the height
			var height = $('select#height').val();

			if (height == 0) {
				$('#side_gates_'+side).html('<h3>Zijde '+side+': Kies een poort</h3><p>Kies eerst een hoogte</p>');
				return;
			}

			//AJAX request to get the gates HTML for this side and the selected height
			$.get('$ajax_page?gate_width&side=' + side + '&gate_category_id='+gate_category_id+'&height=' + height+'&width='+width,
				function(html) {
					$('#gate_'+side+'_'+gate_category_id).html(html);
					calc_price('Got more gates');
				}, \"html\"
			);
		}

		function changed_gate_amount(side) {

			//The base number of lengths
			nr_lengths = 1;

			//Get the amount of lengths we need to add
			$('input.gate_amount_'+side).each(function() {
				nr_lengths += 1*$(this).val();
			});

			//This is the html for each row
			row_html = \"<tr>\\n\\t<td width='140' height='22' align='left' valign='middle'>Lengte [NR]:</td>\\n\\t<td width='15' align='left' valign='middle'>*</td>\\n\\t<td width='180' align='left' valign='middle'><input type='text' name='side_lengths[\"+side+\"][]' class='side_lengths formMed' onchange='calc_price(2);'/></td>\\n\\t<td align='left' valign='top'><p style='padding:4px 10px 0 0; float:left;'>m</p></td>\\n</tr>\";

			//Get the current amount of lengths input rows
			current_rows = $('table#lengths_table_'+side+' tr').size();

			//The difference between needed rows and current rows
			diff = nr_lengths - current_rows;

			//If we need to add rows
			if (diff > 0) {
				for (l = (current_rows); l < nr_lengths; l++) {
					$('table#lengths_table_'+side+' tr:last').after(row_html.replace('[NR]', l+1));
				}
			}

			//If we need to substract rows
			else {

				while (diff < 0) {

					//Remove last element
					$('table#lengths_table_'+side+' tr:last').remove();
					diff++;
				}
			}

			//Price
			calc_price('Changed gate amount');
		}

		//AJAX color HTML getter
		function get_colors() {

			if (height == 0) {
				$('#color_select').html(\"".COLOR_GAAS_CHOOSE_FIRST."\");
				return;
			}

			$.get('$ajax_page?colors&category_id=$category_id&height=' + height,
				function(html) {
					$('#color_select').html(html);
				}, \"html\"
			);
		}

		//AJAX poles HTML getter
		function get_poles() {

			if (height == 0) {
				$('#poles_html_holder').html(\"Kies eerst een hoogte\");
				return;
			}

			$.get('$ajax_page?poles&category_id=$category_id&height=' + height + '&color_id=' + color,
				function(html) {
					$('#poles_html_holder').html(html);					
				}, \"html\"
			);
		}

		//AJAX sides selector getter
		function get_sides(corners) {

			if (corners == 0) {
				$('#sides_selector').html(\"1<input type='hidden' id='sides' name='sides' value='1' />\");
				toggle_sides(1);
				return;
			}
			else if (corners == 1) {
				$('#sides_selector').html(\"2<input type='hidden' id='sides' name='sides' value='2' />\");
				toggle_sides(2);
				return;
			}
			else if (corners == 2) {
				$('#sides_selector').html(\"3<input type='hidden' id='sides' name='sides' value='3' />\");
				toggle_sides(3);
				return;
			}
			else if (corners == 3) {
				$('#sides_selector').html(\"<select name='sides' id='sides' class='selectMed' onchange='changed_sides(this.value);'><option value='3'>3</option><option value='4'>4</option></select>\");
				toggle_sides(3);
				return;
			}
			else if (corners == 4) {
				$('#sides_selector').html(\"<select name='sides' id='sides' class='selectMed' onchange='changed_sides(this.value);'><option value='4'>4</option><option value='5'>5</option></select>\");
				toggle_sides(4);
				return;
			}
		}

		//Toggle the sides
		function toggle_sides(sides) {

			//Automatically determine amount of open sides
			if (sides == null) {
				sides = $('#sides').val();
			}

			for (s = 1; s <= sides; s++) {
				displayDiv('side_'+s, 1);
				displayDiv('side_lengths_'+s, 1);
				if (get_radio_checked_value('has_gate_'+s) == 1) {
					changed_has_gate(s, 1);
				}
			}
			for (s = sides + 1; s < 6; s++) {
				displayDiv('side_'+s, 0);
				displayDiv('side_lengths_'+s, 0);
				displayDiv('side_gates_'+s, 0);
			}
		}

		//Choose gate color
		function choose_gate_color(side, gate_category_id, gate_id) {

			if (gate_category_id == 0) {
				return;
			}

			$.get('$ajax_page?gate_colors&gate_category_id='+gate_category_id+'&gate_id='+gate_id+'&side='+side,
				function(html) {
					$('#gate_colors_available_'+side).html(html);
				}, \"html\"
			);
			displayDiv('gate_color_chooser_'+side, 1);
		}

		//And set the color picked
		function pick_color(side, gate_id, color_id, color_name) {
			$('a#color_link_'+side+'_'+gate_id).html(color_name);
			$('input#gate_color_'+side+'_'+gate_id).val(color_id);
			displayDiv('gate_color_chooser_'+side, 0);
			calc_price('Gate color chosen');
		}
	</script>
";

/******************************************************************************************
 * Form contents
 ***/

$form_contents = $script."

	<div class='blok'><img src='gfx/content/txt/stel_uw_gaas_samen.gif' alt='Stel uw gaas samen!' title='Stel uw gaas samen!' /></div>
	$error
	<div class='blok'>
		<h2>Uw wensen</h2>
		<div class='table'>
			<table cellpadding='0' cellspacing='0' border='0'>
				<tr>
					<td width='140' height='22' align='left' valign='middle'>Hoogte gaas:</td>
					<td width='15' align='left' valign='middle'>*</td>
					<td width='180' align='left' valign='middle'>
						".$Gaas->get_heights_html()."
					</td>
					<td align='left' valign='top'>
						<p style='padding:4px 10px 0 0; float:left;'>mm</p>
						<div class='info' id='gaasHoogte' onmouseover=\"displayDiv('gaasHoogte', 1);\" onmouseout=\"displayDiv('gaasHoogte', 0);\" style='display:none;'>
							".INFO_GAAS_HEIGHT."
						</div>
						<a onmouseover=\"displayDiv('gaasHoogte', 1)\" onmouseout=\"displayDiv('gaasHoogte', 0);\" class='infoIcon'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:3px 0 0;' /></a>
					</td>
				</tr>
			</table>
		</div>
		<div class='subtableDivider'>&nbsp;</div>
	</div>
	<div class='blok'>
		<h2>Kleur van het gaas</h2>
		<div class='table' id='color_select'>Kies eerst de hoogte van het gaas</div>
		<div class='subtableDivider'>&nbsp;</div>
	</div>
	<div class='blok'>
		<h2 class='float'>Bevestiging gaas</h2>
		<div class='info' id='gaasBevestiging' onmouseover=\"displayDiv('gaasBevestiging', 1);\" onmouseout=\"displayDiv('gaasBevestiging', 0);\" style='display:none;'>
			".INFO_GAAS_POLES."
		</div>
		<a onmouseover=\"displayDiv('gaasBevestiging', 1); return false;\" onmouseout=\"displayDiv('gaasBevestiging', 0);\" class='infoIcon'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:3px 0 0;' /></a>
		<div class='table'>
			<table cellpadding='0' cellspacing='0' border='0'>
				<tr>
					<td width='25' height='22' align='left' valign='top'><input type='radio' value='0' id='poles' name='poles' checked='checked'  onclick=\"displayDiv('pole_selector', 0);update_plasitor();calc_price('');displayDiv('rowAantalPalen', 0);\"/></td>
					<td align='left' valign='middle'>Geen bevestigingspalen</td>
				</tr>
				<tr>
					<td width='25' height='22' align='left' valign='top'><input type='radio' value='1' id='poles' name='poles' onclick=\"displayDiv('pole_selector', 1);update_plasitor();calc_price('');displayDiv('rowAantalPalen', 1);\"/></td>
					<td align='left' valign='middle'>Bevestigingspalen</td>
				</tr>
			</table>
		</div>
		<div class='subtableDivider'>&nbsp;</div>
	</div>
	<div class='blok' id='pole_selector' style='display: none;'>
		<h2>Kies een bevestigingspaal</h2>
		<div class='table' id='poles_html_holder'>
   			".$Gaas->get_poles_html()."
		</div>
		<div class='subtableDivider'>&nbsp;</div>
	</div>

	<div class='blok'>
		<h2>Installatie</h2>
		<div class='table'>
			<table cellpadding='0' cellspacing='0' border='0'>
				<tr>
					<td width='140' height='22' align='left' valign='middle'>Aantal hoeken:</td>
					<td width='15' align='left' valign='middle'>*</td>
					<td width='180' align='left' valign='middle' id='corners_selector'>
						".$Gaas->get_corners_html()."
					</td>
					<td align='left' valign='top'>
						<p style='padding:4px 10px 0 0; float:left;'></p>
						<div class='info' id='cornersInfo'  onmouseover=\"displayDiv('cornersInfo', 1);\" onmouseout=\"displayDiv('cornersInfo', 0);\" style='display:none;'>
							".INFO_CORNERS."
						</div>
						<a onmouseover=\"displayDiv('cornersInfo', 1);\" onmouseout=\"displayDiv('cornersInfo', 0);\" class='infoIcon'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:3px 0 0;' /></a>
					</td>
				</tr>
				<tr>
					<td width='140' height='22' align='left' valign='middle'>Aantal zijden:</td>
					<td width='15' align='left' valign='middle'>*</td>
					<td width='180' align='left' valign='middle' id='sides_selector'>
						".$Gaas->get_sides_chooser_html()."
					</td>
					<td align='left' valign='top'>
						<p style='padding:4px 10px 0 0; float:left;'></p>
						<div class='info' id='sidesInfo'  onmouseover=\"displayDiv('sidesInfo', 1);\" onmouseout=\"displayDiv('sidesInfo', 0);\" style='display:none;'>
							".INFO_SIDES."
						</div>
						<a onmouseover=\"displayDiv('sidesInfo', 1);\" onmouseout=\"displayDiv('sidesInfo', 0);\" class='infoIcon'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:3px 0 0;' /></a>
					</td>
				</tr>
				<!--<tr>
					<td width='140' height='22' align='left' valign='middle'>Aantal muurbevestigingen:</td>
					<td width='15' align='left' valign='middle'>*</td>
					<td width='180' align='left' valign='middle'>
						".$Gaas->get_mural_html()."
					</td>
					<td align='left' valign='top'>
						<p style='padding:4px 10px 0 0; float:left;'></p>
						<div class='info' id='muralInfo' onnmouseover=\"displayDiv('muralInfo', 1);\" onmouseout=\"displayDiv('muralInfo', 0);\" style='display:none;'>
							".INFO_MURAL."
						</div>
						<a onmouseover=\"displayDiv('muralInfo', 1);\" onmouseout=\"displayDiv('muralInfo', 0);\" class='infoIcon'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:3px 0 0;' /></a>
					</td>
				</tr>-->
			</table>
		</div>
		<div class='subtableDivider'>&nbsp;</div>
	</div>

	".$Gaas->get_sides_html()."

	<div class='blok'>
		<h2>Accessoires</h2>
		<div class='table'>
			<table cellpadding='0' cellspacing='0' border='0'>
				<tr>
					<td width='140' height='22' align='left' valign='middle'>Aantal rollen gaas:</td>
					<td width='15' align='left' valign='middle'>*</td>
					<td width='180' align='left' valign='middle'><input type='text' name='nr_rolls' id='nr_rolls' class='formMed' onkeyup='javascript:calc_price(\"\");'/></td>
					<td align='left' valign='top'>
						<p style='padding:4px 10px 0 0; float:left;'></p>
						<div class='info' id='paneelHoogte30' onmouseover=\"displayDiv('paneelHoogte30', 1);\" onmouseout=\"displayDiv('paneelHoogte30', 0);\" style='display:none;'>
							<h2>Aantal rollen gaas</h2>
							<p>Lengte per rol: 25 mtr.</p>
						</div>
						<a onmouseover=\"displayDiv('paneelHoogte30', 1); return false;\" onmouseout=\"displayDiv('paneelHoogte30', 0);\" class='infoIcon'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:3px 0 0;' /></a>
					</td>
					<td id='price_plasitor_gaas' style='padding-left:15px;'>

					</td>
				</tr>
				<tr id='rowAantalPalen' style='display:none;'>
					<td width='140' height='22' align='left' valign='middle'>Aantal palen:</td>
					<td width='15' align='left' valign='middle'>*</td>
					<td width='180' align='left' valign='middle'><input type='text' name='needed_poles' id='needed_poles' class='formMed' onkeyup='javascript:calc_price(\"\");'/></td>
					<td align='left' valign='top'>
						<p style='padding:4px 10px 0 0; float:left;'></p>
						<div class='info' id='paneelHoogte3' onmouseover=\"displayDiv('paneelHoogte3', 1);\" onmouseout=\"displayDiv('paneelHoogte3', 0);\" style='display:none;'>
							<h2>Aantal palen</h2>
							<p>Invullen door Topdeal.</p>
						</div>
						<a onmouseover=\"displayDiv('paneelHoogte3', 1); return false;\" onmouseout=\"displayDiv('paneelHoogte3', 0);\" class='infoIcon'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:3px 0 0;' /></a>
					</td>
					<td id='price_pole' style='padding-left:15px;'>

					</td>
				</tr>
				<tr>
					<td width='140' height='22' align='left' valign='middle'>Bovenbuizen:</td>
					<td width='15' align='left' valign='middle'>*</td>
					<td width='180' align='left' valign='middle'><input type='text' name='needed_tubes' id='needed_tubes' class='formMed' onkeyup='javascript:calc_price(\"\");'/></td>
					<td align='left' valign='top'>
						<p style='padding:4px 10px 0 0; float:left;'></p>
						<div class='info' id='paneelHoogte4' onmouseover=\"displayDiv('paneelHoogte4', 1);\" onmouseout=\"displayDiv('paneelHoogte4', 0);\" style='display:none;'>
							<h2>Bovenbuizen:</h2>
							<p>Invullen door Topdeal.</p>
						</div>
						<a onmouseover=\"displayDiv('paneelHoogte4', 1); return false;\" onmouseout=\"displayDiv('paneelHoogte4', 0);\" class='infoIcon'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:3px 0 0;' /></a>
					</td>
					<td style='padding-left:15px;'>
						Normaal ".parseAmount($vat_multiplier * getMyo($db_table_products,"category_id='$tubes_category'",'normal_price'),'&euro; ')." voor ".parseAmount($vat_multiplier * getMyo($db_table_products,"category_id='$tubes_category'",'price'),'&euro; ')." per stuk.
					</td>
				</tr>
				<tr>
					<td width='140' height='22' align='left' valign='middle'>Koppelmof bovenbuis</td>
					<td width='15' align='left' valign='middle'>*</td>
					<td width='180' align='left' valign='middle'><input type='text' name='needed_mofs' id='needed_mofs' class='formMed' onkeyup='javascript:calc_price(\"\");'/></td>
					<td align='left' valign='top'>
						<p style='padding:4px 10px 0 0; float:left;'></p>
						<div class='info' id='paneelHoogte5' onmouseover=\"displayDiv('paneelHoogte5', 1);\" onmouseout=\"displayDiv('paneelHoogte5', 0);\" style='display:none;'>
							<h2>Koppelmof bovenbuis</h2>
							<p>Invullen door Topdeal.</p>
						</div>
						<a onmouseover=\"displayDiv('paneelHoogte5', 1); return false;\" onmouseout=\"displayDiv('paneelHoogte5', 0);\" class='infoIcon'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:3px 0 0;' /></a>
					</td>
					<td style='padding-left:15px;'>
						Normaal ".parseAmount($vat_multiplier * getMyo($db_table_products,"category_id='$mofs_category'",'normal_price'),'&euro; ')." voor ".parseAmount($vat_multiplier * getMyo($db_table_products,"category_id='$mofs_category'",'price'),'&euro; ')." per stuk.
					</td>
				</tr>
				<tr>
					<td width='140' height='22' align='left' valign='middle'>Kopstuk bovenbuis</td>
					<td width='15' align='left' valign='middle'>*</td>
					<td width='180' align='left' valign='middle'><input type='text' name='needed_heads' id='needed_heads' class='formMed' onkeyup='javascript:calc_price(\"\");'/></td>
					<td align='left' valign='top'>
						<p style='padding:4px 10px 0 0; float:left;'></p>
						<div class='info' id='paneelHoogte6' onmouseover=\"displayDiv('paneelHoogte6', 1);\" onmouseout=\"displayDiv('paneelHoogte6', 0);\" style='display:none;'>
							<h2>Kopstuk bovenbuis</h2>
							<p>Invullen door Topdeal.</p>
						</div>
						<a onmouseover=\"displayDiv('paneelHoogte6', 1); return false;\" onmouseout=\"displayDiv('paneelHoogte6', 0);\" class='infoIcon'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:3px 0 0;' /></a>
					</td>
					<td style='padding-left:15px;'>
						Normaal ".parseAmount($vat_multiplier * getMyo($db_table_products,"category_id='$heads_category'",'normal_price'),'&euro; ')." voor ".parseAmount($vat_multiplier * getMyo($db_table_products,"category_id='$heads_category'",'price'),'&euro; ')." per stuk.
					</td>
				</tr>
				<tr>
					<td width='140' height='22' align='left' valign='middle'>Spandraad op rollen</td>
					<td width='15' align='left' valign='middle'>*</td>
					<td width='180' align='left' valign='middle'><input type='text' name='needed_wire' id='needed_wire' class='formMed' onkeyup='javascript:calc_price(\"\");'/></td>
					<td align='left' valign='top'>
						<p style='padding:4px 10px 0 0; float:left;'></p>
						<div class='info' id='paneelHoogte7' onmouseover=\"displayDiv('paneelHoogte7', 1);\" onmouseout=\"displayDiv('paneelHoogte7', 0);\" style='display:none;'>
							<h2>Spandraad op rollen</h2>
							<p>Lengte per rol: 100 mtr.</p>
						</div>
						<a onmouseover=\"displayDiv('paneelHoogte7', 1); return false;\" onmouseout=\"displayDiv('paneelHoogte7', 0);\" class='infoIcon'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:3px 0 0;' /></a>
					</td>
					<td style='padding-left:15px;'>
						Normaal ".parseAmount($vat_multiplier * getMyo($db_table_products,"category_id='$wires_category'",'normal_price'),'&euro; ')." voor ".parseAmount($vat_multiplier * getMyo($db_table_products,"category_id='$wires_category'",'price'),'&euro; ')." per stuk.
					</td>
				</tr>
				<tr>
					<td width='140' height='22' align='left' valign='middle'>Binddraad op rollen</td>
					<td width='15' align='left' valign='middle'>*</td>
					<td width='180' align='left' valign='middle'><input type='text' name='needed_bwire' id='needed_bwire' class='formMed' onkeyup='javascript:calc_price(\"\");'/></td>
					<td align='left' valign='top'>
						<p style='padding:4px 10px 0 0; float:left;'></p>
						<div class='info' id='paneelHoogte8' onmouseover=\"displayDiv('paneelHoogte8', 1);\" onmouseout=\"displayDiv('paneelHoogte8', 0);\" style='display:none;'>
							<h2>Binddraad op rollen</h2>
							<p>Lengte per rol: 100 mtr.</p>
						</div>
						<a onmouseover=\"displayDiv('paneelHoogte8', 1); return false;\" onmouseout=\"displayDiv('paneelHoogte8', 0);\" class='infoIcon'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:3px 0 0;' /></a>
					</td>
					<td style='padding-left:15px;'>
						Normaal ".parseAmount($vat_multiplier * getMyo($db_table_products,"category_id='$bwires_category'",'normal_price'),'&euro; ')." voor ".parseAmount($vat_multiplier * getMyo($db_table_products,"category_id='$bwires_category'",'price'),'&euro; ')." per stuk.
					</td>
				</tr>
				<tr>
					<td width='140' height='22' align='left' valign='middle'>Draadspanners</td>
					<td width='15' align='left' valign='middle'>*</td>
					<td width='180' align='left' valign='middle'><input type='text' name='needed_wspanners' id='needed_wspanners' class='formMed' onkeyup='javascript:calc_price(\"\");'/></td>
					<td align='left' valign='top'>
						<p style='padding:4px 10px 0 0; float:left;'></p>
						<div class='info' id='paneelHoogte9' onmouseover=\"displayDiv('paneelHoogte9', 1);\" onmouseout=\"displayDiv('paneelHoogte9', 0);\" style='display:none;'>
							<h2>Draadspanners</h2>
							<p>Invullen door Topdeal.</p>
						</div>
						<a onmouseover=\"displayDiv('paneelHoogte9', 1); return false;\" onmouseout=\"displayDiv('paneelHoogte9', 0);\" class='infoIcon'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:3px 0 0;' /></a>
					</td>
					<td style='padding-left:15px;'>
						Normaal ".parseAmount($vat_multiplier * getMyo($db_table_products,"category_id='$wspanners_category'",'normal_price'),'&euro; ')." voor ".parseAmount($vat_multiplier * getMyo($db_table_products,"category_id='$wspanners_category'",'price'),'&euro; ')." per stuk.
					</td>
				</tr>
				<tr>
					<td width='140' height='22' align='left' valign='middle'>Inoxbeugel</td>
					<td width='15' align='left' valign='middle'>*</td>
					<td width='180' align='left' valign='middle'><input type='text' name='needed_ibraces' id='needed_ibraces' class='formMed' onkeyup='javascript:calc_price(\"\");'/></td>
					<td align='left' valign='top'>
						<p style='padding:4px 10px 0 0; float:left;'></p>
						<div class='info' id='paneelHoogte10' onmouseover=\"displayDiv('paneelHoogte10', 1);\" onmouseout=\"displayDiv('paneelHoogte10', 0);\" style='display:none;'>
							<h2>Inoxbeugel</h2>
							<p>Invullen door Topdeal.</p>
						</div>
						<a onmouseover=\"displayDiv('paneelHoogte10', 1); return false;\" onmouseout=\"displayDiv('paneelHoogte10', 0);\" class='infoIcon'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:3px 0 0;' /></a>
					</td>
					<td style='padding-left:15px;'>
						Normaal ".parseAmount($vat_multiplier * getMyo($db_table_products,"category_id='$ibraces_category'",'normal_price'),'&euro; ')." voor ".parseAmount($vat_multiplier * getMyo($db_table_products,"category_id='$ibraces_category'",'price'),'&euro; ')." per stuk.
					</td>
				</tr>
				<tr>
					<td width='140' height='22' align='left' valign='middle'>Eindstukken bovenbuis</td>
					<td width='15' align='left' valign='middle'>*</td>
					<td width='180' align='left' valign='middle'><input type='text' name='needed_eputubes' id='needed_eputubes' class='formMed' onkeyup='javascript:calc_price(\"\");'/></td>
					<td align='left' valign='top'>
						<p style='padding:4px 10px 0 0; float:left;'></p>
						<div class='info' id='paneelHoogte11' onmouseover=\"displayDiv('paneelHoogte11', 1);\" onmouseout=\"displayDiv('paneelHoogte11', 0);\" style='display:none;'>
							<h2>Eindstukken bovenbuis</h2>
							<p>Invullen door Topdeal.</p>
						</div>
						<a onmouseover=\"displayDiv('paneelHoogte11', 1); return false;\" onmouseout=\"displayDiv('paneelHoogte11', 0);\" class='infoIcon'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:3px 0 0;' /></a>
					</td>
					<td style='padding-left:15px;'>
						Normaal ".parseAmount($vat_multiplier * getMyo($db_table_products,"category_id='$eputubes_category'",'normal_price'),'&euro; ')." voor ".parseAmount($vat_multiplier * getMyo($db_table_products,"category_id='$eputubes_category'",'price'),'&euro; ')." per stuk.
					</td>
				</tr>
				<tr>
					<td width='140' height='22' align='left' valign='middle'>Bout + moer</td>
					<td width='15' align='left' valign='middle'>*</td>
					<td width='180' align='left' valign='middle'><input type='text' name='needed_boutenmoer' id='needed_boutenmoer' class='formMed' onkeyup='javascript:calc_price(\"\");'/></td>
					<td align='left' valign='top'>
						<p style='padding:4px 10px 0 0; float:left;'></p>
						<div class='info' id='paneelHoogte12' onmouseover=\"displayDiv('paneelHoogte12', 1);\" onmouseout=\"displayDiv('paneelHoogte12', 0);\" style='display:none;'>
							<h2>Bout + moer</h2>
							<p>Invullen door Topdeal.</p>
						</div>
						<a onmouseover=\"displayDiv('paneelHoogte12', 1); return false;\" onmouseout=\"displayDiv('paneelHoogte12', 0);\" class='infoIcon'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:3px 0 0;' /></a>
					</td>
					<td style='padding-left:15px;'>
						Normaal ".parseAmount($vat_multiplier * getMyo($db_table_products,"category_id='$boutmoer_category'",'normal_price'),'&euro; ')." voor ".parseAmount($vat_multiplier * getMyo($db_table_products,"category_id='$boutmoer_category'",'price'),'&euro; ')." per stuk.
					</td>
				</tr>
				<tr>
					<td width='140' height='22' align='left' valign='middle'>Paaldoppen</td>
					<td width='15' align='left' valign='middle'>*</td>
					<td width='180' align='left' valign='middle'><input type='text' name='needed_polecaps' id='needed_polecaps' class='formMed' onkeyup='javascript:calc_price(\"\");'/></td>
					<td align='left' valign='top'>
						<p style='padding:4px 10px 0 0; float:left;'></p>
						<div class='info' id='paneelHoogte13' onmouseover=\"displayDiv('paneelHoogte13', 1);\" onmouseout=\"displayDiv('paneelHoogte13', 0);\" style='display:none;'>
							<h2>Paaldoppen</h2>
							<p>Invullen door Topdeal.</p>
						</div>
						<a onmouseover=\"displayDiv('paneelHoogte13', 1); return false;\" onmouseout=\"displayDiv('paneelHoogte13', 0);\" class='infoIcon'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:3px 0 0;' /></a>
					</td>
					<td style='padding-left:15px;'>
						Normaal ".parseAmount($vat_multiplier * getMyo($db_table_products,"category_id='$polecaps_category'",'normal_price'),'&euro; ')." voor ".parseAmount($vat_multiplier * getMyo($db_table_products,"category_id='$polecaps_category'",'price'),'&euro; ')." per stuk.
					</td>
				</tr>
			</table>
		</div>
		<div class='subtableDivider'>&nbsp;</div>
	</div>

	<script type='text/javascript'>
		if (get_radio_checked_value('poles') == 1) {
			displayDiv('pole_selector', 1);
		}
		changed_height($('select#height').val());
		changed_corners($('select#corners').val());
	</script>";

?>