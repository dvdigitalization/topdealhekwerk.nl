<?php

/******************************************************************************************
 * Gaas formulier
 ***/

//Configuration
require_once('inc/common/cfg_gaas.php');

//AJAX page
$ajax_page					=	'inc/common/ajx_gaas.php';

//Select the right gate/pole categories
$gate_categories = isset($gate_categories[$category_id]) ? $gate_categories[$category_id] : array();
$pole_categories = isset($pole_categories[$category_id]) ? $pole_categories[$category_id] : array();

//Load class
require('classes/class_Gaas.php');
$Gaas = new Gaas($category_id);

/******************************************************************************************
 * Scripts
 ***/

$script = "
	<script type='text/javascript'>

		function update_plasitor() {
			//dummy function, only for plasitor of relevance
			return false;
		}

		//Initialize vars
		var	height	=	0;
		var color	=	0;
		var corners	=	0;
		var sides	=	0;
		var poles	=	0;
		var pole	=	0;

		//Initialize side props
		var side_lengths 		=	'';
		var side_gates 			=	'';
		var side_gates_amounts 	=	'';
		var side_gates_widths 	=	'';
		var side_gates_colors 	=	'';

		//Initial HTML of total price block
		var initial	=	$('#price_block').html();

		//Read form vars
		function read_vars() {

			//Read vars
			height	=	$('#height').val();
			corners	=	$('#corners').val();
			sides	=	$('#sides').val();
			poles	=	$('input[name=\"poles\"]:checked').val();
			pole	=	0;
			color	=	0;

			//Read side lengths
			side_lengths = '';
			pretended = false;
			$('input.side_lengths').each(function() {

				//At least pretend we have one side...
				val = $(this).val();
				if (val == '' && pretended == false) {
					pretended = true;
					val = 1;
				}
				side_lengths += $(this).attr('name') + '=' + val + '&';
			});

			//Read side gates
			side_gates = '';
			$('input.side_gates').each(function() {
				side_gates += $(this).attr('name') + '=' + $(this).val() + '&';
			});

			//Read side gate amounts
			side_gates_amounts = '';
			$('input.side_gates_amounts').each(function() {
				side_gates_amounts += $(this).attr('name') + '=' + $(this).val() + '&';
			});

			//Read side gate widths
			side_gates_widths = '';
			$('select.side_gates_widths').each(function() {
				side_gates += $(this).attr('name') + '=' + $(this).val() + '&';
			});side_gates_widths

			//Read side gate colors
			side_gates_colors = '';
			$('input.side_gates_colors').each(function() {
				side_gates_colors += $(this).attr('name') + '=' + $(this).val() + '&';
			});

			//No sides? Pretend 1 side
			if (sides == 0) {
				sides = 1;
			}

			//Read color
			if ($('input[name=\"color\"]').length) {
				color	=	$('input[name=\"color\"]:checked').val();
				get_poles();
			}

			//Read pole
			if ($('input[name=\"pole\"]').length) {
				pole	=	$('input[name=\"pole\"]:checked').val();
			}
		}

		//Initialization
		function initialize() {

			//Read vars
			read_vars();

			//Do we have a height?
			if (height > 0) {
				changed_height();
			}

			//Do we have corners?
			if (corners > 0) {
				changed_corners();
			}

			//Show the pole selector fix
			if (get_radio_checked_value('poles') == 1) {
				displayDiv('pole_selector', 1);
			}
		}

		//Calculate the total price
		function calc_price(why) {

			//Read the latest vars
			read_vars();

			//alert('Calculating...' + why);

			//We need these vars minimally
			if (height == 0 || color == 0) {
				$('#price_block').html(initial);
				$('#price_block_2').html(initial);
				return;
			}

			//prompt('', 'ajaxhond.php?ajax_calc_price&category_id=$category_id&height=' + height + '&color=' + color + '&corners=' + corners + '&sides=' + sides + '&poles=' + poles + '&pole=' + pole + '&' + side_lengths + side_gates + side_gates_amounts + side_gates_widths + side_gates_colors);

			//Send the price request
			$.get(	'ajaxhond.php?ajax_calc_price&category_id=$category_id&height=' + height + '&color=' + color + '&corners=' + corners + '&sides=' + sides + '&poles=' + poles + '&pole=' + pole + '&' + side_lengths + side_gates + side_gates_amounts + side_gates_widths + side_gates_colors,
				function(html) {
					$('#price_block').html(html);
					$('#price_block_2').html(html);
				}, 'html'
			);
		}

		//Helper function to get the checked value of a radio button
		function get_radio_checked_value(radio_name) {
			return $('input[name='+radio_name+']:checked').val();
		}

		//This function is called when the height is altered
		function changed_height() {
	
			//Read the latest vars
			read_vars();

			//Check for allowed colors
			get_colors();

			//Check for poles
			get_poles();

			//Toggle the sides again
			toggle_sides();
			
			//Calc price
			calc_price('Changed height');
		}

		//This function is called when the amount of corners is changed
		function changed_corners(corners) {

			//Get the sides selector
			get_sides(corners);

			//Calculate price
			calc_price('Corners changed');
		}

		//This function is called when the amount of sides is changed
		function changed_sides(sides) {

			//Toggle the sides
			toggle_sides(sides);

			//Calculate price
			calc_price('Sides changed');
		}

		//Has gates toggled for a certain side
		function changed_has_gate(side, has_gate) {

			if (has_gate == 1) {

				//Show the div
				displayDiv('side_gates_'+side, 1);

				//Get the height
				var height = $('select#height').val();

				if (height == 0) {
					$('#side_gates_'+side).html('<h3>Zijde '+side+': Kies een poort</h3><p>Kies eerst een hoogte</p>');
					return;
				}

				//AJAX request to get the gates HTML for this side and the selected height
				$.get('$ajax_page?gates&side=' + side + '&category_id=$category_id&height=' + height + '&color=' + color,
					function(html) {
						$('#side_gates_'+side).html(html);
						calc_price('Changed has gate');
					}, \"html\"
				);
			}
			else {
				displayDiv('side_gates_'+side, 0);

				//Cleare the amount of gates values
				$('input.gate_amount_'+side).each(function() {
					$(this).val('');
				});
				changed_gate_amount(side);
				calc_price('Changed has gate');
			}
		}

		//Gate width for a certain side / gate category has changed
		function changed_gate_width(side, gate_category_id, width) {

			//Get the height
			var height = $('select#height').val();

			if (height == 0) {
				$('#side_gates_'+side).html('<h3>Zijde '+side+': Kies een poort</h3><p>Kies eerst een hoogte</p>');
				return;
			}

			//AJAX request to get the gates HTML for this side and the selected height
			$.get('$ajax_page?gate_width&side=' + side + '&gate_category_id='+gate_category_id+'&height=' + height+'&width='+width + '&color=' + color,
				function(html) {
					$('#gate_'+side+'_'+gate_category_id).html(html);
					calc_price('Got more gates');
				}, \"html\"
			);
		}

		function changed_gate_amount(side) {

			//The base number of lengths
			nr_lengths = 1;

			//Get the amount of lengths we need to add
			$('input.gate_amount_'+side).each(function() {
				nr_lengths += 1*$(this).val();
			});

			//This is the html for each row
			row_html = \"<tr>\\n\\t<td width='140' height='22' align='left' valign='middle'>Lengte [NR]:</td>\\n\\t<td width='15' align='left' valign='middle'>*</td>\\n\\t<td width='180' align='left' valign='middle'><input type='text' name='side_lengths[\"+side+\"][]' class='side_lengths formMed' onkeyup='calc_price(2);'/></td>\\n\\t<td align='left' valign='top'><p style='padding:4px 10px 0 0; float:left;'>m</p></td>\\n</tr>\";

			//Get the current amount of lengths input rows
			current_rows = $('table#lengths_table_'+side+' tr').size();

			//The difference between needed rows and current rows
			diff = nr_lengths - current_rows;

			//If we need to add rows
			if (diff > 0) {
				for (l = (current_rows); l < nr_lengths; l++) {
					$('table#lengths_table_'+side+' tr:last').after(row_html.replace('[NR]', l+1));
				}
			}

			//If we need to substract rows
			else {

				while (diff < 0) {

					//Remove last element
					$('table#lengths_table_'+side+' tr:last').remove();
					diff++;
				}
			}

			//Price
			calc_price('Changed gate amount');
		}

		//AJAX color HTML getter
		function get_colors() {

			if (height == 0) {
				$('#color_select').html(\"".COLOR_GAAS_CHOOSE_FIRST."\");
				return;
			}

			$.get('$ajax_page?colors&category_id=$category_id&height=' + height,
				function(html) {
					$('#color_select').html(html);
				}, \"html\"
			);
		}

		//AJAX poles HTML getter
		function get_poles() {

			if (height == 0) {
				$('#poles_html_holder').html(\"Kies eerst een hoogte\");
				return;
			}

			if (color == 0) {
				$('#poles_html_holder').html(\"Kies eerst een kleur\");
			}

			$.get('$ajax_page?poles&category_id=$category_id&height=' + height + '&color_id=' + color,
				function(html) {
					$('#poles_html_holder').html(html);
				}, \"html\"
			);
		}

		//AJAX sides selector getter
		function get_sides(corners) {

			if (corners == 0) {
				$('#sides_selector').html(\"1<input type='hidden' id='sides' name='sides' value='1' />\");
				toggle_sides(1);
				return;
			}
			else if (corners == 1) {
				$('#sides_selector').html(\"2<input type='hidden' id='sides' name='sides' value='2' />\");
				toggle_sides(2);
				return;
			}
			else if (corners == 2) {
				$('#sides_selector').html(\"3<input type='hidden' id='sides' name='sides' value='3' />\");
				toggle_sides(3);
				return;
			}
			else if (corners == 3) {
				$('#sides_selector').html(\"<select name='sides' id='sides' class='selectMed' onchange='changed_sides(this.value);'><option value='3'>3</option><option value='4'>4</option></select>\");
				toggle_sides(3);
				return;
			}
			else if (corners == 4) {
				$('#sides_selector').html(\"<select name='sides' id='sides' class='selectMed' onchange='changed_sides(this.value);'><option value='4'>4</option><option value='5'>5</option></select>\");
				toggle_sides(4);
				return;
			}
		}

		//Toggle the sides
		function toggle_sides(sides) {

			//Automatically determine amount of open sides
			if (sides == null) {
				sides = $('#sides').val();
			}

			for (s = 1; s <= sides; s++) {
				displayDiv('side_'+s, 1);
				displayDiv('side_lengths_'+s, 1);
				if (get_radio_checked_value('has_gate_'+s) == 1) {
					changed_has_gate(s, 1);
				}
			}
			for (s = sides + 1; s < 6; s++) {
				displayDiv('side_'+s, 0);
				displayDiv('side_lengths_'+s, 0);
				displayDiv('side_gates_'+s, 0);
			}
		}

		//Choose gate color
		function choose_gate_color(side, gate_category_id, gate_id) {

			if (gate_category_id == 0) {
				return;
			}

			$.get('$ajax_page?gate_colors&gate_category_id='+gate_category_id+'&gate_id='+gate_id+'&side='+side,
				function(html) {
					$('#gate_colors_available_'+side).html(html);
				}, \"html\"
			);
			displayDiv('gate_color_chooser_'+side, 1);
		}

		//And set the color picked
		function pick_color(side, gate_id, color_id, color_name) {
			$('a#color_link_'+side+'_'+gate_id).html(color_name);
			$('input#gate_color_'+side+'_'+gate_id).val(color_id);
			displayDiv('gate_color_chooser_'+side, 0);
			calc_price('Gate color chosen');
		}
	</script>
";

/******************************************************************************************
 * Form contents
 ***/

$form_contents = $script."

	<div class='blok'><img src='gfx/content/txt/stel_uw_gaas_samen.gif' alt='Stel uw gaas samen!' title='Stel uw gaas samen!' /></div>
	$error
	<div class='blok'>
		<h2>Uw wensen</h2>
		<div class='table'>
			<table cellpadding='0' cellspacing='0' border='0'>
				<tr>
					<td width='140' height='22' align='left' valign='middle'>Hoogte gaas:</td>
					<td width='15' align='left' valign='middle'>*</td>
					<td width='180' align='left' valign='middle'>
						".$Gaas->get_heights_html()."
					</td>
					<td align='left' valign='top'>
						<p style='padding:4px 10px 0 0; float:left;'>mm</p>
						<div class='info' id='gaasHoogte' onmouseover=\"displayDiv('gaasHoogte', 1);\" onmouseout=\"displayDiv('gaasHoogte', 0);\" style='display:none;'>
							".INFO_GAAS_HEIGHT."
						</div>
						<a onmouseover=\"displayDiv('gaasHoogte', 1)\" onmouseout=\"displayDiv('gaasHoogte', 0);\" class='infoIcon'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:3px 0 0;' /></a>
					</td>
				</tr>
			</table>
		</div>
		<div class='subtableDivider'>&nbsp;</div>
	</div>
	<div class='blok'>
		<h2>Kleur van het gaas</h2>
		<div class='table' id='color_select'>Kies eerst de hoogte van het gaas</div>
		<div class='subtableDivider'>&nbsp;</div>
	</div>
	<div class='blok'>
		<h2 class='float'>Bevestiging gaas</h2>
		<div class='info' id='gaasBevestiging' onmouseover=\"displayDiv('gaasBevestiging', 1);\" onmouseout=\"displayDiv('gaasBevestiging', 0);\" style='display:none;'>
			".INFO_GAAS_POLES."
		</div>
		<a onmouseover=\"displayDiv('gaasBevestiging', 1); return false;\" onmouseout=\"displayDiv('gaasBevestiging', 0);\" class='infoIcon'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:3px 0 0;' /></a>
		<div class='table'>
			<table cellpadding='0' cellspacing='0' border='0'>
				<tr>
					<td width='25' height='22' align='left' valign='top'><input type='radio' value='0' name='poles' checked='checked'  onclick=\"displayDiv('pole_selector', 0); calc_price('Changed has poles');\"/></td>
					<td align='left' valign='middle'>Geen bevestigingspalen</td>
				</tr>
				<tr>
					<td width='25' height='22' align='left' valign='top'><input type='radio' value='1' name='poles' onclick=\"displayDiv('pole_selector', 1); calc_price('Changed has poles');\"/></td>
					<td align='left' valign='middle'>Bevestigingspalen</td>
				</tr>
			</table>
		</div>
		<div class='subtableDivider'>&nbsp;</div>
	</div>
	<div class='blok' id='pole_selector' style='display: none;'>
		<h2>Kies een bevestigingspaal</h2>
		<div class='table' id='poles_html_holder'>
   			".$Gaas->get_poles_html()."
		</div>
		<div class='subtableDivider'>&nbsp;</div>
	</div>

	<div class='blok'>
		<h2>Installatie</h2>
		<div class='table'>
			<table cellpadding='0' cellspacing='0' border='0'>
				<tr>
					<td width='140' height='22' align='left' valign='middle'>Aantal hoeken:</td>
					<td width='15' align='left' valign='middle'>*</td>
					<td width='180' align='left' valign='middle' id='corners_selector'>
						".$Gaas->get_corners_html()."
					</td>
					<td align='left' valign='top'>
						<p style='padding:4px 10px 0 0; float:left;'></p>
						<div class='info' id='cornersInfo'  onmouseover=\"displayDiv('cornersInfo', 1);\" onmouseout=\"displayDiv('cornersInfo', 0);\" style='display:none;'>
							".INFO_CORNERS."
						</div>
						<a onmouseover=\"displayDiv('cornersInfo', 1);\" onmouseout=\"displayDiv('cornersInfo', 0);\" class='infoIcon'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:3px 0 0;' /></a>
					</td>
				</tr>
				<tr>
					<td width='140' height='22' align='left' valign='middle'>Aantal zijden:</td>
					<td width='15' align='left' valign='middle'>*</td>
					<td width='180' align='left' valign='middle' id='sides_selector'>
						".$Gaas->get_sides_chooser_html()."
					</td>
					<td align='left' valign='top'>
						<p style='padding:4px 10px 0 0; float:left;'></p>
						<div class='info' id='sidesInfo'  onmouseover=\"displayDiv('sidesInfo', 1);\" onmouseout=\"displayDiv('sidesInfo', 0);\" style='display:none;'>
							".INFO_SIDES."
						</div>
						<a onmouseover=\"displayDiv('sidesInfo', 1);\" onmouseout=\"displayDiv('sidesInfo', 0);\" class='infoIcon'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:3px 0 0;' /></a>
					</td>
				</tr>
				<!--<tr>
					<td width='140' height='22' align='left' valign='middle'>Aantal muurbevestigingen:</td>
					<td width='15' align='left' valign='middle'>*</td>
					<td width='180' align='left' valign='middle'>
						".$Gaas->get_mural_html()."
					</td>
					<td align='left' valign='top'>
						<p style='padding:4px 10px 0 0; float:left;'></p>
						<div class='info' id='muralInfo' onnmouseover=\"displayDiv('muralInfo', 1);\" onmouseout=\"displayDiv('muralInfo', 0);\" style='display:none;'>
							".INFO_MURAL."
						</div>
						<a onmouseover=\"displayDiv('muralInfo', 1);\" onmouseout=\"displayDiv('muralInfo', 0);\" class='infoIcon'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:3px 0 0;' /></a>
					</td>
				</tr>-->
			</table>
		</div>
		<div class='subtableDivider'>&nbsp;</div>
	</div>

	".$Gaas->get_sides_html()."

	<script type='text/javascript'>
		initialize();
	</script>";

?>