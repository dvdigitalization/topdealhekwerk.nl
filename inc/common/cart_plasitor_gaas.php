<?php

/******************************************************************************************
 * Cart filler for plasitor_gaas
 ***/

//Configuration
require_once('inc/common/cfg_plasitor_gaas.php');

//Get the required parameters
$height				=	(int) request('height');
$color				=	(int) request('color');
$poles 				= 	request('poles');
$nr_corners 		= 	(int) request('corners');
$nr_sides 			= 	(int) request('sides');

//Identifier (based on the unique properties combination)
$identifier = md5($category_id.$height.$color);

//Price determination
$res_p = eq("	SELECT price,sale_price,on_sale,step_size,normal_price FROM $db_table_products
				WHERE height='$height'  AND color_id='$color'  AND active = '1' AND category_id = '$category_id' LIMIT 1;");
if ($product = mfo($res_p)) {
	$price 			= 	$product->on_sale ? $product->sale_price : $product->price;
	$normal_price 	= 	$product->normal_price;
	$step			=	$product->step_size;
}

else {
	$dont_add = true;
	return;
}

//Build the description
$description = array(	'Lengte gaas'		=>	'',
						'Rollen benodigd'	=>	'',
						'Hoogte gaas'		=>	$height.' mm',
						'Kleur gaas'		=>	getMyo($db_table_colors, $color, 'name'),
						'Bevestiginggaas'	=>	'',
						'Aantal palen'		=>	'',
						'Installatie'		=>	array(	0	=>	array('Aantal hoeken'				=>	$nr_corners),
														1	=>	array('Aantal zijdes'				=>	$nr_sides)
												)
);

//For each side, get the number of gates, or just the length
$side_lengths 			= request('side_lengths');
$side_gates 			= request('side_gates');
$side_gates_amounts		= request('side_gates_amounts');
$side_gates_widths 		= request('side_gates_widths');
$side_gates_colors 		= request('side_gates_colors');
$price_gate 			= 0;
$normal_price_gate 		= 0;
$total_length 			= 0;

for ($i = 1; $i <= $nr_sides; $i++) {

	//Did we select gates
	if (isset($side_gates[$i]) and count($side_gates[$i]) > 0) {

		//Start building the description
		$description['Zijde '.$i] = array(	0 => array('Toegangspoorten'	=>	'ja'));

		//Algoritm to get the data of each gate
		$gates 		= array();
		$lengths 	= array();
		for ($j = 0; $j < count($side_gates[$i]); $j++) {

			//No amount selected for this gate?
			if ($side_gates_amounts[$i][$j] < 1) {
				continue;
			}

			$gate_id 	= $side_gates[$i][$j];
			$amount 	= $side_gates_amounts[$i][$j];
			$width 		= $side_gates_widths[$i][$j];
			$color 		= $side_gates_colors[$i][$j];

			$res_g = eq("	SELECT 		price, sale_price, on_sale, normal_price FROM $db_table_products
							WHERE 		id='$gate_id' AND active='1'
			;");

			if (mnr($res_g)) {
				$gate = mfo($res_g);
				$price_gate += ($gate->on_sale ? $gate->sale_price : $gate->price) * $amount;
				$normal_price_gate += $gate->normal_price * $amount;
			}

			$description['Zijde '.$i][$j+1]['Breedte poort']	= $width.' mm';
			$description['Zijde '.$i][$j+1]['Type poort']		= getMyo($db_table_cats, getMyo($db_table_products, $gate_id, 'category_id'), 'name');
			$description['Zijde '.$i][$j+1]['Aantal']			= $amount;
		}

		for ($k = 0; $k < count($side_lengths[$i]); $k++) {
			
			//Replace , with .
			$side_lengths[$i][$k] = str_replace(',','.',$side_lengths[$i][$k]);
			
			//$description['Zijde '.$i]['Lengte '.$k] 	= $side_lengths[$i][$k];
			$description['Zijde '.$i][0]['Lengte '.($k > 0 ? $k : '')] = 	$side_lengths[$i][$k].' m';

			//Update total length
			$total_length += $side_lengths[$i][$k];
		}
	}

	//No gates selected, just take the length of this side
	else {
		
		//Replace , with .
		$side_lengths[$i][0] = str_replace(',','.',$side_lengths[$i][0]);

		//Start building the description
		$description['Zijde '.$i] = array(	0	=> array(	'Toegangspoorten'	=>	'Nee',
															'Lengte'			=>	$side_lengths[$i][0].' m'
														)
		);

		//Update total length
		$total_length += $side_lengths[$i][0];
	}
}

//Determine the number of rolls needed
$rolls = $nr_rolls = ceil($total_length / $step); //Even dolven door discrepantie in naamgeving in mijn js code -.-

//Accessoires price
$price_acc 			= 0;
$normal_price_acc 	= 0;

//If a pole has been selected
$price_pole 		= 0;
$normal_price_pole 	= 0;
if ($poles) {

	$pole_id = (int) request('pole');
	$res_p = eq("	SELECT 		price, sale_price, on_sale, category_id, normal_price
					FROM 		$db_table_products
					WHERE 		id='$pole_id' AND active='1'
	;");

	//Calculate needed poles
	$needed_poles = (int)request('needed_poles');

	if (mnr($res_p)) {
		$pole = mfo($res_p);
		$price_pole = $pole->on_sale ? $pole->sale_price : $pole->price;
		$normal_price_pole = $pole->normal_price;

		//Add description
		$description['Bevestiging gaas'] 	= getMyo($db_table_cats, $pole->category_id, 'name');
		$description['Aantal palen']		= $needed_poles;
	}

	$price_pole = $needed_poles * $price_pole;
	$normal_price_pole = $needed_poles * $normal_price_pole;

	$description['Accessoires'][]	= array('Aantal bevestigingspalen' => $needed_poles);

	//other stuff
	$nr_poles 		= request('nr_poles');
	$needed_heads 	= request('needed_heads');
}

//Other stuff
$needed_tubes 		= request('needed_tubes');
if ($tube = getMyo($db_table_products, "category_id='$tubes_category'", array('id','price','sale_price','on_sale','normal_price'))) {
	$tmp_price = $tube->on_sale ? $tube->sale_price : $tube->price;
	$price_acc += $needed_tubes * $tmp_price;
	$normal_price_acc += $needed_tubes * $tube->normal_price;
}

$needed_mofs 		= request('needed_mofs');
if ($mof = getMyo($db_table_products, "category_id='$mofs_category'", array('price','sale_price','on_sale','normal_price'))) {
	$tmp_price = $mof->on_sale ? $mof->sale_price : $mof->price;
	$price_acc += $needed_mofs * $tmp_price;
	$normal_price_acc += $needed_mofs * $mof->normal_price;
}

$needed_heads 		= request('needed_heads');
if ($head = getMyo($db_table_products, "category_id='$heads_category'", array('price','sale_price','on_sale','normal_price'))) {
	$tmp_price = $head->on_sale ? $head->sale_price : $head->price;
	$price_acc += $needed_heads * $tmp_price;
	$normal_price_acc += $needed_heads * $head->normal_price;
}

if ($wire = getMyo($db_table_products, "category_id='$wires_category'", array('price','sale_price','on_sale','step_size','normal_price'))) {
	$tmp_price 		= $wire->on_sale ? $wire->sale_price : $wire->price;
	$needed_wire 	= ceil(request('needed_wire') / $wire->step_size);
	$price_acc 		+=$needed_wire * $tmp_price;
	$normal_price_acc += $needed_wire * $wire->normal_price;
}

if ($bwire = getMyo($db_table_products, "category_id='$bwires_category'", array('price','sale_price','on_sale','step_size','normal_price'))) {
	$tmp_price 		= $bwire->on_sale ? $bwire->sale_price : $bwire->price;
	$needed_bwire 	= ceil(request('needed_bwire') / $bwire->step_size);
	$price_acc 		+= $needed_bwire * $tmp_price;
	$normal_price_acc += $needed_bwire * $bwire->normal_price;
}

$needed_wspanners 	= request('needed_wspanners');
if ($wspanner = getMyo($db_table_products, "category_id='$wspanners_category'", array('price','sale_price','on_sale','normal_price'))) {
	$tmp_price = $wspanner->on_sale ? $wspanner->sale_price : $wspanner->price;
	$price_acc += $needed_wspanners * $tmp_price;
	$normal_price_acc += $needed_wspanners * $wspanner->normal_price;
}

$needed_ibraces 	= request('needed_ibraces');
if ($ibrace = getMyo($db_table_products, "category_id='$ibraces_category'", array('price','sale_price','on_sale','normal_price'))) {
	$tmp_price = $ibrace->on_sale ? $ibrace->sale_price : $ibrace->price;
	$price_acc += $needed_ibraces * $tmp_price;
	$normal_price_acc += $needed_ibraces * $ibrace->normal_price;
}

$needed_eputubes 	= request('needed_eputubes');
if ($eputube = getMyo($db_table_products, "category_id='$eputubes_category'", array('price','sale_price','on_sale','normal_price'))) {
	$tmp_price = $eputube->on_sale ? $eputube->sale_price : $eputube->price;
	$price_acc += $needed_eputubes * $tmp_price;
	$normal_price_acc += $needed_eputubes * $eputube->normal_price;
}

$needed_boutenmoer  = request('needed_boutenmoer');
if ($boutenmoer = getMyo($db_table_products, "category_id='$boutmoer_category'", array('price','sale_price','on_sale','normal_price'))) {
	$tmp_price = $boutenmoer->on_sale ? $boutenmoer->sale_price : $boutenmoer->price;
	$price_acc += $needed_boutenmoer * $tmp_price;
	$normal_price_acc += $needed_boutenmoer * $boutenmoer->normal_price;
}

$needed_polecaps 	= request('needed_polecaps');
if ($polecap = getMyo($db_table_products, "category_id='$polecaps_category'", array('price','sale_price','on_sale','normal_price'))) {
	$tmp_price = $polecap->on_sale ? $polecap->sale_price : $polecap->price;
	$price_acc += $needed_polecaps * $tmp_price;
	$normal_price_acc += $needed_polecaps * $polecap->normal_price;
}

$description['Accessoires'][] 	= array('Bovenbuizen'			=>	$needed_tubes);
$description['Accessoires'][]	= array('Koppelmof bovenbuis'	=>	$needed_mofs);
$description['Accessoires'][]	= array('Kopstuk bovenbuis'		=>	$needed_heads);
$description['Accessoires'][] 	= array('Spandraad op rollen'	=>	$needed_wire);
$description['Accessoires'][] 	= array('Binddraad op rollen'	=>	$needed_bwire);
$description['Accessoires'][] 	= array('Draadspanners'			=>	$needed_wspanners);
$description['Accessoires'][] 	= array('Inoxbeugel'			=>	$needed_ibraces);
$description['Accessoires'][] 	= array('Eindstukken bovenbuis'	=>	$needed_eputubes);
$description['Accessoires'][] 	= array('Bout + moer'			=>	$needed_boutenmoer);
$description['Accessoires'][] 	= array('Paaldoppen'			=>	$needed_polecaps);

//Update some description fields
$description['Lengte gaas'] 		= $total_length.' m';
$description['Rollen benodigd'] 	= $rolls.' stuks ('.$step.' m/rol)';

//Price is per roll
$price = ($price * $rolls) + $price_pole + $price_gate + $price_acc;
$normal_price = ($normal_price * $rolls) + $normal_price_pole + $normal_price_gate + $normal_price_acc;

//Item
$item = array(	'amount'		=>	1, //ik denk gewoon altijd 1 laten, want het is toch een maatwerk pakket
				'price'			=>	$price,
				'description'	=>	$description);

if (isset($_GET['ajax_calc_price'])) {
	echo ajax_price_block($price, $normal_price);
	exit;
}
?>