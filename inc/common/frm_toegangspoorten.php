<?php

/******************************************************************************************
 * Toegangspoorten formulier
 ***/

//Configuration
require_once('inc/common/cfg_toegangspoorten.php');

//AJAX page
$ajax_page					=	'inc/common/ajx_toegangspoorten.php';

//Text elements
$info['INFO_GATES_AMOUNT']		=	INFO_GATES_AMOUNT;
$info['INFO_GATES_WIDTH']		=	INFO_GATES_WIDTH;
$info['INFO_GATES_HEIGHT']		=	INFO_GATES_HEIGHT;
$info['INFO_GATES_COLOR']		=	INFO_GATES_COLOR;

/******************************************************************************************
 * Height selection
 ***/

//Read available heights
if ($width = request('width', $vk_width)) {
	$available_heights = array(0 => 'Maak uw keuze');
	$res_h = eq("SELECT height FROM $db_table_products WHERE category_id = '$category_id' AND width='$width' GROUP BY height;");
	while ($height = mfo($res_h)) {
		$available_heights[$height->height] = (int) $height->height;
	}
	asort($available_heights);
}
else {
	$available_heights = array(0 => 'Kies eerst de breedte');
}

//Height selector
$height_selector = makeSelectBox(	'height', $available_heights, request('height',$vk_height), 0,
									"class='selectMed' onchange='get_colors();'");

/******************************************************************************************
 * Width selection
 ***/

//Read available widths
$available_widths = array(0 => 'Maak uw keuze');
$res_h = eq("SELECT width FROM $db_table_products WHERE category_id = '$category_id' GROUP BY width;");
while ($width = mfo($res_h)) {
	$available_widths[$width->width] = (int) $width->width;
}
asort($available_widths);

//Width selector
$width_selector = makeSelectBox(	'width', $available_widths, request('width',$vk_width), 0,
									"class='selectMed' onchange='get_heights();'");

/******************************************************************************************
 * Scripts
 ***/

$script = "
	<script type='text/javascript'>

		//Initialize vars
		var width	=	0;
		var	height	=	0;
		var color	=	0;
		var	pieces	=	0;

		//Initial HTML of total price block
		var initial	=	$('#price_block').html();

		//Read form vars
		function read_vars() {

			//Read width, height, pieces
			width	=	$('#width').val();
			height	=	$('#height').val();
			pieces	=	$('#pieces').val();
			color	=	0;

			//No pieces? Pretend 1 piece
			if (pieces == 0) {
				pieces = 1;
			}

			//Read color
			if ($('input[name=\"color\"]').length) {
				color	=	$('input[name=\"color\"]:checked').val();
			}
		}

		//Initialization
		function initialize() {

			//Read vars
			read_vars();

			//Do we have a width, but no height? Get the heights
			if (width > 0 && height == 0) {
				get_heights();
			}

			//Do we have a width and a height? Get the colors
			else if (width > 0 && height > 0) {
				get_colors();
			}
		}

		//Calculate the total price
		function calc_price(why) {

			//alert('Calculating...' + why);

			//Read the latest vars
			read_vars();

			//We need these vars minimally
			if (height == 0 || width == 0 || color == 0) {
				$('#price_block').html(initial);
				$('#price_block_2').html(initial);
				return;
			}

			//Send the price rquest
			$.get(	'ajaxhond.php?ajax_calc_price&category_id=$category_id&height=' + height + '&width=' + width + '&color=' + color
					+ '&pieces=' + pieces,
				function(html) {
					$('#price_block').html(html);
					$('#price_block_2').html(html);
				}, 'html'
			);
		}

		//Get the heights
		function get_heights() {

			//Read the latest vars
			read_vars();

			//Oops, no more width?
			if (width == 0) {
				$('#color_select').html('Kies eerst de breedte en de hoogte<br/><br/><br/><br/><br/>');
			}

			$.get('$ajax_page?get_heights&category_id=$category_id&width=' + width,
				function(html) {
					$('#height_selector').html(html);
					calc_price('heights_received');
				}, 'html'
			);
		}

		//Get the colors
		function get_colors() {

			//Read the latest vars
			read_vars();

			$.get('$ajax_page?colors&category_id=$category_id&height=' + height + '&width=' + width,
				function(html) {
					$('#color_select').html(html);
					calc_price('colors_received');
				}, 'html'
			);
		}
	</script>
";

/******************************************************************************************
 * Form contents
 ***/

$missing['width'] 	= isset($missing['width']) ? "class='foutmelding'" : '';
$missing['height'] 	= isset($missing['height']) ? "class='foutmelding'" : '';
$missing['pieces'] 	= isset($missing['pieces']) ? "class='foutmelding'" : '';
$pieces = request('pieces', 1);

$form_contents = <<<FORM
	$script
	<div class="blok"><img src="gfx/content/txt/stel_uw_toegangspoort_samen.gif" alt="Stel uw toegangspoort samen!" title="Stel uw toegangspoort samen!" /></div>
	$error
	<div class="blok">
		<h2>Uw wensen</h2>
		<div class="table">
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="140" height="22" align="left" valign="middle" $missing[pieces]>Aantal stuks:</td>
					<td width="15" align="left" valign="middle">*</td>
					<td width="180" align="left" valign="middle"><input type="text" id='pieces' name="pieces" class="formMed" value='$pieces' maxlength='4' onkeyup='calc_price("pieces_changed");'/></td>
					<td align="left" valign="top">
						<div class="info" id="paneelLengte" onmouseover="displayDiv('paneelLengte', 1);" onmouseout="displayDiv('paneelLengte', 0);" style="display:none;">
							$info[INFO_GATES_AMOUNT]
						</div>
						<a onmouseover="displayDiv('paneelLengte', 1); return false;" onmouseout="displayDiv('paneelLengte', 0);" class="infoIcon"><img src="gfx/layout/nav/info.gif" alt="" title="" style="margin:3px 0 0;" /></a>
					</td>
				</tr>
				<tr>
					<td width="140" height="22" align="left" valign="middle" $missing[width]>Breedte poort:</td>
					<td width="15" align="left" valign="middle">*</td>
					<td width="180" align="left" valign="middle">
						$width_selector
					</td>
					<td align="left" valign="top">
						<p style="padding:4px 10px 0 0; float:left;">mm</p>
						<div class="info" id="poortBreedte" onmouseover="displayDiv('poortBreedte', 1);" onmouseout="displayDiv('poortBreedte', 0);" style="display:none;">
							$info[INFO_GATES_WIDTH]
						</div>
						<a onmouseover="displayDiv('poortBreedte', 1); return false;" onmouseout="displayDiv('poortBreedte', 0);" class="infoIcon"><img src="gfx/layout/nav/info.gif" alt="" title="" style="margin:3px 0 0;" /></a>
					</td>
				</tr>
				<tr>
					<td width="140" height="22" align="left" valign="middle" $missing[height]>Hoogte poort:</td>
					<td width="15" align="left" valign="middle">*</td>
					<td width="180" align="left" valign="middle" id='height_selector'>
						$height_selector
					</td>
					<td align="left" valign="top">
						<p style="padding:4px 10px 0 0; float:left;">mm</p>
						<div class="info" id="poortHoogte" onmouseover="displayDiv('poortHoogte', 1);" onmouseout="displayDiv('poortHoogte', 0);" style="display:none;">
							$info[INFO_GATES_HEIGHT]
						</div>
						<a onmouseover="displayDiv('poortHoogte', 1); return false;" onmouseout="displayDiv('poortHoogte', 0);" class="infoIcon"><img src="gfx/layout/nav/info.gif" alt="" title="" style="margin:3px 0 0;" /></a>
					</td>
				</tr>
			</table>
		</div>
		<div class="subtableDivider">&nbsp;</div>
	</div>
	<div class='blok'>
		<h2>Kleur van de poort</h2>
		<div class='table' id='color_select'>Kies eerst de breedte en de hoogte<br/><br/><br/><br/><br/></div>
		<div class='subtableDivider'>&nbsp;</div>
	</div>
	<script type='text/javascript'>
		initialize();
	</script>
FORM;

?>