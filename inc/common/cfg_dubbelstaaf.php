<?php

/******************************************************************************************
 * Dubbelstaaf configuration
 ***/

//Definitions
define('BRACES_PER_POLE',				3);
define('CLIPS_PER_CORNER',				3);
define('CONNECTIONS_PER_CONNECTION',	3);

//Show the ruler
$show_ruler = true;

//Multiplier for mid braces per fence height
$mid_braces_multipliers = array(
	630		=>	6,
	830		=>	6,
	1030	=>	6,
	1230	=>	6,
	1430	=>	6,
	1630	=>	6,
	1830	=>	8,
	2030	=>	8
);

//Base values for end braces per fence height (no correlation with previous array I believe, this just seems to be coincidence)
$end_braces_multipliers = array(
	630		=>	6,
	830		=>	6,
	1030	=>	6,
	1230	=>	6,
	1430	=>	6,
	1630	=>	6,
	1830	=>	8,
	2030	=>	8
);

//Gate categories
$gate_categories			=	array(
	//Usage: product category => array(of gate categories),
	86	=>	array(89)
);

//Gate height map
$gate_height_map = array(
	//Usage: gate category => array(height fence => height gate)

	//Dubbelstaaf poorten
	89	=>	array(
		1030	=>	1000,
		1230	=>	1200,
		1430	=>	1400,
		1630	=>	1600,
		1830	=>	1800,
		2030	=>	2000
	)
);

//Categories
$mid_braces_category	= 	95;
$end_braces_category	= 	96;
$polecaps_category 		=	90;
$connection_category 	= 	10;		//Muurbevestigingen
$boutmoer_category 		= 	93;

//Poles
$pole_categories	=	array(
	86	=>	88,						//Palen te betonneren
);

//Poles
$poles_options = array(
	0	=>	'Geen palen.',
	1	=>	'In beton.'
);

//Pole height map
$pole_height_map = array(
	//Usage: pole category => array(height fence => height pole)

	//Dubbelstaaf palen
	88	=>	array(
		630		=>	1850,
		830		=>	1850,		
		1030	=>	1850,
		1230	=>	2050,
		1430	=>	2300,
		1630	=>	2500,
		1830	=>	2800,
		2030	=>	2800
	)
);

?>