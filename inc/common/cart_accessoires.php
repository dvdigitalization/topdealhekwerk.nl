<?php

/******************************************************************************************
 * Cart filler for accessoires
 ***/

//Amount
$amount = request('amount', 1);
$amount = 	$amount > 0 ? $amount : 1;

//Identifier
$identifier = md5($category_id);

//Price determination
$res_p = eq("	SELECT price,sale_price,on_sale,normal_price FROM $db_table_products
				WHERE active = '1' AND category_id = '$category_id' ORDER BY price ASC LIMIT 1;");
$product = mfo($res_p);
if ($product) {
	$price 			= 	$product->on_sale ? $product->sale_price : $product->price;
	$normal_price	=	$product->normal_price;
}
else {
	$dont_add = true;
	return;
}

//Item
$item = array(	'amount'		=>	$amount,
				'price'			=>	$price,
				'description'	=>	array(	'Aantal'		=> $amount.' stuk(s)'));

if (isset($_GET['ajax_calc_price'])) {
	echo ajax_price_block($price*$amount, $normal_price*$amount);
	exit;
}
?>