<?php

/******************************************************************************************
 * Toegangspoorten AJAX handler
 ***/

//Basic functionality
chdir('../../');
require_once('inc/site_ajax_header.php');

//Configuration
require_once('inc/common/cfg_toegangspoorten.php');

//Vat
$vat_multiplier			=	(100 + $cfg['PRODUCTS_STANDARD_VAT'])/100;

//Settings
$gfx_folder				=	'gfx'.$_sep.'categories';
$cat_prefix				=	'cat_';
$prod_prefix			=	'product_';
$thumb_prefix			=	'thumb_';
$cat_re_prefix			=	'categorie';
$prod_re_prefix			=	'product';

//Get lowest factor to use for prices
$factor					=	getMyo('digi_topdeal_discounts', '', 'multiplier', 'multiplier', 'ASC', 1);

//Database tables
$db_table_cats			=	DIGI_DB_PREFIX.'topdeal_categories';
$db_table_products		=	DIGI_DB_PREFIX.'topdeal_products';
$db_table_colors		=	DIGI_DB_PREFIX.'topdeal_colors';
$db_table_props			=	DIGI_DB_PREFIX.'topdeal_properties';
$db_table_prop_values	=	DIGI_DB_PREFIX.'topdeal_prop_values';

if (isset($_GET['get_heights']) and $category_id = (int) $_GET['category_id']) {

	$width 	= (int) $_GET['width'];
	if (!$width) {
 		$available_heights = array(0 => 'Kies eerst de breedte');
	}
	else {

		//Read available heights
		$available_heights = array(0 => 'Maak uw keuze');
		$res_h = eq("SELECT height FROM $db_table_products WHERE category_id = '$category_id' AND width='$width' GROUP BY height;");
		while ($height = mfo($res_h)) {
			$available_heights[$height->height] = (int) $height->height;
		}
		asort($available_heights);
	}

	//Height selector
	echo makeSelectBox(	'height', $available_heights, request('height',0), 0,
						"class='selectMed' onchange='get_colors();'");
}

//Get available gaas colors
if (isset($_GET['colors']) and
	isset($_GET['width']) and $width = (int) $_GET['width'] and
	isset($_GET['height']) and $height = (int) $_GET['height'] and
	isset($_GET['category_id']) and $category_id = (int) $_GET['category_id']) {

	//Available colors
	$available_colors = array();
	$res_c = eq("	SELECT $db_table_colors.name, $db_table_colors.id FROM $db_table_colors
					LEFT JOIN $db_table_products ON ($db_table_colors.id = color_id)
					WHERE $db_table_products.active = '1' AND category_id = '$category_id'
					AND $db_table_products.width = '$width' AND $db_table_products.height = '$height'
					GROUP BY color_id
					ORDER BY $db_table_colors.name ASC
	");

	while ($color = mfo($res_c)) {
		$available_colors[$color->id] = $color->name;
	}

	$color_selector = '';
	unset($checked);

	$colors = array();
	$c = 0;
	foreach ($available_colors as $color_id => $color_name) {
		$checked = isset($checked) ? '' : "checked='checked'";
		$c++;
		$available_colors[$color_id] = "
			<div class='kleurstaal'>
				<label for='kleur_$c'>
					<img src='gfx/colors/color_$color_id.jpg' alt='$color_name' title='$color_name' />
					<input type='radio' name='color' id='kleur_$c' $checked value='$color_id'/> <span>$color_name</span>
				</label>
			</div>
		";
	}
	$color_selector = implode("\n",$available_colors);

	echo $color_selector;
	exit;
}
elseif (isset($_GET['colors'])) {
	echo "Kies eerste de breedte en hoogte van de poort<br/><br/><br/><br/><br/>";
	exit;
}
?>