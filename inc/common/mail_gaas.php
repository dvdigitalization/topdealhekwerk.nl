<?php

/**
 * Mail contents - Gaas
 */

if (!isset($_POST['color'])) {
	$_POST['color'] = 0;
}

// *** Pole selection
if ($_POST['poles'] and isset($_POST['pole']) and $pole_id = (int) $_POST['pole']) {
	$res_p = eq("	SELECT 	name, menu_name, width
					FROM 	$db_table_products
					LEFT JOIN $db_table_cats ON (category_id = $db_table_cats.id)
					WHERE 	$db_table_products.id = '$pole_id'
	");
	$pole		=	mfo($res_p);
	$pole->name = 	$pole->menu_name ? $pole->menu_name : $pole->name;
}

// *** Gates
$chosen_gates 		= '';
$amount_of_gates 	= 0;
foreach ($_POST as $key => $amount) {

	if (substr($key,0,6) != 'gates_' or !$amount) {
		continue;
	}
	$gate_id 	= 	substr($key,6);
	$res_g = eq("	SELECT 	name, menu_name, width
					FROM 	$db_table_products
					LEFT JOIN $db_table_cats ON (category_id = $db_table_cats.id)
					WHERE 	$db_table_products.id = '$gate_id'
	");
	$gate		=	mfo($res_g);
	$gate->name = 	$gate->menu_name ? $gate->menu_name : $gate->name;
	$amount_of_gates++;

	$gate_color = getMyo($db_table_colors,$_POST['gate_color_'.$gate_id],'name');

	$chosen_gates .= "
		<tr>
			<td height='5' colspan='2'></td>
		</tr>
		<tr>
			<td width='250' align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; font-weight:bold; color:#6e8778;'><em>$gate->name</em></td>
			<td align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000;'>&nbsp;</td>
		</tr>
		<tr>
			<td width='250' align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000;'>Aantal</td>
			<td align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000;'>$amount</td>
		</tr>
		<tr>
			<td width='250' align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000;'>Breedte</td>
			<td align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000;'>$gate->width mm</td>
		</tr>
		<tr>
			<td width='250' align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000;'>Kleur</td>
			<td align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000;'>$gate_color</td>
		</tr>
	";
}

$contents = "
	<!-- Offerte Gaas -->
	<tr>
		<td height='15'></td>
	</tr>
	<tr>
		<td align='left' valign='top'>
			<table width='100%' cellpadding='0' cellspacing='0' border='0'>
				<tr>
					<td align='left' valign='middle' style='padding:3px 0 0; font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000; font-weight:bold;'>$category->name</td>
				</tr>
				<tr>
					<td height='5'></td>
				</tr>
				<tr>
					<td height='5' style='border-top:1px solid #d7d7d7; font-size:1px;'>&nbsp;</td>
				</tr>
			</table>
			<table width='100%' cellpadding='0' cellspacing='0' border='0'>
				<tr>
					<td align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000; font-weight:bold;' colspan='2'>Uw wensen</td>
				</tr>
				<tr>
					<td height='5' colspan='2'></td>
				</tr>
				<tr>
					<td width='250' align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000;'>Lengte gaas</td>
					<td align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000;'>$_POST[length] m</td>
				</tr>
				<tr>
					<td width='250' align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000;'>Hoogte gaas</td>
					<td align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000;'>$_POST[height] mm</td>
				</tr>
				<tr>
					<td height='5' colspan='2'></td>
				</tr>
				<tr>
					<td height='5' style='border-top:1px solid #d7d7d7; font-size:1px;' colspan='2'>&nbsp;</td>
				</tr>
			</table>
			<table width='100%' cellpadding='0' cellspacing='0' border='0'>
				<tr>
					<td align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000; font-weight:bold;' colspan='2'>Kleur van het hekwerk</td>
				</tr>
				<tr>
					<td height='5' colspan='2'></td>
				</tr>
				<tr>
					<td width='250' align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000;'>".getMyo($db_table_colors,$_POST['color'],'name')."</td>
					<td align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000;'>&nbsp;</td>
				</tr>
				<tr>
					<td height='5' colspan='2'></td>
				</tr>
				<tr>
					<td height='5' style='border-top:1px solid #d7d7d7; font-size:1px;' colspan='2'>&nbsp;</td>
				</tr>
			</table>
			<table width='100%' cellpadding='0' cellspacing='0' border='0'>
				<tr>
					<td align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000; font-weight:bold;' colspan='2'>Bevestiging gaas</td>
				</tr>
				<tr>
					<td height='5' colspan='2'></td>
				</tr>
				<tr>
					<td width='250' align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000;'>".(!$_POST['poles'] ? "Geen bevestigingspalen" : "Bevestigingspalen")."</td>
					<td align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000;'>&nbsp;</td>
				</tr>
				<tr>
					<td height='5' colspan='2'></td>
				</tr>
				".(($_POST['poles'] and isset($pole) and $pole) ? "
				<tr>
					<td width='250' align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; font-weight:bold; color:#6e8778;'><em>$pole->name</em></td>
					<td align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000;'>&nbsp;</td>
				</tr>
				<tr>
					<td height='5' colspan='2'></td>
				</tr>
				" : '')."
				<tr>
					<td height='5' style='border-top:1px solid #d7d7d7; font-size:1px;' colspan='2'>&nbsp;</td>
				</tr>
			</table>
			<table width='100%' cellpadding='0' cellspacing='0' border='0'>
				<tr>
					<td align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000; font-weight:bold;' colspan='2'>Toegangscontrole</td>
				</tr>
				<tr>
					<td height='5' colspan='2'></td>
				</tr>
				".(!$_POST['gates'] ? "
				<tr>
					<td width='250' align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000;'>Geen toegangspoort</td>
					<td align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000;'>&nbsp;</td>
				" : "
				<tr>
					<td width='250' align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000;'>Toegangspoorten</td>
					<td align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000;'>&nbsp;</td>
				</tr>
				<tr>
					<td height='5' colspan='2'></td>
				</tr>
				<tr>
					<td height='5' style='border-top:1px solid #d7d7d7; font-size:1px;' colspan='2'>&nbsp;</td>
				</tr>
				<tr>
					<td align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; color:#000000; font-weight:bold;' colspan='2'>Gekozen poort".($amount_of_gates > 1 ? 'en' : '')."</td>
				</tr>
				$chosen_gates
				")."
				<tr>
					<td height='5' style='border-top:1px solid #d7d7d7; font-size:1px;' colspan='2'>&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	<!-- Einde Offerte Gaas -->
";

?>