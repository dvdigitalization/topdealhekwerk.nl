<?php
/*
Title:		Site tags for simple shop modules
File: 		inc/site_tags_simple_shop.php
Version: 	v2.20
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

//View product template and tags
if ($template == 'static/shop_product.html') {

	$html = str_replace('[PRODUCT_NAME]',			$p_name,			$html);
	$html = str_replace('[PRODUCT_DESCRIPTION]',	$p_description,		$html);
	$html = str_replace('[PRODUCT_IMAGE]',			$p_image,			$html);
	$html = str_replace('[PRODUCT_PRICE]',			$p_price,			$html);
	$html = str_replace('[PRODUCT_URL]',			$p_url,				$html);
	$html = str_replace('[PRODUCT_NO]',				$p_product_no,		$html);
	$html = str_replace('[PRODUCT_STOCK]',			$p_stock,			$html);
	$html = str_replace('[CATEGORY_NAME]',			$c_name,			$html);
	$html = str_replace('[CATEGORY_URL]',			$c_url,				$html);
	$html = str_replace('[CART_LINK]',				$cart_link,			$html);
}

elseif ($template == 'static/shop_category.html') {

	$html = str_replace('[CATEGORY_NAME]',			$c_name,			$html);
	$html = str_replace('[CATEGORY_DESCRIPTION]',	$c_description,		$html);
	$html = str_replace('[CATEGORY_IMAGE]',			$c_image,			$html);
	$html = str_replace('[CATEGORY_URL]',			$c_url,				$html);
	$html = str_replace('[CATEGORY_PRODUCTS]',		$c_products,		$html);
}

?>