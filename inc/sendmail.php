<?php
/*
Title:		Standard sendmail function for various purposes of mail sending
File: 		inc/sendmail.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Required PHP Mailer class file
if (defined('CMS_PATH_PREFIX') and file_exists(CMS_PATH_PREFIX.'apps/phpmailer/class.phpmailer.php')) {
	require_once(CMS_PATH_PREFIX.'apps/phpmailer/class.phpmailer.php');
}
elseif (defined('SITE_PATH_PREFIX') and file_exists(SITE_PATH_PREFIX.'apps/phpmailer/class.phpmailer.php')) {
	require_once(SITE_PATH_PREFIX.'apps/phpmailer/class.phpmailer.php');
}
else {
	echo '*** PHP MAILER HAS NOT BEEN INSTALLED ***';
	exit;
}

// *** The sendMail function
function sendMail(	$to, $subject, $message, $from = '', $attachment_name = '', $attachment_url = '',
					$is_html = false, $use_template = false, $override_from = false) {

 	global $cfg, $charset, $Message, $_link;

 	//To field validation
 	if (is_string($to)) {
		$to = array($to);
	}
	if (!is_array($to)) {
		return;
	}

	//Recipients cleansing
	$recipients = array();
	foreach ($to as $recipient) {
		$recipient = trim($recipient);
		if ($recipient and !in_array($recipient, $recipients)) {
			$recipients[] = $recipient;
		}
	}

	//From determination
	if (!$from) {
		//If the from field was not specified, we use the site default
		$from 		= 	reverseHtmlEntities($cfg['SITE_EMAIL_NAME']).' <'.$cfg['SITE_EMAIL_ADDRESS'].'>';
	}
	elseif (!$override_from) {
		//If we want dont want to totally override the from field, we assume we received just an email addres and use the site name
		$from		=	reverseHtmlEntities($cfg['SITE_EMAIL_NAME']).' <'.$from.'>';
	}
	else {
		//If we do want to override, we just use the value we got
		$from		=	$from;
	}

	//Do we intend to use a template?
	if (!$is_html and $use_template) {
		if (defined('CMS_PATH_PREFIX') and file_exists(CMS_PATH_PREFIX.'template/email_template.html')) {

			//Get layout from template and make replacements
			$email_html = file_get_contents(CMS_PATH_PREFIX.'template/email_template.html');
			$email_html = str_replace('[SUBJECT]',	$subject,	$email_html);
			$email_html = str_replace('[LINK]',		$_link,		$email_html);
			$message 	= str_replace('[MESSAGE]',	$message,	$email_html);

		}
		elseif (defined('SITE_PATH_PREFIX') and file_exists(SITE_PATH_PREFIX.'template/email_template.html')) {

			//Get layout from template and make replacements
			$email_html = file_get_contents(SITE_PATH_PREFIX.'template/email_template.html');
			$email_html = str_replace('[SUBJECT]',	$subject,	$email_html);
			$email_html = str_replace('[LINK]',		$_link,		$email_html);
			$message 	= str_replace('[MESSAGE]',	$message,	$email_html);
		}
		else {
			$use_template = false;
		}
	}

 	//Default email layout if no HTML received and no template usage specified (or if template failed to load)
 	if (!$is_html and !$use_template) {
		$message = "
			<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
			<html xmlns='http://www.w3.org/1999/xhtml'>
			<head>
			<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
			<style type='text/css'>
			body,p,td{font-family:verdana;font-size: 11px;color:#000000;}
			</style>
			</head>
			<body style='font-family:verdana;font-size: 11px;color:#000000;'>
			$message
			</body>
			</html>
		";
	}

    $mail 				= 	new PHPMailer();
    $mail->CharSet		= 	$charset;
	$mail->From 		= 	$from;
    $mail->Subject 		= 	reverseHtmlEntities($subject);
    $mail->Body 		= 	$message;
    $mail->IsHTML(true);

    if ($cfg['USE_SMTP']) {
	    $mail->IsSMTP();
    	$mail->Host			=	$cfg['SMTP_SERVER'];
    }

     //Attachment(s)?
    if (!empty($attachment_url)) {

    	if (is_array($attachment_url)) {
			foreach ($attachment_url as $key => $url) {
				if(file_exists($url) and filesize($url) > 0) {
					$mail->AddAttachment($url,$attachment_name[$key],'base64',filetype($url));
				}
				elseif(file_exists('../'.$url) and filesize('../'.$url) > 0) {
					$mail->AddAttachment('../'.$url,$attachment_name[$key],'base64',filetype('../'.$url));
				}
			}
		}
    	else {
			if(file_exists($attachment_url) and filesize($attachment_url) > 0) {
				$mail->AddAttachment($attachment_url,$attachment_name,'base64',filetype($attachment_url));
			}
			elseif(file_exists('../'.$attachment_url) and filesize('../'.$attachment_url) > 0) {
				$mail->AddAttachment('../'.$attachment_url,$attachment_name,'base64',filetype('../'.$attachment_url));
			}
		}
	}

	//Send
	foreach ($recipients as $email) {
		$mail->AddAddress($email);
		if (!$mail->Send()) {
			$mail_error = $mail->ErrorInfo;
		}
		$mail->ClearAddresses();
	}

	if (isset($mail_error) and defined('CMS_PATH_PREFIX')) {
		$Message->set(DIGI_ERR_MAILER.'<br/><br/>'.$mail_error, CRITICAL);
	}
}
?>