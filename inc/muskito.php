<?php

function muskito_api($action, $extra_data = array()) {

	//Check if curl exists
	if (!function_exists('curl_init')) {
		trigger_error('cURL not installed!');
		return false;
	}
	
	//Must have email
	if (!isset($extra_data['email'])) {
		trigger_error('No e-mail given!');
		return false;
	}

	//cURL initializeren
	$ch = curl_init(); 

	//URL naar muskito mail API zetten
	curl_setopt($ch, CURLOPT_URL, 'http://mijn.muskitomail.nl/process/api.php');

	//Aangeven dat er een string moet worden teruggegeven en dat de data met POST gestuurd moet worden
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	curl_setopt($ch, CURLOPT_POST, 1);

	//De POST data initializeren
	$data = array(
		'request'			=>	$action,											//Opt-in of opt-out
		'customer'			=>	51,													//De klant ID
		'customer_hash'		=>	'7f69829c2842a0094fbdd9d908522480d57b02a8',			//De klant hash voor controle
		'group_id'			=>	269,												//Het groeps ID voor aanmelden of afmelden (bij afmelden kan ook 0 voor alle groepen)
		'group_id_hash'		=>	'd8cf4ad451e25ac2c92f424b60ff3655d5602685',			//Groeps ID hash voor controle
		'redirect_veri'		=>	'',							//Redirect voor NA klikken op de opt-in link	
	);

	//De rest van de velden voor aanmelden
	$data = array_merge($extra_data, $data);

	//Data aan curl doorgeven
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

	//Verzoek uitvoeren
	$xml = curl_exec($ch);
	curl_close($ch);

	//XML resultaat inlezen en omzetten naar array
	$xml = new SimpleXMLElement($xml);
	$xml = (array) $xml;
	
	//XML verder analyseren indien gewenst
	return $xml;
}
?>