<?php
/*
Title:		Sorting and page numbers management file
File: 		sorting_and_pages.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

//Set vars
$secondary_sort = '';

//Primary sorting option
if (isset($_POST['sort']) and validateAllowedValue($_POST['sort'], $valid_sort_options)) 	$sort	=	$_POST['sort'];
elseif (isset($_GET['sort']) and validateAllowedValue($_GET['sort'], $valid_sort_options)) 	$sort	=	$_GET['sort'];
else 																						$sort	=	$default_sort_option;

//Only for default sorting, check if we have a secondary sort option
if ($sort == $default_sort_option) {
	if (!empty($secondary_sort_option) and !empty($secondary_sort_method)) {
		$secondary_sort = ', '.$secondary_sort_option.' '.$secondary_sort_method;
	}
}

//Grouping
if (!isset($group_clause)) {
	$group_clause = '';
}

//Primary sorting methods
if (isset($_POST['method']) and ($_POST['method'] == 'ASC' or $_POST['method'] == 'DESC')) 	$method	=	$_POST['method'];
elseif (isset($_GET['method']) and ($_GET['method'] == 'ASC' or $_GET['method'] == 'DESC')) $method	=	$_GET['method'];
else 																						$method	=	$default_sort_method;

//Page determination
if (isset($_POST['page']) and $page_nr = (int) $_POST['page'])		$page	=	$page_nr;
else if (isset($_GET['page']) and $page_nr = (int) $_GET['page'])	$page	=	$page_nr;
else																$page	=	1;

//Set some vars
$new_method 		= 	($method == 'ASC') ? 'DESC' : 'ASC';
$sort_appendix		= 	"sort=$sort&method=$method";
$page_appendix		= 	"page=$page";
$items_per_page 	= 	$cfg['ITEMS_PER_PAGE'];
$max_pages_in_list 	= 	$cfg['MAX_PAGES_IN_LIST'];

//Set the where clause

if (is_array($where) and count($where)) {
	$where_clause	=	'WHERE '.implode(' AND ', $where);
}
elseif (!empty($where)) {
	$where_clause	=	'WHERE '.$where;
}
else {
	$where_clause 	= 	'';
}

if (!isset($join_clause)) {
	$join_clause = '';
}

//Calculate some parameters
$total_items 		= 	countResults($db_table, $where_clause, $group_clause, '*', $join_clause);
$mysql_limit_start 	= 	($page - 1) * $items_per_page;
$pages_needed 		= 	ceil($total_items / $items_per_page);

//Set MySQL query parameters
$order_clause		=	$sort.' '.$method.$secondary_sort;
$limit_clause		=	$mysql_limit_start.', '.$items_per_page;

//Define vars
$first = $last = '';

//More than one page needed?
if ($pages_needed > 1) {

	//If the total amount of pages exceeds the max amount of pages to show in the list...
	if ($pages_needed > $max_pages_in_list) {

		//Define lower and upper limits
		$lower_limit = ceil($max_pages_in_list / 2);
	    $upper_limit = $pages_needed - floor($max_pages_in_list / 2);

	    //If the page we are on is between the limits...
		if ($page > $lower_limit and $page < $upper_limit) {
	        $pages[] = "..";
	        for ($x = ($page - floor($max_pages_in_list/2)); $x <= ($page + floor($max_pages_in_list/2)); $x++) {
	            if ($page != $x) $pages[] = "<a href='{$_file}?page=$x&$sort_appendix&$link_appendix'>$x</a>";
	            else $pages[] = "<u>$x</u>";
	        }
	        $pages[] = "..";
	    }

	    //If it is below or equal to the lower limit...
	    elseif ($page <= $lower_limit) {
	        for ($x = 1; $x <= $max_pages_in_list; $x++) {
	            if ($page != $x) $pages[] = "<a href='{$_file}?page=$x&$sort_appendix&$link_appendix'>$x</a>";
	            else $pages[] = "<u>$x</u>";
	        }
	        $pages[] = "..";
	    }

	    //If it is above or equal to the upper limit...
	    elseif ($page >= $upper_limit) {
	        $pages[] = "..";
	        for ($x = ($pages_needed - $max_pages_in_list + 1); $x <= $pages_needed; $x++) {
	            if ($page != $x) $pages[] = "<a href='{$_file}?page=$x&$sort_appendix&$link_appendix'>$x</a>";
	            else $pages[] = "<u>$x</u>";
	        }
	    }

		//Last
		if ($page != $pages_needed)	$last	=	"&nbsp;<a href='{$_file}?page=$pages_needed&$sort_appendix&$link_appendix'>&raquo;&raquo;</a>";
		else						$last	=	"&nbsp;<span class='grey'>&raquo;&raquo;</span>";

		//First
		if ($page != 1)				$first	=	"<a href='{$_file}?page=1&$sort_appendix&$link_appendix'>&laquo;&laquo</a>&nbsp;";
		else						$first	=	"<span class='grey'>&laquo;&laquo</span>&nbsp;";
	}

	//If it doesn't exceed the max amount of pages...
	else {

		//Fill array with links to the required pages
		for ($x = 1; $x <= $pages_needed; $x++) {
		    if ($x != $page) 		$pages[] = "<a href='{$_file}?page=$x&$sort_appendix&$link_appendix'>$x</a>";
		    else 					$pages[] = "<u>$x</u>";
		}
	}

	//Next
	if ($page != $pages_needed) 	$next = $page+1;
	else							$next = 0;

	if ($next) 						$next = "&nbsp;<a href='{$_file}?page=$next&$sort_appendix&$link_appendix'>&raquo;</a>";
	else 							$next = "&nbsp;<span class='grey'>&raquo;</span>";

	//Prev
	if ($page != 1) 				$prev = $page-1;
	else							$prev = 0;

	if ($prev) 						$prev = "<a href='{$_file}?page=$prev&$sort_appendix&$link_appendix'>&laquo;</a>&nbsp;";
	else 							$prev = "<span class='grey'>&laquo;</span>&nbsp;";


	//HTML to use these pages
	$pages_html = "<h6 class='pages_list'>$first $prev ".implode(" ",$pages)." $next $last</h6>";
}
else {
	$pages_html = '';
}
?>