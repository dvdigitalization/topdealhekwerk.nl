<?php
/*
Title:		Dynamic array definitions
File: 		inc/arrays_dynamic.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Employees array	
$EMPLOYEES_ARRAY = array();
$EMPLOYEES_ALL_ARRAY = array();

$res = eq("	SELECT 		id,first_name,last_name,deleted,allow_login
			FROM 		".DIGI_DB_PREFIX."auth_users
			WHERE 		is_admin = '0' AND use_in_administration = '1'
			ORDER BY 	id ASC
;");
while($myo = mfo($res)) {
	if ($myo->allow_login == 1 and $myo->deleted == 0) {
		$EMPLOYEES_ARRAY[$myo->id] = $myo->first_name.' '.$myo->last_name;
	}	
	$EMPLOYEES_ALL_ARRAY[$myo->id] = $myo->first_name.' '.$myo->last_name;
}

// *** Customers array	
$CUSTOMERS_ARRAY 		= array(0 => 'Intern');
$CUSTOMERS_ALL_ARRAY 	= array(0 => 'Intern');

$res = eq("	SELECT id,display_name,deleted
			FROM ".DIGI_DB_PREFIX."customers_adm
			ORDER BY display_name ASC
;");
while($myo = mfo($res)) {
	
	if ($myo->deleted == 0) {
		$CUSTOMERS_ARRAY[$myo->id] = $myo->display_name;
	}
	$CUSTOMERS_ALL_ARRAY[$myo->id] = $myo->display_name;
}

// *** Projects arrays
$PROJECTS_ARRAY 		= 	array(0 => '');
$PROJECTS_ALL_ARRAY 	= 	array(0 => '');
$PROJECTS_OPEN_ARRAY 	= 	array(0 => '');
$PROJECTS_CLOSED_ARRAY 	= 	array(0 => '');

$res = eq("	SELECT id,name,finish_date,deleted
			FROM ".DIGI_DB_PREFIX."projects
			ORDER BY name ASC
;");
while($myo = mfo($res)) {
	
	if ($myo->deleted == 0) {
		if ($myo->finish_date) {
			$PROJECTS_CLOSED_ARRAY[$myo->id] 	= $myo->name;
		}
		else {
			$PROJECTS_OPEN_ARRAY[$myo->id] 		= $myo->name;
		}
		$PROJECTS_ARRAY[$myo->id] 				= $myo->name;
	}
	$PROJECTS_ALL_ARRAY[$myo->id] 				= $myo->name;
}

// *** Extra project arrays
$EXTRA_PROJECTS_ARRAY		=	array();
$EXTRA_PROJECTS_ALL_ARRAY	=	array();

$res = eq("	SELECT id,name,deleted
			FROM ".DIGI_DB_PREFIX."extra_projects
			ORDER BY name ASC
;");
while($myo = mfo($res)) {
	if ($myo->deleted == 0) {
		$EXTRA_PROJECTS_ARRAY[$myo->id] = $myo->name;
	}
	$EXTRA_PROJECTS_ALL_ARRAY[$myo->id] = $myo->name;
}
?>