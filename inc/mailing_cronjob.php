<?php
/*
Title:		Mailing cronjob handler
File: 		inc/mailing_cronjob.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl		
*/

require_once('inc/site_ajax_header.php');	
require_once('inc/send_mails2go.php');	

if (isset($mails_sent) and $mails_sent > 0) {
	echo "There were $mails_sent mails sent during this batch. (".date('d/m/Y H:i').")";
}
elseif (isset($mails_sent)) {
	echo "No mails had to be sent at this time. (".date('d/m/Y H:i').")";
}
else {
	echo "It is unknown if any mails were sent.";
}
?>