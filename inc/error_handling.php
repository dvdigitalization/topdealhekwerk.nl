<?php
/*
Title:		Custom error handling
File: 		inc/error_handling.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Error handling options
$log_file 		=	'user_files/error.log';
$err_template	=	'foutmelding.html';
$notices		=	array(	E_NOTICE,
							E_USER_NOTICE,
							E_STRICT);
$error_occured	=	false;

// *** Error holder
if (!isset($_SESSION['ERRORS_OCCURED'])) {
	$_SESSION['ERRORS_OCCURED'] = array();
}

// *** Custom exception class
class Digiception extends Exception {

	// Redefine the exception so message isn't optional
    public function __construct($message, $code = 0) {
        // some code

        // make sure everything is assigned properly
        parent::__construct($message, $code);
    }

    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}

// *** Top level exception handler
function Digiception($exception) {
	echo 'EXCEPTION: '.$exception->__toString();
}
set_exception_handler('Digiception');

// *** Custom error handling function
function digiErrorHandler($type, $error, $script_file, $line, $vars) {

	global 	$cfg, $_version, $_revision, $error_html, $error_occured, $_file, $log_file, $var_dump, $notices,
			$err_template, $halt_script, $Message;

	// *** URL prefix
	if (defined('CMS_PATH_PREFIX')) {
		$pr	=	CMS_PATH_PREFIX;
	}
	elseif (defined('SITE_PATH_PREFIX')) {
		$pr	=	SITE_PATH_PREFIX;
	}

	//@ prepended error, do nothing!
	if (error_reporting() == 0) {
		return null;
	}

	$error_types = array (
						E_ERROR			=>	'Critical',
	               		E_WARNING 		=> 	'Warning',
                     	E_NOTICE 		=> 	'Notice',
      	         		E_USER_ERROR 	=> 	'User error',
            	   		E_USER_WARNING 	=> 	'User warning',
	               		E_USER_NOTICE 	=>	'User notice',
	               		E_STRICT 		=>	'Strict notice'
	);

	//Process this error?
	if ( (!$cfg['IGNORE_NOTICE_ERRORS'] and in_array($type, $notices)) or !in_array($type, $notices)) {

		//Show error on screen? Do not show notices.
		if ($cfg['SHOW_ERRORS'] and !$error_occured and !in_array($type, $notices)) {

			$error_occured = true;

			if (defined('CMS_PATH_PREFIX')) {

				$mail_notice = $cfg['ERROR_MAILING'] ? $cfg['TECH_NAME'].' is hiervan op de hoogte gesteld. Zodra het probleem is
													   verholpen zal '.$cfg['TECH_NAME'].' u dit laten weten.' : '';

				if (isset($Message)) {
					$Message->clear();
					$Message->set("Er is een fout opgetreden in het CMS. $mail_notice<br/><br/>Bedankt voor uw begrip.", CRITICAL);
					$error_html = "	<div style='margin-top: -30px;'>".$Message->get()."</div>";
				}
				else {
					$error_html = "	<div><b>Fout:</b><br/><br/>Er is een fout opgetreden in het CMS. $mail_notice
									<br/>Bedankt voor uw begrip.</div>";
				}
			}
			else {

				// *** Get error template if exists
				if (file_exists($pr.'template/'.$err_template)) {
					$error_html = file_get_contents($pr.'template/'.$err_template);
					$error_html = str_replace('[ERROR_TITLE]', 	ERROR_TITLE, 	$error_html);
					$error_html = str_replace('[ERROR_TEXT]', 	ERROR_TEXT, 	$error_html);
				}
				else {
					$error_html = "
						<b><u>Foutmelding:</u>
						<p>Beste bezoeker, er is een fout opgetreden op de pagina die u probeerde te bekijken. De beheerders van de
						website zijn hiervan middels een email automatisch op de hoogte gesteld. Probeert u de pagina later nog eens
						te bezoeken.</p>
						<p>Bedankt voor uw begrip.</p>
						<hr/>
					";
				}
			}
		}

		//Backtrace
		$trace 			= debug_backtrace();
	    $trace_readable = "<br/><br/>Backtrace:";
	    $key			= 0;
	    foreach($trace as $index => $info) {
	    	if ($info['function'] == 'trigger_error' or $info['function'] == 'digiErrorHandler') continue;
	    	$key++;
	    	if (isset($info['args']) and $info['args'] and is_array($info['args'])) {
	    		$args = '';//implode(',',$info['args']);
    		}
    		else {
    			$args = '';
    		}
	    	$trace_readable .= "<br/><br/>&nbsp; &nbsp; $key) ".$info['function']."(".$args.") called from <b>".basename($info['file'])."</b> on line <b>".$info['line']."</b>.";
	    }

		$err = "
			<b>".$error_types[$type].":</b> $error<br/>&nbsp; &nbsp; &nbsp; &nbsp;in file <b>$script_file</b> on line <b>$line</b> (originated from <b>$_file</b>)".$trace_readable;

		//Save to the error log if enabled
		if ($cfg['ERROR_LOGGING']) {
			if ($fh = @fopen($pr.$log_file, 'at')) {
				fwrite($fh,
					@date('d/m/Y H:i:s').' - '.
					$error_types[$type].': '.$error.' in file '.$script_file.' on line '.$line.'. Originated in '.$_file."\r\n"
				);
		        fclose($fh);
			}
		}

		//Error signature
		$error_sig = md5($type.$script_file.$line);

		//Send an email if there is a critical user error
		if ($cfg['ERROR_MAILING'] and file_exists($pr.'inc/sendmail.php') and !isset($_SESSION['ERRORS_OCCURED'][$error_sig])) {

			$_SESSION['ERRORS_OCCURED'][$error_sig] = 1;
			require_once($pr.'inc/sendmail.php');

			if ($_version) {
				$cms_appendix = ' (CMS v'.$_version.', '.$_revision.')';
			}
			else {
				$cms_appendix = '';
			}

	   		sendMail($cfg['DIGI_ALERT_EMAIL'], '[Error] '.$_SERVER['SERVER_NAME'].$cms_appendix, $err);
	   		usleep(10000);
		}

		//Halt script on these errors
		if ($type == E_WARNING or $type == E_ERROR or $type == E_USER_WARNING) {
			$halt_script = true;
		}
	}
}

// *** Force error handler
if (isset($_GET['force_error_handler'])) {
	$_SESSION['force_error_handler'] = 1;
	unset($_SESSION['show_errors']);
}

// *** Show errors
if (isset($_GET['show_errors'])) {
	$_SESSION['show_errors'] = 1;
	unset($_SESSION['force_error_handler']);
}

// *** Initialize custom error handling if set
if (($cfg['DIGI_ERROR_HANDLING'] and !isset($_SESSION['show_errors']) and !$testmode) or isset($_SESSION['force_error_handler'])) {
	set_error_handler('digiErrorHandler');
}
?>