<?php
/*
Title:		Site specific definitions
File: 		inc/site_specific_definitions.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

//Breadcrumbs array initialization
$crumbs = array('index.html' => 'Topdeal Hekwerk');

//Selected category ID and product ID
$category_id 	= 	(int) request('category_id',0);
$product_id 	= 	(int) request('product_id',	0);

//Database tables
$db_table_cats			=	DIGI_DB_PREFIX.'topdeal_categories';
$db_table_products		=	DIGI_DB_PREFIX.'topdeal_products';
$db_table_colors		=	DIGI_DB_PREFIX.'topdeal_colors';
$db_table_props			=	DIGI_DB_PREFIX.'topdeal_properties';
$db_table_prop_values	=	DIGI_DB_PREFIX.'topdeal_prop_values';
$db_table_customers		=	DIGI_DB_PREFIX.'topdeal_customers';
$db_table_quotes		=	DIGI_DB_PREFIX.'topdeal_offertes';
$db_table_holidays		=	DIGI_DB_PREFIX.'holidays';

//Sale positions
$sale_positions			=	array(0 => 'Niet zichtbaar', 1 => 'Links', 2 => 'Midden', 3 => 'Rechts');

//Vat
$vat_multiplier			=	(100 + $cfg['PRODUCTS_STANDARD_VAT'])/100;
$vat_inverse_multiplier	=	$cfg['PRODUCTS_STANDARD_VAT']/(100 + $cfg['PRODUCTS_STANDARD_VAT']);

//Get the cart sub total, factor
function get_cart_sub_total() {

	global $vat_multiplier;

	//Load cart from session
	if (!isset($_SESSION['cart']) or !$_SESSION['cart'] or !is_array($_SESSION['cart']) or isset($_GET['clear_cart'])) {
		$_SESSION['cart'] = array();
	}
	$cart = $_SESSION['cart'];

	//Initialize
	$subtotal 	= 	0;
	$item_prices	=	array();

	//Loop the items to determine the factor..
	foreach ($cart as $item) {
		$subtotal 	+= 	(int) $item['amount'] * $item['price'];
	}

	//Calculate the factor
	$fact = getMyo('digi_topdeal_discounts', "'$subtotal' > from_amount AND '$subtotal' < till_amount", array('fixed_price','multiplier'));
	if ($fact) {
		$factor = 	$fact->multiplier;
		$fixed	=	$fact->fixed_price;
	}
	else {
		$factor = 	1;
		$fixed	=	0;
	}

	//Re-loop (to prevent rounding discrepancies, we need to do this like this)
	$subtotal 	= 	0;

	//Loop the items
	foreach ($cart as $identifier => $item) {
		$item_prices[$identifier] = (int) $item['amount'] * $item['price'] * $factor * $vat_multiplier;
		$subtotal 	+= 	$item_prices[$identifier];
	}

	//Factorize the subtotal
	$subtotal += $fixed;

	//Return
	return array($subtotal, $factor, $item_prices);
}

//Delivery costs
function get_delivery_costs($sub_total) {
	global $cfg;
	if ($sub_total > (int) $cfg['NO_DELIVERY_COSTS_ABOVE']) {
		return 0;
	}
	else {
		return (int) $cfg['DELIVERY_COSTS'];
	}
}

//Discount
function get_discount($code, $sub_total) {

	//No code?
	if (!$code) {
		return array(0,'');
	}

	//Validate the discount code
	evalAll($code);
	$code = strtoupper($code);

	$res = eq("	SELECT id, active, percentage, fixed_amount FROM ".DIGI_DB_PREFIX."discount_codes
				WHERE code = '$code' AND active = '1';");
	if (!mysql_num_rows($res)) {
		return array(0,'Ongeldige of verlopen code');
	}
	$myo = mfo($res);

	//Determine discount type
	if ($myo->percentage) {
		return array(($myo->percentage/100) * $sub_total, $myo->percentage.'% korting');
	}
	elseif ($myo->fixed_amount) {
		return array($myo->fixed_amount > $sub_total ? $sub_total : $myo->fixed_amount, parseAmount($myo->fixed_amount, '&euro; ').' korting');
	}
	else {
		return array(0,'');
	}
}

//Get cart amounts
function get_cart_amounts($discount_code = '') {

	//Global
	global $vat_inverse_multiplier, $vat_multiplier;

	//Discount code in use?
	if (!$discount_code) {
		$discount_code = (isset($_SESSION['discount_code']) and $_SESSION['discount_code']) ? $_SESSION['discount_code'] : '';
	}

	//Get the products subtotal (excl. VAT, incl. FACTOR) and the used factor multiplier
	list ($sub_total, $factor, $item_prices) 	= 	get_cart_sub_total();

	//Get the discount based on the sub total
	$discount	=	get_discount($discount_code, $sub_total);
	list($discount, $discount_info) = $discount;

	//Subtract the discount from the subtotal
	$sub_total	-=	$discount;

	//Get the delivery costs
	$delivery	=	get_delivery_costs($sub_total);

	//Calculate the vat and the new subtotal with VAT included
	$vat		=	($sub_total + $delivery) * $vat_inverse_multiplier;

	//Calculate the total
	$total		=	$sub_total + $delivery;

	return array($sub_total, $delivery, $discount, $vat, $total, $discount_code, $discount_info, $factor, $item_prices);
}

function is_weekend($timestamp) {
	if (($weekday = date('N', $timestamp)) == 6 or $weekday == 7) {
		return true;
	}
	return false;
}

function next_non_weekend_day($timestamp) {
	if (($weekday = date('N', $timestamp)) == 6) {
		return $timestamp+2*86400;
	}
	elseif ($weekday == 7) {
		return $timestamp+86400;
	}
	return $timestamp;
}

function get_price_block($price_normal, $price_topdeal, $price_voordeel, $price_unit = '') {

	return "<div class='priceNormal'>Normaal ".($price_unit ? 'vanaf ' : '')."$price_normal $price_unit</div>
			<h4>Topdeal".($price_unit ? ' vanaf' : '')."</h4>
			<h5>$price_topdeal $price_unit</h5>
			<div class='priceAdv'>Uw voordeel: $price_voordeel $price_unit</div>";
}

function ajax_price_block($total_price, $total_price_normally) {

	global $vat_multiplier;

	//Calculate the factor
	$fact = getMyo('digi_topdeal_discounts', "'$total_price' > from_amount AND '$total_price' < till_amount",
			array('fixed_price','multiplier'));
	if ($fact) {
		$factor = 	$fact->multiplier;
		$fixed	=	$fact->fixed_price;
	}
	else {
		$factor = 	1;
		$fixed	=	0;
	}

	$p_price	 =	$total_price*$vat_multiplier*$factor + $fixed;

	$p_voordeel	 =	parseAmount($total_price_normally*$vat_multiplier-$p_price, '&euro; ');
	$p_price	 =	parseAmount($p_price, '&euro; ');
	$p_normal	 =	parseAmount($total_price_normally*$vat_multiplier, '&euro; ');

	return get_price_block($p_normal, $p_price, $p_voordeel);
}
?>