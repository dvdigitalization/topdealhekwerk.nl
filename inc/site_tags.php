<?php
/*
Title:		Site specific tags to replace
File: 		inc/site_tags.php
Version: 	v2.20
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

//Default page image if none uploaded
if (empty($page_image)) {
	$page_image = 'gfx/layout/sfeer/sfeer_paneelsystemen.jpg';
}

//Cookie checker
if(isset($_SESSION['cookiehide']) && $_SESSION['cookiehide'] == 'aan') {
	$html = str_replace('[COOKIEHIDE]', 'style="display:none;"', $html);
} else {
	$html = str_replace('[COOKIEHIDE]', '', $html);
}

//Boxes on the bottom
$html = str_replace('[BOX_1]',			BOX_1,			$html);
$html = str_replace('[BOX_2]',			BOX_2,			$html);
$html = str_replace('[BOX_3]',			BOX_3,			$html);
$html = str_replace('[BOX_4]',			BOX_4,			$html);
$html = str_replace('[BOX_5]',			BOX_5,			$html);

//Footer menu
$html = str_replace('[BOTTOM_MENU]',	$bottom_menu_html,	$html);

//GA conversion
$html = str_replace('[GA_CONVERSION]',	isset($ga_conversion) ? $ga_conversion : '',	$html);

//Shop menu
$db_cats = DIGI_DB_PREFIX.'topdeal_categories';
$shop_menu_html = '';

//Read main categories
$res = eq("	SELECT id,menu_name FROM $db_cats WHERE active = '1' AND sub_cat_of = '0' AND menu_name <> ''
			ORDER BY category_order ASC;");
while ($cat = mfo($res)) {
	$link				=	'categorie-'.$cat->id.'-'.rewriteSafe($cat->menu_name).'.html';
	$shop_menu_html		.=	"\n<li><a href='$link' title='$cat->menu_name'";

	//Sub categories
	if ($category_id and ($category_id == $cat->id or getMyo($db_cats,$category_id,'sub_cat_of') == $cat->id)) {

		$shop_menu_html .= " class='selected'>$cat->menu_name</a>";

		$res_s = eq("	SELECT id,menu_name FROM $db_cats WHERE active = '1' AND sub_cat_of = '$cat->id' AND menu_name <> ''
						ORDER BY category_order ASC;");
		if (mnr($res_s)) {
			$shop_menu_html .= "\n\t<div class='navLtSub'><ul>";
			while ($sub = mfo($res_s)) {
				$link		 	 =	'categorie-'.$sub->id.'-'.rewriteSafe($sub->menu_name).'.html';
				$shop_menu_html .= "\n\t\t<li><a href='$link' ".($category_id == $sub->id ? "class='selected' " : '')."title='$sub->menu_name'>$sub->menu_name</a></li>";
			}
			$shop_menu_html .= "\n\t</ul></div>";
		}
	}
	else {
		$shop_menu_html .= ">$cat->menu_name</a>";
	}

	$shop_menu_html	.=	"\n</li>";
}
$html = str_replace('[SHOP_MENU]',	$shop_menu_html,	$html);

//Breadcrumbs
$crumb_links = array();
foreach ($crumbs as $link => $title) {
	if ($_file == $link or (isset($active_crumb) and $active_crumb == $link)) {
		$crumb_links[]	=	"<strong>$title</strong>";
	}
	else {
		$crumb_links[]	=	"<a href='$link' title='$title'>$title</a>";
	}
}
$html = str_replace('[CRUMBS]',	implode(" &gt; ",$crumb_links),	$html);

//Sale products on home page
if ($template == 'home.html') {

	$sales = '';
	$res = eq("	SELECT $db_table_products.id, price, sale_price, normal_price, description, name, menu_name, price_unit, category_id, on_sale
				FROM $db_table_products
				LEFT JOIN $db_table_cats ON ($db_table_cats.id = category_id)
				WHERE $db_table_products.active ='1' AND sale_position > 0
				ORDER BY sale_position ASC;");

	$vat_multiplier			=	(100 + $cfg['PRODUCTS_STANDARD_VAT'])/100;
	$gfx_folder				=	'gfx'.$_sep.'categories';

	while ($myo = mfo($res)) {
		$myo->name = $myo->menu_name ? $myo->menu_name : $myo->name;
		$myo->price = ($myo->on_sale ? $myo->sale_price : $myo->price)*$vat_multiplier;
		$myo->normal_price *= $vat_multiplier;
		$voordeel = $myo->normal_price-$myo->price;
		$prod_first = isset($prod_first) ? '' : ' prodFirst';
		$s_image_url	=	$gfx_folder.$_sep.'cat_'.$myo->category_id.'.jpg';
		$s_image		=	file_exists($s_image_url) ? "<img src='$s_image_url' alt='$myo->name'/>" : '';

		$shop_link	 =	$cfg['USE_REWRITE_ENGINE'] ? 	'categorie-'.$myo->category_id.'-'.rewriteSafe($myo->name).'.html' :
														'shop.php?category_id='.$myo->category_id;
		$sales .= "
			<div class='prodVert$prod_first'>
				<div class='prodVertImg'><a href='$shop_link' title='$myo->name'>$s_image</a></div>
				<h3><a href='$shop_link' title='$myo->name'>$myo->name</a></h3>
				<div class='prodVertDesc'>$myo->description</div>
				<div class='priceNormal'>Normaal ".parseAmount($myo->normal_price, '&euro; ')." $myo->price_unit</div>
				<h4>Topdealprijs</h4>
				<h5>".parseAmount($myo->price, '&euro; ')." $myo->price_unit</h5>
				<div class='priceAdv'>Uw voordeel: ".parseAmount($voordeel, '&euro; ')." $myo->price_unit</div>
			</div>
		";
	}

	$html = str_replace('[SALE_ITEMS]',	$sales,	$html);
}

if ($template == 'static/checkout.html') {
	$html = str_replace('[SIDEBAR]', $sidebar, $html);

	//replace the corresponding tag with the js array of holidays
	$holidays = '';
	$res      = eq("SELECT date,date2 FROM $db_table_holidays WHERE active = '1' ORDER BY date ASC");
	if(mnr($res)){
		$holidays = array();
		while($myo = mfo($res)){
			$myo->date = unix_time($myo->date);

			$d = date('d', $myo->date);
			$m = date('m', $myo->date);
			$y = date('Y', $myo->date);

			//Single holiday
			if($myo->date2 == '0000-00-00 00:00:00'){
				$holidays[] = "[$m, $d, 'nl']";
			//Holiday range
			}else{
				$myo->date2 = unix_time($myo->date2);

				$d2 = date('d', $myo->date2);
				$m2 = date('m', $myo->date2);
				$y2 = date('Y', $myo->date2);

				//The stop date is smaller than the start one, continue
				if($myo->date > $myo->date2) continue;

				//The dates are in the same month, so we just need to cycle from start to stop
				if($m == $m2){
					for($i=$d; $i<$d2; $i++){
						$holidays[] = "[$m, $i, 'nl']";
					}
				//Otherwise for each month we need to do the same trick
				}else{
					for($im=$m; $im<=$m2; $im++){
						//Gets the number of days in the current month
						$dom = date('t', unix_time($y.'-'.$im.'-01 00:00:00'));

						for($id=1; $id<=$dom; $id++){
							//if we're looping through the first month, then reset id var
							if($im == $m and $id < $d) $id = $d;

							//here it finishes
							if($im == $m2 and $id == $d2) break 2;

							$holidays[] = "[$im, $id, 'nl']";
						}
					}
				}
			}
		}
		$holidays = 'natDays = ['.implode(', ', $holidays).'];';
	}
	else {
		$holidays = 'natDays = new Array();';
	}
	$html = str_replace('[HOLIDAYS_JS]', $holidays, $html);

	if(!isset($datepicker_min_date)) $datepicker_min_date = '+3d';
	$html = str_replace('[DATEPICKER_MIN_DATE]', $datepicker_min_date, $html);
}
?>