<?php
/*
Title:		Site header
File: 		inc/site_header.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Startup
require_once('inc/site_ajax_header.php');

// *** Determine the page set
if (isset($page_set) and is_array($page_set)) {
	$page_set = "OR page_set = '".implode("' OR page_set = '", $page_set)."'";
}
elseif (isset($page_set) and is_string($page_set)) {
	$page_set = "OR page_set = '$page_set'";
}
else {
	$page_set = '';
}

// *** Read text elements from database according to the page sets found
$res = eq('	SELECT definition, contents, is_fck FROM '.DIGI_DB_PREFIX.'page_contents_'.$_lang."
			WHERE page_set = 'algemeen' OR page_set = 'inloggen' OR page_set = '_general' OR page_set = 'login' $page_set;");
while ($myo = mfo($res)) {
	if (!defined($myo->definition)) {
		define($myo->definition, $myo->is_fck ? $myo->contents : nl2br($myo->contents));
	}
}

// *** Initialize some page variables so that they will always be available
$page = $page_id = $page_contents = $meta_descr = $meta_keywords = $show_404 = '';
$page_allowed_groups = array();

// *** Is there a HTML name coming from our regular page.php file, or has the HTML name been set manually?
if (isset($_GET['html_name'])) {
	evalAll($_GET['html_name'], false);
	$html_name	=	fileNameSafe(strtolower($_GET['html_name']));
	$html_name	=	substr($html_name,-5,5) == '.html' ? $html_name : $html_name.'.html';
	$show_404	=	true;
}
elseif (isset($html_name)) {
	$html_name	=	substr($html_name,-5,5) == '.html' ? $html_name : $html_name.'.html';
	$show_404	=	false;
}
	
// *** Our standard pages module
if (isset($installed_modules[MOD_PAGES])) {
	       
	//Try to find a page in the database: look for HTML name, look for page ID, look for filename
	if (isset($html_name)) {
		$sc_where 	= 	"html_name = '$html_name'";
	}
	elseif ($page_id = (int) request('page_id')) {
		$sc_where	=	"id = '$page_id'";	
	}
	else {
		$sc_where	=	"file_name = '$_file'";	
	}
	
	//Try to find the page in the current language first
	$res = eq('	SELECT * FROM '.DIGI_DB_PREFIX.'pages_'.$_lang.' WHERE '.$sc_where.' AND active = 1;');
	
	//Not found? Try all language tables
	if (!mysql_num_rows($res)) {
		foreach ($installed_languages as $lang) {
			if ($lang == $_lang) continue;
			$res = eq('	SELECT * FROM '.DIGI_DB_PREFIX.'pages_'.$lang.' WHERE '.$sc_where.' AND active = 1;');
			if (mysql_num_rows($res)) {
				$_lang = $lang;
				$_SESSION['lang'] = $lang;
				break;
			}	
		}
	}
	
	//Page found, get all the information and overwrite variables
	if (mysql_num_rows($res)) {      
		
		//Read and set vars
		$myo = $page  			= 	mfo($res);
		$page_id				=	$myo->id;
		$page_contents 			= 	$myo->contents;
		$page_title				= 	$myo->title;
		$template				=	$myo->template;
		$meta_descr				=	$myo->meta_description;
		$meta_keywords			=	$myo->meta_keywords;
		$free_field				=	$myo->free_field;
		$needs_authorization	=	$myo->needs_authorization ? true : false;
		$html_name				=	$myo->html_name;
		$_file					=	$myo->file_name?$myo->file_name:($myo->html_name?$myo->html_name :'page.php?page_id='.$myo->id);

		//Page image or flash file
		if (isset($myo->image_file) and $myo->image_file) {
			$page_image			=	'gfx'.$_sep.'pages'.$_sep.$myo->image_file;	
		}

		//Authorization
		if ($needs_authorization) {
			$page_allowed_groups = makeArray(DIGI_DB_PREFIX.'page_group_relations_'.$_lang,'id','group_id',"page_id = '$myo->id'");
		}
	}
	
	//We got a page id or html name from page.php, but this file doesn't exist. Produce template 404 if exists.
	elseif ($page_id or (isset($html_name) and $show_404)) {
		
		//Set contents and title
		if (file_exists(SITE_PATH_PREFIX.'template/404.html')) {
			$page_contents 	= 	file_get_contents(SITE_PATH_PREFIX.'template/404.html');
			$page_contents	=	str_replace('[ERROR_404_TITLE]',	ERROR_404_TITLE,	$page_contents);
			$page_contents	=	str_replace('[ERROR_404_TEXT]',		ERROR_404_TEXT,		$page_contents);
			$page_title		= 	ERROR_404_TITLE;
		}
		else {
			$page_title		= 	'Pagina niet gevonden';	
			$page_contents 	= 	'<h1>Pagina niet gevonden</h1>
								 <p>Deze pagina kan niet gevonden worden of is tijdelijk niet actief.</p>';	
		}
		
		//Set file variable
		$_file = isset($html_name) ? $html_name : 'page.php?page_id='.$page_id;
	}
}

// *** Customer specific pages module
else if (file_exists(SITE_PATH_PREFIX.'inc/site_pages.php')) {
	require_once(SITE_PATH_PREFIX.'inc/site_pages.php');
}

// *** If these vars are not set, we initialize them now
if (!isset($html_name))  $html_name  = $_file;
if (!isset($page_title)) $page_title = '';
if (!isset($page_image)) $page_image = '';
if (!isset($free_field)) $free_field = '';

// *** File re-define if we use a HTML name
$_file = $cfg['USE_REWRITE_ENGINE'] ? $html_name : $_file;

// *** Authenticate
if ((isset($needs_authorization) and $needs_authorization === true) or
	(isset($do_authorization) and $do_authorization === true)) {
	$needs = (isset($needs_authorization) and $needs_authorization === true) ? true : false;
	require_once(SITE_PATH_PREFIX.'classes/class_Login.php');
	require_once(SITE_PATH_PREFIX.'classes/class_Login_Site.php');
	auth($needs);
}

// *** Menu building
require_once(SITE_PATH_PREFIX.'inc/site_menu.php');

// *** Calendar support
$calendar_support = (isset($calendar_support) and $calendar_support === true) ? "
<script type='text/javascript' src='js/calendar.js'></script>
<script type='text/javascript' src='js/calendar-nl.js'></script>
<script type='text/javascript' src='js/calendar-setup.js'></script>
<link href='style/style_calendar.css' rel='stylesheet' type='text/css'>
" : '';

// *** JS
$javascript = ((isset($load_js) and $load_js === true) or (isset($javascript) and $javascript === true))  ? "
<script type='text/javascript' src='js/globals.js'></script>
<script type='text/javascript' src='js/globals_cms.js'></script>
<script type='text/javascript' src='js/ajax_functionality.js'></script>
" : '';

// *** Customer specific header
if (file_exists(SITE_PATH_PREFIX.'inc/site_header_customer.php')) {
	require_once(SITE_PATH_PREFIX.'inc/site_header_customer.php');
}
?>