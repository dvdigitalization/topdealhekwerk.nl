<?php
/*
Title:		Site footer
File: 		inc/site_footer.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Print version template
if (isset($_GET['print'])) {
	if (isset($print_template)) {
		$template	=	$print_template;
	}
	else {
		$template	=	'print/print_default.html';
	}
}

// *** Get layout from template
if (!isset($template) or empty($template) or !file_exists('template/website/'.$template)) {
	$template = 'default.html';
}
$html = file_get_contents('template/website/'.$template);

// *** Site specific replacements
if (file_exists(SITE_PATH_PREFIX.'inc/site_tags.php')) {
	require_once(SITE_PATH_PREFIX.'inc/site_tags.php');
}

// *** Standard replacements
$html = str_replace('[SITE_PATH_PREFIX]',	SITE_PATH_PREFIX,			$html);
$html = str_replace('[SITE_NAME]',			$site_name,					$html);
$html = str_replace('[CALENDAR]',			$calendar_support,			$html);
$html = str_replace('[JAVASCRIPT]',			$javascript,				$html);
$html = str_replace('[MENU]',				$menu_html,					$html);
$html = str_replace('[META_DESCR]',			$meta_descr,				$html);
$html = str_replace('[META_KEYWORDS]',		$meta_keywords,				$html);
$html = str_replace('[PAGE_TITLE]',			$page_title,				$html);
$html = str_replace('[PAGE_IMAGE]',			$page_image,				$html);
$html = str_replace('[FILE]',				$_file,						$html);
$html = str_replace('[LANG]',				$_lang,						$html);
$html = str_replace('[ABS]',				$_link,						$html);
$html = str_replace('[CHARSET]',			$charset,					$html);

// *** Determine contents
if ($error_occured) {
	ob_clean();
	$html = str_replace('[CONTENT]',			$error_html,			$html);		
}
elseif (isset($needs_authorization) and $needs_authorization === true and $is_authorized === true) {
	$html = str_replace('[CONTENT]',			ob_get_contents(),		$html);
	ob_clean();
}
elseif (isset($needs_authorization) and $needs_authorization === true and !$is_authorized) {
	ob_clean();
	$html = str_replace('[CONTENT]',			$login_box,				$html);	
}
else {
	$html = str_replace('[CONTENT]',			ob_get_contents(),		$html);
	ob_clean();
}

// *** Output
echo $html;
?>
