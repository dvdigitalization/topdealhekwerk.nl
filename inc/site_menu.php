<?php
/*
Title:		Site menu builder
File: 		inc/site_menu.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

//Menu module install check
if (!isset($installed_modules[MOD_MENU])) {
	echo '*** WARNING: The menu module is not installed, but the default site_menu.php file has not been altered! Please verify! ***';
	exit;
}

//Table definitions
$db_table_menu_sections	=	DIGI_DB_PREFIX.'menu_sections_'.$_lang;
$db_table_menu_items	=	DIGI_DB_PREFIX.'menu_items_'.$_lang;
$db_table_pages			=	DIGI_DB_PREFIX.'pages_'.$_lang;

//Fetch all the menu items for the top menu
$res_i = eq("SELECT name, page_id, page_file FROM $db_table_menu_items WHERE section = '1' ORDER BY item_order ASC;");
if (mysql_num_rows($res_i)) {

	//Open the items HTML
	$menu_html .= "<ul>";
	$menu_items = array();

	//Loop the items
	while ($item = mfo($res_i)) {

		//If we have a hard coded file, check if it exists. If not, don't show item in menu.
		if ($item->page_file) {
			if (!$cfg['MENU_IGNORE_WRONG_LINKS'] and !file_exists($item->page_file)) {
				continue;
			}
			$page_file = $item->page_file;
		}

		//If we have a page id...
		elseif ($item->page_id) {

			//Get the page info
			$page = getMyo($db_table_pages, $item->page_id, array('active','file_name','html_name'));

			//If the page is not active, don't show item in menu.
			if (!$page or !$page->active) {
				continue;
			}

			//If we have a file name, use that
			if ($page->file_name) {
				$page_file = $page->file_name;
			}
			//If we have a html name, use that
			elseif ($cfg['USE_REWRITE_ENGINE'] and $page->html_name) {
				$page_file = $page->html_name;
			}
			//Otherwise just use the ID
			else {
				$page_file = 'page.php?page_id='.$item->page_id;
			}
		}

		//Not linked, don't show in menu
		else {
			$page_file = '#';
		}

		//Is this menu item active?
		$class	=	($page_file == $html_name ? 'selected' : '');

		//Make the item HTML
		$menu_items[] = "<li><a href='$page_file' title='$item->name' class='$class'>$item->name</a></li>";
	}

	//Close the items HTML
	$menu_html .= implode("<li><p>|</p></li>",$menu_items)."</ul>";
}

//Fetch all the menu items for the footer menu
$i = 1;
$res_i = eq("SELECT name, page_id, page_file FROM $db_table_menu_items WHERE section = '2' ORDER BY item_order ASC;");
if (mysql_num_rows($res_i)) {

	//Clear the items array
	$menu_items = array();

	//Loop the items
	while ($item = mfo($res_i)) {

		//If we have a hard coded file, check if it exists. If not, don't show item in menu.
		if ($item->page_file) {
			if (!$cfg['MENU_IGNORE_WRONG_LINKS'] and !file_exists($item->page_file)) {
				continue;
			}
			$page_file = $item->page_file;
		}

		//If we have a page id...
		elseif ($item->page_id) {

			//Get the page info
			$page = getMyo($db_table_pages, $item->page_id, array('active','file_name','html_name'));

			//If the page is not active, don't show item in menu.
			if (!$page or !$page->active) {
				continue;
			}

			//If we have a file name, use that
			if ($page->file_name) {
				$page_file = $page->file_name;
			}
			//If we have a html name, use that
			elseif ($cfg['USE_REWRITE_ENGINE'] and $page->html_name) {
				$page_file = $page->html_name;
			}
			//Otherwise just use the ID
			else {
				$page_file = 'page.php?page_id='.$item->page_id;
			}
		}

		//Not linked, don't show in menu
		else {
			$page_file = '#';
		}

		//Is this menu item active?
		$class	=	($page_file == $html_name ? 'selected' : '');

		$target = '';
		if ($i == 1) $target = "target='_blank'";

		//Make the item HTML
		$menu_items[] = "<a href='$page_file' title='$item->name' class='$class' $target>$item->name</a>";

		$i++;
	}

	//Close the items HTML
	$bottom_menu_html = implode("&nbsp;&nbsp;|&nbsp;&nbsp;",$menu_items);
}

?>