<?php
/*
Title:		Opened mails counter
File: 		inc/times_opened_counter.php
Version: 	v2.20
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

chdir('../');
require('inc/site_ajax_header.php');
error_reporting(0);

//Validate incoming data first
if (isset($_GET['hash']) and isset($_GET['newsletter']) and $newsletter_id = (int) $_GET['newsletter'] and
	sha1($newsletter_id.'timesopenedhash123') == $_GET['hash'] and isset($_GET['r']) and isset($_GET['rv']) and
	$email = validateEmail($_GET['r']) and sha1($email.'recipienthash456') == $_GET['rv']) {

	//Already counted?
	if (!getMyo(DIGI_DB_PREFIX.'newsletters_opened', "newsletter_id='$newsletter_id' AND recipient='$email'", 'id')) {

		//Increment count field and insert record
		incrementField(DIGI_DB_PREFIX.'newsletters',$newsletter_id,'times_opened');
		insRec(DIGI_DB_PREFIX.'newsletters_opened',array('newsletter_id','recipient'),array($newsletter_id,$email));
	}
}

//Output a transparent pixel
@ob_clean();
header('Content-type: image/gif');
echo file_get_contents('gfx/cms/system/transparent.gif');

?>