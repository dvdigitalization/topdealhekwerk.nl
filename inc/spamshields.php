<?php
/*
Title:		Digi SPAM shields script
File: 		inc/spamshields.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization

Notes:		Let your main script set the variable $spam_check_string.
			The script returns $ip_blocked = true if the IP is blocked, and $very_bad_word_found or $bad_word_found
			with the relevant word in them, if one of those is found. So you can use this in the rest of the script
			for form submit validation and make warning messages accordingly.
			
*/

// *** Warning
if (!isset($spam_check_string)) {
	echo "*** VARIABLE \$spam_check_string NOT set! ***";
	exit;
}

// *** Settings
$db_table_bad_words		=	DIGI_DB_PREFIX.'bad_words';
$db_table_blocked_ips	=	DIGI_DB_PREFIX.'blocked_ips';
$badwords 				= 	array();
$verybadwords 			= 	array();
$badsentences			=	array();
$verybadsentences		=	array();
$verybadstrings			=	array();
$badstrings				=	array();
$spam_check_words 		= 	explode(' ',strtolower($spam_check_string));
$very_bad_word_found	=	'';
$bad_word_found			=	'';
$ip_blocked				=	false;

// *** Load bad words and sentences from database
$res = eq("SELECT word, ban_ip, sub_string FROM $db_table_bad_words;");
while ($myo = mfo($res)) {
	
	if ($myo->ban_ip) {
		
		if (strstr($myo->word,' ')) {
			$verybadsentences[]	=	strtolower($myo->word);		
		}
		else {
			if ($myo->sub_string) {
				$verybadstrings[]	=	strtolower($myo->word);			
			}
			else {
				$verybadwords[]		=	strtolower($myo->word);				
			}
		}
	}
	else {
		
		if (strstr($myo->word,' ')) {
			$badsentences[]		=	strtolower($myo->word);		
		}
		else {
			if ($myo->sub_string) {
				$badstrings[]	=	strtolower($myo->word);			
			}
			else {
				$badwords[]		=	strtolower($myo->word);				
			}
		}
	}
}

// *** Check for blocked IP
$ip_parts	=	explode('.',$_ip);
$ip_1		=	$ip_parts[0].'.*';
$ip_2		=	isset($ip_parts[1]) ? $ip_parts[0].'.'.$ip_parts[1].'.*' : $ip_1;
$ip_3		=	isset($ip_parts[2]) ? $ip_parts[0].'.'.$ip_parts[1].'.'.$ip_parts[2].'.*' : $ip_2;

$res = eq("SELECT id,block_count FROM $db_table_blocked_ips WHERE ip = '$_ip' OR ip = '$ip_1' OR ip = '$ip_2' OR ip = '$ip_3';");
if (mysql_num_rows($res)) {
	$ip_blocked		=	true;
	$myo 			= 	mfo($res);
	upRec($db_table_blocked_ips, $myo->id, 'block_count', $myo->block_count+1);
}
else {
	
	// *** Instant block IP strings
	foreach ($verybadstrings as $verybadstring) {
		if (strstr($spam_check_string, 	$verybadstring)) {
			$very_bad_word_found = $verybadstring;
			break;
		}
	}
	
	// *** Instant block IP words
	if (!$very_bad_word_found) {
		$words_found = array_intersect($verybadwords, $spam_check_words);
		if (count($words_found)) {		
			$very_bad_word_found = implode(', ', $words_found);
		}
	}
	
	// *** Instant block IP sentences
	if (!$very_bad_word_found) {
		foreach ($verybadsentences as $sentence) {
			if (strstr($spam_check_string, $sentence)) {
				$very_bad_word_found = "'".$sentence."'";
				break;
			}
		}
	}	
	
	// *** No post strings
	if (!$very_bad_word_found) {
		foreach ($badstrings as $badstring) {
			if (strstr($spam_check_string, 	$badstring)) {
				$bad_word_found = $badstring;
				break;
			}
		}
	}
	
	// *** No post words
	if (!$very_bad_word_found and !$bad_word_found) {
		$words_found = array_intersect($badwords, $spam_check_words);
		if (count($words_found)) {		
			$bad_word_found = implode(', ', $words_found);
		}	
	}

	// *** No post sentences
	if (!$very_bad_word_found and !$bad_word_found) {
		foreach ($badsentences as $sentence) {
			if (strstr($spam_check_string, $sentence)) {
				$bad_word_found = "'".$sentence."'";
				break;
			}
		}
	}
	
	// *** Block the IP if very bad word found
	if ($very_bad_word_found) {
		insRec($db_table_blocked_ips,array('ip','block_count'),array($_ip,1));	
	}
}
?>