<?php
/*
Title:		Frequently used array definitions (EN)
File: 		inc/arrays_EN.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Array with weekday numbers / names in it
$WEEKDAY_ARRAY = array (   	0 => 'Dimanche',
							1 => 'Lundi',
                        	2 => 'Mardi',
                        	3 => 'Mercredi',
                        	4 => 'Jeudi',
                        	5 => 'Vendredi',
                        	6 => 'Samedi',
                        	7 => 'Dimanche'
);

// *** Array with month numbers / names in it
$MONTH_ARRAY = array (   	1 => 'Janvier',
                        	2 => 'Février',
                        	3 => 'Mars',
                        	4 => 'Avril',
                        	5 => 'Mai',
                        	6 => 'Juin',
                        	7 => 'Juillet',
                        	8 => 'Août',
                        	9 => 'Septembre',
                        	10 => 'Octobre',
                        	11 => 'Novembre',
                        	12 => 'Décembre'
);
                                	
// *** Array with month numbers / names in it, short format
$MONTH_SHORT_ARRAY = array (	1 => 'Jan',
                        		2 => 'Fév',
                        		3 => 'Mar',
	                        	4 => 'Avr',
	                        	5 => 'Mai',
	                        	6 => 'Jun',
	                        	7 => 'Jul',
	                        	8 => 'Aoû',
	                        	9 => 'Sep',
	                        	10 => 'Oct',
	                        	11 => 'Nov',
	                        	12 => 'Déc'
);                                	

// *** Array with years in it
$YEAR_ARRAY = array (   2005 => 2005,
                    	2006 => 2006,
                    	2007 => 2007,
                    	2008 => 2008,
                    	2009 => 2009,
                    	2010 => 2010,
                    	2011 => 2011,
                    	2012 => 2012,
                    	2013 => 2013,
                    	2014 => 2014,
                    	2015 => 2015
);

// *** Hours array
for ($h = 6; $h <= 29; $h++) {
	for ($m = 0; $m <=45; $m+=15) {
		if ($h >= 24) 	$h_p = $h - 24;
		else			$h_p = $h;
		$hour = sprintf('%02d',$h_p).':'.sprintf('%02d',$m);
		$HOUR_ARRAY[$hour] = $hour;
	}
}
                                	
// *** Array with week nr's in it
for ($w = 1; $w <= 52; $w++) {
	$WEEK_ARRAY[$w] = $w;
}

// *** Array with sex definitions in it
$SEX_ARRAY = array (	0	=>	'',
						1 	=> 'Mr. ',
						2	=> 'Mme. '
					);

// *** Different type of sex definitions
$SEX_ARRAY_2 = array(	0	=>	'',
						1 	=> 'Mr. ',
						2	=> 'Mme. '
					);

// *** Yes/No array
$YESNO_ARRAY = array( 	1	=> 'Oui',
						0	=> 'Non'
					);	

// *** Regions array for delivery costs etc.				
$REGION_ARRAY = array(
						1	=>	'Le Pays-Bas',
						2	=>	'Europe',
						3	=>	'Le monde'
);	

// *** Languages array
$LANGUAGES_ARRAY = array(
						'NL'	=>	'hollandais',
						'EN'	=>	'anglais',
						'FR'	=>	'francais'
);	
					
// *** Countries array
$COUNTRY_ARRAY = array();
$res = eq("SELECT id,name_EN FROM ".DIGI_DB_PREFIX."countries ORDER BY name_EN ASC;");
while ($myo = mfo($res)) {
	$COUNTRY_ARRAY[$myo->id] = $myo->name_EN;
}		
?>