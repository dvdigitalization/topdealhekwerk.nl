<?php
/*
Title:		File download stream script
File: 		inc/download.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

$allowed_directories	=	array('user_files');

if (isset($_GET['file']) and !empty($_GET['file'])) {
	
	$file = $_GET['file'];
	
	$parts = explode('/', $file);
	if (substr($file,-3) == 'php' or substr($file,-3) == 'inc' or !in_array($parts[0], $allowed_directories) or in_array('..', $parts)) {
		echo "501 - Not allowed";
		exit;		
	}
	
	if (file_exists('../'.$file)) {
		
		if (!isset($_GET['filename']) or empty($_GET['filename'])) {
			$_GET['filename'] = array_pop($parts);
		}
		
		header('Content-Type: application/octet-stream');
		header('Content-Length: '.filesize('../'.$file));
		header('Content-Disposition: attachment; filename="'.$_GET['filename'].'"');
		readfile('../'.$file);
	}
	else {
		echo "404 - File not found";
		exit;
	}
}
?>