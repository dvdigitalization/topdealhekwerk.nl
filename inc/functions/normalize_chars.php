<?php
/*
Title:		Normalize special characters to regular ones
File: 		inc/functions/normalize_chars.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

function normalize_chars($string) {
	
	$string = str_replace('é', 'e', $string);
	$string = str_replace('è', 'e', $string);
	$string = str_replace('ë', 'e', $string);
	$string = str_replace('ê', 'e', $string);
	
	$string = str_replace('á', 'a', $string);
	$string = str_replace('à', 'a', $string);
	$string = str_replace('ä', 'a', $string);
	$string = str_replace('â', 'a', $string);
	
	$string = str_replace('ó', 'o', $string);
	$string = str_replace('ò', 'o', $string);
	$string = str_replace('ö', 'o', $string);
	$string = str_replace('ô', 'o', $string);
	
	$string = str_replace('ú', 'u', $string);
	$string = str_replace('ù', 'u', $string);
	$string = str_replace('ü', 'u', $string);
	$string = str_replace('û', 'u', $string);
	
	$string = str_replace('í', 'i', $string);
	$string = str_replace('ì', 'i', $string);
	$string = str_replace('ï', 'i', $string);
	$string = str_replace('î', 'i', $string);
		
	return $string;
}

?>