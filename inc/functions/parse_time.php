<?php
/*
Title:		Time string parse and validate function
File: 		inc/functions/parse_time.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization

Comments:	Parse a time into hours, minutes and seconds, or just seconds

*/

function parse_time($time_string, $to_seconds = false) {
	
	$time_string	=	trim($time_string);

	if (!empty($time_string)) {
		$time_parts	=	explode(':',$time_string);
		if (count($time_parts)) {
			if (isset($time_parts[0]))	$hours	=	(int)	$time_parts[0];
			if (isset($time_parts[1]))	$mins	=	(int)	$time_parts[1];
			if (isset($time_parts[2]))	$secs	=	(int)	$time_parts[2];
			if ($hours < 0 or $hours > 23) 	$hours 	= 0;
			if ($mins < 0 or $mins > 59) 	$mins 	= 0;
			if ($secs < 0 or $secs > 59) 	$secs 	= 0;
			return 	$to_seconds ? $hours*3600+$mins*60+$secs : 
					array('h' => sprintf('%02d',$hours), 'm' => sprintf('%02d',$mins), 's' => sprintf('%02d',$secs));
		}
	}
	return $to_seconds ? 0 : array('h' => '00', 'm' => '00', 's' => '00');
}
?>