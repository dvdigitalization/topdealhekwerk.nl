<?php
/*
Title:		Dutch bank account number validation
File: 		inc/functions/validate_account_nr.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization

Comments:	Return true if valid bank account number

*/

function validate_account_nr($account_nr){

	$account_nr = preg_replace ('/[^0-9]/', '', $account_nr); 	//Strip non numerical
	$c_sum 		= 0;                            				//Initialize sum to zero
	$pos 		= 9;                             				//The amount of positions an account nr should have

	for ($i = 0; $i < strlen($account_nr); $i++) {
			$c_sum += $account_nr[$i] * $pos;       //Calculate sum product of the number and it's position
			$pos--;                          		//To the next position
	}
	
	$postbank 	= 	($pos > 1) and ($pos < 7);    	//Postbank if rest of positions between 1 and 7
	$mod 		= 	$c_sum % 11;                    //Calculate modulus of sum product divided by 11
	
	if ($postbank or !($pos or $mod)) {				//Return true if postbank or if no modulus and no position
		return $account_nr;
	}
	return false;
}

?>