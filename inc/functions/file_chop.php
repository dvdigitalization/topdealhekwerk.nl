<?php
/*
Title:		File chop function
File: 		inc/functions/file_chop.php
Version: 	v2.02
Author:		NicholasSolutions
Contact:	http://www.nicholassolutions.com/
Adapted by:	Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization

Comments:	Chop a file in smaller chunks

*/

function file_chop($source_file, $chunk_size, $chunks_directory) {
	
	//Prepare the chunks directory
	if (substr($chunks_directory,-1) != '/') {
		$chunks_directory .= '/';
	}
	
	//Check the chunks directory
	if (!is_dir($chunks_directory)) {
		trigger_error("Directory $chunks_directory doesn't exist! Can't save file chunks here...", E_USER_WARNING);
		return false;
	}
	elseif (!is_writable($chunks_directory)) {
		trigger_error("Directory $chunks_directory is not writable! Can't save file chunks here...", E_USER_WARNING);
		return false;	
	}

	//Get size of the source ifle
	$size = filesize($source_file);
	
	//Find number of full $chunk_size byte portions
	$num_chunks = floor($size/$chunk_size);
		
	//Open the file for reading
	$file_handle = fopen($source_file, 'rb');
	
	//Initialize the array of chunks
	$chunks = array();
	
	//Go over all the chunks needed
	for ($c=0; $c < $num_chunks; $c++) {
		
		//Prepare the chunk filename and open the handle
		$chunks[$c] 	= 	basename($source_file).'.parts.'.($c+1);
		$chunk_handle 	= 	fopen($chunks_directory.$chunks[$c], 'w');
		
		//Write the data to the chunk file 1k at a time
		while((ftell($chunk_handle) + 1024) <= $chunk_size) {
			fwrite($chunk_handle, fread($file_handle, 1024));
		}
		
		//Write leftovers
		if(($leftover = $chunk_size - ftell($chunk_handle)) > 0 ){
			fwrite($chunk_handle, fread($file_handle, $leftover));
		}
		
		//Close the chunk handle
		fclose($chunk_handle);
	}
	
	//Leftovers?
	if (($leftover = $size - ftell($file_handle)) > 0) {
		$chunks[$num_chunks] 	= 	basename($source_file).'.parts'.($num_chunks + 1);
		$chunk_handle 			= 	fopen($chunks_directory.$chunks[$num_chunks], 'w');

		//Put all the rest in here
		while(!feof($file_handle)){
			fwrite($chunk_handle, fread($file_handle, 1024));
		}
		
		//Close the chunk handle
		fclose($chunk_handle);
	}
	
	//Close the file handle and return the chunk names
	fclose($file_handle);
	return $chunks;
} 
?>