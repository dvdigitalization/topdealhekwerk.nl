<?php
/*
Title:		Highlight a file.
File: 		inc/functions/highlight_filei.php
Version: 	v1.1.0
Author:		Aidan Lister
Contact:	aidan@php.net
Comments:	See http://aidanlister.com/repos/v/function.highlight_filei.php
*/

function highlight_filei($file, $return = false, $linenum = true, $funclink = true) {
    // Init
    $data = explode('<br />', file_get_contents($file));
    $start = '<span style="color: black;">';
    $end   = '</span>';
    $i = 1;
    $text = '';

    // Loop
    foreach ($data as $line) {
        $text .= $start . $i . ' ' . $end .
            str_replace("\n", '', $line) . "<br />\n";
        ++$i;
    }

    // Optional function linking
    if ($funclink === true) {
        $keyword_col = ini_get('highlight.keyword');
        $manual = 'http://www.php.net/function.';

        if (version_compare('5.0.0', PHP_VERSION) === -1) {
            $text = preg_replace(
                // Match a highlighted keyword
                '~([\w_]+)(\s*</span>)'.
                // Followed by a bracket
                '(\s*<span\s+style="color: ' . $keyword_col . '">\s*\()~m',
                // Replace with a link to the manual
                '<a href="' . $manual . '$1">$1</a>$2$3', $text);
        } else {
            $text = preg_replace(
                // Match a highlighted keyword
                '~([\w_]+)(\s*</font>)'.
                // Followed by a bracket
                '(\s*<font\s+color="' . $keyword_col . '">\s*\()~m',
                // Replace with a link to the manual
                '<a href="' . $manual . '$1">$1</a>$2$3', $text);
        }
    }
    
    // Return mode
    if ($return === false) {
        echo $text;
    } else {
        return $text;
    }
}

?>