<?php
/*
Title:		Array splitter function
File: 		inc/functions/array_split_equal.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization

Comments:	Split an array of items into equal sub-arrays

*/

function array_split_equal($array, $amount_of_arrays = 2, $threshold = 0) {
	
	$amount 		= 	count($array);
	$per_array		=	ceil($amount / $amount_of_arrays);
	$return_array	=	array();
	
	if ($threshold and $per_array < $threshold) {
		$per_array = $threshold;
	}
	
	for ($a = 1; $a <= $amount_of_arrays; $a++) {
		
		$temp_array	=	array();
		for ($b = 1; $b <= $per_array; $b++) {
			if (!count($array)) {
				break;
			}
			$temp_array[]	=	array_shift($array);
		}
		$return_array[]	=	$temp_array;
	}
	return $return_array;
}

?>