<?php
/*
Title:		Convert tabs to spaces
File: 		inc/functions/tabs_to_spaces.php
Version: 	v1.1.0
Author:		Aidan Lister
Contact:	aidan@php.net
*/

function tabs_to_spaces($text, $spaces = 4) {
	
    // Explode the text into an array of single lines
    $lines = explode("\n", $text);

    // Loop through each line
    foreach ($lines as $line) {

        // Break out of the loop when there are no more tabs to replace
        while (false !== $tab_pos = strpos($line, "\t")) {

            // Break the string apart, insert spaces then concatenate
            $start = substr($line, 0, $tab_pos);
            $tab   = str_repeat(' ', $spaces - $tab_pos % $spaces);
            $end   = substr($line, $tab_pos + 1);
            $line  = $start . $tab . $end;
        }

        $result[] = $line;
    }

    return implode("\n", $result);
}

?>