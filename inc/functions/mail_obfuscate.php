<?php
/*
Title:		Obfuscate an email address
File: 		inc/functions/mail_obfuscate.php
Version: 	v1.1.0
Author:		Aidan Lister
Contact:	aidan@php.net
Comments:	See http://aidanlister.com/repos/v/function.mail_obfuscate.php
*/

function mail_obfuscate($email, $text = '') {
    
	//Default text
    if (empty($text)) {
		$text = $email;
    }

    //Create string
    $string = sprintf('document.write(\'<a href="mailto:%s">%s</a>\');',
    htmlspecialchars($email),
    htmlspecialchars($text));

    //Encode    
    for ($encode = '', $i = 0; $i < strlen($string); $i++) {
        $encode .= '%' . bin2hex($string[$i]);
    }

    //Javascript
    $javascript = '<script language="javascript">eval(unescape(\'' . $encode . '\'))</script>';

    return $javascript;
}

?>