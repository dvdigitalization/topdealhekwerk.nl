<?php
/*
Title:		Index redirect away from forbidden boo boo no access directories
File: 		index.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization	
*/

header('Location: ../index.php');
exit;
?>