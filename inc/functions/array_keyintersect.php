<?php
/*
Title:		Array keyintersect function
File: 		inc/functions/array_keyintersect.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization

Comments:	Return an array of values (with corresponding key) from $array_values, of which the keys are in $array_keys

*/

function array_keyintersect($array_keys, $array_values) {
	
	if (!is_array($array_keys) or !is_array($array_values)) {
		return array();
	}
	
	$return_values = array();
	foreach ($array_keys as $key) {
		$return_values[$key] = $array_values[$key];
	}
	
	return $return_values;
}

?>