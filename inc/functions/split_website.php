<?php
/*
Title:		Split a website input into the name and a valid url
File: 		inc/functions/split_website.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

define('WEBSITE_NAME',	1);
define('WEBSITE_URL',	2);
define('WEBSITE_ARRAY',	3);

function split_website($website, $return_type = WEBSITE_ARRAY) {
	
	if (!$website) {
		switch ($return_type) {
			case WEBSITE_NAME:
			case WEBSITE_URL:
				return '';
			case WEBSITE_ARRAY:
			default:
				return array('', '');
		}		
	}
	
	$website = strtolower($website);
	
	if (substr($website,0,7) != 'http://') {
		$site_url 	= 'http://'.$website;
		$site_name	= $website;
	}
	else {
		$site_url 	= $website;
		$site_name	= substr($website,7);
	}
	
	switch ($return_type) {
		case WEBSITE_NAME:
			return $site_name;
		case WEBSITE_URL:
			return $site_url;
		case WEBSITE_ARRAY:
		default:
			return array($site_url, $site_name);
	}
}

?>