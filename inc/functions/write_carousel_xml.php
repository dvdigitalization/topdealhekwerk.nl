<?php
/*
Title:		Flash carousel XML writer
File: 		inc/functions/write_carousel_xml.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization

Comments:	For use with the flash image carousel
*/

function write_carousel_xml($image_urls, $xml_file = 'carousel.xml') {
	
	global $_sep;
	
	$prefix 	= 	defined('CMS_PATH_PREFIX') ? CMS_PATH_PREFIX : SITE_PATH_PREFIX;
	$xml_file	=	$prefix.$xml_file;
	
	if (!is_array($image_urls) or !file_exists($xml_file) or !is_writable($xml_file)) {
		return false;
	}
	
	$images = array();
	foreach ($image_urls as $image_url) {
		$images[] = '<img url="'.$image_url.'"></img>';
	}
	
	$xml = '<?xml version="1.0" encoding="ISO-8859-1"?>'."\n".'<img_list>'."\n".implode("\n",$images)."\n".'</img_list>';
	
	write_to_file($xml_file, $xml);
	return true;
}

?>