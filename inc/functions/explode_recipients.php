<?php
/*
Title:		Explode recipients from a string to a clean array of emails, filtering for uniques
File: 		inc/functions/explode_recipients.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

function explode_recipients($string_recipients) {
	
	$array_recipients = array();
	
	//Change comma's to semi colons
	$string_recipients 	= str_replace(',',';',$string_recipients);
	$array_recipients	= explode(';',$string_recipients);
	
	//Clean up
	foreach ($array_recipients as $key => $recipient) {
		$array_recipients[$key] = trim($recipient);
	}
	
	//Filter uniques
	$array_recipients = array_unique($array_recipients);
	
	return $array_recipients;
}

?>