<?php
/*
Title:		Custom parser functions
File: 		inc/functions/custom_parser.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization

Comments:	Custom parser functions (probably deprecated since the usage of FCKeditor, but might still be useful sometimes.

*/

function searchForInternalLinks(&$text, $offset) {
		
	if ($start_pos = stripos($text, '[link', $offset)) {
		$end_pos = stripos($text, '[/link]', $start_pos);
		$end_of_link = strpos($text, ']', $start_pos);
		
		$link 	= substr($text, $start_pos + 6, ($end_of_link - $start_pos - 6));
		$title	= substr($text, $end_of_link + 1, ($end_pos - $end_of_link - 1));
		
		$text 	= substr_replace($text, "<a href='?pid=$link'>$title</a>", $start_pos, ($end_pos-$start_pos+7));
		
		searchForInternalLinks($text, $end_pos);
	}
}

function searchForExternalLinks(&$text, $offset) {
	
	if ($start_pos = stripos($text, '[link', $offset)) {
		$end_pos = stripos($text, '[/link]', $start_pos);
		$end_of_link = strpos($text, ']', $start_pos);
		
		$link 	= substr($text, $start_pos + 6, ($end_of_link - $start_pos -6));
		$title	= substr($text, $end_of_link + 1, ($end_pos - $end_of_link - 1));
		
		$text 	= substr_replace($text, "<a href='$link'>$title</a>", $start_pos, ($end_pos-$start_pos+7));
		
		searchForExternalLinks($text, $end_pos);
	}
}

function searchForColorTags(&$text, $offset) {
		
	if ($start_pos = stripos($text, '[color', $offset)) {
		$end_pos = stripos($text, '[/color]', $start_pos);
		$end_of_tag = strpos($text, ']', $start_pos);
		
		$color 		= substr($text, $start_pos + 7, ($end_of_tag - $start_pos - 7));
		$content	= substr($text, $end_of_tag + 1, ($end_pos - $end_of_tag - 1));
		
		$text 	= substr_replace($text, "<span style='color: $color;'>$content</span>", $start_pos, ($end_pos-$start_pos+8));
		
		searchForColorTags($text, $end_pos);
	}
}

function searchForLeftImages(&$text, $offset) {
		
	if ($start_pos = stripos($text, '[img_left', $offset)) {
		$end_pos = stripos($text, '[/img_left]', $start_pos);
		$end_of_link = strpos($text, ']', $start_pos);
		
		$image 	= substr($text, $start_pos + 10, ($end_of_link - $start_pos - 10));
		$title	= substr($text, $end_of_link + 1, ($end_pos - $end_of_link - 1));

		list ($width, $height) = @getimagesize('gfx/uploaded/'.$image);
		
		$text 	= substr_replace($text, "<div class='image_left'><img src='gfx/uploaded/$image' alt='$title'/></div>",
								 $start_pos, ($end_pos-$start_pos+11));
		
		searchForLeftImages($text, $end_pos);
	}
}

function searchForRightImages(&$text, $offset) {
		
	if ($start_pos = stripos($text, '[img_right', $offset)) {
		$end_pos = stripos($text, '[/img_right]', $start_pos);
		$end_of_link = strpos($text, ']', $start_pos);
		
		$image 	= substr($text, $start_pos + 11, ($end_of_link - $start_pos - 11));
		$title	= substr($text, $end_of_link + 1, ($end_pos - $end_of_link - 1));
		
		list ($width, $height) = @getimagesize('gfx/uploaded/'.$image);
		
		$text 	= substr_replace($text, "<div class='image_right'><img src='gfx/uploaded/$image' alt='$title'/></div>",
								 $start_pos, ($end_pos-$start_pos+12));
		
		searchForRightImages($text, $end_pos);
	}
}

function searchForCenterImages(&$text, $offset) {
		
	if ($start_pos = stripos($text, '[img_center', $offset)) {
		$end_pos = stripos($text, '[/img_center]', $start_pos);
		$end_of_link = strpos($text, ']', $start_pos);
		
		$image 	= substr($text, $start_pos + 12, ($end_of_link - $start_pos - 12));
		$title	= substr($text, $end_of_link + 1, ($end_pos - $end_of_link - 1));

		list ($width, $height) = @getimagesize('gfx/uploaded/'.$image);
		
		$text 	= substr_replace($text, "<div class='image_center'><img src='gfx/uploaded/$image' alt='$title'/></div>",
								 $start_pos, ($end_pos-$start_pos+13));
		
		searchForCenterImages($text, $end_pos);
	}
}

function parseCustomTags($contents, $nl2br = true) {
	
	$contents = str_replace('[b]','<b>',$contents);
	$contents = str_replace('[/b]','</b>',$contents);
	$contents = str_replace('[i]','<i>',$contents);
	$contents = str_replace('[/i]','</i>',$contents);
	$contents = str_replace('[u]','<u>',$contents);
	$contents = str_replace('[/u]','</u>',$contents);
	
	$contents = str_replace('[h1]','<h1>',$contents);
	$contents = str_replace('[/h1]','</h1>',$contents);
	$contents = str_replace('[h2]','<h2>',$contents);
	$contents = str_replace('[/h2]','</h2>',$contents);		
	$contents = str_replace('[h3]','<h3>',$contents);
	$contents = str_replace('[/h3]','</h3>',$contents);
	$contents = str_replace('[h4]','<h4>',$contents);
	$contents = str_replace('[/h4]','</h4>',$contents);		
	$contents = str_replace('[h5]','<h5>',$contents);
	$contents = str_replace('[/h5]','</h5>',$contents);
	$contents = str_replace('[h6]','<h6>',$contents);
	$contents = str_replace('[/h6]','</h6>',$contents);		

	$contents = str_replace('[comic]',"<span style='font-family: Comic Sans MS;'>",$contents);
	$contents = str_replace('[/comic]','</span>',$contents);	

	$contents = str_replace("[tab]",'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', $contents);

	$contents = str_replace('[bull]','&bull;',$contents);	
	$contents = str_replace('[raquo]','&raquo;',$contents);	
	$contents = str_replace('[laquo]','&laquo;',$contents);
	$contents = str_replace('[euro]','&euro;',$contents);		
			
	$contents = ' '.$contents;

	if ($nl2br) $contents = nl2br($contents);	
	
	searchForInternalLinks($contents, 0);
	searchForLeftImages($contents, 0);
	searchForRightImages($contents, 0);
	searchForCenterImages($contents, 0);
	searchForColorTags($contents, 0);
			
	$contents = ereg_replace(	" [[:alpha:]]+://[^<>[:space:]]+[[:alnum:]/]",
	                     		" <a href='\\0' target='_blank' title='Open website in een nieuw venster'>\\0</a>", $contents);
								 	
	return $contents;
}
?>