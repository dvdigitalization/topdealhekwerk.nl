<?php
/*
Title:		Cross reference / checkbox functions
File: 		inc/functions/checkbox_cross_ref.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

//Make an array for a checkbox list, based on cross-reference values in a database
function getCrossRef($db_table_cross, $e_field, $e_id, $c_field, $c_array = null, $extra_constraint = '') {
	
	$extra_constraint 	= $extra_constraint ? ' AND '.$extra_constraint : '';
	$checked_items 		= array();
	
	$res = eq("	SELECT $c_field AS id FROM $db_table_cross WHERE $e_field = '$e_id'	$extra_constraint ORDER BY $c_field ASC;");
	while ($myo = mfo($res)) {
		$checked_items[$myo->id] = $myo->id;
	}
	
	//If we get an array to compare against, we will use that for our return array
	if (is_array($c_array)) {
		if (defined('CMS_PATH_PREFIX')) {
			require_once(CMS_PATH_PREFIX.'inc/functions/array_keyintersect.php');	
		}
		else {
			require_once('inc/functions/array_keyintersect.php');
		}
		return array_keyintersect($checked_items, $c_array);
	}
	
	//Otherwise we just return an array with the values
	else return $checked_items;
}

//Set cross-reference items from a checkbox list
function insCrossRef($db_table_cross, $e_field, $e_id, $c_field, $c_array, $extra_constraint = '') {

	//Delete existing references
	$extra_constraint = $extra_constraint ? ' AND '.$extra_constraint : '';
	delRec($db_table_cross,"$e_field = '$e_id' $extra_constraint");

	//If no array found, our work is done
	if (!is_array($c_array)) {
		return true;
	}
	//Insert new references
	else {
		foreach	($c_array as $c_id) {
			insRec($db_table_cross, array($e_field, $c_field), array($e_id, $c_id));
		}
		return true;
	}
}

?>