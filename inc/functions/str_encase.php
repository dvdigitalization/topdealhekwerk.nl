<?php
/*
Title:		Prefix and/or affix every line, in a block of text.
File: 		inc/functions/str_encase.php
Version: 	v1.1.0
Author:		Aidan Lister
Contact:	aidan@php.net
Comments:	See http://aidanlister.com/repos/v/function.str_encase.php
*/

function str_encase($text, $prefix = '', $affix = '') {
	
    // Handle line endings correctly
    $str = preg_replace('#(\r\n?|\n)#s', $affix . '\1' . $prefix, $text);

    // Finish string
    return $prefix . $str . $affix; 
}

?>