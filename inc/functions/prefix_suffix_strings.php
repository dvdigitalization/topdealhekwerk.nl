<?php
/*
Title:		Add a prefix and suffix to an array of strings, or a single string
File: 		inc/functions/prefix_suffix_strings.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

function prefix_suffix_strings(&$string, $prefix = '', $suffix = '') {
    
    if (is_array($string)) {
		foreach ($string as $key => $item) {
			$string[$key] = $prefix.$item.$suffix;
		}
	}
	else {
		$string = $prefix.$string.$suffix;
	}
}

?>