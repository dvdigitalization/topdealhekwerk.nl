<?php
/*
Title:		Chop a string into a smaller string.
File: 		inc/functions/str_chop.php
Version: 	v1.1.0
Author:		Aidan Lister
Contact:	aidan@php.net
Comments:	See http://aidanlister.com/repos/v/function.str_chop.php
*/

function str_chop($string, $length = 60, $center = false, $append = null) 
{
	// Set the default append string
	if ($append === null) {
		$append = ($center === true) ? ' ... ' : ' ...';
	}

	// Get some measurements
	$len_string = strlen($string);
	$len_append = strlen($append);

	// If the string is longer than the maximum length, we need to chop it
	if ($len_string > $length) {
		// Check if we want to chop it in half
		if ($center === true) {
			// Get the lengths of each segment
			$len_start = $length / 2;
			$len_end = $len_start - $len_append;

			// Get each segment
			$seg_start = substr($string, 0, $len_start);
			$seg_end = substr($string, $len_string - $len_end, $len_end);

			// Stick them together
			$string = $seg_start . $append . $seg_end;
		} else {
            // Otherwise, just chop the end off
			$string = substr($string, 0, $length - $len_append) . $append;
		}
	}

	return $string;
}

?>