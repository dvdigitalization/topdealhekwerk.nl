<?php
/*
Title:		Turn HTML into plain text
File: 		inc/functions/html_to_text.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

function html_to_text($html){
	$search = array('@<script[^>]*?>.*?</script>@si',  // Strip out javascript
	               '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
	               '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
	               '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA
	);
	
	$text = preg_replace($search, '', $html);
	return $text;
}

?>