<?php
/*
Title:		Change the extension of a filename
File: 		inc/functions/change_file_extension.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

function change_file_extension($filename, $new_extension) {
	
	$parts = explode('.', $filename);
	if (is_array($parts) and count($parts) > 1) {
		array_pop($parts);
		$parts[] = $new_extension;
		return implode('.', $parts);
	}
    else return $filename;
}

?>