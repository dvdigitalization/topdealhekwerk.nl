<?php
/*
Title:		Parse external links
File: 		inc/functions/parse_external_links.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

function parse_external_links($contents) {
	$contents = ereg_replace(	" [[:alpha:]]+://[^<>[:space:]]+[[:alnum:]/]",
                     			" <a href='\\0' target='_blank' title='Open deze website in een nieuw venster'>\\0</a>", $contents);
    return $contents;
}

?>