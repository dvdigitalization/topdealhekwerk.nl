<?php
/*
Title:		See if a file exists, within a constrained set of extensions
File: 		inc/functions/get_file_from_extensions.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

function get_file_from_extensions($file_url_template, $extensions) {
	
	//Image extension check
 	foreach ($extensions as $extension) {
 		$file_url = $file_url_template.$extension;
		if (file_exists($file_url)) {
			return array($file_url, $extension);
		}
	}
	return false;
}

?>