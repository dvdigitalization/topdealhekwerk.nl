<?php
/*
Title:		Banner redirection and click registration
File: 		banner_redirect.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization

Template PHP code for site_tags.php:

// *** Banners
$db_table_banners	=	DIGI_DB_PREFIX.'banners';
$banners			=	array();
$banner_dir			=	'gfx/banners/';

$res = eq("	SELECT id, banner_url, name, views, link FROM $db_table_banners
			WHERE active='1' AND (views < max_views OR max_views = 0) AND (clicks < max_clicks OR max_clicks = 0)
			ORDER BY RAND() ASC LIMIT 2;");
while ($myo = mfo($res)) {
	$link = 	$cfg['USE_REWRITE_ENGINE'] ?
				'banner-'.$myo->id.'-'.rewriteSafe($myo->name).'.html' :
				'banner-redirect.php?banner_id='.$myo->id;
	$banners[]	=	"<a href='$link' title='$myo->name'><img src='$banner_dir$myo->banner_url' border='0px' alt='$myo->link'/></a>";
	upRec($db_table_banners, $myo->id, 'views', $myo->views + 1);
}

if (count($banners)) {
	$html = str_replace('[BANNER]',			array_pop($banners),		$html);
}
else {
	$html = str_replace('[BANNER]',			'',							$html);
}
//etc
*/

require_once('inc/site_ajax_header.php');

//Settings
$db_table	=	DIGI_DB_PREFIX.'banners';
$banner_id	=	isset($_GET['banner_id']) ? (int) $_GET['banner_id'] : 0;
$banner		=	getMyo($db_table,$banner_id,array('clicks','link'));

//Save clicks in session
if (!isset($_SESSION['banner_clicked']) or !is_array($_SESSION['banner_clicked'])) {
	$_SESSION['banner_clicked'] = array();
}

//Do we have a valid banner
if ($banner and $banner->link) {

	//Only one registration per session per banner
	if (!in_array($banner_id,$_SESSION['banner_clicked'])) {
		upRec($db_table,$banner_id,'clicks',$banner->clicks+1);
	}
	$_SESSION['banner_clicked'][] = $banner_id;

	//Redirect
	header('Location: '.$banner->link);
}
else {
	header('Location: index.html');
}
?>