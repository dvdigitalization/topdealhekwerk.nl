<?php
/*
Title:		Manage newsletter subscribers.
File: 		cms_newsletter_subscribers.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

/******************************************************************************************
 * Settings
 ***/

// *** Required files
require_once('inc/cms_header.php');
require_once(CMS_PATH_PREFIX.'classes/class_DataTable.php');

// *** Extra database table definitions
$db_table_newsletters	=	'newsletters';

// *** Object definition
$object					=	'Aanmelding';
$object_lc 				= 	'aanmelding';
$plural					=	'Aanmeldingen';
$plural_lc				=	'aanmeldingen';
$adjective_e			=	'e';
$ident					=	'Deze';

// *** Sorting and pages functionality
$default_sort_option	=	'id';
$default_sort_method	=	'DESC';
$secondary_sort_option	=	'';
$secondary_sort_method	=	'';
$valid_sort_options		=	array('id','first_name','last_name','language','email','timestamp','newsletter');

// *** Search constraints
if (!empty($search_query)) {
	$search				=	array();
	$search[]			=	"first_name LIKE '%$search_query%'";
	$search[]			=	"last_name LIKE '%$search_query%'";
	$search[]			=	"email LIKE '%$search_query%'";
	$where[]			=	'('.implode(' OR ', $search).')';
}

// *** Message definitions
$msg_added				=	'Aanmelding toegevoegd.';
$msg_editted			=	'Aanmelding bijgewerkt.';
$msg_deleted			=	'Aanmelding verwijderd.';
$msg_deleted_m 			= 	'Aanmeldingen verwijderd.';
$msg_delete_confirm		= 	'Deze aanmelding zeker weten verwijderen?';
$msg_delete_confirm_m 	= 	'Deze aanmeldingen zeker weten verwijderen?';
$msg_activated			=	'Deze aanmelding zal vanaf nu weer nieuwsbrieven ontvangen.';
$msg_activated_m		=	'Deze aanmeldingen zullen vanaf nu weer nieuwsbrieven ontvangen.';
$msg_deactivated		=	'Deze aanmelding zal vanaf nu geen nieuwsbrieven meer ontvangen.';
$msg_deactivated_m		=	'Deze aanmeldingen zullen vanaf nu geen nieuwsbrieven meer ontvangen.';
$msg_exists				=	'Dit email adres staat al in de lijst van aanmeldingen.';
$msg_invalid_email		=	'Dit is geen geldig email adres.';
$msg_upload_error		=	'Er ging iets mis met het uploaden van het import bestand.';
$msg_process_error		=	'Dit bestand kon niet verwerkt worden. Wees er zeker van dat u een CSV of XML bestand gebruikt.';
$msg_no_data			=	'Er kon geen geldige data worden gevonden in dit bestand.';
$msg_imported			=	'Er zijn [AMOUNT] aanmeldingen geimporteerd.';

// *** Sorting and pages and link appendix
require_once(CMS_PATH_PREFIX.'inc/sorting_and_pages.php');
$_app					=	$page_appendix.'&'.$sort_appendix.'&'.$link_appendix;

/******************************************************************************************
 * POST and GET Cases
 ***/

if (isset($_POST['add_finish']) or isset($_POST['edit_finish'])) {

	foreach(array_keys($_POST) as $key) {
		evalAll($_POST[$key]);
	}
}

if (isset($_POST['add_finish'])) {

	if (!$email = validateEmail($_POST['email'])) {
		$Message->set($msg_invalid_email, CRITICAL);
		$_GET['add'] = 1;
	}
	elseif (countResults($db_table, "email = '$email'")) {
		$Message->set($msg_exists, CRITICAL);
		$_GET['add'] = 1;
	}
	else {
		$db_id = insRec($db_table,	array('title','first_name','last_name','email','language','to_process'),
									array($_POST['title'],$_POST['first_name'],$_POST['last_name'],$email,$_POST['language'],1));

    	cmsLog("Nieuwsbrief aanmelding $_POST[last_name], $_POST[email] (#$db_id) toegevoegd.");
    	$Message->set($msg_added);
	}

}

if (isset($_POST['edit_finish']) and $db_id = (int) $_POST['id']) {

	if (!$email = validateEmail($_POST['email'])) {
		$Message->set($msg_invalid_email, CRITICAL);
		$_GET['edit'] = $db_id;
	}
	elseif (countResults($db_table, "email = '$email' AND id <> '$db_id'")) {
		$Message->set($msg_exists, CRITICAL);
		$_GET['edit'] = $db_id;
	}
	else {
    	upRec($db_table,$db_id,	array('title','first_name','last_name','email','language'),
								array($_POST['title'],$_POST['first_name'],$_POST['last_name'],$email,$_POST['language']));

    	cmsLog("Nieuwsbrief aanmelding $_POST[last_name], $_POST[email] (#$db_id) gewijzigd.");
    	$Message->set($msg_editted);
    }
}

// *** Delete confirmation
if (isset($_GET['delete']) and $id_holder = $_GET['delete']) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			$Message->setConfirmMultiple($msg_delete_confirm_m, 'delete',$db_ids,'last_name');
		}
	}
	else if ($db_id = (int) $id_holder) {
		$Message->setConfirm($msg_delete_confirm,'delete',$db_id,'last_name');
	}
}

// *** Delete
if (isset($_GET['delete_confirm']) and $id_holder = $_GET['delete_confirm'] and noBrowse()) {

	if (isset($_GET['multiple_confirm']) and is_array($id_holder)) {

		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			delRec($db_table,$db_ids);
			cmsLog("Meerdere $plural_lc verwijderd.");
			$Message->set($msg_deleted_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		delRec($db_table,$db_id);
		cmsLog("$object (#$db_id) verwijderd.");
		$Message->set($msg_deleted);
	}
}

// *** Acivate
if (isset($_GET['subscribe']) and $id_holder = $_GET['subscribe'] and noBrowse()) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			upRec($db_table,$db_ids,array('newsletter','to_process'),array(1,1));
			cmsLog("Meerdere $plural_lc geactiveerd");
			$Message->set($msg_activated_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		upRec($db_table,$db_id,array('newsletter','to_process'),array(1,1));
		cmsLog("$object (#$db_id) geactiveerd");
		$Message->set($msg_activated);
	}
}

// *** De-activate
if (isset($_GET['unsubscribe']) and $id_holder = $_GET['unsubscribe'] and noBrowse()) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			upRec($db_table,$db_ids,array('newsletter','to_process'),array(0,1));
			cmsLog("Meerdere $plural_lc op non-actief gezet");
			$Message->set($msg_deactivated_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		upRec($db_table,$db_id,array('newsletter','to_process'),array(0,1));
		cmsLog("$object (#$db_id) op non-actief gezet");
		$Message->set($msg_deactivated);
	}
}

// *** Import
if (isset($_POST['import_finish']) and !empty($_FILES['import_data']['name'])) {

	//Upload the file
	if (($result = uploadFile('import_data', $upload_dir, 'import', $chmod, false, true, array('xml','csv'))) !== false) {

		//Get file info
		list($file_name, $file_extension) = $result;
		$file_type = strtoupper(substr($file_extension,1));

		//Set the item and props definitions
		$item	=	'subscriber';
		$props	=	array('title','first_name','last_name','email','language');

		//Set up import engine
		require_once(CMS_PATH_PREFIX.'classes/class_ImportEngine.php');
		$ImportEngine = new ImportEngine($upload_dir.$_sep.$file_name, $file_type, $item, $props);

		//Process data
		if ($ImportEngine->process()) {

			if ($count = $ImportEngine->get_count()) {

				//Get data and import
				while ($values = $ImportEngine->get_element()) {
					if (empty($values[2])) {
						$values[2] = $_lang;
					}
					insRec($db_table, $props, $values);
				}

				//Log and message
				cmsLog("$count $plural geimporteerd.");
				$Message->set(str_replace('[AMOUNT]', $count, $msg_imported));
			}
			else {
				$Message->set($msg_no_data, WARNING);
			}

			//Delete the temporary file
			@unlink($upload_dir.$_sep.$file_name);
		}
		else {
			$Message->set($msg_process_error, CRITICAL);
		}
	}
	else {
		$Message->set($msg_upload_error.'<br/><br/>'.$_error, WARNING);
	}
}

/******************************************************************************************
 * Add and edit options
 ***/

if (isset($_GET['add'])) {

	$pre_html = "
		<h3>Aanmelding handmatig toevoegen:</h3><hr/><br/>
        <form name='add_form' method='post' action='$_file'>
        $page_and_sort_inputs
        <table cellspacing='0px' cellpadding='1px' border='0px'>

        <tr>
			<td width='150px'>Voornaam:</td>
			<td><input type='text' name='first_name' class='input_regular' /></td>
		</tr>
        <tr>
			<td>Achternaam:</td>
			<td><input type='text' name='last_name' class='input_regular' /></td>
		</tr>
		<tr>
			<td>Aanhef:</td>
			<td>".makeSelectbox('title', $SEX_ARRAY, 0)."</td>
		</tr>
        <tr>
			<td>Email:</td>
			<td><input type='text' name='email' class='input_regular' /></td>
		</tr>
        <tr>
			<td>Taal:</td>
			<td>".makeSelectbox('language',$installed_languages,$_lang)."</td>
		</tr>
        <tr>
			<td>&nbsp;</td><td><br/><input type='submit' class='input_submit' name='add_finish' value='Toevoegen' /></td>
		</tr>

        </table>
        </form><br/>
    ";
}

if (isset($_GET['edit']) and $db_id = (int) $_GET['edit']) {

    $res = eq("SELECT name,email,language FROM $db_table WHERE id = '$db_id';");
	$myo = mfo($res);

	$pre_html = "
		<h3>Aanmelding bijwerken:</h3><hr/><br/>
        <form name='edit_form' method='post' action='$_file'>
        <input type='hidden' name='id' value='$db_id' />
        $page_and_sort_inputs
        <table cellspacing='0px' cellpadding='1px' border='0px'>

        <tr>
			<td width='150px'>Voornaam:</td>
			<td><input type='text' name='first_name' value='$myo->first_name' class='input_regular' /></td>
		</tr>
        <tr>
			<td>Achternaam:</td>
			<td><input type='text' name='last_name' value='$myo->last_name' class='input_regular' /></td>
		</tr>
		<tr>
			<td>Aanhef:</td>
			<td>".makeSelectbox('title', $SEX_ARRAY, $myo->title)."</td>
		</tr>
        <tr>
			<td>Email:</td>
			<td><input type='text' name='email' value='$myo->email' class='input_regular' /></td>
		</tr>
        <tr>
			<td>Taal:</td>
			<td>".makeSelectbox('language',$installed_languages,$myo->language)."</td>
		</tr>
		<tr>
			<td>&nbsp;</td><td><br/><input type='submit' class='input_submit' name='edit_finish' value='Opslaan' /></td>
		</tr>

        </table>
        </form><br/>
    ";
}

if (isset($_GET['import'])) {

	$pre_html = "
		<h3>Aanmeldingen importeren:</h3><hr/>
		<p>Met onderstaand formulier kunt u nieuwsbrief aanmeldingen importeren. U kunt hiervoor een CSV of XML bestand gebruiken.
		<br/><br/>
		Voor een CSV bestand geldt dat er in ieder geval 4 kolommen dienen te zijn, gescheiden met een komma of een puntkomma. De eerste
		<br/>kolom dient de aanhef te bevatten, de tweede de voornaam, de derde de achternaam en de vierde kolom het email adres.
		Optioneel kunt u ook een vijfde kolom toevoegen<br/>voor de taal. Geldige waarden voor de taal zijn:
		".implode(', ',array_keys($installed_languages)).".</p>
		<br/>

		<form name='import_form' method='post' action='$_file' enctype='multipart/form-data'>
		Bestand: <input type='file' name='import_data' class='input_file'/>
		<input type='submit' class='input_submit' name='import_finish' value='Importeren' />
		</form>
		<br/><br/>
	";
}

/******************************************************************************************
 * Page contents
 ***/

 //Page title HTML
$html = "
	<a href='$_file'><img src='$site_icon' border='0px' class='cms_icon'/></a>
	<h6>
		<a href='$_file'>overzicht</a> &nbsp;
		$img[user_add] <a href='$_file?add'>nieuwe aanmelding toevoegen</a> &nbsp;
		$img[users_add] <a href='$_file?import'>aanmeldingen importeren</a>
	</h6>
	<br/>".$Message->get()."
	<br/><br/>$pre_html
";

//Get items for this page and this sorting method
$data_rows = array();
$res = eq("	SELECT 		id, first_name, last_name, language, email, timestamp, newsletter
			FROM 		$db_table $where_clause
			ORDER BY 	$order_clause
			LIMIT 		$limit_clause
		;");

while ($myo = mfo($res)) {

	//Invalid language fix
	if (!isset($installed_languages[$myo->language])) {
		upRec($db_table, $myo->id, 'language', reset($installed_languages));
		$myo->language = reset($installed_languages);
	}

	$data_rows[]	= array(

		//This MUST be the id
		$myo->id,

		//All desired other fields
		$myo->id,
		$myo->last_name,
		$myo->first_name,
		"<a href='$mailer$myo->email' title='Stuur deze persoon een email'>$myo->email</a>",
		$installed_languages[$myo->language],
		$myo->newsletter ?  	"<a href='$_file?unsubscribe=$myo->id&$_app' title='Aanmelding afmelden'>$img[mail_ok]</a>"
						 :		"<a href='$_file?subscribe=$myo->id&$_app' title='Aanmelding aanmelden'>$img[mail_del]</a>",
		time2date($myo->timestamp),

		//Options
		"	<a href='$_file?edit=$myo->id&$_app' title='Aanmelding bijwerken'>$img[edit]</a>
			<a href='$_file?delete=$myo->id&$_app' title='Aanmelding verwijderen'>$img[user_del]</a>
		"
	);
}

//Output data table
$DataTable = new DataTable();
$DataTable->setMarkAllOptions(	array('verwijder','delete',$plural.' verwijderen'),
								array('meld aan','subscribe',$plural.' aanmelden'),
								array('meld af','unsubscribe',$plural.' afmelden'));
$DataTable->showSearch();
$DataTable->setDataRows($data_rows);

$DataTable->setLegendRow(
	array('#','Achternaam','Voornaam','Email','Taal','Actief','Aangemeld','Opties'),
	array(60,0,100,240,50,60,100,40),
	array('id','last_name','first_name','email','language','newsletter','timestamp')
);

echo $html.$DataTable->getHTML();

require_once('inc/cms_footer.php');
?>