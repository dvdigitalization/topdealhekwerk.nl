<?php
/*
Title:		Products management [TOPDEAL]
File: 		cms_topdeal_products.php
Version: 	v2.20
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

/******************************************************************************************
 * Settings
 ***/

// *** Requred files
require_once('inc/cms_header.php');
require_once(CMS_PATH_PREFIX.'classes/class_DataTable.php');

// *** Database table definitions (naming kept the same throughout the product system)
$db_table_cats			=	DIGI_DB_PREFIX.'topdeal_categories';
$db_table_props			=	DIGI_DB_PREFIX.'topdeal_properties';
$db_table_prop_values	=	DIGI_DB_PREFIX.'topdeal_prop_values';
$db_table_colors		=	DIGI_DB_PREFIX.'topdeal_colors';

// *** VAT
$standard_vat			=	validateFloat(trim($cfg['PRODUCTS_STANDARD_VAT']));

// *** Unset session
if (isset($_GET['category_id'])) {
	unset($_SESSION['category_id']);
}

// *** Category ID selection
if ((isset($_GET['category_id']) and $category_id = (int) $_GET['category_id']) or
	(isset($_SESSION['category_id']) and $category_id = (int) $_SESSION['category_id'])) {
	$where[]					=	"category_id = '$category_id'";
	$_SESSION['category_id'] 	= 	$category_id;
}
else {
	$category_id	=	0;
	$where[]		=	"1 > 2";
}

// *** Sorting and pages functionality
$default_sort_option	=	'product_order';
$default_sort_method	=	'ASC';
$secondary_sort_option	=	'';
$secondary_sort_method	=	'';
$valid_sort_options		=	array('product_no','id','active','times_ordered','stock','ean_code');

// *** Object definition
$object					=	'Product';
$object_lc 				= 	'product';
$plural					=	'Producten';
$plural_lc				=	'producten';
$adjective_e			=	'';
$ident					=	'Dit';

// *** Search constraints
if (!empty($search_query)) {
	$search				=	array();
	$search[]			=	"product_no LIKE '%$search_query%'";
	$search[]			=	"ean_code LIKE '%$search_query%'";
	$where[]			=	'('.implode(' OR ', $search).')';
}

// *** Messages
$msg_added				=	$object.' toegevoegd.';
$msg_editted			=	$object.' bijgewerkt.';
$msg_deleted			=	$object.' verwijderd.';
$msg_deleted_m 			= 	$object.' verwijderd.';
$msg_delete_confirm		= 	$ident.' '.$object_lc.' zeker weten verwijderen?';
$msg_delete_confirm_m 	= 	'Deze '.$plural_lc.' zeker weten verwijderen?';
$msg_activated			=	$object.' geactiveerd.';
$msg_activated_m		=	$object.' geactiveerd.';
$msg_deactivated		=	$object.' op non-actief gezet.';
$msg_deactivated_m		=	$object.' op non-actief gezet.';
$msg_on_sale			=	$object.' in de aanbieding gezet.';
$msg_on_sale_m			=	$object.' in de aanbieding gezet.';
$msg_off_sale			=	$object.' uit de aanbieding gehaald.';
$msg_off_sale_m			=	$object.' uit de aanbieding gehaald.';
$msg_process_error		=	'Dit bestand kon niet verwerkt worden. Wees er zeker van dat u een CSV bestand gebruikt.';
$msg_no_data			=	'Er kon geen geldige data worden gevonden in dit bestand.';
$msg_imported			=	'Er zijn [AMOUNT] producten geimporteerd.';

// *** Sorting and pages and link appendix
require_once(CMS_PATH_PREFIX.'inc/sorting_and_pages.php');
$_app					=	$page_appendix.'&'.$sort_appendix.'&'.$link_appendix;

// *** Categories array
$CATEGORIES_ARRAY = array(0 => '');
$res = eq("SELECT id,name FROM $db_table_cats WHERE sub_cat_of='0' ORDER BY category_order ASC;");
while ($myo = mfo($res)) {
	$CATEGORIES_ARRAY[$myo->id] = $myo->name;
	$res_s = eq("SELECT id,name FROM $db_table_cats WHERE sub_cat_of='$myo->id' ORDER BY category_order ASC;");
	while ($myo_s = mfo($res_s)) {
		$CATEGORIES_ARRAY[$myo_s->id] = ' - '.$myo_s->name;
	}
}

// *** Colors array
$COLORS_ARRAY	=	array(0 => 'n.v.t.') + makeArray($db_table_colors, 'id', 'name','','item_order','ASC');

/******************************************************************************************
 * POST and GET Cases
 ***/

if (isset($_POST['add_finish']) or isset($_POST['edit_finish'])) {

	//Prepare properties array
	$properties_to_save = array();

	//Loop & cleanse post vars
	foreach(array_keys($_POST) as $key) {
		evalAll($_POST[$key]);

		//Find properties
		if (substr($key,0,9) == 'property_') {
			$property_id = (int) substr($key,9);
			if ($property_id) {
				$properties_to_save[$property_id] = $_POST[$key];
			}
		}
	}

	//Clear sale position of other products
	if ($_POST['sale_position'] and $sp = (int) $_POST['sale_position']) {
		upRec($db_table,"sale_position = '$sp'",'sale_position',0);
	}
}

if (isset($_POST['add_finish']) and $category_id) {

	//Product order
	$product_order = 1 + getMyo($db_table,"category_id='$category_id'",'product_order','product_order','DESC',1);

	//Insert product data
    $db_id = insRec($db_table,
				array(	'category_id','step_size','vat_percentage','stock','price','sale_price',
						'ean_code','color_id','product_order','normal_price','height','width','sale_position','delivery_days'),
				array(	$category_id,(float) $_POST['step_size'],
						validateFloat($_POST['vat_percentage']),(int) $_POST['stock'],dbAmount($_POST['price']),
						dbAmount($_POST['sale_price']),$_POST['ean_code'],$_POST['color_id'],$product_order,
						dbAmount($_POST['normal_price']),(int) $_POST['height'],(int) $_POST['width'],$_POST['sale_position'],
						(int) $_POST['delivery_days']));

	//Insert properties
	foreach ($properties_to_save as $property_id => $value) {
		insRec($db_table_prop_values, 	array('product_id','property_id','property_value'),
										array($db_id,$property_id,$value));
	}

	//Log
	cmsLog("$object '$_POST[ean_code]' (#$db_id) toegevoegd.");
	$Message->set($msg_added);
}

if (isset($_POST['edit_finish']) and isset($_POST['id']) and $db_id = (int) $_POST['id']) {

	//Update product data
	upRec($db_table, $db_id,
				array(	'step_size','vat_percentage','stock','price','sale_price',
						'ean_code','color_id','normal_price','height','width','sale_position','delivery_days'),
				array(	(float) $_POST['step_size'],
						validateFloat($_POST['vat_percentage']),(int) $_POST['stock'],dbAmount($_POST['price']),
						dbAmount($_POST['sale_price']),$_POST['ean_code'],$_POST['color_id'],dbAmount($_POST['normal_price']),
						(int) $_POST['height'],(int) $_POST['width'],$_POST['sale_position'], (int) $_POST['delivery_days']));

	//Delete old properties
	delRec($db_table_prop_values, "product_id='$db_id'");

	//Insert properties
	foreach ($properties_to_save as $property_id => $value) {
		insRec($db_table_prop_values, 	array('product_id','property_id','property_value'),
										array($db_id,$property_id,$value));
	}

	//Log
    cmsLog("$object '$_POST[ean_code]' (#$db_id) gewijzigd.");
    $Message->set($msg_editted);
}

// *** Delete confirmation
if (isset($_GET['delete']) and $id_holder = $_GET['delete']) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			$Message->setConfirmMultiple($msg_delete_confirm_m, 'delete',$db_ids,'product_no');
		}
	}
	else if ($db_id = (int) $id_holder) {
		$Message->setConfirm($msg_delete_confirm,'delete',$db_id,'product_no');
	}
}

// *** Delete action
if (isset($_GET['delete_confirm']) and $id_holder = $_GET['delete_confirm'] and noBrowse()) {

	if (isset($_GET['multiple_confirm']) and is_array($id_holder)) {

		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			delRec($db_table, $db_ids);
			delRec($db_table_prop_values, "product_id IN (".implode(',',$db_ids).")");
			cmsLog("Meerdere $plural verwijderd.");
			$Message->set($msg_deleted_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		delRec($db_table, $db_id);
		delRec($db_table_prop_values, "product_id = '$db_id'");
		$db_ids = array($db_id);
		cmsLog("$object (#$db_id) verwijderd.");
		$Message->set($msg_deleted);
	}
}

// *** Acivate
if (isset($_GET['activate']) and $id_holder = $_GET['activate'] and noBrowse()) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			upRec($db_table,$db_ids,'active',1);
			cmsLog("Meerdere $plural_lc geactiveerd");
			$Message->set($msg_activated_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		upRec($db_table,$db_id,'active',1);
		cmsLog("$object (#$db_id) geactiveerd");
		$Message->set($msg_activated);
	}
}

// *** De-activate
if (isset($_GET['deactivate']) and $id_holder = $_GET['deactivate'] and noBrowse()) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			upRec($db_table,$db_ids,'active',0);
			cmsLog("Meerdere $plural_lc op non-actief gezet.");
			$Message->set($msg_deactivated_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		upRec($db_table,$db_id,'active',0);
		cmsLog("$object (#$db_id) op non-actief gezet");
		$Message->set($msg_deactivated);
	}
}

// *** On-sale
if (isset($_GET['on_sale']) and $id_holder = $_GET['on_sale'] and noBrowse()) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			upRec($db_table,$db_ids,'on_sale',1);
			cmsLog("Meerdere $plural_lc in de aanbieding gezet");
			$Message->set($msg_on_sale_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		upRec($db_table,$db_id,'on_sale',1);
		cmsLog("$object (#$db_id) in de aanbieding gezet");
		$Message->set($msg_on_sale);
	}
}

// *** Off-sale
if (isset($_GET['off_sale']) and $id_holder = $_GET['off_sale'] and noBrowse()) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			upRec($db_table,$db_ids,'on_sale',0);
			cmsLog("Meerdere $plural_lc uit de aanbieding gehaald");
			$Message->set($msg_off_sale_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		upRec($db_table,$db_id,'on_sale',0);
		cmsLog("$object (#$db_id) uit de aanbieding gehaald");
		$Message->set($msg_off_sale);
	}
}

// *** Move up
if (isset($_GET['move_up']) and $db_id = (int) $_GET['move_up'] and noBrowse() and $category_id) {

	//Get some data
	$old_order	=	getMyo($db_table,$db_id,'product_order');
	$new_order 	= 	$old_order - 1;
	$other_id 	= 	getMyo($db_table, "product_order = '$new_order' AND category_id='$category_id'", 'id');

	//Flip
	if (!empty($other_id)) {
		upRec($db_table, $db_id,	'product_order', $new_order);
		upRec($db_table, $other_id,	'product_order', $old_order);
	}
}

// *** Move down
if (isset($_GET['move_down']) and $db_id = (int) $_GET['move_down'] and noBrowse() and $category_id) {

	//Get some data
	$old_order	=	getMyo($db_table,$db_id,'product_order');
	$new_order 	= 	$old_order + 1;
	$other_id 	= 	getMyo($db_table, "product_order = '$new_order' AND category_id='$category_id'", 'id');

	//Flip
	if (!empty($other_id)) {
		upRec($db_table, $db_id,	'product_order', $new_order);
		upRec($db_table, $other_id,	'product_order', $old_order);
	}
}

// *** Import
if (isset($_POST['import_finish']) and !empty($_FILES['import_data']['name'])) {

	//Upload the file
	if (($result = uploadFile('import_data', $upload_dir, 'import', $chmod, false, true, array('csv'))) !== false) {

		//Get file info
		list($file_name, $file_extension) = $result;
		$file_type = strtoupper(substr($file_extension,1));

		//Set the item and CSV props definitions
		$item	=	'product';
		$props	=	array(	'ean_code','price','normal_price','price_unit','stock','color_name','color_image','step_size','category_name',
							'sub_cat_name','sub_sub_cat_name','category_image','descr','height','width','prop_1','value_1',
							'prop_2','value_2','prop_3','value_3','prop_4','value_4');

		//Set up import engine
		require_once(CMS_PATH_PREFIX.'classes/class_ImportEngine.php');
		$ImportEngine = new ImportEngine($upload_dir.$_sep.$file_name, $file_type, $item, $props);

		//Process data
		if ($ImportEngine->process()) {

			if ($count = $ImportEngine->get_count()) {

				//Get data and import
				while ($values = $ImportEngine->get_element()) {

					//die(print_pre_r($values,true));

					//Clear additional props array
					$additional			=	array();

					//Initialize the insert-props array
					$ins_props 			= 	$props;

					//Set the vars
					$ean_code			=	$values[0];
					$price				=	dbAmount($values[1]);
					$normal_price		=	dbAmount($values[2]);
					$price_unit			=	$values[3];
					$color_name			=	$values[5];
					$step_size			=	$values[7];
					$category_name		=	$values[8];
					$subcategory_name	=	$values[9];
					$category_descr		=	$values[12];
					$height				=	$values[13];
					$width				=	$values[14];

					//Additional properties
					if (!empty($values[15])) {
						$additional[$values[15]] = $values[16];
					}
					if (!empty($values[17])) {
						$additional[$values[17]] = $values[18];
					}
					if (!empty($values[19])) {
						$additional[$values[19]] = $values[20];
					}
					if (!empty($values[21])) {
						$additional[$values[21]] = $values[22];
					}

					//Now, try to find the color. If not found, insert to database
					if (!$color_id = getMyo($db_table_colors, "name='$color_name'", 'id')) {
						$color_order = getMyo($db_table_colors, '', 'item_order', 'item_order', 'DESC', 1) + 1;
						$color_id = insRec($db_table_colors, array('name','item_order'), array($color_name,$color_order));
					}

					//Try to find the main category. If not found, insert
					if (!$main_category_id = getMyo($db_table_cats, "name='$category_name'", 'id')) {
						$cat_order = getMyo($db_table_cats, "sub_cat_of='0'", 'category_order', 'category_order', 'DESC', 1) + 1;
						$main_category_id = insRec($db_table_cats, array('name','category_order'), array($category_name,$cat_order));
					}

					//Now for the subcategory...
					if (!$category_id = getMyo($db_table_cats, "name='$subcategory_name' AND sub_cat_of='$main_category_id'", 'id')) {
						$cat_order = getMyo($db_table_cats, "sub_cat_of='$main_category_id'", 'category_order', 'category_order', 'DESC', 1) + 1;
						$category_id = insRec($db_table_cats,
							array('name','category_order','sub_cat_of','description','price_unit'),
							array($subcategory_name,$cat_order,$main_category_id,"<p>$category_descr</p>",$price_unit));
					}

					//Get the product order
					$product_order = getMyo($db_table, "category_id='$category_id'", 'product_order', 'product_order', 'DESC', 1) + 1;

					//Set-up the props array
					$ins_props = array(
						'ean_code','price','normal_price','category_id','color_id','vat_percentage','product_order','height','width'
					);

					//Set-up the values array
					$ins_values = array(
						$ean_code, $price, $normal_price, $category_id, $color_id, 19, $product_order, $height, $width
					);

					//Insert the actual product now
					$product_id = insRec($db_table, $ins_props, $ins_values);

					//Now check the additional properties
					foreach ($additional as $property => $prop_value) {

						//Find the prop ID
						if (!$prop_id = getMyo($db_table_props, "property='$property' AND category_id='$category_id'", 'id')) {
							$prop_order = getMyo($db_table_props, "category_id='$category_id'", 'prop_order', 'prop_order', 'DESC', 1) + 1;
							$prop_id = insRec($db_table_props, 	array('category_id','property','prop_order'),
																array($category_id,$property,$prop_order));
						}

						//Insert the prop value
						insRec($db_table_prop_values, 	array('product_id','property_id','property_value'),
														array($product_id,$prop_id,$prop_value));
					}
				}

				//Log and message
				cmsLog("$count $plural geimporteerd.");
				$Message->set(str_replace('[AMOUNT]', $count, $msg_imported));
			}
			else {
				$Message->set($msg_no_data, WARNING);
			}

			//Delete the temporary file
			@unlink($upload_dir.$_sep.$file_name);
		}
		else {
			$Message->set($msg_process_error, CRITICAL);
		}
	}
	else {
		$Message->set($msg_upload_error.'<br/><br/>'.$_error, WARNING);
	}
}

/******************************************************************************************
 * Add / Edit / View options
 ***/

if (isset($_GET['add']) and $category_id) {

	//Read properties from database for this category
	$properties = makeArray($db_table_props, 'id', 'property', "category_id='$category_id'", 'prop_order', 'ASC');

	//Make property boxes
	foreach ($properties as $prop_id => $property) {
		$properties[$prop_id] = "
			<tr>
				<td>".ucfirst($property).":</td>
				<td><input type='text' class='input_regular' name='property_$prop_id' /></td>
			</tr>
		";
	}

	$pre_html = "
    	<h3>$object toevoegen:</h3><hr/><br/>
        <form name='add_form' method='post' action='$_file'>
        <table cellspacing='0px' cellpadding='1px' border='0px'>
		<tr>
			<td>Categorie:</td>
			<td>".trim($CATEGORIES_ARRAY[$category_id],'- ')."</td>
		</tr>
		<tr>
			<td width='150px'>EAN code:</td>
			<td><input type='text' name='ean_code' class='input_regular' /></td>
		</tr>
		<tr>
			<td>Kleur:</td>
			<td>".makeSelectBox('color_id',$COLORS_ARRAY,0)."</td>
		</tr>
		<tr>
			<td>Home pagina:</td>
			<td>".makeSelectBox('sale_position',$sale_positions,0)."</td>
		</tr>

		<tr>
			<td><br/>Besteleenheid:</td>
			<td><br/><input type='text' name='step_size' class='input_small' value='1' $js[int]/></td>
		</tr>
		<tr>
			<td>Voorraad:</td>
			<td><input type='text' name='stock' class='input_small' $js[int]/></td>
		</tr>
		<tr>
			<td>Afwijkende levertijd:</td>
			<td><input type='text' name='delivery_days' value='' class='input_small'/> dagen (alleen invullen als afwijkend van categorie)</td>
		</tr>

		<tr>
			<td><br/>Topdeal prijs:</td>
			<td><br/><input type='text' name='price' class='input_small' value='' $js[NaN]/> (excl. BTW)</td>
		</tr>
		<tr>
			<td>Normale prijs:</td>
			<td><input type='text' name='normal_price' class='input_small' value='' $js[NaN]/></td>
		</tr>
		<tr>
			<td>Aanbieding prijs:</td>
			<td><input type='text' name='sale_price' class='input_small' value='0,00' $js[NaN]/></td>
		</tr>
		<tr>
			<td>BTW tarief:</td>
			<td><input type='text' name='vat_percentage' class='input_small' value='$standard_vat'/> %</td>
		</tr>

		<tr>
			<td colspan='2'>&nbsp;</td>
		</tr>
		<tr>
			<td>Hoogte:</td>
			<td><input type='text' class='input_small' name='height' /> mm</td>
		</tr>
		<tr>
			<td>Breedte:</td>
			<td><input type='text' class='input_small' name='width' /> mm</td>
		</tr>
		".implode("\n",$properties)."

        <tr>
			<td colspan='2'><br/><input type='submit' name='add_finish' value='Toevoegen' /></td>
		</tr>

        </table>
        </form><br/>
    ";
}

if (isset($_GET['edit']) and $db_id = (int) $_GET['edit'] and $category_id) {

	//Read product info
	$res = eq("	SELECT * FROM $db_table WHERE id = '$db_id';");
	$myo = mfo($res);

	//Parse prices
	$myo->price 		= parseAmount($myo->price);
	$myo->sale_price 	= parseAmount($myo->sale_price);
	$myo->normal_price 	= parseAmount($myo->normal_price);

	//Read properties from database for this category
	$properties  = makeArray($db_table_props, 'id', 'property', "category_id='$category_id'", 'prop_order', 'ASC');
	$prop_values = makeArray($db_table_prop_values, 'property_id', 'property_value', "product_id='$db_id'");

	//Make property boxes
	foreach ($properties as $prop_id => $property) {
		$prop_value = isset($prop_values[$prop_id]) ? $prop_values[$prop_id] : '';
		$properties[$prop_id] = "
			<tr>
				<td>".ucfirst($property).":</td>
				<td><input type='text' class='input_regular' name='property_$prop_id' value='$prop_value'/></td>
			</tr>
		";
	}

	$pre_html = "
    	<h3>$object gegevens bijwerken:</h3><hr/><br/>
        <form name='edit_form' method='post' action='$_file'>
        <input type='hidden' name='id' value='$db_id' />
        <table cellspacing='0px' cellpadding='1px' border='0px'>
		<tr>
			<td>Categorie:</td>
			<td>".trim($CATEGORIES_ARRAY[$category_id],'- ')."</td>
		</tr>
		<tr>
			<td width='150px'>EAN code:</td>
			<td><input type='text' name='ean_code' class='input_regular' value='$myo->ean_code'/></td>
		</tr>
		<tr>
			<td>Kleur:</td>
			<td>".makeSelectBox('color_id',$COLORS_ARRAY,$myo->color_id)."</td>
		</tr>
		<tr>
			<td>Home pagina:</td>
			<td>".makeSelectBox('sale_position',$sale_positions,$myo->sale_position)."</td>
		</tr>

		<tr>
			<td><br/>Besteleenheid:</td>
			<td><br/><input type='text' name='step_size' class='input_small' value='$myo->step_size'/></td>
		</tr>
		<tr>
			<td>Voorraad:</td>
			<td><input type='text' name='stock' class='input_small' value='$myo->stock' $js[int]/></td>
		</tr>
		<tr>
			<td>Afwijkende levertijd:</td>
			<td><input type='text' name='delivery_days' value='".($myo->delivery_days ? $myo->delivery_days : '')."' class='input_small'/> dagen (alleen invullen als afwijkend van categorie)</td>
		</tr>

		<tr>
			<td><br/>Topdeal prijs:</td>
			<td><br/><input type='text' name='price' class='input_small' value='$myo->price' $js[NaN]/> (excl. BTW)</td>
		</tr>
		<tr>
			<td>Normale prijs:</td>
			<td><input type='text' name='normal_price' class='input_small' value='$myo->normal_price' $js[NaN]/></td>
		</tr>
		<tr>
			<td>Aanbieding prijs:</td>
			<td><input type='text' name='sale_price' class='input_small' value='$myo->sale_price' $js[NaN]/></td>
		</tr>
		<tr>
			<td>BTW tarief:</td>
			<td><input type='text' name='vat_percentage' class='input_small' value='$myo->vat_percentage'/> %</td>
		</tr>

		<tr>
			<td colspan='2'>&nbsp;</td>
		</tr>
		<tr>
			<td>Hoogte:</td>
			<td><input type='text' class='input_small' name='height' value='$myo->height'/> mm</td>
		</tr>
		<tr>
			<td>Breedte:</td>
			<td><input type='text' class='input_small' name='width' value='$myo->width'/> mm</td>
		</tr>
		".implode("\n",$properties)."

        <tr>
			<td colspan='2'><br/><input type='submit' name='edit_finish' value='Opslaan' /></td>
		</tr>

        </table>
        </form><br/>
    ";
}

if (isset($_GET['view']) and $db_id = (int) $_GET['view']) {

	//Read product info
	$res = eq("	SELECT * FROM $db_table WHERE id = '$db_id';");
	$myo = mfo($res);

	//Set category, if not set already
	$category_id = $_SESSION['category_id'] = $myo->category_id;

	//Parse price
	$price = $myo->on_sale ? parseAmount($myo->sale_price,1) : parseAmount($myo->price,1);
	$normal_price = parseAmount($myo->normal_price, 1);

	//Read properties from database for this category
	$properties  = makeArray($db_table_props, 'id', 'property', "category_id='$category_id'", 'prop_order', 'ASC');
	$prop_values = makeArray($db_table_prop_values, 'property_id', 'property_value', "product_id='$db_id'");

	//Make property boxes
	foreach ($properties as $prop_id => $property) {
		$prop_value = isset($prop_values[$prop_id]) ? $prop_values[$prop_id] : '';
		$properties[$prop_id] = "
			<tr>
				<td>".ucfirst($property).":</td>
				<td>$prop_value</td>
			</tr>
		";
	}

	$pre_html = "
    	<h3>$object gegevens inzien:</h3><hr/><br/>

        <table cellspacing='0px' cellpadding='1px' border='0px'>
		<tr>
			<td>Categorie:</td>
			<td>".trim($CATEGORIES_ARRAY[$myo->category_id],'- ')."</td>
		</tr>
		<tr>
			<td width='150px'>EAN code:</td>
			<td>$myo->ean_code</td>
		</tr>
		<tr>
			<td>Kleur:</td>
			<td>".$COLORS_ARRAY[$myo->color_id]."</td>
		</tr>
		<tr>
			<td>Home pagina:</td>
			<td>".$sale_positions[$myo->sale_position]."</td>
		</tr>

		<tr>
			<td><br/>Besteleenheid:</td>
			<td><br/>$myo->step_size</td>
		</tr>
		<tr>
			<td>Voorraad:</td>
			<td>$myo->stock</td>
		</tr>
		<tr>
			<td>Afwijkende levertijd:</td>
			<td>".($myo->delivery_days ? "$myo->delivery_days dagen" : '-')."</td>
		</tr>
		<tr>
			<td>Topdeal prijs:</td>
			<td>$price (excl. $myo->vat_percentage% BTW)</td>
		</tr>
		<tr>
			<td>Normale prijs:</td>
			<td>$normal_price</td>
		</tr>

		<tr>
			<td colspan='2'>&nbsp;</td>
		</tr>
		<tr>
			<td>Hoogte:</td>
			<td>$myo->height mm</td>
		</tr>
		<tr>
			<td>Breedte:</td>
			<td>$myo->width mm</td>
		</tr>
		".implode("\n",$properties)."

        </table>
        <br/>
    ";
}

if (isset($_GET['import'])) {

	$pre_html = "
		<h3>Producten importeren:</h3><hr/>
		<p>Met onderstaand formulier kunt u producten/categorieen importeren. U dient hiervoor een CSV bestand te gebruiken.
		<br/>Zorg ervoor dat u de juiste kolommen gebruikt. Laat dit bij twijfel doen door uw technische dienst.<br/><br/></p>
		<form name='import_form' method='post' action='$_file' enctype='multipart/form-data'>
		Bestand: <input type='file' name='import_data' class='input_file'/>
		<input type='submit' class='input_submit' name='import_finish' value='Importeren' />
		</form>
		<br/><br/>
	";
}

if (isset($_GET['export'])) {

	define('TEMP_FOLDER', '../user_files/tmp/');
	require_once(CMS_PATH_PREFIX.'classes/export.php');
	$values = array();

	//Read main categories
	$res = eq("SELECT * FROM $db_table_cats WHERE sub_cat_of='0' ORDER BY category_order ASC;");
	while ($main_cat = mfo($res)) {

		//Category name
		$category_name = $main_cat->name;

		//Read sub categories
		$res2 = eq("SELECT * FROM $db_table_cats WHERE sub_cat_of='$main_cat->id' ORDER BY category_order ASC;");
		while ($cat = mfo($res2)) {

			//Set more category vars
			$subcategory_name 	= 	$cat->name;
			$category_descr		=	$cat->description;
			$price_unit			=	$cat->price_unit;
			$category_image		=	'gfx/categories/cat_'.$cat->id.'.jpg';

			//Read the individual products
			$res3 = eq("SELECT * FROM $db_table WHERE category_id = '$cat->id' ORDER BY product_order ASC;");
			while ($prod = mfo($res3)) {

				//Set the vars
				$ean_code			=	$prod->ean_code;
				$price				=	parseAmount($prod->price);
				$normal_price		=	parseAmount($prod->normal_price);
				$color_name			=	isset($COLORS_ARRAY[$prod->color_id]) ? $COLORS_ARRAY[$prod->color_id] : '';
				$color_file			=	'gfx/colors/color_'.$prod->color_id.'.jpg';
				$step_size			=	$prod->step_size;
				$stock				=	$prod->stock;
				$height				=	$prod->height;
				$width				=	$prod->width;

				//Read the props
				$props_array = array();
				$key = 15;
				$res4 = eq("SELECT property_value,property FROM $db_table_prop_values
							LEFT JOIN $db_table_props ON ($db_table_props.id = property_id)
							WHERE product_id = '$prod->id' ORDER BY prop_order ASC;");
				while ($prop = mfo($res4)) {
					$props_array[$key] = $prop->property;
					$props_array[$key+1] = $prop->property_value;
					$key+=2;
				}

				//Add values
				$values[] = array(	$ean_code,$price,$normal_price,$price_unit,$stock,$color_name,$color_file,
									$step_size,$category_name,$subcategory_name,'',$category_image,
									$category_descr,$height,$width) + $props_array;
			}
		}
	}

	$Export = new Export();
	$Export->set_csv_delimiter(';');
	$Export->set_fields(array(	'EAN code','Topdeal Prijs','Adviesprijs','Prijs eenheid','Voorraad','Kleur','Kleurafbeelding',
								'Besteleenheid','Categorie naam','Sub categorie naam','Sub-sub categorie naam','Categorie afbeelding',
								'Categorie beschrijving','Hoogte (mm)','Breedte (mm)','Eigenschap 1','Waarde 1',
								'Eigenschap 2','Waarde 2','Eigenschap 3','Waarde 3','Eigenschap 4','Waarde 4'));
	$Export->set_values($values);
	$Export->download_csv('producten.csv');
}

/******************************************************************************************
 * Page contents
 ***/

//Page title HTML
$html = "
	<a href='$_file'><img src='$site_icon' border='0px' class='cms_icon'/></a>
	<h6>
		<a href='$_file'>overzicht</a> &nbsp;
		$img[product_add] <a href='$_file?add'>nieuw product</a> &nbsp;
		$img[note_list] <a href='cms_topdeal_colors.php'>kleuren beheren</a> &nbsp;
		$img[product_up] <a href='$_file?import'>importeren</a> &nbsp;
		$img[product_down] <a href='$_file?export'>exporteren</a> &nbsp;
	</h6>
	<br/><br/><br/>
	Categorie: &nbsp;".makeSelectBox('category_id', $CATEGORIES_ARRAY, $category_id,0,"onchange='document.location.href=\"$_file?category_id=\"+this.value'")."
	".$Message->get()."
	<br/><br/>$pre_html
";

$res = eq("	SELECT 		id, ean_code, product_no, active, on_sale, stock, times_ordered, price, sale_price
			FROM 		$db_table $where_clause
			ORDER BY 	$order_clause
			LIMIT		$limit_clause
		;");
$data_rows = array();

while ($myo = mfo($res)) {

	$data_rows[] = array(

		//This MUST be the id
		$myo->id,

		//All desired other fields
		$myo->id,
		"<a href='$_file?view=$myo->id' title='Product gegevens inzien'>$myo->ean_code</a>",
		$myo->stock,
		$myo->on_sale ? 	"<span style='color: red'>".parseAmount($myo->sale_price,1)."</span>" : parseAmount($myo->price,1),
		$myo->times_ordered.' x',
		$myo->on_sale ? 	"<a href='$_file?off_sale=$myo->id&$_app' title='$object uit de aanbieding halen'>$img[ok]</a>" :
							"<a href='$_file?on_sale=$myo->id&$_app' title='$object in de aanbieding zetten'>$img[notok]</a>",
		$myo->active ? 		"<a href='$_file?deactivate=$myo->id&$_app' title='$object de-activeren'>$img[ok]</a>" :
							"<a href='$_file?activate=$myo->id&$_app' title='$object activeren'>$img[notok]</a>",
		"<a href='$_file?move_up=$myo->id&$_app' title='$object omhoog verplaatsen'>$img[up]</a>
	 	 <a href='$_file?move_down=$myo->id&$_app' title='$object omlaag verplaatsen'>$img[down]</a>",
		"<a href='$_file?edit=$myo->id&$_app' title='$object gegevens bijwerken'>$img[edit]</a>
		 <a href='$_file?delete=$myo->id&$_app' title='$object verwijderen'>$img[product_del]</a>
		"
	);
}

//Output data table
$DataTable = new DataTable();
$DataTable->setMarkAllOptions(	array('verwijder','delete',$plural.' verwijderen'),
								array('on sale','on_sale',$plural.' in de aanbieding zetten'),
								array('off sale','off_sale',$plural.' uit de aanbieding halen'),
								array('activeer','activate',$plural.' activeren'),
								array('de-activeer','deactivate',$plural.' de-activeren'));

$DataTable->showSearch();
$DataTable->setDataRows($data_rows);
$DataTable->setLegendRow(
	array('#','EAN code','Voorraad','Prijs','Besteld','Sale','Actief','Volgorde','Opties'),
	array(60,0,90,80,70,50,60,80,40),
	array('id','ean_code','stock','price','times_ordered','on_sale','active','product_order')
);
echo $html.$DataTable->getHTML();

require_once('inc/cms_footer.php');
?>