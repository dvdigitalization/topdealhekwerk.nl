<?php
/*
Title:		CMS users
File: 		cms_auth_users.php
Version: 	v2.04
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

/******************************************************************************************
 * Settings
 ***/

// *** Required files
require_once('inc/cms_header.php');
require_once('inc/lang/'.$cms_lang.'_'.$_file);
require_once(CMS_PATH_PREFIX.'classes/class_DataTable.php');
require_once(CMS_PATH_PREFIX.'classes/class_Activity.php');

// *** Extra database table definitions
$db_table_logs			=	DIGI_DB_PREFIX.'activity_log';
$db_table_hours			=	DIGI_DB_PREFIX.'hours';
$db_table_projects		=	DIGI_DB_PREFIX.'projects';
$db_table_customers		=	DIGI_DB_PREFIX.'customers_adm';
$db_table_tickets		=	DIGI_DB_PREFIX.'tickets';
$db_table_ticket_events	=	DIGI_DB_PREFIX.'ticket_events';
$db_table_relations		=	DIGI_DB_PREFIX.'administrative_relations';

// *** Is digi?
$is_digi				=	(($s_id == 1 and $s_admin) or ($testmode and $s_admin)) ? true : false;

// *** Sorting and pages functionality
$default_sort_option	=	'username';
$default_sort_method	=	'ASC';
$secondary_sort_option	=	'';
$secondary_sort_method	=	'';
$valid_sort_options		=	array('id','username','last_login','allow_login','is_admin');

// *** Where clauses
if (!$is_digi and !$s_admin) {
	$where[]			= 	"id > 1 AND username <> 'digitalization' AND is_admin = '0'";	
}
elseif (!$is_digi and $s_admin) {
	$where[]			= 	"id > 1 AND username <> 'digitalization'";	
}

// *** Search constraints
if (!empty($search_query)) {
	$search				=	array();
	$search[]			=	"username LIKE '%$search_query%'";
	$search[]			=	"first_name LIKE '%$search_query%'";
	$search[]			=	"last_name LIKE '%$search_query%'";
	$where[]			=	'('.implode(' OR ', $search).')';
}

// *** Sorting and pages and link appendix
require_once(CMS_PATH_PREFIX.'inc/sorting_and_pages.php');
$_app					=	$page_appendix.'&'.$sort_appendix.'&'.$link_appendix;

// *** Update last active times for idle people
$last_online_limit = mysql_time(time() - 600);
upRec($db_table, "last_online < '$last_online_limit'", 'last_online', '0000-00-00 00:00:00');

/******************************************************************************************
 * POST and GET Cases
 ***/

if (isset($_POST['add_finish']) or isset($_POST['edit_finish'])) {
 
 	$privs = array();
	foreach(array_keys($_POST) as $key) {
		evalAll($_POST[$key]);			
		
		if (substr($key,0,5) == 'priv_') {
			$privs[]	=	substr($key,5);
		}
	}
	$privs = implode(';', $privs);	
}

if (isset($_POST['add_finish']) and checkPriv(PRIV_AUTH_USERS)) {

	//Only admins can create admins
	if (isset($_POST['is_admin']) and $_POST['is_admin'] and !$s_admin) {
		$Message->set($msg_invalid_action, CRITICAL);
	}	
	elseif (empty($_POST['username'])) {
		$Message->set($msg_no_username, CRITICAL);
		$_GET['add'] = 1;
	}
	elseif (!$_POST['username'] = validateSimpleString($_POST['username'])) {
		$Message->set($msg_invalid_chars_user, WARNING);
		$_GET['add'] = 1;
	}
	elseif ($_POST['username'] == 'digitalization') {
		$Message->set($msg_invalid_username, WARNING);
		$_GET['add'] = 1;
	}
	elseif (countResults($db_table, "username = '$_POST[username]'")) {
		$Message->set($msg_username_exists, WARNING);
		$_GET['add'] = 1;
	}
	elseif (empty($_POST['pass_1']) or empty($_POST['pass_2'])) {
		$Message->set($msg_no_pass, CRITICAL);
		$_GET['add'] = 1;
	}
	elseif (!$_POST['pass_1'] = validateSimpleString($_POST['pass_1'])) {
		$Message->set($msg_invalid_chars_pass, WARNING);
		$_GET['add'] = 1;
	}	
	elseif ($_POST['pass_1'] != $_POST['pass_2']) {
		$Message->set($msg_pass_dont_match, CRITICAL);
		$_GET['add'] = 1;
	}
	elseif (!empty($_POST['email_address']) and !($_POST['email_address'] = validateEmail($_POST['email_address']))) {
		$Message->set($msg_invalid_email, WARNING);
		$_GET['add'] = 1;
	}
	else {
		
		if ($s_admin) {
			$is_admin		=	$_POST['is_admin'];
			$ip_login_1		=	validateIP($_POST['ip_login_1']);
			$ip_login_2		=	validateIP($_POST['ip_login_2']);
		}
		else {
			$ip_login_1		=	$ip_login_2		=	'';
			$is_admin		=	0;
		}
		$password			=	dbSalt($_POST['pass_1']);
		$use_in_admin		= 	$is_admin ? 0 : $_POST['use_in_administration'];
				
		$db_id = insRec($db_table, 	array(	'username','password','privs','first_name','last_name','email_address','is_admin',
											'use_in_administration','ip_login_1','ip_login_2'),
									array(	$_POST['username'], $password ,$privs, $_POST['first_name'], $_POST['last_name'],
											$_POST['email_address'],$is_admin,$use_in_admin,$ip_login_1, $ip_login_2)
		);	
		
		cmsLog("$object $_POST[username] (#$db_id) toegevoegd.");
		$Message->set($msg_added);
	}
}

if (isset($_POST['edit_finish']) and checkPriv(PRIV_AUTH_USERS) and $db_id = (int) $_POST['id'] and $db_id > 1) {
	
	//Only admins can edit admins
	if (getMyo($db_table, $db_id, 'is_admin') and !$s_admin) {
		$Message->set($msg_invalid_action, CRITICAL);
	}
	elseif (empty($_POST['username'])) {
		$Message->set($msg_no_username, CRITICAL);
		$_GET['edit'] = $db_id;
	}
	elseif (!$_POST['username'] = validateSimpleString($_POST['username'])) {
		$Message->set($msg_invalid_chars_user, WARNING);
		$_GET['edit'] = $db_id;
	}
	elseif ($_POST['username'] == 'digitalization') {
		$Message->set($msg_invalid_username, WARNING);
		$_GET['edit'] = $db_id;
	}
	elseif (countResults($db_table, "username = '$_POST[username]' AND id <> '$db_id'")) {
		$Message->set($msg_username_exists, WARNING);
		$_GET['edit'] = $db_id;
	}
	elseif (!empty($_POST['newpass_1']) and (empty($_POST['newpass_2']) or $_POST['newpass_1'] != $_POST['newpass_2'])) {
		$Message->set($msg_pass_dont_match, WARNING);
		$_GET['edit'] = $db_id;
	}
	elseif (!empty($_POST['email_address']) and !($_POST['email_address'] = validateEmail($_POST['email_address']))) {
		$Message->set($msg_invalid_email, WARNING);
		$_GET['edit'] = $db_id;
	}
	else {
		
		if ($s_admin) {
			$is_admin		=	$_POST['is_admin'];
			$ip_login_1		=	validateIP($_POST['ip_login_1']);
			$ip_login_2		=	validateIP($_POST['ip_login_2']);
		}
		else {
			$user			=	getMyo($db_table, $db_id, array('is_admin','ip_login_1','ip_login_2'));
			$ip_login_1		=	$user->ip_login_1;
			$ip_login_2		=	$user->ip_login_2;
			$is_admin		=	$user->is_admin;
		}
		$use_in_admin		= 	$is_admin ? 0 : $_POST['use_in_administration'];
		
		if (!empty($_POST['newpass_1'])) {
			
			$password	=	dbSalt($_POST['newpass_1']);
			
			upRec($db_table, $db_id, 	array(	'username','password','privs','first_name','last_name','email_address','is_admin',
												'use_in_administration','ip_login_1','ip_login_2'),
										array(	$_POST['username'],$password,$privs,$_POST['first_name'],$_POST['last_name'],
												$_POST['email_address'],$is_admin,$use_in_admin,$ip_login_1,$ip_login_2)
			);
												  
			$Message->set($msg_editted.' '.$msg_newpass);
		}
		else {
			upRec($db_table, $db_id, 	array(	'username','privs','first_name','last_name','email_address','is_admin',
												'use_in_administration','ip_login_1','ip_login_2'),
										array(	$_POST['username'],$privs,$_POST['first_name'],$_POST['last_name'],$_POST['email_address'],
												$is_admin,$use_in_admin,$ip_login_1,$ip_login_2)
			);	
			$Message->set($msg_editted);
		}
		
		cmsLog("$object $_POST[username] (#$db_id) gewijzigd.");
	}
}

// *** Login as another user confirmation
if (isset($_GET['login_as']) and $db_id = (int) $_GET['login_as'] and ($db_id > 1 or $is_digi) and $s_admin) {

	$Message->setConfirm($msg_login_confirm,'login_as',$db_id,'username');
}

// *** Login as another user
if (isset($_GET['login_as_confirm']) and $db_id = (int) $_GET['login_as_confirm'] and ($db_id > 1 or $is_digi) and $s_admin) {
	
	$user = getMyo($db_table, $db_id, array('username','password','uses_mailer','first_name','last_name'));
	
	if ($user) {
		
		cmsLog("Gebruiker is ingelogd als $user->username (#$db_id).");
		
		$_SESSION['login_id']			=	$db_id;
		$_SESSION['login_user']			=	$user->first_name.' '.$user->last_name.' ('.$user->username.')';
		$_SESSION['last_active']		=	time();
		$_SESSION['login_validation']	=	sha1($user->password.$sha1_appendix.'7H3ki');
		$_SESSION['uses_mailer'] 		= 	file_exists('cms_mailer.php') ? $user->uses_mailer : 0;
		
		session_regenerate_id(true);
		
		upRec($db_table, $db_id, array('last_login','last_login_ip','session_id','cookie_id','login_attempts'),
								array('[NOW()]',$_ip,session_id(),'',0));

		//Clear the cookie, which invalidates a login
		$c_name = '';
	    for ($i=0; $i<strlen($site_name); $i++) {
	        if (preg_match('([0-9a-zA-Z])', $site_name[$i])) {
	            $c_name .= $site_name[$i];
	        }    
	    }
		setCookie($c_name.'CMSCOOKIE');
		
		//Redirect the user
		header('Location: index.php');
		exit;
	}
	else {
		$Message->set($msg_login_failed, CRITICAL);
	}
}

// *** Delete confirmation
if (isset($_GET['delete']) and $id_holder = $_GET['delete'] and checkPriv(PRIV_AUTH_USERS)) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		
		//Invalid user id's
		foreach ($db_ids as $key => $db_id) {
			if ($db_id == 1 or $db_id == $s_id)	unset($db_ids[$key]);
		}
		
		if (count($db_ids)) {
			$Message->setConfirmMultiple($msg_delete_confirm_m, 'delete',$db_ids,'username');
		}
		else {
			$Message->set($msg_unable_to_delete, CRITICAL);
		}
	}
	else if ($db_id = (int) $id_holder and $db_id != 1 and $db_id != $s_id) {
		$Message->setConfirm($msg_delete_confirm,'delete',$db_id,'username');
	}
	else {
		$Message->set($msg_unable_to_delete, CRITICAL);
	}
}

// *** Delete a user
if (isset($_GET['delete_confirm']) and $id_holder = $_GET['delete_confirm'] and checkPriv(PRIV_AUTH_USERS) and noBrowse()) {

	$db_ids = $handle_ids = array();
	if (isset($_GET['multiple_confirm']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		
		//Invalid user id's
		foreach ($db_ids as $key => $db_id) {
			if ($db_id == 1 or $db_id == $s_id)	unset($db_ids[$key]);
		}
		
		if (count($db_ids)) {
			delRec($db_table, $db_ids);
			delRec($db_table_logs, "auth_user IN (".implode(',', $db_ids).")");
			cmsLog("Meerdere $plural verwijderd.");
			$Message->set($msg_deleted_m);
			$handle_ids = $db_ids;
		}
		else {
			$Message->set($msg_unable_to_delete, CRITICAL);
		}	
	}
	else if ($db_id = (int) $id_holder and $db_id != 1 and $db_id != $s_id) {
		delRec($db_table, $db_id);
		delRec($db_table_logs, "auth_user = '$db_id'");
		cmsLog("$object (#$db_id) verwijderd.");
		$handle_ids = array($db_id);
		$Message->set($msg_deleted);
	}
	else {
		$Message->set($msg_unable_to_delete, CRITICAL);
	}
	
	foreach ($handle_ids as $db_id) {
		if (isset($installed_modules[MOD_PROJECTS])) {
			delRec($db_table_relations, "employee_id = '$db_id'");
		}
		if (isset($installed_modules[MOD_CUSTOMERS_ADM])) {
			upRec($db_table_customers, "assigned_to = '$db_id'", 'assigned_to', '');
		}
		if (isset($installed_modules[MOD_HOURS])) {
			delRec($db_table_hours, "employee_id = '$db_id'");
		}
		if (isset($installed_modules[MOD_TICKETS])) {
			upRec($db_table_tickets, "opened_by = '$db_id'", 'opened_by', 1);
			upRec($db_table_ticket_events, "event_by = '$db_id'", 'event_by', 1);
		}
	}
}

// *** Acivate users
if (isset($_GET['activate']) and $id_holder = $_GET['activate'] and checkPriv(PRIV_AUTH_USERS) and noBrowse()) {
	
	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		
		//Invalid user id's
		foreach ($db_ids as $key => $db_id) {
			if ($db_id == 1 or $db_id == $s_id)								unset($db_ids[$key]);
			elseif (getMyo($db_table, $db_id, 'is_admin') and !$s_admin)	unset($db_ids[$key]);
		}
		
		if (count($db_ids)) {
			upRec($db_table,$db_ids,array('allow_login','login_attempts'),array(1,0));	
			cmsLog("Meerdere $plural_lc geactiveerd");
			$Message->set($msg_activated_m);
		}
	}
	else if ($db_id = (int) $id_holder and $db_id != 1 and $db_id != $s_id) {
		if ($s_admin or !getMyo($db_table, $db_id, 'is_admin')) {
			upRec($db_table,$db_id,array('allow_login','login_attempts'),array(1,0));	
			cmsLog("$object (#$db_id) geactiveerd");
			$Message->set($msg_activated);
		}
	}
}

// *** De-activate users
if (isset($_GET['deactivate']) and $id_holder = $_GET['deactivate'] and checkPriv(PRIV_AUTH_USERS) and noBrowse()) {
	
	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		
		//Invalid user id's
		foreach ($db_ids as $key => $db_id) {
			if ($db_id == 1 or $db_id == $s_id)								unset($db_ids[$key]);
			elseif (getMyo($db_table, $db_id, 'is_admin') and !$s_admin)	unset($db_ids[$key]);
		}
		
		if (count($db_ids)) {
			upRec($db_table,$db_ids,'allow_login',0);	
			cmsLog("Meerdere $plural_lc op non-actief gezet");
			$Message->set($msg_deactivated_m);
		}
	}
	else if ($db_id = (int) $id_holder and $db_id != 1 and $db_id != $s_id) {
		if ($s_admin or !getMyo($db_table, $db_id, 'is_admin')) {
			upRec($db_table,$db_id,'allow_login',0);	
			cmsLog("$object (#$db_id) op non-actief gezet");
			$Message->set($msg_deactivated);
		}
	}
}

// *** Purge activity logs
if (isset($_GET['purge_log']) and checkPriv(PRIV_PURGE_LOGS) and noBrowse()) {

	if (isset($_GET['user_id'])) 	$db_id = (int) $_GET['user_id'];
	else							$db_id = 0;
	
	$Activity	=	new Activity($db_table_logs, $db_id);
	
	if ($Activity->purgeLog()) {
		
		if ($db_id) {
			cmsLog("Activiteiten log van $object_lc (#$db_id) geleegd.");	
		}
		else {
			cmsLog("Activiteiten log van alle $plural_lc geleegd.");
		}
		
		$Message->set($msg_purged);
	}	
}

/******************************************************************************************
 * Add / Edit / View options
 ***/

if (isset($_GET['add']) and checkPriv(PRIV_AUTH_USERS)) {

	$p					=	0;
	$priv_tick_boxes 	= 	array();
	$username			=	isset($_POST['username']) 		? $_POST['username'] : '';
	$first_name			=	isset($_POST['first_name']) 	? $_POST['first_name'] : '';
	$last_name			=	isset($_POST['last_name']) 		? $_POST['last_name'] : '';
	$email_address		=	isset($_POST['email_address']) 	? $_POST['email_address'] : '';
	$ip_login_1			=	isset($_POST['ip_login_1']) 	? $_POST['ip_login_1'] : '';
	$ip_login_2			=	isset($_POST['ip_login_2']) 	? $_POST['ip_login_2'] : '';
	$priv_tick_boxes[] 	= 	"<input type='checkbox' name='mark_all' title='".TXT_MARK_OR_CLEAR_ALL_PRIVS."'
							 onclick='toggleMarkAll(\"priv_\")'/> <i>".TXT_ALL."</i>";

	require_once('inc/definitions/def_privs_'.$cms_lang.'.php');
	foreach($privs_array as $priv_id => $priv) {
		
		$p++;
		$priv_txt			=	isset($priv_def[$priv]) ? $priv_def[$priv] : $priv;
		$priv_tick_boxes[] 	= 	"<input type='checkbox' name='priv_$priv_id' id='priv_$p' /> $priv_txt";
	}

    $pre_html = "
        	<h3>".TXT_ADD_USER.":</h3><hr/><br/>
			<form name='add_form' method='post' action='$_file'>      	
            <table cellspacing='0px' cellpadding='1px' border='0px'>
			<tr>
				<td width='150px'>".TXT_USERNAME.":</td>
				<td><input type='text' name='username' value='$username' class='input_regular'/></td>
			</tr>
			<tr>
				<td>".TXT_PASSWORD.":</td>
				<td><input type='password' name='pass_1' class='input_regular' autocomplete='off'/></td>
			</tr>
			<tr>
				<td>".TXT_REPEAT_PASS.":</td>
				<td><input type='password' name='pass_2' class='input_regular' autocomplete='off'/></td>
			</tr>
			<tr>
				<td>".TXT_FIRST_NAME.":</td>
				<td><input type='text' name='first_name' value='$first_name' class='input_regular'/></td>
			</tr>
			<tr>
				<td>".TXT_LAST_NAME.":</td>
				<td><input type='text' name='last_name' value='$last_name' class='input_regular'/></td>
			</tr>
			<tr>
				<td>".TXT_EMAIL_ADDRESS.":</td>
				<td><input type='text' name='email_address' value='$email_address' class='input_regular'/></td>
			</tr>
			<tr>
				<td>".TXT_USE_IN_ADMIN.":</td>
				<td>".makeSelectBox('use_in_administration',$YESNO_ARRAY,1)."</td>
			</tr>
			".($s_admin ? "
			<tr>
				<td>".TXT_ADMINISTRATOR.":</td>
				<td>".makeSelectBox('is_admin',$YESNO_ARRAY,0)."</td>
			</tr>
			<tr>
				<td>".TXT_IP_LOGIN_1.":</td>
				<td><input type='text' name='ip_login_1' value='$ip_login_1' class='input_regular'/></td>
			</tr>
			<tr>
				<td>".TXT_IP_LOGIN_2.":</td>
				<td><input type='text' name='ip_login_2' value='$ip_login_2' class='input_regular'/></td>
			</tr>				
			" : '')."	
			<tr>
				<td valign='top'><br/>".TXT_PRIVS.":</td>
				<td><br/>".implode("\n<br/>",$priv_tick_boxes)."</td>
			</tr>
			
        	<tr><td colspan='2'><br/><input type='submit' name='add_finish' class='input_submit' value='".BUTTON_ADD."' /></td></tr>
			</table>
			</form><br/>
    ";
}

if (isset($_GET['edit']) and $db_id = (int) $_GET['edit'] and $db_id != 1 and checkPriv(PRIV_AUTH_USERS)) {

    $res = eq("	SELECT 	username,privs,last_login_ip,allow_login,first_name,last_name,email_address,is_admin,
						UNIX_TIMESTAMP(last_login) AS last_login, use_in_administration, ip_login_1, ip_login_2
				FROM 	$db_table WHERE id = '$db_id';");
    $myo = mfo($res);

	if (!$myo->is_admin or $s_admin) {

		$p					=	0;
		$priv_tick_boxes 	= 	array();
		$user_privs		  	=	explode(';', $myo->privs);
		$ip_address 		= 	in_array($myo->last_login_ip, $digi_ips_array) ? '-' : $myo->last_login_ip;
		$priv_tick_boxes[]	= 	"<input type='checkbox' name='mark_all' title='".TXT_MARK_OR_CLEAR_ALL_PRIVS."'
								 onclick='toggleMarkAll(\"priv_\")'/> <i>".TXT_ALL."</i>";
		
		require_once('inc/definitions/def_privs_'.$cms_lang.'.php');
		foreach($privs_array as $priv_id => $priv) {

			$p++;
			$priv_txt			=	isset($priv_def[$priv]) ? $priv_def[$priv] : $priv;
			$checked 			= 	in_array($priv_id, $user_privs) ? "checked=''" : '';
			$priv_tick_boxes[]	= 	"<input type='checkbox' name='priv_$priv_id' id='priv_$p' $checked/> $priv_txt";
		}
	
	    $pre_html = "
        	<h3>".TXT_EDIT_USER."</h3><hr/>
        	<h6>".TXT_LAST_LOGIN.": ".($myo->last_login ? date("d/m/Y, h:i", $myo->last_login).' '.TXT_FROM.' '.$ip_address : TXT_NA)."</h6><br/>
        	
			<form name='edit_form' method='post' action='$_file'>
			<input type='hidden' name='id' value='$_GET[edit]' />        	
            <table cellspacing='0px' cellpadding='1px' border='0px'>

				<tr>
					<td width='150px'><br/>".TXT_USERNAME.":</td>
					<td><br/><input type='text' name='username' value='$myo->username' class='input_regular'/></td>
				</tr>
				<tr>
					<td>".TXT_NEW_PASS.":</td>
					<td><input type='password' name='newpass_1' value='' class='input_regular' autocomplete='off'/></td>
				</tr>
				<tr>
					<td>".TXT_REPEAT_PASS.":</td>
					<td><input type='password' name='newpass_2' value='' class='input_regular' autocomplete='off'/></td>
				</tr>
				<tr>
					<td>".TXT_FIRST_NAME.":</td>
					<td><input type='text' name='first_name' value='$myo->first_name' class='input_regular'/></td>
				</tr>
				<tr>
					<td>".TXT_LAST_NAME.":</td>
					<td><input type='text' name='last_name' value='$myo->last_name' class='input_regular'/></td>
				</tr>
				<tr>
					<td>".TXT_EMAIL_ADDRESS.":</td>
					<td><input type='text' name='email_address' value='$myo->email_address' class='input_regular'/></td>
				</tr>
				<tr>
					<td>".TXT_USE_IN_ADMIN.":</td>
					<td>".makeSelectBox('use_in_administration',$YESNO_ARRAY,$myo->use_in_administration)."</td>
				</tr>
				".($s_admin ? "
				<tr>
					<td>".TXT_ADMINISTRATOR.":</td>
					<td>".makeSelectBox('is_admin',$YESNO_ARRAY,$myo->is_admin)."</td>
				</tr>
				<tr>
					<td>".TXT_IP_LOGIN_1.":</td>
					<td><input type='text' name='ip_login_1' value='$myo->ip_login_1' class='input_regular'/></td>
				</tr>
				<tr>
					<td>".TXT_IP_LOGIN_2.":</td>
					<td><input type='text' name='ip_login_2' value='$myo->ip_login_2' class='input_regular'/></td>
				</tr>	
				" : '')."	
								
				<tr>
					<td><br/>".TXT_ACTIVE.":</td>
					<td><br/>".($myo->allow_login ? "<a href='$_file?edit=$db_id&deactivate=$db_id&$link_appendix'>$img[ok]</a> ".TXT_YES." &nbsp; <h6>(".TXT_LOGIN_ALLOWED.")</h6>" : "<a href='$_file?edit=$db_id&activate=$db_id&$link_appendix'>$img[notok]</a> ".TXT_NO." &nbsp; <h6>(".TXT_LOGIN_NOT_ALLOWED.")</h6>")."</td>
				</tr>
				
				".(!$myo->is_admin ? "
				<tr>
					<td valign='top'><br/>".TXT_PRIVS.":</td>
					<td><br/>".implode("\n<br/>",$priv_tick_boxes)."</td>
				</tr>
				" : "
				<tr>
					<td valign='top'><br/>".TXT_PRIVS.":</td>
					<td><br/>".TXT_ADMIN_ACCOUNT."</td>
				</tr>					
				")."				

            	<tr>
					<td colspan='2'><br/><input type='submit' name='edit_finish' class='input_submit' value='".BUTTON_SAVE."' /></td>
				</tr>
				
			</table>
			</form><br/>
	    ";
	}
}

if (isset($_GET['view']) and $db_id = (int) $_GET['view'] and $db_id != 1) {

    $res = eq("	SELECT 	username,privs,last_login_ip,allow_login,first_name,last_name,email_address,is_admin,
						UNIX_TIMESTAMP(last_login) AS last_login, use_in_administration,ip_login_1,ip_login_2
				FROM 	$db_table WHERE id = '$db_id';");
    $myo = mfo($res);

	if (!$myo->is_admin or $s_admin) {

		$user_privs		=	explode(';', $myo->privs);
		$priv_overview	=	array();
		$ip_address 	= 	in_array($myo->last_login_ip, $digi_ips_array) ? '-' : $myo->last_login_ip;
		
		require_once('inc/definitions/def_privs_'.$cms_lang.'.php');
		foreach($privs_array as $priv_id => $priv) {
			$checked 			= 	in_array($priv_id, $user_privs) ? $img['ok'] : $img['notok'];
			$priv_txt			=	isset($priv_def[$priv]) ? $priv_def[$priv] : $priv;
			$priv_overview[] 	= 	$checked.' '.$priv_txt;
		}
	
	    $pre_html = "
	        	<h3>".TXT_CMS_USER." $myo->username</h3><hr/>
	        	<h6>".TXT_LAST_LOGIN.": ".($myo->last_login ? date("d/m/Y, h:i", $myo->last_login).' '.TXT_FROM.' '.$ip_address : TXT_NA)."</h6>
	        	<br/><br/>
	       	
	            <table cellspacing='0px' cellpadding='1px' border='0px'>
	
					<tr>
						<td width='150px'>".TXT_NAME.":</td>
						<td>$myo->first_name $myo->last_name</td>
					</tr>
					<tr>
						<td>".TXT_EMAIL_ADDRESS.":</td>
						<td>$myo->email_address</td>
					</tr>
					<tr>
						<td>".TXT_USE_IN_ADMIN.":</td>
						<td>".$YESNO_ARRAY[$myo->use_in_administration]."</td>
					</tr>
					<tr>
						<td>".TXT_ADMINISTRATOR.":</td>
						<td>".$YESNO_ARRAY[$myo->is_admin]."</td>
					</tr>
					".($s_admin ? "
					<tr>
						<td>".TXT_IP_LOGIN_1.":</td>
						<td>$myo->ip_login_1</td>
					</tr>					
					<tr>
						<td>".TXT_IP_LOGIN_2.":</td>
						<td>$myo->ip_login_2</td>
					</tr>
					" : '')."					
					<tr>
						<td><br/>".TXT_ACTIVE.":</td>
						<td><br/>".($myo->allow_login ? "$img[ok] ".TXT_YES." &nbsp; <h6>(".TXT_LOGIN_ALLOWED.")</h6>" : 
														"$img[notok] ".TXT_NO." &nbsp; <h6>(".TXT_LOGIN_NOT_ALLOWED.")</h6>")."
						</td>
					</tr>
					".(!$myo->is_admin ? "
					<tr>
						<td valign='top'><br/>".TXT_PRIVS.":</td>
						<td><br/>".implode("\n<br/>", $priv_overview)."</td>
					</tr>
					" : "
					<tr>
						<td valign='top'><br/>".TXT_PRIVS.":</td>
						<td><br/>".TXT_ADMIN_ACCOUNT."</td>
					</tr>					
					")."
				</table><br/><br/>
	    ";
	}
}

if ($db_id = request('show_log') and $cfg['ACTIVITY_LOGGING'] and checkPriv(PRIV_VIEW_LOGS)) {

    $Activity 	= 	new Activity($db_table_logs, $db_id, request('page'), 40);
	$logs 		=	$Activity->readLog();
	$log_rows	=	array();
	$x			=	0;
	
	foreach ($logs as $timestamp => $log) {
		
		$x++;
		
		list($ip_address, $activity_text) = $log;
		$ip_address = in_array($ip_address, $digi_ips_array) ? '-' : $ip_address;
		
		$log_rows[] = "
			<tr>
				<td width='120px'><h6>".date('d-m-Y, H:i', $timestamp)."</h6></td>
				<td><h6>$activity_text</h6></td>
				<td width='120px' align='right'><h6>$ip_address</h6></td>
			</tr>
		";
	}
	
	if (!count($log_rows)) {
		$log_rows[] = "
			<tr><td><i>".TXT_NO_ACTIVITIES_LOGGED."</i></td></tr>
		";
	}

    $res = eq("SELECT username FROM $db_table WHERE id = '$db_id';");
    $myo = mfo($res);
	
	$pre_html 	=	"
		<h3>".TXT_ACTIVITY_LOG_OF." $myo->username</h3><hr/>
		".(checkPriv(PRIV_PURGE_LOGS) ? "<h6><a href='$_file?purge_log=1&user_id=$db_id&$_app'>".TXT_EMPTY_LOG."</a></h6>
		" : '')."
		<br/><br/>
		
		<table border='0px' cellspacing='0px' cellpadding='1px' class='data_table'>
			".implode("\n", $log_rows)."
		</table>
		<br/>".$Activity->getPageLinks()."
		<br/><br/>
	";
}

/******************************************************************************************
 * Page contents
 ***/

//Page title HTML
$html = "
	<a href='$_file' title='".ucfirst(TXT_OVERVIEW)."'><img src='$site_icon' border='0px' class='cms_icon'/></a>
	<h6>
		<a href='$_file'>".TXT_OVERVIEW."</a> &nbsp;
		".(checkPriv(PRIV_AUTH_USERS) ? "$img[user_add] <a href='$_file?add=1&$_app'>".TXT_NEW_USER."</a> &nbsp;" : '')."
		".((checkPriv(PRIV_PURGE_LOGS) and $cfg['ACTIVITY_LOGGING']) ? "
		 $img[page_del] <a href='$_file?purge_log=1&$_app'>".TXT_EMPTY_ALL_LOGS."</a> &nbsp;" : '')."
	</h6>
	<br/>".$Message->get()."
	<br/><br/>$pre_html
";

//Build the data rows
$res = eq("	SELECT 		id, username, UNIX_TIMESTAMP(last_login) AS last_login, allow_login, login_attempts,
						UNIX_TIMESTAMP(TIMEDIFF(NOW(),last_online)) AS online_check, is_admin
			FROM 		$db_table $where_clause
			ORDER BY 	$order_clause
			LIMIT 		$limit_clause
;"); 
while ($myo = mfo($res)) {

	//Login as functionality
	$login_as = $s_admin ? "<a href='$_file?login_as=$myo->id&$_app' title='".TXT_LOGIN_AS." $myo->username'>$img[user_next]</a> " : '';

	//Online check
	$online_check = ((int) $myo->online_check > 600 or !is_numeric($myo->online_check)) ? 	
					"<span style='color: grey; font-size: 9px;' title='".TXT_USER_IS_OFFLINE."'>".TXT_OFFLINE."</span>" : 
					"<span style='color: green; font-size: 9px;' title='".TXT_USER_IS_ONLINE."'>".TXT_ONLINE."</span>";

	$data_rows[] = array(
		
		//This MUST be the ID
		$myo->id,
		
		//All other desired fields
		$myo->id,
		$login_as."<a href='$_file?view=$myo->id&$_app' title='".TXT_VIEW_USER_DETAILS."'>$myo->username</a>",
		$online_check,
		$myo->last_login ? date("d/m/Y, H:i", $myo->last_login) : '&nbsp;',
		$myo->is_admin ? "<span title='".TXT_USER_IS_ADMIN."'>$img[user_ok]</span>" : '&nbsp;',
		(!$myo->is_admin or $s_admin) ? (
			$myo->login_attempts >= 4 ? 
				"<a href='$_file?activate=$myo->id&$_app' title='".TXT_UNBLOCK_USER."'>$img[notok]</a>"
			: (
			$myo->allow_login ? 
				"<a href='$_file?deactivate=$myo->id&$_app' title='".TXT_DISALLOW_LOGIN."'>$img[ok]</a>" : 
				"<a href='$_file?activate=$myo->id&$_app' title='".TXT_ALLOW_LOGIN."'>$img[notok]</a>"
			)
		) : (
			($myo->login_attempts >= 4 or !$myo->allow_login) ? $img['notok'] : $img['ok']
		),
		
		//And the options				
		((checkPriv(PRIV_VIEW_LOGS) and $cfg['ACTIVITY_LOGGING'] and $myo->id > 1) ? "
			<a href='$_file?show_log=$myo->id&$_app' title='".TXT_SHOW_ACTIVITY_LOG."'>$img[page_search]</a>
		" : '').
		((checkPriv(PRIV_AUTH_USERS) and $myo->id > 1 and (!$myo->is_admin or $s_admin)) ? "
			<a href='$_file?edit=$myo->id&$_app' title='".TXT_EDIT_USER."'>$img[edit]</a>
			<a href='$_file?delete=$myo->id&$_app' title='".TXT_DELETE_USER."'>$img[user_del]</a>
		" : "
		$img[placeholder] $img[placeholder]
		")
	);
}

//Output data table
$DataTable = new DataTable();
$DataTable->setMarkAllOptions(	array(TXT_DELETE,'delete',TXT_DELETE_USERS),
								array(TXT_ACTIVATE,'activate',TXT_ACTIVATE_USERS),
								array(TXT_DEACTIVATE,'deactivate',TXT_DEACTIVATE_USERS));
$DataTable->showSearch();
$DataTable->setDataRows($data_rows);
$DataTable->setLegendRow(
	array('#',TXT_USER,'',ucfirst(TXT_LAST_LOGIN),TXT_ADMIN,TXT_ACTIVE,TXT_OPTIONS),
	array(60,0,60,130,60,60,60),
	array('id','username','last_online','last_login','is_admin','allow_login')
);
echo $html.$DataTable->getHTML();

require_once('inc/cms_footer.php');
?>