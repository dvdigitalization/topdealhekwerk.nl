<?php
/*
Title:		Manage SPAM words (for form data filtering)
File: 		cms_bad_words.php
Version: 	v2.04
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

/******************************************************************************************
 * Settings
 ***/

// *** Required files
require_once('inc/cms_header.php');
require_once('inc/lang/'.$cms_lang.'_'.$_file);
require_once(CMS_PATH_PREFIX.'classes/class_DataTable.php');

// *** Extra table definitions
$db_table_bad_words		= 	'blocked_words';

// *** Sorting and pages functionality
$default_sort_option	=	'word';
$default_sort_method	=	'ASC';
$secondary_sort_option	=	'';
$secondary_sort_method	=	'';
$valid_sort_options		=	array('id','word','usage_count','ban_ip','sub_string');

// *** Search constraints
if (!empty($search_query)) {
	$search				=	array();
	$search[]			=	"word LIKE '%$search_query%'";
	$where[]			=	'('.implode(' OR ', $search).')';
}

// *** Sorting and pages and link appendix
require_once(CMS_PATH_PREFIX.'inc/sorting_and_pages.php');
$_app					=	$page_appendix.'&'.$sort_appendix.'&'.$link_appendix;

/******************************************************************************************
 * POST and GET Cases
 ***/

// *** Add
if (isset($_POST['add_finish'])) {
	
	evalAll($_POST['word'], false);
	
	if (empty($_POST['word'])) {
		$Message->set($msg_no_word, CRITICAL);
		$_GET['add'] = 1;
	}
	elseif(countResults($db_table, "word = '".$_POST['word']."'")) {
		$Message->set($msg_word_exists);
	}
	else {
		$db_id = insRec($db_table, array('word','ban_ip','sub_string'),
								   array($_POST['word'], isset($_POST['ban_ip']) ? 1 : 0, isset($_POST['sub_string']) ? 1 : 0));	
		
		cmsLog("$object '$_POST[word]' (#$db_id) ".LOG_ADDED);
		$Message->set($msg_added);
		
		$_GET['add'] = 1;
	}
}

// *** Edit
if (isset($_POST['edit_finish']) and $db_id = (int) $_POST['id']) {
	
	evalAll($_POST['word'], false);
	
	if (empty($_POST['word'])) {
		$Message->set($msg_no_word, CRITICAL);
		$_GET['edit'] = $db_id;
	}
	elseif(countResults($db_table, "word = '".$_POST['word']."' AND id <> '$db_id'")) {
		$Message->set($msg_word_exists);
		$_GET['edit'] = $db_id;
	}
	else {
		upRec($db_table, $db_id, 'word', $_POST['word']);
		
		cmsLog("$object '$_POST[word]' (#$db_id) ".LOG_EDITTED);
		$Message->set($msg_editted);
	}
}


// *** Delete confirmation
if (isset($_GET['delete']) and $id_holder = $_GET['delete']) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			$Message->setConfirmMultiple($msg_delete_confirm_m, 'delete',$db_ids,'word');
		}
	}
	else if ($db_id = (int) $id_holder) {
		$Message->setConfirm($msg_delete_confirm,'delete',$db_id,'word');
	}	
}

// *** Delete action
if (isset($_GET['delete_confirm']) and $id_holder = $_GET['delete_confirm'] and noBrowse()) {

	if (isset($_GET['multiple_confirm']) and is_array($id_holder)) {

		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			delRec($db_table, $db_ids);
			cmsLog(LOG_MULTIPLE.' '.$plural_lc.' '.LOG_DELETED);
			$Message->set($msg_deleted_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		delRec($db_table, $db_id);
		cmsLog("$object (#$db_id) ".LOG_DELETED);
		$Message->set($msg_deleted);	
	}	
}

// *** IP block words
if (isset($_GET['block']) and $id_holder = $_GET['block'] and noBrowse()) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {

		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			upRec($db_table,$db_ids,'ban_ip',1);
			$Message->set($msg_blocking_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		upRec($db_table,$db_id,'ban_ip',1);
		$Message->set($msg_blocking);	
	}	
}

// *** Don't IP block words
if (isset($_GET['no_block']) and $id_holder = $_GET['no_block'] and noBrowse()) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {

		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			upRec($db_table,$db_ids,'ban_ip',0);
			$Message->set($msg_not_blocking_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		upRec($db_table,$db_id,'ban_ip',0);
		$Message->set($msg_not_blocking);	
	}	
}

// *** Sub string toggling
if (isset($_GET['sub_string']) and $db_id = (int) $_GET['sub_string'] and noBrowse()) {
	upRec($db_table,$db_id,'sub_string',1);
	$Message->set($msg_substr);	
}

if (isset($_GET['no_sub_string']) and $db_id = (int) $_GET['no_sub_string'] and noBrowse()) {
	upRec($db_table,$db_id,'sub_string',0);
	$Message->set($msg_no_substr);		
}

/******************************************************************************************
 * Add / edit / view options
 ***/

if (isset($_GET['add'])) {
	
	$bad_word = isset($_POST['word']) ? $_POST['word'] : '';
	
	$pre_html = "
	
		<h3>".TXT_ADD_ITEM.":</h3><hr/><br/>
		<form name='spam_word_form' method='post' action='$_file'>
		<table border='0px' cellspacing='0px' cellpadding='1px'>
			<tr>
				<td width='150px'>".TXT_WORD_SENTENCE.":</td>
				<td><input type='text' name='word' value='$bad_word' class='input_regular'/></td>
			</tr>
			<tr>
				<td><br/>".TXT_IP_BLOCK.":</td>
				<td><br/><input type='checkbox' name='ban_ip' value='1' ".(isset($_POST['ban_ip']) ? $chk : '')."/> ".TXT_BLOCK_ITEM."</td>
			</tr>
			<tr>
				<td>".TXT_SEPARATE.":</td>
				<td><input type='checkbox' name='sub_string' value='1' ".(isset($_POST['sub_string']) ? $chk : '')."/> ".TXT_FILTER_SEPARATE_ONLY."</td>
			</tr>
			<tr>
				<td>&nbsp;</td><td><br/><input type='submit' name='add_finish' value='".BUTTON_ADD."' class='input_submit'/></td>
			</tr>			
		</table>
		</form><br/>	
	";
}

if (isset($_GET['edit']) and $db_id = (int) $_GET['edit']) {
	
	$pre_html = "
	
		<h3>".TXT_EDIT_ITEM.":</h3><hr/><br/>
		<form name='spam_word_edit_form' method='post' action='$_file'>
		<input type='hidden' name='id' value='$db_id' />
		<table border='0px' cellspacing='0px' cellpadding='1px'>
			<tr>
				<td width='150px'>".TXT_WORD_SENTENCE.":</td>
				<td><input type='text' name='word' value='".getMyo($db_table,$db_id,'word')."' class='input_regular'/></td>
			</tr>
			<tr>
				<td>&nbsp;</td><td><br/><input type='submit' name='edit_finish' value='".BUTTON_SAVE."' class='input_submit'/></td>
			</tr>			
		</table>
		</form><br/>	
	";
}

/******************************************************************************************
 * Page contents
 ***/

//Page title HTML
$html = 	"
	<a href='$_file'><img src='$site_icon' border='0px' class='cms_icon'/></a>
	<h6>
		<a href='$_file'>".TXT_OVERVIEW."</a> &nbsp;
		$img[comment_add] <a href='$_file?add=1'>".TXT_NEW_ITEM."<a/>	
	</h6>
	<br/>".$Message->get()."
	<br/><br/>$pre_html
";

//Get items for this page and this sorting method
$res = eq("	SELECT 		id, word, ban_ip, usage_count, sub_string
			FROM 		$db_table $where_clause
			ORDER BY 	$order_clause
			LIMIT 		$limit_clause			
;");
while ($myo = mfo($res)) {

	$data_rows[] = array(
	
		//This MUST be the id
		$myo->id,
		
		//All desired other fields
		$myo->id,
		$myo->word,
		$myo->usage_count.'x',
		
		$myo->ban_ip ?
		"<a href='$_file?no_block=$myo->id&$_app' title='".TXT_UNBLOCK_ITEM."'>$img[ok]</a>" :
		"<a href='$_file?block=$myo->id&$_app' title='".TXT_BLOCK_ITEM."'>$img[notok]</a>",
		
		//sub_string = 1 -> alleen inwendige string
		$myo->sub_string ?
		"<a href='$_file?no_sub_string=$myo->id&$_app' title='".TXT_FILTER_SEPARATE_ONLY."'>$img[notok]</a>" :
		"<a href='$_file?sub_string=$myo->id&$_app' title='".TXT_FILTER_STRING."'>$img[ok]</a>",
									
		//The options last
		"<a href='$_file?edit=$myo->id&$_app' title='".TXT_EDIT_ITEM."'>$img[edit]</a>
		 <a href='$_file?delete=$myo->id&$_app' title='".TXT_DELETE_ITEM."'>$img[comment_del]</a>"
	);
}

//Output data table
$DataTable = new DataTable();
$DataTable->setMarkAllOptions(	array(TXT_DELETE, 		'delete', 		TXT_DELETE_ITEMS),
								array(TXT_BLOCK,		'block',		TXT_BLOCK_ITEMS),
								array(TXT_UNBLOCK,		'no_block',		TXT_UNBLOCK_ITEMS));
$DataTable->showSearch();
$DataTable->setDataRows($data_rows);
$DataTable->setLegendRow(
	array('#', TXT_WORD_SENTENCE, TXT_BLOCKED, TXT_IP_BLOCK, TXT_SEPARATE, TXT_OPTIONS),
	array(60,0,100,70,100,40),
	array('id','word','usage_count','ban_ip','sub_string')
);
echo $html.$DataTable->getHTML();

require_once('inc/cms_footer.php');
?>