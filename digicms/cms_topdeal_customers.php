<?php
/*
Title:		Customers management [TOPDEAL]
File: 		cms_topdeal_customers.php
Version: 	v2.20
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

/******************************************************************************************
 * Settings
 ***/

// *** Required files
require_once('inc/cms_header.php');
require_once(CMS_PATH_PREFIX.'classes/class_Customer_Apple.php');
require_once(CMS_PATH_PREFIX.'classes/class_DataTable.php');
require_once(CMS_PATH_PREFIX.'classes/class_Order.php');

// *** Extra database table definitions
$db_table_orders		=	DIGI_DB_PREFIX.'topdeal_offertes';

// *** File definitions
$file_orders			=	'cms_topdeal_offertes.php';
$file_invoice_pdf		=	'cms_invoice_shop_pdf.php';

// *** Object definition
$object					=	'Klant';
$object_lc 				= 	'klant';
$plural					=	'Klanten';
$plural_lc				=	'klanten';
$adjective_e			=	'e';
$ident					=	'Deze';

// *** Sorting and pages functionality
$default_sort_option	=	'last_name';
$default_sort_method	=	'ASC';
$secondary_sort_option	=	'';
$secondary_sort_method	=	'';
$valid_sort_options		=	array('id','last_name','timestamp','language','newsletter');

// *** ID and invoice no formatting
$id_format				=	$cfg['ORDER_ID_FORMAT'];
$invoice_format			=	$cfg['INVOICE_NO_FORMAT'];

// *** Deleted check
$deleted				=	(int) request('deleted');

// *** Where clauses
if ($deleted) {
	$where[] 			=	'deleted = 1';
	$plural_lc			=	'verwijderde klanten';
	$object_lc			=	'verwijderde klant';
}
else {
	$where[] 			=	'deleted = 0';
}

// *** Search constraints
if (!empty($search_query)) {
	$search				=	array();
	$search[]			=	"first_name LIKE '%$search_query%'";
	$search[]			=	"last_name LIKE '%$search_query%'";
	$search[]			=	"email LIKE '%$search_query%'";
	$search[]			=	"city LIKE '%$search_query%'";
	$where[]			=	'('.implode(' OR ', $search).')';
}

// *** Message definitions
$msg_added				=	'Klant toegevoegd.';
$msg_editted			=	'Klant gegevens opgeslagen.';
$msg_deleted			=	'Klant verwijderd.';
$msg_deleted_m 			= 	'Klanten verwijderd.';
$msg_delete_confirm		= 	'Deze klant zeker weten verwijderen?';
$msg_delete_confirm_m 	= 	'Deze klanten zeker weten verwijderen?';
$msg_perma_confirm		= 	'Deze klant en bijbehorende bestellingen zeker weten permanent verwijderen?';
$msg_perma_confirm_m 	= 	'Deze klanten en bijbehorende bestellingen zeker weten permanent verwijderen?';
$msg_del_orders			=	'Bijbehorende bestellingen verwijderd.';
$msg_restored 			= 	'Klant teruggezet.';
$msg_restored_m 		= 	'Klanten teruggezet.';
$msg_restore_confirm	= 	'Deze klanten zeker weten terugzetten?';
$msg_restore_confirm_m 	= 	'Deze klanten zeker weten terugzetten?';
$msg_imported			=	'Er zijn [AMOUNT] klanten geimporteerd.';
$msg_activated			=	'Deze klant zal vanaf nu nieuwsbrieven ontvangen.';
$msg_activated_m		=	'Deze klanten zullen vanaf nu nieuwsbrieven ontvangen.';
$msg_deactivated		=	'Deze klant zal vanaf nu geen nieuwsbrieven meer ontvangen.';
$msg_deactivated_m		=	'Deze klanten zullen vanaf nu geen nieuwsbrieven meer ontvangen.';

// *** Extra appendix
$link_appendix 			.= 	'&deleted='.$deleted;

// *** Sorting and pages and link appendix
require_once(CMS_PATH_PREFIX.'inc/sorting_and_pages.php');
$_app					=	$page_appendix.'&'.$sort_appendix.'&'.$link_appendix;

/******************************************************************************************
 * POST and GET Cases
 ***/

if (isset($_POST['add_finish']) or isset($_POST['edit_finish'])) {

	foreach(array_keys($_POST) as $key) {
		evalAll($_POST[$key]);
	}

	$_POST['timestamp'] = mysql_time(date2time($_POST['timestamp']));
}

if (isset($_POST['add_finish']) and checkPriv(PRIV_ADD_CUSTOMERS)) {

    $db_id = insRec($db_table,
            array(	'timestamp','title','first_name','last_name','address','nr','postal_code',
					'city','country','phone','mobile','prefix','email','newsletter','remarks','password','language'),
           	array(	$_POST['timestamp'],$_POST['title'],$_POST['first_name'],$_POST['last_name'],$_POST['address'],$_POST['nr'],
			   		$_POST['postal_code'],$_POST['city'],$_POST['country'],$_POST['phone'],$_POST['mobile'],$_POST['prefix'],
		   			$_POST['email'],isset($_POST['newsletter']) ? 1 : 0,
					$_POST['remarks'],sha1($_POST['password']),$_POST['language']));

    //Delete subscriber, if given
    if (isset($_POST['nb_id']) and $_POST['nb_id'] > 0) delRec($db_table_subscribers,$_POST['nb_id']);

	cmsLog("$object $_POST[first_name] $_POST[last_name] (#$db_id) toegevoegd.");
	$Message->set($msg_added);
}

if (isset($_POST['edit_finish']) and isset($_POST['id']) and $db_id = (int) $_POST['id'] and checkPriv(PRIV_EDIT_CUSTOMERS)) {

	if ($_POST['new_password']) {
		$_POST['password'] = sha1($_POST['new_password']);
	}

	upRec($db_table,$db_id,
            array(	'timestamp','title','first_name','last_name','address','nr', 'postal_code',
					'city','country','phone','mobile','prefix','email','newsletter','remarks','password','language'),
            array(	$_POST['timestamp'],$_POST['title'],$_POST['first_name'],$_POST['last_name'],$_POST['address'],$_POST['nr'],
					$_POST['postal_code'],$_POST['city'],$_POST['country'],$_POST['phone'],$_POST['mobile'],$_POST['prefix'],
					$_POST['email'],isset($_POST['newsletter']) ? 1 : 0,
					$_POST['remarks'],$_POST['password'],$_POST['language']));

    cmsLog("$object $_POST[first_name] $_POST[last_name] (#$db_id) gewijzigd.");
    $Message->set($msg_editted);
}

// *** Delete confirmation
if (isset($_GET['delete']) and $id_holder = $_GET['delete'] and checkPriv(PRIV_DELETE_CUSTOMERS)) {

	$msg_delete_confirm_m 	= $deleted ? $msg_perma_confirm_m 	: $msg_delete_confirm_m;
	$msg_delete_confirm 	= $deleted ? $msg_perma_confirm 	: $msg_delete_confirm;

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			$Message->setConfirmMultiple($msg_delete_confirm_m, 'delete',$db_ids,'last_name');
		}
	}
	else if ($db_id = (int) $id_holder) {
		$Message->setConfirm($msg_delete_confirm,'delete',$db_id,'last_name');
	}
}

// *** Delete action
if (isset($_GET['delete_confirm']) and $id_holder = $_GET['delete_confirm'] and noBrowse() and checkPriv(PRIV_DELETE_CUSTOMERS)) {

	if (isset($_GET['multiple_confirm']) and is_array($id_holder)) {

		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			if ($deleted) {
				$res = eq("SELECT id FROM $db_table_orders WHERE customer_id IN (".implode(',',$db_ids).");");
				while ($myo = mfo($res)) {
					$Order = new Order($myo->id);
					$Order->deleteOrder(true);
				}
				cmsLog("Meerdere $plural permanent verwijderd.");
				$Message->set($msg_deleted_m.'<br/>'.$msg_del_orders);
			}
			else {
				upRec($db_table, $db_ids, 'deleted', 1);
				cmsLog("Meerdere $plural verwijderd.");
				$Message->set($msg_deleted_m);
			}
		}
	}
	else if ($db_id = (int) $id_holder) {
		if ($deleted) {
			$res = eq("SELECT id FROM $db_table_orders WHERE customer_id = '$db_id';");
			while ($myo = mfo($res)) {
				$Order = new Order($myo->id);
				$Order->deleteOrder(true);
			}
			cmsLog("$object (#$db_id) permanent verwijderd.");
			$Message->set($msg_deleted.'<br/>'.$msg_del_orders);
		}
		else {
			upRec($db_table, $db_id, 'deleted', 1);
			cmsLog("$object (#$db_id) verwijderd.");
			$Message->set($msg_deleted);
		}
	}
}

// *** Restore confirmation
if (isset($_GET['restore']) and $id_holder = $_GET['restore'] and checkPriv(PRIV_DELETE_CUSTOMERS)) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			$Message->setConfirmMultiple($msg_restore_confirm_m, 'restore',$db_ids,'last_name');
		}
	}
	else if ($db_id = (int) $id_holder) {
		$Message->setConfirm($msg_restore_confirm,'restore',$db_id,'last_name');
	}
}

// *** Restore action
if (isset($_GET['restore_confirm']) and $id_holder = $_GET['restore_confirm'] and noBrowse() and checkPriv(PRIV_DELETE_CUSTOMERS)) {

	if (isset($_GET['multiple_confirm']) and is_array($id_holder)) {

		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			upRec($db_table, $db_ids, 'deleted', 0);
			cmsLog("Meerdere $plural teruggezet.");
			$Message->set($msg_restored_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		upRec($db_table, $db_id, 'deleted', 0);
		cmsLog("$object (#$db_id) teruggezet.");
		$Message->set($msg_restored);
	}
}

// *** Subscribe
if (isset($_GET['subscribe']) and $id_holder = $_GET['subscribe'] and noBrowse()) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			upRec($db_table,$db_ids,'newsletter',1);
			cmsLog("Meerdere $plural_lc aangemeld voor nieuwsbrief");
			$Message->set($msg_activated_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		upRec($db_table,$db_id,'newsletter',1);
		cmsLog("$object (#$db_id) aangemeld voor nieuwsbrief");
		$Message->set($msg_activated);
	}
}

// *** Un-subscribe
if (isset($_GET['unsubscribe']) and $id_holder = $_GET['unsubscribe'] and noBrowse()) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			upRec($db_table,$db_ids,'newsletter',0);
			cmsLog("Meerdere $plural_lc afgemeld van nieuwsbrief");
			$Message->set($msg_deactivated_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		upRec($db_table,$db_id,'newsletter',0);
		cmsLog("$object (#$db_id) afgemeld van nieuwsbrief");
		$Message->set($msg_deactivated);
	}
}

// *** Import
if (isset($_POST['import_finish']) and !empty($_FILES['import_data']['name'])) {

	//Upload the file
	if (($result = uploadFile('import_data', $upload_dir, 'import', $chmod, false, true, array('csv'))) !== false) {

		//Get file info
		list($file_name, $file_extension) = $result;
		$file_type = strtoupper(substr($file_extension,1));

		//Set the item and props definitions
		$item	=	'customer';
		$props	=	array('first_name','last_name','address','phone','postal_code','city','email','password');

		//Set up import engine
		require_once(CMS_PATH_PREFIX.'classes/class_ImportEngine.php');
		$ImportEngine = new ImportEngine($upload_dir.$_sep.$file_name, $file_type, $item, $props);

		//Process data
		if ($ImportEngine->process()) {

			if ($count = $ImportEngine->get_count()) {

				//Get data and import
				while ($values = $ImportEngine->get_element()) {

					//Make database props and values arrays
					$db_props		=	array(	'first_name','last_name','address','phone','postal_code','city','email',
												'password','newsletter','country');
					$db_values		=	array(	$values[0], $values[1], $values[2], $values[3], $values[4], $values[5], $values[6],
												$values[7] ? dbSaltSite($values[7]) : '',1,124);

					//Insert customer
					$customer_id = insRec($db_table, $db_props, $db_values);
				}

				//Log and message
				cmsLog("$count $plural geimporteerd.");
				$Message->set(str_replace('[AMOUNT]', $count, $msg_imported));
			}
			else {
				$Message->set($msg_no_data, WARNING);
			}

			//Delete the temporary file
			@unlink($upload_dir.$_sep.$file_name);
		}
		else {
			$Message->set($msg_process_error, CRITICAL);
		}
	}
	else {
		$Message->set($msg_upload_error.'<br/><br/>'.$_error, WARNING);
	}
}

/******************************************************************************************
 * Add / Edit / View options
 ***/

if (isset($_GET['export'])) {

	define('TEMP_FOLDER', '../user_files/tmp/');
	require_once(CMS_PATH_PREFIX.'classes/export.php');
	$values = array();
	$res = eq("SELECT * FROM $db_table ORDER BY last_name ASC;");
	while ($myo = mfo($res)) {
		$values[] = array(	$myo->first_name,$myo->last_name,$myo->prefix,$myo->address,$myo->nr,$myo->postal_code,$myo->city,
							$myo->email,$myo->phone);
	}

	$Export = new Export();
	$Export->set_csv_delimiter(';');
	$Export->set_fields(array('Voornaam','Achternaam','Voorvoegsel','Adres','Huisnummer','Postcode','Plaats','E-mail','Telefoon'));
	$Export->set_values($values);
	$Export->download_csv('klanten.csv');
}

if (isset($_GET['import'])) {

	$pre_html = "
		<h3>Klanten importeren:</h3><hr/>
		<p>Met onderstaand formulier kunt u klanten importeren. U kunt hiervoor een CSV bestand gebruiken met de volgende kolommen:
		<br/><br/>
		voornaam,achternaam,adres,telefoonnummer,postcode,plaats,emailadres,wachtwoord
		<br/><br/>
		<form name='import_form' method='post' action='$_file' enctype='multipart/form-data'>
		Bestand: <input type='file' name='import_data' class='input_file'/>
		<input type='submit' class='input_submit' name='import_finish' value='Importeren' />
		</form>
		<br/><br/>
	";
}

if (isset($_GET['add']) and checkPriv(PRIV_ADD_CUSTOMERS)) {

    $pre_html = "
        <h3>Klant toevoegen:</h3><br/>
        <form name='add_form' method='post' action='$_file'>
        $page_and_sort_inputs
        <table cellspacing='0px' cellpadding='1px' border='0px'>

			<tr>
				<td width='150px'>Klant sinds:</td>
				<td><input type='text' readonly='readonly' name='timestamp' id='timestamp' class='input_calendar' value='".date('d/m/Y', time())."' />
					<button class='calendar_button' type='reset' id='timestamp_trigger'>...</button></td>
			</tr>
			<tr>
				<td><br/>Aanhef:</td>
				<td><br/>".makeSelectBox('title',$SEX_ARRAY,1)."</td>
			</tr>
            <tr>
				<td>Voornaam:</td>
				<td><input type='text' name='first_name' class='input_regular' /></td>
			</tr>
            <tr>
				<td>Tussenvoegsel:</td>
				<td><input type='text' name='prefix' class='input_regular' /></td>
			</tr>
            <tr>
				<td>Achternaam:</td>
				<td><input type='text' name='last_name' class='input_regular' /></td>
			</tr>
            <tr>
				<td>Straat en huisnummer:</td>
				<td><input type='text' name='address' class='input_medium'/>
					<input type='text' name='nr' class='input_small' /></td>
			</tr>
            <tr>
				<td>Postcode en plaats:</td>
				<td><input type='text' name='postal_code' class='input_small'/>
					<input type='text' name='city' class='input_medium' /></td>
			</tr>
            <tr>
				<td>Land:</td>
				<td>".makeSelectBox('country',$COUNTRY_ARRAY,124)."</td>
			</tr>
            <tr>
				<td>Telefoon:</td>
				<td><input type='text' name='phone' class='input_regular' /></td>
			</tr>
            <tr>
				<td>Mobiel:</td>
				<td><input type='text' name='mobile' class='input_regular' /></td>
			</tr>
            <tr>
				<td>Email:</td>
				<td><input type='text' name='email' class='input_regular' /></td>
			</tr>
			<tr>
				<td>Taal:</td>
				<td>".makeSelectBox('language',$installed_languages,$_lang)."</td>
			</tr>
			<tr>
				<td>Wachtwoord:</td>
				<td><input type='text' name='password' value='' class='input_regular'/></td>
			</tr>
            <tr>
				<td><br/>Nieuwsbrief:</td>
				<td><br/><input type='checkbox' name='newsletter' /> Ja, klant ontvangt nieuwsbrief</td>
			</tr>
            <tr>
				<td colspan='2'><br/>Opmerkingen:<br/><textarea name='remarks'></textarea></td>
			</tr>
            <tr>
				<td colspan='2'><br/><input type='submit' name='add_finish' value='Toevoegen' class='input_submit' /></td>
			</tr>

        </table>
        </form><br/>

        <script type='text/javascript'>
			Calendar.setup({
		        inputField     :    'timestamp',      		// id of the input field
		        button         :    'timestamp_trigger'   	// trigger for the calendar (button ID)
		    });
        </script>
    ";
}

if (isset($_GET['edit']) and $db_id = (int) $_GET['edit'] and checkPriv(PRIV_EDIT_CUSTOMERS)) {

    $res = eq("SELECT title,first_name,last_name,address,nr,postal_code,city,country,phone,email,newsletter,remarks,password,UNIX_TIMESTAMP(timestamp) AS timestamp,language FROM $db_table WHERE id = '$db_id';");
    $myo = mfo($res);

    $pre_html = "
        <h3>Klant gegevens bijwerken:</h3><br/>
        <form name='edit_form' method='post' action='$_file'>
        <input type='hidden' name='id' value='$db_id' />
        <input type='hidden' name='password' value='$myo->password' />
        $page_and_sort_inputs
        <table cellspacing='0px' cellpadding='1px' border='0px'>

			<tr>
				<td width='150px'>Klant sinds:</td>
				<td><input type='text' readonly='readonly' name='timestamp' id='timestamp' class='input_calendar' value='".date('d/m/Y', $myo->timestamp)."' />
					<button class='calendar_button' type='reset' id='timestamp_trigger'>...</button></td>
			</tr>
			<tr>
				<td><br/>Aanhef:</td>
				<td><br/>".makeSelectBox('title',$SEX_ARRAY,$myo->title)."</td>
			</tr>
            <tr>
				<td>Voornaam:</td>
				<td><input type='text' name='first_name' class='input_regular' value='$myo->first_name'/></td>
			</tr>
            <tr>
				<td>Tussenvoegsel:</td>
				<td><input type='text' name='prefix' class='input_regular' value='$myo->prefix'/></td>
			</tr>
            <tr>
				<td>Achternaam:</td>
				<td><input type='text' name='last_name' class='input_regular' value='$myo->last_name'/></td>
			</tr>
            <tr>
				<td>Straat en huisnummer:</td>
				<td><input type='text' name='address' class='input_medium' value='$myo->address'/>
					<input type='text' name='nr' class='input_small' value='$myo->nr'/></td>
			</tr>
            <tr>
				<td>Postcode en plaats:</td>
				<td><input type='text' name='postal_code' class='input_small' value='$myo->postal_code'/>
					<input type='text' name='city' class='input_medium' value='$myo->city'/></td>
			</tr>
            <tr>
				<td>Land:</td>
				<td>".makeSelectBox('country',$COUNTRY_ARRAY,$myo->country)."</td>
			</tr>
            <tr>
				<td>Telefoon:</td>
				<td><input type='text' name='phone' class='input_regular' value='$myo->phone'/></td>
			</tr>
            <tr>
				<td>Mobiel:</td>
				<td><input type='text' name='mobile' class='input_regular' value='$myo->mobile'/></td>
			</tr>
            <tr>
				<td>Email:</td>
				<td><input type='text' name='email' class='input_regular' value='$myo->email'/></td>
			</tr>
			<tr>
				<td>Taal:</td>
				<td>".makeSelectBox('language',$installed_languages,$myo->language)."</td>
			</tr>

            <tr>
				<td><br/>Nieuwsbrief:</td>
				<td><br/><input type='checkbox' name='newsletter' ".($myo->newsletter ? "checked='checked'" : '')."/> Ja, klant ontvangt nieuwsbrief</td>
			</tr>

            <tr>
				<td colspan='2'><br/>Opmerkingen:<br/><textarea name='remarks'>$myo->remarks</textarea></td>
			</tr>
			<tr>
				<td><br/>Wachtwoord:</td>
				<td><br/>".($myo->password ? 'ja, wachtwoord is ingesteld' : 'nee, niet ingesteld')."</td>
			</tr>
			<tr>
				<td>Nieuw wachtwoord:</td>
				<td><input type='text' name='new_password' value='' class='input_regular'/></td>
			</tr>
            <tr>
				<td colspan='2'><br/><input type='submit' name='edit_finish' value='Opslaan' class='input_submit' /></td>
			</tr>

        </table>
        </form>

        <script type='text/javascript'>
			Calendar.setup({
		        inputField     :    'timestamp',      		// id of the input field
		        button         :    'timestamp_trigger'   	// trigger for the calendar (button ID)
		    });
        </script>
    ";

}

if (isset($_GET['view']) and $db_id = (int) $_GET['view']) {

	$Customer 	= new Customer_Shop($db_id);

    $pre_html = "
        <h3>Klant gegevens:</h3><br/>
        <table cellspacing='0px' cellpadding='1px' border='0px'>

            <tr>
				<td width='150px'>Klant ID:</td>
				<td>#".$Customer->getId()." <a href='$_file?edit=$db_id&$_app' title='$object gegevens bijwerken'>$img[edit]</a>
											<a href='$_file?delete=$db_id&$_app' title='$object verwijderen'>$img[del]</a></td>
			</tr>
            <tr>
				<td>Klant sinds:</td>
				<td>".$Customer->getSince()."</td>
			</tr>
            <tr>
				<td valign='top'><br/>Adres gegevens:</td>
				<td><br/>".$Customer->getAddressLabel()."</td>
			</tr>
            <tr>
				<td><br/>Telefoon:</td>
				<td><br/>".$Customer->getPhone()."</td>
			</tr>
            <tr>
				<td>Email:</td>
				<td>".$Customer->getEmailLink()."</td>
			</tr>
			<tr>
				<td>Taal:</td>
				<td>".$Customer->getLanguage()."</td>
			</tr>
            <tr>
				<td>Nieuwsbrief:</td>
				<td>".(($Customer->hasNewsletter()) ? 'Ja' : 'Nee')."</td>
			</tr>
            <tr>
				<td valign='top'><br/>Opmerkingen:</td>
				<td><br/>".$Customer->getRemarks()."</td>
			</tr>

        </table><br/><br/>
    ";
}

/******************************************************************************************
 * Page contents
 ***/

//Page title HTML
$html = "
	<a href='$_file'><img src='$site_icon' border='0px' class='cms_icon'/></a>
	<h6>
		<a href='$_file'>overzicht</a> &nbsp;
		".(checkPriv(PRIV_ADD_CUSTOMERS) ? "$img[user_add] <a href='$_file?add=1'>nieuwe klant</a> &nbsp;
		$img[user_add] <a href='$_file?import'>klanten importeren</a> &nbsp;" : "")."
		$img[user_next] <a href='$_file?export'>klanten exporteren</a> &nbsp;
		$img[users_del] <a href='$_file?deleted=1'>verwijderde klanten</a>
	</h6>
	<br/>".$Message->get()."
	<br/><br/>$pre_html
";


//Get items for this page and this sorting method
$res = eq("	SELECT 		id, title, first_name, last_name, email, UNIX_TIMESTAMP(timestamp) AS timestamp, deleted, newsletter, language
			FROM 		$db_table $where_clause
			ORDER BY 	$order_clause
			LIMIT 		$limit_clause
;");

while ($myo = mfo($res)) {

	$data_rows[] = array(
		//This MUST be the id
		$myo->id,

		//All desired other fields
		$myo->id,
		"<a href='$mailer$myo->email' title='Stuur deze klant een email'>$img[mail]</a>
		<a href='$_file?view=$myo->id&$$_app' title='$object gegevens bekijken'>".$SEX_ARRAY[$myo->title]."$myo->first_name $myo->last_name</a>",
		(int) countResults($db_table_orders, "customer_id='$myo->id'"),
		$myo->language,
		$myo->newsletter ?  	"<a href='$_file?unsubscribe=$myo->id&$_app' title='Klant afmelden voor nieuwsbrief'>$img[mail_ok]</a>"
						 :		"<a href='$_file?subscribe=$myo->id&$_app' title='Klant aanmelden voor nieuwsbrief'>$img[mail_del]</a>",
		date('d/m/Y', $myo->timestamp),

		//The options
		(($myo->deleted and checkPriv(PRIV_DELETE_CUSTOMERS)) ? "
		<a href='$_file?restore=$myo->id&$_app' title='Verwijderde klant terugzetten'>$img[user_prev]</a>
		" : "")."
		".((!$myo->deleted and checkPriv(PRIV_EDIT_CUSTOMERS)) ? "<a href='$_file?edit=$myo->id&$_app' title='$object gegevens bewerken'>$img[edit]</a>" : "")."
		".((!$myo->deleted and checkPriv(PRIV_DELETE_CUSTOMERS)) ? "<a href='$_file?delete=$myo->id&$_app' title='$object verwijderen'>$img[user_del]</a>" : "")
	);
}

//Output data table
$DataTable = new DataTable();

if (request('deleted')) $DataTable->setMarkAllOptions(	array('restore','restore',$plural.' terugzetten'),
														array('verwijder permanent','delete',$plural.' permanent verwijderen'));
else 					$DataTable->setMarkAllOptions(	array('verwijder','delete',$plural.' verwijderen'),
														array('meld aan','subscribe',$plural.' aanmelden voor nieuwsbrief'),
														array('meld af','unsubscribe',$plural.' afmelden van nieuwsbrief'));

$DataTable->showSearch(array('deleted' => request('deleted')));
$DataTable->setDataRows($data_rows);
$DataTable->setLegendRow(
	array('#','Naam','Bestellingen','Taal','Nieuwsbrief','Klant sinds','Opties'),
	array(60,0,110,60,100,100,40),
	array('id','last_name','','language','newsletter','timestamp')
);

echo $html.$DataTable->getHTML();

require_once('inc/cms_footer.php');
?>