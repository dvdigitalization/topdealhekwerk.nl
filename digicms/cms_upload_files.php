<?
/*
Title:		Resources uploader for use in FCKeditor
File: 		cms_upload_files.php
Version: 	v2.04
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

/******************************************************************************************
 * Settings
 ***/

// *** Give the user 10 minutes max to upload his files
ini_set('max_execution_time',600);

// *** Required files
require_once('inc/cms_header.php');
require_once('inc/lang/'.$cms_lang.'_'.$_file);
require_once(CMS_PATH_PREFIX.'classes/class_ImageHandler.php');
require_once(CMS_PATH_PREFIX.'classes/class_DataTable.php');
require_once(CMS_PATH_PREFIX.'inc/functions/change_file_extension.php');
require_once('inc/definitions/def_file_types_'.$cms_lang.'.php');
require_once('inc/definitions/def_file_icons.php');

// *** Generic file settings
$file_dir				=	'..'.$_sep.'user_files'.$_sep.'file';
$file_dir_clean			=	'user_files'.$_sep.'file';
$file_allowed_ext		=	'';

// *** Flash file settings
$flash_dir				=	'..'.$_sep.'user_files'.$_sep.'flash';
$flash_dir_clean		=	'user_files'.$_sep.'flash';
$flash_allowed_ext		=	array('fla', 'swf', 'flv');

// *** Image settings
$image_dir				=	'..'.$_sep   .'user_files'.$_sep.'image';
$image_dir_clean		=	'user_files'.$_sep.'image';
$image_allowed_ext 		=	array('jpg','jpeg','gif','png');
$image_resize_size		=	$cfg['DEFAULT_IMAGE_RESIZE_SIZE'] ? $cfg['DEFAULT_IMAGE_RESIZE_SIZE'] : 200;
$max_display_width		=	600;

// *** General settings
$max_upload_bytes		=	returnBytes(ini_get('upload_max_filesize'));
$max_upload_size		=	byte2kb($max_upload_bytes);
$max_script_time		=	round(ini_get('max_execution_time')/60);
$max_files_at_once		=	$cfg['MAX_FILES_AT_ONCE'];
$no_space				=	$cfg['NO_SPACE_IN_FILENAME'];
$to_lower_case			=	$cfg['FILENAMES_TO_LOWERCASE'];

// *** Specific definitions
define('FILETYPE_GENERIC',	1);
define('FILETYPE_IMAGE',	2);
define('FILETYPE_FLASH',	3);

$filetypes_array	=	array(
	FILETYPE_IMAGE		=>	TXT_IMAGES,
	FILETYPE_GENERIC	=>	TXT_DOCUMENTS,
	FILETYPE_FLASH		=>	TXT_FLASH_ANIMATIONS
);

// *** Current selected filetype
if (!$filetype = request('filetype')) {
	$filetype	=	FILETYPE_IMAGE;
}

// *** Config depends on currently selected filetype
switch($filetype) {

	case FILETYPE_GENERIC:
		$object				=	$doc_object;
		$object_lc 			= 	$doc_object_lc;
		$plural				=	$doc_plural;
		$plural_lc			=	$doc_plural_lc;
		$txt_upload_new		=	$doc_upload_new;
		$allowed_extensions	=	$file_allowed_ext;
		$directory			=	$file_dir;
		$directory_clean	=	$file_dir_clean;
		$img_add			=	$img['folder_full_add'];
		$img_del			=	$img['folder_full_del'];
		$img_get			=	$img['folder_full_down'];

		$msg_none_uploaded		=	$doc_msg_none_uploaded;
		$msg_deleted			=	$doc_msg_deleted;
		$msg_deleted_m			=	$doc_msg_deleted_m;
		$msg_delete_confirm		=	$doc_msg_delete_confirm;
		$msg_delete_confirm_m	=	$doc_msg_delete_confirm_m;
		$msg_del_error			=	$doc_msg_del_error;
		$msg_doesnt_exist		=	$doc_msg_doesnt_exist;
		break;

	case FILETYPE_IMAGE:
		$object				=	$img_object;
		$object_lc 			= 	$img_object_lc;
		$plural				=	$img_plural;
		$plural_lc			=	$img_plural_lc;
		$txt_upload_new		=	$img_upload_new;
		$allowed_extensions	=	$image_allowed_ext;
		$directory			=	$image_dir;
		$directory_clean	=	$image_dir_clean;
		$img_add			=	$img['image_add'];
		$img_del			=	$img['image_del'];
		$img_get			=	$img['image_down'];

		$msg_none_uploaded		=	$img_msg_none_uploaded;
		$msg_deleted			=	$img_msg_deleted;
		$msg_deleted_m			=	$img_msg_deleted_m;
		$msg_delete_confirm		=	$img_msg_delete_confirm;
		$msg_delete_confirm_m	=	$img_msg_delete_confirm_m;
		$msg_del_error			=	$img_msg_del_error;
		$msg_doesnt_exist		=	$img_msg_doesnt_exist;
		break;

	case FILETYPE_FLASH:
		$object				=	$fla_object;
		$object_lc 			= 	$fla_object_lc;
		$plural				=	$fla_plural;
		$plural_lc			=	$fla_plural_lc;
		$txt_upload_new		=	$fla_upload_new;
		$allowed_extensions	=	$flash_allowed_ext;
		$directory			=	$flash_dir;
		$directory_clean	=	$flash_dir_clean;
		$img_add			=	$img['film_add'];
		$img_del			=	$img['film_del'];
		$img_get			=	$img['film_down'];

		$msg_none_uploaded		=	$fla_msg_none_uploaded;
		$msg_deleted			=	$fla_msg_deleted;
		$msg_deleted_m			=	$fla_msg_deleted_m;
		$msg_delete_confirm		=	$fla_msg_delete_confirm;
		$msg_delete_confirm_m	=	$fla_msg_delete_confirm_m;
		$msg_del_error			=	$fla_msg_del_error;
		$msg_doesnt_exist		=	$fla_msg_doesnt_exist;
		break;
}

// *** Sorting and pages functionality
$default_sort_option	=	'name';
$default_sort_method	=	'ASC';
$secondary_sort_option	=	'';
$secondary_sort_method	=	'';
$valid_sort_options		=	array('id','timestamp','name','filename','extension');

// *** Where clause
$where[]				=	"filetype = '$filetype'";

// *** Search constraints
if (!empty($search_query)) {
	$search				=	array();
	$search[]			=	"name LIKE '%$search_query%'";
	$search[]			=	"filename LIKE '%$search_query%'";
	$search[]			=	"extension LIKE '%$search_query%'";
	$where[]			=	'('.implode(' OR ', $search).')';
}

// *** Extra appendix
$link_appendix .= '&filetype='.$filetype;

// *** Sorting and pages and link appendix
require_once(CMS_PATH_PREFIX.'inc/sorting_and_pages.php');
$_app					=	$page_appendix.'&'.$sort_appendix.'&'.$link_appendix;

// *** Extra definitions
$resize_constraints		=	array(	0					=> 	CONSTRAINT_NAME_NONE,
									CONSTRAINT_SIZE 	=> 	CONSTRAINT_NAME_SIZE,
									CONSTRAINT_WIDTH 	=> 	CONSTRAINT_NAME_WIDTH,
									CONSTRAINT_HEIGHT 	=> 	CONSTRAINT_NAME_HEIGHT,
									CONSTRAINT_CROP		=> 	CONSTRAINT_NAME_CROP);

/******************************************************************************************
 * Photo folder check
 ***/

$dir_exists = checkFolder($directory);

/******************************************************************************************
 * POST and GET Cases
 ***/

if (isset($_POST['add_finish'])) {

	//Upload image and resize
	if ($dir_exists) {

		//Set counter and errors array
		$files_added 	= 	0;
		$errors			=	array();

		//Force images
		$force_image = $filetype == FILETYPE_IMAGE ? true : false;

		//Overwrite
		$overwrite = isset($_POST['overwrite']) ? true : false;

 		//Did we specify a subdirectory?
 		if ($_POST['destination_folder']) {
			$to_directory 		= 	$directory.$_sep.$_POST['destination_folder'];
			$filename_prefix	=	$_POST['destination_folder'].$_sep;
			$uploaded_to		=	$plural_lc.$_sep.$_POST['destination_folder'];
		}
		else {
			$to_directory 		= 	$directory;
			$filename_prefix	=	'';
			$uploaded_to		=	$plural_lc;
		}

		//Loop files
		for ($f=1; $f<=$max_files_at_once; $f++) {

			if (!empty($_FILES['user_file_'.$f]['name'])) {

		 		evalStripSlashes($_FILES['user_file_'.$f]['name']);

		 		//Move file to dir
				if (($result = uploadFile('user_file_'.$f, $to_directory, '', $chmod, $force_image, $overwrite, $allowed_extensions,
										  $no_space, $to_lower_case)) !== false) {

					list($file_name, $file_extension) = $result;

					cmsLog($object." '$file_name' geupload naar $to_directory.");

					//Resize images if desired
					if ($filetype == FILETYPE_IMAGE and $resize_constraint = (int) $_POST['resize_constraint']) {

						//Set the urls
						$source_url = 	$to_directory.$_sep.$file_name;
						$target_url	=	$to_directory.$_sep.change_file_extension($file_name, 'jpg');

						//Resize and resample as JPG
						$ImageHandler	=	new ImageHandler($source_url, $target_url);

						//Valid crop constraints set?
						if ($resize_constraint == CONSTRAINT_CROP
							and isset($_POST['crop_width']) and isset($_POST['crop_height'])
							and ($width = (int) $_POST['crop_width']) and ($height = (int) $_POST['crop_height'])) {

							$ImageHandler->setTargetSize($_POST['crop_width'], $_POST['crop_height'], true);
						}

						//Valid size constraint set?
						elseif (isset($_POST['resize_size']) and $size = (int) $_POST['resize_size']) {
							$ImageHandler->setTargetSizeConstraint($size, $resize_constraint);
						}

						if ($ImageHandler->output()) {

							//Remove the original file if it was not a jpg
							if ($file_extension != '.jpg') {
								unlink($source_url);
								$file_extension = 	'.jpg';
								$file_name		=	change_file_extension($file_name, 'jpg');
							}
						}
						else {
							$errors[] = $msg_resize_error.$file_name.'.<br/><br/>'.$_error;
						}
					}

					//Insert into database or update if file existed
					if ($db_id = isDoublePost($db_table, array('filename','filetype'), array($filename_prefix.$file_name, $filetype))) {
						upRec($db_table, $db_id,	array('name'),
													array($filename_prefix.$file_name)
						);

					}
					else {
						insRec($db_table,	array('name', 'filename', 'extension', 'filetype'),
											array($file_name, $filename_prefix.$file_name, substr($file_extension,1), $filetype)
						);
					}

					//Update counter
					$files_added++;
				}
				else {
					$errors[] = $msg_upload_error.$_FILES['user_file_'.$f]['name'].'.<br/><br/>'.$_error;
				}
			}
		}

		//Status
		if ($files_added) {
			$Message->set($files_added.' '.($files_added == 1 ? $object_lc : $plural).' '.TXT_UPLOADED_TO.' <b>'.$uploaded_to.'</b>.');
			cmsLog($files_added.' '.($files_added == 1 ? $object_lc : $plural_lc).' '.TXT_UPLOADED_TO.' '.$uploaded_to);
		}
		else {
			$Message->set($msg_none_uploaded, WARNING);
		}

		//Errors?
		if (count($errors)) {
			foreach($errors as $error) {
				$Message->add($error, WARNING);
			}
		}

		//Add more?
		if (isset($_POST['more_files'])) {
			$_GET['add'] = 1;
		}
	}
	else {
		$Message->set($msg_directory_error, CRITICAL);
	}
}

// *** Delete confirmation
if (isset($_GET['delete']) and $id_holder = $_GET['delete']) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			$Message->setConfirmMultiple($msg_delete_confirm_m, 'delete',$db_ids,'filename');
		}
	}
	else if ($db_id = (int) $id_holder) {
		$Message->setConfirm($msg_delete_confirm,'delete',$db_id,'filename');
	}
}

// *** Delete action
if (isset($_GET['delete_confirm']) and $id_holder = $_GET['delete_confirm'] and noBrowse()) {

	if (isset($_GET['multiple_confirm']) and is_array($id_holder)) {

		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			foreach ($db_ids as $db_id) {
				$filename = getMyo($db_table, $db_id, 'filename');
				if (file_exists($directory.$_sep.$filename)) {
					unlink($directory.$_sep.$filename);
				}
				delRec($db_table, $db_id);
				cmsLog("$object $filename ".LOG_DELETED);
			}
			$Message->set($msg_deleted_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		$filename = getMyo($db_table, $db_id, 'filename');
		if (file_exists($directory.$_sep.$filename)) {
			unlink($directory.$_sep.$filename);
		}
		delRec($db_table, $db_id);
		cmsLog("$object $filename ".LOG_DELETED);
		$Message->set($msg_deleted);
	}
}

// *** Delete confirmation
if (isset($_GET['delete_folder']) and $folder = $_GET['delete_folder']) {

	if (is_dir($directory.$_sep.$folder)) {
		$files	= 	readDirectory($directory.$_sep.$folder, false, false);
		if (count($files)) {
			$Message->setConfirm(	$msg_delete_ff_confirm,'delete_folder',$folder,'',	$plural_lc.'/'.$folder,
									$link_appendix."&manage_folders=1");
		}
		else {
			$Message->setConfirm(	$msg_delete_f_confirm,'delete_folder',$folder,'',	$plural_lc.'/'.$folder,
									$link_appendix."&manage_folders=1");
		}
	}
}

// *** Delete action
if (isset($_GET['delete_folder_confirm']) and $folder = $_GET['delete_folder_confirm'] and noBrowse()) {

	if (is_dir($directory.$_sep.$folder)) {

		//Delete the files
		$files	= 	readDirectory($directory.$_sep.$folder, false, false);
		if (count($files)) {
			foreach ($files as $file) {
				delRec($db_table, "filename='".$folder.$_sep.$file."'");
				if (file_exists($directory.$_sep.$folder.$_sep.$file)) {
					unlink($directory.$_sep.$folder.$_sep.$file);
				}
				cmsLog($object.' '.$folder.$_sep.$file.' '.LOG_DELETED);
			}
		}

		//Delete the directory
		if (@rmdir($directory.$_sep.$folder)) {
			$Message->set($msg_folder_deleted);
			cmsLog(TXT_FOLDER.' '.$directory.$_sep.$folder.' '.LOG_DELETED);
		}
		else {
			$Message->set($msg_folder_not_deleted, CRITICAL);
		}
	}
}

// *** Make new folder
if (isset($_POST['make_new_folder']) and $folder = $_POST['folder_name']) {

	evalAll($folder, false);
	$folder = filenameSafe($folder);

	if (!empty($_POST['within_folder'])) {
		$folder = filenameSafe($_POST['within_folder']).$_sep.$folder;
	}

	if ($folder) {
		if (!is_dir($directory.$_sep.$folder)) {
			if ($folder and @mkdir($directory.$_sep.$folder)) {
				if ($chmod) {
					@chmod($directory.$_sep.$folder, $chmod);
				}
				$Message->set($msg_folder_created);
				cmsLog(TXT_FOLDER.' '.$directory.$_sep.$folder.' '.LOG_CREATED);
			}
			else {
				$Message->set($msg_folder_not_created, CRITICAL);
				$_GET['add_folder'] = 1;
				unset($_POST['manage_folders']);
			}
		}
		else {
			$Message->set($msg_folder_exists, WARNING);
			$_GET['add_folder'] = 1;
			unset($_POST['manage_folders']);
		}
	}
	else {
		$Message->set($msg_invalid_folder_name, WARNING);
		$_GET['add_folder'] = 1;
		unset($_POST['manage_folders']);
	}
}

/******************************************************************************************
 * Add / Edit / View options
 ***/

if (isset($_GET['add'])) {

	$folders 			= 	readDirectory($directory, true, true, true);
	$folders_to_pick	=	array('' => $plural_lc);
	foreach ($folders as $folder) {
		if (!is_dir($directory.$_sep.$folder)) {
			unset($folders[$folder]);
		}
		else {
			$folders_to_pick[$folder]	=	$plural_lc.$_sep.$folder;
		}
	}

	$pre_html = "
		<h3>".TXT_UPLOAD_FILES." ($plural_lc):</h3><hr/>
		<h6>".TXT_MAX." $max_upload_size / $max_script_time ".TXT_MINUTES."</h6><br/><br/>
		<script type='text/javascript'>
			function evalConstraint(constraint) {
				if (constraint == 0) {
					setVisibility('resize_size', false);
					setVisibility('crop_width',  false);
					setVisibility('crop_height', false);
				}
				else if (constraint == ".CONSTRAINT_CROP.") {
					setVisibility('resize_size', false);
					setVisibility('crop_width',  true);
					setVisibility('crop_height', true);
				}
				else {
					setVisibility('resize_size', true);
					setVisibility('crop_width',  false);
					setVisibility('crop_height', false);
				}
			}
		</script>

		<form name='upload_files' method='post' action='$_file' enctype='multipart/form-data'>
		<input type='hidden' name='MAX_FILE_SIZE' value='$max_upload_bytes' />
		<input type='hidden' name='filetype' value='$filetype' />

		<table border='0px' cellspacing='0px' cellpadding='1px'>
	";

	for ($f=1; $f <= $max_files_at_once; $f++) {
		$pre_html .= "
			<tr>
				<td>$object $f:</td>
				<td><input type='file' name='user_file_$f' class='input_file' /></td>
			</tr>
		";
	}

	$pre_html .= "
		<tr>
			<td width='150px'><br/>".TXT_TO_FOLDER.":</td>
			<td><br/>".makeSelectBox('destination_folder', $folders_to_pick, 0)."</td>
		</tr>
	";

	if ($filetype == FILETYPE_IMAGE) {
		$pre_html .= "
			<tr>
				<td colspan='2'><br/><h3>".TXT_RESIZE_IMAGES."</h3></td>
			</tr>
			<tr>
				<td>".TXT_RESIZE_TO.":</td>
				<td>".makeSelectBox('resize_constraint', $resize_constraints, request('resize_constraint'), '',
					"onchange='evalConstraint(this.value);'")."</td>
			</tr>
			<tr id='resize_size' style='display: none;'>
				<td>".TXT_MAX_SIZE.":</td>
				<td><input type='text' name='resize_size' class='input_small' $js[int] value='$image_resize_size'/> ".TXT_PIXELS."</td>
			</tr>
			<tr id='crop_width' style='display: none;'>
				<td>".TXT_WIDTH.":</td>
				<td><input type='text' name='crop_width' class='input_small' $js[int] /> ".TXT_PIXELS."</td>
			</tr>
			<tr id='crop_height' style='display: none;'>
				<td>".TXT_HEIGHT.":</td>
				<td><input type='text' name='crop_height' class='input_small' $js[int] /> ".TXT_PIXELS."</td>
			</tr>
		";
	}

	$pre_html .= "
		<tr>
			<td colspan='2'><br/><input type='checkbox' name='overwrite' /> ".str_replace('[ITEMS]',$plural_lc,TXT_OVERWRITE)."
			<br/><input type='checkbox' name='more_files' /> ".str_replace('[ITEMS]',$plural_lc,TXT_UPLOAD_MORE)."</td>
		</tr>
		<tr>
			<td colspan='2'><br/><input type='submit' class='input_submit' name='add_finish' value='".BUTTON_UPLOAD."' /></td>
		</tr>
		</table>
		</form><br/>
	";
}

// *** View (for images only)
if (isset($_GET['view'])) {

	//Get the basename and extension
	$file_name 		= 	basename($_GET['view']);
	$image_url 		= 	$directory.$_sep.$file_name;
	$file_extension = 	getFileExtension($file_name);

	//Show if this is really an image and if it exists
	if (in_array($file_extension, $image_allowed_ext)) {
		if (file_exists($image_url)) {

			//Get the dimensions
			list($width, $height) = @getimagesize($image_url);

			//If too wide, make a thumbnail
			if ($width > $max_display_width) {
				$image_url = CMS_PATH_PREFIX.'inc/makethumb.php?file='.$directory_clean.$_sep.$file_name.'&maxwidth='.$max_display_width;
			}

			//Show html
			$pre_html = "
				<h3>$file_name</h3><hr/>
				<h6>".TXT_ORIGINAL_SIZE.": $width x $height ".TXT_PIXELS."</h6>
				<br/><br/><img src='$image_url' />
				<br/><br/>
			";
		}
		else {
			$Message->set($msg_doesnt_exist, WARNING);
		}
	}
}

// *** Manage folders
if (isset($_GET['manage_folders']) or isset($_POST['manage_folders'])) {

	$folders 		= 	readDirectory($directory, true, true, true);
	$folder_rows	=	array();
	foreach ($folders as $folder) {
		if (!is_dir($directory.$_sep.$folder)) {
			unset($folders[$folder]);
		}
		else {
			$folder_rows[] = "
				<tr>
					<td>$plural_lc/$folder</td>
					<td><a href='$_file?manage_folders&filetype=$filetype&delete_folder=$folder'
						title='".TXT_DELETE_FOLDER."'>$img[folder_del]</a></td>
				</tr>
			";
		}
	}

	if (!count($folder_rows)) {
		$folder_rows[] = "
			<tr>
				<td colspan='2'>".TXT_NO_FOLDERS_YET."</td>
			</tr>
		";
	}

	$pre_html = "
		<h3>".ucfirst(TXT_MANAGE_FOLDERS)." ($plural_lc):</h3><hr/>
		<table border='0px' cellspacing='0px' cellpadding='1px'>
		<tr>
			<td width='300px'>&nbsp;</td>
			<td width='40px'>&nbsp;</td>
		</tr>
		".implode("\n",$folder_rows)."
		</table><br/><br/>
	";
}

// *** Add folder
if (isset($_GET['add_folder'])) {

	$folders 		= 	readDirectory($directory, true, true, true);
	foreach ($folders as $folder) {
		if (!is_dir($directory.$_sep.$folder)) {
			unset($folders[$folder]);
		}
		else {
			$folders[$folder] = $plural_lc.$_sep.$folder;
		}
	}
	$folders		=	array('' => $plural_lc) + $folders;

	$pre_html = "
		<h3>".ucfirst(TXT_NEW_FOLDER)." ($plural_lc):</h3><hr/><br/>
		<form name='new_folder_form' method='post' action='$_file'>
		<input type='hidden' name='manage_folders' value='1' />
		<input type='hidden' name='filetype' value='$filetype' />
		<table border='0px' cellspacing='0px' cellpadding='1px'>
		<tr>
			<td width='150px'>".TXT_FOLDER.":</td>
			<td><input type='text' name='folder_name' class='input_regular' /></td>
		</tr>
		<tr>
			<td>Binnen map:</td>
			<td>".makeSelectBox('within_folder', $folders)."</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><br/><input type='submit' class='input_submit' name='make_new_folder' value='".BUTTON_CREATE."' /></td>
		</tr>
		</table>
		</form><br/>
	";
}


/******************************************************************************************
 * Page contents
 ***/

//Filetype selector
$type_selectors = array();
foreach ($filetypes_array as $type_id => $type_name) {
	$type_selectors[] = ($filetype == $type_id) ? 	"<a href='$_file?filetype=$type_id' style='color: black;'>$type_name</a>" :
													"<a href='$_file?filetype=$type_id'>$type_name</a>";
}
$type_selectors = implode(' - ', $type_selectors);

//Page title HTML
$html = 	"
	<a href='$_file'><img src='$site_icon' border='0px' class='cms_icon'/></a>
	<h6>
		$type_selectors &nbsp; | &nbsp;
		$img_add <a href='$_file?add&filetype=$filetype'>$txt_upload_new</a> &nbsp;
		$img[folder] <a href='$_file?manage_folders&filetype=$filetype'>".TXT_MANAGE_FOLDERS."</a> &nbsp;
		$img[folder_add] <a href='$_file?add_folder&filetype=$filetype'>".TXT_NEW_FOLDER."</a>
	</h6>
	<br/>".$Message->get()."
	<br/><br/>$pre_html
";

//Get items for this page and this sorting method
$res = eq("	SELECT 		id, name, filename, UNIX_TIMESTAMP(timestamp) AS timestamp, extension
			FROM 		$db_table $where_clause
			ORDER BY 	$order_clause
			LIMIT 		$limit_clause
		;");
while ($myo = mfo($res)) {

	$handle_as_img	=	($filetype == FILETYPE_IMAGE or in_array($myo->extension, $image_allowed_ext)) ? true : false;
	$download_link	=	CMS_PATH_PREFIX.'inc/download.php?file='.$directory_clean.$_sep.$myo->filename.'&filename='.$myo->filename;
	$extension 		= 	isset($f_icon[$myo->extension]) ? $f_icon[$myo->extension] : $f_icon[0];
	$type_full		=	isset($f_type[$myo->extension]) ? $f_type[$myo->extension] : strtoupper($myo->extension).$f_type[0];
	$view_target 	=	$handle_as_img ? 	'' : '_blank';
	$view_link		=	$handle_as_img ? 	"$_file?filetype=$filetype&view=$myo->filename" : $download_link;
	$view_title		=	$handle_as_img ?	TXT_VIEW_IMAGE : TXT_DOWNLOAD_FILE;

	//Folder determination
	$parts			=	explode($_sep, $myo->filename);
	if (count($parts) > 1) {
		array_pop($parts);
		$folder		=	implode($_sep, $parts);
	}
	else {
		$folder 	=	'';
	}

	$data_rows[] = array(

		//This MUST be the id
		$myo->id,

		//All desired other fields
		$extension."&nbsp; <a href='$view_link' target='$view_target' title='$view_title'>$myo->name</a>",
		$folder,
		$type_full,
		date('d/m/Y H:i', $myo->timestamp),

		//The options last
		"<a href='$download_link' target='_blank' title='".TXT_DOWNLOAD_FILE."'>$img_get</a>
		 <a href='$_file?delete=$myo->id&$_app' title='".TXT_DELETE_FILE."'>$img_del</a>
		"
	);
}

//Output data table
$DataTable = new DataTable();
$DataTable->setMarkAllOptions(	array(TXT_DELETE, 		'delete', 		TXT_DELETE_FILES));
$DataTable->showSearch(array('filetype' => $filetype));
$DataTable->setDataRows($data_rows);
$DataTable->setLegendRow(
	array(TXT_FILE, TXT_FOLDER, TXT_TYPE, TXT_DATE, TXT_OPTIONS),
	array(0,180,120,120,40),
	array('name','filename','extension','timestamp')
);
echo $html.$DataTable->getHTML();

require_once('inc/cms_footer.php');
?>