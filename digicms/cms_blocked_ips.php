<?php
/*
Title:		Manage blocked IPs
File: 		cms_blocked_ips.php
Version: 	v2.04
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

/******************************************************************************************
 * Settings
 ***/

// *** Required files
require_once('inc/cms_header.php');
require_once('inc/lang/'.$cms_lang.'_'.$_file);
require_once(CMS_PATH_PREFIX.'classes/class_DataTable.php');

// *** Sorting and pages functionality
$default_sort_option	=	'id';
$default_sort_method	=	'DESC';
$secondary_sort_option	=	'';
$secondary_sort_method	=	'';
$valid_sort_options		=	array('id','ip','block_count');

// *** Search constraints
if (!empty($search_query)) {
	$search				=	array();
	$search[]			=	"ip LIKE '%$search_query%'";
	$where[]			=	'('.implode(' OR ', $search).')';
}

// *** Sorting and pages and link appendix
require_once(CMS_PATH_PREFIX.'inc/sorting_and_pages.php');
$_app					=	$page_appendix.'&'.$sort_appendix.'&'.$link_appendix;

/******************************************************************************************
 * POST and GET Cases
 ***/

// ** Add
if (request('add')) {
	
	$ip_address = request('ip');
	evalAll($ip_address);
	
	if (empty($ip_address)) {
		$Message->set($msg_no_ip, CRITICAL);
	}	
	elseif (!($ip_address = validateIPwildcard($ip_address))) {
		$Message->set($msg_illegal_ip_format, WARNING);
		$_GET['add_manually'] = 1;
	}
	else if (countResults($db_table, "ip = '$ip_address'")) {
		$Message->set($msg_ip_exists);
		$_GET['add_manually'] = 1;
	}
	else {
		$db_id 	= insRec($db_table,'ip',$ip_address);					
		cmsLog(LOG_IP_BLOCKED." ($ip_address, #$db_id)");
		$Message->set($msg_added);
	}
}

// *** Edit
if (isset($_POST['edit_finish']) and $db_id = (int) $_POST['id']) {
	
	evalAll($_POST['ip']);
	
	if (empty($_POST['ip'])) {
		$Message->set($msg_no_ip, CRITICAL);
		$_GET['edit'] = $db_id;
	}	
	elseif (!($ip_address = validateIPwildcard($_POST['ip']))) {
		$Message->set($msg_illegal_ip_format, WARNING);
		$_GET['edit'] = $db_id;
	}
	else if (countResults($db_table, "ip = '$ip_address' AND id <> '$db_id'")) {
		$Message->set($msg_ip_exists);
		$_GET['edit'] = $db_id;
	}
	else {
		upRec($db_table,$db_id,'ip',$ip_address);	
		cmsLog("$object $ip_address (#$db_id) ".LOG_EDITTED);
		$Message->set($msg_editted);
	}
}

// *** Delete confirmation
if (isset($_GET['delete']) and $id_holder = $_GET['delete']) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			$Message->setConfirmMultiple($msg_delete_confirm_m, 'delete',$db_ids,'ip');
		}
	}
	else if ($db_id = (int) $id_holder) {
		$Message->setConfirm($msg_delete_confirm,'delete',$db_id,'ip');
	}	
}

// *** Delete action
if (isset($_GET['delete_confirm']) and $id_holder = $_GET['delete_confirm'] and noBrowse()) {

	if (isset($_GET['multiple_confirm']) and is_array($id_holder)) {

		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			delRec($db_table, $db_ids);
			cmsLog(LOG_MULTIPLE.' '.$plural_lc.' '.LOG_DELETED);
			$Message->set($msg_deleted_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		delRec($db_table, $db_id);
		cmsLog("$object (#$db_id) ".LOG_DELETED);
		$Message->set($msg_deleted);	
	}	
}

/******************************************************************************************
 * Add / edit / view options
 ***/

if (isset($_GET['add_manually'])) {
	
	$ip_address = isset($_POST['ip']) ? $_POST['ip'] : '';
	
	$pre_html = "
	
		<h3>".TXT_ADD_ITEM.":</h3><hr/><br/>
		<form name='block_ip_form' method='post' action='$_file'>
		<table border='0px' cellspacing='0px' cellpadding='1px'>
			<tr>
				<td width='150px'>$object:</td>
				<td><input type='text' name='ip' value='$ip_address' class='input_regular'/></td>
			</tr>
			<tr>
				<td>&nbsp;</td><td><br/><input type='submit' name='add' value='".BUTTON_ADD."' class='input_submit'/></td>
			</tr>			
		</table>
		</form><br/>	
	";
}

if (isset($_GET['edit']) and $db_id = (int) $_GET['edit']) {
	
	$pre_html = "
			
		<h3>".TXT_EDIT_ITEM.":</h3><hr/><br/>
		<form name='ip_edit_form' method='post' action='$_file'>
		<input type='hidden' name='id' value='$db_id' />
		<table border='0px' cellspacing='0px' cellpadding='1px'>
			<tr>
				<td width='150px'>$object:</td>
				<td><input type='text' name='ip' value='".getMyo($db_table,$db_id,'ip')."' class='input_regular'/></td>
			</tr>
			<tr>
				<td>&nbsp;</td><td><br/><input type='submit' name='edit_finish' value='".BUTTON_SAVE."' class='input_submit'/></td>
			</tr>			
		</table>
		</form><br/>	
	";
}

/******************************************************************************************
 * Page contents
 ***/

//Page title HTML
$html = 	"
	<a href='$_file'><img src='$site_icon' border='0px' class='cms_icon'/></a>
	<h6>
		<a href='$_file'>".TXT_OVERVIEW."</a> &nbsp;
		$img[window_add] <a href='$_file?add_manually'>".TXT_NEW_ITEM."</a>
	</h6>
	<br/>".$Message->get()."
	<br/><br/>$pre_html
";

//Get items for this page and this sorting method
$res = eq("	SELECT 		id, ip, block_count
			FROM 		$db_table $where_clause
			ORDER BY 	$order_clause
			LIMIT 		$limit_clause			
;");
$data_rows = array();

while ($myo = mfo($res)) {

	$data_rows[] = array(
	
		//This MUST be the id
		$myo->id,
		
		//All desired other fields
		$myo->id,
		$myo->ip,
		$myo->block_count.'x',
		
		//The options last
		"<a href='$_file?edit=$myo->id&$_app' title='".TXT_EDIT_ITEM."'>$img[edit]</a>
		 <a href='$_file?delete=$myo->id&$_app' title='".TXT_DELETE_ITEM."'>$img[window_del]</a>"
	);
}

//Output data table
$DataTable = new DataTable();
$DataTable->setMarkAllOptions(	array('verwijder','delete',$plural.' verwijderen'));
$DataTable->showSearch();
$DataTable->setDataRows($data_rows);
$DataTable->setLegendRow(
	array('#',$object, TXT_BLOCKED, TXT_OPTIONS),
	array(60,0,100,40),
	array('id','ip','block_count')
);
echo $html.$DataTable->getHTML();

require_once('inc/cms_footer.php');
?>