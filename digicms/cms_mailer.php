<?
/*
Title:		Standard mailer (++FCKeditor)
File: 		cms_mailer.php
Version: 	v2.04
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization

Takes:		$_GET['email']  	(optionally)
			$_GET['template']  	(optionally)
*/

/******************************************************************************************
 * Settings
 ***/

// *** Required files
require_once('inc/cms_header.php');
require_once('inc/lang/'.$cms_lang.'_'.$_file);
require_once(CMS_PATH_PREFIX.'inc/sendmail.php');
require_once(CMS_PATH_PREFIX.'apps/fckeditor/fckeditor.php');
require_once(CMS_PATH_PREFIX.'classes/class_DataTable.php');
require_once(CMS_PATH_PREFIX.'inc/functions/explode_recipients.php');

// *** Database tables
$db_table				=	DIGI_DB_PREFIX.'mails2save';
$db_table_m2g			=	DIGI_DB_PREFIX.'mails2go';

// *** Sorting and pages functionality
$default_sort_option	=	'timestamp';
$default_sort_method	=	'DESC';
$secondary_sort_option	=	'';
$secondary_sort_method	=	'';
$valid_sort_options		=	array('id','timestamp','timestamp_sent','subject');

// *** Where clause
if (request('show_sent')) {
	$where[] 			=	"timestamp_sent > '0000-00-00 00:00:00'";
	$object_lc			=	$sent_object_lc;
	$plural_lc			=	$sent_plural_lc;
}
else {
	$where[] 			=	"timestamp_sent = '0000-00-00 00:00:00'";
	$object_lc			=	$draft_object_lc;
	$plural_lc			=	$draft_plural_lc;
}

// *** Search constraints
if (!empty($search_query)) {
	$search				=	array();
	$search[]			=	"subject LIKE '%$search_query%'";
	$search[]			=	"body LIKE '%$search_query%'";
	$where[]			=	'('.implode(' OR ', $search).')';
}

// *** Messages
$msg_sending_limits		=	str_replace('[MAX]',	$cfg['MAX_PER_BATCH'],				$msg_sending_limits);
$msg_sending_limits		=	str_replace('[MINS]',	round($cfg['BATCH_INTERVAL']/60),	$msg_sending_limits);

// *** Extra appendix
if (request('show_sent'))		$link_appendix .= '&show_sent=1';
if (request('show_saved'))		$link_appendix .= '&show_saved=1';

// *** Sorting and pages and link appendix
require_once(CMS_PATH_PREFIX.'inc/sorting_and_pages.php');
$_app					=	$page_appendix.'&'.$sort_appendix.'&'.$link_appendix;

// *** Set vars
$set_from				=	isset($_POST['from']) ? $_POST['from'] : '';
$set_email 				=	isset($_GET['email']) ? $_GET['email'] : '';
$saved_id				=	request('saved_id');
$template_id			=	request('template_id');
$message				=	'';
$subject				=	'';
$attachment_html		=	'';

// *** General settings and variable initialization
$no_subject				=	TXT_NO_SUBJECT;
$attachment_folder		=	'..'.$_sep.'user_files'.$_sep.'attachments';
$set_bcc				=	$cfg['DEFAULT_BCC'];
$use_template			=	false;
$from_emails 			= 	array();
$refuse_to_send			=	false;

// *** Allowed attachment extensions
$allowed_extensions		=	array(	'txt','rtf','doc','docx','xls','xlsx','ppt','pptx','pdf',
									'svw','sxc','sxd','sxm','sxi',
									'bmp','png','gif','jpg','jpeg','psd','tiff','svg','swf','fla','ico',
									'htm','html','css','xml','log',
									'zip','rar','arj','tar','gz','ace','bz2','lha'
							);

// *** Customer specific settings (template handling)
require_once('inc/config/cfg_mailer.php');

// *** Check mailer email addresses
if (!empty($cfg['MAILER_EMAIL_ADDRESSES'])) {
	$emails = explode(';', $cfg['MAILER_EMAIL_ADDRESSES']);
	foreach ($emails as $email) {
		evalTrim($email);
		if ($email = validateEmail($email)) {
			$from_emails[$email] = $email;
		}
	}
}

// *** If none found, we can't use the mailer
if (!count($from_emails)) {
	$Message->set($msg_no_mail_addresses, WARNING);
	$refuse_to_send	= true;
}

/******************************************************************************************
 * Clean the attachments folder
 ***/

if (checkFolder($attachment_folder)) {

	//Make files array
	$files 	= readDirectory($attachment_folder, false);

	//Prepend attachment folder
	foreach ($files as $key => $file) {
		$files[$key] = $attachment_folder.$_sep.$file;
	}

	//Don't delete if the attachment is in the queue to send
	$res = eq("SELECT attachment_urls FROM $db_table_m2g WHERE attachment_urls <> '' GROUP BY attachment_urls;");
	while ($myo = mfo($res)) {

		$urls = explode(';',$myo->attachment_urls);
		foreach ($urls as $url) {
			if (($key = array_search($url, $files)) !== false) {
				unset($files[$key]);
			}
		}
	}

	//Don't delete if the attachment is in the saved emails table
	$res = eq("SELECT attachment_urls FROM $db_table WHERE attachment_urls <> '' GROUP BY attachment_urls;");
	while ($myo = mfo($res)) {

		$urls = explode(';',$myo->attachment_urls);
		foreach ($urls as $url) {
			if (($key = array_search($url, $files)) !== false) {
				unset($files[$key]);
			}
		}
	}

	//Delete unwanted attachments
	foreach ($files as $attachment) {
		@unlink($attachment);
	}
}

else {
	$Message->set($msg_attachment_folder, WARNING);
}

/******************************************************************************************
 * Functions
 ***/

function handleAttachments() {

	global $db_table, $_sep, $attachment_folder, $saved_id, $chmod, $allowed_extensions, $Message, $_error, $msg_upload_error;
	global $attachment_urls, $attachment_names;
	global $attachment_urls_str, $attachment_names_str;

	//Initialize vars
	$file_names				= array();
	$keepers				= array();
	$attachment_urls 		= array();
	$attachment_names 		= array();
	$attachment_urls_str	= '';
	$attachment_names_str	= '';

	//Existing attachments
	if ($saved_id) {

		//First determine which ones we want to keep
		foreach ($_POST as $key => $val) {
			if (substr($key,0,16) == 'keep_attachment_') {
				$keepers[$val] = $val;
			}
		}

		//Now get them again from the database
		$res = eq("SELECT attachment_names, attachment_urls FROM $db_table WHERE id ='$saved_id';");
		$myo = mfo($res);

		$existing_names = explode(';', $myo->attachment_names);
		$existing_urls 	= explode(';', $myo->attachment_urls);

		//Go over the existing ones
		foreach ($existing_names as $key => $a_name) {

			$a_url = $existing_urls[$key];

			//If we want to keep them, add to array of urls / names
			if (in_array($key, $keepers)) {
				$attachment_urls[] 	= $a_url;
				$attachment_names[] = $a_name;
			}

			//Otherwise delete the file
			else {
				@unlink($a_url);
			}
		}
	}

	//Handle attachments
	for($a = 1; $a <= $_POST['attachments']; $a++) {

		//Something wrong
		if (!isset($_FILES['attachment_'.$a]) or empty($_FILES['attachment_'.$a]['tmp_name'])) {
			continue;
		}

		//Prevent the same attachment twice
		if (array_search($_FILES['attachment_'.$a]['name'], $file_names)) {
			continue;
		}
		else $file_names[] = $_FILES['attachment_'.$a]['name'];

		if (($result = uploadFile('attachment_'.$a, $attachment_folder, '', $chmod, false, true, $allowed_extensions)) !== false) {

			list($file_name, $file_extension) = $result;

			$attachment_urls[] 	= $attachment_folder.$_sep.$file_name;
			$attachment_names[] = $file_name;
		}
		else {
			echo "";
			$Message->add($msg_upload_error.'<br/><br/>'.$_error, WARNING);
		}
	}

	//Make strings from the attachment urls and names
	$attachment_urls_str 	= implode(';',$attachment_urls);
	$attachment_names_str 	= implode(';',$attachment_names);
}


/******************************************************************************************
 * POST and GET Cases
 ***/

// *** Send and save pre-handling
if ((isset($_POST['send']) or isset($_POST['save'])) and !$refuse_to_send) {

 	//Data cleansing
 	evalAll($_POST['to'], false);
 	evalAll($_POST['subject'], false);
 	evalAll($_POST['message'], false);

	//No subject?
	if (empty($_POST['subject'])) {
		$_POST['subject'] = $no_subject;
	}

	//Handle attachments
	handleAttachments();
}

// *** Send an email
if (isset($_POST['send']) and !$refuse_to_send) {

	//Explode recipient and bcc field
	$emails = explode_recipients($_POST['to']);
	$bccs 	= explode_recipients($_POST['bcc']);

	//No recipients?
	if (!count($emails)) {
		$Message->set($msg_no_recipients, CRITICAL);
	}

	//Too many recipients for one batch?
	elseif (count($emails) > $cfg['MAX_PER_BATCH']) {

		if ($use_template) {

			if (file_exists(CMS_PATH_PREFIX.'template/email_template.html')) {

				//Get layout from template and make replacements
				$email_html = file_get_contents(CMS_PATH_PREFIX.'template/email_template.html');
				$email_html = str_replace('[SUBJECT]',	$_POST['subject'],	$email_html);
				$email_html = str_replace('[LINK]',		$_link,				$email_html);
				$message 	= str_replace('[MESSAGE]',	$_POST['message'],	$email_html);
			}
		}
		else {
			$message = $_POST['message'];
		}

		foreach ($emails as $email) {
			insRec($db_table_m2g, 	array(	'sender', 'recipient', 'subject',
											'body', 'is_html', 'attachment_names', 'attachment_urls'),
									array(	$_POST['from'], $email, $_POST['subject'],
											$message, $use_template ? 1 : 0, $attachment_names_str, $attachment_urls_str)
			);
		}

		$Message->set($msg_queued.' '.$msg_sending_limits);
	}

	//We can send this batch
	else {
		sendMail($emails, $_POST['subject'], $_POST['message'], $_POST['from'], $attachment_names, $attachment_urls, false,$use_template);
		$Message->set(str_replace('[AMOUNT]', count($emails), $msg_sent));
	}

	//If we had valid recipients, and if we have BCC's, send these.
	if (count($bccs) and count($emails)) {
		if (count($emails) > 20)	$bcc_to_list	=	count($emails).' '.TXT_ADDRESSEES.'.';
		else						$bcc_to_list	=	implode(', ', $emails).'.';
		$bcc_message = "<i>".TXT_BCC_MESSAGE." $bcc_to_list</i><br/><br/><hr/><br/>$_POST[message]";
		sendMail($bccs, TXT_BCC.': '.$_POST['subject'], $bcc_message, $_POST['from'], $attachment_names, $attachment_urls,false,$use_template);
	}

	//Mark as sent. If new email, insert into saved emails table
	if ($saved_id) {

		upRec($db_table, 	$saved_id,
							array(	'timestamp_sent', 'sender', 'recipients', 'subject',
									'body', 'attachment_names', 'attachment_urls'),
							array(	mysql_time(), $_POST['from'], $_POST['to'], $_POST['subject'],
									$_POST['message'], $attachment_names_str, $attachment_urls_str)
		);
	}
	else {
		$saved_id =
		insRec($db_table, 	array(	'timestamp_sent', 'sender', 'recipients', 'subject',
									'body', 'is_html', 'attachment_names', 'attachment_urls'),
							array(	mysql_time(), $_POST['from'], $_POST['to'], $_POST['subject'],
									$_POST['message'], 0, $attachment_names_str, $attachment_urls_str)
		);
	}

	//Fresh email
	$_POST 		= 	array();
	$saved_id	=	0;

	//Log
	cmsLog($object." (#$saved_id) ".TXT_SENT_TO.' '.count($emails).' '.TXT_ADDRESSEES);
}

// *** Send an email
if (isset($_POST['save']) and !$refuse_to_send) {

	//Save
	if ($saved_id) {

		upRec($db_table, 	$saved_id,
							array(	'sender', 'recipients', 'subject',
									'body', 'attachment_names', 'attachment_urls'),
							array(	$_POST['from'], $_POST['to'], $_POST['subject'],
									$_POST['message'], $attachment_names_str, $attachment_urls_str)
		);
	}
	else {
		$saved_id =
		insRec($db_table, 	array(	'sender', 'recipients', 'subject',
									'body', 'is_html', 'attachment_names', 'attachment_urls'),
							array(	$_POST['from'], $_POST['to'], $_POST['subject'],
									$_POST['message'], 0, $attachment_names_str, $attachment_urls_str)
		);
	}

	cmsLog($object." (#$saved_id) ".TXT_SAVED);
	$Message->set($msg_saved);
}

// *** Delete confirmation
if (isset($_GET['delete']) and $id_holder = $_GET['delete'] and checkPriv(PRIV_DELETE_EMAILS)) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			$Message->setConfirmMultiple($msg_delete_confirm_m, 'delete',$db_ids,'subject');
		}
	}
	else if ($db_id = (int) $id_holder) {
		$Message->setConfirm($msg_delete_confirm,'delete',$db_id,'subject');
	}
}

// *** Delete
if (isset($_GET['delete_confirm']) and $id_holder = $_GET['delete_confirm'] and noBrowse() and checkPriv(PRIV_DELETE_EMAILS)) {

	if (isset($_GET['multiple_confirm']) and is_array($id_holder)) {

		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			delRec($db_table, $db_ids);
			$Message->set($msg_deleted_m);
			cmsLog(LOG_MULTIPLE.' '.$plural_lc.' '.LOG_DELETED);
		}
	}
	else if ($db_id = (int) $id_holder) {
		delRec($db_table, $db_id);
		$Message->set($msg_deleted);
		cmsLog("$object (#$db_id) ".LOG_DELETED);
	}
}

/******************************************************************************************
 * Page contents
 ***/

//Page title HTML
$html = 	"
	<a href='$_file'><img src='$site_icon' border='0px' class='cms_icon'/></a>
	<h6>
		<a href='$_file'>".TXT_NEW_ITEM."</a> &nbsp;
		$img[mail_search] <a href='$_file?show_saved=1'>$draft_plural_lc</a> &nbsp;
		$img[mail_ok] <a href='$_file?show_sent=1'>$sent_plural_lc</a> &nbsp;
	</h6>
	<br/>".$Message->get()."
	<br/><br/>$pre_html
";

//Saved or sent emails list
if (isset($_GET['show_saved']) or isset($_GET['show_sent'])) {

	//Set timestamp vars
	if (isset($_GET['show_sent'])) {
		$ts_meaning	=	TXT_TS_SENT;
		$ts			=	'timestamp_sent';
	}
	else {
		$ts_meaning	=	TXT_TS_SAVED;
		$ts			=	'timestamp';
	}

	//Get items for this page and this sorting method
	$res = eq("	SELECT 		id, subject, $ts AS timestamp
				FROM 		$db_table $where_clause
				ORDER BY 	$order_clause
				LIMIT 		$limit_clause
	;");
	while ($myo = mfo($res)) {

		$data_rows[] = array(

			//This MUST be the id
			$myo->id,

			//All desired other fields
			$myo->id,
			"<a href='$_file?saved_id=$myo->id' title='".TXT_EDIT_ITEM."'>$myo->subject</a>",
			time2date($myo->timestamp),

			//The options last
			"<a href='$_file?saved_id=$myo->id' title='".TXT_EDIT_ITEM."'>$img[edit]</a>
			".(checkPriv(PRIV_DELETE_EMAILS) ? "
			 <a href='$_file?delete=$myo->id&$_app' title='".TXT_DELETE_ITEM."'>$img[mail_del]</a>
			" : '')
		);
	}

	//Output data table
	$DataTable = new DataTable();
	$DataTable->setMarkAllOptions(	array('verwijder','delete',$plural.' verwijderen'));
	$DataTable->showSearch(array('deleted' => request('deleted')));
	$DataTable->setDataRows($data_rows);
	$DataTable->setLegendRow(
		array('#', TXT_SUBJECT, $ts_meaning, TXT_OPTIONS),
		array(60,0,100,40),
		array('id','subject',$ts)
	);
	echo $html.$DataTable->getHTML();
}

//New email form
else {

	//We want to see a saved email -- override all previously set vars
	if ($saved_id) {
		$res = eq("	SELECT subject, sender, body, recipients, attachment_names, attachment_urls
					FROM $db_table WHERE id = '$saved_id';");
		$myo = mfo($res);

		$message 		= $myo->body;
		$subject		= $myo->subject;
		$set_email		= $myo->recipients;
		$set_from		= $myo->sender;

		$attachment_names	=	explode(';', $myo->attachment_names);
		$attachment_urls	=	explode(';', $myo->attachment_urls);
		$attachment_html	=	'';

		foreach ($attachment_names as $key => $a_name) {

			$attachment_html .= "
				<div id='existing_div_$key' style='clear: both;'>
					<input type='hidden' name='keep_attachment_$key' value='$key' />
					<div style='float: left; width: 200px;'>$a_name</div>
					<div style='float:left; margin-left: 10px; padding-bottom: 9px;'><h6 style='display: inline;'>
						<a href='JavaScript: void(0);' onclick='removeExisting($key);'>".TXT_DELETE."</a></h6>
					</div>
				</div>";
		}
	}

	//FCK editor configuration
	$oFCKeditor = new FCKeditor('message') ;
	$oFCKeditor->BasePath	= CMS_PATH_PREFIX.'apps/fckeditor/';
	$oFCKeditor->Value		= $message;
	$oFCKeditor->Width  	= '507px';
	$oFCKeditor->Height		= '400px';
	$oFCKeditor->ToolbarSet = 'DigiMailer';
	$oFCKeditor->changeCss('body {background:#ffffff;color:#000000;} p{color:#000000;}');

	//Base HTML
	$html .= "

	<script type='text/javascript'>

		function moreAttachments() {

			var q = String.fromCharCode(34);

			var amount_input 	= document.getElementById('attachments');
			var new_amount	 	= (document.getElementById('attachments').value - 1) + 2;
			amount_input.value 	= new_amount;

			var new_div 		= document.createElement('div');
			var new_name 		= 'attachment_div_' + new_amount;

			new_div.setAttribute('id',	new_name);
			new_div.setAttribute('name',new_name);

			new_div.innerHTML = '<div style=\"float: left; width: 200px;\"><input type=\"file\" name=\"attachment_' + new_amount + '\" /></div><div style=\"float:left; margin-left: 40px; margin-top: 5px; padding-bottom: 1px;\"><h6 style=\"display: inline;\"><a href=\"JavaScript: void(0);\" onclick=\"removeAttachment('+ new_amount +');\">".TXT_DELETE."</a></h6></div>';

			document.getElementById('attachments_box').appendChild(new_div);
			setStyle(new_name,'clear', 			'both');
		}

		function removeAttachment(attachment_no) {

			var to_remove 		= document.getElementById('attachment_div_'+attachment_no);
			document.getElementById('attachments_box').removeChild(to_remove);
		}

		function removeExisting(attachment_no) {

			var to_remove 		= document.getElementById('existing_div_'+attachment_no);
			document.getElementById('attachments_box').removeChild(to_remove);
		}

	</script>

	<form name='mailer_form' method='post' action='$_file' enctype='multipart/form-data'>
	<input type='hidden' name='saved_id' value='$saved_id' />
	<input type='hidden' name='template_id' value='$template_id' />
	<input type='hidden' name='attachments' value='1' id='attachments' />
	<table border='0px' cellspacing='0px' cellpadding='2px'>

	<tr>
		<td width='100px' valign='top'><b>".TXT_FROM.":</b></td>
		<td>
			".makeSelectBox('from',$from_emails,$set_from,506)."
		</td>
	</tr>
	<tr>
		<td valign='top'><b>".TXT_TO.":</b></td>
		<td>
			<textarea name='to' style='height: 50px; width: 500px;'>$set_email</textarea>
			<br/><h6>".TXT_SEPARATE_NOTICE."</h6>
		</td>
	</tr>
	<tr>
		<td valign='top'><br/><b>".TXT_BCC.":</b></td>
		<td><br/><input type='text' name='bcc' class='input_regular' style='width:500px;' value='$set_bcc'/></td>
	</tr>
	<tr>
		<td valign='top'><br/><b>".TXT_SUBJECT.":</b></td>
		<td><br/><input type='text' name='subject' class='input_regular' style='width:500px;' value='$subject'/></td>
	</tr>
	<tr>
		<td valign='top'><br/><b>".TXT_MESSAGE.":</b></td>
		<td><br/>".$oFCKeditor->CreateHtml()."</td>
	</tr>
	<tr>
		<td valign='top'><br/><b>".TXT_ATTACHMENTS.":</b></td>
		<td>
			<br/><div id='attachments_box'>
				$attachment_html
				<div id='attachment_div_1' style='clear: both;'>
					<div style='float: left; width: 200px;'><input type='file' name='attachment_1' /></div>
					<div style='float:left; margin-left: 40px; margin-top: 5px;'>
						<h6><a href='JavaScript: void(0);' onclick='removeAttachment(1);'>".TXT_DELETE."</a></h6>
					</div>
				</div>
			</div>
			<h6 style='clear: both; display: block;'><a href='JavaScript: void(0);' onclick='moreAttachments();'>".TXT_MORE_ATTACHMENTS."</a></h6>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><br/>
			<input class='input_submit' type='submit' name='send' value='".BUTTON_SEND."' />
			<input class='input_submit' type='submit' name='save' value='".BUTTON_SAVE."' />
		</td>
	</tr>
	</table>
	</form>
	";

	echo $html;
}

require_once('inc/cms_footer.php');
?>