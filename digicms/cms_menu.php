<?
/*
Title:		Menu management file.
File: 		cms_menu.php
Version: 	v2.20
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

/******************************************************************************************
 * Settings
 ***/

// *** Required files
require_once('inc/cms_header.php');
require_once('inc/lang/'.$cms_lang.'_'.$_file);

// *** Database tables are dependant on language
$db_table				=	DIGI_DB_PREFIX.'menu_items_'.$_lang;
$db_table_sections		= 	DIGI_DB_PREFIX.'menu_sections_'.$_lang;
$db_table_pages			= 	DIGI_DB_PREFIX.'pages_'.$_lang;

// *** Overwrite pages table?
if (isset($cfg['CUSTOM_PAGES_TABLE']) and !empty($cfg['CUSTOM_PAGES_TABLE'])) {
	$db_table_pages 	= 	DIGI_DB_PREFIX.$cfg['CUSTOM_PAGES_TABLE'].'_'.$_lang;
}

// *** Search constraints
if (!empty($search_query)) {
	$search				=	array();
	$search[]			=	"name LIKE '%$search_query%'";
	$search[]			=	"description LIKE '%$search_query%'";
	$search[]			=	"assigned_to LIKE '%$search_query%'";
	$where[]			=	'('.implode(' OR ', $search).')';
}

// *** Link appendix
$link_appendix 			.=	'&lang='.$_lang;
$_app					=	$link_appendix;

// *** Sub handling
$sub					=	request('sub');

// *** Display settings
$show_section_options	=	$cfg['MENU_SECTION_OPTIONS'];
$show_sub_options		=	$cfg['MENU_SUB_OPTIONS'];

/******************************************************************************************
 * Make arrays
 ***/

$pages_array 			= 	makeArray($db_table_pages,'id','title','','title','ASC');
$pages_array			= 	array('') + $pages_array;
$sections_array 		= 	makeArray($db_table_sections,'id','name','','section_order','ASC');

function getItemOptions($selected = '') {

	global $db_table, $sections_array;

	$items_options			=	array();
	foreach ($sections_array as $section_id => $section_name) {
		$res = eq("SELECT id, name FROM $db_table WHERE section = '$section_id' ORDER BY item_order ASC;");
		if (mysql_num_rows($res)) {
			$items_options[]	=	"<option value='' style='font-weight: bold;'>$section_name</option>";
			while ($myo = mfo($res)) {
				$sel = $selected == $myo->id ? "selected='selected'" : '';
				$items_options[]	=	"<option value='$myo->id' $sel> &nbsp; $myo->name</option>";
			}
			$items_options[]	=	"<option value=''></option>";
		}
	}
	array_pop($items_options);
	return $items_options;
}

/******************************************************************************************
 * POST and GET Cases
 ***/

if (isset($_POST['add_finish']) or isset($_POST['edit_finish']) or
	isset($_POST['add_section_finish']) or isset($_POST['edit_section_finish'])) {

 	//Clean data
	foreach(array_keys($_POST) as $key) {
		evalAll($_POST[$key]);
	}

	//Cleare page file if manually set
	if ($_POST['manual_file'])	$_POST['page_file'] = $_POST['manual_file'];

	//Clear page id if manually set
	if ($_POST['page_file']) 	$_POST['page_id'] = 0;

	//Standard log
	cmsLog(LOG_MENU_EDITTED);
}

if (isset($_POST['add_finish'])) {

	if ($sub and empty($_POST['sub_item_of'])) {
		$Message->set($msg_no_item_selected, WARNING);
	}
	else {

		if ($sub) {

			//Sub item of
			$sub_item_of 	= 	(int) $_POST['sub_item_of'];

			//For all languages or not?
			if (isset($_POST['all_languages'])) {

				//Loop all languages
				foreach ($installed_languages as $language) {

					//Set language table
					$table = DIGI_DB_PREFIX.'menu_items_'.$language;

					//Does this sub-item exist for this language?
					if (!getMyo($table, $sub_item_of, 'id')) {
						continue;
					}

					//Get item order
					$order = 1 + getMyo($table, "sub_item_of='$sub_item_of'", 'item_order', 'item_order','DESC',1);

					//Insert
					insRec($table,	array(	'name','sub_item_of','page_id','page_file','item_order'),
									array(	$_POST['name'],$sub_item_of,(int) $_POST['page_id'],$_POST['page_file'],$order));
				}
			}
			else {

				//Get item order
				$order = 1 + getMyo($db_table, "sub_item_of='$sub_item_of'", 'item_order', 'item_order','DESC',1);

				//Insert
				insRec($db_table,	array(	'name','sub_item_of','page_id','page_file','item_order'),
									array(	$_POST['name'],$sub_item_of,(int) $_POST['page_id'],$_POST['page_file'],$order));
			}

		}
		else {

			//Section
			$section_id 	= 	(int) $_POST['section'];

			//For all languages or not?
			if (isset($_POST['all_languages'])) {

				//Loop all languages
				foreach ($installed_languages as $language) {

					//Set language tables
					$table 		= DIGI_DB_PREFIX.'menu_items_'.$language;
					$table_sec 	= DIGI_DB_PREFIX.'menu_sections_'.$language;

					//Does this section exist for this language?
					if (!getMyo($table_sec, $section_id, 'id')) {
						continue;
					}

					//Get item order
					$order = 1 + getMyo($table, "section='$section_id'", 'item_order', 'item_order','DESC',1);

					//Insert
					insRec($table,	array(	'name','section','page_id','page_file','item_order'),
									array(	$_POST['name'],$section_id,(int) $_POST['page_id'],$_POST['page_file'],$order));
				}
			}
			else {

				//Get item order
				$order = 1 + getMyo($db_table, "section='$section_id'", 'item_order', 'item_order','DESC',1);

				//Insert
				insRec($db_table,	array(	'name','section','page_id','page_file','item_order'),
									array(	$_POST['name'],$section_id,(int) $_POST['page_id'],$_POST['page_file'],$order));
			}
		}

		//Message
		$Message->set($msg_added);
	}
	$_GET['add'] = 1;
}

if (isset($_POST['edit_finish']) and $db_id = (int) $_POST['id']) {

	if ($sub and empty($_POST['sub_item_of'])) {
		$Message->set($msg_no_item_selected, WARNING);
		$_GET['edit'] = $db_id;
	}
	else {

		if ($sub) {

			//Old and new items
			$old_subof		=	getMyo($db_table, $db_id, 'sub_item_of');
			$new_subof		=	(int) $_POST['sub_item_of'];

			//Check if we need to adjust the item order
			if ($old_subof != $new_subof) {
				$old_order 	= getMyo($db_table, $db_id, 'item_order');
				$new_order 	= getMyo($db_table, "sub_item_of = '$new_subof'", 'item_order','item_order','DESC',1) + 1;
				$section	= getMyo($db_table,$db_id,'sub_item_of');
			}
			else {
				$new_order = getMyo($db_table, $db_id, 'item_order');
			}

			//Update
			upRec($db_table, $db_id,
							array('name','page_id','page_file','sub_item_of','item_order'),
							array($_POST['name'],(int) $_POST['page_id'],$_POST['page_file'],$new_subof,$new_order));

			//Fix the sort order if we changed the section
			if ($old_subof != $new_subof) {
				fixSortOrder($db_table, 'item_order', $old_order, "sub_item_of = '$old_subof'");
			}
		}
		else {

			//Old and new sections
			$old_section	=	getMyo($db_table, $db_id, 'section');
			$new_section	=	(int) $_POST['section'];

			//Check if we need to adjust the item order
			if ($old_section != $new_section) {
				$old_order 	= getMyo($db_table, $db_id, 'item_order');
				$new_order 	= getMyo($db_table, "section = '$new_section'", 'item_order','item_order','DESC',1) + 1;
				$section	= getMyo($db_table,$db_id,'section');
			}
			else {
				$new_order = getMyo($db_table, $db_id, 'item_order');
			}

			//Update
			upRec($db_table, $db_id,
							array('name','page_id','page_file','section','item_order'),
							array($_POST['name'],(int) $_POST['page_id'],$_POST['page_file'],$new_section,$new_order));

			//Fix the sort order if we changed the section
			if ($old_section != $new_section) {
				fixSortOrder($db_table, 'item_order', $old_order, "section = '$old_section'");
			}
		}

		//Message
		$Message->set($msg_editted);
	}
}

if (isset($_POST['add_section_finish'])) {

	//For all languages or not?
	if (isset($_POST['all_languages'])) {

		//Loop all languages
		foreach ($installed_languages as $language) {

			//Set language table
			$table_sec = DIGI_DB_PREFIX.'menu_sections_'.$language;

			//Get section order
			$order = 1 + getMyo($table_sec, '', 'section_order', 'section_order','DESC',1);

			//Insert
			insRec($table_sec,	array('name','section_order','page_id','page_file'),
								array($_POST['name'],$order,(int) $_POST['page_id'],$_POST['page_file']));
		}
	}
	else {
		//Get section order
		$order = 1 + getMyo($db_table_sections, '', 'section_order', 'section_order','DESC',1);

		//Insert
		insRec($db_table_sections,	array('name','section_order','page_id','page_file'),
									array($_POST['name'],$order,(int) $_POST['page_id'],$_POST['page_file']));
	}

	//Message
	$Message->set($msg_section_added);
}

if (isset($_POST['edit_section_finish']) and $db_id = (int) $_POST['id']) {

	//Update
	upRec($db_table_sections,$db_id,	array('name','page_id','page_file'),
										array($_POST['name'],(int) $_POST['page_id'],$_POST['page_file']));

	//Message
	$Message->set($msg_section_editted);
}

if (isset($_GET['delete']) and $db_id = (int) $_GET['delete'] and noBrowse()) {
	$Message->setConfirm($msg_delete_confirm,'delete',$db_id,'name');
}

if (isset($_GET['delete_confirm']) and $db_id = (int) $_GET['delete_confirm'] and noBrowse()) {

	if ($sub) {
		$item = getMyo($db_table,$db_id,array('item_order','sub_item_of'));
	}
	else {
		$item = getMyo($db_table,$db_id,array('item_order','section'));
	}

	delRec($db_table, $db_id);

	if ($sub) {
		fixSortOrder($db_table, 'item_order', $item->item_order, "sub_item_of = '$item->sub_item_of'");
	}
	else {
		fixSortOrder($db_table, 'item_order', $item->item_order, "section = '$item->section'");
	}

	$Message->set($msg_deleted);
}

if (isset($_GET['delete_section']) and $db_id = (int) $_GET['delete_section'] and noBrowse()) {
	swap($db_table,$db_table_sections);
	$Message->setConfirm($msg_delete_sec_confirm,'delete_section',$db_id,'name');
	swap($db_table,$db_table_sections);
}

if (isset($_GET['delete_section_confirm']) and $db_id = (int) $_GET['delete_section_confirm'] and noBrowse()) {

	$sort_order = getMyo($db_table_sections,$db_id,'section_order');

	delRec($db_table_sections, $db_id);
	$res = eq("SELECT id FROM $db_table WHERE section='$db_id';");
	while ($myo = mfo($res)) {
		delRec($db_table, "sub_item_of = '$myo->id'");
	}
	delRec($db_table, "section = '$db_id'");
	fixSortOrder($db_table_sections, 'section_order', $sort_order);
	$Message->set($msg_section_deleted);
}

if (isset($_GET['move_up']) and $db_id = (int) $_GET['move_up'] and noBrowse()) {

	if ($sub) {
		$item 		= getMyo($db_table, $db_id, array('item_order','sub_item_of'));
		$old_order 	= $item->item_order;
		$new_order 	= $item->item_order - 1;
		$other_id 	= getMyo($db_table, "item_order = '$new_order' AND sub_item_of = '$item->sub_item_of'", 'id');
	}
	else {
		$item 		= getMyo($db_table, $db_id, array('item_order','section'));
		$old_order 	= $item->item_order;
		$new_order 	= $item->item_order - 1;
		$other_id 	= getMyo($db_table, "item_order = '$new_order' AND section = '$item->section'", 'id');
	}

	if (!empty($other_id)) {
		upRec($db_table, $db_id,	'item_order', $new_order);
		upRec($db_table, $other_id,	'item_order', $old_order);
	}
}

if (isset($_GET['move_down']) and $db_id = (int) $_GET['move_down'] and noBrowse()) {

	if ($sub) {
		$item 		= getMyo($db_table, $db_id, array('item_order','sub_item_of'));
		$old_order 	= $item->item_order;
		$new_order 	= $item->item_order + 1;
		$other_id 	= getMyo($db_table, "item_order = '$new_order' AND sub_item_of = '$item->sub_item_of'", 'id');
	}
	else {
		$item 		= getMyo($db_table, $db_id, array('item_order','section'));
		$old_order 	= $item->item_order;
		$new_order 	= $item->item_order + 1;
		$other_id 	= getMyo($db_table, "item_order = '$new_order' AND section = '$item->section'", 'id');
	}

	if (!empty($other_id)) {
		upRec($db_table, $db_id,	'item_order', $new_order);
		upRec($db_table, $other_id,	'item_order', $old_order);
	}
}

if (isset($_GET['move_section_up']) and $db_id = (int) $_GET['move_section_up'] and noBrowse()) {

	$old_order 	= getMyo($db_table_sections, $db_id, 'section_order');
	$new_order 	= $old_order - 1;
	$other_id 	= getMyo($db_table_sections, "section_order = '$new_order'", 'id');

	if (!empty($other_id)) {
		upRec($db_table_sections, $db_id,		'section_order', $new_order);
		upRec($db_table_sections, $other_id,	'section_order', $old_order);
	}
}

if (isset($_GET['move_section_down']) and $db_id = (int) $_GET['move_section_down'] and noBrowse()) {

	$old_order 	= getMyo($db_table_sections, $db_id, 'section_order');
	$new_order 	= $old_order + 1;
	$other_id 	= getMyo($db_table_sections, "section_order = '$new_order'", 'id');

	if (!empty($other_id)) {
		upRec($db_table_sections, $db_id,		'section_order', $new_order);
		upRec($db_table_sections, $other_id,	'section_order', $old_order);
	}
}

/******************************************************************************************
 * Add / Edit / View options
 ***/

if (isset($_GET['add'])) {

	$items_options	=	$sub ? getItemOptions(isset($_POST['sub_item_of']) ? $_POST['sub_item_of'] : '') : array();

	if (!count($sections_array)) 				$Message->set($msg_no_sections, CRITICAL);
	elseif ($sub and !count($items_options)) 	$Message->set($msg_no_items, CRITICAL);
	else {

		$files_available	=	array('' => '') + readDirectory(CMS_PATH_PREFIX, false, false, true);

		$pre_html = "
			<h3>".($sub ? str_replace('[SUB]','sub',TXT_ADD_MENU_ITEM) : str_replace('[SUB]','',TXT_ADD_MENU_ITEM))." ($_lang):</h3><hr/><br/>
			<form name='add_item' method='post' action='$_file'>
			<input type='hidden' name='lang' value='$_lang' />
			<input type='hidden' name='sub' value='$sub' />

			<table border='0px' cellspacing='0px' cellpadding='2px'>
			<tr>
				<td width='150px'>".TXT_NAME.":</td>
				<td><input type='text' name='name' class='input_regular'/></td>
			</tr>
			".($sub ? "
			<tr>
				<td>".TXT_BELOW." menu item:</td>
				<td><select name='sub_item_of'>
					".implode("\n",$items_options)."
					</select></td>
			</tr>
			" : "
			<tr>
				<td>".TXT_SECTION.":</td>
				<td>".makeSelectBox('section',$sections_array,isset($_POST['section']) ? $_POST['section'] : 0)."</td>
			</tr>
			")."
			<tr>
				<td>".TXT_LINK_TO_PAGE.":</td>
				<td>".makeSelectBox('page_id',$pages_array,0)."</td>
			</tr>
			<tr>
				<td>".TXT_OR_FILE.":</td>
				<td>".makeSelectBox('page_file',$files_available,0)."</td>
			</tr>
			<tr>
				<td>".TXT_OR_MANUAL.":</td>
				<td><input type='text' name='manual_file' class='input_regular' /></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><br/><input type='submit' class='input_submit' name='add_finish' value='".BUTTON_ADD."' />
				".(count($installed_languages) > 1 ? "
				&nbsp;
				<input type='checkbox' name='all_languages' ".(isset($_POST['all_languages']) ? "checked='checked'" : '')." value='1' />
				".TXT_ADD_TO_ALL_LANGUAGES."
				" : '')."
				</td>
			</tr>
			</table>
			</form><br/>
		";
	}
}

if (isset($_GET['edit']) and $db_id = (int) $_GET['edit']) {

	$res = eq("SELECT * FROM $db_table WHERE id='$db_id';");
	$myo = mfo($res);

	$items_options	=	$sub ? getItemOptions($myo->sub_item_of) : array();

	if (!$sub and !count($sections_array)) 		$Message->set($msg_no_sections, CRITICAL);
	elseif ($sub and !count($items_options)) 	$Message->set($msg_no_items, CRITICAL);
	else {

 		$files_available	=	array('' => '') + readDirectory(CMS_PATH_PREFIX, false, false, true);
 		$manual_file		= 	in_array($myo->page_file,$files_available) ? '' : $myo->page_file;

		$pre_html = "
			<h3>".($sub ? str_replace('[SUB]','sub',TXT_EDIT_MENU_ITEM) : str_replace('[SUB]','',TXT_EDIT_MENU_ITEM))." ($_lang):</h3><hr/><br/>
			<form name='edit_item' method='post' action='$_file'>
			<input type='hidden' name='id' value='$db_id'/>
			<input type='hidden' name='lang' value='$_lang' />
			<input type='hidden' name='sub' value='$sub' />

			<table border='0px' cellspacing='0px' cellpadding='2px'>
			<tr>
				<td width='150px'>".TXT_NAME.":</td>
				<td><input type='text' name='name' class='input_regular' value='$myo->name'/></td>
			</tr>
			".($sub ? "
			<tr>
				<td>".TXT_BELOW." menu item:</td>
				<td><select name='sub_item_of'>
					".implode("\n",$items_options)."
					</select></td>
			</tr>
			" : "
			<tr>
				<td>".TXT_SECTION.":</td>
				<td>".makeSelectBox('section',$sections_array,$myo->section)."</td>
			</tr>
			")."
			<tr>
				<td>".TXT_LINK_TO_PAGE.":</td>
				<td>".makeSelectBox('page_id',$pages_array,$myo->page_id)."</td>
			</tr>
			<tr>
				<td>".TXT_OR_FILE.":</td>
				<td>".makeSelectBox('page_file',$files_available,$myo->page_file)."</td>
			</tr>
			<tr>
				<td>".TXT_OR_MANUAL.":</td>
				<td><input type='text' name='manual_file' class='input_regular' value='$manual_file'/></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><br/><input type='submit' class='input_submit' name='edit_finish' value='".BUTTON_SAVE."' /></td>
			</tr>
			</table>
			</form><br/>
		";
	}
}

if (isset($_GET['add_section'])) {

	$files_available	=	array('' => '') + readDirectory(CMS_PATH_PREFIX, false, false, true);

	$pre_html = "
		<h3>".TXT_ADD_SECTION." ($_lang):</h3><hr/><br/>
		<form name='add_section' method='post' action='$_file'>
		<input type='hidden' name='lang' value='$_lang' />

		<table border='0px' cellspacing='0px' cellpadding='2px'>
		<tr>
			<td width='150px'>".TXT_NAME.":</td>
			<td><input type='text' name='name' class='input_regular'/></td>
		</tr>
		<tr>
			<td>".TXT_LINK_TO_PAGE.":</td>
			<td>".makeSelectBox('page_id',$pages_array,0)."</td>
		</tr>
		<tr>
			<td>".TXT_OR_FILE.":</td>
			<td>".makeSelectBox('page_file',$files_available,0)."</td>
		</tr>
		<tr>
			<td>".TXT_OR_MANUAL.":</td>
			<td><input type='text' name='manual_file' class='input_regular'/></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><br/><input type='submit' class='input_submit' name='add_section_finish' value='".BUTTON_ADD."' />
			".(count($installed_languages) > 1 ? "
			&nbsp;
			<input type='checkbox' name='all_languages' ".(isset($_POST['all_languages']) ? "checked='checked'" : '')." value='1' />
			".TXT_ADD_TO_ALL_LANGUAGES."
			" : '')."
			</td>
		</tr>
		</table>
		</form><br/>
	";
}

if (isset($_GET['edit_section']) and $db_id = (int) $_GET['edit_section']) {

 	$res = eq("SELECT id, name, page_id, page_file FROM $db_table_sections WHERE id='$db_id';");
 	$myo = mfo($res);

	$files_available	=	array('' => '') + readDirectory(CMS_PATH_PREFIX, false, false, true);
	$manual_file		= 	in_array($myo->page_file,$files_available) ? '' : $myo->page_file;

	$pre_html = "
		<h3>".TXT_EDIT_SECTION." ($_lang):</h3><hr/><br/>
		<form name='edit_section' method='post' action='$_file'>
		<input type='hidden' name='lang' value='$_lang' />
		<input type='hidden' name='id' value='$myo->id'/>

		<table border='0px' cellspacing='0px' cellpadding='2px'>
		<tr>
			<td width='150px'>".TXT_NAME.":</td>
			<td><input type='text' name='name' class='input_regular' value='$myo->name'/></td>
		</tr>
		<tr>
			<td>".TXT_LINK_TO_PAGE.":</td>
			<td>".makeSelectBox('page_id',$pages_array,$myo->page_id)."</td>
		</tr>
		<tr>
			<td>".TXT_OR_FILE.":</td>
			<td>".makeSelectBox('page_file',$files_available,$myo->page_file)."</td>
		</tr>
		<tr>
			<td>".TXT_OR_MANUAL.":</td>
			<td><input type='text' name='manual_file' class='input_regular' value='$manual_file'/></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><br/><input type='submit' class='input_submit' name='edit_section_finish' value='".BUTTON_SAVE."' /></td>
		</tr>
		</table>
		</form><br/>
	";
}

 /******************************************************************************************
 * Page contents
 ***/

$non_linked = 0;

// *** Sections
$res = eq("SELECT id,name,page_id,page_file FROM $db_table_sections ORDER BY section_order ASC;");
while ($sec = mfo($res)) {

	if ($sec->page_id) {
		$linked_to = getMyo($db_table_pages, $sec->page_id, 'title');
	}
	elseif ($sec->page_file) {
		if (!$cfg['MENU_IGNORE_WRONG_LINKS'] and !file_exists(CMS_PATH_PREFIX.$sec->page_file)) {
			$linked_to = "<span class='red'>$sec->page_file</span>";
			$non_linked++;
		}
		else {
			$linked_to =  $sec->page_file;
		}
	}
	else {
		$linked_to = TXT_NOT_LINKED;
	}

	$data_rows[]	=	"	<div class='section'><b>$sec->name</b> &raquo; <i>$linked_to</i></div>
							<div class='section_options'>
							".($show_section_options ? "
								<a href='$_file?edit_section=$sec->id&$_app' title='".TXT_EDIT_SECTION."'>$img[edit]</a>
								<a href='$_file?delete_section=$sec->id&$_app' title='".TXT_DELETE_SECTION."'>$img[del]</a>
								<a href='$_file?move_section_up=$sec->id&$_app' title='".TXT_MOVE_SECTION_UP."'>$img[up]</a>
								<a href='$_file?move_section_down=$sec->id&$_app' title='".TXT_MOVE_SECTION_DOWN."'>$img[down]</a>
							" : '')."
							</div><div class='clear'></div>";

	// *** Menu items
	$res_items = eq("	SELECT id, name, page_id, page_file, item_order
						FROM $db_table WHERE section = '$sec->id' ORDER BY item_order ASC;");
	while ($item = mfo($res_items)) {

		if ($item->page_id) {
			$linked_to = getMyo($db_table_pages, $item->page_id, 'title');
		}
		elseif ($item->page_file) {
			if (!$cfg['MENU_IGNORE_WRONG_LINKS'] and !file_exists(CMS_PATH_PREFIX.$item->page_file)) {
				$linked_to = "<span class='red'>$item->page_file</span>";
				$non_linked++;
			}
			else {
				$linked_to =  $item->page_file;
			}
		}
		else {
			$linked_to = "<span class='red'>".TXT_NOT_YET_LINKED."</span>";
			$non_linked++;
		}

		$data_rows[]	=	"	<div class='item'>$item->name &raquo; <i>$linked_to</i></div>
								<div class='item_options'>
									<a href='$_file?edit=$item->id&$_app' title='".str_replace('[SUB]','',TXT_EDIT_MENU_ITEM)."'>$img[edit]</a>
									<a href='$_file?delete=$item->id&$_app' title='".TXT_DELETE_MENU_ITEM."'>$img[del]</a>
									<a href='$_file?move_up=$item->id&$_app' title='".TXT_MOVE_MENU_ITEM_UP."'>$img[up]</a>
									<a href='$_file?move_down=$item->id&$_app' title='".TXT_MOVE_MENU_ITEM_DOWN."'>$img[down]</a>
								</div><div class='clear'></div>";

		// *** Sub menu items
		$res_subitems = eq("	SELECT id, name, page_id, page_file, item_order
								FROM $db_table WHERE sub_item_of = '$item->id' ORDER BY item_order ASC;");
		while ($subitem = mfo($res_subitems)) {

			if ($subitem->page_id) {
				$linked_to = getMyo($db_table_pages, $subitem->page_id, 'title');
			}
			elseif ($subitem->page_file) {
				if (!$cfg['MENU_IGNORE_WRONG_LINKS'] and !file_exists(CMS_PATH_PREFIX.$subitem->page_file)) {
					$linked_to = "<span class='red'>$subitem->page_file</span>";
					$non_linked++;
				}
				else {
					$linked_to =  $subitem->page_file;
				}
			}
			else {
				$linked_to = "<span class='red'>nog niet gelinkt</span>";
				$non_linked++;
			}

			$data_rows[] =	"	<div class='sub_item'>$subitem->name &raquo; <i>$linked_to</i></div>
								<div class='sub_item_options'>
								  <a href='$_file?edit=$subitem->id&sub=1&$_app' title='".str_replace('[SUB]','',TXT_EDIT_MENU_ITEM)."'>$img[edit]</a>
								  <a href='$_file?delete=$subitem->id&$_app' title='".TXT_DELETE_MENU_ITEM."'>$img[del]</a>
								  <a href='$_file?move_up=$subitem->id&sub=1&$_app' title='".TXT_MOVE_MENU_ITEM_UP."'>$img[up]</a>
								  <a href='$_file?move_down=$subitem->id&sub=1&$_app' title='".TXT_MOVE_MENU_ITEM_DOWN."'>$img[down]</a>
								</div><div class='clear'></div>";
		}
	}
}

//Set non-linked message
if ($non_linked and !$cfg['MENU_IGNORE_WRONG_LINKS']) {
	$Message->add($msg_non_linked, WARNING);
}

//Language selectors
$lang_selectors = array();
foreach ($installed_languages as $language) {
	$lang_selectors[] = ($language == $_lang) ? $language : "<a href='$_file?lang=$language'>$language</a>";
}
$lang_selectors = implode(' - ', $lang_selectors);

//Page title HTML
$html = "
	<a href='$_file'><img src='$site_icon' border='0px' class='cms_icon'/></a>
	<h6>
		<a href='$_file?lang=$_lang'>".TXT_OVERVIEW."</a> &nbsp;
		".($show_section_options ? "$img[add] <a href='$_file?add_section=1&lang=$_lang'>".TXT_NEW_SECTION."</a> &nbsp; " : '')."
		$img[add] <a href='$_file?add=1&lang=$_lang'>".TXT_NEW_MENU_ITEM."</a> &nbsp;
		".($show_sub_options ? "$img[add] <a href='$_file?add=1&sub=1&lang=$_lang'>".TXT_NEW_SUBMENU_ITEM."</a>" : '')."
		&nbsp; ($lang_selectors)
	</h6>
	<br/>".$Message->get()."
	<br/><br/>$pre_html

	<h3>Website menu ($_lang)</h3><hr/><br/>
	".implode("\n", $data_rows)."
";

echo $html;

require_once('inc/cms_footer.php');
?>