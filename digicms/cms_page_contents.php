<?php
/*
Title:		Manages text elements on the website (++FCKeditor)
File: 		cms_page_contents.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

/******************************************************************************************
 * Settings
 ***/

require_once('inc/cms_header.php');
require_once('inc/lang/'.$cms_lang.'_'.$_file);
require_once('inc/definitions/def_page_sets_'.$cms_lang.'.php');
require_once(CMS_PATH_PREFIX.'classes/class_DataTable.php');
require_once(CMS_PATH_PREFIX.'apps/fckeditor/fckeditor.php');

// *** Digi check
$is_digi				=	(($s_id == 1 and $s_admin) or ($testmode and $s_admin)) ? true : false;

// *** Database table is dependent on language
$db_table				=	DIGI_DB_PREFIX.'page_contents_'.$_lang;

// *** Sorting and pages functionality
$default_sort_option	=	'page_set';
$default_sort_method	=	'ASC';
$secondary_sort_option	=	'';
$secondary_sort_method	=	'';
$valid_sort_options		=	array('page_set');

// *** Group clause
$group_clause			=	'GROUP BY page_set';

// *** Sorting and pages and link appendix
require_once(CMS_PATH_PREFIX.'inc/sorting_and_pages.php');
$_app					=	$page_appendix.'&'.$sort_appendix.'&'.$link_appendix.'&lang='.$_lang;

/******************************************************************************************
 * POST and GET Cases
 ***/

// *** Images url fixing
if (isset($_GET['fix_images'])) {

	$from_url	=	$_GET['from_url'];
	$to_url		=	$_GET['to_url'];

	if (!is_dir($to_url)) {
		$Message->set($msg_invalid_to_url, WARNING);
	}

	foreach ($installed_languages as $language) {
		$table = DIGI_DB_PREFIX.'page_contents_'.$language;
		$res = eq("SELECT id,contents FROM $table;");
		while ($myo = mfo($res)) {
			$myo->contents = str_replace($from_url, $to_url, $myo->contents);
			upRec($table, $myo->id, 'contents', $myo->contents);
		}
	}
	$Message->set($msg_images_fixed);
}

if (isset($_POST['add_finish']) and $is_digi) {

	if (empty($_POST['definition']) or empty($_POST['page_set'])) {
		$Message->set($msg_empty_fields, CRITICAL);
	}
	else {

		evalAll($_POST['definition']);
		evalAll($_POST['description']);
		evalAll($_POST['page_set']);
		evalAll($_POST['contents'], $_POST['is_fck'] ? false : true);

		$definition = strtoupper($_POST['definition']);

		$res = eq("SELECT page_set FROM $db_table WHERE definition = '$definition' AND page_set = '$_POST[page_set]';");
		if (mysql_num_rows($res)) {
			$myo = mfo($res);
			$Message->set($msg_element_exists, WARNING);
		}

		else {

			//Insert the definition into each language table
			foreach ($installed_languages as $language) {
				insRec(DIGI_DB_PREFIX.'page_contents_'.$language,
					array('definition','page_set','contents','description','is_fck'),
					array($definition, $_POST['page_set'], $_POST['contents'], $_POST['description'],$_POST['is_fck'])
				);
			}

			cmsLog("$object '$definition' ".LOG_CREATED);
			$Message->set($msg_element_added.'<b>'.$definition.'</b>');
			unset($_POST['contents']);
			unset($_POST['description']);
			unset($_POST['definition']);
		}
	}
	$_GET['add'] = 1;
}

if (isset($_POST['edit_finish'])) {

    foreach ($_POST as $key => $value) {

		if (substr($key,0,2) == 'v_') {
			evalAll($value);
			$process = true;
        }
        elseif (substr($key,0,2) == 'f_') {
			evalAll($value,false);
			$process = true;
		}
		else {
			$process = false;
		}

		if ($process and $db_id = (int) substr($key,2)) {

			$props	=	array('contents');
			$values	=	array($value);

			if ($is_digi) {
				$props[]	=	'is_fck';
				$values[]	=	$_POST['is_fck_'.$db_id];
			}

	        upRec($db_table, $db_id, $props, $values);
	    }
    }

	cmsLog($plural.' '.LOG_EDITTED);
    $Message->set($msg_elements_editted);
}

if (isset($_POST['edit_page_finish']) and $is_digi) {

    foreach ($_POST as $key => $value) {
        if (substr($key,0,2) == 'v_') {

        	evalAll($value);
            $db_id = (int) substr($key,2);
            upRec($db_table, $db_id, 'page_set', $value);
        }
    }

	cmsLog(LOG_PAGE_SETS_EDITTED);
    $Message->set($msg_pages_editted);
}

if (isset($_GET['delete']) and $db_id = (int) $_GET['delete'] and $is_digi) {
	$Message->setConfirm($msg_delete_confirm, 'delete', $db_id,'description','','edit_page_set='.$_GET['edit_page_set'].'&rs='.$random_string);
}

if (isset($_GET['delete_confirm']) and $db_id = (int) $_GET['delete_confirm'] and noBrowse() and $is_digi) {
	delRec($db_table, $db_id);
	cmsLog("$object (#$db_id) ".LOG_DELETED);
	$Message->set($msg_deleted);
}

if (isset($_GET['delete_page_set']) and $page_set = $_GET['delete_page_set'] and $is_digi) {
	$Message->setConfirm($msg_delete_ps_confirm,'delete_page_set',$page_set);
}

if (isset($_GET['delete_page_set_confirm']) and $page_set = $_GET['delete_page_set_confirm'] and noBrowse() and $is_digi) {
	delRec($db_table, "page_set = '$page_set'");
	cmsLog("$plural ($page_set) ".LOG_DELETED);
	$Message->set($msg_deleted_ps);
}


/******************************************************************************************
 * Add and edit options
 ***/

if (isset($_GET['add']) and $is_digi) {

	$_POST['definition']	=	isset($_POST['definition']) 	? $_POST['definition'] : '';
	$_POST['page_set']		=	isset($_POST['page_set']) 		? $_POST['page_set'] : '';
	$_POST['contents']		=	isset($_POST['contents']) 		? $_POST['contents'] : '';
	$_POST['description']	=	isset($_POST['description'])	? $_POST['description'] : '';

	$oFCKeditor 			= 	new FCKeditor('contents') ;
	$oFCKeditor->BasePath	= 	CMS_PATH_PREFIX.'apps/fckeditor/';
	$oFCKeditor->Value		= 	$_POST['contents'];
	$oFCKeditor->Width  	= 	'366px';
	$oFCKeditor->Height		= 	'200px';
	$oFCKeditor->ToolbarSet = 	'Basic';

	$pre_html = "
		<h3>".TXT_ADD_ITEM."</h3><hr/><br/>
		<form name='add_element_form' method='post' action='$_file'>
		<table cellspacing='0px' cellpadding='1px' border='0px'>
		<tr>
			<td width='150px'>".TXT_ELEMENT_NAME.":</td>
			<td><input type='text' name='definition' class='input_regular' value='$_POST[definition]'/></td>
		</tr>
		<tr>
			<td>".TXT_DESCRIPTION.":</td>
			<td><input type='text' name='description' class='input_regular' value='$_POST[description]'/></td>
		</tr>
		<tr>
			<td>".TXT_PAGE_SET.":</td>
			<td><input type='text' name='page_set' class='input_regular' value='$_POST[page_set]'/></td>
		</tr>
		<tr>
			<td>".TXT_FCK_FIELD.":</td>
			<td>".makeSelectbox('is_fck',$YESNO_ARRAY,0)."</td>
		</tr>
		<tr>
			<td colspan='2'><br/>".TXT_TEXT.":<br/><textarea name='contents'>$_POST[contents]</textarea></td>
		</tr>
		<tr>
			<td colspan='2'><br/><input type='submit' class='input_submit' name='add_finish' value='".BUTTON_ADD."' /></td>
		</tr>
		</table>
		</form><br/>
	";
}

if (isset($_GET['edit_pages']) and $is_digi) {

	$page_set_rows 	= 	array();
	$prev_page		=	'';

    $res = eq("SELECT * FROM $db_table ORDER BY page_set ASC, definition ASC;");
    while ($myo = mfo($res)) {

     	if ($prev_page != $myo->page_set) {
			$page_set_rows[] = "<tr><td colspan='3'><br/><b>$myo->page_set</b></td></tr>";
		}

     	$prev_page = $myo->page_set;
        $page_set_rows[] = "<tr><td>$myo->definition:</td>
								<td><input type='text' name='v_$myo->id' value='$myo->page_set' class='input_regular'/></td>
							</tr>";
    }

    $pre_html = "
		<h3>".TXT_EDIT_PAGE_SETS." ($_lang)</h3><hr/><br/>
        <form name='update_pages_form' method='post' action='$_file'>
        <input type='hidden' name='lang' value='$_lang' />
        <table cellspacing='0px' cellpadding='1px' border='0px'>
        ".implode("\n",$page_set_rows)."
		<tr>
			<td width='200px'>&nbsp;</td>
			<td><br/><input type='submit' class='input_submit' name='edit_page_finish' value='".BUTTON_SAVE."' /></td>
		</tr>
		</table>
		</form><br/>";
}

if (isset($_GET['edit_page_set']) and $page_set = $_GET['edit_page_set']) {

	//Read elements
	$res = eq("SELECT id,description,contents,is_fck,definition FROM $db_table WHERE page_set = '$page_set' ORDER BY description ASC;");
	while ($myo = mfo($res)) {

		if ($myo->is_fck) {
			$oFCKeditor 			= 	new FCKeditor('f_'.$myo->id) ;
			$oFCKeditor->BasePath	= 	CMS_PATH_PREFIX.'apps/fckeditor/';
			$oFCKeditor->Value		= 	$myo->contents;
			$oFCKeditor->Width  	= 	'456px';
			$oFCKeditor->Height		= 	'200px';
			$oFCKeditor->ToolbarSet = 	'Basic';
		}

		$fck_option		=	$is_digi ? '<br/>FCK: '.makeSelectBox('is_fck_'.$myo->id, $YESNO_ARRAY, $myo->is_fck, 60).'<br/>&nbsp;' : '';
		$del_option		=	$is_digi ? "<h6><a href='$_file?delete=$myo->id&edit_page_set=$page_set&$_app'>$img[text_del]</a>
										$myo->definition</h6>" : '';

		$element_rows[] = "
			<tr>
				<td valign='top' width='200px'>$myo->description:<br/>$del_option</td>
				<td>
				".($myo->is_fck ? $oFCKeditor->CreateHtml().$fck_option : "
				<textarea name='v_$myo->id' style='margin-left: 1px; width: 450px; height: 92px;'>$myo->contents</textarea>$fck_option
				")."
				</td>
			</tr>
		";
	}

	$page_set_name 	= 	isset($page_set_names[$page_set]) ? $page_set_names[$page_set] : $page_set;

	$pre_html = "
		<h3>$page_set_name ($_lang)</h3><hr/><br/>
		<form name='edit_form' method='post' action='$_file'>
        <input type='hidden' name='lang' value='$_lang' />
		<table cellspacing='0px' cellpadding='1px' border='0px'>
		".implode("\n",$element_rows)."
		<tr>
			<td width='200px'></td>
			<td><br/><input type='submit' class='input_submit' style='margin-left: 1px;' name='edit_finish' value='".BUTTON_SAVE."'/></td>
		</tr>
		</table>
		</form><br/>
	";
}

/******************************************************************************************
 * Page contents
 ***/

//Language selectors
require_once('inc/definitions/def_lang_selectors.php');

//Page title HTML
$html = 	"
	<a href='$_file'><img src='$site_icon' border='0px' class='cms_icon'/></a>
	<h6>
		<a href='$_file?lang=$_lang'>".TXT_OVERVIEW."</a>
		".(($is_digi) ?  " &nbsp; $img[text_add] <a href='$_file?add&lang=$_lang'>".TXT_NEW_ITEM."</a>
						   &nbsp; $img[text_edit] <a href='$_file?edit_pages&lang=$_lang'>".TXT_ALTER_PAGE_SETS."</a>
		" : '')."
		$lang_selectors
	</h6>
	<br/>".$Message->get()."
	<br/><br/>$pre_html
";

//Read sets
$res = eq("	SELECT 		id,page_set
			FROM 		$db_table $where_clause $group_clause
			ORDER BY 	$order_clause
");

while ($myo = mfo($res)) {

	$page_set_name 	= 	ucfirst(isset($page_set_names[$myo->page_set]) ? $page_set_names[$myo->page_set] : $myo->page_set);
	$data_rows[]	= 	array(

		//This MUST be the id
		$myo->id,

		//All other desired fields
		"&nbsp;<a href='$_file?edit_page_set=$myo->page_set&$_app' title='".TXT_EDIT_ITEM."'>$page_set_name</a>",

		//The options last
		"<a href='$_file?edit_page_set=$myo->page_set&$_app' title='".TXT_EDIT_ITEM."'>$img[edit]</a>"
		.($is_digi ? "
		<a href='$_file?delete_page_set=$myo->page_set&$_app' title='".TXT_DELETE_ITEM."'>$img[text_del]</a>
		" : '')
	);
}

//Output data table
$DataTable = new DataTable(TXT_PAGE_SETS." ($_lang)", 300);
$DataTable->setMarkAllOptions();
$DataTable->setDataRows($data_rows);
$DataTable->setLegendRow(
	array(TXT_PAGE_SET,TXT_OPTIONS),
	array(0,40),
	array('page_set')
);
echo $html.$DataTable->getHTML();

require_once('inc/cms_footer.php');
?>