<?
/*
Title:		Pages editor (++FCKeditor)
File: 		cms_pages.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

/******************************************************************************************
 * Settings
 ***/

// *** Set max upload time
ini_set('max_execution_time',300);

// *** Required files
require_once('inc/cms_header.php');
require_once('inc/lang/'.$cms_lang.'_'.$_file);
require_once('inc/definitions/def_file_icons.php');
require_once(CMS_PATH_PREFIX.'classes/class_ImageHandler.php');
require_once(CMS_PATH_PREFIX.'classes/class_DataTable.php');
require_once(CMS_PATH_PREFIX.'apps/fckeditor/fckeditor.php');

// *** Digi check
$is_digi				=	(($s_id == 1 and $s_admin) or ($testmode and $s_admin)) ? true : false;

// *** Database table is dependent on language
$db_table				=	DIGI_DB_PREFIX.'pages_'.$_lang;
$db_table_extranet_gr	=	DIGI_DB_PREFIX.'extranet_user_groups';
$db_table_page_gr_rel	=	DIGI_DB_PREFIX.'page_group_relations_'.$_lang;
$db_table_categories	=	DIGI_DB_PREFIX.'page_categories';

// *** Files
$file_page_cats			=	'cms_page_categories.php';

// *** Display settings
$show_meta_tags			=	$cfg['PAGES_SHOW_META'];
$show_image				=	$cfg['PAGES_SHOW_IMAGE'];
$show_template			=	$cfg['PAGES_SHOW_TEMPLATE'];
$show_file				=	$cfg['PAGES_SHOW_FILE'];
$show_html_name			=	$cfg['PAGES_SHOW_HTML_NAME'];
$show_auth_options		=	$cfg['PAGES_SHOW_AUTH'];
$show_free_field		=	$cfg['PAGES_SHOW_FREE'];

// *** Image handling default settings
$image_resize_size		=	$cfg['PAGEIMAGE_SIZE'];
$image_crop_width		=	$cfg['PAGEIMAGE_CROP_WIDTH'];
$image_crop_height		=	$cfg['PAGEIMAGE_CROP_HEIGHT'];

// *** Other image handling settings
$image_dir				=	'..'.$_sep.'gfx'.$_sep.'pages';
$allowed_extensions		=	array('jpg','jpeg','gif','png','swf','fla','flv');
$max_upload_bytes		=	returnBytes(ini_get('upload_max_filesize'));
$max_upload_size		=	byte2kb($max_upload_bytes);
$max_script_time		=	round(ini_get('max_execution_time')/60);

// *** Free field title and explanation
$free_field_title		=	$cfg['FREE_FIELD_TITLE'];
$free_field_explanation	=	$cfg['FREE_FIELD_EXPLANATION'];

// *** Sorting and pages functionality
$default_sort_option	=	'title';
$default_sort_method	=	'ASC';
$secondary_sort_option	=	'';
$secondary_sort_method	=	'';
$valid_sort_options		=	array('id','title','file_name');

// *** Search constraints
if (!empty($search_query)) {
	$search				=	array();
	$search[]			=	"title LIKE '%$search_query%'";
	$search[]			=	"file_name LIKE '%$search_query%'";
	$search[]			=	"html_name LIKE '%$search_query%'";
	$search[]			=	"contents LIKE '%$search_query%'";
	$search[]			=	"meta_description LIKE '%$search_query%'";
	$search[]			=	"meta_keywords LIKE '%$search_query%'";
	$where[]			=	'('.implode(' OR ', $search).')';
}

// *** Initialize
$category_id			=	-1;

// *** Clear old session info
if (isset($_GET['category_id'])) {
	unset($_SESSION['page_category_id']);
}

// *** Category filter
if ((isset($_GET['category_id']) and is_numeric($category_id = (int) $_GET['category_id'])) or
	(isset($_SESSION['page_category_id']) and is_numeric($category_id = (int) $_SESSION['page_category_id']))) {

	$_SESSION['page_category_id'] = $category_id;
	if ($category_id > -1) {
		$where[] = "category_id='$category_id'";
	}
}

// *** Message definitions
$msg_directory_error	=	str_replace('[DIR]', $image_dir, $msg_directory_error);

// *** Extra link appendix
$link_appendix 			.=	'&lang='.$_lang;

// *** Sorting and pages and link appendix
require_once(CMS_PATH_PREFIX.'inc/sorting_and_pages.php');
$_app					=	$page_appendix.'&'.$sort_appendix.'&'.$link_appendix;

// *** Extra definitions
$resize_constraints		=	array(	0					=> 	CONSTRAINT_NAME_NONE,
									CONSTRAINT_SIZE 	=> 	CONSTRAINT_NAME_SIZE,
									CONSTRAINT_WIDTH 	=> 	CONSTRAINT_NAME_WIDTH,
									CONSTRAINT_HEIGHT 	=> 	CONSTRAINT_NAME_HEIGHT,
									CONSTRAINT_CROP		=> 	CONSTRAINT_NAME_CROP);

// *** Read user groups, depending on modules installed
$USER_GROUPS = array();

if (isset($installed_modules[MOD_EXTRANET])) {
	$res = eq("SELECT id,name FROM $db_table_extranet_gr ORDER BY name ASC;");
	while ($myo = mfo($res)) {
		$USER_GROUPS[$myo->id] = $myo->name;
	}
}

// *** Read categories
$PAGE_CATEGORIES	=	array(0 => TXT_NO_CATEGORY) + makeArray($db_table_categories,'id','name','','name','ASC');

/******************************************************************************************
 * Image folder check
 ***/

$dir_exists = checkFolder($image_dir);

/******************************************************************************************
 * Image/flash file processing function
 ***/

function processImage($language, $db_id) {

	global $dir_exists, $image_dir, $_sep, $chmod, $allowed_extensions, $Message;
	global $msg_directory_error, $msg_upload_error, $msg_resample_error, $_error;

	if ($dir_exists and !empty($_FILES['image']['name'])) {

		$image_file	=	'page_'.$language.'_'.$db_id;	//WITHOUT EXTENSION
		$image_url 	= 	$image_dir.$_sep.$image_file;

		//Move new image to dir
		if (($result = uploadFile('image', $image_dir, $image_file, $chmod, false,true, $allowed_extensions, true,false)) !== false) {

			list($file_name, $file_extension) = $result;

			//Do we need to resize or crop? Only images
			if ($_POST['resize_constraint'] and in_array($file_extension, array('.jpg','.jpeg','.png','.gif'))) {

				//Resample as JPG
				$ImageHandler	=	new ImageHandler($image_url.$file_extension, $image_url.'.jpg');

				//Valid crop constraints set?
				if ($resize_constraint = (int) $_POST['resize_constraint']) {
					if ($resize_constraint == CONSTRAINT_CROP
						and isset($_POST['crop_width']) and isset($_POST['crop_height'])
						and ($width = (int) $_POST['crop_width']) and ($height = (int) $_POST['crop_height'])) {

						$ImageHandler->setTargetSize($width, $height, true);
					}

					//Valid size constraint set?
					elseif (isset($_POST['resize_size']) and $size = (int) $_POST['resize_size']) {
						$ImageHandler->setTargetSizeConstraint($size, $resize_constraint);
					}
				}

				//Output resized image
				if ($ImageHandler->output()) {

					//Remove the original file if it was not a jpg
					if ($file_extension != '.jpg') {
						unlink($image_url.$file_extension);
						$file_extension = '.jpg';
						$file_name = basename($image_url . '.jpg');
					}
				}
				else {
					$Message->add($msg_resample_error.'<br/><br/>'.$_error, WARNING);
				}
			}

			//Return the filename
			return $file_name;
		}
		else {
			$Message->add($msg_upload_error.'<br/><br/>'.$_error, WARNING);
		}
	}
	else if (!empty($_FILES['image']['name'])) {
		$Message->add($msg_directory_error, WARNING);
	}
	return '';
}

/******************************************************************************************
 * POST and GET Cases
 ***/

// *** Images url fixing
if (isset($_POST['fix_urls_finish']) and $is_digi) {

	evalAll($_POST['from_url'], false);
	evalAll($_POST['to_url'], false);

	$from_url	=	$_POST['from_url'];
	$to_url		=	$_POST['to_url'];

	foreach ($installed_languages as $language) {
		$table = DIGI_DB_PREFIX.'pages_'.$language;
		$res = eq("SELECT id,contents FROM $table;");
		while ($myo = mfo($res)) {
			$myo->contents = str_replace($from_url, $to_url, $myo->contents);
			upRec($table, $myo->id, 'contents', $myo->contents);
		}
	}
	$Message->set($msg_urls_fixed);
}

// *** Data cleansing and preparation of props and values
if (isset($_POST['add_finish']) or isset($_POST['edit_finish']) or isset($_POST['contents'])) {

	//Title and contents
	evalAll($_POST['title']);
	evalAll($_POST['contents'], false);

	if (empty($_POST['title'])) {
		$_POST['title'] = TXT_UNNAMED;
	}

 	$props	=	array('title','contents','category_id');
 	$values	=	array($_POST['title'],$_POST['contents'],$_POST['category_id']);

	//Active or not
	if (isset($_POST['add_finish']) or (isset($_POST['contents']) and !isset($_POST['id']))) {
		$props[]	=	'active';
		$values[]	=	checkPriv(PRIV_PUB_PAGES) ? 1 : 0;
	}

	//File name
	if ($show_file) {
		evalAll($_POST['file_name'], false);
		$_POST['file_name'] = basename($_POST['file_name']);
		$props[]	=	'file_name';
		$values[]	=	$_POST['file_name'];
	}

	//HTML name
	if ($show_html_name and $cfg['USE_REWRITE_ENGINE']) {
		evalAll($_POST['html_name'], false);
		$_POST['html_name'] = fileNameSafe(str_replace(' ','-',basename($_POST['html_name'])),false,true);
		if ($_POST['html_name'] and substr($_POST['html_name'],-5,5) != '.html') {
			$_POST['html_name'] .= '.html';
		}
		$props[]	=	'html_name';
		$values[]	=	$_POST['html_name'];
	}

	//Template
	if ($show_template) {
		evalAll($_POST['template'], false);
		$_POST['template'] = basename($_POST['template']);
		$props[]	=	'template';
		$values[]	=	$_POST['template'];
	}

	//Free field
	if ($show_free_field) {
		evalAll($_POST['free_field']);
		$props[]	=	'free_field';
		$values[]	=	$_POST['free_field'];
	}

	//Meta tags
	if ($show_meta_tags) {
		evalAll($_POST['meta_description']);
		evalAll($_POST['meta_keywords']);
		$props[]	=	'meta_description';
		$props[]	=	'meta_keywords';
		$values[]	=	$_POST['meta_description'];
		$values[]	=	$_POST['meta_keywords'];
	}

	//Auth users
	if ($show_auth_options and count($USER_GROUPS)) {
		$props[]	=	'needs_authorization';
		$values[]	=	(int) $_POST['needs_authorization'];

		if (isset($_POST['auth_groups']) and is_array($_POST['auth_groups'])) {
			$auth_groups = $_POST['auth_groups'];
		}
		else {
			$auth_groups = array();
		}
	}
}

// *** Add a page
if ((isset($_POST['add_finish']) or (!isset($_POST['id']) and isset($_POST['contents']))) and checkPriv(PRIV_ADD_PAGES)) {

	//For all languages or not?
	if (($cfg['PAGES_HARD_LINK_ML'] and count($installed_languages) > 1) or isset($_POST['all_languages'])) {

		//Loop all languages
		foreach ($installed_languages as $language) {

			//Set language table
			$table 			= DIGI_DB_PREFIX.'pages_'.$language;
			$table_gr_rel	= DIGI_DB_PREFIX.'page_group_relations_'.$language;

			//Insert page
			$db_id = insRec($table,$props,$values);

			//Process image only once
			if ($show_image and !isset($image_file) and $image_file = processImage($language, $db_id)) {
				upRec($table, $db_id, 'image_file', $image_file);
			}

			//Copy for the other languages
			elseif ($show_image and isset($image_file) and $image_file) {
				$image_file_lang = 'page_'.$language.'_'.$db_id.getFileExtension($image_dir.$_sep.$image_file, true);
				copy($image_dir.$_sep.$image_file, $image_dir.$_sep.$image_file_lang);
				upRec($table, $db_id, 'image_file', $image_file_lang);
			}

			//Page and auth users relations
			if ($show_auth_options and count($USER_GROUPS)) {
				delRec($table_gr_rel, "page_id='$db_id'");
				foreach ($auth_groups as $group_id) {
					insRec($table_gr_rel, array('page_id','group_id'), array($db_id, $group_id));
				}
			}

			//Log
			cmsLog("$plural '$_POST[title]' ".LOG_CREATED);
		}
	}
	else {

		//Insert and process image
		$db_id = insRec($db_table,$props,$values);
		if ($show_image and $image_file = processImage($_lang, $db_id)) {
			upRec($db_table, $db_id, 'image_file', $image_file);
		}

		//Page and auth users relations
		if ($show_auth_options and count($USER_GROUPS)) {
			delRec($db_table_page_gr_rel, "page_id='$db_id'");
			foreach ($auth_groups as $group_id) {
				insRec($db_table_page_gr_rel, array('page_id','group_id'), array($db_id, $group_id));
			}
		}

		//Log
		cmsLog("$object '$_POST[title]' (#$db_id) ".LOG_CREATED);
	}

	//Message
	$Message->set($msg_added);
}

// *** Edit a page
if ((isset($_POST['edit_finish']) or (isset($_POST['id']) and isset($_POST['contents'])))
	and $db_id = (int) $_POST['id'] and checkPriv(PRIV_EDIT_PAGES)) {

	//Update and process image
	if ($show_image and $image_file = processImage($_lang, $db_id)) {
		$props[] 	= 'image_file';
		$values[]	= $image_file;

		//Remove old image file if not overwritten by new one
		$old_image_file	=	getMyo($db_table, $db_id, 'image_file');
		if ($old_image_file != $image_file and file_exists($image_dir.$_sep.$old_image_file)) {
			@unlink($image_dir.$_sep.$old_image_file);
		}
	}
	upRec($db_table,$db_id,$props,$values);

	//Page and auth users relations
	if ($show_auth_options and count($USER_GROUPS)) {
		delRec($db_table_page_gr_rel, "page_id='$db_id'");
		foreach ($auth_groups as $group_id) {
			insRec($db_table_page_gr_rel, array('page_id','group_id'), array($db_id, $group_id));
		}
	}

	//Log and message
	cmsLog("$object '$_POST[title]' (#$db_id) ".LOG_EDITTED);
	$Message->set($msg_editted);
}

// *** Delete confirmation
if (isset($_GET['delete']) and $id_holder = $_GET['delete'] and checkPriv(PRIV_DEL_PAGES)) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			$Message->setConfirmMultiple($msg_delete_confirm_m, 'delete',$db_ids,'title');
		}
	}
	else if ($db_id = (int) $id_holder) {
		$Message->setConfirm($msg_delete_confirm,'delete',$db_id,'title');
	}
}

// *** Delete pages
if (isset($_GET['delete_confirm']) and $id_holder = $_GET['delete_confirm'] and noBrowse() and checkPriv(PRIV_DEL_PAGES)) {

	if (isset($_GET['multiple_confirm']) and is_array($id_holder)) {

		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			$deleted_pages = 0;
			foreach ($db_ids as $key => $db_id) {
				if (getMyo($db_table,$db_id,'locked')) {
					unset($db_ids[$key]);
					$some_locked = true;
				}
				else {
					if ($cfg['PAGES_HARD_LINK_ML'] and count($installed_languages) > 1) {
						foreach ($installed_languages as $language) {

							$table 			= DIGI_DB_PREFIX.'pages_'.$language;
							$table_gr_rel	= DIGI_DB_PREFIX.'page_group_relations_'.$language;

							$image_file	= getMyo($table, $db_id, 'image_file');
							if (file_exists($image_dir.$_sep.$image_file)) {
								@unlink($image_dir.$_sep.$image_file);
							}

							delRec($table, $db_id);
							delRec($table_gr_rel, "page_id = '$db_id'");
						}
					}
					else {
						$image_file	= getMyo($db_table, $db_id, 'image_file');
						if (file_exists($image_dir.$_sep.$image_file)) {
							@unlink($image_dir.$_sep.$image_file);
						}
						delRec($db_table, $db_id);
						delRec($db_table_page_gr_rel, "page_id = '$db_id'");
					}
					$deleted_pages++;
				}
			}

			if ($deleted_pages) {
				cmsLog(LOG_MULTIPLE.' '.$plural_lc.' '.LOG_DELETED);
				$Message->set($msg_deleted_m);
			}
			if (isset($some_locked)) {
				$Message->set($msg_some_pages_locked, WARNING);
			}
		}
	}
	else if ($db_id = (int) $id_holder) {
		if (!getMyo($db_table,$db_id,'locked')) {
			if ($cfg['PAGES_HARD_LINK_ML'] and count($installed_languages) > 1) {
				foreach ($installed_languages as $language) {

					$table 			= DIGI_DB_PREFIX.'pages_'.$language;
					$table_gr_rel	= DIGI_DB_PREFIX.'page_group_relations_'.$language;

					$image_file	= getMyo($table, $db_id, 'image_file');
					if (file_exists($image_dir.$_sep.$image_file)) {
						@unlink($image_dir.$_sep.$image_file);
					}

					delRec($table, $db_id);
					if (isset($installed_modules[MOD_EXTRANET]) or isset($installed_modules[MOD_EXTRANET_BASIC])) {
						delRec($table_gr_rel, "page_id = '$db_id'");
					}
				}
			}
			else {
				$image_file	= getMyo($db_table, $db_id, 'image_file');
				if (file_exists($image_dir.$_sep.$image_file)) {
					@unlink($image_dir.$_sep.$image_file);
				}
				delRec($db_table, $db_id);
				if (isset($installed_modules[MOD_EXTRANET]) or isset($installed_modules[MOD_EXTRANET_BASIC])) {
					delRec($db_table_page_gr_rel, "page_id = '$db_id'");
				}
			}
			cmsLog("$object (#$db_id) ".LOG_DELETED);
			$Message->set($msg_deleted);
		}
		else {
			$Message->set($msg_page_is_locked, CRITICAL);
		}
	}
}

// *** Acivate
if (isset($_GET['activate']) and $id_holder = $_GET['activate'] and noBrowse() and checkPriv(PRIV_PUB_PAGES)) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			upRec($db_table,$db_ids,'active',1);
			cmsLog(LOG_MULTIPLE.' '.$plural_lc.' '.LOG_ACTIVATED);
			$Message->set($msg_activated_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		upRec($db_table,$db_id,'active',1);
		cmsLog("$object (#$db_id) ".LOG_ACTIVATED);
		$Message->set($msg_activated);
	}
}

// *** De-activate
if (isset($_GET['deactivate']) and $id_holder = $_GET['deactivate'] and noBrowse() and checkPriv(PRIV_PUB_PAGES)) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			upRec($db_table,$db_ids,'active',0);
			cmsLog(LOG_MULTIPLE.' '.$plural_lc.' '.LOG_DEACTIVATED);
			$Message->set($msg_deactivated_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		upRec($db_table,$db_id,'active',0);
		cmsLog("$object (#$db_id) ".LOG_DEACTIVATED);
		$Message->set($msg_deactivated);
	}
}

// *** Lock
if (isset($_GET['lock']) and $id_holder = $_GET['lock'] and noBrowse() and checkPriv(PRIV_LOCK_PAGES)) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			upRec($db_table,$db_ids,'locked',1);
			cmsLog(LOG_MULTIPLE.' '.$plural_lc.' '.LOG_LOCKED);
			$Message->set($msg_locked_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		upRec($db_table,$db_id,'locked',1);
		cmsLog("$object (#$db_id) ".LOG_LOCKED);
		$Message->set($msg_locked);
	}
}

// *** Unlock
if (isset($_GET['unlock']) and $id_holder = $_GET['unlock'] and noBrowse() and checkPriv(PRIV_LOCK_PAGES)) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			upRec($db_table,$db_ids,'locked',0);
			cmsLog(LOG_MULTIPLE.' '.$plural_lc.' '.LOG_UNLOCKED);
			$Message->set($msg_unlocked_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		upRec($db_table,$db_id,'locked',0);
		cmsLog("$object (#$db_id) ".LOG_UNLOCKED);
		$Message->set($msg_unlocked);
	}
}

// *** Protect with authentication
if (isset($_GET['auth']) and $id_holder = $_GET['auth'] and noBrowse() and checkPriv(PRIV_AUTH_PAGES) and $show_auth_options) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			upRec($db_table,$db_ids,'needs_authorization',1);
			cmsLog(LOG_MULTIPLE.' '.$plural_lc.' '.LOG_PROTECTED);
			$Message->set($msg_authed_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		upRec($db_table,$db_id,'needs_authorization',1);
		cmsLog("$object (#$db_id) ".LOG_PROTECTED);
		$Message->set($msg_authed);
	}
}

// *** Remove authentication
if (isset($_GET['unauth']) and $id_holder = $_GET['unauth'] and noBrowse() and checkPriv(PRIV_AUTH_PAGES) and $show_auth_options) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			upRec($db_table,$db_ids,'needs_authorization',0);
			cmsLog(LOG_MULTIPLE.' '.$plural_lc.' '.LOG_UNPROTECTED);
			$Message->set($msg_unauthed_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		upRec($db_table,$db_id,'needs_authorization',0);
		cmsLog("$object (#$db_id) ".LOG_UNPROTECTED);
		$Message->set($msg_unauthed);
	}
}


/******************************************************************************************
 * Add / Edit / View options
 ***/

if (isset($_GET['add']) and checkPriv(PRIV_ADD_PAGES)) {

	$oFCKeditor 			= 	new FCKeditor('contents') ;
	$oFCKeditor->BasePath	= 	CMS_PATH_PREFIX.'apps/fckeditor/';
	$oFCKeditor->Value		= 	'';
	$oFCKeditor->Width  	= 	'694px';
	$oFCKeditor->Height		= 	'500px';
	$oFCKeditor->ToolbarSet = 	'Digi';

	//Available templates
	if ($show_template) {
		$templates_available	=	readDirectory(CMS_PATH_PREFIX.'template/website', false, false, true);
	}

	//Available files
	if ($show_file) {
		$files_available		=	array('' => '') + readDirectory(CMS_PATH_PREFIX, false, false, true);
	}

	$pre_html = "
		<h3>".TXT_ADD_ITEM." ($_lang):</h3><hr/><br/>
		<form name='add_page' method='post' action='$_file' enctype='multipart/form-data'>
		<input type='hidden' name='lang' value='$_lang' />
		<table border='0px' cellspacing='0px' cellpadding='2px'>

		<tr>
			<td width='105px'>".TXT_TITLE.":</td>
			<td><input type='text' name='title' class='input_regular' />
				&nbsp; <h6 class='grey'>".EXPL_PAGE_TITLE."</h6></td>
		</tr>
		<tr>
			<td>".TXT_CATEGORY.":</td>
			<td>".makeSelectBox('category_id',$PAGE_CATEGORIES,0)."
				&nbsp; <h6 class='grey'>".EXPL_CATEGORY."</h6></td>
		</tr>
		".($show_file ? "
		<tr>
			<td>".TXT_FILE.":</td>
			<td>".makeSelectBox('file_name',$files_available,0)."
				&nbsp; <h6 class='grey'>".EXPL_FILE_NAME."</h6></td>
		</tr>
		" : '')."
		".(($show_html_name and $cfg['USE_REWRITE_ENGINE']) ? "
		<tr>
			<td>".TXT_HTML_ALIAS.":</td>
			<td><input type='text' name='html_name' class='input_regular' />
				&nbsp; <h6 class='grey'>".EXPL_HTML_NAME."</h6></td>
		</tr>
		" : '')."
		".($show_template ? "
		<tr>
			<td>".TXT_TEMPLATE.":</td>
			<td>".makeSelectBox('template',$templates_available,0)."
				&nbsp; <h6 class='grey'>".EXPL_TEMPLATE."</h6></td>
		</tr>
		" : '')."
		".($show_free_field ? "
		<tr>
			<td>$free_field_title:</td>
			<td><input type='text' name='free_field' class='input_regular' />
				&nbsp; <h6 class='grey'>$free_field_explanation</h6></td>
		</tr>
		" : '')."
		".(($show_auth_options and count($USER_GROUPS)) ? "
		<tr>
			<td>".TXT_SECURED.":</td>
			<td>".makeSelectBox('needs_authorization',$YESNO_ARRAY,0)."
			&nbsp; <h6 class='grey'>".EXPL_AUTH."</h6></td>
		</tr>
		<tr>
			<td valign='top'>".TXT_GROUPS.":</td>
			<td style='padding-top: 3px;'>".makeCheckBoxList('auth_groups',$USER_GROUPS)."</td>
		</tr>
		" : '')."

		<tr>
			<td valign='top'><br/>".TXT_CONTENTS.":</td>
			<td><br/>".$oFCKeditor->CreateHtml()."</td>
		</tr>
		".($show_meta_tags ? "
		<tr>
			<td valign='top'><br/>".TXT_META_DESCRIPTION.":</td>
			<td><br/><textarea name='meta_description'></textarea></td>
		</tr>
		<tr>
			<td valign='top'><br/>".TXT_META_KEYWORDS.":</td>
			<td><br/><textarea name='meta_keywords'></textarea></td>
		</tr>
		" : '')."
		".($show_image ? "
		<tr>
			<td colspan='2'><br/><h3>".TXT_IMAGE_OR_FLASH."</h3></td>
		</tr>
		<tr>
			<td><br/>".TXT_FILE.":</td>
			<td><br/><input type='file' name='image' class='input_file' />
			<b>".TXT_MAX." $max_upload_size / $max_script_time ".TXT_MINUTES."</b></td>
		</tr>
		<tr>
			<td>
			<script type='text/javascript'>
				function evalConstraint(constraint) {
					if (constraint == 0) {
						setVisibility('resize_size', false);
						setVisibility('crop_width',  false);
						setVisibility('crop_height', false);
					}
					else if (constraint == ".CONSTRAINT_CROP.") {
						setVisibility('resize_size', false);
						setVisibility('crop_width',  true);
						setVisibility('crop_height', true);
					}
					else {
						setVisibility('resize_size', true);
						setVisibility('crop_width',  false);
						setVisibility('crop_height', false);
					}
				}
			</script>
			<br/>".TXT_RESIZE_TO.":</td>
			<td><br/>".makeSelectBox('resize_constraint', $resize_constraints, 0, '', "onchange='evalConstraint(this.value);'")."</td>
		</tr>
		<tr id='resize_size' style='display: none;'>
			<td>".TXT_MAX_SIZE.":</td>
			<td><input type='text' name='resize_size' class='input_small' $js[int] value='$image_resize_size'/> ".TXT_PIXELS."</td>
		</tr>
		<tr id='crop_width' style='display: none;'>
			<td>".TXT_WIDTH.":</td>
			<td><input type='text' name='crop_width' class='input_small' $js[int] value='$image_crop_width'/> ".TXT_PIXELS."</td>
		</tr>
		<tr id='crop_height' style='display: none;'>
			<td>".TXT_HEIGHT.":</td>
			<td><input type='text' name='crop_height' class='input_small' $js[int] value='$image_crop_height'/> ".TXT_PIXELS."</td>
		</tr>
		" : '')."
		<tr>
			<td>&nbsp;</td><td><br/><input type='submit' class='input_submit' name='add_finish' value='".BUTTON_MAKE."' />
			".(!$cfg['PAGES_HARD_LINK_ML'] and count($installed_languages) > 1 ? "
			&nbsp;
			<input type='checkbox' name='all_languages' ".(isset($_POST['all_languages']) ? "checked='checked'" : '')." value='1' />
			".TXT_ADD_TO_ALL_LANGUAGES."
			" : '')."
			</td>
		</tr>
		</table>
		</form><br/>
	";
}

if (isset($_GET['edit']) and $db_id = (int) $_GET['edit'] and checkPriv(PRIV_EDIT_PAGES)) {

 	$res = eq("SELECT * FROM $db_table WHERE id='$db_id';");
 	$myo = mfo($res);

 	if ($myo->locked) {
		$Message->set($msg_page_is_locked, WARNING);
	}
 	else {
	 	$oFCKeditor 			=  	new FCKeditor('contents') ;
		$oFCKeditor->BasePath	= 	CMS_PATH_PREFIX.'apps/fckeditor/';
		$oFCKeditor->Value		= 	$myo->contents;
		$oFCKeditor->Width  	= 	'694px';
		$oFCKeditor->Height		= 	'500px';
		$oFCKeditor->ToolbarSet = 	'Digi';

		//Available templates
		if ($show_template) {
			$templates_available	=	readDirectory(CMS_PATH_PREFIX.'template/website', false, false, true);
		}

		//Available files
		if ($show_file) {
			$files_available		=	array('' => '') + readDirectory(CMS_PATH_PREFIX, false, false, true);
		}

		//Image
		if ($show_image) {
			if ($myo->image_file and file_exists($image_dir.$_sep.$myo->image_file)) {
				$ext = getFileExtension($myo->image_file);
				if (in_array($ext, array('jpg','gif','png','jpeg'))) {
					$current_image = "<img src='".$image_dir.$_sep.$myo->image_file."' style='padding-bottom: 10px;'/>";
				}
				else {
					$current_image = "<a href='".$image_dir.$_sep.$myo->image_file."' target='_blank'>$myo->image_file</a>";
				}
			}
			elseif ($myo->image_file) {
				upRec($db_table, $db_id, 'image_file', '');
				$current_image = 'geen';
			}
			else {
				$current_image = 'geen';
			}
		}

		//Auth groups for this page
		if ($show_auth_options) {
			$checked_auth_groups = makeArray($db_table_page_gr_rel, 'group_id','group_id',"page_id = '$db_id'");
		}

		$pre_html = "
			<h3>".TXT_EDIT_ITEM." ($_lang):</h3><hr/><br/>
			<form name='edit_page' method='post' action='$_file' enctype='multipart/form-data'>
			<input type='hidden' name='lang' value='$_lang' />
			<input type='hidden' name='id' value='$db_id'/>
			$page_and_sort_inputs
			<table border='0px' cellspacing='0px' cellpadding='2px'>
			<tr>
				<td width='105px'>".TXT_TITLE.":</td>
				<td><input type='text' name='title' value='$myo->title' class='input_regular' />
					&nbsp; <h6 class='grey'>".EXPL_PAGE_TITLE."</h6></td>
			</tr>
			<tr>
				<td>".TXT_CATEGORY.":</td>
				<td>".makeSelectBox('category_id',$PAGE_CATEGORIES,$myo->category_id)."
				&nbsp; <h6 class='grey'>".EXPL_CATEGORY."</h6></td>
			</tr>
			".($show_file ? "
			<tr>
				<td>".TXT_FILE.":</td>
				<td>".makeSelectBox('file_name',$files_available,$myo->file_name)."
				&nbsp; <h6 class='grey'>".EXPL_FILE_NAME."</h6></td>
			</tr>
			" : '')."
			".(($show_html_name and $cfg['USE_REWRITE_ENGINE']) ? "
			<tr>
				<td>".TXT_HTML_ALIAS.":</td>
				<td><input type='text' name='html_name' class='input_regular' value='$myo->html_name'/>
					&nbsp; <h6 class='grey'>".EXPL_HTML_NAME."</h6></td>
			</tr>
			" : '')."
			".($show_template ? "
			<tr>
				<td>".TXT_TEMPLATE.":</td>
				<td>".makeSelectBox('template',$templates_available,$myo->template)."
				&nbsp; <h6 class='grey'>".EXPL_TEMPLATE."</h6></td>
			</tr>
			" : '')."
			".($show_free_field ? "
			<tr>
				<td>$free_field_title</td>
				<td><input type='text' name='free_field' class='input_regular' value='$myo->free_field' />
					&nbsp; <h6 class='grey'>$free_field_explanation</h6></td>
			</tr>
			" : '')."
			".(($show_auth_options and count($USER_GROUPS)) ? "
			<tr>
				<td>".TXT_SECURED.":</td>
				<td>".makeSelectBox('needs_authorization',$YESNO_ARRAY,$myo->needs_authorization)."
				&nbsp; <h6 class='grey'>".EXPL_AUTH."</h6></td>
			</tr>
			<tr>
				<td valign='top'>".TXT_GROUPS.":</td>
				<td style='padding-top: 3px;'>".makeCheckBoxList('auth_groups',$USER_GROUPS, $checked_auth_groups)."</td>
			</tr>
			" : '')."

			<tr>
				<td valign='top'><br/>".TXT_CONTENTS.":</td>
				<td><br/>".$oFCKeditor->CreateHtml()."</td>
			</tr>
			".($show_meta_tags ? "
			<tr>
				<td valign='top'><br/>".TXT_META_DESCRIPTION.":</td>
				<td><br/><textarea name='meta_description'>$myo->meta_description</textarea></td>
			</tr>
			<tr>
				<td valign='top'><br/>".TXT_META_KEYWORDS.":</td>
				<td><br/><textarea name='meta_keywords'>$myo->meta_keywords</textarea></td>
			</tr>
			" : '')."
			".($show_image ? "
			<tr>
				<td colspan='2'><br/><h3>".TXT_IMAGE_OR_FLASH."</h3></td>
			</tr>
			<tr>
				<td valign='top'><br/>".TXT_CURRENT_FILE.":</td>
				<td><br/>$current_image</td>
			</tr>
			<tr>
				<td>".BUTTON_CHANGE.":</td>
				<td><input type='file' name='image' class='input_file' />
				<b>".TXT_MAX." $max_upload_size / $max_script_time ".TXT_MINUTES."</b></td>
			</tr>
			<tr>
				<td>
				<script type='text/javascript'>
					function evalConstraint(constraint) {
						if (constraint == 0) {
							setVisibility('resize_size', false);
							setVisibility('crop_width',  false);
							setVisibility('crop_height', false);
						}
						else if (constraint == ".CONSTRAINT_CROP.") {
							setVisibility('resize_size', false);
							setVisibility('crop_width',  true);
							setVisibility('crop_height', true);
						}
						else {
							setVisibility('resize_size', true);
							setVisibility('crop_width',  false);
							setVisibility('crop_height', false);
						}
					}
				</script>
				<br/>".TXT_RESIZE_TO.":</td>
				<td><br/>".makeSelectBox('resize_constraint', $resize_constraints, 0, '', "onchange='evalConstraint(this.value);'")."</td>
			</tr>
			<tr id='resize_size' style='display: none;'>
				<td>".TXT_MAX_SIZE.":</td>
				<td><input type='text' name='resize_size' class='input_small' $js[int] value='$image_resize_size'/> ".TXT_PIXELS."</td>
			</tr>
			<tr id='crop_width' style='display: none;'>
				<td>".TXT_WIDTH.":</td>
				<td><input type='text' name='crop_width' class='input_small' $js[int] value='$image_crop_width'/> ".TXT_PIXELS."</td>
			</tr>
			<tr id='crop_height' style='display: none;'>
				<td>".TXT_HEIGHT.":</td>
				<td><input type='text' name='crop_height' class='input_small' $js[int] value='$image_crop_height'/> ".TXT_PIXELS."</td>
			</tr>
			" : '')."
			<tr>
				<td>&nbsp;</td><td><br/><input type='submit' class='input_submit' name='edit_finish' value='".BUTTON_SAVE."' /></td>
			</tr>
			</table>
			</form><br/>
		";
	}
}

// *** Images url fixing
if (isset($_GET['fix_urls']) and $is_digi) {

	$pre_html = "
		<h3>".ucfirst(TXT_FIX_URLS).":</h3><hr/><br/>
		<form name='fix_urls' method='post' action='$_file'
		$page_and_sort_inputs
		<table border='0px' cellspacing='0px' cellpadding='2px'>
		<tr>
			<td width='150px'>".TXT_FROM_URL.":</td>
			<td><input type='text' name='from_url' value='".request('from_url')."' class='input_regular' />
				&nbsp; <h6 class='grey'>".TXT_FOR_EXAMPLE.": /_klanten/abc/xyz/user_files</h6></td>
		</tr>
		<tr>
			<td>".TXT_TO_URL.":</td>
			<td><input type='text' name='to_url' value='".request('to_url')."' class='input_regular' />
				&nbsp; <h6 class='grey'>".TXT_FOR_EXAMPLE.": /user_files</h6></td>
		</tr>
		<tr>
			<td>&nbsp;</td><td><br/><input type='submit' class='input_submit' name='fix_urls_finish' value='".BUTTON_FIX."' /></td>
		</tr>
		</table>
		</form><br/>
	";
}

/******************************************************************************************
 * Page contents
 ***/

//Language selectors
require_once('inc/definitions/def_lang_selectors.php');

//Page title HTML
$html = "
	<a href='$_file' title='".ucfirst(TXT_OVERVIEW)."'><img src='$site_icon' border='0px' class='cms_icon'/></a>
	<h6>
		<a href='$_file?lang=$_lang'>".TXT_OVERVIEW."</a>
		".(checkPriv(PRIV_ADD_PAGES) ? " &nbsp; $img[page_add] <a href='$_file?add&lang=$_lang'>".TXT_NEW_ITEM."</a>" : '')."
		".(checkPriv(PRIV_PAGE_CATS) ? " &nbsp; $img[folder_full] <a href='$file_page_cats'>".TXT_PAGE_CATS."</a>" : '')."
		".($is_digi ? " &nbsp; $img[image_ok] <a href='$_file?fix_urls'>".TXT_FIX_URLS."</a>" : '')."
		$lang_selectors
	</h6>
	<br/><br/><br/>".TXT_FILTER_ON_CATEGORY.": ".makeSelectBox('category_id',array(-1 => '') + $PAGE_CATEGORIES,$category_id,0,"onchange='document.location.href=\"$_file?category_id=\"+this.value;'")."
	<br/>".$Message->get()."
	<br/><br/>$pre_html
";

//Get items for this page and this sorting method
$res = eq("	SELECT 		id, file_name, html_name, title, active, locked, needs_authorization AS auth
			FROM 		$db_table $where_clause
			ORDER BY 	$order_clause
			LIMIT 		$limit_clause
;");
$data_rows = array();

while ($myo = mfo($res)) {

	$file_name = $myo->file_name ? $myo->file_name : ($myo->html_name ? $myo->html_name : 'page.php?page_id='.$myo->id);

	$data_rows[] = array(

		//This MUST be the id
		$myo->id,

		//All desired other fields
		$myo->id,
		"<a href='".CMS_PATH_PREFIX.$file_name."' title='".TXT_VIEW_ITEM."' target='_blank'>$myo->title</a>",
		"<span title='Adres van de pagina'>$file_name</span>",

		$show_auth_options ? (
		checkPriv(PRIV_AUTH_PAGES) ? (
		$myo->auth ? 		"<a href='$_file?unauth=$myo->id&$_app' title='".TXT_UNPROTECT_ITEM."'>".$img['lock']."</a>" :
							"<a href='$_file?auth=$myo->id&$_app' title='".TXT_PROTECT_ITEM."'>".$img['lock_off_disabled']."</a>"
		) : (
		$myo->auth ? 		$img['lock'] :
							$img['lock_off_disabled']
		)) : '&nbsp;'
		,
		checkPriv(PRIV_LOCK_PAGES) ? (
		$myo->locked ? 		"<a href='$_file?unlock=$myo->id&$_app' title='".TXT_UNLOCK_ITEM."'>".$img['page_lock']."</a>" :
							"<a href='$_file?lock=$myo->id&$_app' title='".TXT_LOCK_ITEM."'>".$img['page']."</a>"
		) : (
		$myo->locked ? 		$img['page_lock'] :
							$img['page']
		),

		checkPriv(PRIV_PUB_PAGES) ? (
		$myo->active ? 		"<a href='$_file?deactivate=$myo->id&$_app' title='".TXT_DEACTIVATE_ITEM."'>$img[ok]</a>" :
							"<a href='$_file?activate=$myo->id&$_app' title='".TXT_ACTIVATE_ITEM."'>$img[notok]</a>"
		) : (
		$myo->active ? 		$img['ok'] :
							$img['notok']
		),

		//The options last
		(checkPriv(PRIV_EDIT_PAGES) ?
		"<a href='$_file?edit=$myo->id&$_app' title='".TXT_EDIT_ITEM."'>$img[page_edit]</a>&nbsp;" : '').
		(checkPriv(PRIV_DEL_PAGES) ?
		"<a href='$_file?delete=$myo->id&$_app' title='".TXT_DELETE_ITEM."'>$img[page_del]</a>" : '')
	);
}

//Output data table
$DataTable = new DataTable(TXT_TOTAL." [AMOUNT] $plural_lc ($_lang)");
$DataTable->setMarkAllOptions(	array(TXT_DELETE, 		'delete', 		TXT_DELETE_ITEMS),
								array(TXT_ACTIVATE, 	'activate', 	TXT_ACTIVATE_ITEMS),
								array(TXT_DEACTIVATE, 	'deactivate', 	TXT_DEACTIVATE_ITEMS),
								array(TXT_LOCK,			'lock',			TXT_LOCK_ITEMS),
								array(TXT_UNLOCK,		'unlock',		TXT_UNLOCK_ITEMS));
$DataTable->showSearch(array('lang' => $_lang));
$DataTable->setDataRows($data_rows);
$DataTable->setLegendRow(
	array('#',TXT_TITLE, TXT_PAGE_ADDRESS, $show_auth_options ? TXT_AUTH : '',TXT_LOCKED, TXT_ACTIVE, TXT_OPTIONS),
	array(60,0,200,50,60,60,40),
	array('id','title','','needs_authorization','locked','active')
);
echo $html.$DataTable->getHTML();

require_once('inc/cms_footer.php');
?>