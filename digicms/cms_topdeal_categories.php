<?php
/*
Title:		Product categories management [TOPDEAL]
File: 		cms_topdeal_categories.php
Version: 	v2.20
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

/******************************************************************************************
 * Settings
 ***/

// *** Requred files
require_once('inc/cms_header.php');
require_once(CMS_PATH_PREFIX.'classes/class_DataTable.php');
require_once(CMS_PATH_PREFIX.'classes/class_ImageHandler.php');
require_once(CMS_PATH_PREFIX.'apps/fckeditor/fckeditor.php');

// *** Database tables
$db_table_props			=	DIGI_DB_PREFIX.'topdeal_properties';
$db_table_prop_values	=	DIGI_DB_PREFIX.'topdeal_prop_values';

// *** Object definition
$object					=	'Categorie';
$object_lc 				= 	'categorie';
$plural					=	'Categorieen';
$plural_lc				=	'categorieen';
$adjective_e			=	'e';
$ident					=	'Deze';

// *** Image settings
$gfx_folder				=	'..'.$_sep.'gfx'.$_sep.'categories';
$gfx_prefix				=	'cat_';
$prod_cat_use_gfx		=	true;
$prod_cat_gfx_resize	=	true;
$prod_cat_gfx_size		=	171;
$prod_cat_constraint	=	CONSTRAINT_HEIGHT;
$allowed_extensions		=	array('jpg','jpeg','gif','png');
$max_upload_bytes		=	returnBytes(ini_get('upload_max_filesize'));
$max_upload_size		=	byte2kb($max_upload_bytes);
$max_script_time		=	round(ini_get('max_execution_time')/60);

// *** Messages
$msg_added				=	$object.' aangemaakt.';
$msg_editted			=	$object.' bijgewerkt.';
$msg_deleted			=	'Categorie(en) verwijderd.';
$msg_delete_confirm		= 	'Deze '.$object_lc.' en alle bijbehorende subcategorieen zeker weten verwijderen?';
$msg_delete_confirm_m 	= 	'Deze '.$plural_lc.' en alle bijbehorende subcategorieen zeker weten verwijderen?';
$msg_activated			=	$object.' geactiveerd.';
$msg_activated_m		=	$object.' geactiveerd.';
$msg_deactivated		=	$object.' op non-actief gezet.';
$msg_deactivated_m		=	$object.' op non-actief gezet.';
$msg_upload_error		=	'Er ging iets mis tijdens het uploaden van de afbeelding.';
$msg_resample_error		=	'De afbeelding kon niet verwerkt worden.';
$msg_directory_error	=	'De afbeeldingen map '.$gfx_folder.' bestaat niet.';

// *** Link appendix
$_app					=	$link_appendix;

// *** Manuals page category
$manuals_cat			=	1;

/******************************************************************************************
 * Image folder check
 ***/

$dir_exists = checkFolder($gfx_folder);

/******************************************************************************************
 * POST and GET Cases
 ***/

if (isset($_POST['add_finish']) or isset($_POST['edit_finish'])) {

	evalAll($_POST['name']);
	evalAll($_POST['menu_name']);
	evalAll($_POST['price_unit']);
	evalAll($_POST['description'], false);
	evalAll($_POST['description_overview'], false);
	$sc_of = (int) request('sub_cat_of');
}

if (isset($_POST['add_finish'])) {

	//Get the order this category is supposed to get
	$cat_order 	= 1 + getMyo($db_table,"sub_cat_of='$sc_of'",'category_order','category_order','DESC',1);

	//Insert category
    $db_id 		= insRec($db_table, array(	'name','sub_cat_of','category_order','description','menu_name','price_unit','manual',
											'description_overview','delivery_days'),
									array(	$_POST['name'],$sc_of,$cat_order,$_POST['description'],$_POST['menu_name'],
											$_POST['price_unit'],$_POST['manual'],$_POST['description_overview'], (int) $_POST['delivery_days']));

	cmsLog("$object '$_POST[name]' (#$db_id) aangemaakt.");
	$Message->set($msg_added);
}

if (isset($_POST['edit_finish']) and isset($_POST['id']) and $db_id = (int) $_POST['id']) {

    upRec($db_table, $db_id,	array(	'name','sub_cat_of','description','menu_name','price_unit','manual','description_overview',
										'delivery_days'),
								array(	$_POST['name'], $sc_of, $_POST['description'],$_POST['menu_name'],
										$_POST['price_unit'],$_POST['manual'],$_POST['description_overview'],(int) $_POST['delivery_days']));

    cmsLog("$object '$_POST[name]' (#$db_id) gewijzigd.");
    $Message->set($msg_editted);
}

// *** Image handling
if ($prod_cat_use_gfx and $dir_exists and isset($db_id) and $db_id and !empty($_FILES['image']['name'])) {

 	$image_file	=	$gfx_prefix.$db_id;					//WITHOUT EXTENSION
	$image_url 	= 	$gfx_folder.$_sep.$image_file;

	//Move new image to dir
	if (($result = uploadFile('image', $gfx_folder, $image_file, $chmod, true, true, $allowed_extensions)) !== false) {

		list($file_name, $file_extension) = $result;

		//Resampling as JPG
		$ImageHandler	=	new ImageHandler($image_url.$file_extension, $image_url.'.jpg');

		//Do we resize?
		if ($prod_cat_gfx_resize and $prod_cat_gfx_size) {
			$ImageHandler->setTargetSizeConstraint($prod_cat_gfx_size, $prod_cat_constraint);
		}

		//If not keep original size
		else {
			list($width,$height) = getimagesize($image_url.$file_extension);
			$ImageHandler->setTargetSize($width, $height);
		}

		//Resample
		if ($ImageHandler->output()) {

			//Remove the original file if it was not a jpg
			if ($file_extension != '.jpg') {
				@unlink($image_url.$file_extension);
			}
		}
		else {
			@unlink($image_url.$file_extension);
			$Message->add($msg_resample_error.'<br/><br/>'.$_error, WARNING);
		}
	}
	else {
		$Message->add($msg_upload_error.'<br/><br/>'.$_error, WARNING);
	}
}
else if ($prod_cat_use_gfx and isset($db_id) and $db_id and !empty($_FILES['image']['name'])) {
	$Message->add($msg_directory_error, WARNING);
}

// *** Delete confirmation
if (isset($_GET['delete']) and $id_holder = $_GET['delete']) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			$Message->setConfirmMultiple($msg_delete_confirm_m, 'delete',$db_ids,'name');
		}
	}
	else if ($db_id = (int) $id_holder) {
		$Message->setConfirm($msg_delete_confirm,'delete',$db_id,'name');
	}
}

// *** Delete action
if (isset($_GET['delete_confirm']) and $id_holder = $_GET['delete_confirm'] and noBrowse()) {

	if (isset($_GET['multiple_confirm']) and is_array($id_holder)) {
		$db_ids = $all_ids = validateArrayInt($id_holder);
		$scs = array();
	}
	else if ($db_id = (int) $id_holder) {
		$db_ids = $all_ids = array($db_id);
	}

	if (count($db_ids)) {

		//Loop the categories to delete
		foreach ($db_ids as $db_id) {

			//Subcat for fixing sort order
			$scs[] = getMyo($db_table, $db_id, 'sub_cat_of');

			//Get their sub categories
			$res = eq("SELECT id,sub_cat_of FROM $db_table WHERE sub_cat_of = '$db_id';");
			while ($myo = mfo($res)) {

				//Add to deletion array
				$all_ids[] = $myo->id;

				//Find THEIR sub categories
				$res_s = eq("SELECT id FROM $db_table WHERE sub_cat_of = '$myo->id';");
				while ($myo_s = mfo($res_s)) {

					//Add to deletion array
					$all_ids[] = $myo->id;
				}
			}

			//Make arrays unique
			$all_ids 	= array_unique($all_ids);
			$scs 		= array_unique($scs);

			//Delete the categories
			delRec($db_table, $all_ids);

			//Fix the sort orders
			foreach ($scs as $sc) {
				fixWholeSortOrder($db_table,'category_order',"sub_cat_of='$sc'");
			}
		}
		cmsLog("$plural verwijderd.");
		$Message->set($msg_deleted);
	}
}

// *** Acivate
if (isset($_GET['activate']) and $id_holder = $_GET['activate'] and noBrowse()) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = $all_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			upRec($db_table,$db_ids,'active',1);
			cmsLog("Meerdere $plural_lc geactiveerd");
			$Message->set($msg_activated_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		upRec($db_table,$db_id,'active',1);
		$all_ids = array($db_id);
		cmsLog("$object (#$db_id) geactiveerd");
		$Message->set($msg_activated);
	}
}

// *** De-activate
if (isset($_GET['deactivate']) and $id_holder = $_GET['deactivate'] and noBrowse()) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = $all_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			upRec($db_table,$db_ids,'active',0);
			cmsLog("Meerdere $plural_lc op non-actief gezet.");
			$Message->set($msg_deactivated_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		upRec($db_table,$db_id,'active',0);
		$all_ids = array($db_id);
		cmsLog("$object (#$db_id) op non-actief gezet");
		$Message->set($msg_deactivated);
	}
}

// *** Move up
if (isset($_GET['move_up']) and $db_id = (int) $_GET['move_up'] and noBrowse()) {

	//Get some data
	$category	=	getMyo($db_table,$db_id,array('category_order','sub_cat_of'));
	$old_order	=	$category->category_order;
	$new_order 	= 	$old_order - 1;
	$other_id 	= 	getMyo($db_table, "category_order = '$new_order' AND sub_cat_of='$category->sub_cat_of'", 'id');

	//Flip
	if (!empty($other_id)) {
		upRec($db_table, $db_id,	'category_order', $new_order);
		upRec($db_table, $other_id,	'category_order', $old_order);
	}
}

// *** Move down
if (isset($_GET['move_down']) and $db_id = (int) $_GET['move_down'] and noBrowse()) {

	//Get some data
	$category	=	getMyo($db_table,$db_id,array('category_order','sub_cat_of'));
	$old_order	=	$category->category_order;
	$new_order 	= 	$old_order + 1;
	$other_id 	= 	getMyo($db_table, "category_order = '$new_order' AND sub_cat_of='$category->sub_cat_of'", 'id');

	//Flip
	if (!empty($other_id)) {
		upRec($db_table, $db_id,	'category_order', $new_order);
		upRec($db_table, $other_id,	'category_order', $old_order);
	}
}

/******************************************************************************************
 * Properties management
 ***/

if (isset($_POST['props_finish']) and isset($_POST['id']) and $db_id = (int) $_POST['id']) {

	//Loop all post vars
	foreach ($_POST as $key => $value) {

		//Filter the relevant ones
		if (substr($key,0,9) == 'property_') {

			//Find the ID
			$prop_id = substr($key,9);

			//Clean the value
			evalAll($value);

			//New? Insert.
			if ($prop_id == 'new' and $value) {
				$new_order = 1 + getMyo($db_table_props, "category_id='$db_id'", 'prop_order');
				insRec($db_table_props,array('category_id','property','prop_order'),array($db_id,$value,$new_order));
			}

			//Update?
			elseif ($prop_id = (int) $prop_id) {
				upRec($db_table_props, $prop_id, 'property', $value);
			}
		}
	}

	//View props again
	$_GET['props'] = $db_id;
}

if (isset($_GET['del_prop']) and $prop_id = (int) $_GET['del_prop']) {
	swap($db_table, $db_table_props);
	$Message->setConfirm('Deze eigenschap zeker weten verwijderen?','del_prop',$prop_id,'property','',"props=".request('props'));
	swap($db_table, $db_table_props);
}

if (isset($_GET['del_prop_confirm']) and $prop_id = (int) $_GET['del_prop_confirm']) {
	delRec($db_table_props, $prop_id);
	delRec($db_table_prop_values, "property_id='$prop_id'");
}

/******************************************************************************************
 * Add / Edit / View options
 ***/

if (isset($_GET['add'])) {

	//Get categories for subcats
	$cats_array = array(0 => '');
	$res = eq("SELECT id,name FROM $db_table WHERE sub_cat_of='0' ORDER BY category_order ASC;");
	while ($myo = mfo($res)) {
		$cats_array[$myo->id] = $myo->name;
		$res_s = eq("SELECT id,name FROM $db_table WHERE sub_cat_of='$myo->id' ORDER BY category_order ASC;");
		while ($myo_s = mfo($res_s)) {
			$cats_array[$myo_s->id] = ' - '.$myo_s->name;
		}
	}

	//FCK field
	$oFCKeditor 			= new FCKeditor('description') ;
	$oFCKeditor->BasePath	= CMS_PATH_PREFIX.'apps/fckeditor/';
	$oFCKeditor->Value		= '';
	$oFCKeditor->Width  	= '500px';
	$oFCKeditor->Height		= '300px';
	$oFCKeditor->ToolbarSet = 'Basic';
	$oFCKeditor->Lang		= $_lang;

	//FCK field
	$dFCKeditor 			= new FCKeditor('description_overview') ;
	$dFCKeditor->BasePath	= CMS_PATH_PREFIX.'apps/fckeditor/';
	$dFCKeditor->Value		= '';
	$dFCKeditor->Width  	= '500px';
	$dFCKeditor->Height		= '300px';
	$dFCKeditor->ToolbarSet = 'Basic';
	$dFCKeditor->Lang		= $_lang;

	//Manuals
	$MANUALS_ARRAY 			= 	array('')+makeArray(DIGI_DB_PREFIX.'pages_NL','id','title',"category_id='$manuals_cat'",'title','ASC');

	$pre_html = "
    	<h3>$object aanmaken:</h3><hr/><br/>
        <form name='add_form' method='post' action='$_file' enctype='multipart/form-data'>
        <table cellspacing='0px' cellpadding='1px' border='0px'>

		<tr>
			<td width='150px'>Naam:</td>
			<td><input type='text' name='name' class='input_regular' /></td>
		</tr>
		<tr>
			<td>Menu naam:</td>
			<td><input type='text' name='menu_name' class='input_regular' /></td>
		</tr>
		<tr>
			<td>Sub-categorie van:</td>
			<td>".makeSelectbox('sub_cat_of',$cats_array,0)."</td>
		</tr>
		<tr>
			<td>Prijs eenheid:</td>
			<td><input type='text' name='price_unit' class='input_regular' /></td>
		</tr>
		<tr>
			<td>Montagehandleiding:</td>
			<td>".makeSelectBox('manual',$MANUALS_ARRAY,0)."</td>
		</tr>
		<tr>
			<td>Standaard levertijd:</td>
			<td><input type='text' name='delivery_days' value='3' class='input_small'/> dagen</td>
		</tr>

		".($prod_cat_use_gfx ? "
        <tr>
			<td><br/>Afbeelding:</td>
			<td><br/><input type='file' name='image' class='input_file'/></td>
		</tr>
        " : '')."

		<tr><td valign='top'><br/>Omschrijving:</td><td><br/>
			".$oFCKeditor->CreateHtml()."
		</td></tr>
		<tr><td valign='top'><br/>Omschrijving overzicht:</td><td><br/>
			".$dFCKeditor->CreateHtml()."
		</td></tr>

        <tr>
			<td colspan='2'><br/><input type='submit' name='add_finish' value='Aanmaken' /></td>
		</tr>

        </table>
        </form><br/>
    ";
}

if (isset($_GET['edit']) and $db_id = (int) $_GET['edit']) {

	//Get categories for subcats
	$cats_array = array(0 => '');
	$res = eq("SELECT id,name FROM $db_table WHERE sub_cat_of='0' ORDER BY category_order ASC;");
	while ($myo = mfo($res)) {
		$cats_array[$myo->id] = $myo->name;
		$res_s = eq("SELECT id,name FROM $db_table WHERE sub_cat_of='$myo->id' ORDER BY category_order ASC;");
		while ($myo_s = mfo($res_s)) {
			$cats_array[$myo_s->id] = ' - '.$myo_s->name;
		}
	}

	//Unset current
	if (isset($cats_array[$db_id])) {
		unset($cats_array[$db_id]);
	}

	//Get category properties
	$res = eq("SELECT * FROM $db_table WHERE id = '$db_id';");
	$myo = mfo($res);

	//Using image?
	if ($prod_cat_use_gfx) {
		$prod_cat_image = $gfx_folder.$_sep.$gfx_prefix.$db_id.'.jpg';
		if (file_exists($prod_cat_image)) {
			$prod_cat_image = "<img src='$prod_cat_image' style='margin-bottom: 10px;' />";
		}
		else {
			$prod_cat_image = 'geen';
		}
	}

	//FCK field
	$oFCKeditor 			= new FCKeditor('description') ;
	$oFCKeditor->BasePath	= CMS_PATH_PREFIX.'apps/fckeditor/';
	$oFCKeditor->Value		= $myo->description;
	$oFCKeditor->Width  	= '500px';
	$oFCKeditor->Height		= '300px';
	$oFCKeditor->ToolbarSet = 'Basic';
	$oFCKeditor->Lang		= $_lang;

	//FCK field
	$dFCKeditor 			= new FCKeditor('description_overview') ;
	$dFCKeditor->BasePath	= CMS_PATH_PREFIX.'apps/fckeditor/';
	$dFCKeditor->Value		= $myo->description_overview;
	$dFCKeditor->Width  	= '500px';
	$dFCKeditor->Height		= '300px';
	$dFCKeditor->ToolbarSet = 'Basic';
	$dFCKeditor->Lang		= $_lang;

	//Manuals
	$MANUALS_ARRAY 			= 	array('')+makeArray(DIGI_DB_PREFIX.'pages_NL','id','title',"category_id='$manuals_cat'",'title','ASC');

	$pre_html = "
    	<h3>$object gegevens bijwerken:</h3><hr/><br/>
        <form name='edit_form' method='post' action='$_file' enctype='multipart/form-data'>
        <input type='hidden' name='id' value='$db_id' />
        <table cellspacing='0px' cellpadding='1px' border='0px'>

		<tr>
			<td width='150px'>Naam:</td>
			<td><input type='text' name='name' class='input_regular' value='$myo->name'/></td>
		</tr>
		<tr>
			<td>Menu naam:</td>
			<td><input type='text' name='menu_name' class='input_regular' value='$myo->menu_name'/></td>
		</tr>
		<tr>
			<td>Sub-categorie van:</td>
			<td>".makeSelectbox('sub_cat_of',$cats_array,$myo->sub_cat_of)."</td>
		</tr>
		<tr>
			<td>Prijs eenheid:</td>
			<td><input type='text' name='price_unit' class='input_regular' value='$myo->price_unit' /></td>
		</tr>
		<tr>
			<td>Montagehandleiding:</td>
			<td>".makeSelectBox('manual',$MANUALS_ARRAY,$myo->manual)."</td>
		</tr>
		<tr>
			<td>Standaard levertijd:</td>
			<td><input type='text' name='delivery_days' value='$myo->delivery_days' class='input_small'/> dagen</td>
		</tr>

        ".($prod_cat_use_gfx ? "
        <tr>
			<td valign='top'><br/>Afbeelding:</td>
			<td><br/>$prod_cat_image</td>
		</tr>
        <tr>
			<td>Nieuwe afbeelding:</td>
			<td><input type='file' name='image' class='input_file'/></td>
		</tr>
        " : '')."

		<tr><td valign='top'><br/>Omschrijving:</td><td><br/>
			".$oFCKeditor->CreateHtml()."
		</td></tr>
		<tr><td valign='top'><br/>Omschrijving overzicht:</td><td><br/>
			".$dFCKeditor->CreateHtml()."
		</td></tr>

        <tr>
			<td colspan='2'><br/><input type='submit' name='edit_finish' value='Opslaan' /></td>
		</tr>

        </table>
        </form><br/>
    ";
}

if (isset($_GET['view']) and $db_id = (int) $_GET['view']) {

	//Using image?
	if ($prod_cat_use_gfx) {
		$prod_cat_image = $gfx_folder.$_sep.$gfx_prefix.$db_id.'.jpg';
		if (file_exists($prod_cat_image)) {
			$prod_cat_image = "<img src='$prod_cat_image' />";
		}
		else {
			$prod_cat_image = 'geen';
		}
	}

	//Get category properties
	$res = eq("SELECT * FROM $db_table WHERE id = '$db_id';");
	$myo = mfo($res);

	//Manual
	$manual = getMyo(DIGI_DB_PREFIX.'pages_NL', $myo->manual, array('title','html_name'));

	$pre_html = "
    	<h3>Categorie gegevens inzien:</h3><hr/><br/>
        <table cellspacing='0px' cellpadding='1px' border='0px'>
		<tr>
			<td width='150px'>Naam:</td>
			<td>$myo->name</td>
		</tr>
		<tr>
			<td>Menu naam:</td>
			<td>".($myo->menu_name ? $myo->menu_name : '-')."</td>
		</tr>
		<tr>
			<td>Sub-categorie van:</td>
			<td>".getMyo($db_table,$myo->sub_cat_of,'name')."</td>
		</tr>
		<tr>
			<td>Prijs eenheid:</td>
			<td>$myo->price_unit</td>
		</tr>
		<tr>
			<td>Montagehandleiding:</td>
			<td>".($manual ?
			"<a href='../$manual->html_name' target='_blank' title='Openen in nieuw venster'>$manual->title</a>" : 'geen')."</td>
		</tr>
		<tr>
			<td>Standaard levertijd:</td>
			<td>$myo->delivery_days dagen</td>
		</tr>

        ".($prod_cat_use_gfx ? "
        <tr>
			<td valign='top'><br/>Afbeelding:</td>
			<td><br/>$prod_cat_image</td>
		</tr>
        " : '')."

		<tr>
			<td valign='top'><br/>Omschrijving:</td>
			<td width='500px'><br/>".str_replace("<ul>","<ul style='margin-left: 20px;'>",$myo->description)."</td>
		</tr>
        </table><br/><br/>
    ";
}

if (isset($_GET['props']) and $db_id = (int) $_GET['props']) {

	//Read existing props
	$props = array();
	$p = 0;
	$res = eq("SELECT id,property FROM $db_table_props WHERE category_id = '$db_id' ORDER BY prop_order ASC;");
	while ($myo = mfo($res)) {
		$p++;
		$props[] = "
			<tr>
				<td>Eigenschap $p:</td>
				<td><input type='text' name='property_$myo->id' value='$myo->property' class='input_regular' />
				<a href='$_file?del_prop=$myo->id&props=$db_id' title='Eigenschap verwijderen'>$img[del]</a></td>
			</tr>
		";
	}

	$pre_html = "
    	<h3>Product eigenschappen voor ".getMyo($db_table,$db_id,'name').":</h3><hr/><br/>
    	<form name='props_form' method='post' action='$_file'>
    	<input type='hidden' name='id' value='$db_id' />
        <table cellspacing='0px' cellpadding='1px' border='0px'>
		".implode("\n", $props)."
		<tr>
			<td width='150px'><br/>Nieuwe eigenschap:</td>
			<td><br/><input type='text' name='property_new' class='input_regular' /></td>
		</tr>
		<tr>
			<td colspan='2'><br/><input type='submit' name='props_finish' value='Opslaan' /></td>
		</tr>
        </table>
		</form><br/>
    ";
}

/******************************************************************************************
 * Page contents
 ***/

//Page title HTML
$html = "
	<a href='$_file'><img src='$site_icon' border='0px' class='cms_icon'/></a>
	<h6>
		<a href='$_file'>overzicht</a> &nbsp;
		$img[folder_add] <a href='$_file?add'>nieuw$adjective_e $object_lc</a>
	</h6>
	<br/>".$Message->get()."
	<br/><br/>$pre_html
";

$res = eq("	SELECT 		id, name, active, sub_cat_of
			FROM 		$db_table
			ORDER BY	category_order ASC
		;");
$data_rows = $main_categories = $sub_categories = array();

while ($myo = mfo($res)) {
	if ($myo->sub_cat_of) {
		if (!isset($sub_categories[$myo->sub_cat_of])) {
			$sub_categories[$myo->sub_cat_of] = array();
		}
		$sub_categories[$myo->sub_cat_of][] = $myo;
	}
	else {
		$main_categories[] = $myo;
	}
}

foreach ($main_categories as $main_cat) {

	$data_rows[] = array(

		//This MUST be the id
		$main_cat->id,

		//All desired other fields
		$main_cat->id,
		"<a href='$_file?view=$main_cat->id&$_app' title='$object gegevens inzien'><b>$main_cat->name</b></a>",
		$main_cat->active ? "<a href='$_file?deactivate=$main_cat->id&$_app' title='$object de-activeren'>$img[ok]</a>" :
							"<a href='$_file?activate=$main_cat->id&$_app' title='$object activeren'>$img[notok]</a>",
		"<a href='$_file?move_up=$main_cat->id&$_app' title='$object omhoog verplaatsen'>$img[up]</a>
		 <a href='$_file?move_down=$main_cat->id&$_app' title='$object omlaag verplaatsen'>$img[down]</a>",
	 	"
	 	 <a href='$_file?props=$main_cat->id&$_app' title='Product eigenschappen bijwerken'>$img[product]</a>
		 <a href='$_file?edit=$main_cat->id&$_app' title='$object gegevens bijwerken'>$img[edit]</a>
		 <a href='$_file?delete=$main_cat->id&$_app' title='$object verwijderen'>$img[folder_del]</a>
		"
	);

	if (isset($sub_categories[$main_cat->id]) and count($sub_categories[$main_cat->id])) {
		foreach ($sub_categories[$main_cat->id] as $sub_cat) {

			$data_rows[] = array(

				//This MUST be the id
				$sub_cat->id,

				//All desired other fields
				$sub_cat->id,
				"<img src='../gfx/cms/system/products.gif' /> <a href='$_file?view=$sub_cat->id&$_app' title='$object gegevens inzien'>$sub_cat->name</a>",
				$sub_cat->active ? 	"<a href='$_file?deactivate=$sub_cat->id&$_app' title='$object de-activeren'>$img[ok]</a>" :
									"<a href='$_file?activate=$sub_cat->id&$_app' title='$object activeren'>$img[notok]</a>",
				"<a href='$_file?move_up=$sub_cat->id&$_app' title='$object omhoog verplaatsen'>$img[up]</a>
				 <a href='$_file?move_down=$sub_cat->id&$_app' title='$object omlaag verplaatsen'>$img[down]</a>",
	 	"
				 <a href='$_file?props=$sub_cat->id&$_app' title='Product eigenschappen bijwerken'>$img[product]</a>
				 <a href='$_file?edit=$sub_cat->id&$_app' title='$object gegevens bijwerken'>$img[edit]</a>
				 <a href='$_file?delete=$sub_cat->id&$_app' title='$object verwijderen'>$img[folder_del]</a>
				"
			);

			//And another level of sub categories
			if (isset($sub_categories[$sub_cat->id]) and count($sub_categories[$sub_cat->id])) {
				foreach ($sub_categories[$sub_cat->id] as $sub_sub_cat) {

					$data_rows[] = array(

						//This MUST be the id
						$sub_sub_cat->id,

						//All desired other fields
						$sub_sub_cat->id,
						"&nbsp; &nbsp; <img src='../gfx/cms/system/products.gif' /> <a href='$_file?view=$sub_sub_cat->id&$_app' title='$object gegevens inzien'>$sub_sub_cat->name</a>",
						$sub_cat->active ? 	"<a href='$_file?deactivate=$sub_sub_cat->id&$_app' title='$object de-activeren'>$img[ok]</a>" :
											"<a href='$_file?activate=$sub_sub_cat->id&$_app' title='$object activeren'>$img[notok]</a>",
						"<a href='$_file?move_up=$sub_sub_cat->id&$_app' title='$object omhoog verplaatsen'>$img[up]</a>
						 <a href='$_file?move_down=$sub_sub_cat->id&$_app' title='$object omlaag verplaatsen'>$img[down]</a>",
	 	"
						 <a href='$_file?props=$sub_sub_cat->id&$_app' title='Product eigenschappen bijwerken'>$img[product]</a>
						 <a href='$_file?edit=$sub_sub_cat->id&$_app' title='$object gegevens bijwerken'>$img[edit]</a>
						 <a href='$_file?delete=$sub_sub_cat->id&$_app' title='$object verwijderen'>$img[folder_del]</a>
						"
					);
				}
			}
		}
	}
}

//Output data table
$DataTable = new DataTable();
$DataTable->setMarkAllOptions(	array('verwijder','delete',$plural.' verwijderen'),
								array('activeer','activate',$plural.' activeren'),
								array('de-activeer','deactivate',$plural.' de-activeren'));

$DataTable->setDataRows($data_rows);
$DataTable->setLegendRow(
	array('#','Naam','Actief','Volgorde','Opties'),
	array(60,0,60,80,60),
	array()
);
echo $html.$DataTable->getHTML();

require_once('inc/cms_footer.php');
?>