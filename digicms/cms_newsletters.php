<?
/*
Title:		Newsletters manager
File: 		cms_newsletter.php
Version: 	v2.20
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

/******************************************************************************************
 * Settings
 ***/

// *** Required files
require_once('inc/cms_ajax_header.php');
require_once(CMS_PATH_PREFIX.'inc/functions/explode_recipients.php');
require_once(CMS_PATH_PREFIX.'classes/class_Newsletter.php');

// *** All the subscribers tables
$subscriber_tables		=	explode(';',$cfg['NL_DB_TABLES']);
$subscribers			=	array();

foreach ($subscriber_tables as $subscr_table) {

	//Database table, email field, newsletter yes or no field, language field
	$subscribers[]	=	array(DIGI_DB_PREFIX.trim($subscr_table), 'email','newsletter','language');
}

// *** General settings
$date_format			=	$cfg['NL_DATE_FORMAT'];
$from_email				=	$cfg['NL_FROM_EMAIL'];
$test_addresses			=	$cfg['NL_TEST_ADDRESSES'];
$sign_off_url			=	$_link.$cfg['NL_SIGN_OFF_URL'];

// *** Newsletter example
if ($db_id = (int) request('example')) {

	$Newsletter = new Newsletter($db_id);
	echo $Newsletter->getHTML();
	exit;
}

// *** Regular CMS page
require_once('inc/cms_header.php');
require_once(CMS_PATH_PREFIX.'apps/fckeditor/fckeditor.php');
require_once(CMS_PATH_PREFIX.'classes/class_DataTable.php');
require_once(CMS_PATH_PREFIX.'inc/sendmail.php');

// *** Table definitions
$db_table_m2g			=	DIGI_DB_PREFIX.'mails2go';

// *** Object definition
$object					=	'Nieuwsbrief';
$object_lc 				= 	'nieuwsbrief';
$plural					=	'Nieuwsbrieven';
$plural_lc				=	'nieuwsbrieven';
$adjective_e			=	'e';
$ident					=	'Deze';

// *** Sorting and pages functionality
$default_sort_option	=	'edition';
$default_sort_method	=	'DESC';
$secondary_sort_option	=	'language';
$secondary_sort_method	=	'ASC';
$valid_sort_options		=	array('edition','language','timestamp','subject','subscribers_sent','times_opened');

// *** Search constraints
if (!empty($search_query)) {
	$search				=	array();
	$search[]			=	"subject LIKE '%$search_query%'";
	$search[]			=	"message LIKE '%$search_query%'";
	$where[]			=	'('.implode(' OR ', $search).')';
}

// *** Messages
$msg_saved				=	'Nieuwsbrief opgeslagen.';
$msg_test_sent			=	'Test nieuwsbrief is verzonden naar: ';
$msg_send_confirm		=	'Deze nieuwsbrief zeker weten versturen?';
$msg_queued				=	'Nieuwsbrief is aangeboden aan het mail-systeem.';
$msg_no_recipients		=	'Geen aanmeldingen gevonden. Nieuwsbrief is niet verstuurd.';
$msg_sending_limits		=	'Er zullen maximaal '.$cfg['MAX_PER_BATCH'].' mailtjes per '.($mins = round($cfg['BATCH_INTERVAL']/60)).
							(($mins == 1) ? ' minuut' : ' minuten').' worden verstuurd.';
$msg_deleted			=	'De nieuwsbrief is verwijderd.';
$msg_deleted_m			=	'De nieuwsbrieven zijn verwijderd.';
$msg_delete_confirm		=	'Deze nieuwsbrief zeker weten verwijderen?';
$msg_delete_confirm_m	=	'Deze nieuwsbrieven zeker weten verwijderen?';
$msg_send_test_to		=	'Waar wilt u de test nieuwsbrief naar toe versturen?<br/>Meerdere adressen scheiden met een puntkomma.';
$msg_no_test_addresses	=	'Geen email adressen opgegeven. De test nieuwsbrief wordt niet verstuurd.';
$msg_already_sent		=	'Deze nieuwsbrief is al aangeboden aan het mail systeem.';

// *** Sorting and pages and link appendix
require_once(CMS_PATH_PREFIX.'inc/sorting_and_pages.php');
$_app					=	$page_appendix.'&'.$sort_appendix.'&'.$link_appendix;

/******************************************************************************************
 * POST and GET Cases
 ***/

// *** Test
if (isset($_GET['test']) and $db_id = (int) $_GET['test']) {
	$Message->setConfirmText($msg_send_test_to, 'test', $db_id, '',$test_addresses);
}

// *** Test confirmation
if (isset($_POST['confirm_test']) and $db_id = (int) $_POST['confirm_test']) {
	if (empty($_POST['text_value'])) {
		$Message->set($msg_no_test_addresses, WARNING);
	}
	$test_addresses = $_POST['text_value'];
	$Newsletter = new Newsletter($db_id, true);
	switch($Newsletter->send()) {

		case NO_RECIPIENTS_FOUND:
			$Message->set($msg_no_recipients, CRITICAL);
			break;
		case NL_TEST_SENT:
			$Message->set($msg_test_sent.$test_addresses);
			break;
		case ALREADY_SENT:
			$Message->set($msg_already_sent);
			break;
	}
}

// *** Send confirmation
if (isset($_GET['send']) and $db_id = (int) $_GET['send']) {
	$Message->setConfirm($msg_send_confirm,'send',$db_id,'subject');
}

// *** Send newsletter
if (isset($_GET['send_confirm']) and $db_id = (int) $_GET['send_confirm'] and noBrowse()) {

 	if (!isset($test)) $test = 0;

 	$Newsletter = new Newsletter($db_id, $test);
 	$Newsletter->setSubscribers($subscribers);

	switch($Newsletter->send()) {

		case NO_RECIPIENTS_FOUND:
			$Message->set($msg_no_recipients, CRITICAL);
			break;
		case NL_TEST_SENT:
			$Message->set($msg_test_sent.$test_addresses);
			break;
		case NL_REAL_SENT:
			cmsLog("Nieuwsbrief (#$db_id) verstuurd.");
			$Message->set($msg_queued.' '.$msg_sending_limits);
			break;
	}
}

// *** Save newsletter
if (isset($_POST['save'])) {

	$db_id = isset($_POST['id']) ? (int) $_POST['id'] : 0;

 	$Newsletter = new Newsletter($db_id);

 	if ($db_id = $Newsletter->saveToDB()) {
		cmsLog("Nieuwsbrief $_POST[subject] (#$db_id) opgeslagen.");
		$Message->set($msg_saved);
	}
}

// *** Delete confirmation
if (isset($_GET['delete']) and $id_holder = $_GET['delete']) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			$Message->setConfirmMultiple($msg_delete_confirm_m, 'delete',$db_ids,'subject');
		}
	}
	else if ($db_id = (int) $id_holder) {
		$Message->setConfirm($msg_delete_confirm,'delete',$db_id,'subject');
	}
}

// *** Delete
if (isset($_GET['delete_confirm']) and $id_holder = $_GET['delete_confirm'] and noBrowse()) {

	if (isset($_GET['multiple_confirm']) and is_array($id_holder)) {

		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			delRec($db_table, $db_ids);
			$Message->set($msg_deleted_m);
			cmsLog("Meerdere $plural_lc verwijderd.");
		}
	}
	else if ($db_id = (int) $id_holder) {
		delRec($db_table, $db_id);
		$Message->set($msg_deleted);
		cmsLog("$object (#$db_id) verwijderd.");
	}
}

/******************************************************************************************
 * Add / edit options
 ***/

if (isset($_GET['add'])) {

	//Get last newsletter data
	$res = eq("SELECT subject, message FROM $db_table ORDER BY id DESC LIMIT 1;");
	$myo = mfo($res);

	$subject = isset($myo->subject) ? $myo->subject : '';
	$message = isset($myo->message) ? $myo->message : '';

	//FCK editor configuration
	$oFCKeditor 			= new FCKeditor('message') ;
	$oFCKeditor->BasePath	= CMS_PATH_PREFIX.'apps/fckeditor/';
	$oFCKeditor->Value		= $message;
	$oFCKeditor->Width  	= '700px';
	$oFCKeditor->Height		= '600px';
	$oFCKeditor->ToolbarSet = 'DigiMailer';

	$pre_html = "
			<h3>Nieuwe nieuwsbrief aanmaken:</h3><hr/><br/>
			<form name='newsletter_form' method='post' action='$_file'>
			<table border='0px' cellspacing='0px' cellpadding='2px'>
			<tr>
				<td width='100px'>Taal:</td>
				<td>".makeSelectBox('language',$installed_languages,$_lang)."</td>
			</tr>
			<tr>
				<td valign='top'>Onderwerp:</td>
				<td><input type='text' name='subject' class='input_regular' value='$subject'/></td>
			</tr>
			<tr>
				<td valign='top'><br/>Inhoud:</td>
				<td><br/>".$oFCKeditor->CreateHtml()."</td>
			</tr>
			<tr>
				<td>&nbsp;</td><td><br/><input type='submit' name='save' value='Opslaan'/></td>
			</tr>
			</table>
			</form><br/>
	";
}


if (isset($_GET['edit']) and $db_id = (int) $_GET['edit']) {

	//Get last newsletter data
	$res = eq("SELECT edition,timestamp,language,subject,message FROM $db_table WHERE id = '$db_id';");
	$myo = mfo($res);

	//FCK editor configuration
	$oFCKeditor 			= new FCKeditor('message') ;
	$oFCKeditor->BasePath	= CMS_PATH_PREFIX.'apps/fckeditor/';
	$oFCKeditor->Value		= $myo->message;
	$oFCKeditor->Width  	= '700px';
	$oFCKeditor->Height		= '600px';
	$oFCKeditor->ToolbarSet = 'DigiMailer';

	if ($myo->timestamp == '0000-00-00 00:00:00') {
		$date = date($date_format);
	}
	else {
		$date = date($date_format, unix_time($myo->timestamp));
	}

	$pre_html = "
			<h3>Nieuwsbrief bewerken:</h3><hr/><br/>
			<form name='newsletter_form' method='post' action='$_file'>
			<input type='hidden' name='id' value='$db_id' />
			$page_and_sort_inputs
			<table border='0px' cellspacing='0px' cellpadding='2px'>
			<tr>
				<td width='100px'>Editie:</td>
				<td><b>Nr. ".($myo->edition)."</b> (".$installed_languages[$myo->language].", $date)</td>
			</tr>
			<tr>
				<td valign='top'><br/>Onderwerp:</td>
				<td><br/><input type='text' name='subject' class='input_regular'  value='$myo->subject'/></td>
			</tr>
			<tr>
				<td valign='top'><br/>Bericht:</td>
				<td><br/>".$oFCKeditor->CreateHtml()."</td>
			</tr>
			<tr>
				<td>&nbsp;</td><td><br/><input type='submit' name='save' value='Opslaan'/></td>
			</tr>
			</table>
			</form><br/>
	";
}

if (isset($_GET['recipients_summary'])) {

	$Newsletter = new Newsletter(0);
 	$Newsletter->setSubscribers($subscribers);
	$recipients = $Newsletter->howManyRecipients();

	$summary = array();
	foreach ($recipients as $table => $amount) {

		$table = substr($table,5);
		$style = ($table == 'total') ? " style='font-weight: bold;'" : '';

		switch ($table) {
			case 'total':
				$table = 'Totaal';
				break;
			default:
				$table = ucfirst($table);
		}

		$summary[] = "<tr><td width='200px'$style>$table:</td><td$style>$amount</td></tr>";
	}

	$pre_html = "
			<h3>Ontvangers van de nieuwsbrief:</h3><hr/><br/>

			<table border='0px' cellspacing='0px' cellpadding='2px'>
			".implode("\n",$summary)."
			</table>
			<br/><br/>
	";
}

/******************************************************************************************
 * Page contents
 ***/

//Page title HTML
$html = 	"
	<a href='$_file'><img src='$site_icon' border='0px' class='cms_icon'/></a>
	<h6>
		<a href='$_file'>overzicht</a> &nbsp;
		$img[mail_add] <a href='$_file?add'>nieuwe nieuwsbrief</a> &nbsp;
		$img[users] <a href='$_file?recipients_summary'>ontvangers</a>
	</h6>
	<br/>".$Message->get()."
	<br/><br/>$pre_html
";

//Get items for this page and this sorting method
$res = eq("	SELECT 		id, edition, language, subject, timestamp, subscribers_sent, sending_finished, times_opened
			FROM 		$db_table $where_clause
			ORDER BY 	$order_clause
			LIMIT 		$limit_clause
		;");
while ($myo = mfo($res)) {


	if ($myo->timestamp == '0000-00-00 00:00:00') {
		$sent = false;
		$date = 'nieuw';
	}
	else {
		$sent = true;
		$date = date($date_format, unix_time($myo->timestamp));
	}

	if ($myo->subscribers_sent) {
		if (!$myo->sending_finished and ($still_to_send = countResults($db_table_m2g, "newsletter_id = '$myo->id'")) > 0) {

			$sent		=	$myo->subscribers_sent - $still_to_send;
			$ratio 		= 	$sent / $myo->subscribers_sent;
			$width_1 	= 	round($ratio*100);
			$width_2	=	100 - $width_1;

			$progress	=	"<h6 class='progress_indicator'>&nbsp; $sent / $myo->subscribers_sent</h6><div class='green_indicator' style='width: {$width_1}px; border-right: 0px;'></div><div class='red_indicator' style='width: {$width_2}px; border-left: 0px;'></div>";
		}
		elseif (!$myo->sending_finished) {
			upRec($db_table, $myo->id, 'sending_finished', 1);
			$progress	=	"<div class='green_indicator'><h6>&nbsp; $myo->subscribers_sent</h6></div>";
		}
		else {
			$progress	=	"<div class='green_indicator'><h6>&nbsp; $myo->subscribers_sent</h6></div>";
		}
	}
	else {
		$progress	=	"<div class='red_indicator'><h6>&nbsp; 0</h6></div>";
	}

	$data_rows[] = array(

		//This MUST be the id
		$myo->id,

		//All desired other fields
		$myo->edition,
		"<a href='$_file?example=$myo->id' title='Voorbeeld bekijken' target='_blank'>$myo->subject</a>",
		$installed_languages[$myo->language],
		$date,
		$myo->times_opened.'x',
		$progress,

		//The options last
		"<a href='$_file?example=$myo->id' title='Voorbeeld bekijken' target='_blank'>$img[page_search]</a>
		 <a href='$_file?test=$myo->id&$_app' title='Test $object_lc versturen'>$img[mail_search]</a> ".(
		$sent ? $img['placeholder'] : "
		 <a href='$_file?send=$myo->id&$_app' title='$object versturen'>$img[mail_next]</a>")."
		 <a href='$_file?edit=$myo->id&$_app' title='$object bewerken'>$img[edit]</a>
		 <a href='$_file?delete=$myo->id&$_app' title='$object verwijderen'>$img[mail_del]</a>
		"
	);
}

//Output data table
$DataTable = new DataTable();
$DataTable->setMarkAllOptions(	array('verwijder','delete',$plural.' verwijderen'));
$DataTable->showSearch();
$DataTable->setDataRows($data_rows);
$DataTable->setLegendRow(
	array('Nr.','Onderwerp','Taal','Verstuurd','Geopend','Ontvangers','Opties'),
	array(60,0,50,100,90,120,100),
	array('edition','subject','language','timestamp','times_opened','subscribers_sent')
);
echo $html.$DataTable->getHTML();

require_once('inc/cms_footer.php');
?>