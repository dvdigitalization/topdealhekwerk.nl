<?php
/*
Title:		Discounts management file
File: 		cms_topdeal_discounts.php
Version: 	v2.20
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

/******************************************************************************************
 * Settings
 ***/

// *** Required files
require_once('inc/cms_header.php');
require_once(CMS_PATH_PREFIX.'classes/class_DataTable.php');

// *** Object definition
$object					=	'Staffelkorting';
$object_lc 				= 	'staffelkorting';
$plural					=	'Staffelkortingen';
$plural_lc				=	'staffelkortingen';
$adjective_e			=	'e';
$ident					=	'Deze';

// *** Sorting and pages functionality
$default_sort_option	=	'from_amount';
$default_sort_method	=	'ASC';
$secondary_sort_option	=	'';
$secondary_sort_method	=	'';
$valid_sort_options		=	array('id','from_amount','till_amount');

// *** Message definitions
$msg_added 				= 	'Staffelkorting toegevoegd.';
$msg_editted 			= 	'Staffelkorting aangepast.';
$msg_deleted			=	'Staffelkorting verwijderd.';
$msg_deleted_m 			= 	'Staffelkortingen verwijderd.';
$msg_delete_confirm		= 	'Deze staffelkorting zeker weten verwijderen?';
$msg_delete_confirm_m 	= 	'Deze staffelkortingen zeker weten verwijderen?';

// *** Sorting and pages and link appendix
require_once(CMS_PATH_PREFIX.'inc/sorting_and_pages.php');
$_app					=	$page_appendix.'&'.$sort_appendix.'&'.$link_appendix;

/******************************************************************************************
 * POST and GET Cases
 ***/

if (isset($_POST['add_finish']) or isset($_POST['edit_finish'])) {
	foreach(array_keys($_POST) as $key) {
		evalAll($_POST[$key]);
	}

	$_POST['from_amount'] 	= 	dbAmount($_POST['from_amount']);
	$_POST['till_amount'] 	= 	dbAmount($_POST['till_amount']);
	$_POST['fixed_price'] 	= 	dbAmount($_POST['fixed_price']);
	$_POST['multiplier']	=	validateFloat($_POST['multiplier']);
}

if (isset($_POST['add_finish'])) {

	$db_id = insRec(	$db_table,
           		array(	'from_amount','till_amount','fixed_price','multiplier'),
            	array(	$_POST['from_amount'],$_POST['till_amount'],$_POST['fixed_price'],$_POST['multiplier'])
	);

    $Message->set($msg_added);

	cmsLog("$object (#$db_id) toegevoegd.");
}

if (isset($_POST['edit_finish']) and $db_id = (int) $_POST['id']) {

	upRec(	$db_table, $db_id,
           		array(	'from_amount','till_amount','fixed_price','multiplier'),
            	array(	$_POST['from_amount'],$_POST['till_amount'],$_POST['fixed_price'],$_POST['multiplier'])
	);

	$Message->set($msg_editted);

	cmsLog("$object (#$db_id) aangepast.");
}

// *** Delete confirmation
if (isset($_GET['delete']) and $id_holder = $_GET['delete']) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			$Message->setConfirmMultiple($msg_delete_confirm_m, 'delete',$db_ids,'from_amount');
		}
	}
	else if ($db_id = (int) $id_holder) {
		$Message->setConfirm($msg_delete_confirm,'delete',$db_id,'from_amount');
	}
}

// *** Delete action
if (isset($_GET['delete_confirm']) and $id_holder = $_GET['delete_confirm'] and noBrowse()) {

	if (isset($_GET['multiple_confirm']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			delRec($db_table, $db_ids);
			cmsLog("Meerdere $plural_lc permanent verwijderd.");
			$Message->set($msg_deleted_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		delRec($db_table, $db_id);
		cmsLog("$object (#$db_id) verwijderd.");
		$Message->set($msg_deleted);
	}
}

/******************************************************************************************
 * Add / Edit / View options
 ***/

if (isset($_GET['add'])) {

	$pre_html = "
		<h3>Nieuwe $object_lc toevoegen:</h3><hr/><br/>
        <form name='add_form' method='post' action='$_file'>
        $page_and_sort_inputs
        <table cellspacing='0px' cellpadding='1px' border='0px'>

        <tr>
			<td width='150px'>Van bedrag:</td>
			<td><input type='text' name='from_amount' class='input_small' $js[NaN] /> euro</td>
		</tr>
        <tr>
			<td>Tot bedrag:</td>
			<td><input type='text' name='till_amount' class='input_small' $js[NaN] /> euro</td>
		</tr>
  		<tr>
			<td>Vast:</td>
			<td><input type='text' name='fixed_price' class='input_small' $js[NaN] /> euro</td>
		</tr>
		<tr>
			<td>Verm. factor:</td>
			<td><input type='text' name='multiplier' class='input_small' $js[NaN] /> x</td>
		</tr>

	    <tr>
			<td colspan='2'><br/><input type='submit' name='add_finish' value='Toevoegen' /></td>
		</tr>
        </table>
        </form><br/>
	";
}

if (isset($_GET['edit']) and $db_id = (int) $_GET['edit']) {

	$res = eq("SELECT * FROM $db_table WHERE id = '$db_id';");
	$myo = mfo($res);

	$pre_html = "
		<h3>$object gegevens aanpassen:</h3><hr/><br/>
        <form name='edit_form' method='post' action='$_file'>
        <input type='hidden' name='id' value='$db_id' />
        $page_and_sort_inputs
        <table cellspacing='0px' cellpadding='1px' border='0px'>

        <tr>
			<td width='150px'>Van bedrag:</td>
			<td><input type='text' name='from_amount' class='input_small' value='".parseAmount($myo->from_amount)."' $js[NaN] /> euro</td>
		</tr>
        <tr>
			<td>Tot bedrag:</td>
			<td><input type='text' name='till_amount' class='input_small' value='".parseAmount($myo->till_amount)."' $js[NaN] /> euro</td>
		</tr>
  		<tr>
			<td>Vast:</td>
			<td><input type='text' name='fixed_price' class='input_small' value='".parseAmount($myo->fixed_price)."' $js[NaN] /> euro</td>
		</tr>
		<tr>
			<td>Verm. factor:</td>
			<td><input type='text' name='multiplier' class='input_small' value='".$myo->multiplier."' $js[NaN] /> x</td>
		</tr>

	    <tr>
			<td colspan='2'><br/><input type='submit' name='edit_finish' value='Opslaan' /></td>
		</tr>
        </table>
        </form><br/>
	";
}

/******************************************************************************************
 * Page contents
 ***/

//Page title HTML
$html = "
	<a href='$_file'><img src='$site_icon' border='0px' class='cms_icon'/></a>
	<h6>
		<a href='$_file'>overzicht</a> &nbsp;
		$img[invoice_add] <a href='$_file?add=1'>nieuwe staffelkorting</a>
	</h6>
	<br/>".$Message->get()."
	<br/><br/>$pre_html
";

//Get items for this page and this sorting method
$data_rows = array();
$res = eq("	SELECT 		id,from_amount,till_amount, fixed_price, multiplier
			FROM 		$db_table $where_clause
			ORDER BY 	$order_clause
			LIMIT 		$limit_clause
;");

while ($myo = mfo($res)) {

	$options = "<a href='$_file?edit=$myo->id&$_app' title='$object gegevens bijwerken'>$img[edit]</a> ";
	$options .= "<a href='$_file?delete=$myo->id&$_app' title='$object verwijderen'>$img[invoice_del]</a> ";

    $data_rows[] = array(

		//This MUST be the id
		$myo->id,

		//All desired other fields
		$myo->id,
		parseAmount($myo->from_amount,1),
		parseAmount($myo->till_amount,1),
		parseAmount($myo->fixed_price,1),
		$myo->multiplier.'x',

	    //The options last
		$options

	);
}

//Output data table
$DataTable = new DataTable();
$DataTable->setMarkAllOptions(	array('verwijder','delete',$plural.' verwijderen'));
$DataTable->setDataRows($data_rows);
$DataTable->setLegendRow(
	array('#','Van','Tot','Vast','Verm. factor','Opties'),
	array(60,0,160,160,160,40),
	array('id','from_amount','till_amount','fixed_price','multiplier','active')
);

echo $html.$DataTable->getHTML();

require_once('inc/cms_footer.php');
?>