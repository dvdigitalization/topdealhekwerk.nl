<?php
/*
Title:		Edit CMS/site settings, change your own login details.
File: 		cms_settings.php
Version: 	v2.03
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization	
*/

/******************************************************************************************
 * Settings
 ***/

// *** Required files
require_once('inc/cms_header.php');
require_once('inc/lang/'.$cms_lang.'_'.$_file);

// *** Extra database table definitions
$db_table_auth			=	DIGI_DB_PREFIX.'auth_users';

// *** Is digi?
$is_digi				=	(($s_id == 1 and $s_admin) or ($testmode and $s_admin)) ? true : false;

/******************************************************************************************
 * POST and GET Cases
 ***/

if (isset($_POST['change_user_finish']) and checkPriv(PRIV_CHANGE_USER) and $s_id > 1) {
    
    evalAll($_POST['username']);
    evalAll($_POST['pass1']);
    evalAll($_POST['pass2']);
    evalAll($_POST['first_name']);
    evalAll($_POST['last_name']);
    evalAll($_POST['email_address']);
			    
	if (($current_pass = dbSalt($_POST['pass0'])) != getMyo(DIGI_DB_PREFIX.'auth_users',$s_id,'password')) {
        $Message->set($msg_wrong_pass, CRITICAL);
    }
	elseif (!empty($_POST['pass1']) and !($_POST['pass1'] = validateSimpleString($_POST['pass1']))) {
		$Message->set($msg_invalid_chars_pass, WARNING);
	}	
    elseif ($_POST['pass1'] and $_POST['pass1'] != $_POST['pass2']) {
        $Message->set($msg_pass_dont_match, CRITICAL);
    }
    elseif (!$username = validateSimpleString($_POST['username'])) {
		$Message->set($msg_invalid_chars_user, CRITICAL);
	}
	elseif (!empty($_POST['email_address']) and !($_POST['email_address'] = validateEmail($_POST['email_address']))) {
		$Message->set($msg_invalid_email, WARNING);
	}
    else {
    	
    	//Set the password to save in the database, and if needed update validation session string
        if ($_POST['pass1']) {
			$pass = dbSalt($_POST['pass1']);
			$_SESSION['login_validation'] = sha1($pass.$sha1_appendix.'7H3ki');
		}
		else {
			$pass = $current_pass;
		}	
        
        //Mailer priv
        if (!isset($installed_modules[MOD_MAILER]) or !isset($_POST['uses_mailer']) or !checkPriv(PRIV_MAILER)) {
			$_POST['uses_mailer'] = 0;
		}
        
        $_SESSION['uses_mailer'] 	= 	$uses_mailer	=	(int)	$_POST['uses_mailer'];
        $_SESSION['login_user']		=	$_POST['first_name'].' '.$_POST['last_name'].' ('.$username.')';
        
        upRec(	$db_table_auth, $s_id,
				array('username','password','uses_mailer','first_name','last_name','email_address'),
				array($username,$pass,$uses_mailer,$_POST['first_name'],$_POST['last_name'],$_POST['email_address']));
				
		if (!$cfg['FIRST_LOGIN_PASS_CHANGED'] and $s_id == 2 and $_POST['pass1'] and $_POST['pass1'] != $_POST['pass0']) {
			changeSetting('FIRST_LOGIN_PASS_CHANGED', 1);
		}
        
		$Message->set($msg_data_modified);
		cmsLog(LOG_USER_SETTINGS_ALTERED." (#$s_id)");
   }
}

if (isset($_POST['change_settings_finish']) and checkPriv(PRIV_SETTINGS)) {
	
	foreach ($_POST as $setting => $value) {
		
		if (substr($setting,0,8) == 'setting_') {

		 	$setting 		= 	substr($setting,8);		 
		 	$type 			=	getMyo($db_table,"setting = '$setting'",array('type_int','type_yes_no'));
		 	
		 	if ($type->type_yes_no) {
				$value = $value == 1 ? 1 : 0;
			}
			if ($type->type_int) {
				$value = (int) $value;
			}
		 		 		 
		 	$cfg[$setting] 	= 	$value;
		 	
			evalAll($value);
			upRec($db_table,"setting = '$setting'",'setting_value',"$value");
		}
	}
	
	unset($setting);
	$Message->set($msg_settings_modified);
	cmsLog(LOG_SETTINGS_CHANGED);
}

if (isset($_POST['add_finish']) and $is_digi) {

	evalAll($_POST['setting_set']);	
	evalAll($_POST['setting_value']);
	evalAll($_POST['setting_descr']);
	evalAll($_POST['setting']);
	
	$setting = strtoupper(str_replace(' ','_',$_POST['setting']));
	
	//Dolf check
	if ($_POST['type_yes_no']) $_POST['type_int'] = 0;
	
	if (!isset($cfg[$setting])) {
		insRec($db_table,	array(	'setting','setting_set','setting_value','setting_descr',
									'digi_protected','type_yes_no','type_int'), 
							array(	$setting,$_POST['setting_set'],$_POST['setting_value'],$_POST['setting_descr'],
									$_POST['digi_protected'],$_POST['type_yes_no'],$_POST['type_int'])
		);
		$Message->set($msg_setting_added.' <b>$cfg[\''.$setting.'\']</b>');
		$cfg[$setting] = $_POST['setting_value'];
		
		unset($_POST['setting']);
		unset($_POST['setting_value']);
	}
	else {
		$Message->set($msg_setting_exists, WARNING);
	}
	$_GET['add'] = 1;
	unset($setting);
}

if (isset($_GET['delete']) and $db_id = (int) $_GET['delete'] and $is_digi) {
	$Message->setConfirm($msg_confirm_delete, 'delete', $db_id, 'setting', '', "rs=$random_string");
}

if (isset($_GET['delete_confirm']) and $db_id = (int) $_GET['delete_confirm'] and noBrowse() and $is_digi) {
	delRec($db_table, $db_id);
	$Message->set($msg_setting_deleted);
}

if (isset($_POST['add_sql_finish']) and !empty($_POST['query']) and $is_digi) {
	
	evalAll($_POST['query'], false);
	
	$queries 		= 	explode(';', $_POST['query']);
	$failed_queries = 	array();
	$queries_ok		=	0;
	
	foreach ($queries as $key => $query) {
		evalTrim($query);
		if (!empty($query)) {
			
			//Multi language query?
			if (substr($query,0,4) == '[ML]') { 
				$query 			= 	substr($query,4);
				$ml_queries_ok	=	0;

				foreach ($installed_languages as $lang) {
					$ml_query = str_replace('[ML]',$lang, $query);

					if (@eq($ml_query)) {
						$ml_queries_ok++;
					}
					else {
						$failed_queries[] = $ml_query.'<br/><br/>'.mysql_error();
					}
				}
				
				if ($ml_queries_ok == count($installed_languages)) {
					$queries_ok++;
				}
			}
			
			//Normal query
			else if (@eq($query)) {
				$queries_ok++;				
			}
			else {
				$failed_queries[] = $query.'<br/><br/>'.mysql_error();
			}
		}
		else {
			unset($queries[$key]);
		}
	}
	
	if ($queries_ok) {
		$Message->set($queries_ok.' '.($queries_ok == 1 ? TXT_QUERY : TXT_QUERIES).' '.TXT_PERFORMED.'.');
		if ($failed = count($failed_queries)) {
			$Message->add($failed.' '.($failed == 1 ? TXT_QUERY : TXT_QUERIES).' '.TXT_FAILED.':<br/><br/>'.implode('<br/><hr/><br/>',$failed_queries),
						  WARNING);
		}
	}
	elseif ($failed = count($failed_queries)) {
		$Message->add($failed.' '.($failed == 1 ? TXT_QUERY : TXT_QUERIES).' '.TXT_FAILED.':<br/><br/>'.implode('<br/><hr/><br/>',$failed_queries),
					  CRITICAL);
	}
}

if (isset($_GET['generate_install_sql'])) {
	
	$db_tables = $output = array();
	$res = eq("SHOW TABLES FROM `".dbName."` LIKE '".DIGI_DB_PREFIX."%';");
	while ($mya = mfa($res)) {
		$db_tables[] = $mya[0];	
	}
	
	$output[] = "# $site_name install script file generated by Digitalization CMS v$_version ($_revision)";
    $output[] = "# Generates table structures and initial content. To be loaded during a DigiCMS install.";
	
	foreach ($db_tables as $table) {
	
		$output[] = '';
        $output[] = "# Structure for $table";
        $output[] = '';	
        
        //Start table structure
		$output[] = "CREATE TABLE `$table` (";

		//Initialize primary key holder
		$primary_key = 'id';
		
		$res = eq("DESCRIBE $table;");
        while ($myo = mfo($res)) {
        	
        	//Set primary key
			if ($myo->Key == 'PRI') {
				$primary_key = $myo->Field;
			}
			
			//Check default value
			if (empty($myo->Default)) {
				$default = '';
			}
			elseif ($myo->Default == 'CURRENT_TIMESTAMP') {
				$default = "DEFAULT CURRENT_TIMESTAMP ";
			}
			else {
				$default = "DEFAULT '$myo->Default' ";
			}
			
			//Generate field row
			$output[] = "  `$myo->Field` $myo->Type NOT NULL $default$myo->Extra,";
		}
		
		//Primary key definition
		$output[] = "  PRIMARY KEY  (`$primary_key`)";
		
		//Close table structure
		$output[] = ") ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci AUTO_INCREMENT=1;";
        
        //Read table contents and output insert queries (not for auth users table though)
        if ($table == DIGI_DB_PREFIX.'auth_users') {
			$output[] = '';
	        $output[] = "# Data for $table is NOT dumped for install scripts";
	    }
	    else {
			$res = eq("SELECT * FROM $table;");
			if (mnr($res)) {
	
				$output[] = '';
		        $output[] = "# Dump for $table";
		        $output[] = '';			
	
		        while($myr = mfa($res)) {
		         
		            $props = array();
		            foreach($myr as $key => $value) {
		                if (is_string($key)) $props[] = "$key = '".mysql_real_escape_string($value)."'";
		            }
		            $output[] = "INSERT INTO $table SET ".implode(", ",$props).";";
		    	}
	        }			
		}
	}
	
	$output[] = "# End of install script";	
	
	//Provide file for download
	header('Content-Type: application/octet-stream');
	header('Content-Disposition: attachment; filename="digi_install_script_'.$site_name.'.sql"');
	echo "\xEF\xBB\xBF";
	$newline = '';
	foreach($output as $line) {
		echo $newline.$line;
		$newline = "\n";
	}
	exit;
}

/******************************************************************************************
 * Options
 ***/
 
if (isset($_GET['add']) and $is_digi) {
	
	$pre_html = "
		<h3>".TXT_ADD_SETTING."</h3><hr/><br/>
		<form name='add_setting_form' method='post' action='$_file'>
		<table cellspacing='0px' cellpadding='1px' border='0px'>
		<tr>
			<td width='150px'>".TXT_SETTING_NAME.":</td>
			<td><input type='text' name='setting' class='input_regular' value='".request('setting')."'/></td>
		</tr>
		<tr>
			<td>".TXT_SETTING_SET.":</td>
			<td><input type='text' name='setting_set' class='input_regular' value='".request('setting_set')."'/></td>
		</tr>
		<tr>
			<td>".TXT_DESCRIPTION.":</td>
			<td><input type='text' name='setting_descr' class='input_regular' value='".request('setting_descr')."'/></td>
		</tr>
		<tr>
			<td>".TXT_DIGI_PROTECTED.":</td>
			<td>".makeSelectBox('digi_protected', $YESNO_ARRAY, request('digi_protected', 0))."</td>
		</tr>
		<tr>
			<td>".TXT_TYPE_YESNO.":</td>
			<td>".makeSelectBox('type_yes_no', $YESNO_ARRAY, request('type_yes_no', 0))."</td>
		</tr>
		<tr>
			<td>".TXT_TYPE_INT.":</td>
			<td>".makeSelectBox('type_int', $YESNO_ARRAY, request('type_int', 0))."</td>
		</tr>		
		<tr>
			<td colspan='2'><br/>".TXT_VALUE.":<br/><textarea name='setting_value'>".request('setting_value')."</textarea></td>
		</tr>
		<tr>
			<td colspan='2'><br/><input type='submit' class='input_submit' name='add_finish' value='".BUTTON_ADD."' /></td>
		</tr>		
		</table>
		</form><br/>
	";
}

if (isset($_GET['add_sql']) and $is_digi) {
	
	$pre_html = "
		<h3>".TXT_PERFORM_SQL."</h3><hr/><br/>
		<form name='add_sql_form' method='post' action='$_file'>
		<table cellspacing='0px' cellpadding='1px' border='0px'>
		<tr>
			<td>".TXT_QUERIES_FIELD.":<br/><textarea name='query'></textarea></td>
		</tr>
		<tr>
			<td><br/><input type='submit' class='input_submit' name='add_sql_finish' value='".TXT_PERFORM."' /></td>
		</tr>		
		</table>
		</form><br/>
	";
}

if (isset($_GET['view_error_log'])) {
	
	$log_file = '..'.$_sep.'user_files'.$_sep.'error.log';
	
	if (!file_exists($log_file)) {
		$Message->set($msg_error_log_na, WARNING);
	}
	else {
		
		$pre_html = "
			<h3>Error log</h3><hr/><br/>
		";
		
		$lines = file($log_file);
		array_shift($lines);
		foreach ($lines as $line) {
			$pre_html .= $line.'<br/>';
		}
		
		$pre_html .= '<br/>';
	}
}

/******************************************************************************************
 * Page contents
 ***/

//Page title HTML
$html = 	"
	<a href='$_file' title='".ucfirst(TXT_OVERVIEW)."'><img src='$site_icon' border='0px' class='cms_icon'/></a>
	<h6>
		<a href='$_file'>".TXT_OVERVIEW."</a> &nbsp;
		".(($is_digi) ? "
			$img[window_add] <a href='$_file?add'>".TXT_NEW_SETTING."</a> &nbsp;
			$img[db_process] <a href='$_file?add_sql'>".strtolower(TXT_PERFORM_SQL)."</a> &nbsp;
			$img[db_process] <a href='$_file?generate_install_sql'>".TXT_MAKE_INSTALL_SQL."</a> &nbsp;
			$img[window] <a href='$_file?view_error_log'>".TXT_VIEW_ERROR_LOG."</a>
		" : '')."
	</h6>
	<br/>".$Message->get()."
	<br/><br/>$pre_html
";			

//Change user details HTML
if (checkPriv(PRIV_CHANGE_USER)) {
	
	$mailer_array 	= array(0 => TXT_USE_EXTERNAL_MAILER, 1 => TXT_USE_CMS_MAILER);
	$user			= getMyo($db_table_auth, $s_id, array('is_admin','first_name','last_name','email_address','username'));

	$html .= "
        <h3>".TXT_CHANGE_USER_INFO.":</h3><hr/>
        <form name='change_user_form' method='post' action='$_file'>
            <table cellspacing='0px' cellpadding='1px' border='0px'>

                <tr>
					<td width='150px'><br/>".TXT_FIRST_NAME.":</td>
					<td><br/><input type='text' name='first_name' class='input_regular' value='".$user->first_name."'/></td>
				</tr>
                <tr>
					<td>".TXT_LAST_NAME.":</td>
					<td><input type='text' name='last_name' class='input_regular' value='".$user->last_name."'/></td>
				</tr>
                <tr>
					<td>".TXT_EMAIL_ADDRESS.":</td>
					<td><input type='text' name='email_address' class='input_regular' value='".$user->email_address."'/></td>
				</tr>
                <tr>
					<td><br/>".TXT_USERNAME.":</td>
					<td><br/><input type='text' name='username' class='input_regular' value='".$user->username."'/></td>
				</tr>				
                <tr>
					<td>".TXT_NEW_PASS.":</td>
					<td><input type='password' name='pass1' class='input_regular' autocomplete='off'/></td>
				</tr>
                <tr>
					<td>".TXT_REPEAT_PASS.":</td>
					<td><input type='password' name='pass2' class='input_regular' autocomplete='off'/></td>
				</tr>
				".((isset($installed_modules[MOD_MAILER]) and checkPriv(PRIV_MAILER)) ? "
				<tr>
					<td>".TXT_CMS_MAILER_OR_NOT.":</td>
					<td>".makeSelectBox('uses_mailer',$mailer_array,$_SESSION['uses_mailer'])."</td>
				</tr>
				" : '')."
				<tr>
					<td>".TXT_CURRENT_PASSWORD.":</td>
					<td><input type='password' name='pass0' class='input_regular' /></td>
				</tr>				
                <tr>
					<td></td><td><br/><input type='submit' class='input_submit' name='change_user_finish' value='".BUTTON_CHANGE."' /></td>
				</tr>
            </table>
        </form>
	";
}

//Simple username display
else {
	
	$user = getMyo($db_table_auth, $s_id, array('first_name','last_name','email_address','username'));
	
	$html .= "
        <h3>".TXT_USER_INFO.":</h3><hr/><br/>
        <table cellspacing='0px' cellpadding='1px' border='0px'>
            <tr>
				<td width='150px'>".TXT_FIRST_NAME.":</td>
				<td>".$user->first_name."</td>
			</tr>
            <tr>
				<td>".TXT_LAST_NAME.":</td>
				<td>".$user->last_name."</td>
			</tr>
            <tr>
				<td >".TXT_USERNAME.":</td>
				<td>".$user->username."</td>
			</tr>
            <tr>
				<td >".TXT_EMAIL_ADDRESS.":</td>
				<td>".$user->email_address."</td>
			</tr>
        </table>
    ";
}

//Settings edit HTML
if (checkPriv(PRIV_SETTINGS)) {

	//Setting definitions
	require_once('inc/definitions/def_general_settings_'.$cms_lang.'.php');

	//Set vars
	$set_rows		=	array();
		
	//Read sets
	$where	 = $is_digi ? '' : "WHERE digi_protected = '0'";
	$res_set = eq("SELECT setting_set FROM $db_table $where GROUP BY setting_set ORDER BY setting_set ASC;");
	while($myo_set = mfo($res_set)) {

		if ($myo_set->setting_set == 'CMS' and !$is_digi) {
			continue;
		}
		
		$item_rows 		= 	array();
			
		//Read elements
		$res = eq("	SELECT * FROM $db_table WHERE setting_set = '$myo_set->setting_set';");
		while ($myo = mfo($res)) {

			if ($myo->digi_protected and !$is_digi) {
				continue;
			}
	
			$icon 		= 	$myo->digi_protected ? $img['halfok'].' ' : '';
			$int  		= 	$myo->type_int ? $js['int'] : '';			
			$descr		=	isset($setting[$myo->setting]) ? $setting[$myo->setting] : (
							isset($myo->setting_descr) ? $myo->setting_descr : 'Undefined');
			$del_option = 	$is_digi ? "<a href='$_file?delete=$myo->id&rs=$random_string'
										title='".TXT_DELETE_SETTING."'>$img[window_del]</a>" : '';
													
			if ($myo->type_yes_no) {
				$item_rows[$descr] =   "<tr>
											<td>$icon<span title='$myo->setting'>$descr</span>:</td>
											<td style='padding-left: 10px;'>
							  				".makeSelectBox("setting_$myo->setting",$YESNO_ARRAY, $myo->setting_value, 306)."
											$del_option</td>
										</tr>";
			}
			else {
				$item_rows[$descr] =   "<tr>
											<td>$icon<span title='$myo->setting'>$descr</span>:</td>
											<td style='padding-left: 10px;'>
							  				<input type='text' name='setting_$myo->setting' value='".$myo->setting_value."' $int
											class='input_regular' style='width: 300px;'/>
											$del_option</td>
										</tr>";
			}
		}
		
		ksort($item_rows);
		$set_title		=	isset($setting_set[$myo_set->setting_set]) ? $setting_set[$myo_set->setting_set] : $myo_set->setting_set;
		$set_rows[]		=	"<tr><td colspan='2'><h3>$set_title</h3><br/></td></tr>";
		$set_rows[]		=	implode("\n",$item_rows);
		$set_rows[]		=	"<tr>
								<td width='400px'>&nbsp;</td>
								<td style='padding-left: 10px;'><br/>
								<input type='submit' class='input_submit' name='change_settings_finish' value='".BUTTON_ADJUST."' /></td>
							</tr>";
	} 

	$html .= "
        <br/><h3>".TXT_CHANGE_SETTINGS.":</h3><hr/>
        <br/>".TXT_CONTACT_TECHNICAL_1."
		<br/>".TXT_CONTACT_TECHNICAL_2." $cfg[TECH_NAME] ".TXT_THROUGH." <a href='$mailer$cfg[TECH_EMAIL]'>$cfg[TECH_EMAIL]</a>.
        <br/><br/><br/>
        <form name='change_settings_form' method='post' action='$_file'>
        <table cellspacing='0px' cellpadding='1px' border='0px' style='width: 850px;'>
			".implode("\n",$set_rows)."
		</table>
        </form> 	
	";
}

echo $html;

require_once('inc/cms_footer.php');
?>