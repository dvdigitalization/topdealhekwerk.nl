<?php
/*
Title:		Discount codes management file
File: 		cms_discount_codes.php
Version: 	v2.20
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

/******************************************************************************************
 * Settings
 ***/

// *** Required files
require_once('inc/cms_header.php');
require_once(CMS_PATH_PREFIX.'classes/class_DataTable.php');

// *** Extra database table definitions
$db_table_orders 		= 	DIGI_DB_PREFIX.'orders';

// *** Object definition
$object					=	'Kortingscode';
$object_lc 				= 	'kortingscode';
$plural					=	'Kortingscodes';
$plural_lc				=	'kortingscodes';
$adjective_e			=	'e';
$ident					=	'Deze';

// *** Sorting and pages functionality
$default_sort_option	=	'name';
$default_sort_method	=	'ASC';
$secondary_sort_option	=	'';
$secondary_sort_method	=	'';
$valid_sort_options		=	array('id','code','name','percentage','fixed_amount','active','valid_till');

// *** Deleted check

// *** Search constraints
if (!empty($search_query)) {
	$search				=	array();
	$search[]			=	"code LIKE '%$search_query%'";
	$search[]			=	"name LIKE '%$search_query%'";
	$where[]			=	'('.implode(' OR ', $search).')';
}

// *** Message definitions
$msg_added 				= 	'Kortingscode toegevoegd.';
$msg_editted 			= 	'Kortingscode aangepast.';
$msg_deleted			=	'Kortingscode verwijderd.';
$msg_deleted_m 			= 	'Kortingscodes verwijderd.';
$msg_delete_confirm		= 	'Deze kortingscode zeker weten verwijderen?';
$msg_delete_confirm_m 	= 	'Deze kortingscodes zeker weten verwijderen?';
$msg_activated			=	'Kortingscode geactiveerd.';
$msg_activated_m		=	'Kortingscodes geactiveerd.';
$msg_deactivated		=	'Kortingscode op non-actief gezet.';
$msg_deactivated_m		=	'Kortingscode op non-actief gezet.';

// *** Extra appendix

// *** Sorting and pages and link appendix
require_once(CMS_PATH_PREFIX.'inc/sorting_and_pages.php');
$_app					=	$page_appendix.'&'.$sort_appendix.'&'.$link_appendix;

/******************************************************************************************
 * POST and GET Cases
 ***/

if (isset($_POST['add_finish']) or isset($_POST['edit_finish'])) {
	foreach(array_keys($_POST) as $key) {
		evalAll($_POST[$key]);
	}

	if ($_POST['percentage']) {
		$_POST['fixed_amount'] = 0;
	}
	$_POST['valid_till'] = mysql_time(date2time($_POST['valid_till']));
}

if (isset($_POST['add_finish'])) {

	$db_id = insRec(	$db_table,
           		array(	'name','code','percentage','fixed_amount','active','one_time_use','valid_till'),
            	array(	$_POST['name'],$_POST['code'],$_POST['percentage'],dbAmount($_POST['fixed_amount']),1,
						isset($_POST['one_time_use']) ? 1 : 0,$_POST['valid_till'])
	);

    $Message->set($msg_added);

	cmsLog("$object '$_POST[code]' (#$db_id) toegevoegd.");
}

if (isset($_POST['edit_finish']) and $db_id = (int) $_POST['id']) {

	upRec(	$db_table, $db_id,
           		array(	'name','code','percentage','fixed_amount','one_time_use','valid_till'),
            	array(	$_POST['name'],$_POST['code'],$_POST['percentage'],dbAmount($_POST['fixed_amount']),
						isset($_POST['one_time_use']) ? 1 : 0,$_POST['valid_till'])
	);

	$Message->set($msg_editted);

	cmsLog("$object '$_POST[code]' (#$db_id) aangepast.");
}

// *** Delete confirmation
if (isset($_GET['delete']) and $id_holder = $_GET['delete']) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			$Message->setConfirmMultiple($msg_delete_confirm_m, 'delete',$db_ids,'code');
		}
	}
	else if ($db_id = (int) $id_holder) {
		$Message->setConfirm($msg_delete_confirm,'delete',$db_id,'code');
	}
}

// *** Delete action
if (isset($_GET['delete_confirm']) and $id_holder = $_GET['delete_confirm'] and noBrowse()) {

	if (isset($_GET['multiple_confirm']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			delRec($db_table, $db_ids);
			cmsLog("Meerdere $plural_lc permanent verwijderd.");
			$Message->set($msg_deleted_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		delRec($db_table, $db_id);
		cmsLog("$object (#$db_id) verwijderd.");
		$Message->set($msg_deleted);
	}
}

// *** Acivate
if (isset($_GET['activate']) and $id_holder = $_GET['activate'] and noBrowse()) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			upRec($db_table,$db_ids,'active',1);
			cmsLog("Meerdere $plural_lc geactiveerd");
			$Message->set($msg_activated_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		upRec($db_table,$db_id,'active',1);
		cmsLog("$object (#$db_id) geactiveerd");
		$Message->set($msg_activated);
	}
}

// *** De-activate
if (isset($_GET['deactivate']) and $id_holder = $_GET['deactivate'] and noBrowse()) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			upRec($db_table,$db_ids,'active',0);
			cmsLog("Meerdere $plural_lc op non-actief gezet.");
			$Message->set($msg_deactivated_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		upRec($db_table,$db_id,'active',0);
		cmsLog("$object (#$db_id) op non-actief gezet");
		$Message->set($msg_deactivated);
	}
}

/******************************************************************************************
 * Add / Edit / View options
 ***/

if (isset($_GET['add'])) {

	$pre_html = "
		<h3>Nieuwe $object_lc toevoegen:</h3><hr/><br/>
        <form name='add_form' method='post' action='$_file'>
        $page_and_sort_inputs
        <table cellspacing='0px' cellpadding='1px' border='0px'>

        <tr>
			<td width='150px'>Naam:</td>
			<td><input type='text' name='name' class='input_regular' /></td>
		</tr>
        <tr>
			<td>Code:</td>
			<td><input type='text' name='code' class='input_regular' value='".strtoupper(randomPass(8))."'/></td>
		</tr>
        <tr>
			<td>Percentage:</td>
			<td><input type='text' name='percentage' class='input_small' $js[NaN] /> %</td>
		</tr>
        <tr>
			<td>Of vast bedrag:</td>
			<td><input type='text' name='fixed_amount' class='input_small' $js[NaN] /> euro</td>
		</tr>
		<tr>
			<td>Geldig tot:</td>
			<td><input type='text' readonly='readonly' name='valid_till' id='valid_till' class='input_calendar' value='".date('d/m/Y', time()+24*14*3600)."' />
				<button class='calendar_button' type='reset' id='valid_till_trigger'>...</button></td>
		</tr>

	    <tr>
			<td colspan='2'><br/><input type='submit' name='add_finish' value='Toevoegen' /></td>
		</tr>
        </table>
        </form><br/>

        <script type='text/javascript'>
			Calendar.setup({
		        inputField     :    'valid_till',      		// id of the input field
		        button         :    'valid_till_trigger'   	// trigger for the calendar (button ID)
		    });
        </script>
	";
}

if (isset($_GET['edit']) and $db_id = (int) $_GET['edit']) {

	$res = eq("SELECT * FROM $db_table WHERE id = '$db_id';");
	$myo = mfo($res);

	$pre_html = "
		<h3>$object gegevens aanpassen:</h3><hr/><br/>
        <form name='edit_form' method='post' action='$_file'>
        <input type='hidden' name='id' value='$db_id' />
        $page_and_sort_inputs
        <table cellspacing='0px' cellpadding='1px' border='0px'>

        <tr>
			<td width='150px'>Naam:</td>
			<td><input type='text' name='name' class='input_regular' value='$myo->name'/></td>
		</tr>
		<tr>
			<td>Code:</td>
			<td><input type='text' name='code' class='input_regular' value='$myo->code'/></td>
		</tr>
        <tr>
			<td>Percentage:</td>
			<td><input type='text' name='percentage' class='input_small' $js[NaN] value='".($myo->percentage)."'/> %</td>
		</tr>
        <tr>
			<td>Of vast bedrag:</td>
			<td><input type='text' name='fixed_amount' class='input_small' $js[NaN] value='".parseAmount($myo->fixed_amount)."'//> euro</td>
		</tr>
		<tr>
			<td>Geldig tot:</td>
			<td><input type='text' readonly='readonly' name='valid_till' id='valid_till' class='input_calendar' value='".date('d/m/Y', unix_time($myo->valid_till))."' />
				<button class='calendar_button' type='reset' id='valid_till_trigger'>...</button></td>
		</tr>

	    <tr>
			<td colspan='2'><br/><input type='submit' name='edit_finish' value='Opslaan' /></td>
		</tr>
        </table>
        </form><br/>

        <script type='text/javascript'>
			Calendar.setup({
		        inputField     :    'valid_till',      		// id of the input field
		        button         :    'valid_till_trigger'   	// trigger for the calendar (button ID)
		    });
        </script>
	";
}

if (isset($_GET['view']) and $db_id = (int) $_GET['view']) {

	$res = eq("SELECT * FROM $db_table WHERE id = '$db_id';");
	$myo = mfo($res);

    $pre_html = "
		<h3>$object gegevens bekijken:</h3><hr/><br/>
		<table>
		<tr>
			<td width='150px'>Naam:</td>
			<td>$myo->name</td>
		</tr>
		<tr>
			<td>Code:</td>
			<td>$myo->code</td>
		</tr>
		<tr>
			<td>Percentage:</td>
			<td>$myo->percentage%</td>
		</tr>
		<tr>
			<td>Of vast bedrag:</td>
			<td>".parseAmount($myo->fixed_amount,1)."</td>
		</tr>
		<tr>
			<td>Geldig tot:</td>
			<td>".time2date($myo->valid_till)."</td>
		</tr>
		</table><br/>
	";
}


/******************************************************************************************
 * Page contents
 ***/

//Page title HTML
$html = "
	<a href='$_file'><img src='$site_icon' border='0px' class='cms_icon'/></a>
	<h6>
		<a href='$_file'>overzicht</a> &nbsp;
		$img[invoice_add] <a href='$_file?add=1'>nieuwe kortingscode</a>
	</h6>
	<br/>".$Message->get()."
	<br/><br/>$pre_html
";

//Get items for this page and this sorting method
$data_rows = array();
$res = eq("	SELECT 		id, percentage, fixed_amount, code, name, active, valid_till
			FROM 		$db_table $where_clause
			ORDER BY 	$order_clause
			LIMIT 		$limit_clause
;");

while ($myo = mfo($res)) {

	$options = "<a href='$_file?edit=$myo->id&$_app' title='$object gegevens bijwerken'>$img[edit]</a> ";
	$options .= "<a href='$_file?delete=$myo->id&$_app' title='$object verwijderen'>$img[invoice_del]</a> ";

    $data_rows[] = array(

		//This MUST be the id
		$myo->id,

		//All desired other fields
		$myo->id,
		"<a href='$_file?view=$myo->id&$_app' title='$object gegevens inzien'>$myo->name</a>",
		$myo->code,
		$myo->percentage.'%',
		parseAmount($myo->fixed_amount,1),
		time2date($myo->valid_till),
		$myo->active ? 		"<a href='$_file?deactivate=$myo->id&$_app' title='$object de-activeren'>$img[ok]</a>" :
							"<a href='$_file?activate=$myo->id&$_app' title='$object activeren'>$img[notok]</a>",

	    //The options last
		$options

	);
}

//Output data table
$DataTable = new DataTable();

$DataTable->setMarkAllOptions(	array('verwijder','delete',$plural.' verwijderen'),
								array('activeer','activate',$plural.' activeren'),
								array('de-activeer','deactivate',$plural.' de-activeren'));

$DataTable->showSearch();
$DataTable->setDataRows($data_rows);


$DataTable->setLegendRow(
	array('#','Naam','Code','Percentage','Bedrag','Geldig tot','Actief','Opties'),
	array(60,0,140,100,80,90,60,40),
	array('id','name','code','percentage','fixed_amount','valid_till','active')
);

echo $html.$DataTable->getHTML();

require_once('inc/cms_footer.php');
?>