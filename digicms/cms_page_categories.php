<?
/*
Title:		Page categories management
File: 		cms_page_categories.php
Version: 	v2.20
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

/******************************************************************************************
 * Settings
 ***/

// *** Required files
require_once('inc/cms_header.php');
require_once('inc/lang/'.$cms_lang.'_'.$_file);
require_once(CMS_PATH_PREFIX.'classes/class_DataTable.php');

// *** File definitions
$file_pages				= 	'cms_pages.php';

// *** Database tables
$db_table				=	DIGI_DB_PREFIX.'page_categories';
$db_table_pages			=	DIGI_DB_PREFIX.'pages_';

// *** Sorting and pages functionality
$default_sort_option	=	'name';
$default_sort_method	=	'ASC';
$secondary_sort_option	=	'';
$secondary_sort_method	=	'';
$valid_sort_options		=	array('id','name');

// *** Search constraints
if (!empty($search_query)) {
	$search				=	array();
	$search[]			=	"name LIKE '%$search_query%'";
	$where[]			=	'('.implode(' OR ', $search).')';
}

// *** Sorting and pages and link appendix
require_once(CMS_PATH_PREFIX.'inc/sorting_and_pages.php');
$_app					=	$page_appendix.'&'.$sort_appendix.'&'.$link_appendix;

/******************************************************************************************
 * POST and GET Cases
 ***/

// *** Data cleansing
if (isset($_POST['add_finish']) or isset($_POST['edit_finish'])) {

 	foreach (array_keys($_POST) as $key) {
		evalAll($_POST[$key]);
	}

	if (empty($_POST['name']))	$_POST['name']	=	TXT_UNNAMED;
}

// *** Add
if (isset($_POST['add_finish'])) {

	$db_id = insRec($db_table,
					array(	'name'),
					array(	$_POST['name']));

	cmsLog("$object '$_POST[name]' (#$db_id) ".LOG_ADDED);
    $Message->set($msg_added);
}

// *** Edit
if (isset($_POST['edit_finish']) and $db_id = (int) $_POST['id']) {

	upRec($db_table,$db_id,
				array('name'),
				array($_POST['name']));

	cmsLog("$object '$_POST[name]' (#$db_id) ".LOG_EDITTED);
    $Message->set($msg_editted);
}

// *** Delete confirmation
if (isset($_GET['delete']) and $id_holder = $_GET['delete']) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			$Message->setConfirmMultiple($msg_delete_confirm_m, 'delete',$db_ids,'name');
		}
	}
	else if ($db_id = (int) $id_holder) {
		$Message->setConfirm($msg_delete_confirm,'delete',$db_id,'name');
	}
}

// *** Delete
if (isset($_GET['delete_confirm']) and $id_holder = $_GET['delete_confirm'] and noBrowse()) {

	if (isset($_GET['multiple_confirm']) and is_array($id_holder)) {

		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			delRec($db_table, $db_ids);
			$all_ids = $db_ids;
			cmsLog(LOG_MULTIPLE.' '.$plural_lc.' '.LOG_DELETED);
			$Message->set($msg_deleted_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		delRec($db_table, $db_id);
		$all_ids = array($db_id);
		cmsLog("$object (#$db_id) ".LOG_DELETED);
		$Message->set($msg_deleted);
	}

	$all_ids = implode(',',$all_ids);
	foreach ($installed_languages as $lang) {
		upRec($db_table_pages.$lang,"category_id IN ($all_ids)",'category_id',0);
	}
}

/******************************************************************************************
 * Add / Edit / View options
 ***/

if (isset($_GET['add'])) {

	$pre_html = "
		<h3>".TXT_ADD_ITEM.":</h3><hr/><br/>
		<form name='add_form' method='post' action='$_file'>
		<table border='0px' cellspacing='0px' cellpadding='2px'>

		<tr>
			<td width='150px'>".TXT_NAME.":</td>
			<td><input type='text' name='name' class='input_regular'/></td>
		</tr>

		<tr>
			<td>&nbsp;</td>
			<td><br/><input type='submit' class='input_submit' name='add_finish' value='".BUTTON_ADD."' /></td>
		</tr>

		</table>
		</form><br/>
	";
}

if (isset($_GET['edit']) and $db_id = (int) $_GET['edit']) {

 	$res = eq("SELECT * FROM $db_table WHERE id='$db_id';");
 	$myo = mfo($res);

	$pre_html = "
		<h3>".TXT_EDIT_ITEM.":</h3><hr/><br/>
		<form name='edit_form' method='post' action='$_file'>
		<input type='hidden' name='id' value='$db_id'/>
		$page_and_sort_inputs
		<table border='0px' cellspacing='0px' cellpadding='2px'>

		<tr>
			<td width='150px'>".TXT_NAME.":</td>
			<td><input type='text' name='name' value='$myo->name' class='input_regular'/></td>
		</tr>

		<tr>
			<td>&nbsp;</td>
			<td><br/><input type='submit' class='input_submit' name='edit_finish' value='".BUTTON_SAVE."' /></td>
		</tr>

		</table>
		</form><br/>
	";
}

if (isset($_GET['view']) and $db_id = (int) $_GET['view']) {

 	$res = eq("SELECT * FROM $db_table WHERE id='$db_id';");
 	$myo = mfo($res);

	$pre_html = "
		<h3>".TXT_VIEW_ITEM.":</h3><hr/><br/>

		<table border='0px' cellspacing='0px' cellpadding='2px'>
		<tr>
			<td width='150px'>".TXT_NAME.":</td>
			<td>$myo->name</td>
		</tr>

		</table><br/><br/>
	";
}

/******************************************************************************************
 * Page contents
 ***/

//Page name HTML
$html = 	"
	<a href='$_file' name='".ucfirst(TXT_OVERVIEW)."'><img src='$icon[folder_full]' border='0px' class='cms_icon'/></a>
	<h6>
		<a href='$_file'>".TXT_OVERVIEW."</a> &nbsp;
		$img[folder_add] <a href='$_file?add'>".TXT_NEW_ITEM."</a> &nbsp;
		$img[page_next] <a href='$file_pages'>".TXT_BACK_TO_PAGES."</a>
	</h6>
	<br/>".$Message->get()."
	<br/><br/>$pre_html
";

//Get items for this page and this sorting method
$res = eq("	SELECT 		id, name
			FROM 		$db_table $where_clause
			ORDER BY 	$order_clause
			LIMIT 		$limit_clause
		;");
while ($myo = mfo($res)) {

	$data_rows[] = array(

		//This MUST be the id
		$myo->id,

		//All desired other fields
		$myo->id,
		"<a href='$_file?view=$myo->id&$_app' title='".TXT_VIEW_ITEM."'>$myo->name</a>",

		//The options last
		"<a href='$_file?edit=$myo->id&$_app' title='".TXT_EDIT_ITEM."'>$img[edit]</a>
		 <a href='$_file?delete=$myo->id&$_app' title='".TXT_DELETE_ITEM."'>$img[folder_del]</a>
		"
	);
}

//Output data table
$DataTable = new DataTable();
$DataTable->setMarkAllOptions(	array(TXT_DELETE, 		'delete', 		TXT_DELETE_ITEMS));
$DataTable->showSearch();
$DataTable->setDataRows($data_rows);
$DataTable->setLegendRow(
	array('#',TXT_NAME,TXT_OPTIONS),
	array(60,0,40),
	array('id','name')
);
echo $html.$DataTable->getHTML();

require_once('inc/cms_footer.php');
?>