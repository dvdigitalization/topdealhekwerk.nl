<?
/*
Title:		Offertes management file [Topdeal]
File: 		cms_topdeal_offertes.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization

$order = array(
	array('170672','5 412298 170672',10,2513,19),
	array('170689','5 412298 170689',5,3605,19)
);

echo stripslashes(serialize($order));

*/

/******************************************************************************************
 * Settings
 ***/

// *** Required files
require_once('inc/cms_header.php');
require_once(CMS_PATH_PREFIX.'classes/class_DataTable.php');

// *** Other database tables
$db_table_customers		=	DIGI_DB_PREFIX.'topdeal_customers';
$file_c					=	'cms_topdeal_customers.php';

// *** Sorting and pages functionality
$default_sort_option	=	'status';
$default_sort_method	=	'ASC';
$secondary_sort_option	=	'timestamp';
$secondary_sort_method	=	'DESC';
$valid_sort_options		=	array('id','customer_id','timestamp','status','sub_total');

// *** Object definition
$object					=	'Offerte aanvraag';
$object_lc 				= 	'offerte aanvraag';
$plural					=	'Offerte aanvragen';
$plural_lc				=	'offerte aanvragen';

// *** Message definitions
$msg_deleted			=	'De offerte aanvraag is verwijderd.';
$msg_deleted_m			=	'De offerte aanvragen zijn verwijderd.';
$msg_delete_confirm		=	'Deze offerte aanvraag zeker weten verwijderen?';
$msg_delete_confirm_m	=	'Deze offerte aanvragen zeker weten verwijderen?';

// *** Sorting and pages and link appendix
require_once(CMS_PATH_PREFIX.'inc/sorting_and_pages.php');
$_app					=	$page_appendix.'&'.$sort_appendix.'&'.$link_appendix;

// *** Customers
$CUSTOMERS_ARRAY		=	array();
$res = eq("SELECT id, first_name, last_name, title, company_name FROM $db_table_customers ORDER BY company_name ASC, last_name ASC;");
while ($myo = mfo($res)) {
	if ($myo->company_name) {
		$CUSTOMERS_ARRAY[$myo->id] = $myo->company_name;
	}
	else {
		$CUSTOMERS_ARRAY[$myo->id] = $SEX_ARRAY[$myo->title].$myo->last_name.', '.$myo->first_name;
	}
}

// *** Statusses
$STATUS_ARRAY			=	array(0 => 'Nieuw', 1 => 'In behandeling', 2 => 'Behandeld');

/******************************************************************************************
 * POST and GET Cases
 ***/

// *** Delete confirmation
if (isset($_GET['delete']) and $id_holder = $_GET['delete']) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			$Message->setConfirmMultiple($msg_delete_confirm_m, 'delete',$db_ids,'id');
		}
	}
	else if ($db_id = (int) $id_holder) {
		$Message->setConfirm($msg_delete_confirm,'delete',$db_id,'id');
	}
}

// *** Delete
if (isset($_GET['delete_confirm']) and $id_holder = $_GET['delete_confirm'] and noBrowse()) {

	if (isset($_GET['multiple_confirm']) and is_array($id_holder)) {

		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			delRec($db_table, $db_ids);
			cmsLog(LOG_MULTIPLE.' '.$plural_lc.' '.LOG_DELETED);
			$Message->set($msg_deleted_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		delRec($db_table, $db_id);
		cmsLog("$object (#$db_id) ".LOG_DELETED);
		$Message->set($msg_deleted);
	}
}

// *** Status change
if (isset($_GET['status']) and $db_id = (int) $_GET['status']) {
	$status = 1 + getMyo($db_table, $db_id, 'status');
	$status = ($status >= count($STATUS_ARRAY)) ? 0 : $status;
	upRec($db_table, $db_id, 'status', $status);
}

/******************************************************************************************
 * Add / Edit / View options
 ***/

if (isset($_GET['view']) and $db_id = (int) $_GET['view']) {

	//Read data
 	$res = eq("SELECT * FROM $db_table WHERE id='$db_id';");
 	$myo = mfo($res);

	/*//Make product arrays (unserialize)
	$products = unserialize($myo->order_serialized);
	if (!is_array($products)) {
		$products = array();
	}

	//Loop products
	foreach ($products as $key => $product) {

		list($product_no, $ean_code, $amount, $price, $vat) = $product;
		$total_price 	= parseAmount($price*$amount, 1);
		$price 			= parseAmount($price,1);

		$products[$key] = "
			<tr>
				<td>$product_no</td>
				<td>$ean_code</td>
				<td>$amount x</td>
				<td>$price =</td>
				<td>$total_price (excl. $vat% BTW)</td>
			</tr>
		";
	}*/

	$pre_html = "
		<h3>Offerte gegevens inzien:</h3><hr/><br/>
		<table border='0px' cellspacing='0px' cellpadding='2px'>
		<tr>
			<td width='150px'>Nummer:</td>
			<td>$myo->id</td>
		</tr>
		<tr>
			<td>Klant:</td>
			<td><a href='$file_c?view=$myo->customer_id' title='Klantgegevens inzien'>".$CUSTOMERS_ARRAY[$myo->customer_id]."</a></td>
		</tr>
		<tr>
			<td>Datum:</td>
			<td>".time2date($myo->timestamp)."</td>
		</tr>
		<tr>
			<td>Status:</td>
			<td>".$STATUS_ARRAY[$myo->status]."</td>
		</tr>
		</table><br/>
		$myo->order_serialized
		<br/><br/>
	";

	/*
		<tr>
			<td>Subtotaal:</td>
			<td>".parseAmount($myo->sub_total,1)."</td>
		</tr>
		</table>

		".(count($products) ? "<br/>
		<table border='0px' cellspacing='0px' cellpadding='2px'>
		<tr class='legend_row'>
			<td width='100px'>Product nr</td>
			<td width='150px'>EAN code</td>
			<td width='60px'>Aantal</td>
			<td width='100px'>Prijs</td>
			<td width='160px'>Subtotaal</td>
		</tr>
		".implode("\n", $products)."
		</table>
		" : '')."
		<br/><br/>
	";*/
}

/******************************************************************************************
 * Page contents
 ***/

//Page name HTML
$html = 	"
	<a href='$_file' name='".ucfirst(TXT_OVERVIEW)."'><img src='$site_icon' border='0px' class='cms_icon'/></a>
	<h6>
		<a href='$_file'>".TXT_OVERVIEW."</a>
	</h6>
	<br/>".$Message->get()."
	<br/><br/>$pre_html
";

//Get items for this page and this sorting method
$res = eq("	SELECT 		id, timestamp, customer_id, status, sub_total
			FROM 		$db_table $where_clause
			ORDER BY 	$order_clause
			LIMIT 		$limit_clause
		;");
while ($myo = mfo($res)) {

	$data_rows[] = array(

		//This MUST be the id
		$myo->id,

		//All desired other fields
		"<a href='$_file?view=$myo->id&$_app' title='Offerte gegevens inzien'>$myo->id</a>",
		"<a href='$file_c?view=$myo->customer_id' title='Klant gegevens inzien'>".$CUSTOMERS_ARRAY[$myo->customer_id]."</a>",
		"<a href='$_file?status=$myo->id&$_app' title='Status aanpassen'>$img[process]</a> ".$STATUS_ARRAY[$myo->status],
		time2date($myo->timestamp),

		//The options last
		"<a href='$_file?delete=$myo->id&$_app' title='Aanvraag verwijderen'>$img[invoice_del]</a>"
	);
}

//Output data table
$DataTable = new DataTable();
$DataTable->setMarkAllOptions(	array(TXT_DELETE, 		'delete', 		'Aanvragen verwijderen'));
$DataTable->setDataRows($data_rows);
$DataTable->setLegendRow(
	array('#','Klant','Status','Datum',TXT_OPTIONS),
	array(60,0,140,80,40),
	array('id','customer_id','status','timestamp')
);
echo $html.$DataTable->getHTML();

require_once('inc/cms_footer.php');
?>