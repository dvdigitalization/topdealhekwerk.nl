<?
/*
Title:		Holidays management file
File: 		cms_holidays.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

/******************************************************************************************
 * Settings
 ***/

// *** Required files
require_once('inc/cms_header.php');
require_once('inc/lang/'.$cms_lang.'_'.$_file);
require_once(CMS_PATH_PREFIX.'classes/class_DataTable.php');

// *** File definitions
$file_item				= 	'cms_holidays.php';

// *** Other database tables
$db_table_yyy			=	DIGI_DB_PREFIX.'holidays';

// *** Sorting and pages functionality
$default_sort_option	=	'date';
$default_sort_method	=	'ASC';
$secondary_sort_option	=	'';
$secondary_sort_method	=	'';
$valid_sort_options		=	array('id','date','active');

// *** Search constraints
if (!empty($search_query)) {
	$search				=	array();
	$search[]			=	"name LIKE '%$search_query%'";
	$where[]			=	'('.implode(' OR ', $search).')';
}

// *** Sorting and pages and link appendix
require_once(CMS_PATH_PREFIX.'inc/sorting_and_pages.php');
$_app					=	$page_appendix.'&'.$sort_appendix.'&'.$link_appendix;

/******************************************************************************************
 * POST and GET Cases
 ***/

// *** Data cleansing
if (isset($_POST['add_finish']) or isset($_POST['edit_finish'])) {

 	foreach (array_keys($_POST) as $key) {
		evalAll($_POST[$key]);
	}
	
	if (empty($_POST['name']))	$_POST['name']	=	TXT_UNNAMED;
	
	//Convert timestamp
	$_POST['date'] = mysql_time(date2time($_POST['date']));	
	
	$save_fields   = array('name','date');
	$save_values   = array($_POST['name'],$_POST['date']);
	
	//We're trying to save a range
	if(isset($_POST['range']) && $_POST['range']){
		$save_fields[] = 'date2';
		
		$_POST['date2'] = mysql_time(date2time($_POST['date2']));
		$save_values[] = $_POST['date2'];
	}
}

// *** Add
if (isset($_POST['add_finish'])) {

	$db_id = insRec($db_table, $save_fields, $save_values);

	cmsLog("$object '$_POST[name]' (#$db_id) ".LOG_ADDED);
    $Message->set($msg_added);
}

// *** Edit
if (isset($_POST['edit_finish']) and $db_id = (int) $_POST['id']) {

	$db_id = upRec($db_table, $db_id, $save_fields, $save_values);

	cmsLog("$object '$_POST[name]' (#$db_id) ".LOG_EDITTED);
    $Message->set($msg_editted);
}

// *** Delete confirmation
if (isset($_GET['delete']) and $id_holder = $_GET['delete']) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			$Message->setConfirmMultiple($msg_delete_confirm_m, 'delete',$db_ids,'name');
		}
	}
	else if ($db_id = (int) $id_holder) {
		$Message->setConfirm($msg_delete_confirm,'delete',$db_id,'name');
	}
}

// *** Delete
if (isset($_GET['delete_confirm']) and $id_holder = $_GET['delete_confirm'] and noBrowse()) {

	if (isset($_GET['multiple_confirm']) and is_array($id_holder)) {

		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			delRec($db_table, $db_ids);
			cmsLog(LOG_MULTIPLE.' '.$plural_lc.' '.LOG_DELETED);
			$Message->set($msg_deleted_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		delRec($db_table, $db_id);
		cmsLog("$object (#$db_id) ".LOG_DELETED);
		$Message->set($msg_deleted);
	}
}

// *** Acivate
if (isset($_GET['activate']) and $id_holder = $_GET['activate'] and noBrowse()) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			upRec($db_table,$db_ids,'active',1);
			cmsLog(LOG_MULTIPLE.' '.$plural_lc.' '.LOG_ACTIVATED);
			$Message->set($msg_activated_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		upRec($db_table,$db_id,'active',1);
		cmsLog("$object (#$db_id) ".LOG_ACTIVATED);
		$Message->set($msg_activated);
	}
}

// *** De-activate
if (isset($_GET['deactivate']) and $id_holder = $_GET['deactivate'] and noBrowse()) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			upRec($db_table,$db_ids,'active',0);
			cmsLog(LOG_MULTIPLE.' '.$plural_lc.' '.LOG_DEACTIVATED);
			$Message->set($msg_deactivated_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		upRec($db_table,$db_id,'active',0);
		cmsLog("$object (#$db_id) ".LOG_DEACTIVATED);
		$Message->set($msg_deactivated);
	}
}

/******************************************************************************************
 * Add / Edit / View options
 ***/

if (isset($_GET['add'])) {
	$date2_js = $date2_html = $date2_hidden = '';
	
	if(isset($_GET['range'])){
		$date1_label = TXT_DATE_START;
		
		$date2_html = "
	  	<tr>
			<td>".TXT_DATE_STOP.":</td>
			<td><input type='text' readonly='readonly' name='date2' id='date2' class='input_calendar'
				value='".time2date(time())."' />
				<button class='calendar_button' type='reset' id='date2_trigger'>...</button></td>
		</tr>		
		";
		
		$date2_js = "
        <script type='text/javascript'>
			Calendar.setup({
		        inputField     :    'date2',      		// id of the input field
		        button         :    'date2_trigger'   	// trigger for the calendar (button ID)
		    });	
        </script>		
		";
		
		$date2_hidden = "<input type='hidden' name='range' value='1' />";
	}else{
		$date1_label = TXT_DATE;
	}

	$pre_html = "
		<h3>".TXT_ADD_ITEM.":</h3><hr/><br/>
		<form name='add_form' method='post' action='$_file'>
		
		$date2_hidden
		
		<table border='0px' cellspacing='0px' cellpadding='2px'>

		<tr>
			<td width='150px'>".TXT_NAME.":</td>
			<td><input type='text' name='name' class='input_regular'/></td>
		</tr>

	  	<tr>
			<td>$date1_label:</td>
			<td><input type='text' readonly='readonly' name='date' id='date' class='input_calendar'
				value='".time2date(time())."' />
				<button class='calendar_button' type='reset' id='date_trigger'>...</button></td>
		</tr>		
		
		$date2_html
		
		<tr>
			<td>&nbsp;</td>
			<td><br/><input type='submit' class='input_submit' name='add_finish' value='".BUTTON_ADD."' /></td>
		</tr>

		</table>
		</form><br/>
        <script type='text/javascript'>
			Calendar.setup({
		        inputField     :    'date',      		// id of the input field
		        button         :    'date_trigger'   	// trigger for the calendar (button ID)
		    });	
        </script>	
		$date2_js
	";
}

if (isset($_GET['edit']) and $db_id = (int) $_GET['edit']) {
 	$res = eq("SELECT * FROM $db_table WHERE id='$db_id';");
 	$myo = mfo($res);
	
	$date2_js = $date2_html = $date2_hidden = '';
	
	if($myo->date2 != '0000-00-00 00:00:00'){
		$date1_label = TXT_DATE_START;
		
		$date2_html = "
	  	<tr>
			<td>".TXT_DATE_STOP.":</td>
			<td><input type='text' readonly='readonly' name='date2' id='date2' class='input_calendar'
				value='".time2date($myo->date2)."' />
				<button class='calendar_button' type='reset' id='date2_trigger'>...</button></td>
		</tr>		
		";
		
		$date2_js = "
        <script type='text/javascript'>
			Calendar.setup({
		        inputField     :    'date2',      		// id of the input field
		        button         :    'date2_trigger'   	// trigger for the calendar (button ID)
		    });	
        </script>		
		";
		
		$date2_hidden = "<input type='hidden' name='range' value='1' />";
	}else{
		$date1_label = TXT_DATE;
	}

	$pre_html = "
		<h3>".TXT_EDIT_ITEM.":</h3><hr/><br/>
		<form name='edit_form' method='post' action='$_file'>
		$date2_hidden
		<input type='hidden' name='id' value='$db_id'/>
		$page_and_sort_inputs
		<table border='0px' cellspacing='0px' cellpadding='2px'>

		<tr>
			<td width='150px'>".TXT_NAME.":</td>
			<td><input type='text' name='name' value='$myo->name' class='input_regular'/></td>
		</tr>

		<tr>
			<td>$date1_label:</td>
			<td><input type='text' readonly='readonly' name='date' id='date' class='input_calendar' value='".time2date($myo->date)."' />
			<button class='calendar_button' type='reset' id='date_trigger'>...</button></td>
		</tr>		
		
		$date2_html
		
		<tr>
			<td>&nbsp;</td>
			<td><br/><input type='submit' class='input_submit' name='edit_finish' value='".BUTTON_SAVE."' /></td>
		</tr>

		</table>
		</form><br/>
        <script type='text/javascript'>
			Calendar.setup({
		        inputField     :    'date',      		// id of the input field
		        button         :    'date_trigger'   	// trigger for the calendar (button ID)
		    });	
        </script>	
		$date2_js
	";
}

if (isset($_GET['view']) and $db_id = (int) $_GET['view']) {

 	$res = eq("SELECT * FROM $db_table WHERE id='$db_id';");
 	$myo = mfo($res);
	
	$date2_html = '';
	if($myo->date2 != '0000-00-00 00:00:00'){
		$date1_label = TXT_DATE_START;
		
		$date2_html  = "
		<tr>
			<td width='150px'>".TXT_DATE_STOP."</td>
			<td>".time2date($myo->date2)."</td>
		</tr>		
		";
	}else{
		$date1_label = TXT_DATE;
	}

	$pre_html = "
		<h3>".TXT_VIEW_ITEM.":</h3><hr/><br/>

		<table border='0px' cellspacing='0px' cellpadding='2px'>
		<tr>
			<td width='150px'>".TXT_NAME.":</td>
			<td>$myo->name</td>
		</tr>
		<tr>
			<td width='150px'>$date1_label</td>
			<td>".time2date($myo->date)."</td>
		</tr>		
		$date2_html
		
		</table><br/><br/>
	";
}

/******************************************************************************************
 * Page contents
 ***/

//Page name HTML
$html = 	"
	<a href='$_file' name='".ucfirst(TXT_OVERVIEW)."'><img src='$site_icon' border='0px' class='cms_icon'/></a>
	<h6>
		<a href='$_file'>".TXT_OVERVIEW."</a> &nbsp;
		$img[calendar_add] <a href='$_file?add'>".TXT_NEW_ITEM."</a> &nbsp;
		$img[calendar_add] <a href='$_file?add&range'>".TXT_NEW_ITEMS."</a> &nbsp;
	</h6>
	<br/>".$Message->get()."
	<br/><br/>$pre_html
";

//Get items for this page and this sorting method
$res = eq("	SELECT 		id, name, date, date2, active
			FROM 		$db_table $where_clause
			ORDER BY 	$order_clause
			LIMIT 		$limit_clause
		;");
while ($myo = mfo($res)) {

	$data_rows[] = array(

		//This MUST be the id
		$myo->id,

		//All desired other fields
		$myo->id,
		time2date($myo->date).($myo->date2 != '0000-00-00 00:00:00'? ' tot '.time2date($myo->date2): ''),
		"<a href='$_file?view=$myo->id' title='".TXT_VIEW_ITEM."'>$myo->name</a>",
		$myo->active ? 		"<a href='$_file?deactivate=$myo->id&$_app' title='".TXT_DEACTIVATE_ITEM."'>$img[ok]</a>" :
							"<a href='$_file?activate=$myo->id&$_app' title='".TXT_ACTIVATE_ITEM."'>$img[notok]</a>",

		//The options last
		"<a href='$_file?edit=$myo->id&$_app' name='".TXT_EDIT_ITEM."'>$img[calendar_edit]</a>
		 <a href='$_file?delete=$myo->id&$_app' name='".TXT_DELETE_ITEM."'>$img[calendar_del]</a>
		"
	);
}

//Output data table
$DataTable = new DataTable();
$DataTable->setMarkAllOptions(	array(TXT_DELETE, 		'delete', 		TXT_DELETE_ITEMS),
								array(TXT_ACTIVATE, 	'activate', 	TXT_ACTIVATE_ITEMS),
								array(TXT_DEACTIVATE, 	'deactivate', 	TXT_DEACTIVATE_ITEMS));
$DataTable->showSearch();
$DataTable->setDataRows($data_rows);
$DataTable->setLegendRow(
	array('#',TXT_DATE,TXT_NAME,TXT_ACTIVE,TXT_OPTIONS),
	array(60,0,0,60,40),
	array('id','date',false,'active')
);
echo $html.$DataTable->getHTML();

require_once('inc/cms_footer.php');
?>