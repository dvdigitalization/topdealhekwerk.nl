<?
/*
Title:		Product colors management [TOPDEAL]
File: 		cms_topdeal_colors.php
Version: 	v2.20
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

/******************************************************************************************
 * Settings
 ***/

// *** Required files
require_once('inc/cms_header.php');
require_once(CMS_PATH_PREFIX.'classes/class_ImageHandler.php');
require_once(CMS_PATH_PREFIX.'classes/class_DataTable.php');
require_once(CMS_PATH_PREFIX.'classes/class_DBItem.php');

// *** Database table
$db_table				=	DIGI_DB_PREFIX.'topdeal_colors';
$db_table_products		=	DIGI_DB_PREFIX.'topdeal_products';

// *** File definitions
$file_products			= 	'cms_topdeal_products.php';

// *** Image settings
$image_dir 				= 	'../gfx/colors';
$image_size				=	16;
$image_constraint		=	CONSTRAINT_SIZE;
$allowed_extensions		=	array('jpg','jpeg','gif','png');
$max_upload_bytes		=	returnBytes(ini_get('upload_max_filesize'));
$max_upload_size		=	byte2kb($max_upload_bytes);
$max_script_time		=	round(ini_get('max_execution_time')/60);

// *** Object definition
$object					=	'Kleur';
$object_lc 				= 	'kleur';
$plural					=	'Kleuren';
$plural_lc 				= 	'kleuren';
$adjective_e			=	'e';
$ident					=	'Deze';
$id_format				=	'';			//Sprintf format for the id

// *** General settings
$perma_delete			=	true;		//Perma delete, or set deleted = 1 ?
$manually_sortable		=	true;		//Is this list manually sortable?
$activatable			=	false;		//Is this item activatable/de-activatable?
$dependent_tables		=	array($db_table_products);	//Tables with entries that are dependent on this item (only for perma delete)
$dependent_fields		=	array('color_id');	//The corresponding fields where the id of this DB item is found (only for perma delete)

// *** Define all property fields and corresponding title
$name_field				=	'name';		//Naming field
$name_title				=	'Naam';		//Naming title
$extra_props			=	array();
$extra_prop_functions	=	array();

// *** Sorting and pages functionality (depends on manual sorting or not)
if ($manually_sortable) {
	$default_sort_option	=	'item_order';
	$default_sort_method	=	'ASC';
	$secondary_sort_option	=	'';
	$secondary_sort_method	=	'';
	$valid_sort_options		=	array();
}
else {
	$default_sort_option	=	$name_field;
	$default_sort_method	=	'ASC';
	$secondary_sort_option	=	'';
	$secondary_sort_method	=	'';
	$valid_sort_options		=	array('id',$name_field,'active');
}

// *** Perma delete constraint
if (!$perma_delete) {
	$where[]			= 	"deleted = '0'";
}

// *** Search constraints
if (!empty($search_query)) {
	$search				=	array();
	$search[]			=	"$name_field LIKE '%$search_query%'";
	$where[]			=	'('.implode(' OR ', $search).')';
}

// *** Messages
$msg_added				=	"$object toegevoegd.";
$msg_editted			=	"$object gegevens bijgewerkt.";
$msg_deleted			=	"$object verwijderd.";
$msg_deleted_m			=	"$plural verwijderd.";
$msg_delete_confirm		=	"$ident $object_lc zeker weten verwijderen?";
$msg_delete_confirm_m	=	"Deze $plural_lc zeker weten verwijderen?";
$msg_could_not_delete 	= 	"$object kon niet verwijderd worden. Er bestaan nog producten met dit $object_lc.";
$msg_not_all_deleted	=	$msg_deleted_m." Sommige $plural_lc konden niet verwijderd worden, omdat er nog producten bestaan met dit $object_lc.";
$msg_activated			=	"$object geactiveerd.";
$msg_activated_m		=	"$plural geactiveerd.";
$msg_deactivated		=	"$object op non-actief gezet.";
$msg_deactivated_m		=	"$plural op non-actief gezet.";
$msg_upload_error		=	'Er ging iets mis tijdens het uploaden van de afbeelding.';
$msg_resize_error		=	'Er kon geen verkleinde versie van de afbeelding worden gemaakt.';
$msg_directory_error	=	'De doelmap '.$image_dir.' bestaat niet.';

// *** Sorting and pages and link appendix
require_once(CMS_PATH_PREFIX.'inc/sorting_and_pages.php');
$_app					=	$page_appendix.'&'.$sort_appendix.'&'.$link_appendix;

// *** Initialize vars
$other_props 			= 	'';

/******************************************************************************************
 * Image folder check
 ***/

$image_dir_exists = checkFolder($image_dir);

/******************************************************************************************
 * POST and GET Cases
 ***/

if (isset($_POST['add_finish']) or isset($_POST['edit_finish'])) {
	foreach (array_keys($_POST) as $key) {
		evalAll($_POST[$key]);
	}
}

if (isset($_POST['add_finish'])) {
	$Item = new DBItem();
	$Item->readPostVars();
	$db_id = $Item->save();
	$Message->set($msg_added);
}

if (isset($_POST['edit_finish']) and $db_id = (int) $_POST['id']) {
	$Item = new DBItem($db_id);
	$Item->readPostVars();
	$Item->save();
	$Message->set($msg_editted);
}

//Image handling
if (isset($_POST['add_finish']) or (isset($_POST['edit_finish']) and isset($_POST['id']) and $db_id = (int) $_POST['id'])) {

	//Resize image
	if ($image_dir_exists and isset($_FILES['image']['name']) and !empty($_FILES['image']['name'])) {

	 	$image_file	=	'color_'.$db_id;					//WITHOUT EXTENSION
		$image_url 	= 	$image_dir.$_sep.$image_file;

		//Move new image to dir
		if (($result = uploadFile('image', $image_dir, $image_file, $chmod, true, true, $allowed_extensions)) !== false) {

			list($file_name, $file_extension, $file_size) = $result;

			//Resize and resample as JPG
			$ImageHandler	=	new ImageHandler($image_url.$file_extension, $image_url.'.jpg');
			//$ImageHandler->setTargetSizeConstraint($image_size, $image_constraint);
			if ($ImageHandler->output()) {

				//Remove the original file if it was not a jpg
				if ($file_extension != '.jpg') {
					unlink($image_url.$file_extension);
					$file_extension = '.jpg';
				}
			}
			else {
				$Message->add($msg_resize_error.'<br/><br/>'.$_error, WARNING);
			}
		}
		else {
			$Message->add($msg_upload_error.'<br/><br/>'.$_error, WARNING);
		}
	}
	else if (isset($_FILES['image']['name']) and !empty($_FILES['image']['name'])) {
		$Message->add($msg_directory_error, WARNING);
	}
}

// *** Sorting
if (isset($_GET['move_up']) and $db_id = (int) $_GET['move_up'] and $manually_sortable and noBrowse()) {
	$Item = new DBItem($db_id);
	$Item->move_up();
}
if (isset($_GET['move_down']) and $db_id = (int) $_GET['move_down'] and $manually_sortable and noBrowse()) {
	$Item = new DBItem($db_id);
	$Item->move_down();
}

// *** Activation
if (isset($_GET['activate']) and $id_holder = $_GET['activate'] and $activatable and noBrowse()) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {

		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			foreach ($db_ids as $db_id) {
				$Item = new DBItem($db_id);
				$Item->activate();
			}
			$Message->set($msg_activated_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		$Item = new DBItem($db_id);
		$Item->activate();
		$Message->set($msg_activated);
	}
}

// *** De-activation
if (isset($_GET['de_activate']) and $id_holder = $_GET['de_activate'] and $activatable and noBrowse()) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {

		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			foreach ($db_ids as $db_id) {
				$Item = new DBItem($db_id);
				$Item->de_activate();
			}
			$Message->set($msg_deactivated_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		$Item = new DBItem($db_id);
		$Item->de_activate();
		$Message->set($msg_deactivated);
	}
}

// *** Delete confirmation
if (isset($_GET['delete']) and $id_holder = $_GET['delete']) {

	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			$Message->setConfirmMultiple($msg_delete_confirm_m, 'delete',$db_ids,$name_field);
		}
	}
	else if ($db_id = (int) $id_holder) {
		$Message->setConfirm($msg_delete_confirm,'delete',$db_id,$name_field);
	}
}

// *** Delete the items
if (isset($_GET['delete_confirm']) and $id_holder = $_GET['delete_confirm'] and noBrowse()) {

	if (isset($_GET['multiple_confirm']) and is_array($id_holder)) {

		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {

			//Set to zero
			$dependencies = 0;

			foreach ($db_ids as $db_id) {

				//Make new item object
				$Item = new DBItem($db_id);

				//Read item order, if manual sorting enabled
				if ($manually_sortable) $item_order = getMyo($db_table, $db_id, 'item_order');

				//Delete the image
				if (file_exists($image_dir.$_sep.'color_'.$db_id.'.jpg')) {
					unlink($image_dir.$_sep.'color_'.$db_id.'.jpg');
				}

				//Check if we can delete it
				if ($Item->delete($perma_delete)) {

					//If so, and if manual sorting enabled, fix the sort order
					if ($manually_sortable) fixSortOrder($db_table, 'item_order', $item_order);
				}

				//If we couldn't delete it, dependant items were found
				else {
					$dependencies++;
				}
			}

			//What message to show?
			if ($dependencies > 0) {
				$Message->set($msg_not_all_deleted, WARNING);
			}
			else {
				$Message->set($msg_deleted_m);
			}
		}
	}
	else if ($db_id = (int) $id_holder) {

		//Make new item object
		$Item = new DBItem($db_id);

		//Read item order, if manual sorting enabled
		if ($manually_sortable) $item_order = getMyo($db_table, $db_id, 'item_order');

		//Delete the image
		if (file_exists($image_dir.$_sep.'color_'.$db_id.'.jpg')) {
			unlink($image_dir.$_sep.'color_'.$db_id.'.jpg');
		}

		//Check if we can delete it
		if ($Item->delete($perma_delete)) {

			//If so, and if manual sorting enabled, fix the sort order
			if ($manually_sortable) fixSortOrder($db_table, 'item_order', $item_order);

			//Set the message
			$Message->set($msg_deleted);
		}

		//If we couldn't delete it, dependant items were found
		else {
			$Message->set($msg_could_not_delete, CRITICAL);
		}
	}
}

/******************************************************************************************
 * Add / Edit / View options
 ***/

if (isset($_GET['add'])) {

	foreach ($extra_props as $prop_name => $prop_title) {
		$other_props .= "
		   \n<tr>
				<td>$prop_title:</td>
				<td><input type='text' name='$prop_name' class='input_regular'/></td>
			</tr>";
	}

	$pre_html = "

		<h3>$object toevoegen:</h3><hr/><br/>
		<form name='add_form' method='post' action='$_file' enctype='multipart/form-data'>

        <table cellspacing='0px' cellpadding='1px' border='0px'>

        <tr>
			<td width='150px'>$name_title:</td>
			<td><input type='text' name='$name_field' class='input_regular'/></td>
		</tr>
		$other_props
		<tr>
			<td>Kleurvakje:</td>
			<td><input type='file' name='image' class='input_file' />
			<b>Max. $max_upload_size / $max_script_time minuten</b></td>
		</tr>
		<tr>
			<td>&nbsp;</td><td><br/><input type='submit' name='add_finish' class='input_submit' value='Toevoegen' /></td>
		</tr>
		</table>
		</form><br/>
	";
}

if (isset($_GET['edit']) and $db_id = (int) $_GET['edit']) {

	$Item = new DBItem($db_id);

	foreach ($extra_props as $prop_name => $prop_title) {

		$prop_value = $Item->getVar($prop_name);
		if (isset($extra_prop_functions[$prop_name])) {
			$prop_value = $extra_prop_functions[$prop_name][0]($prop_value);
		}

		$other_props .= "
		   \n<tr>
				<td>$prop_title:</td>
				<td><input type='text' name='$prop_name' value='$prop_value' class='input_regular'/></td>
			</tr>";
	}

	//Image
	$image_url 		= $image_dir.$_sep.'color_'.$db_id.'.jpg';
	$current_image 	= file_exists($image_url) ? "<img src='$image_url' />" : 'geen';

	$pre_html = "

		<h3>$object gegevens bijwerken:</h3><hr/><br/>
		<form name='edit_form' method='post' action='$_file' enctype='multipart/form-data'>
		<input type='hidden' name='id' value='$db_id'/>

        <table cellspacing='0px' cellpadding='1px' border='0px'>

        <tr>
			<td width='150px'>$name_title:</td>
			<td><input type='text' name='$name_field' value='".$Item->getVar($name_field)."' class='input_regular'/></td>
		</tr>
		$other_props
		<tr>
			<td valign='top'><br/>Kleurvakje:</td>
			<td><br/>$current_image</td>
		</tr>
		<tr>
			<td>Wijzigen:</td>
			<td><input type='file' name='image' class='input_file'/>
			<b>Max. $max_upload_size / $max_script_time minuten</b></td>
		</tr>
		<tr>
			<td>&nbsp;</td><td><br/><input type='submit' name='edit_finish' class='input_submit' value='Opslaan' /></td>
		</tr>
		</table>
		</form><br/>
	";
}

if (isset($_GET['view']) and $db_id = (int) $_GET['view']) {

	$Item = new DBItem($db_id);

	foreach ($extra_props as $prop_name => $prop_title) {

		$prop_value = $Item->getVar($prop_name);
		if (isset($extra_prop_functions[$prop_name])) {
			$prop_value = $extra_prop_functions[$prop_name][0]($prop_value);
		}

		$other_props .= "
		   \n<tr>
				<td>$prop_title:</td>
				<td>$prop_value</td>
			</tr>";
	}

	//Image
	$image_url 		= $image_dir.$_sep.'color_'.$db_id.'.jpg';
	$current_image 	= file_exists($image_url) ? "<img src='$image_url' />" : 'geen';

	$pre_html = "

		<h3>$object gegevens:</h3><hr/><br/>

        <table cellspacing='0px' cellpadding='1px' border='0px'>
        <tr>
			<td width='150px'>$name_title:</td>
			<td>".$Item->getVar($name_field)."</td>
		</tr>
		$other_props
		<tr>
			<td valign='top'>Kleurvakje:</td>
			<td>$current_image</td>
		</tr>
		</table>
		<br/>
	";
}

/******************************************************************************************
 * Page contents
 ***/

//Page title HTML
$html = 	"
	<a href='$_file'><img src='$icon[note_list]' border='0px' class='cms_icon'/></a>
	<h6>
		<a href='$_file'>overzicht</a> &nbsp;
		$img[note_add] <a href='$_file?add=1&$_app'>nieuw$adjective_e $object_lc</a> &nbsp;
		$img[product_next] <a href='$file_products'>terug naar de producten</a>
	</h6>
	<br/>".$Message->get()."
	<br/><br/>$pre_html
";

//Get items for this page and this sorting method
$res = eq("	SELECT 		id, $name_field AS name, active
			FROM 		$db_table $where_clause
			ORDER BY 	$order_clause
			LIMIT 		$limit_clause
;");
$data_rows = array();

while ($myo = mfo($res)) {

	//Image
	//$image_url 	= $image_dir.$_sep.'color_'.$myo->id.'.jpg';
	//$image_url	= file_exists($image_url) ? "<img src='$image_url' />" : '';

	$data_rows[] = array(

		//This MUST be the id
		$myo->id,

		//All desired other fields
		!empty($id_format) ? sprintf($id_format, $myo->id) : $myo->id,
		//$image_url,
		"<a href='$_file?view=$myo->id&$_app' title='$object gegevens inzien'>$myo->name</a>",
		$activatable ? ($myo->active ? "
		 <a href='$_file?de_activate=$myo->id&$_app' title='$object de-activeren'>$img[ok]</a>" : "
		 <a href='$_file?activate=$myo->id&$_app' title='$object activeren'>$img[notok]</a>"
		) : $img['ok'],

		//The options last
		($manually_sortable ? "
		 <a href='$_file?move_up=$myo->id&$_app' title='$object omhoog verplaatsen'>$img[up]</a>
		 <a href='$_file?move_down=$myo->id&$_app' title='$object omlaag verplaatsen'>$img[down]</a>
		" : '').
		"<a href='$_file?edit=$myo->id&$_app' title='$object gegevens bijwerken'>$img[edit]</a>
		 <a href='$_file?delete=$myo->id&$_app' title='$object verwijderen'>$img[note_del]</a>"
	);
}

//Output data table
$DataTable = new DataTable();
if ($activatable) {
	$DataTable->setMarkAllOptions(	array('verwijder','delete',$plural.' verwijderen'),
									array('activeren','activate',$plural.' activeren'),
									array('de-activeren','de_activate',$plural.' de-activeren'));
}
else {
	$DataTable->setMarkAllOptions(	array('verwijder','delete',$plural.' verwijderen'));
}
$DataTable->showSearch();
$DataTable->setDataRows($data_rows);

if ($manually_sortable) {
	$DataTable->setLegendRow(
		array('#',/*'',*/$name_title,'Actief','Opties'),
		array(60,/*40,*/0,60,80),
		array()
	);
}
else {
	$DataTable->setLegendRow(
		array('#',/*'',*/$name_title,'Actief','Opties'),
		array(60,/*40,*/0,60,40),
		array('id','',$name_field, $activatable ? 'active' : '')
	);
}
echo $html.$DataTable->getHTML();

require_once('inc/cms_footer.php');
?>