<?php
/*
Title:		CMS auto updater
File: 		cms_auto_update.php
Version: 	v2.04
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

/******************************************************************************************
 * Settings
 ***/

// *** Required files
require_once('inc/cms_header.php'); 
require_once('inc/lang/'.$cms_lang.'_'.$_file);                     

/******************************************************************************************
 * Encode/decode funtions
 ***/
 
function u_encode($username) {
	global $sha1_hash;
	for($i = 0; $i < 3; $i++) {
		$username = strrev(base64_encode($username.$sha1_hash));
	}
	return $username;
}

function p_encode($password) {
	global $sha1_hash;
	for($i = 0; $i < 3; $i++) {
		$password = strrev(base64_encode($password.$sha1_hash.'HeW5'));
	}
	return $password;
}

function u_decode($username_encoded) {
	global $sha1_hash;
	for($i = 0; $i < 3; $i++) {
		$username_encoded = base64_decode(strrev($username_encoded));
		$username_encoded = substr($username_encoded,0,strlen($username_encoded)-strlen($sha1_hash));
	}
	return $username_encoded;
}

function p_decode($password_encoded) {
	global $sha1_hash;
	for($i = 0; $i < 3; $i++) {
		
		$password_encoded = base64_decode(strrev($password_encoded));
		$password_encoded = substr($password_encoded,0,strlen($password_encoded)-strlen($sha1_hash.'HeW5'));
	}
	return $password_encoded;
}

/******************************************************************************************
 * Page contents
 ***/

//Page title HTML
$html = 	"
	<a href='$_file'><img src='$site_icon' border='0px' class='cms_icon'/></a>
	<h6>
		<a href='$_file'>".TXT_OVERVIEW."</a>
	</h6>
";

echo $html;

require_once('inc/cms_footer.php');
?>