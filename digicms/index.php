<?php
/*
Title:		CMS index page
File: 		index.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

/******************************************************************************************
 * Settings
 ***/

require_once('inc/cms_header.php');

/******************************************************************************************
 * Page contents
 ***/

//Page title HTML
$html =  "
	<br/><div style='margin-top: -40px;'>".$Message->get()."<br/><br/></div>
";

//Vars
$icons_html		= 	array();
$sections		=	array();
$last_is_cat	= 	false;
$current_cat    = 	0;

// *** Make the menu HTML
foreach($menu as $section_name => $section_modules) {

	//Increment cat counter
	$current_cat++;
		
	//Menu old or new style
	$section_name = $menu_old_style ? $section_name : constant($section_name);
	
	//Fix the name if double (backwards compatibility)
	if ($menu_old_style) {
		$last_bit = substr($section_name,-4);
		if ($last_bit == ' (2)' or $last_bit == ' (3)') {
			$section_name = substr($section_name, 0, strlen($section_name) - 4);
		}
	}
	
	//Set the name
	$sections[$current_cat]	= $section_name;
	
	//Re-set the counter
	$items_count = 0;

	//Initialize the html for this cat
	$icons_html[$current_cat] = '';
	
	//All modules in this section
	foreach ($section_modules as $module_id) {
		
		//Increment count
		$items_count++;
		
		//Make new cat, if past 5 items
		if ($items_count > 5) {
			$items_count = 1;
			$current_cat++;
			$icons_html[$current_cat]	= 	'';
			$sections[$current_cat]		= 	$section_name;
		}
		
		//Menu old or new style
		$module_id = $menu_old_style ? $module_id : constant($module_id);
		
		//Installation check
		if (!isset($installed_modules[$module_id])) {  			
			continue;
		}
		
		//Read module properties
		$menu_priv = key($modules_in_use[$module_id][2]);
		$menu_file = $modules_in_use[$module_id][1][0];
		$menu_icon = $modules_in_use[$module_id][6];
		$menu_icon = isset($icon[$menu_icon]) ? $icon[$menu_icon] : (strstr($menu_icon,'.') ? '../gfx/cms/icons/'.$menu_icon : '../gfx/cms/icons/'.$menu_icon.'.png');
		$menu_name = $modules_in_use[$module_id][0];  	

		if (is_array($menu_name)) {
			$menu_name = $menu_name[0];
		}
				
		//This is a menu item, check the priv
		if (($menu_priv and checkPriv($menu_priv)) or !$menu_priv) {
			
			//The category is not empty
			$last_is_cat = false;

			$icons_html[$current_cat] .= "
				<div class='icon_div'>
					<a href='$menu_file' title='$menu_name'><img src='$menu_icon' border='0px' /></a>
					<br/><h6><a href='$menu_file' title='$menu_name'>$menu_name</a></h6>
				</div>";
		}           
	}
}

foreach ($icons_html as $cat => $cat_html) {

	//Make the html if items inside
	if ($cat_html) {
		
		//The current cat's div tags
		$icons_html[$cat] 	=	"
			<div class='group_container_div'>
				<div class='icons_header'>".$sections[$cat]."</div>
				<div class='icons_div'>
				".$cat_html."
				</div>
			</div>
		";
				
	}
}

echo $html.implode("\n", $icons_html);

require_once('inc/cms_footer.php');
?>