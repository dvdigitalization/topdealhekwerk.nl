<?php
/*
Title:		Backup MySQL data, create restore points
File: 		cms_backup.php
Version: 	v2.04
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

/******************************************************************************************
 * Settings
 ***/

require_once('inc/cms_header.php');
require_once('inc/lang/'.$cms_lang.'_'.$_file);
require_once(CMS_PATH_PREFIX.'classes/class_DataTable.php');
require_once(CMS_PATH_PREFIX.'classes/class_Backup.php');

// *** Database table
$db_table				=	DIGI_DB_PREFIX.'restore_points';

// *** Sorting and pages functionality
$default_sort_option	=	'created_on';
$default_sort_method	=	'DESC';
$secondary_sort_option	=	'';
$secondary_sort_method	=	'';
$valid_sort_options		=	array('id','created_on');

// *** Message definitions
$msg_mailed				.=	' '.$cfg['EMAIL_BACKUPS_ADDRESS'];

// *** Backup dir
if (isset($private_folder) and !empty($private_folder) and is_writable($private_folder) 
	and !file_exists($private_folder.'backup')) {
		
	mkdir($private_folder.'backup');
	if ($chmod) {
		chmod($private_folder.'backup', $chmod);
	}
}

if (isset($private_folder) and !empty($private_folder) and is_dir($private_folder.'backup') 
	and is_writable($private_folder.'backup')) {
	$backup_dir 	= 	$private_folder.'backup';
}
else { 
	$backup_dir 	= 	'backup';
}

// *** Sorting and pages and link appendix
require_once(CMS_PATH_PREFIX.'inc/sorting_and_pages.php');
$_app					=	$page_appendix.'&'.$sort_appendix.'&'.$link_appendix;

/******************************************************************************************
 * POST and GET Cases
 ***/
 
// *** Download a backup file
if (isset($_GET['download']) and $db_id = (int) $_GET['download']) {
	
	$filename = getMyo($db_table, $db_id, 'file');
	$file_url = $backup_dir.$_sep.$filename;
	if ($filename and file_exists($file_url)) {
		header('Content-Type: application/octet-stream');
		header('Content-Length: '.filesize($file_url));
		header('Content-Disposition: attachment; filename="'.$filename.'"');
		readfile($file_url);
		exit;
	}
	else {
		$Message->set($msg_file_not_exist, CRITICAL);
	}
}

// *** Create restore points
if (isset($_GET['create_restore_point']) and noBrowse() and checkPriv(PRIV_BACKUP)) {

	$Backup	= new Backup();
		
	if ($db_id = $Backup->makeRestorePoint()) {
		$Backup->deleteOldRestorePoints();	
		cmsLog("$object ".date('d-m-Y H:i')." (#$db_id) aangemaakt.");
		$Message->set($msg_backupped.($cfg['EMAIL_BACKUP_FILES'] ? $msg_mailed : ''));
	}
	else {
		$Message->set($msg_not_backupped, CRITICAL);
	}
}

// *** Restore confirmation
if (isset($_GET['restore']) and $db_id = (int) $_GET['restore'] and checkPriv(PRIV_RESTORE)) {
	$Message->setConfirm($msg_restore_confirm, 'restore', $db_id, 'file');
}

// *** Restore
if (isset($_GET['restore_confirm']) and $db_id = (int) $_GET['restore_confirm'] and noBrowse() and checkPriv(PRIV_RESTORE)) {

	$Backup	= new Backup();
		
	if ($Backup->loadRestorePoint($db_id)) {		
		cmsLog("$object (#$db_id) teruggezet.");
		$Message->set($msg_restored);
	}
	else {
		$Message->set($msg_not_restored, CRITICAL);
	}
}

// *** Delete confirm
if (isset($_GET['delete']) and $id_holder = $_GET['delete'] and checkPriv(PRIV_BACKUP)) {
	
	if (isset($_GET['multiple']) and is_array($id_holder)) {
		$db_ids = validateArrayInt($id_holder);
		$Message->setConfirmMultiple($msg_delete_confirm_m, 'delete', $db_ids, 'file');	
	}
	else if ($db_id = (int) $id_holder) {
		$Message->setConfirm($msg_delete_confirm, 'delete', $db_id, 'file');
	}	
}

// *** Delete
if (isset($_GET['delete_confirm']) and $id_holder = $_GET['delete_confirm'] and noBrowse() and checkPriv(PRIV_BACKUP)) {

	if (isset($_GET['multiple_confirm']) and is_array($id_holder)) {

		$db_ids = validateArrayInt($id_holder);
		if (count($db_ids)) {
			foreach ($db_ids as $db_id) {
				$Backup	= new Backup();
				$Backup->deleteRestorePoint($db_id);
			}
			cmsLog("Meerdere $plural_lc verwijderd.");
			$Message->set($msg_deleted_m);
		}
	}
	else if ($db_id = (int) $id_holder) {
		$Backup	= new Backup();
		if ($Backup->deleteRestorePoint($db_id)) {
			cmsLog("$object (#$db_id) verwijderd.");
			$Message->set($msg_deleted);
		}
	}
}

/******************************************************************************************
 * Page contents
 ***/

//Page title HTML
$html = 	"
	<a href='$_file' title='".ucfirst(TXT_OVERVIEW)."'><img src='$site_icon' border='0px' class='cms_icon'/></a>
	<h6>
		<a href='$_file'>".TXT_OVERVIEW."</a> &nbsp;
		".(checkPriv(PRIV_BACKUP) ? "
		 $img[backup_add] <a href='$_file?create_restore_point=1&$link_appendix'>".TXT_BACKUP_NOW."</a>
		" : '')."
	</h6>
	<br/>".$Message->get()."
	<br/><br/>$pre_html
";

//Build the data rows
$res = eq("	SELECT 		id, UNIX_TIMESTAMP(created_on) as created_on, file
			FROM 		$db_table $where_clause
			ORDER BY 	$order_clause
			LIMIT 		$limit_clause
;"); 
while ($myo = mfo($res)) {

	$file		=	$backup_dir.$_sep.$myo->file;
	$file_ok 	= 	file_exists($file);

	$data_rows[] = array(
		
		//This MUST be the ID
		$myo->id,
		
		//All other desired fields
		$myo->id,
		date('d/m/Y h:i', $myo->created_on),
		$file_ok ? byte2kb(filesize($file)) : '',
		$file_ok ? $img['ok'] : $img['notok'],
		
		//The options last				
		(($file_ok and checkPriv(PRIV_BACKUP)) ? "
		<a href='$_file?download=$myo->id&$_app' title='".TXT_DOWNLOAD_BACKUP_FILE."'>$img[backup_down]</a>		
		<a href='$_file?restore=$myo->id&$_app' title='".TXT_LOAD_RESTORE_POINT."'>$img[process]</a>
		" : '').(checkPriv(PRIV_BACKUP) ? "
		<a href='$_file?delete=$myo->id&$_app' title='".TXT_DELETE_RESTORE_POINT."'>$img[backup_del]</a>
		" : '')
	);
}

//Custom title
$total_items  = countResults($db_table, $where_clause);
$amount_text  = ($total_items == 1) ? TXT_ONE_RESTORE_POINT : str_replace('[AMOUNT]', $total_items, TXT_MULTIPLE_RESTORE_POINTS);
$amount_text .= ' ('.diskUsage($backup_dir, null, true, 1000).')';
			
//Base HTML
$html .= "
	\n<div style='width: 700px;'>".str_replace('[AMOUNT]', $cfg['MAX_AMOUNT_OF_DUMPS'], TXT_AUTO_BACKUP_EXPLANATION_1)." <a href='cms_settings.php'>".MOD_NAME_SETTINGS."</a> ".TXT_AUTO_BACKUP_EXPLANATION_2.".<br/><br/>".($cfg['MAKE_AUTO_BACKUPS'] ? str_replace('[DAYS]',$cfg['DUMP_INTERVAL'],TXT_AUTO_BACKUPS) : TXT_NO_AUTO_BACKUPS)."</div><br/>";

//Output data table
$DataTable = new DataTable($amount_text);
$DataTable->setMarkAllOptions(array(TXT_DELETE, 'delete', TXT_DELETE_RESTORE_POINTS));
$DataTable->setDataRows($data_rows);
$DataTable->setLegendRow(
	array('#',TXT_DATE_AND_TIME_OF_BACKUP,TXT_SIZE,TXT_STATUS,TXT_OPTIONS),
	array(40,0,70,70,60),
	array('id','created_on','','')
);
echo $html.$DataTable->getHTML();	

require_once('inc/cms_footer.php');
?>