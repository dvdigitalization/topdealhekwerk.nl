<?php
/*
Title:		cms_backup.php language definitions (NL)
File: 		inc/lang/NL_cms_backup.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Backup punt';
$object_lc 				= 	'backup punt';
$plural					=	'Backup punten';
$plural_lc				=	'backup punten';

// *** Message definitions
$msg_deleted			=	'Backup punt verwijderd.';
$msg_deleted_m			=	'Backup punten zijn verwijderd.';
$msg_backupped			=	'Backup punt aangemaakt.';
$msg_restored			=	'Backup punt terug gezet.';
$msg_not_restored		=	'Backup punt kon niet terug gezet worden.';
$msg_not_backupped		=	'Backup punt kon niet worden aangemaakt.';
$msg_delete_confirm		=	'Dit backup punt zeker weten verwijderen?';
$msg_delete_confirm_m	=	'Deze backup punten zeker weten verwijderen?';
$msg_file_not_exist		=	'Het backup bestand kan niet worden gevonden.';
$msg_restore_confirm	=	'Weet u zeker dat u de gegevens van dit backup punt wilt terugzetten over de huidige gegevens?
							<br/>De huidige gegevens zullen worden opgeslagen in een nieuw backup punt.';
$msg_mailed				=	'<br/>Backup bestand is ge-emailed naar';

// *** Form and text elements
define('TXT_BACKUP_NOW',				'nu een backup punt maken');
define('TXT_DOWNLOAD_BACKUP_FILE',		'Backup bestand downloaden');
define('TXT_LOAD_RESTORE_POINT',		'Dit backup punt laden');
define('TXT_DELETE_RESTORE_POINT',		'Dit backup punt verwijderen');
define('TXT_ONE_RESTORE_POINT',			'Er is 1 backup punt aangemaakt');
define('TXT_MULTIPLE_RESTORE_POINTS',	'Er zijn [AMOUNT] backup punten aangemaakt');
define('TXT_DELETE_RESTORE_POINTS',		'Backup punten verwijderen');
define('TXT_AUTO_BACKUP_EXPLANATION_1',	'Hieronder bevinden zich de backup punten die gemaakt zijn van het systeem. Er worden maximaal [AMOUNT] backup punten tegelijk bewaard, de oudste zal steeds automatisch worden verwijderd. Deze instellingen zijn te wijzigen onder bij de');
define('TXT_AUTO_BACKUP_EXPLANATION_2',	'van het CMS');
define('TXT_AUTO_BACKUPS',				'Er wordt om de [DAYS] dagen automatisch een backup gemaakt van het systeem.');
define('TXT_NO_AUTO_BACKUPS',			'Momenteel worden er geen automatische backups gemaakt.');
define('TXT_DATE_AND_TIME_OF_BACKUP',	'Datum en tijdstip van backup');
define('TXT_SIZE',						'Grootte');
define('TXT_STATUS',					'Status');

?>