<?php
/*
Title:		cms_auto_update.php language definitions (EN)
File: 		inc/lang/EN_cms_auto_update.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Message definitions
$msg_no_updates				=	'No new updates are available at the moment.';
$msg_no_soap				=	'The system is unable to connect to the update server at this moment. Please try again later.';
$msg_no_ftp_data			=	'You have to provide your server\'s FTP settings to continue the update process..';
$msg_update_completed		=	'The update process was successful. Your CMS version is now up-to-date.';

// *** Form and text elements
define('TXT_AUTO_UPDATE',				'Automatic update');
define('TXT_TO_VERSION',				'Update to version');
define('TXT_AUTOMATIC',					'The update process will be performed automatically. You can start the process now.');
define('TXT_THROUGH_FTP',				'through FTP');
define('TXT_PROVIDE_FTP',				'You have to provide your server\'s FTP settings to continue the update process.');
define('TXT_MANUAL_UPDATE',				'Manual update');
define('TXT_AUTOMATIC_IMPOSSIBLE',		'Unfortunately, it is not possible to automatically update your CMS system.');
define('TXT_DOWNLOADMANUALLY',			'You will have to download the update file manually and extract it to your server.');
define('TXT_DOWNLOAD_UPDATE',			'Download the update file');
define('TXT_UPDATE_DATABASE',			'After you have extracted the files to your server, you have to update the database.');
define('BUTTON_UPDATE_DATABASE',		'Update database');
define('BUTTON_UPDATE',					'Update');
define('TXT_FTP_SERVER',				'FTP server');
define('TXT_FTP_USER',					'FTP username');
define('TXT_FTP_PASS',					'FTP password');
define('TXT_FTP_FOLDER',				'Destination folder');
define('TXT_FTP_REMEMBER',				'Remember username and password?');
define('TXT_RELEASED_ON',				'Released on');
define('TXT_NEW',						'New');
define('TXT_VERSIONS',					'versions');
define('TXT_AVAILABLE',					'available');
define('TXT_NEW_UPDATES_FOUND',			'New updates were found for your CMS version. You can read more information about these updates below and perform the update afterwards.');
define('TXT_PERFORM_UPDATE',			'Perform update');

?>