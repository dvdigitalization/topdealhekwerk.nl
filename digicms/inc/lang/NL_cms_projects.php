<?php
/*
Title:		cms_projects.php language definitions (NL)
File: 		inc/lang/NL_cms_projects.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Project';
$object_lc 				= 	'project';
$plural					=	'Projecten';
$plural_lc				=	'projecten';

// *** Form and text elements
define('TXT_DEADLINE_ALERT_MAIL',		"Beste [NAME],<br/><br/>Van de volgende projecten zal de deadline binnen [DAYS] dagen verlopen:<br/><br/>[PROJECTS]");
define('TXT_DEADLINE_MAIL_SUBJECT',		'Projecten deadline waarschuwing');

?>