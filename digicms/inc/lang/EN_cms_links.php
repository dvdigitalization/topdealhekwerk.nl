<?php
/*
Title:		cms_links.php language definitions (EN)
File: 		inc/lang/EN_cms_links.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Links';
$object_lc 				= 	'link';
$plural					=	'Link';
$plural_lc				=	'links';

// *** Message definitions
$msg_added 				=	'The link has been added.';
$msg_editted			=	'The links has been editted.';
$msg_deleted			=	'The link has been deleted.';
$msg_deleted_m			=	'The links have been deleted.';
$msg_delete_confirm		=	'Are you sure you want to delete this link?';
$msg_delete_confirm_m	=	'Are you sure you want to delete these links?';
$msg_deactivated		=	'The link will no longer be displayed on the website.';
$msg_deactivated_m		=	'The links will no longer be displayed on the website.';
$msg_activated			=	'The link will be displayed on the website.';
$msg_activated_m		=	'The links will be displayed on the website.';

// *** Form and text elements
define('TXT_ADD_ITEM',					'Add '.$object_lc);
define('TXT_EDIT_ITEM',					'Edit '.$object_lc);
define('TXT_VIEW_ITEM',					'View '.$object_lc.' details');
define('TXT_DELETE_ITEM',				'Delete '.$object_lc);
define('TXT_ACTIVATE_ITEM',				'Activate '.$object_lc);
define('TXT_DEACTIVATE_ITEM',			'De-activate '.$object_lc);
define('TXT_ITEM_UP',					'Move '.$object_lc.' up');
define('TXT_ITEM_DOWN',					'Move '.$object_lc.' down');
define('TXT_DELETE_ITEMS',				'Delete '.$plural_lc);
define('TXT_ACTIVATE_ITEMS',			'Activate '.$plural_lc);
define('TXT_DEACTIVATE_ITEMS',			'De-activate '.$plural_lc);
define('TXT_NEW_ITEM',					'new link');
define('TXT_CONTENTS',					'Contents');
define('TXT_URL',						'URL');

?>