<?php
/*
Title:		cms_page_categories.php language definitions (NL)
File: 		inc/lang/NL_cms_page_categories.php
Version: 	v2.20
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Categorie';
$object_lc 				= 	'categorie';
$plural					=	'Categorieen';
$plural_lc				=	'categorieen';

// *** Message definitions
$msg_added 				=	'De categorie is toegevoegd.';
$msg_editted			=	'De categorie is gewijzigd.';
$msg_deleted			=	'De categorie is verwijderd.';
$msg_deleted_m			=	'De categorieen zijn verwijderd.';
$msg_delete_confirm		=	'Deze categorie zeker weten verwijderen?';
$msg_delete_confirm_m	=	'Deze categorieen zeker weten verwijderen?';

// *** Form and text elements
define('TXT_ADD_ITEM',					$object.' toevoegen');
define('TXT_EDIT_ITEM',					$object.' wijzigen');
define('TXT_VIEW_ITEM',					$object.' gegevens inzien');
define('TXT_DELETE_ITEM',				$object.' verwijderen');
define('TXT_DELETE_ITEMS',				$plural.' verwijderen');
define('TXT_NEW_ITEM',					'nieuwe categorie');
define('TXT_BACK_TO_PAGES',				'terug naar de pagina\'s');

?>