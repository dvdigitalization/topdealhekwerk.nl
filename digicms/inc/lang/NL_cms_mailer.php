<?php
/*
Title:		cms_mailer.php language definitions (NL)
File: 		inc/lang/NL_cms_mailer.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Email';
$object_lc 				= 	'email';
$plural					=	'Emails';
$plural_lc				=	'emails';

$sent_object_lc			=	'verstuurde email';
$sent_plural_lc			=	'verstuurde emails';

$draft_object_lc		=	'concept email';
$draft_plural_lc		=	'concept emails';

// *** Message definitions
$msg_no_recipients		=	'Geen geadresseerden gevonden! Email niet verstuurd.';
$msg_queued				=	'Email is aangeboden aan het mail-systeem.';
$msg_saved				=	'Email opgeslagen';
$msg_sending_limits		=	'Er zullen maximaal [MAX] mailtjes per [MINS] minuten worden verstuurd.';
$msg_sent				=	'Email verstuurd aan [AMOUNT] geadresseerde(n).';
$msg_deleted			=	'Email verwijderd.';
$msg_deleted_m			=	'Emails verwijderd.';
$msg_delete_confirm		=	'Deze email zeker weten verwijderen?';
$msg_delete_confirm_m	=	'Deze emails zeker weten verwijderen?';
$msg_attachment_folder	=	'De map voor bijlagen bestaat niet en kan niet aangemaakt worden.';
$msg_upload_error		=	'Er ging iets mis tijdens het uploaden van een van de bijlagen.';
$msg_no_mail_addresses	=	"Nog geen email adressen ingesteld om mee te emailen!
							<br/>Dit kunt u bij de Algemene opties van het CMS invullen, onder de instellingen van 'Mailing'.";

// *** Form and text elements
define('TXT_NO_SUBJECT',				'(geen onderwerp)');
define('TXT_BCC_MESSAGE',				'Email verstuurd via de CMS mailer aan:');
define('TXT_BCC',						'BCC');
define('TXT_ADDRESSEES',				'geadresseerden');
define('TXT_SENT_TO',					'verstuurd aan');
define('TXT_SAVED',						'opgeslagen');
define('TXT_NEW_ITEM',					'nieuwe email');
define('TXT_TS_SENT',					'Verzonden');
define('TXT_TS_SAVED',					'Opgeslagen');
define('TXT_EDIT_ITEM',					'Email bewerken');
define('TXT_DELETE_ITEM',				'Email verwijderen');
define('TXT_SUBJECT',					'Onderwerp');
define('TXT_FROM',						'Van');
define('TXT_TO',						'Aan');
define('TXT_SEPARATE_NOTICE',			'Meerdere adressen scheiden met komma of puntkomma');
define('TXT_MESSAGE',					'Bericht');
define('TXT_ATTACHMENTS',				'Bijlage(n)');
define('TXT_MORE_ATTACHMENTS',			'meer bijlagen?');
define('BUTTON_SEND',					'Verstuur');

?>