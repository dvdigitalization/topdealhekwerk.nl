<?php
/*
Title:		cms_template.php language definitions (EN)
File: 		inc/lang/EN_cms_template.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Form';
$object_lc 				= 	'form';
$plural					=	'Forms';
$plural_lc				=	'forms';

// *** Message definitions
$msg_added 				=	'The form has been added.';
$msg_editted			=	'The form has been editted.';
$msg_deleted			=	'The form has been deleted.';
$msg_deleted_m			=	'The forms have been deleted.';
$msg_delete_confirm		=	'Are you sure you want to delete this form?';
$msg_delete_confirm_m	=	'Are you sure you want to delete these forms?';
$msg_deactivated		=	'The form will no longer be displayed on the website.';
$msg_deactivated_m		=	'The forms will no longer be displayed on the website.';
$msg_activated			=	'The form will be displayed on the website.';
$msg_activated_m		=	'The forms will be displayed on the website.';


// *** Form and text elements
define('TXT_ADD_ITEM',					'Add '.$object_lc);
define('TXT_EDIT_ITEM',					'Edit '.$object_lc);
define('TXT_VIEW_ITEM',					'View '.$object_lc.' details');
define('TXT_DELETE_ITEM',				'Delete '.$object_lc);
define('TXT_ACTIVATE_ITEM',				'Activate '.$object_lc);
define('TXT_DEACTIVATE_ITEM',			'De-activate '.$object_lc);
define('TXT_ARCHIVE_ITEM',				'Archive '.$object_lc);
define('TXT_UNARCHIVE_ITEM',			'Unarchive '.$object_lc);
define('TXT_ITEM_UP',					'Move '.$object_lc.' up');
define('TXT_ITEM_DOWN',					'Move '.$object_lc.' down');
define('TXT_DELETE_ITEMS',				'Delete '.$plural_lc);
define('TXT_ACTIVATE_ITEMS',			'Activate '.$plural_lc);
define('TXT_DEACTIVATE_ITEMS',			'De-activate '.$plural_lc);
define('TXT_ARCHIVE_ITEMS',				'Archive '.$plural_lc);
define('TXT_UNARCHIVE_ITEMS',			'Unarchive '.$plural_lc);
define('TXT_NEW_ITEM',					'new form');
define('TXT_CONTENTS',					'Contents');

?>