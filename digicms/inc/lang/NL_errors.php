<?php
/*
Title:		Digi error definitions (NL)
File: 		inc/lang/NL_errors.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

define('DIGI_ERR_SAFE_MODE',				'PHP safe_mode() staat aan. Sommige functies werken hierdoor mogelijk niet naar behoren.<br/>U wordt aangeraden om safe_mode() uit te zetten.');
define('DIGI_ERR_REG_GLOBALS',				'PHP register_globals() staat aan. Dit kan voor veiligheidsproblemen zorgen.<br/>U wordt aangeraden om register_globals() uit te zetten.');
define('DIGI_ERR_PASS_CHANGE_REQUIRED',		'U heeft uw initiele wachtwoord nog niet veranderd.<br/>U wordt sterk aangeraden dit zo snel mogelijk te wijzigen bij de Algemene opties van het CMS.');
define('DIGI_ERR_CFG_MENU_ELEMENTS',		'Let op: er zijn een of meer modules in het menu gedefinieerd die niet geinstalleerd zijn.<br/><br/>Controleer inc/config.php');

define('DIGI_ERR_UPLOAD_SIZE',				'Het bestand is te groot. Dit mag maximaal [SIZE] zijn.');
define('DIGI_ERR_UPLOAD_NO_FILE',			'Het bestand is ongeldig.');
define('DIGI_ERR_UPLOAD_NO_TMP',			'Er kan geen tijdelijke map gevonden worden om het bestand naar te uploaden.');
define('DIGI_ERR_UPLOAD_PARTIAL',			'Het bestand is niet helemaal geupload.');
define('DIGI_ERR_UPLOAD_CANTWRITE',			'Het bestand kon niet naar de tijdelijke map gekopieerd worden.');
define('DIGI_ERR_UPLOAD_NO_IMG',			'Het bestand moet een afbeelding zijn.');
define('DIGI_ERR_UPLOAD_FILENAME',			'Er is geen geldige bestandsnaam gevonden.');
define('DIGI_ERR_UPLOAD_EXISTS',			'Er bestaat al een bestand met deze naam.');
define('DIGI_ERR_UPLOAD_DEST_DIR',			'De doel map [DIR] bestaat niet of is niet bruikbaar.');
define('DIGI_ERR_UPLOAD_MOVING',			'Het bestand kon niet naar de doel map [DIR] gekopieerd worden.');
define('DIGI_ERR_UPLOAD_EXT',				'Het bestand moet een van de volgende types zijn: [EXTENSIONS]');
define('DIGI_ERR_UPLOAD_FIELD_NAME',		'Het veld [FIELD] is geen geldig bestands type veld.');
define('DIGI_ERR_UPLOAD_HACK',				'Er wordt mogelijk met het CMS geknoeid.');

define('DIGI_ERR_IMGHANDLER_NOFILE',		'De bron afbeelding bestaat niet.');
define('DIGI_ERR_IMGHANDLER_NOSIZE',		'De afmetingen van de bron afbeelding konden niet bepaald worden.');
define('DIGI_ERR_IMGHANDLER_WRONGTYPE',		'Ongeldig bestand type als bron afbeelding gekozen.');
define('DIGI_ERR_IMGHANDLER_WRONGTGTYPE',	'Ongeldig bestand type als doel afbeelding gekozen.');
define('DIGI_ERR_IMGHANDLER_SOURCE_FAIL',	'Er is iets misgegaan tijdens het voorbereiden van de bron afbeelding.');
define('DIGI_ERR_IMGHANDLER_CREATE_FAIL',	'Er is iets misgegaan tijdens het aanmaken van de doel afbeelding.');
define('DIGI_ERR_IMGHANDLER_COPY_FAIL',		'Er is iets misgegaan tijdens het kopieren van de bron afbeelding naar de doel afbeelding.');
define('DIGI_ERR_IMGHANDLER_OUTPUT_FAIL',	'Er is iets misgegaan tijdens het schrijven van de doel afbeelding.');

define('DIGI_ERR_AU_DIR_CREATE_FAIL',		'De update map [DIR] bestaat niet en kon niet aangemaakt worden.');
define('DIGI_ERR_AU_DIR_NOT_A_DIR',			'De update locatie [DIR] bestaat al, maar is geen map.');
define('DIGI_ERR_AU_UNLINK_FAIL',			'De update bestanden kunnen niet verwijderd worden uit de update map [DIR].');
define('DIGI_ERR_AU_CANT_WRITE',			'Er kan niet naar de update map [DIR] worden geschreven.');
define('DIGI_ERR_AU_PACKAGE_RETRIEVE',		'Het update archief kon niet gekopieerd worden naar de update map [DIR].');
define('DIGI_ERR_AU_ALL_QUERIES_FAILED',	'De database queries konden niet worden uitgevoerd. Uw technische dienst is op de hoogte gesteld.');
define('DIGI_ERR_AU_SOME_QUERIES_FAILED',	'Niet alle database queries konden worden uitgevoerd. Uw technische dienst is op de hoogte gesteld.');
define('DIGI_ERR_AU_QUERIES_FAILED',		'De volgende queries zijn mislukt:<br/><br/>');
define('DIGI_ERR_AU_PACKAGE_UNPACK',		'Het update archief kon niet worden uitgepakt.');
define('DIGI_ERR_AU_PKG_CREATE_FAIL',		'Het update archief kon niet worden aangemaakt op de server.');
define('DIGI_ERR_AU_PACKAGE_HASH_FAIL',		'Het update archief is corrupt geraakt tijdens het downloaden. Probeer het a.u.b. opnieuw.');
define('DIGI_ERR_AU_SQL_FETCH',				'De database queries konden niet worden opgehaald van de server.');
define('DIGI_ERR_AU_FTP_CONNECT',			'De verbinding naar de FTP server is mislukt.');
define('DIGI_ERR_AU_FTP_UPLOAD',			'Een of meer bestanden konden niet geupload worden via FTP. Uw technische dienst is op de hoogte gesteld.');
define('DIGI_ERR_AU_AVAILABLE',				'Er is een nieuwe update beschikbaar voor het CMS. U wordt sterk aangeraden om het CMS automatisch te updaten naar de nieuwste versie. Dit kunt u doen door in het menu op <b>Auto update</b> te klikken en de stappen verder te volgen.<br/><br/>Update informatie: ');

define('DIGI_ERR_MAILER',					'Er ging iets mis bij het versturen van deze email.');
define('DIGI_ERR_DISKSPACE',				'Let op! Er is nog maar [SPACE] schijfruimte vrij op de server.');
define('DIGI_ERR_FOLDER_CREATE_FAILED',		'Map bestaat niet en kan niet worden aangemaakt.');
define('DIGI_ERR_FOLDER_NONEXIST_CREATED',	'Map bestond niet maar is aangemaakt.');
?>