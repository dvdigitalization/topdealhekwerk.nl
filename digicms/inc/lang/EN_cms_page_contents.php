<?php
/*
Title:		cms_page_contents.php language definitions (EN)
File: 		inc/lang/EN_cms_page_contents.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Page set';
$object_lc 				= 	'page set';
$plural					=	'Page sets';
$plural_lc				=	'page sets';

// *** Message definitions
$msg_element_added		=	'The text element has been added. It is now accessible through: ';
$msg_elements_editted	=	'The text elements have been modified.';
$msg_pages_editted		=	'The page sets have been modified.';
$msg_element_exists		=	'A text element with this name already exists in this page set.';
$msg_empty_fields		=	'You have to provide a name and a page set for the element.';
$msg_delete_confirm		=	'Are you sure you want to remove this text element?';
$msg_delete_ps_confirm	=	'Are you sure you want to remove this page set and all text elements within it?';
$msg_deleted			=	'The text element has been removed.';
$msg_deleted_ps			=	'The page set has been removed.';
$msg_invalid_to_url		=	'Attention! The new images folder does not seem to exist.';
$msg_images_fixed		=	'The image links have been fixed.';

// *** Form and text elements
define('TXT_ADD_ITEM',					'Add new text element (all languages)');
define('TXT_EDIT_ITEM',					'Edit '.$object_lc);
define('TXT_DELETE_ITEM',				'Delete '.$object_lc);
define('TXT_ELEMENT_NAME',				'Element name');
define('TXT_FCK_FIELD',					'FCK field');
define('TXT_TEXT',						'Text');
define('TXT_EDIT_PAGE_SETS',			'Alter pagina sets');
define('TXT_NEW_ITEM',					'new text element');
define('TXT_ALTER_PAGE_SETS',			'alter page sets');
define('TXT_PAGE_SET',					'Page set');
define('TXT_PAGE_SETS',					'Page sets');
define('LOG_PAGE_SETS_EDITTED',			'Page sets editted');

?>