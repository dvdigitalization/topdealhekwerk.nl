<?php
/*
Title:		cms_faq.php language definitions (EN)
File: 		inc/lang/EN_cms_faq.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Message definitions
$msg_cat_added			=	'Category added.';
$msg_cat_editted		=	'Category editted.';
$msg_cat_deleted		=	'Category deleted.';
$msg_added				=	'Topic added.';
$msg_editted			=	'Topic editted.';
$msg_deleted			=	'Topic deleted.';

// *** Form and text elements
define('TXT_NO_TOPICS',					'No topics in this category');
define('TXT_ADD_CATEGORY',				'Add new category');
define('TXT_EDIT_CATEGORY',				'Edit category');
define('TXT_SUBCAT',					'Sub-category of');
define('TXT_ADD_TOPIC',					'Add new faq topic');
define('TXT_EDIT_TOPIC',				'Edit faq topic');
define('TXT_NEW_CAT',					'new category');
define('TXT_NEW_TOPIC',					'new faq topic');
define('TXT_FAQ_NOTE',					'If something is unclear, or if you have additional questions, you can contact [TECH_NAME] at [TECH_EMAIL]');
define('TXT_BROWSE',					'Pick a help topic to display on the right hand side. To navigate between help topics and categories, click on the relevant names.');

?>