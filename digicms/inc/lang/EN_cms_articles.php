<?php
/*
Title:		cms_template.php language definitions (NL) [TEMPLATE]
File: 		inc/lang/NL_cms_template.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Product';
$object_lc 				= 	'product';
$plural					=	'Products';
$plural_lc				=	'products';

// *** Messages
$msg_added				=	'Product added.';
$msg_editted			=	'Product editted.';
$msg_deleted			=	'Product deleted.';
$msg_deleted_m			=	'AProducts deleted.';
$msg_delete_confirm		=	'Are you sure you want to delete this product?';
$msg_delete_confirm_m	=	'Are you sure you want to delete these products?';
$msg_no_article_cats	=	'No product categories exist. Create these first.';

// *** Form and text elements
define('TXT_ADD_ITEM',					'Add '.$object_lc);
define('TXT_EDIT_ITEM',					'Edit '.$object_lc);
define('TXT_VIEW_ITEM',					'View '.$object_lc.' details');
define('TXT_DELETE_ITEM',				'Delete '.$object_lc);
define('TXT_ACTIVATE_ITEM',				'Activate '.$object_lc);
define('TXT_DEACTIVATE_ITEM',			'De-activate '.$object_lc);
define('TXT_DELETE_ITEMS',				'Delete '.$plural_lc);
define('TXT_ACTIVATE_ITEMS',			'Activate '.$plural_lc);
define('TXT_DEACTIVATE_ITEMS',			'De-activate '.$plural_lc);
define('TXT_NEW_ITEM',					'new product');

define('TXT_ARTICLE_CATEGORIES',		'product categories');
define('TXT_ARTICLE_UNITS',				'product units');

define('TXT_ARTICLE_DATA',				'Product data');
define('TXT_ARTICLE_NAME',				'Product name');
define('TXT_ARTICLE_NR',				'Article number');
define('TXT_PRICES_AND_VAT',			'Price and VAT');
define('TXT_COST_PRICE',				'Cost price');
define('TXT_SALE_PRICE',				'Sale price');
define('TXT_VAT_LEVEL',					'VAT level');
define('TXT_SUPPLY_AND_UNIT',			'Supply and unit');
define('TXT_AMOUNT_IN_STOCK',			'Amount in stock');
define('TXT_UNIT',						'Unit');
define('TXT_ADD_MORE_ARTICLES',			'Add more products?');

define('TXT_VAT',						'VAT');


?>