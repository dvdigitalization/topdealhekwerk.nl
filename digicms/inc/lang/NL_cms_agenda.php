<?php
/*
Title:		cms_agenda.php language definitions (NL)
File: 		inc/lang/NL_cms_agenda.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Evenement';
$object_lc 				= 	'evenement';
$plural					=	'Evenementen';
$plural_lc				=	'evenementen';

// *** Message definitions
$msg_added 				=	'Het evenement is geplaatst.';
$msg_editted			=	'Het evenement is gewijzigd. Let op, als u de afbeelding heeft gewijzigd kan het zijn dat uw browser
							 de oude afbeelding nog weer geeft. Ververs in dat geval de pagina of leeg uw cache.';
$msg_deleted			=	'Het evenement is verwijderd.';
$msg_deleted_m			=	'De evenementen zijn verwijderd.';
$msg_delete_confirm		=	'Dit evenement zeker weten verwijderen?';
$msg_delete_confirm_m	=	'Deze evenementen zeker weten verwijderen?';
$msg_deactivated		=	'Het evenement wordt niet langer op de website getoond.';
$msg_deactivated_m		=	'De evenementen worden niet langer op de website getoond.';
$msg_activated			=	'Het evenement wordt vanaf nu op de website getoond.';
$msg_activated_m		=	'De evenementen worden vanaf nu op de website getoond.';
$msg_upload_error		=	'Er ging iets mis tijdens het uploaden van de afbeelding.';
$msg_thumb_error		=	'Er kon geen thumbnail van de afbeelding worden gemaakt.';
$msg_resize_error		=	'Er kon geen verkleinde versie van de afbeelding worden gemaakt.';
$msg_directory_error	=	'De doelmap [DIR] bestaat niet.';

// *** Form and text elements
define('TXT_ADD_ITEM',					$object.' toevoegen');
define('TXT_EDIT_ITEM',					$object.' wijzigen');
define('TXT_VIEW_ITEM',					$object.' bekijken op de website');
define('TXT_DELETE_ITEM',				$object.' verwijderen');
define('TXT_ACTIVATE_ITEM',				$object.' activeren');
define('TXT_DEACTIVATE_ITEM',			$object.' de-activeren');
define('TXT_ARCHIVE_ITEM',				$object.' archiveren');
define('TXT_UNARCHIVE_ITEM',			$object.' uit archief halen');
define('TXT_DELETE_ITEMS',				$plural.' verwijderen');
define('TXT_ACTIVATE_ITEMS',			$plural.' activeren');
define('TXT_DEACTIVATE_ITEMS',			$plural.' de-activeren');
define('TXT_ARCHIVE_ITEMS',				$plural.' archiveren');
define('TXT_UNARCHIVE_ITEMS',			$plural.' uit archief halen');
define('TXT_NEW_ITEM',					'nieuw evenement');
define('TXT_CONTENTS',					'Informatie');
define('TXT_PREVIEW',					'Preview');

?>