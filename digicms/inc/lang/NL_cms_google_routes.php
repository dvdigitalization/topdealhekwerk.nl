<?php
/*
Title:		cms_template.php language definitions (NL)
File: 		inc/lang/NL_cms_template.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Route';
$object_lc 				= 	'route';
$plural					=	'Routes';
$plural_lc				=	'routes';

// *** Message definitions
$msg_added 				=	'De route is geplaatst.';
$msg_editted			=	'De route is gewijzigd.';
$msg_deleted			=	'De route is verwijderd.';
$msg_deleted_m			=	'De routes zijn verwijderd.';
$msg_delete_confirm		=	'Deze route zeker weten verwijderen?';
$msg_delete_confirm_m	=	'Deze routes zeker weten verwijderen?';
$msg_deactivated		=	'De route wordt niet langer op de website getoond.';
$msg_deactivated_m		=	'De routes worden niet langer op de website getoond.';
$msg_activated			=	'De route wordt vanaf nu op de website getoond.';
$msg_activated_m		=	'De routes worden vanaf nu op de website getoond.';
$msg_wp_added 			=	'De locatie is aan de route toegevoegd.';
$msg_wp_deleted 		=	'De locatie is van de route verwijderd.';
$msg_wp_delete_confirm	=	'Deze locatie zeker weten verwijderen van deze route?';

// *** Form and text elements
define('TXT_ADD_ITEM',					$object.' toevoegen');
define('TXT_EDIT_ITEM',					$object.' wijzigen');
define('TXT_VIEW_ITEM',					$object.' gegevens inzien');
define('TXT_DELETE_ITEM',				$object.' verwijderen');
define('TXT_ACTIVATE_ITEM',				$object.' activeren');
define('TXT_DEACTIVATE_ITEM',			$object.' de-activeren');
define('TXT_ITEM_UP',					'Locatie omhoog verplaatsen');
define('TXT_ITEM_DOWN',					'Locatie omlaag verplaatsen');
define('TXT_DELETE_ITEMS',				$plural.' verwijderen');
define('TXT_ACTIVATE_ITEMS',			$plural.' activeren');
define('TXT_DEACTIVATE_ITEMS',			$plural.' de-activeren');
define('TXT_NEW_ITEM',					'nieuwe route');
define('TXT_CONTENTS',					'Inhoud');
define('TXT_MAP_TYPE',					'Type kaart');
define('TXT_MAP_TYPE_NORMAL',			'Normale kaart');
define('TXT_MAP_TYPE_SAT',				'Satelliet kaart');
define('TXT_MAP_TYPE_HYBRID',			'Hybride kaart');
define('TXT_LOCATION',					'Locatie');
define('TXT_LOCATIONS',					'Locaties');
define('TXT_DEL_WP',					'Locatie verwijderen');
?>