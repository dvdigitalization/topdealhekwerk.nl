<?php
/*
Title:		cms_auth_users.php language definitions (EN)
File: 		inc/lang/EN_cms_auth_users.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'CMS user';
$object_lc 				= 	'CMS user';
$plural					=	'CMS users';
$plural_lc				=	'CMS users';

// *** Message definitions
$msg_added				=	'User added.';
$msg_editted			=	'User information editted.';
$msg_deleted			=	'User deleted.';
$msg_deleted_m			=	'Users deleted.';
$msg_delete_confirm		=	'Are you sure you want to delete this user?';
$msg_delete_confirm_m	=	'Are you sure you want to delete these users?';
$msg_activated			=	'User activated.';
$msg_activated_m		=	'Users activated.';
$msg_deactivated		=	'User de-activated.';
$msg_deactivated_m		=	'Users de-activated.';
$msg_newpass			=	'The new password has been set.';
$msg_purged				=	'The activity log has been emptied.';
$msg_no_username		=	'You have to fill in a username.';
$msg_no_pass			=	'You have to fill in a password and repeat it twice.';
$msg_pass_dont_match	=	'The passwords do not match.';
$msg_username_exists	=	'This username is already in use.';
$msg_invalid_username	=	'Invalid usrename.';
$msg_invalid_chars_user	=	'You can only use alphanumeric characters or dashes and underscores for the username.';
$msg_invalid_chars_pass	=	'You can only use alphanumeric characters or dashes and underscores for the password.';
$msg_invalid_email		=	'Ivalid email address.';
$msg_unable_to_delete	=	'You cannot delete this user.';
$msg_login_confirm		=	'Login as this user?';
$msg_login_failed		=	'Logging in as this user did not succeed.';
$msg_invalid_action		=	'Invalid operation.';

// *** Form and text elements
define('TXT_ADD_USER',					'Add a new user');
define('TXT_USE_IN_ADMIN',				'Use in administration');
define('TXT_ADMINISTRATOR',				'Administrator');
define('TXT_PRIVS',						'Privilleges');
define('TXT_LOGIN_ALLOWED',				'login allowed');
define('TXT_LOGIN_NOT_ALLOWED',			'login not allowed');
define('TXT_ADMIN_ACCOUNT',				'Administrator account');
define('TXT_CMS_USER',					'CMS user');
define('TXT_NO_ACTIVITIES_LOGGED',		'No activities logged');
define('TXT_ACTIVITY_LOG_OF',			'Activity log of');
define('TXT_EMPTY_LOG',					'empty activity log');
define('TXT_NEW_USER',					'new user');
define('TXT_EMPTY_ALL_LOGS',			'empty all activity logs');
define('TXT_OFFLINE',					'offline');
define('TXT_ONLINE',					'online');
define('TXT_USER_IS_OFFLINE',			'User is offline');
define('TXT_USER_IS_ONLINE',			'User is online');
define('TXT_LOGIN_AS',					'Login as');
define('TXT_USER_IS_ADMIN',				'User is an administrator');
define('TXT_UNBLOCK_USER',				'Unblock user');
define('TXT_DISALLOW_LOGIN',			'Disallow user to login');
define('TXT_ALLOW_LOGIN',				'Allow user to login');
define('TXT_SHOW_ACTIVITY_LOG',			'Show activity log');
define('TXT_EDIT_USER',					'Edit user details');
define('TXT_DELETE_USER',				'Delete user');
define('TXT_DELETE_USERS',				'Delete users');
define('TXT_ACTIVATE_USERS',			'Activate users');
define('TXT_DEACTIVATE_USERS',			'De-activate users');
define('TXT_VIEW_USER_DETAILS',			'View user details');
define('TXT_LAST_LOGIN',				'last login');
define('TXT_FROM',						'from');
define('TXT_USER',						'User');
define('TXT_ADMIN',						'Admin');
define('TXT_MARK_OR_CLEAR_ALL_PRIVS',	'Mark or clear all privilleges');
define('TXT_IP_LOGIN_1',				'Auto login IP (1)');
define('TXT_IP_LOGIN_2',				'Auto login IP (2)');

?>