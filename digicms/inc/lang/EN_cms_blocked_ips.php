<?php
/*
Title:		cms_blocked_ips.php language definitions (EN)
File: 		inc/lang/EN_cms_blocked_ips.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'IP address';
$object_lc 				= 	'IP address';
$plural					=	'IP addresses';
$plural_lc				=	'IP addresses';

// *** Message definitions
$msg_added				=	'The IP address has been added to the list.';
$msg_editted			=	'The IP address has been editted.';
$msg_deleted			=	'The IP address has been deleted of the list.';
$msg_deleted_m			=	'De IP addresses have been deleted of the list.';
$msg_delete_confirm		=	'Are you sure you want to delete this IP address of the list?';
$msg_delete_confirm_m	=	'Are you sure you want to delete these IP addresses of the list?';
$msg_no_ip				=	'You must fill in an IP address.';
$msg_illegal_ip_format	=	'Invalid IP address.';
$msg_bad_word_blocked	=	'Word added to the list of SPAM words:';
$msg_ip_exists			=	'This IP address is already on the list.';

// *** Form and text elements
define('TXT_ADD_ITEM',					'Add IP address to the list');
define('TXT_EDIT_ITEM',					'Edit IP address');
define('TXT_DELETE_ITEM',				'Delete IP address');
define('TXT_DELETE_ITEMS',				'Delete IP addresses');
define('TXT_NEW_ITEM',					'add IP address');
define('TXT_BLOCKED',					'Blocked');

?>