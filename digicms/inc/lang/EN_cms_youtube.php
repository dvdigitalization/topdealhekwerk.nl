<?php
/*
Title:		cms_youtube.php language definitions (EN)
File: 		inc/lang/EN_cms_youtube.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Youtube video';
$object_lc 				= 	'Youtube video';
$plural					=	'Youtube video\'s';
$plural_lc 				= 	'Youtube video\'s';

// *** Message definitions
$msg_added				=	"$object added.";
$msg_editted			=	"$object data editted.";
$msg_deleted			=	"$object deleted.";
$msg_deleted_m			=	"$plural deleted.";
$msg_delete_confirm		=	"Are you sure you want to delete this $object_lc?";
$msg_delete_confirm_m	=	"Are you sure you want to delete these $plural_lc?";
$msg_activated			=	"$object activated.";
$msg_activated_m		=	"$plural activated.";
$msg_deactivated		=	"$object deactivated.";
$msg_deactivated_m		=	"$plural deactivated.";

// *** Form and text elements
define('TXT_ADD_ITEM',						'Add '.$object_lc);
define('TXT_EDIT_ITEM',						'Edit '.$object_lc);
define('TXT_VIEW_ITEM',						'View '.$object_lc);
define('TXT_NEW_VIDEO',						'new video');
define('TXT_VIEW_ITEM_OPTION',				'View '.$object_lc.' details');
define('TXT_VIEW_VIDEO',					'View video'); 
define('TXT_DEACTIVATE_ITEM_OPTION',		'Deactivate '.$object_lc);
define('TXT_ACTIVATE_ITEM_OPTION',			'Activate '.$object_lc);
define('TXT_MOVE_ITEM_UP',					'Move '.$object_lc.' up');
define('TXT_MOVE_ITEM_DOWN',				'Move '.$object_lc.' down');
define('TXT_EDIT_ITEM_OPTION',				'Edit '.$object_lc.' ');
define('TXT_DELETE_ITEM_OPTION',			'Delete '.$object_lc);

define('TXT_YOUTUBE_LINK',					'Youtube link');
define('TXT_YOUTUBE_VIDEO_ID',				'Youtube video ID');
define('TXT_THUMBNAIL',						'Thumbnail');
define('TXT_HTML_CODE',						'HTML code');
?>