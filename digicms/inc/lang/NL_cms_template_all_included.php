<?php
/*
Title:		cms_site_projects.php language definitions (NL)
File: 		inc/lang/NL_cms_site_projects.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Project';
$object_lc 				= 	'project';
$plural					=	'Projecten';
$plural_lc				=	'projecten';

// *** Message definitions
$msg_added 				=	'Het project is geplaatst.';
$msg_editted			=	'Het project is gewijzigd.';
$msg_deleted			=	'Het project is verwijderd.';
$msg_deleted_m			=	'De projecten zijn verwijderd.';
$msg_delete_confirm		=	'Dit project zeker weten verwijderen?';
$msg_delete_confirm_m	=	'Deze projecten zeker weten verwijderen?';
$msg_p_delete_confirm	=	'Deze foto zeker weten verwijderen?';
$msg_p_delete_confirm_m	=	'Deze foto\'s zeker weten verwijderen?';
$msg_photo_deleted		=	'Foto verwijderd.';
$msg_photo_deleted_m	=	'Foto\'s verwijderd.';
$msg_deactivated		=	'Het project wordt niet langer op de website getoond.';
$msg_deactivated_m		=	'De projecten worden niet langer op de website getoond.';
$msg_activated			=	'Het project wordt vanaf nu op de website getoond.';
$msg_activated_m		=	'De projecten worden vanaf nu op de website getoond.';
$msg_highlighted		= 	'Het project is highlighted.';
$msg_highlighted_m		= 	'De projecten zijn highlighted.';
$msg_unhighlighted		= 	'Het project is niet langer highlighted.';
$msg_unhighlighted_m	= 	'De projecten zijn niet langer highlighted.';
$msg_album_empty		=	'Dit foto album bevat nog geen foto\'s.';
$msg_directory_error	=	'De doelmap of thumbnailmap bestaat niet.';
$msg_upload_error		=	'Er ging iets mis tijdens het uploaden van de afbeelding.';
$msg_resize_error		=	'Er ging iets mis tijdens het verkleinen van ';
$msg_thumb_error		=	'Er ging iets mis tijdens het maken van een thumbnail versie van ';
$msg_none_uploaded		=	'Geen foto\'s geupload.';
$msg_none_processed		=	'Geen foto\'s verwerkt.';
$msg_some_photos_failed	=	'Enkele foto\'s konden niet worden verwerkt.';
$msg_some_deleted		=	'Een paar foto\'s bestonden niet (meer). Deze zijn uit het album verwijderd.';
$msg_photo_editted		=	'Foto gegevens bijgewerkt.';

// *** Form and text elements
define('TXT_ADD_ITEM',					$object.' toevoegen');
define('TXT_EDIT_ITEM',					$object.' wijzigen');
define('TXT_VIEW_ITEM',					$object.' gegevens inzien');
define('TXT_DELETE_ITEM',				$object.' verwijderen');
define('TXT_ACTIVATE_ITEM',				$object.' activeren');
define('TXT_DEACTIVATE_ITEM',			$object.' de-activeren');
define('TXT_HIGHLIGHT_ITEM',			$object_lc.' highlighten');
define('TXT_UNHIGHLIGHT_ITEM',			$object_lc.' niet langer highlighten');
define('TXT_ITEM_UP',					$object.' omhoog verplaatsen');
define('TXT_ITEM_DOWN',					$object.' omlaag verplaatsen');
define('TXT_DELETE_ITEMS',				$plural.' verwijderen');
define('TXT_ACTIVATE_ITEMS',			$plural.' activeren');
define('TXT_DEACTIVATE_ITEMS',			$plural.' de-activeren');
define('TXT_LOCATION',					'Locatie');
define('TXT_HIGHLIGHTED',				'Highlighted');
define('TXT_AREA',						'Oppervlakte');
define('TXT_NEW_ITEM',					'nieuw project');
define('TXT_ADD_PHOTOS',				'Foto\'s toevoegen');
define('TXT_ADD_DOCUMENTS',				'Documenten toevoegen');
define('TXT_PICTURES',					'Foto\'s');
define('TXT_CONTENTS',					'Inhoud');

?>