<?php
/*
Title:		Digi error definitions (EN)
File: 		inc/lang/EN_errors.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

define('DIGI_ERR_SAFE_MODE',				'PHP safe_mode() is switched on. Some functions within the CMS might not work correctly because of this.<br/>You are advised to turn safe_mode() off.');
define('DIGI_ERR_REG_GLOBALS',				'PHP register_globals() is switched on. This could result in security problems.<br/>You are advised to turn register_globals() off.');
define('DIGI_ERR_PASS_CHANGE_REQUIRED',		'You did not change your initial password yet.<br/>You are advised to change this as quickly as possible in the General settings section of the CMS.');
define('DIGI_ERR_CFG_MENU_ELEMENTS',		'Note: there are one or more modules defined in the menu which have not been installed.<br/><br/>Check inc/config.php');

define('DIGI_ERR_UPLOAD_SIZE',				'The file is too large. Maximum file size is [SIZE].');
define('DIGI_ERR_UPLOAD_NO_FILE',			'The file is invalid.');
define('DIGI_ERR_UPLOAD_NO_TMP',			'There is no temporary folder to upload files to.');
define('DIGI_ERR_UPLOAD_PARTIAL',			'The file was not uploaded completely.');
define('DIGI_ERR_UPLOAD_CANTWRITE',			'The file could not be copied to the temporary folder.');
define('DIGI_ERR_UPLOAD_NO_IMG',			'The file has to be an image.');
define('DIGI_ERR_UPLOAD_FILENAME',			'No valid filename could be found.');
define('DIGI_ERR_UPLOAD_EXISTS',			'A file with this filename already exists.');
define('DIGI_ERR_UPLOAD_DEST_DIR',			'The target folder [DIR] does not exist or is not usable.');
define('DIGI_ERR_UPLOAD_MOVING',			'The file could not be copied to the target folder [DIR].');
define('DIGI_ERR_UPLOAD_EXT',				'The file must be one of the following types: [EXTENSIONS]');
define('DIGI_ERR_UPLOAD_FIELD_NAME',		'The field [FIELD] is not a valid file type field.');
define('DIGI_ERR_UPLOAD_HACK',				'There is possibly being tampered with the CMS.');

define('DIGI_ERR_IMGHANDLER_NOFILE',		'The source image does not exist.');
define('DIGI_ERR_IMGHANDLER_NOSIZE',		'The dimensions of the source image could not be determined.');
define('DIGI_ERR_IMGHANDLER_WRONGTYPE',		'Invalid file type chosen as source image.');
define('DIGI_ERR_IMGHANDLER_WRONGTGTYPE',	'Invalid file type chosen as target image.');
define('DIGI_ERR_IMGHANDLER_SOURCE_FAIL',	'Something went wrong while preparing the source image.');
define('DIGI_ERR_IMGHANDLER_CREATE_FAIL',	'Something went wrong while creating the source image.');
define('DIGI_ERR_IMGHANDLER_COPY_FAIL',		'Something went wrong while copying the source image to the target image.');
define('DIGI_ERR_IMGHANDLER_OUTPUT_FAIL',	'Something went wrong while writing the target image.');

define('DIGI_ERR_AU_DIR_CREATE_FAIL',		'The update folder [DIR] does not exist and could not be created.');
define('DIGI_ERR_AU_DIR_NOT_A_DIR',			'The update location [DIR] already exists, but is not a folder.');
define('DIGI_ERR_AU_UNLINK_FAIL',			'The update files could not be removed from the update folder [DIR].');
define('DIGI_ERR_AU_CANT_WRITE',			'The update folder [DIR] is not writable.');
define('DIGI_ERR_AU_PACKAGE_RETRIEVE',		'The update archive could not be copied to the update folder [DIR].');
define('DIGI_ERR_AU_ALL_QUERIES_FAILED',	'The database queries could not be performed. Your technical support has been notified.');
define('DIGI_ERR_AU_SOME_QUERIES_FAILED',	'Not all database queries could be performed. Your technical support has been notified.');
define('DIGI_ERR_AU_QUERIES_FAILED',		'The following queries have failed:<br/><br/>');
define('DIGI_ERR_AU_PACKAGE_UNPACK',		'The update archive could not be unpacked.');
define('DIGI_ERR_AU_PKG_CREATE_FAIL',		'The update archive could not be created on the server.');
define('DIGI_ERR_AU_PACKAGE_HASH_FAIL',		'The update archive became corrupted while downloading. Please try again.');
define('DIGI_ERR_AU_SQL_FETCH',				'The database queries could not be fetched from the server.');
define('DIGI_ERR_AU_FTP_CONNECT',			'The FTP connection to the server has failed.');
define('DIGI_ERR_AU_FTP_UPLOAD',			'One or more files failed to upload through FTP. Your technical support has been notified.');
define('DIGI_ERR_AU_AVAILABLE',				'There is a new update available for the CMS. You are strongly advised to automatically update your CMS system by clicking on <b>Auto update</b> in the menu and to perform the following steps.<br/><br/>Update information: ');

define('DIGI_ERR_MAILER',					'Something went wrong while sending this email.');
define('DIGI_ERR_DISKSPACE',				'Warning! There is only [SPACE] of diskspace left on the server.');
define('DIGI_ERR_FOLDER_CREATE_FAILED',		'Folder does not exist and could not be created.');
define('DIGI_ERR_FOLDER_NONEXIST_CREATED',	'Folder did not exist, but has been created.');
?>