<?php
/*
Title:		cms_auto_update.php language definitions (NL)
File: 		inc/lang/NL_cms_auto_update.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Message definitions
$msg_no_updates				=	'Er zijn op dit moment geen nieuwe updates beschikbaar.';
$msg_no_soap				=	'Het CMS kan op dit moment geen verbinding maken met de update server. Probeer het later nog een keer.';
$msg_no_ftp_data			=	'U moet de FTP gegevens opgeven om de update uit te kunnen voeren.';
$msg_update_completed		=	'Het update proces is met succes uitgevoerd. Uw CMS versie is nu up-to-date.';

// *** Form and text elements
define('TXT_AUTO_UPDATE',				'Automatische update');
define('TXT_TO_VERSION',				'Update naar versie');
define('TXT_AUTOMATIC',					'Het update proces zal volautomatisch verlopen. U kunt nu het update proces starten.');
define('TXT_THROUGH_FTP',				'via FTP');
define('TXT_PROVIDE_FTP',				'U moet uw FTP gegevens opgeven om het update proces te kunnen starten.');
define('TXT_MANUAL_UPDATE',				'Handmatige update');
define('TXT_AUTOMATIC_IMPOSSIBLE',		'Het is helaas niet mogelijk om uw CMS automatisch te laten updaten.');
define('TXT_DOWNLOADMANUALLY',			'U zult het update bestand handmatig moeten downloaden en uitpakken op uw server.');
define('TXT_DOWNLOAD_UPDATE',			'Download het update bestand');
define('TXT_UPDATE_DATABASE',			'Nadat u de bestanden heeft uitgepakt op uw server, dient u uw database te updaten.');
define('BUTTON_UPDATE_DATABASE',		'Update database');
define('BUTTON_UPDATE',					'Update');
define('TXT_FTP_SERVER',				'FTP server');
define('TXT_FTP_USER',					'FTP gebruikersnaam');
define('TXT_FTP_PASS',					'FTP wachtwoord');
define('TXT_FTP_FOLDER',				'Bestemmings map');
define('TXT_FTP_REMEMBER',				'Gebruikersnaam en wachtwoord opslaan?');
define('TXT_RELEASED_ON',				'Uitgebracht op');
define('TXT_NEW',						'Nieuwe');
define('TXT_VERSIONS',					'versies');
define('TXT_AVAILABLE',					'beschikbaar');
define('TXT_NEW_UPDATES_FOUND',			'Er zijn updates gevonden voor uw CMS versie. U kunt hieronder informatie over deze updates nalezen en vervolgens de update uitvoeren.');
define('TXT_PERFORM_UPDATE',			'Update uitvoeren');

?>