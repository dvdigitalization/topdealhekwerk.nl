<?php
/*
Title:		cms_affiliates.php language definitions (NL)
File: 		inc/lang/NL_cms_affiliates.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Affiliate';
$object_lc 				= 	'affiliate';
$object_lc_deleted		=	'verwijderde affiliate';
$plural					=	'Affiliates';
$plural_lc				=	'affiliates';
$plural_lc_deleted		=	'verwijderde affiliates';

// *** Message definitions
$msg_added 				= 	'Affiliate toegevoegd.';
$msg_editted 			= 	'Affiliate aangepast.';
$msg_deleted			=	'Affiliate verwijderd.';
$msg_deleted_m 			= 	'Affiliates verwijderd.';
$msg_delete_confirm		= 	'Deze affiliate zeker weten verwijderen?';
$msg_delete_confirm_m 	= 	'Deze affiliates zeker weten verwijderen?';
$msg_perma_confirm		= 	'Deze affiliate zeker weten permanent verwijderen?';
$msg_perma_confirm_m 	= 	'Deze affiliates zeker weten permanent verwijderen?';
$msg_restored			= 	'Affiliate teruggezet.';
$msg_restored_m	 		= 	'Affiliates teruggezet.';
$msg_restore_confirm	= 	'Deze affiliate zeker weten terugzetten?';
$msg_restore_confirm_m 	= 	'Deze affiliates zeker weten terugzetten?';
$msg_activated			=	'Affiliate geactiveerd.';
$msg_activated_m		=	'Affiliates geactiveerd.';
$msg_deactivated		=	'Affiliate op non-actief gezet.';
$msg_deactivated_m		=	'Affiliates op non-actief gezet.';
$msg_exists				=	'Er bestaat al een affiliate met deze naam en URL.';

// *** Form and text elements
define('TXT_ADD_ITEM',							$object.' toevoegen');
define('TXT_EDIT_ITEM',							$object.' wijzigen');
define('TXT_RESTORE_DELETED_ITEMS',				'Verwijderde '.$object_lc.' terugzetten');
define('TXT_RESTORE_ITEM',						$object.' terugzetten');
define('TXT_RESTORE_ITEMS',						$plural.' terugzetten');
define('TXT_DELETE_PERM_ITEM',					$object.' permanent verwijderen');
define('TXT_DELETE_PERM_ITEMS',					$plural.' permanent verwijderen');
define('TXT_DELETE_ITEM',						$object.' verwijderen');
define('TXT_DELETE_ITEMS',						$plural.' verwijderen');
define('TXT_VIEW_ITEM',							$object.' gegevens inzien');
define('TXT_ACTIVATE_ITEM',						$object.' activeren');
define('TXT_ACTIVATE_ITEMS',					$plural.' activeren');
define('TXT_DEACTIVATE_ITEM',					$object.' de-activeren');
define('TXT_DEACTIVATE_ITEMS',					$plural.' de-activeren');
define('TXT_SEND_EMAIL',						'Stuur deze '.$object_lc.' een email');

define('TXT_NEW_ITEM',							'nieuwe affiliate');
define('TXT_DELETED_ITEMS',						'verwijderde affiliates');
define('TXT_WEBSITE',							'Website');
define('TXT_CONTACT_PERSON',					'Contactpersoon');
define('TXT_REMARKS',							'Opmerkingen');
define('TXT_AFFILIATE_CODE',					'Affiliate code');
define('TXT_AFFILIATE_LINK',					'Link voor affiliate');
define('TXT_PAGE_HITS',							'Page hits');
define('TXT_VIEW_ORDERS',						'Order inzien');
define('TXT_VISIT_WEBSITE',						'Bezoek deze website');
define('TXT_LINKED_ORDERS',						'Gelinkte orders');
define('TXT_ORDERS',							'Orders');
?>