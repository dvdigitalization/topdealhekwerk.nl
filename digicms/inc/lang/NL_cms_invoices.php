<?php
/*
Title:		cms_invoices.php language definitions (NL)
File: 		inc/lang/NL_cms_invoices.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Template';
$object_lc 				= 	'template';
$plural					=	'Templates';
$plural_lc				=	'templates';

// *** Message definitions
$msg_added 				=	'Het xxx is geplaatst.';
$msg_editted			=	'Het xxx is gewijzigd.';
$msg_deleted			=	'Het xxx is verwijderd.';
$msg_deleted_m			=	'De xxxen zijn verwijderd.';
$msg_delete_confirm		=	'Dit xxx zeker weten verwijderen?';
$msg_delete_confirm_m	=	'Deze xxxen zeker weten verwijderen?';
$msg_deactivated		=	'Het xxx wordt niet langer op de website getoond.';
$msg_deactivated_m		=	'De xxxen worden niet langer op de website getoond.';
$msg_activated			=	'Het xxx wordt vanaf nu op de website getoond.';
$msg_activated_m		=	'De xxxen worden vanaf nu op de website getoond.';
$msg_archived			=	'Het xxx is gearchiveerd.';
$msg_archived_m			=	'De xxxen zijn gearchiveerd.';
$msg_unarchived			=	'Het xxx is uit het archief gehaald.';
$msg_unarchived_m		=	'De xxxen zijn uit het archief gehaald.';
$msg_upload_error		=	'Er ging iets mis tijdens het uploaden van de afbeelding.';
$msg_file_upload_error	=	'Er ging iets mis tijdens het uploaden van het bestand.';
$msg_thumb_error		=	'Er kon geen thumbnail van de afbeelding worden gemaakt.';
$msg_resize_error		=	'Er kon geen verkleinde versie van de afbeelding worden gemaakt.';
$msg_directory_error	=	'De doelmap [DIR] bestaat niet.';

// *** Form and text elements
define('TXT_ADD_ITEM',					$object.' toevoegen');
define('TXT_EDIT_ITEM',					$object.' wijzigen');
define('TXT_VIEW_ITEM',					$object.' gegevens inzien');
define('TXT_DELETE_ITEM',				$object.' verwijderen');
define('TXT_ACTIVATE_ITEM',				$object.' activeren');
define('TXT_DEACTIVATE_ITEM',			$object.' de-activeren');
define('TXT_ARCHIVE_ITEM',				$object.' archiveren');
define('TXT_UNARCHIVE_ITEM',			$object.' uit archief halen');
define('TXT_ITEM_UP',					$object.' omhoog verplaatsen');
define('TXT_ITEM_DOWN',					$object.' omlaag verplaatsen');
define('TXT_DELETE_ITEMS',				$plural.' verwijderen');
define('TXT_ACTIVATE_ITEMS',			$plural.' activeren');
define('TXT_DEACTIVATE_ITEMS',			$plural.' de-activeren');
define('TXT_ARCHIVE_ITEMS',				$plural.' archiveren');
define('TXT_UNARCHIVE_ITEMS',			$plural.' uit archief halen');
define('TXT_NEW_ITEM',					'nieuw xxx');
define('TXT_CONTENTS',					'Inhoud');

?>