<?php
/*
Title:		cms_template.php language definitions (NL)
File: 		inc/lang/NL_cms_template.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Feestdag';
$object_lc 				= 	'feestdag';
$plural					=	'Feestdagen';
$plural_lc				=	'feestdagen';

// *** Message definitions
$msg_added 				=	'Het feestdag is geplaatst.';
$msg_editted			=	'Het feestdag is gewijzigd.';
$msg_deleted			=	'Het feestdag is verwijderd.';
$msg_deleted_m			=	'De feestdagen zijn verwijderd.';
$msg_delete_confirm		=	'Dit feestdag zeker weten verwijderen?';
$msg_delete_confirm_m	=	'Deze feestdagen zeker weten verwijderen?';
$msg_deactivated		=	'Het feestdag wordt niet langer op de website getoond.';
$msg_deactivated_m		=	'De feestdagen worden niet langer op de website getoond.';
$msg_activated			=	'Het feestdag wordt vanaf nu op de website getoond.';
$msg_activated_m		=	'De feestdagen worden vanaf nu op de website getoond.';
$msg_archived			=	'Het feestdag is gearchiveerd.';
$msg_archived_m			=	'De feestdagen zijn gearchiveerd.';
$msg_unarchived			=	'Het feestdag is uit het archief gehaald.';
$msg_unarchived_m		=	'De feestdagen zijn uit het archief gehaald.';
$msg_upload_error		=	'Er ging iets mis tijdens het uploaden van de afbeelding.';
$msg_file_upload_error	=	'Er ging iets mis tijdens het uploaden van het bestand.';
$msg_thumb_error		=	'Er kon geen thumbnail van de afbeelding worden gemaakt.';
$msg_resize_error		=	'Er kon geen verkleinde versie van de afbeelding worden gemaakt.';
$msg_directory_error	=	'De doelmap [DIR] bestaat niet.';

// *** Form and text elements
define('TXT_ADD_ITEM',					$object.' toevoegen');
define('TXT_EDIT_ITEM',					$object.' wijzigen');
define('TXT_VIEW_ITEM',					$object.' gegevens inzien');
define('TXT_DELETE_ITEM',				$object.' verwijderen');
define('TXT_ACTIVATE_ITEM',				$object.' activeren');
define('TXT_DEACTIVATE_ITEM',			$object.' de-activeren');
define('TXT_ARCHIVE_ITEM',				$object.' archiveren');
define('TXT_UNARCHIVE_ITEM',			$object.' uit archief halen');
define('TXT_ITEM_UP',					$object.' omhoog verplaatsen');
define('TXT_ITEM_DOWN',					$object.' omlaag verplaatsen');
define('TXT_DELETE_ITEMS',				$plural.' verwijderen');
define('TXT_ACTIVATE_ITEMS',			$plural.' activeren');
define('TXT_DEACTIVATE_ITEMS',			$plural.' de-activeren');
define('TXT_ARCHIVE_ITEMS',				$plural.' archiveren');
define('TXT_UNARCHIVE_ITEMS',			$plural.' uit archief halen');
define('TXT_NEW_ITEM',					'nieuwe feestdag');
define('TXT_NEW_ITEMS',					'nieuwe feestdagen');
define('TXT_CONTENTS',					'Inhoud');

define('TXT_DATE_START',				'Start');
define('TXT_DATE_STOP',					'Stop');
?>