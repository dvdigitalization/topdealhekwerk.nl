<?php
/*
Title:		cms_agenda.php language definitions (EN)
File: 		inc/lang/EN_cms_agenda.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Event';
$object_lc 				= 	'event';
$plural					=	'Events';
$plural_lc				=	'events';

// *** Message definitions
$msg_added 				=	'The event has been added.';
$msg_editted			=	'The event has been editted. Pay attention, if you have changed the image, it is possible that your
							 browser will still display the old image. Refresh the page in that case, or empty your cache.';
$msg_deleted			=	'The event has been deleted.';
$msg_deleted_m			=	'The events have been deleted.';
$msg_delete_confirm		=	'Are you sure you want to delete this event?';
$msg_delete_confirm_m	=	'Are you sure you want to delete these events?';
$msg_deactivated		=	'The event will no longer be displayed on the website.';
$msg_deactivated_m		=	'The events will no longer be displayed on the website.';
$msg_activated			=	'The event will be displayed on the website.';
$msg_activated_m		=	'The events will be displayed on the website.';
$msg_upload_error		=	'Something went wrong while uploading the image.';
$msg_thumb_error		=	'No thumbnail could be created from the image.';
$msg_resize_error		=	'The image could nog be resized.';
$msg_directory_error	=	'The target directory [DIR] does not exist.';

// *** Form and text elements
define('TXT_ADD_ITEM',					'Add '.$object_lc);
define('TXT_EDIT_ITEM',					'Edit '.$object_lc);
define('TXT_VIEW_ITEM',					'View this '.$object_lc.' on the website');
define('TXT_DELETE_ITEM',				'Delete '.$object_lc);
define('TXT_ACTIVATE_ITEM',				'Activate '.$object_lc);
define('TXT_DEACTIVATE_ITEM',			'De-activate '.$object_lc);
define('TXT_ARCHIVE_ITEM',				'Archive '.$object_lc);
define('TXT_UNARCHIVE_ITEM',			'Unarchive '.$object_lc);
define('TXT_DELETE_ITEMS',				'Delete '.$plural_lc);
define('TXT_ACTIVATE_ITEMS',			'Activate '.$plural_lc);
define('TXT_DEACTIVATE_ITEMS',			'De-activate '.$plural_lc);
define('TXT_ARCHIVE_ITEMS',				'Archive '.$plural_lc);
define('TXT_UNARCHIVE_ITEMS',			'Unarchive '.$plural_lc);
define('TXT_NEW_ITEM',					'new event');
define('TXT_CONTENTS',					'Event info');
define('TXT_PREVIEW',					'Preview');

?>