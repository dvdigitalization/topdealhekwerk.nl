<?php
/*
Title:		cms_mailer.php language definitions (EN)
File: 		inc/lang/EN_cms_mailer.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Email';
$object_lc 				= 	'email';
$plural					=	'Emails';
$plural_lc				=	'emails';

$sent_object_lc			=	'sent email';
$sent_plural_lc			=	'sent emails';

$draft_object_lc		=	'draft';
$draft_plural_lc		=	'drafts';

// *** Message definitions
$msg_no_recipients		=	'No recipients found! Email has not been sent.';
$msg_queued				=	'Email has been handed to the mail delivery system.';
$msg_saved				=	'Email saved';
$msg_sending_limits		=	'A maximum of [MAX] email per [MINS] minutes will be sent.';
$msg_sent				=	'Email has been sent to [AMOUNT] recipient(s).';
$msg_deleted			=	'Email deleted.';
$msg_deleted_m			=	'Emails deleted.';
$msg_delete_confirm		=	'Are you sure you want to delete this email?';
$msg_delete_confirm_m	=	'Are you sure you want to delete these emails?';
$msg_attachment_folder	=	'The folder for attachments does not exist and could not be created.';
$msg_upload_error		=	'Something went wrong while uploading one of the attachments.';
$msg_no_mail_addresses	=	"No email addresses set-up to email with!
							<br/>You can do this at the General Settings of the CMS, under the setting 'Mailing'.";

// *** Form and text elements
define('TXT_NO_SUBJECT',				'(no subject)');
define('TXT_BCC_MESSAGE',				'Email sent through the CMS mailer to:');
define('TXT_BCC',						'BCC');
define('TXT_ADDRESSEES',				'recipients');
define('TXT_SENT_TO',					'sent to');
define('TXT_SAVED',						'saved');
define('TXT_NEW_ITEM',					'new email');
define('TXT_TS_SENT',					'Sent');
define('TXT_TS_SAVED',					'Saved');
define('TXT_EDIT_ITEM',					'Edit email');
define('TXT_DELETE_ITEM',				'Delete email');
define('TXT_SUBJECT',					'Subject');
define('TXT_FROM',						'From');
define('TXT_TO',						'To');
define('TXT_SEPARATE_NOTICE',			'Separate multiple recipients with a comma or a semi-colon');
define('TXT_MESSAGE',					'Message');
define('TXT_ATTACHMENTS',				'Attachment(s)');
define('TXT_MORE_ATTACHMENTS',			'more attachments?');
define('BUTTON_SEND',					'Send');

?>