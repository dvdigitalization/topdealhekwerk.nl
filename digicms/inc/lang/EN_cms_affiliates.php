<?php
/*
Title:		cms_affiliates.php language definitions (EN)
File: 		inc/lang/NL_cms_affiliates.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Affiliate';
$object_lc 				= 	'affiliate';
$object_lc_deleted		=	'deleted affiliate';
$plural					=	'Affiliates';
$plural_lc				=	'affiliates';
$plural_lc_deleted		=	'deleted affiliates';

// *** Message definitions
$msg_added 				= 	'Affiliate added.';
$msg_editted 			= 	'Affiliate editted.';
$msg_deleted			=	'Affiliate deleted.';
$msg_deleted_m 			= 	'Affiliates deleted.';
$msg_delete_confirm		= 	'Are you sure you want to delete this affiliate?';
$msg_delete_confirm_m 	= 	'Are you sure you want to delete these affiliates?';
$msg_perma_confirm		= 	'Are you sure you want to permanently delete this affiliate?';
$msg_perma_confirm_m 	= 	'Are you sure you want to permanently delete these affiliates?';
$msg_restored			= 	'Affiliate restored.';
$msg_restored_m	 		= 	'Affiliates restored.';
$msg_restore_confirm	= 	'Are you sure you want to restore this affiliate?';
$msg_restore_confirm_m 	= 	'Are you sure you want to restore these affiliates?';
$msg_activated			=	'Affiliate activated.';
$msg_activated_m		=	'Affiliates activated.';
$msg_deactivated		=	'Affiliate deactivated.';
$msg_deactivated_m		=	'Affiliates deactivated.';
$msg_exists				=	'An affiliate with this name and URL already exists.';

// *** Form and text elements
define('TXT_ADD_ITEM',							'Add '.$object_lc);
define('TXT_EDIT_ITEM',							'Edit '.$object_lc);
define('TXT_RESTORE_DELETED_ITEMS',				'Restore deleted '.$object_lc);
define('TXT_RESTORE_ITEM',						'Restore '.$object_lc);
define('TXT_RESTORE_ITEMS',						'Restore '.$plural_lc);
define('TXT_DELETE_PERM_ITEM',					'Permanently delete '.$object_lc);
define('TXT_DELETE_PERM_ITEMS',					'Permanently delete '.$plural_lc);
define('TXT_DELETE_ITEM',						'Delete '.$object_lc);
define('TXT_DELETE_ITEMS',						'Delete '.$plural_lc);
define('TXT_VIEW_ITEM',							'View '.$object_lc.' data');
define('TXT_ACTIVATE_ITEM',						'Activate '.$object_lc);
define('TXT_ACTIVATE_ITEMS',					'Activate '.$plural_lc);
define('TXT_DEACTIVATE_ITEM',					'Deactivate '.$object_lc);
define('TXT_DEACTIVATE_ITEMS',					'Deactivate '.$plural_lc);
define('TXT_SEND_EMAIL',						'Send this '.$object_lc.' an email');

define('TXT_NEW_ITEM',							'new affiliate');
define('TXT_DELETED_ITEMS',						'deleted affiliates');
define('TXT_WEBSITE',							'Website');
define('TXT_CONTACT_PERSON',					'Contact');
define('TXT_REMARKS',							'Remarks');
define('TXT_AFFILIATE_CODE',					'Affiliate code');
define('TXT_AFFILIATE_LINK',					'Affiliate link');
define('TXT_PAGE_HITS',							'Page hits');
define('TXT_VIEW_ORDERS',						'View order');
define('TXT_VISIT_WEBSITE',						'Visit this website');
define('TXT_LINKED_ORDERS',						'Linked orders');
define('TXT_ORDERS',							'Orders');
?>