<?php
/*
Title:		cms_news.php language definitions (EN)
File: 		inc/lang/EN_cms_news.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'News topic';
$object_lc 				= 	'news topic';
$plural					=	'News topics';
$plural_lc				=	'news topics';

// *** Message definitions
$msg_added 				=	'The news topic has been added.';
$msg_editted			=	'The news topic has been editted. Pay attention, if you have changed the image, it is possible that your
							 browser will still display the old image. Refresh the page in that case, or empty your cache.';
$msg_deleted			=	'The news topic has been deleted.';
$msg_deleted_m			=	'The news topics have been deleted.';
$msg_delete_confirm		=	'Are you sure you want to delete this news topic?';
$msg_delete_confirm_m	=	'Are you sure you want to delete these news topics?';
$msg_deactivated		=	'The news topic will no longer be displayed on the website.';
$msg_deactivated_m		=	'The news topics will no longer be displayed on the website.';
$msg_activated			=	'The news topic will be displayed on the website.';
$msg_activated_m		=	'The news topics will be displayed on the website.';
$msg_archived			=	'The news topic has been archived.';
$msg_archived_m			=	'The news topics have been archived.';
$msg_unarchived			=	'The news topic has been taken out of the archive.';
$msg_unarchived_m		=	'The news topics have been taken out of the archive.';
$msg_upload_error		=	'Something went wrong while uploading the image.';
$msg_thumb_error		=	'No thumbnail could be created from the image.';
$msg_resize_error		=	'The image could nog be resized.';
$msg_directory_error	=	'The target directory [DIR] does not exist.';
$msg_rss_editted 		= 	'The RSS settings have been changed.';
$msg_rss_write_failed	=	'Writing to the RSS feed file has failed.';
$msg_comment_deleted	=	'The comment has been deleted.';
$msg_comm_del_block		=	'The comment has been deleted and the IP address has been blocked.';

// *** Form and text elements
define('TXT_ADD_ITEM',					'Add '.$object_lc);
define('TXT_EDIT_ITEM',					'Edit '.$object_lc);
define('TXT_VIEW_ITEM',					'View this '.$object_lc.' on the website');
define('TXT_DELETE_ITEM',				'Delete '.$object_lc);
define('TXT_ACTIVATE_ITEM',				'Activate '.$object_lc);
define('TXT_DEACTIVATE_ITEM',			'De-activate '.$object_lc);
define('TXT_ARCHIVE_ITEM',				'Archive '.$object_lc);
define('TXT_UNARCHIVE_ITEM',			'Unarchive '.$object_lc);
define('TXT_DELETE_ITEMS',				'Delete '.$plural_lc);
define('TXT_ACTIVATE_ITEMS',			'Activate '.$plural_lc);
define('TXT_DEACTIVATE_ITEMS',			'De-activate '.$plural_lc);
define('TXT_ARCHIVE_ITEMS',				'Archive '.$plural_lc);
define('TXT_UNARCHIVE_ITEMS',			'Unarchive '.$plural_lc);
define('TXT_NEW_ITEM',					'new news topic');
define('TXT_CONTENTS',					'Topic');
define('TXT_PREVIEW',					'Preview');
define('TXT_VIEW_COMMENTS',				'View comments');
define('TXT_COMMENTS_FOR_THIS_TOPIC',	'Comments on this news topic');
define('TXT_MAIL_THIS_PERSON',			'Send this person an email');
define('TXT_DELETE_COMMENT',			'Delete this comment');
define('TXT_DELETE_COMMENT_AND_BLOCK',	'Delete this comment and block IP address');
define('TXT_TIMES_VIEWED',				'Viewed');

define('TXT_AUTHOR',					'Author');
define('TXT_RSS_DESCRIPTION',			'RSS description');
define('TXT_CHANGE_RSS_SETTINGS',		'Change RSS settings');
define('TXT_RSS_SETTINGS',				'RSS settings');
define('TXT_RSS_FEED',					'RSS feed');
define('LOG_RSS_UPDATED',				'RSS settings changed');
define('LOG_COMMENT_DELETED',			'Comment deleted');
?>