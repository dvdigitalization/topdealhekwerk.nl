<?php
/*
Title:		cms_blocked_ips.php language definitions (NL)
File: 		inc/lang/NL_cms_blocked_ips.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'IP adres';
$object_lc 				= 	'IP adres';
$plural					=	'IP adressen';
$plural_lc				=	'IP adressen';

// *** Message definitions
$msg_added				=	'IP adres is toegevoegd aan de lijst.';
$msg_editted			=	'IP adres is bijgewerkt.';
$msg_deleted			=	'IP adres is verwijderd van de lijst.';
$msg_deleted_m			=	'De IP adresssen zijn verwijderd van de lijst.';
$msg_delete_confirm		=	'Dit IP adres zeker weten verwijderen van de lijst?';
$msg_delete_confirm_m	=	'Deze IP adresssen zeker weten verwijderen van de lijst?';
$msg_no_ip				=	'Geen IP adres ingevuld.';
$msg_illegal_ip_format	=	'Ongeldig IP adres ingevuld.';
$msg_bad_word_blocked	=	'Woord toegevoegd aan de lijst van SPAM woorden:';
$msg_ip_exists			=	'Dit IP adres staat al op de lijst.';

// *** Form and text elements
define('TXT_ADD_ITEM',					'IP adres toevoegen aan de lijst');
define('TXT_EDIT_ITEM',					'IP adres wijzigen');
define('TXT_DELETE_ITEM',				'IP adres verwijderen');
define('TXT_DELETE_ITEMS',				'IP adressen verwijderen');
define('TXT_NEW_ITEM',					'nieuw IP adres toevoegen');
define('TXT_BLOCKED',					'Geblokkeerd');

?>