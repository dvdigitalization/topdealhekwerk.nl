<?php
/*
Title:		cms_template.php language definitions (NL) [TEMPLATE]
File: 		inc/lang/NL_cms_template.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Product unit';
$object_lc 				= 	'prduct unit';
$plural					=	'Product units';
$plural_lc 				= 	'product units';

$name_title				=	'Name';		//Naming title

// *** Messages
$msg_added				=	"$object added.";
$msg_editted			=	"$object editted.";
$msg_deleted			=	"$object deleted.";
$msg_deleted_m			=	"$plural deleted.";
$msg_delete_confirm		=	"Are you sure you want to delete this $object_lc?";
$msg_delete_confirm_m	=	"Are you sure you want to delete these $plural_lc?";
$msg_could_not_delete 	= 	"$object could not be deleted. There are other dependent elements in the database.";
$msg_not_all_deleted	=	$msg_deleted_m." Some $plural_lc could not be deleted, because there are other dependent elements in the database.";
$msg_activated			=	"$object activated.";
$msg_activated_m		=	"$plural activated.";
$msg_deactivated		=	"$object deactivated.";
$msg_deactivated_m		=	"$plural deactivated.";

// *** Form and text elements
define('TXT_ADD_ITEM',					'Add '.$object_lc);
define('TXT_EDIT_ITEM',					'Edit '.$object_lc);
define('TXT_VIEW_ITEM',					'View '.$object_lc.' details');
define('TXT_DELETE_ITEM',				'Delete '.$object_lc);
define('TXT_ACTIVATE_ITEM',				'Activate '.$object_lc);
define('TXT_DEACTIVATE_ITEM',			'De-activate '.$object_lc);
define('TXT_DELETE_ITEMS',				'Delete '.$plural_lc);
define('TXT_ACTIVATE_ITEMS',			'Activate '.$plural_lc);
define('TXT_DEACTIVATE_ITEMS',			'De-activate '.$plural_lc);
define('TXT_MOVE_ITEM_UP',				'Move '.$object.' up');
define('TXT_MOVE_ITEM_DOWN',			'Move '.$object.' down');
define('TXT_NEW_ITEM',					'new product unit');

define('TXT_BACK_TO_ARTICLES',			'back to the products');

?>