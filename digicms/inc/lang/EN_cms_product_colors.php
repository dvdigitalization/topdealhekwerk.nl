<?php
/*
Title:		cms_product_colors.php language definitions (EN)
File: 		inc/lang/EN_cms_product_colors.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Template';
$object_lc 				= 	'template';
$plural					=	'Templates';
$plural_lc				=	'templates';

// *** Message definitions
$msg_added 				=	'The xxx has been added.';
$msg_editted			=	'The xxx has been editted.';
$msg_deleted			=	'The xxx has been deleted.';
$msg_deleted_m			=	'The xxxs have been deleted.';
$msg_delete_confirm		=	'Are you sure you want to delete this xxx?';
$msg_delete_confirm_m	=	'Are you sure you want to delete these xxxs?';
$msg_deactivated		=	'The xxx will no longer be displayed on the website.';
$msg_deactivated_m		=	'The xxxs will no longer be displayed on the website.';
$msg_activated			=	'The xxx will be displayed on the website.';
$msg_activated_m		=	'The xxxs will be displayed on the website.';
$msg_archived			=	'The xxx has been archived.';
$msg_archived_m			=	'The xxxs have been archived.';
$msg_unarchived			=	'The xxx has been taken out of the archive.';
$msg_unarchived_m		=	'The xxxs have been taken out of the archive.';
$msg_upload_error		=	'Something went wrong while uploading the image.';
$msg_file_upload_error	=	'Something went wrong while uploading the file.';
$msg_thumb_error		=	'No thumbnail could be created from the image.';
$msg_resize_error		=	'The image could nog be resized.';
$msg_directory_error	=	'The target directory [DIR] does not exist.';

// *** Form and text elements
define('TXT_ADD_ITEM',					'Add '.$object_lc);
define('TXT_EDIT_ITEM',					'Edit '.$object_lc);
define('TXT_VIEW_ITEM',					'View '.$object_lc.' details');
define('TXT_DELETE_ITEM',				'Delete '.$object_lc);
define('TXT_ACTIVATE_ITEM',				'Activate '.$object_lc);
define('TXT_DEACTIVATE_ITEM',			'De-activate '.$object_lc);
define('TXT_ARCHIVE_ITEM',				'Archive '.$object_lc);
define('TXT_UNARCHIVE_ITEM',			'Unarchive '.$object_lc);
define('TXT_ITEM_UP',					'Move '.$object_lc.' up');
define('TXT_ITEM_DOWN',					'Move '.$object_lc.' down');
define('TXT_DELETE_ITEMS',				'Delete '.$plural_lc);
define('TXT_ACTIVATE_ITEMS',			'Activate '.$plural_lc);
define('TXT_DEACTIVATE_ITEMS',			'De-activate '.$plural_lc);
define('TXT_ARCHIVE_ITEMS',				'Archive '.$plural_lc);
define('TXT_UNARCHIVE_ITEMS',			'Unarchive '.$plural_lc);
define('TXT_NEW_ITEM',					'new xxx');
define('TXT_CONTENTS',					'Contents');

?>