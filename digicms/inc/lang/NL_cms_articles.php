<?php
/*
Title:		cms_template.php language definitions (NL) [TEMPLATE]
File: 		inc/lang/NL_cms_template.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Artikel';
$object_lc 				= 	'artikel';
$plural					=	'Artikelen';
$plural_lc				=	'artikelen';

// *** Messages
$msg_added				=	'Artikel toegevoegd.';
$msg_editted			=	'Artikel opgeslagen.';
$msg_deleted			=	'Artikel verwijderd.';
$msg_deleted_m			=	'Artikelen verwijderd.';
$msg_delete_confirm		=	'Dit artikel zeker weten verwijderen?';
$msg_delete_confirm_m	=	'Deze artikelen zeker weten verwijderen?';
$msg_no_article_cats	=	'Er zijn nog geen artikel categorieen aangemaakt. Maak deze eerst aan.';

// *** Form and text elements
define('TXT_ADD_ITEM',					$object.' toevoegen');
define('TXT_EDIT_ITEM',					$object.' wijzigen');
define('TXT_VIEW_ITEM',					$object.' gegevens inzien');
define('TXT_DELETE_ITEM',				$object.' verwijderen');
define('TXT_ACTIVATE_ITEM',				$object.' activeren');
define('TXT_DEACTIVATE_ITEM',			$object.' de-activeren');
define('TXT_DELETE_ITEMS',				$plural.' verwijderen');
define('TXT_ACTIVATE_ITEMS',			$plural.' activeren');
define('TXT_DEACTIVATE_ITEMS',			$plural.' de-activeren');
define('TXT_NEW_ITEM',					'nieuw artikel');

define('TXT_ARTICLE_CATEGORIES',		'artikel categorieen');
define('TXT_ARTICLE_UNITS',				'artikel eenheden');

define('TXT_ARTICLE_DATA',				'Artikel gegevens');
define('TXT_ARTICLE_NAME',				'Artikel naam');
define('TXT_ARTICLE_NR',				'Artikel nummer');
define('TXT_PRICES_AND_VAT',			'Prijzen en BTW');
define('TXT_COST_PRICE',				'Kost prijs');
define('TXT_SALE_PRICE',				'Verkoop prijs');
define('TXT_VAT_LEVEL',					'BTW tarief');
define('TXT_SUPPLY_AND_UNIT',			'Voorraad en eenheid');
define('TXT_AMOUNT_IN_STOCK',			'Aantal in voorraad');
define('TXT_UNIT',						'Eenheid');
define('TXT_ADD_MORE_ARTICLES',			'Meer artikelen toevoegen?');

define('TXT_VAT',						'BTW');


?>