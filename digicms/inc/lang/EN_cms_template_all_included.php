<?php
/*
Title:		cms_site_projects.php language definitions (EN)
File: 		inc/lang/EN_cms_site_projects.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Project';
$object_lc 				= 	'project';
$plural					=	'Projects';
$plural_lc				=	'projects';

// *** Message definitions
$msg_added 				=	'The project has been added.';
$msg_editted			=	'The project has been editted.';
$msg_deleted			=	'The project has been deleted.';
$msg_deleted_m			=	'The projects have been deleted.';
$msg_delete_confirm		=	'Are you sure you want to delete this project?';
$msg_delete_confirm_m	=	'Are you sure you want to delete these projects?';
$msg_p_delete_confirm	=	'Are you sure you want to delete this picture?';
$msg_p_delete_confirm_m	=	'Are you sure you want to delete these pictures?';
$msg_photo_deleted		=	'Picture deleted.';
$msg_photo_deleted_m	=	'Pictures verwijderd.';
$msg_deactivated		=	'The project will no longer be displayed on the website.';
$msg_deactivated_m		=	'The projects will no longer be displayed on the website.';
$msg_activated			=	'The project will be displayed on the website.';
$msg_activated_m		=	'The projects will be displayed on the website.';
$msg_highlighted		= 	'The project is highlighted.';
$msg_highlighted_m		= 	'The projects are highlighted.';
$msg_unhighlighted		= 	'The project is no longer highlighted.';
$msg_unhighlighted_m	= 	'The projects are no longer highlighted.';
$msg_album_empty		=	'This project has no pictures yet.';
$msg_directory_error	=	'The target folder or de thumbnail folder does not exist.';
$msg_upload_error		=	'Something went wrong while uploading the picture.';
$msg_resize_error		=	'Something went wrong while resizing ';
$msg_thumb_error		=	'Something went wrong while making the thumbnail of ';
$msg_none_uploaded		=	'No pictures were uploaded.';
$msg_none_processed		=	'No pictures were editted.';
$msg_some_photos_failed	=	'Some pictures could not be editted.';
$msg_some_deleted		=	'Some pictures no longer existed. These were deleted.';
$msg_photo_editted		=	'Picture editted.';

// *** Form and text elements
define('TXT_ADD_ITEM',					'Add '.$object_lc);
define('TXT_EDIT_ITEM',					'Edit '.$object_lc);
define('TXT_VIEW_ITEM',					'View '.$object_lc.' details');
define('TXT_DELETE_ITEM',				'Delete '.$object_lc);
define('TXT_ACTIVATE_ITEM',				'Activate '.$object_lc);
define('TXT_DEACTIVATE_ITEM',			'De-activate '.$object_lc);
define('TXT_HIGHLIGHT_ITEM',			'Highlight '.$object_lc);
define('TXT_UNHIGHLIGHT_ITEM',			'Un-highlight '.$object_lc);
define('TXT_ITEM_UP',					'Move '.$object_lc.' up');
define('TXT_ITEM_DOWN',					'Move '.$object_lc.' down');
define('TXT_DELETE_ITEMS',				'Delete '.$plural_lc);
define('TXT_ACTIVATE_ITEMS',			'Activate '.$plural_lc);
define('TXT_DEACTIVATE_ITEMS',			'De-activate '.$plural_lc);  
define('TXT_LOCATION',					'Location');
define('TXT_HIGHLIGHTED',				'Highlighted');
define('TXT_AREA',						'Area');
define('TXT_NEW_ITEM',					'new project');
define('TXT_ADD_PHOTOS',				'Add photos');
define('TXT_ADD_DOCUMENTS',				'Add documents');
define('TXT_PICTURES',					'Pictures');
define('TXT_CONTENTS',					'Contents');

?>