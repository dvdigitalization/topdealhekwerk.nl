<?php
/*
Title:		cms_page_contents.php language definitions (NL) [TEMPLATE]
File: 		inc/lang/NL_cms_page_contents.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Pagina set';
$object_lc 				= 	'pagina set';
$plural					=	'Pagina sets';
$plural_lc				=	'pagina sets';

// *** Message definitions
$msg_element_added		=	'Tekst element is toegevoegd, het is nu aan te roepen met: ';
$msg_elements_editted	=	'Tekst elementen zijn bijgewerkt.';
$msg_pages_editted		=	'Pagina sets zijn bijgewerkt.';
$msg_element_exists		=	'Er bestaat al een tekst element met deze naam in deze pagina set!';
$msg_empty_fields		=	'U moet een element naam en een pagina set opgeven.';
$msg_delete_confirm		=	'Dit tekst element zeker weten verwijderen?';
$msg_delete_ps_confirm	=	'Deze pagina set en alle bijbehorende tekst elementen zeker weten verwijderen?';
$msg_deleted			=	'Tekst element verwijderd.';
$msg_deleted_ps			=	'Pagina set verwijderd.';
$msg_invalid_to_url		=	'Let op! De opgegeven nieuwe afbeeldingen map lijkt niet te bestaan.';
$msg_images_fixed		=	'De afbeelding links zijn gefixed.';

// *** Form and text elements
define('TXT_ADD_ITEM',					'Nieuw tekst element toevoegen (alle talen)');
define('TXT_EDIT_ITEM',					$object.' wijzigen');
define('TXT_DELETE_ITEM',				$object.' verwijderen');

define('TXT_ELEMENT_NAME',				'Element naam');
define('TXT_FCK_FIELD',					'FCK veld');
define('TXT_TEXT',						'Tekst');

define('TXT_EDIT_PAGE_SETS',			'Pagina sets wijzigen');
define('TXT_NEW_ITEM',					'nieuw tekst element');
define('TXT_ALTER_PAGE_SETS',			'pagina sets wijzigen');

define('TXT_PAGE_SET',					'Pagina set');
define('TXT_PAGE_SETS',					'Pagina sets');
define('LOG_PAGE_SETS_EDITTED',			'Pagina sets gewijzigd');
?>