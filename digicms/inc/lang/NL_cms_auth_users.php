<?php
/*
Title:		cms_auth_users.php language definitions (NL)
File: 		inc/lang/NL_cms_auth_users.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'CMS gebruiker';
$object_lc 				= 	'CMS gebruiker';
$plural					=	'CMS gebruikers';
$plural_lc				=	'CMS gebruikers';

// *** Message definitions
$msg_added				=	'Gebruiker toegevoegd.';
$msg_editted			=	'Gebruiker gegevens aangepast.';
$msg_deleted			=	'Gebruiker is verwijderd.';
$msg_deleted_m			=	'Gebruikers zijn verwijderd.';
$msg_delete_confirm		=	'Deze gebruiker zeker weten verwijderen?';
$msg_delete_confirm_m	=	'Deze gebruikers zeker weten verwijderen?';
$msg_activated			=	'Gebruiker geactiveerd.';
$msg_activated_m		=	'Gebruikers geactiveerd.';
$msg_deactivated		=	'Gebruiker op non-actief gezet.';
$msg_deactivated_m		=	'Gebruikers op non-actief gezet.';
$msg_newpass			=	'Het nieuwe wachtwoord is ingesteld.';
$msg_purged				=	'Activiteiten log is geleegd.';
$msg_no_username		=	'U moet een gebruikersnaam invullen.';
$msg_no_pass			=	'U moet een wachtwoord invullen en dit twee maal herhalen.';
$msg_pass_dont_match	=	'De wachtwoorden komen niet overeen.';
$msg_username_exists	=	'Er bestaat al een gebruiker met deze gebruikersnaam.';
$msg_invalid_username	=	'Ongeldige gebruikersnaam.';
$msg_invalid_chars_user	=	'U mag alleen letters, cijfers een streepje en een underscore gebruiken voor de gebruikersnaam.';
$msg_invalid_chars_pass	=	'U mag alleen letters, cijfers een streepje en een underscore gebruiken voor het wachtwoord.';
$msg_invalid_email		=	'Ongeldig email adres ingevoerd.';
$msg_unable_to_delete	=	'U kunt deze gebruiker niet verwijderen.';
$msg_login_confirm		=	'Inloggen als deze gebruiker?';
$msg_login_failed		=	'Inloggen als deze gebruiker is niet gelukt.';
$msg_invalid_action		=	'Ongeldige bewerking.';

// *** Form and text elements
define('TXT_ADD_USER',					'Gebruiker toevoegen');
define('TXT_USE_IN_ADMIN',				'Gebruik in administratie');
define('TXT_ADMINISTRATOR',				'Administrator');
define('TXT_PRIVS',						'Privilleges');
define('TXT_LOGIN_ALLOWED',				'inloggen toegestaan');
define('TXT_LOGIN_NOT_ALLOWED',			'inloggen niet toegestaan');
define('TXT_ADMIN_ACCOUNT',				'Administrator account');
define('TXT_CMS_USER',					'CMS gebruiker');
define('TXT_NO_ACTIVITIES_LOGGED',		'Geen activiteiten gelogd');
define('TXT_ACTIVITY_LOG_OF',			'Activiteiten log van');
define('TXT_EMPTY_LOG',					'activiteiten log legen');
define('TXT_NEW_USER',					'nieuwe gebruiker');
define('TXT_EMPTY_ALL_LOGS',			'alle activiteiten logs legen');
define('TXT_OFFLINE',					'offline');
define('TXT_ONLINE',					'online');
define('TXT_USER_IS_OFFLINE',			'Gebruiker is offline');
define('TXT_USER_IS_ONLINE',			'Gebruiker is online');
define('TXT_LOGIN_AS',					'Inloggen als');
define('TXT_USER_IS_ADMIN',				'Gebruiker is een administrator');
define('TXT_UNBLOCK_USER',				'Gebruiker deblokkeren');
define('TXT_DISALLOW_LOGIN',			'Gebruiker niet toestaan in te loggen');
define('TXT_ALLOW_LOGIN',				'Gebruiker toestaan in te loggen');
define('TXT_SHOW_ACTIVITY_LOG',			'Activiteiten log weergeven');
define('TXT_EDIT_USER',					'Gebruiker gegevens bewerken');
define('TXT_DELETE_USER',				'Gebruiker verwijderen');
define('TXT_DELETE_USERS',				'Gebruikers verwijderen');
define('TXT_ACTIVATE_USERS',			'Gebruikers activeren');
define('TXT_DEACTIVATE_USERS',			'Gebruikers de-activeren');
define('TXT_VIEW_USER_DETAILS',			'Gebruiker gegevens inzien');
define('TXT_LAST_LOGIN',				'laatste login');
define('TXT_FROM',						'vanaf');
define('TXT_USER',						'Gebruiker');
define('TXT_ADMIN',						'Admin');
define('TXT_MARK_OR_CLEAR_ALL_PRIVS',	'Alle privilleges markeren/leegmaken');
define('TXT_IP_LOGIN_1',				'Auto login IP (1)');
define('TXT_IP_LOGIN_2',				'Auto login IP (2)');
?>