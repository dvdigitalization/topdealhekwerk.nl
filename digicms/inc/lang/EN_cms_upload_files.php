<?php
/*
Title:		cms_upload_files.php language definitions (EN)
File: 		inc/lang/EN_cms_upload_files.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$doc_object				=	'Document';
$doc_object_lc 			= 	'document';
$doc_plural				=	'Documents';
$doc_plural_lc			=	'documents';

$img_object				=	'Image';
$img_object_lc 			= 	'image';
$img_plural				=	'Images';
$img_plural_lc			=	'images';

$fla_object				=	'Flash animation';
$fla_object_lc 			= 	'flash animation';
$fla_plural				=	'Flash animations';
$fla_plural_lc			=	'flash animations';

// *** Message definitions
$doc_msg_none_uploaded		=	'No documents were uploaded.';
$doc_msg_deleted			=	'Document deleted.';
$doc_msg_deleted_m			=	'Documents deleted.';
$doc_msg_delete_confirm		=	'Are you sure you want to delete this document?';
$doc_msg_delete_confirm_m	=	'Are you sure you want to delete these documents?';
$doc_msg_del_error			=	'This document could not be deleted.';
$doc_msg_doesnt_exist		=	'This document does not exist.';
$doc_upload_new				=	'upload new documents';

$img_msg_none_uploaded		=	'No images were uploaded.';
$img_msg_deleted			=	'Image deleted.';
$img_msg_deleted_m			=	'Images deleted.';
$img_msg_delete_confirm		=	'Are you sure you want to delete this image?';
$img_msg_delete_confirm_m	=	'Are you sure you want to delete these images?';
$img_msg_del_error			=	'This image could not be deleted.';
$img_msg_doesnt_exist		=	'This image does not exist.';
$img_upload_new				=	'upload new images';

$fla_msg_none_uploaded		=	'No flash animations were uploaded.';
$fla_msg_deleted			=	'Flash animation deleted.';
$fla_msg_deleted_m			=	'Flash animations deleted.';
$fla_msg_delete_confirm		=	'Are you sure you want to delete this flash animation?';
$fla_msg_delete_confirm_m	=	'Are you sure you want to delete these flash animations?';
$fla_msg_del_error			=	'This flash animation could not be deleted.';
$fla_msg_doesnt_exist		=	'This flash animation does not exist.';
$fla_upload_new				=	'upload new flash animations';

$msg_upload_error			=	'Something went wrong while uploading ';
$msg_resize_error			=	'Something went wrong while resizing ';
$msg_directory_error		=	'The target folder does not exist.';
$msg_delete_f_confirm		=	'Are you sure you want to delete this folder?';
$msg_delete_ff_confirm		=	'Are you sure you want to delete this folder?<br/>The files in this folder will be deleted as well.';
$msg_folder_deleted			=	'The folder has been deleted.';
$msg_folder_created			=	'The folder has been created.';
$msg_folder_not_created		=	'The folder could not be created.';
$msg_folder_not_deleted		=	'The folder could not be deleted.';
$msg_invalid_folder_name	=	'The folder name you specified is invalid.';
$msg_folder_exists			=	'This folder already exists.';

// *** Form and text elements
define('TXT_MANAGE_FOLDERS',		'manage folders');
define('TXT_CHOOSE_FILETYPE',		'Choose file type');
define('TXT_DOCUMENTS',				'documents');
define('TXT_IMAGES',				'images');
define('TXT_FLASH_ANIMATIONS',		'flash animations');
define('TXT_FOLDER',				'Folder');
define('TXT_DELETE_FILE',			'Delete file');
define('TXT_DELETE_FILES',			'Delete files');
define('TXT_VIEW_IMAGE',			'View image');
define('TXT_NEW_FOLDER',			'new folder');
define('TXT_DELETE_FOLDER',			'Delete folder');
define('TXT_ORIGINAL_SIZE',			'Original size');
define('BUTTON_UPLOAD',				'Upload');
define('TXT_OVERWRITE',				'Overwrite existing [ITEMS]?');
define('TXT_UPLOAD_MORE',			'Upload more [ITEMS]?');
define('TXT_TO_FOLDER',				'To folder');
define('TXT_RESIZE_IMAGES',			'Resize images?');
define('TXT_UPLOAD_FILES',			'Upload files');
define('TXT_UPLOADED_TO',			'uploaded to the folder');
define('TXT_NO_FOLDERS_YET',		'No folders yet');
?>