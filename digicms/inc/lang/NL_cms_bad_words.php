<?php
/*
Title:		cms_bad_words.php language definitions (NL)
File: 		inc/lang/NL_cms_bad_words.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'SPAM woord';
$object_lc 				= 	'SPAM woord';
$plural					=	'SPAM woorden';
$plural_lc				=	'SPAM woorden';

// *** Message definitions
$msg_added				=	'Het woord is toegevoegd aan de SPAM lijst.';
$msg_editted			=	'Het woord is aangepast.';
$msg_deleted			=	'Het woord is verwijderd van de SPAM lijst.';
$msg_deleted_m			=	'De woorden zijn verwijderd van de SPAM lijst.';
$msg_delete_confirm		=	'Dit woord zeker weten verwijderen van de SPAM lijst?';
$msg_delete_confirm_m	=	'Deze woorden zeker weten verwijderen van de SPAM lijst?';
$msg_no_word			=	'Geen woord ingevuld.';
$msg_word_exists		=	'Dit woord staat al op de SPAM lijst.';
$msg_blocking			=	'IP adres zal met gebruik van dit woord direct geblokkeerd worden.';
$msg_blocking_m			=	'IP adres zal met gebruik van deze woorden direct geblokkeerd worden.';
$msg_not_blocking		=	'IP adres zal met gebruik van dit woord niet meer direct geblokkeerd worden.';
$msg_not_blocking_m		=	'IP adres zal met gebruik van deze woorden niet meer direct geblokkeerd worden.';
$msg_substr				=	'Woord zal ook gefilterd worden als het in een ander woord zit.';
$msg_no_substr			=	'Woord zal alleen gefilterd worden als vrijstaand woord.';

// *** Form and text elements
define('TXT_ADD_ITEM',					'Woord toevoegen aan de SPAM lijst');
define('TXT_EDIT_ITEM',					'Woord wijzigen');
define('TXT_DELETE_ITEM',				'Woord verwijderen');
define('TXT_BLOCK_ITEM',				'IP adres direct blokkeren bij gebruik van dit woord');
define('TXT_UNBLOCK_ITEM',				'IP adres niet direct blokkeren bij gebruik van dit woord');
define('TXT_FILTER_SEPARATE_ONLY',		'Woord alleen als vrijstaand woord filteren');
define('TXT_FILTER_STRING',				'Woord ook filteren als inwendige string');
define('TXT_DELETE_ITEMS',				'Woorden verwijderen');
define('TXT_BLOCK_ITEMS',				'IP adres direct blokkeren bij gebruik van deze woorden');
define('TXT_UNBLOCK_ITEMS',				'IP adres niet direct blokkeren bij gebruik van deze woorden');
define('TXT_NEW_ITEM',					'nieuw woord toevoegen aan de SPAM lijst');
define('TXT_WORD_SENTENCE',				'Woord of zin');
define('TXT_BLOCKED',					'Geblokkeerd');
define('TXT_IP_BLOCK',					'IP block');
define('TXT_SEPARATE',					'Vrijstaand');
define('TXT_BLOCK',						'ip blokkeren');
define('TXT_UNBLOCK',					'ip niet blokkeren');

?>