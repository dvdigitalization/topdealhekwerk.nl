<?php
/*
Title:		cms_settings.php language definitions (NL)
File: 		inc/lang/NL_cms_settings.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Message definitions
$msg_wrong_pass			=	'Onjuist wachtwoord ingevoerd. Gegevens zijn niet gewijzigd.';
$msg_pass_dont_match	=	'De nieuwe ingevoerde wachtwoorden komen niet overeen. Gegevens zijn niet gewijzigd.';
$msg_invalid_chars_user	=	'U mag alleen letters, cijfers een streepje en een underscore gebruiken voor de gebruikersnaam.';
$msg_invalid_chars_pass	=	'U mag alleen letters, cijfers een streepje en een underscore gebruiken voor het wachtwoord.';
$msg_invalid_email		=	'Ongeldig email adres ingevoerd.';
$msg_data_modified		=	'Gegevens zijn gewijzigd.';
$msg_settings_modified	=	'Instellingen bijgewerkt.';
$msg_setting_added		=	'Instelling toegevoegd. De instelling is nu aan te roepen met: ';
$msg_setting_exists		=	'Deze instelling bestaat al.';
$msg_setting_deleted	=	'Instelling verwijderd.';
$msg_confirm_delete		=	'Deze instelling zeker weten verwijderen?';
$msg_error_log_na		=	'Het error log bestand is niet gevonden.';

// *** Form and text elements
define('TXT_ADD_SETTING',				'Nieuwe instelling toevoegen');
define('TXT_SETTING_NAME',				'Instelling');
define('TXT_SETTING_SET',				'Instelling set');
define('TXT_DIGI_PROTECTED',			'Digi protected');
define('TXT_TYPE_YESNO',				'Type yes/no');
define('TXT_TYPE_INT',					'Type integer');
define('TXT_VALUE',						'Waarde');
define('TXT_PERFORM_SQL',				'SQL queries uitvoeren');
define('TXT_QUERIES_FIELD',				'SQL queries (scheiden met punt-komma)');
define('TXT_PERFORM',					'Uitvoeren');
define('TXT_NEW_SETTING',				'nieuwe instelling');
define('TXT_MAKE_INSTALL_SQL',			'install SQL maken');
define('TXT_USE_EXTERNAL_MAILER',		'Nee (gebruik extern mail programma)');
define('TXT_USE_CMS_MAILER',			'Ja (gebruik CMS mailer)');
define('TXT_CHANGE_USER_INFO',			'Gebruiker gegevens wijzigen');
define('TXT_CMS_MAILER_OR_NOT',			'Mailer gebruiken');
define('TXT_CURRENT_PASSWORD',			'Huidig wachtwoord');
define('TXT_USER_INFO',					'Gebruiker gegevens');
define('TXT_DELETE_SETTING',			'Instelling verwijderen');
define('TXT_CHANGE_SETTINGS',			'Instellingen wijzigen');
define('TXT_CONTACT_TECHNICAL_1',		'Voor technische zaken wat betreft het CMS of vragen over onderstaande instellingen kunt u');
define('TXT_CONTACT_TECHNICAL_2',		'contact opnemen met');
define('TXT_THROUGH',					'via');
define('TXT_VIEW_ERROR_LOG',			'error log inzien');
define('TXT_QUERY',						'query');
define('TXT_QUERIES',					'queries');
define('TXT_PERFORMED',					'uitgevoerd');
define('TXT_FAILED',					'mislukt');
define('LOG_USER_SETTINGS_ALTERED',		'Gebruikersinstellingen gewijzigd');
define('LOG_SETTINGS_CHANGED',			'Algemene instellingen gewijzigd');
?>