<?php
/*
Title:		cms_menu.php language definitions (EN)
File: 		inc/lang/EN_cms_menu.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Menu item';
$plural					=	'Menu items';
$object_lc 				= 	'menu item';
$plural_lc				=	'menu items';

// *** Message definitions
$msg_added 				=	'The menu item has been added.';
$msg_editted			=	'The menu item has been modified.';
$msg_deleted			=	'The menu item has been deleted.';
$msg_delete_confirm		=	'Are you sure you want to delete this menu item?';
$msg_section_added		=	'The menu section has been added.';
$msg_section_editted	=	'The menu section has been modified.';
$msg_section_deleted	=	'The menu section has been deleted.';
$msg_delete_sec_confirm	=	'Are you sure you want to delete this menu section and all its items?';
$msg_no_pages 			=	'There are no pages yet.';
$msg_no_sections		=	'There are no menu section yet. Create these first.';
$msg_no_items			=	'There are no menu items yet. Create these first.';
$msg_non_linked			=	'Some menu elements are not (yet) correctly linked. These are displayed in red.';
$msg_no_item_selected	=	'You must select a valid menu item.';

// *** Form and text elements
define('LOG_MENU_EDITTED',					'Website menu modified.');

define('TXT_ADD_MENU_ITEM',					'Add [SUB]menu item');
define('TXT_EDIT_MENU_ITEM',				'Edit [SUB]menu item');
define('TXT_DELETE_MENU_ITEM',				'Delete menu item');
define('TXT_MOVE_MENU_ITEM_UP',				'Move menu item up');
define('TXT_MOVE_MENU_ITEM_DOWN',			'Move menu item down');
define('TXT_ADD_SECTION',					'Add menu section');
define('TXT_EDIT_SECTION',					'Edit menu section');
define('TXT_DELETE_SECTION',				'Delete menu section');
define('TXT_MOVE_SECTION_UP',				'Move menu section up');
define('TXT_MOVE_SECTION_DOWN',				'Move menu section down');
define('TXT_NEW_SECTION',					'new menu section');
define('TXT_NEW_MENU_ITEM',					'new menu item');
define('TXT_NEW_SUBMENU_ITEM',				'new sub menu item');

define('TXT_LINK_TO_PAGE',					'Link to page');
define('TXT_SECTION',						'Section');
define('TXT_BELOW',							'Below');
define('TXT_OR_FILE',						'Or file');
define('TXT_OR_MANUAL',						'Or manual');
define('TXT_ADD_TO_ALL_LANGUAGES',			'Add to all languages');
define('TXT_NOT_LINKED',					'not linked');
define('TXT_NOT_YET_LINKED',				'not yet linked');
?>