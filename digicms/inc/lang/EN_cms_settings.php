<?php
/*
Title:		cms_settings.php language definitions (EN)
File: 		inc/lang/EN_cms_settings.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Message definitions
$msg_wrong_pass			=	'Incorrect password entered. User information has not been altered.';
$msg_pass_dont_match	=	'The new passwords do not match. User information has not been altered.';
$msg_invalid_chars_user	=	'You can only use alphanumeric characters or dashes and underscores for your username.';
$msg_invalid_chars_pass	=	'You can only use alphanumeric characters or dashes and underscores for your password.';
$msg_invalid_email		=	'Invalid email address entered.';
$msg_data_modified		=	'User information has been altered.';
$msg_settings_modified	=	'Settings adjusted.';
$msg_setting_added		=	'Setting added. The setting is now accessible through: ';
$msg_setting_exists		=	'This setting already exists.';
$msg_setting_deleted	=	'Setting deleted.';
$msg_confirm_delete		=	'Are you sure you want to delete this setting?';
$msg_error_log_na		=	'The error log could not be found.';

// *** Form and text elements
define('TXT_ADD_SETTING',				'Add new setting');
define('TXT_SETTING_NAME',				'Setting');
define('TXT_SETTING_SET',				'Setting set');
define('TXT_DIGI_PROTECTED',			'Digi protected');
define('TXT_TYPE_YESNO',				'Type yes/no');
define('TXT_TYPE_INT',					'Type integer');
define('TXT_VALUE',						'Value');
define('TXT_PERFORM_SQL',				'Perform SQL queries');
define('TXT_QUERIES_FIELD',				'SQL queries (separate with a semicolon)');
define('TXT_PERFORM',					'Perform');
define('TXT_NEW_SETTING',				'new setting');
define('TXT_MAKE_INSTALL_SQL',			'generate install SQL');
define('TXT_USE_EXTERNAL_MAILER',		'No (use external mail program)');
define('TXT_USE_CMS_MAILER',			'Yes (use CMS mailer)');
define('TXT_CHANGE_USER_INFO',			'Change user information');
define('TXT_CMS_MAILER_OR_NOT',			'Use CMS mailer');
define('TXT_CURRENT_PASSWORD',			'Current password');
define('TXT_USER_INFO',					'User information');
define('TXT_DELETE_SETTING',			'Delete setting');
define('TXT_CHANGE_SETTINGS',			'Change settings');
define('TXT_CONTACT_TECHNICAL_1',		'For technical issues regarding the CMS or for questions about these settings you can');
define('TXT_CONTACT_TECHNICAL_2',		'contact');
define('TXT_THROUGH',					'on');
define('TXT_VIEW_ERROR_LOG',			'view error log');
define('TXT_QUERY',						'query');
define('TXT_QUERIES',					'queries');
define('TXT_PERFORMED',					'performed');
define('TXT_FAILED',					'failed');
define('LOG_USER_SETTINGS_ALTERED',		'User settings changed');
define('LOG_SETTINGS_CHANGED',			'General settings changed');
?>