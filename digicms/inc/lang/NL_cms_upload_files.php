<?php
/*
Title:		cms_upload_files.php language definitions (NL)
File: 		inc/lang/NL_cms_upload_files.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$doc_object				=	'Document';
$doc_object_lc 			= 	'document';
$doc_plural				=	'Documenten';
$doc_plural_lc			=	'documenten';

$img_object				=	'Afbeelding';
$img_object_lc 			= 	'afbeelding';
$img_plural				=	'Afbeeldingen';
$img_plural_lc			=	'afbeeldingen';

$fla_object				=	'Flash animatie';
$fla_object_lc 			= 	'flash animatie';
$fla_plural				=	'Flash animaties';
$fla_plural_lc			=	'flash animaties';

// *** Message definitions
$doc_msg_none_uploaded		=	'Geen documenten geupload.';
$doc_msg_deleted			=	'Document verwijderd.';
$doc_msg_deleted_m			=	'Documenten verwijderd.';
$doc_msg_delete_confirm		=	'Dit document zeker weten verwijderen?';
$doc_msg_delete_confirm_m	=	'Deze documenten zeker weten verwijderen?';
$doc_msg_del_error			=	'Dit document kon niet verwijderd worden.';
$doc_msg_doesnt_exist		=	'Dit document bestaat niet.';
$doc_upload_new				=	'nieuwe documenten uploaden';

$img_msg_none_uploaded		=	'Geen afbeeldingen geupload.';
$img_msg_deleted			=	'Afbeelding verwijderd.';
$img_msg_deleted_m			=	'Afbeeldingen verwijderd.';
$img_msg_delete_confirm		=	'Deze afbeelding zeker weten verwijderen?';
$img_msg_delete_confirm_m	=	'Deze afbeeldingen zeker weten verwijderen?';
$img_msg_del_error			=	'Deze afbeelding kon niet verwijderd worden.';
$img_msg_doesnt_exist		=	'Deze afbeelding bestaat niet.';
$img_upload_new				=	'nieuwe afbeeldingen uploaden';

$fla_msg_none_uploaded		=	'Geen flash animaties geupload.';
$fla_msg_deleted			=	'Flash animatie verwijderd.';
$fla_msg_deleted_m			=	'Flash animaties verwijderd.';
$fla_msg_delete_confirm		=	'Deze flash animatie zeker weten verwijderen?';
$fla_msg_delete_confirm_m	=	'Deze flash animaties zeker weten verwijderen?';
$fla_msg_del_error			=	'Deze flash animatie kon niet verwijderd worden.';
$fla_msg_doesnt_exist		=	'Deze flash animatie bestaat niet.';
$fla_upload_new				=	'nieuwe flash animaties uploaden';

$msg_upload_error			=	'Er ging iets mis tijdens het uploaden van ';
$msg_resize_error			=	'Er ging iets mis tijdens het verkleinen van ';
$msg_directory_error		=	'De doelmap bestaat niet.';
$msg_delete_f_confirm		=	'Deze map zeker weten verwijderen?';
$msg_delete_ff_confirm		=	'Deze map zeker weten verwijderen?<br/>De bestanden in de map zullen ook verwijderd worden.';
$msg_folder_deleted			=	'De map is verwijderd.';
$msg_folder_created			=	'De map is aangemaakt.';
$msg_folder_not_created		=	'De map kon niet aangemaakt worden.';
$msg_folder_not_deleted		=	'De map kon niet verwijderd worden.';
$msg_invalid_folder_name	=	'Deze map naam is niet geldig.';
$msg_folder_exists			=	'Deze map bestaat al.';

// *** Form and text elements
define('TXT_MANAGE_FOLDERS',		'mappen beheren');
define('TXT_CHOOSE_FILETYPE',		'Kies bestandstype');
define('TXT_DOCUMENTS',				'documenten');
define('TXT_IMAGES',				'afbeeldingen');
define('TXT_FLASH_ANIMATIONS',		'flash animaties');
define('TXT_FOLDER',				'Map');
define('TXT_DELETE_FILE',			'Bestand verwijderen');
define('TXT_DELETE_FILES',			'Bestanden verwijderen');
define('TXT_VIEW_IMAGE',			'Bekijk afbeelding');
define('TXT_NEW_FOLDER',			'nieuwe map');
define('TXT_DELETE_FOLDER',			'Map verwijderen');
define('TXT_ORIGINAL_SIZE',			'Originele grootte');
define('BUTTON_UPLOAD',				'Uploaden');
define('TXT_OVERWRITE',				'Bestaande [ITEMS] overschrijven?');
define('TXT_UPLOAD_MORE',			'Meer [ITEMS] uploaden?');
define('TXT_TO_FOLDER',				'Naar map');
define('TXT_RESIZE_IMAGES',			'Afbeeldingen verkleinen?');
define('TXT_UPLOAD_FILES',			'Bestanden uploaden');
define('TXT_UPLOADED_TO',			'geupload naar de map');
define('TXT_NO_FOLDERS_YET',		'Nog geen mappen aangemaakt');
?>