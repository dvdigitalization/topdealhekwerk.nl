<?php
/*
Title:		cms_template.php language definitions (NL) [TEMPLATE]
File: 		inc/lang/NL_cms_template.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Artikel categorie';
$object_lc 				= 	'artikel categorie';
$plural					=	'Artikel categorieen';
$plural_lc 				= 	'artikel categorieen';

// *** Other definitions
$name_title				=	'Naam';		//Naming title


// *** Messages
$msg_added				=	"$object toegevoegd.";
$msg_editted			=	"$object gegevens bijgewerkt.";
$msg_deleted			=	"$object verwijderd.";
$msg_deleted_m			=	"$plural verwijderd.";
$msg_delete_confirm		=	"Deze $object_lc zeker weten verwijderen?";
$msg_delete_confirm_m	=	"Deze $plural_lc zeker weten verwijderen?";
$msg_could_not_delete 	= 	"$object kon niet verwijderd worden. Er bestaan nog andere afhankelijke elementen in de database.";
$msg_not_all_deleted	=	$msg_deleted_m." Sommige $plural_lc konden niet verwijderd worden, omdat er nog andere afhankelijke elementen in de database bestonden.";
$msg_activated			=	"$object geactiveerd.";
$msg_activated_m		=	"$plural geactiveerd.";
$msg_deactivated		=	"$object op non-actief gezet.";
$msg_deactivated_m		=	"$plural op non-actief gezet.";


// *** Form and text elements
define('TXT_ADD_ITEM',					$object.' toevoegen');
define('TXT_EDIT_ITEM',					$object.' wijzigen');
define('TXT_VIEW_ITEM',					$object.' gegevens inzien');
define('TXT_DELETE_ITEM',				$object.' verwijderen');
define('TXT_ACTIVATE_ITEM',				$object.' activeren');
define('TXT_DEACTIVATE_ITEM',			$object.' de-activeren');
define('TXT_DELETE_ITEMS',				$plural.' verwijderen');
define('TXT_ACTIVATE_ITEMS',			$plural.' activeren');
define('TXT_DEACTIVATE_ITEMS',			$plural.' de-activeren');
define('TXT_MOVE_ITEM_UP',				$object.' omhoog verplaatsen');
define('TXT_MOVE_ITEM_DOWN',			$object.' omlaag verplaatsen');
define('TXT_NEW_ITEM',					'nieuw artikel');

define('TXT_BACK_TO_ARTICLES',			'terug naar de artikelen');

?>