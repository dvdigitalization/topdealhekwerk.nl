<?php
/*
Title:		Basic CMS language definitions (EN)
File: 		inc/lang/EN_cms.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Module names
define('MOD_NAME_SETTINGS',				'General settings');
define('MOD_NAME_AUTH_USERS',			'User management');
define('MOD_NAME_PAGE_CONTENTS',		'Text elements');
define('MOD_NAME_FAQ',					'Help');
define('MOD_NAME_MAILER',				'Email mailer');
define('MOD_NAME_AUTO_UPDATE',			'Auto update');
define('MOD_NAME_BAD_WORDS',			'SPAM words');
define('MOD_NAME_BLOCKED_IPS',			'Blocked IP\'s');
define('MOD_NAME_BACKUP',				'System backup');
define('MOD_NAME_PAGES',				'Website pages');
define('MOD_NAME_NEWS',					'News topics');
define('MOD_NAME_MENU',					'Website menu');
define('MOD_NAME_FILES',				'Website files');
define('MOD_NAME_POLLS',				'Polls');
define('MOD_NAME_PHOTO_ALBUMS',			'Photo albums');
define('MOD_NAME_PHOTO_ALBUM_CATS',		'Photo album categories');
define('MOD_NAME_GUESTBOOK',			'Guestbook');
define('MOD_NAME_NEWSLETTER_SUBSCR',	'Subscribers');
define('MOD_NAME_NEWSLETTERS',			'Newsletters');
define('MOD_NAME_CUSTOMER_QUOTES',		'Customer quotes');
define('MOD_NAME_AGENDA',				'Agenda');
define('MOD_NAME_CALENDAR',				'Calendar');
define('MOD_NAME_BANNERS',				'Banners');
define('MOD_NAME_CUSTOMERS_ADM',		'Customers');
define('MOD_NAME_PROJECTS',				'Projects');
define('MOD_NAME_EXTRA_PROJECTS',		'Extra projects');
define('MOD_NAME_INVOICES',				'Invoices');
define('MOD_NAME_INVOICE_LISTS',		'Lists for invoicing');
define('MOD_NAME_ARTICLES',				'Products');
define('MOD_NAME_ARTICLE_CATS',			'Product categories');
define('MOD_NAME_ARTICLE_UNITS',		'Product units');
define('MOD_NAME_HOURS',				'Hours');
define('MOD_NAME_HOURS_OVERVIEW',		'Hours overview');
define('MOD_NAME_HOURS_TOTALS',			'Hour totals');
define('MOD_NAME_QUOTES',				'Quotes');
define('MOD_NAME_QUOTE_TEXTS',			'Quote texts');
define('MOD_NAME_STATISTICS',			'Statistics');
define('MOD_NAME_STATS_PER_CUSTOMER',	'Statistics per customer');
define('MOD_NAME_TICKETS',				'Tickets');
define('MOD_NAME_TICKET_TYPES',			'Ticket types');
define('MOD_NAME_EXTRANET_USERS',		'Extranet users');
define('MOD_NAME_EXTRANET_USER_GROUPS',	'Extranet usergroups');
define('MOD_NAME_EXTRANET_FILES',		'Extranet files');
define('MOD_NAME_EXTRANET_FILE_CATS',	'Extranet file categories');
define('MOD_NAME_FORMS',				'Forms');
define('MOD_NAME_CUSTOMERS_SHOP',		'Webshop customers');
define('MOD_NAME_PRODUCT_CATEGORIES',	'Collections');
define('MOD_NAME_CATS_SIMPLE',			'Categories');
define('MOD_NAME_PRODUCTS',				'Products');
define('MOD_NAME_PRODUCT_PROPERTIES',	'Properties');
define('MOD_NAME_DELIVERY_COSTS',		'Delivery costs');
define('MOD_NAME_COUNTRIES',			'Countries');
define('MOD_NAME_ORDERS',				'Orders');
define('MOD_NAME_ORDER_STATUSSES',		'Order statusses');
define('MOD_NAME_PDF_INVOICE',			'PDF invoice');
define('MOD_NAME_AFFILIATES',			'Affiliates');
define('MOD_NAME_DISCOUNT_CODES',		'Kortingscodes');
define('MOD_NAME_SITE_STATS',			'Site statistics');
define('MOD_NAME_FILESHARING',			'Filesharing');
define('MOD_NAME_CF',					'Contact form');
define('MOD_NAME_TAF',					'Tell-a-friend');
define('MOD_NAME_SEARCH',				'Search form');
define('MOD_NAME_YOUTUBE',				'Youtube videos');
define('MOD_NAME_LEADS',				'Leads');
define('MOD_NAME_LEAD_CATEGORIES',		'Leads categories');
define('MOD_NAME_LEAD_ACTIONS',			'Leads actions');
define('MOD_NAME_GOOGLE_MAPS',			'Google maps');
define('MOD_NAME_GEOCODES',				'Geocodes');
define('MOD_NAME_GOOGLE_DIRECTIONS',	'Google directions');
define('MOD_NAME_GOOGLE_ROUTES',		'Google routes');
define('MOD_NAME_VACANCIES',			'Vacancies');
define('MOD_NAME_LINKS',				'Links');
define('MOD_NAME_SITE_PROJECTS',		'Site projects');
define('MOD_NAME_CONTACT_FORM_DATA',	'Contact form data');
define('MOD_NAME_SITE_FAQ',				'Site FAQ');
define('MOD_NAME_SITEMAP',				'Sitemap');
define('MOD_NAME_PAGE_CATS',			'Page categories');

// *** Logging
define('LOG_ADDED',						'added');
define('LOG_CREATED',					'created');
define('LOG_EDITTED',					'editted');
define('LOG_DELETED',					'deleted');
define('LOG_RESTORED',					'restored');
define('LOG_ACTIVATED',					'activated');
define('LOG_DEACTIVATED',				'de-activated');
define('LOG_ARCHIVED',					'archived');
define('LOG_UNARCHIVED',				'un-archived');
define('LOG_IP_BLOCKED',				'IP address blocked');
define('LOG_MULTIPLE',					'Multiple');

// *** CMS menu labels
define('MENU_LABEL_MANAGEMENT',			'Management');
define('MENU_LABEL_WEBSHOP',			'Webshop');
define('MENU_LABEL_PRODUCTS',			'Products');
define('MENU_LABEL_ADMIN',				'Administrative');
define('MENU_LABEL_WEBSITE',			'Website');
define('MENU_LABEL_EXTRANET',			'Extranet');
define('MENU_LABEL_MAILING',			'Mailing');
define('MENU_LABEL_TOOLS',				'Tools');

// *** Login related
define('LOGIN_REQUIRED_NOTICE',			'You have to login before you can use the CMS');
define('LOGIN_PROBLEMS_NOTICE',			'If you have forgotten your password, or if you have trouble logging in, please contact');
define('LOGIN_USERNAME',				'Username');
define('LOGIN_PASSWORD',				'Password');
define('LOGIN_BUTTON',					'Login');
define('LOGIN_REMEMBER_ME',				'Keep me logged in on this computer');
define('LOGIN_OK_MSG',					'You are logged in');
define('LOGIN_LAST_LOGIN',				'Last login was on');
define('LOGIN_INVALID_MSG',				'Invalid username and password combination.');
define('LOGIN_INVALID_BF_MSG',			'Invalid username and password combination.<br/>Please contact [TECHNICAL] if you forgot your password.');
define('LOGIN_IDLE_LOGOUT',				'You are logged out because you were inactive for more than [TIME] minutes.');
define('LOGIN_EMPTY_POST_FIELDS',		'You have to fill in both your username and password.');
define('LOGIN_NO_LONGER_AUTHORIZED',	'You are logged out because you are no longer authorized to use the CMS.');
define('LOGIN_NOT_ALLOWED',				'You are currently not authorized to login.');
define('LOGIN_ATTEMPTS_LEFT',			'You have [ATTEMPTS] attempt(s) left before your account will be blocked.');
define('LOGIN_MAX_ATTEMPTS',			'You have surpassed the maximally allowed wrong login attempts.<br/>Contact your system administrator or [TECHNICAL] to unblock your account.');

// *** Footer texts
define('TXT_PHP_VERSION',				'PHP version');
define('TXT_VERSION',					'Version');
define('TXT_REVISION',					'Revision');
define('TXT_SAFE_MODE_ON',				'safe_mode is on');
define('TXT_BENCHMARK',					'Page loaded in');
define('TXT_SECONDS',					'seconds');
define('TXT_DIGI_LINK_TITLE',			'Development and maintenance of internet applications and database systems');
define('TXT_DEVELOPED_BY',				'developed by');
define('TXT_COPYRIGHT',					'copyright');

// *** Data table items
define('TXT_WITH_MARKED_ITEMS',			'With marked items');
define('TXT_ALL',						'all');
define('TXT_NONE',						'none');
define('TXT_MARK_ALL',					'Mark all items');
define('TXT_MARK_NONE',					'Mark none');
define('TXT_SORT_BY',					'Sort by');
define('TXT_SEARCH',					'Search');
define('TXT_TOTAL',						'A total of');
define('TXT_NO_ITEMS',					'No');
define('TXT_OK',						'OK');
define('TXT_WARNING',					'Warning');
define('TXT_ERROR',						'Error');
define('TXT_CONFIRMATION',				'Confirmation');
define('BUTTON_CONFIRM',				'confirm');
define('BUTTON_CANCEL',					'cancel');

// *** Top icons texts
define('TXT_NEW_VERSION',				'New version available');
define('TXT_HELP_USING_THE_CMS',		'Help using the CMS');
define('TXT_EMAIL_TECH_SUPPORT',		'Email [TECH_NAME]\'s technical support');

// *** Resize constraints and text definitions
define('TXT_RESIZE_TO',					'Resize to');
define('TXT_MAX_SIZE',					'Max. dimension');
define('TXT_WIDTH',						'Width');
define('TXT_HEIGHT',					'Height');
define('TXT_PIXELS',					'pixels');
define('CONSTRAINT_NAME_NONE',			'Do not resize');
define('CONSTRAINT_NAME_SIZE',			'Longest side');
define('CONSTRAINT_NAME_WIDTH',			'Width');
define('CONSTRAINT_NAME_HEIGHT',		'Height');
define('CONSTRAINT_NAME_CROP',			'Fixed size');

// *** General texts
define('HOME',							'Home');
define('BUTTON_ADD',					'Add');
define('BUTTON_MAKE',					'Create');
define('BUTTON_CREATE',					'Create');
define('BUTTON_SAVE',					'Save');
define('BUTTON_CHANGE',					'Change');
define('BUTTON_ADJUST',					'Adjust');
define('TXT_OVERVIEW',					'overview');
define('TXT_FIRST_NAME',				'First name');
define('TXT_LAST_NAME',					'Last name');
define('TXT_NAME',						'Name');
define('TXT_SEX',						'Title');
define('TXT_TITLE',						'Title');
define('TXT_EMAIL_ADDRESS',				'Email address');
define('TXT_EMAIL',						'Email');
define('TXT_PHONE',						'Telefoon');
define('TXT_USERNAME',					'Username');
define('TXT_PASSWORD',					'Password');
define('TXT_NEW_PASS',					'New password');
define('TXT_REPEAT_PASS',				'Repeat password');
define('TXT_ACTIVE',					'Active');
define('TXT_ARCHIVED',					'Archive');
define('TXT_YES',						'yes');
define('TXT_NO',						'no');
define('TXT_DELETE',					'delete');
define('TXT_DELETE_VERB',				'delete');
define('TXT_DELETE_PERM',				'delete permanently');
define('TXT_RESTORE',					'restore');
define('TXT_ACTIVATE',					'activate');
define('TXT_DEACTIVATE',				'de-activate');
define('TXT_ARCHIVE',					'archive');
define('TXT_UNARCHIVE',					'unarchive');
define('TXT_NA',						'n.a.');
define('TXT_OPTIONS',					'Options');
define('TXT_LOGOUT',					'Logout');
define('TXT_DATE',						'Date');
define('TXT_SINCE',						'Since');
define('TXT_IMAGE',						'Image');
define('TXT_MAX',						'Max.');
define('TXT_MINUTES',					'minutes');
define('TXT_LINK',						'Link');
define('TXT_DESCRIPTION',				'Description');
define('TXT_UNNAMED',					'Unnamed');
define('TXT_FILE',						'File');
define('TXT_DOWNLOAD_FILE',				'Download file');
define('TXT_TYPE',						'Type');
define('TXT_CATEGORY',					'Category');

?>