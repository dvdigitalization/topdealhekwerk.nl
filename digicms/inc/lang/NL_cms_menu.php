<?php
/*
Title:		cms_menu.php language definitions (NL)
File: 		inc/lang/NL_cms_menu.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Menu item';
$plural					=	'Menu items';
$object_lc 				= 	'menu item';
$plural_lc				=	'menu items';

// *** Message definitions
$msg_added 				=	'Het menu item is toegevoegd.';
$msg_editted			=	'Het menu item is gewijzigd.';
$msg_deleted			=	'Het menu item is verwijderd.';
$msg_delete_confirm		=	'Dit menu item zeker weten verwijderen?';
$msg_section_added		=	'De menu sectie is toegevoegd.';
$msg_section_editted	=	'De menu sectie is gewijzigd.';
$msg_section_deleted	=	'De menu sectie is verwijderd.';
$msg_delete_sec_confirm	=	'Deze menu sectie en alle bijbehorende items zeker weten verwijderen?';
$msg_no_pages 			=	'Er zijn nog geen pagina\'s.';
$msg_no_sections		=	'Er zijn nog geen menu secties. Maak deze eerst aan.';
$msg_no_items			=	'Er zijn nog geen menu items. Maak deze eerst aan.';
$msg_non_linked			=	'Sommige menu elementen zijn nog niet (correct) gelinkt. Deze zijn in het rood aangegeven.';
$msg_no_item_selected	=	'U moet een geldig menu item kiezen.';

// *** Form and text elements
define('LOG_MENU_EDITTED',					'Website menu aangepast.');

define('TXT_ADD_MENU_ITEM',					'Menu [SUB]item toevoegen');
define('TXT_EDIT_MENU_ITEM',				'Menu [SUB]item bewerken');
define('TXT_DELETE_MENU_ITEM',				'Menu item verwijderen');
define('TXT_MOVE_MENU_ITEM_UP',				'Menu item omhoog verplaatsen');
define('TXT_MOVE_MENU_ITEM_DOWN',			'Menu item omlaag verplaatsen');
define('TXT_ADD_SECTION',					'Menu sectie toevoegen');
define('TXT_EDIT_SECTION',					'Menu sectie bewerken');
define('TXT_DELETE_SECTION',				'Menu sectie verwijderen');
define('TXT_MOVE_SECTION_UP',				'Menu sectie omhoog verplaatsen');
define('TXT_MOVE_SECTION_DOWN',				'Menu sectie omlaag verplaatsen');
define('TXT_NEW_SECTION',					'nieuwe menu sectie');
define('TXT_NEW_MENU_ITEM',					'nieuw menu item');
define('TXT_NEW_SUBMENU_ITEM',				'nieuw sub menu item');

define('TXT_LINK_TO_PAGE',					'Link naar pagina');
define('TXT_SECTION',						'Sectie');
define('TXT_BELOW',							'Onder');
define('TXT_OR_FILE',						'Of bestand');
define('TXT_OR_MANUAL',						'Of handmatig');
define('TXT_ADD_TO_ALL_LANGUAGES',			'Toevoegen aan alle talen');
define('TXT_NOT_LINKED',					'niet gelinkt');
define('TXT_NOT_YET_LINKED',				'nog niet gelinkt');
?>