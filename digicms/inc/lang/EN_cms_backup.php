<?php
/*
Title:		cms_backup.php language definitions (EN)
File: 		inc/lang/EN_cms_backup.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Restore point';
$object_lc 				= 	'restore point';
$plural					=	'Restore points';
$plural_lc				=	'restore points';

// *** Message definitions
$msg_deleted			=	'Restore point deleted.';
$msg_deleted_m			=	'Restore points deleted.';
$msg_backupped			=	'Restore point created.';
$msg_restored			=	'Restore point loaded.';
$msg_not_restored		=	'Restore point could not be loaded.';
$msg_not_backupped		=	'Restore point could not be created.';
$msg_delete_confirm		=	'Are you sure you want to delete this restore point?';
$msg_delete_confirm_m	=	'Are you sure you want to delete these restore points?';
$msg_file_not_exist		=	'The backup file could not be found.';
$msg_restore_confirm	=	'Are you sure you want to load the data from this restore point and overwrite all current data?
							<br/>The current data will be saved in a new restore point.';
$msg_mailed				=	'<br/>Backup file has been emailed to';

// *** Form and text elements
define('TXT_BACKUP_NOW',				'create a restore point now');
define('TXT_DOWNLOAD_BACKUP_FILE',		'Download backup file');
define('TXT_LOAD_RESTORE_POINT',		'Load this restore point');
define('TXT_DELETE_RESTORE_POINT',		'Delete this restore point');
define('TXT_ONE_RESTORE_POINT',			'1 restore point has been created');
define('TXT_MULTIPLE_RESTORE_POINTS',	'[AMOUNT] restore points have been created');
define('TXT_DELETE_RESTORE_POINTS',		'Delete restore points');
define('TXT_AUTO_BACKUP_EXPLANATION_1',	'Here you can find the restore points that have been created of the system. Maximally [AMOUNT] restore points will be saved at once, the oldest one will be removed automatically. You can change these settings at the');
define('TXT_AUTO_BACKUP_EXPLANATION_2',	'of the CMS');
define('TXT_AUTO_BACKUPS',				'Every [DAYS] days a restore point is created automatically by the system.');
define('TXT_NO_AUTO_BACKUPS',			'Currently, restore points are not being created automatically.');
define('TXT_DATE_AND_TIME_OF_BACKUP',	'Date and time of backup');
define('TXT_SIZE',						'Size');
define('TXT_STATUS',					'Status');

?>