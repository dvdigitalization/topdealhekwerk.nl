<?php
/*
Title:		cms_page_categories.php language definitions (EN)
File: 		inc/lang/EN_cms_page_categories.php
Version: 	v2.20
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Category';
$object_lc 				= 	'category';
$plural					=	'Categories';
$plural_lc				=	'categories';

// *** Message definitions
$msg_added 				=	'The category has been added.';
$msg_editted			=	'The category has been editted.';
$msg_deleted			=	'The category has been deleted.';
$msg_deleted_m			=	'The categories have been deleted.';
$msg_delete_confirm		=	'Are you sure you want to delete this category?';
$msg_delete_confirm_m	=	'Are you sure you want to delete these categories?';

// *** Form and text elements
define('TXT_ADD_ITEM',					'Add '.$object_lc);
define('TXT_EDIT_ITEM',					'Edit '.$object_lc);
define('TXT_VIEW_ITEM',					'View '.$object_lc.' details');
define('TXT_DELETE_ITEM',				'Delete '.$object_lc);
define('TXT_DELETE_ITEMS',				'Delete '.$plural_lc);
define('TXT_NEW_ITEM',					'new category');
define('TXT_BACK_TO_PAGES',				'back to pages');

?>