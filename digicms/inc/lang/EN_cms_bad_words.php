<?php
/*
Title:		cms_bad_words.php language definitions (EN)
File: 		inc/lang/EN_cms_bad_words.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'SPAM word';
$object_lc 				= 	'SPAM word';
$plural					=	'SPAM words';
$plural_lc				=	'SPAM words';

// *** Message definitions
$msg_added				=	'The word has been added to the SPAM list.';
$msg_editted			=	'The word has been editted.';
$msg_deleted			=	'The word has been deleted from the SPAM list.';
$msg_deleted_m			=	'The words have been deleted from the SPAM list.';
$msg_delete_confirm		=	'Are you sure you want to delete this word from the SPAM list?';
$msg_delete_confirm_m	=	'Are you sure you want to delete these words from the SPAM list?';
$msg_no_word			=	'You must fill in a word.';
$msg_word_exists		=	'This word is already on the SPAM list.';
$msg_blocking			=	'IP address will be blocked immediately with the usage of this word.';
$msg_blocking_m			=	'IP address will be blocked immediately with the usage of these words.';
$msg_not_blocking		=	'IP address will no longer be blocked immediately with the usage of this word.';
$msg_not_blocking_m		=	'IP address will no longer be blocked immediately with the usage of these words.';
$msg_substr				=	'The word will also be filtered when it is an internal string.';
$msg_no_substr			=	'The word will only be filtered as a standalone word.';

// *** Form and text elements
define('TXT_ADD_ITEM',					'Add word to the SPAM list');
define('TXT_EDIT_ITEM',					'Edit word');
define('TXT_DELETE_ITEM',				'Delete word');
define('TXT_BLOCK_ITEM',				'Block IP address immediately with the usage of this word');
define('TXT_UNBLOCK_ITEM',				'Do not block IP address immediately with the usage of this word');
define('TXT_FILTER_SEPARATE_ONLY',		'Filter only as a standalone word');
define('TXT_FILTER_STRING',				'Filter as an internal string as well');
define('TXT_DELETE_ITEMS',				'Delete words');
define('TXT_BLOCK_ITEMS',				'Block IP address immediately with the usage of these words');
define('TXT_UNBLOCK_ITEMS',				'Do not block IP address immediately with the usage of these words');
define('TXT_NEW_ITEM',					'add new word to the SPAM list');
define('TXT_WORD_SENTENCE',				'Word or sentence');
define('TXT_BLOCKED',					'Blocked');
define('TXT_IP_BLOCK',					'IP block');
define('TXT_SEPARATE',					'Standalone');
define('TXT_BLOCK',						'block ip');
define('TXT_UNBLOCK',					'do not block ip');

?>