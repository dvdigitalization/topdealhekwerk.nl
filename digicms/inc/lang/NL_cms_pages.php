<?php
/*
Title:		cms_pages.php language definitions (NL)
File: 		inc/lang/NL_cms_pages.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Pagina';
$object_lc 				= 	'pagina';
$plural					=	'Pagina\'s';
$plural_lc				=	'pagina\'s';

// *** Message definitions
$msg_added 				=	'De pagina is aangemaakt.';
$msg_editted			=	'De pagina is gewijzigd.';
$msg_deleted			=	'De pagina is verwijderd.';
$msg_deleted_m			=	'De pagina\'s zijn verwijderd.';
$msg_delete_confirm		=	'Deze pagina zeker weten verwijderen?';
$msg_delete_confirm_m	=	'Deze pagina\'s zeker weten verwijderen?';
$msg_deactivated		=	'De pagina is op non-actief gezet.';
$msg_deactivated_m		=	'De pagina\'s zijn op non-actief gezet.';
$msg_activated			=	'De pagina is geactiveerd.';
$msg_activated_m		=	'De pagina\'s zijn geactiveerd.';
$msg_locked				=	'De pagina kan nu niet meer gewijzigd worden.';
$msg_locked_m			=	'De pagina\'s kunnen nu niet meer gewijzigd worden.';
$msg_unlocked			=	'De pagina kan nu weer gewijzigd worden.';
$msg_unlocked_m			=	'De pagina\'s kunnen nu weer gewijzigd worden.';
$msg_page_is_locked		=	'Deze pagina is vergrendeld.';
$msg_some_pages_locked	=	'Sommige pagina\'s waren vergrendeld en zijn niet verwijderd.';
$msg_authed				=	'De pagina is beveiligd met inloggen.';
$msg_authed_m			=	'De pagina\'s zijn beveiligd met inloggen.';
$msg_unauthed			=	'De pagina is nu weer vrij toegankelijk.';
$msg_unauthed_m			=	'De pagina\'s zijn nu weer vrij toegankelijk.';
$msg_upload_error		=	'Er ging iets mis tijdens het uploaden van het bestand.';
$msg_resample_error		=	'Er ging iets mis tijdens het verwerken van de afbeelding.';
$msg_directory_error	=	'De doelmap [DIR] bestaat niet.';
$msg_urls_fixed			=	'De afbeelding URLs zijn gefixed.';

// *** Form and text elements
define('TXT_ADD_ITEM',					$object.' aanmaken');
define('TXT_EDIT_ITEM',					$object.' wijzigen');
define('TXT_VIEW_ITEM',					$object.' bekijken');
define('TXT_DELETE_ITEM',				$object.' verwijderen');
define('TXT_ACTIVATE_ITEM',				$object.' activeren');
define('TXT_DEACTIVATE_ITEM',			$object.' de-activeren');
define('TXT_ARCHIVE_ITEM',				$object.' archiveren');
define('TXT_UNARCHIVE_ITEM',			$object.' uit archief halen');
define('TXT_DELETE_ITEMS',				$plural.' verwijderen');
define('TXT_ACTIVATE_ITEMS',			$plural.' activeren');
define('TXT_DEACTIVATE_ITEMS',			$plural.' de-activeren');
define('TXT_ARCHIVE_ITEMS',				$plural.' archiveren');
define('TXT_UNARCHIVE_ITEMS',			$plural.' uit archief halen');
define('TXT_NEW_ITEM',					'nieuwe pagina');
define('TXT_CONTENTS',					'Inhoud');
define('TXT_PAGE_ADDRESS',				'Pagina adres');
define('TXT_AUTH',						'Auth.');
define('TXT_LOCKED',					'Vergr.');
define('TXT_HTML_ALIAS',				'HTML alias');
define('TXT_TEMPLATE',					'Template');
define('TXT_SECURED',					'Beveiligd');
define('TXT_GROUPS',					'Groepen');
define('TXT_META_DESCRIPTION',			'Meta description');
define('TXT_META_KEYWORDS',				'Meta keywords');
define('TXT_IMAGE_OR_FLASH',			'Afbeelding of flash bestand');
define('TXT_CURRENT_FILE',				'Huidig bestand');
define('TXT_ADD_TO_ALL_LANGUAGES',		'Toevoegen aan alle talen');
define('TXT_FIX_URLS',					'afbeelding urls fixen');
define('TXT_FOR_EXAMPLE',				'bijvoorbeeld');
define('TXT_FROM_URL',					'Van URL');
define('TXT_TO_URL',					'Naar URL');
define('BUTTON_FIX',					'Fix');
define('TXT_PAGE_CATS',					'pagina categorieen');
define('TXT_FILTER_ON_CATEGORY',		'Filter op categorie');
define('TXT_NO_CATEGORY',				'Geen categorie');

define('TXT_LOCK',						'vergrendelen');
define('TXT_UNLOCK',					'ontgrendelen');
define('TXT_LOCK_ITEM',					'Pagina vergrendelen');
define('TXT_UNLOCK_ITEM',				'Pagina ontgrendelen');
define('TXT_LOCK_ITEMS',				'Pagina\'s vergrendelen');
define('TXT_UNLOCK_ITEMS',				'Pagina\'s ontgrendelen');
define('TXT_PROTECT_ITEM',				'Pagina beveiligen met login');
define('TXT_UNPROTECT_ITEM',			'Pagina vrij toegankelijk maken');

define('LOG_LOCKED',					'vergrendeld');
define('LOG_UNLOCKED',					'ontgrendeld');
define('LOG_PROTECTED',					'beveiligd met login');
define('LOG_UNPROTECTED',				'vrij toegankelijk gemaakt');

define('EXPL_PAGE_TITLE',				'De titel van deze pagina');
define('EXPL_FILE_NAME',				'Verwijst deze pagina naar een fysiek bestand?');
define('EXPL_HTML_NAME',				'HTML alias voor deze pagina (bijvoorbeeld: mijn-pagina.html)');
define('EXPL_TEMPLATE',					'Welke template wordt er gebruikt voor deze pagina?');
define('EXPL_AUTH',						'Is deze pagina beveiligd met inloggen? Zo ja voor welke groepen?');
define('EXPL_CATEGORY',					'U kunt deze pagina optioneel aan een categorie toekennen.');

?>