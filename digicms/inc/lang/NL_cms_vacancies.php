<?php
/*
Title:		cms_template.php language definitions (NL)
File: 		inc/lang/NL_cms_template.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Vacature';
$object_lc 				= 	'vacature';
$plural					=	'Vacatures';
$plural_lc				=	'vacatures';

// *** Message definitions
$msg_added 				=	'De vacature is geplaatst.';
$msg_editted			=	'De vacature is gewijzigd.';
$msg_deleted			=	'De vacature is verwijderd.';
$msg_deleted_m			=	'De vacatures zijn verwijderd.';
$msg_delete_confirm		=	'Deze vacature zeker weten verwijderen?';
$msg_delete_confirm_m	=	'Deze vacatures zeker weten verwijderen?';
$msg_deactivated		=	'De vacature wordt niet langer op de website getoond.';
$msg_deactivated_m		=	'De vacatures worden niet langer op de website getoond.';
$msg_activated			=	'De vacature wordt vanaf nu op de website getoond.';
$msg_activated_m		=	'De vacatures worden vanaf nu op de website getoond.';

// *** Form and text elements
define('TXT_ADD_ITEM',					$object.' toevoegen');
define('TXT_EDIT_ITEM',					$object.' wijzigen');
define('TXT_VIEW_ITEM',					$object.' gegevens inzien');
define('TXT_DELETE_ITEM',				$object.' verwijderen');
define('TXT_ACTIVATE_ITEM',				$object.' activeren');
define('TXT_DEACTIVATE_ITEM',			$object.' de-activeren');
define('TXT_ARCHIVE_ITEM',				$object.' archiveren');
define('TXT_UNARCHIVE_ITEM',			$object.' uit archief halen');
define('TXT_ITEM_UP',					$object.' omhoog verplaatsen');
define('TXT_ITEM_DOWN',					$object.' omlaag verplaatsen');
define('TXT_DELETE_ITEMS',				$plural.' verwijderen');
define('TXT_ACTIVATE_ITEMS',			$plural.' activeren');
define('TXT_DEACTIVATE_ITEMS',			$plural.' de-activeren');
define('TXT_ARCHIVE_ITEMS',				$plural.' archiveren');
define('TXT_UNARCHIVE_ITEMS',			$plural.' uit archief halen');
define('TXT_NEW_ITEM',					'nieuwe vacature');
define('TXT_CONTENTS',					'Inhoud');

?>