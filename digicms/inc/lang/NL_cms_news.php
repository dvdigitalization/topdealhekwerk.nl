<?php
/*
Title:		cms_news.php language definitions (NL)
File: 		inc/lang/NL_cms_news.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Nieuwsbericht';
$object_lc 				= 	'nieuwsbericht';
$plural					=	'Nieuwsberichten';
$plural_lc				=	'nieuwsberichten';

// *** Message definitions
$msg_added 				=	'Het nieuwsbericht is geplaatst.';
$msg_editted			=	'Het nieuwsbericht is gewijzigd. Let op, als u de afbeelding heeft gewijzigd kan het zijn dat uw browser
							 de oude afbeelding nog weer geeft. Ververs in dat geval de pagina of leeg uw cache.';
$msg_deleted			=	'Het nieuwsbericht is verwijderd.';
$msg_deleted_m			=	'De nieuwsberichten zijn verwijderd.';
$msg_delete_confirm		=	'Dit nieuwsbericht zeker weten verwijderen?';
$msg_delete_confirm_m	=	'Deze nieuwsberichten zeker weten verwijderen?';
$msg_deactivated		=	'Het nieuwsbericht wordt niet langer op de website getoond.';
$msg_deactivated_m		=	'De nieuwsberichten worden niet langer op de website getoond.';
$msg_activated			=	'Het nieuwsbericht wordt vanaf nu op de website getoond.';
$msg_activated_m		=	'De nieuwsberichten worden vanaf nu op de website getoond.';
$msg_archived			=	'Het nieuwsbericht is gearchiveerd.';
$msg_archived_m			=	'De nieuwsberichten zijn gearchiveerd.';
$msg_unarchived			=	'Het nieuwsbericht is uit het archief gehaald.';
$msg_unarchived_m		=	'De nieuwsberichten zijn uit het archief gehaald.';
$msg_upload_error		=	'Er ging iets mis tijdens het uploaden van de afbeelding.';
$msg_thumb_error		=	'Er kon geen thumbnail van de afbeelding worden gemaakt.';
$msg_resize_error		=	'Er kon geen verkleinde versie van de afbeelding worden gemaakt.';
$msg_directory_error	=	'De doelmap [DIR] bestaat niet.';
$msg_rss_editted 		= 	'De RSS instellingen zijn gewijzigd.';
$msg_rss_write_failed	=	'Het schrijven van het RSS bestand is mislukt.';
$msg_comment_deleted	=	'De reactie is verwijderd.';
$msg_comm_del_block		=	'De reactie is verwijderd en het IP adres is geblokkeerd.';

// *** Form and text elements
define('TXT_ADD_ITEM',					$object.' toevoegen');
define('TXT_EDIT_ITEM',					$object.' wijzigen');
define('TXT_VIEW_ITEM',					$object.' bekijken op de website');
define('TXT_DELETE_ITEM',				$object.' verwijderen');
define('TXT_ACTIVATE_ITEM',				$object.' activeren');
define('TXT_DEACTIVATE_ITEM',			$object.' de-activeren');
define('TXT_ARCHIVE_ITEM',				$object.' archiveren');
define('TXT_UNARCHIVE_ITEM',			$object.' uit archief halen');
define('TXT_DELETE_ITEMS',				$plural.' verwijderen');
define('TXT_ACTIVATE_ITEMS',			$plural.' activeren');
define('TXT_DEACTIVATE_ITEMS',			$plural.' de-activeren');
define('TXT_ARCHIVE_ITEMS',				$plural.' archiveren');
define('TXT_UNARCHIVE_ITEMS',			$plural.' uit archief halen');
define('TXT_NEW_ITEM',					'nieuw nieuwsbericht');
define('TXT_CONTENTS',					'Bericht');
define('TXT_PREVIEW',					'Preview');
define('TXT_VIEW_COMMENTS',				'Reacties bekijken');
define('TXT_COMMENTS_FOR_THIS_TOPIC',	'Reacties op dit nieuwsbericht');
define('TXT_MAIL_THIS_PERSON',			'Stuur deze persoon een email');
define('TXT_DELETE_COMMENT',			'Verwijder deze reactie');
define('TXT_DELETE_COMMENT_AND_BLOCK',	'Verwijder deze reactie en blokkeer dit IP adres');
define('TXT_TIMES_VIEWED',				'Bekeken');

define('TXT_AUTHOR',					'Auteur');
define('TXT_RSS_DESCRIPTION',			'RSS beschrijving');
define('TXT_CHANGE_RSS_SETTINGS',		'RSS instellingen wijzigen');
define('TXT_RSS_SETTINGS',				'RSS instellingen');
define('TXT_RSS_FEED',					'RSS feed');
define('LOG_RSS_UPDATED',				'RSS instellingen gewijzigd');
define('LOG_COMMENT_DELETED',			'Reactie verwijderd');
?>