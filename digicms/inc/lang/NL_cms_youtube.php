<?php
/*
Title:		cms_youtube.php language definitions (NL)
File: 		inc/lang/NL_cms_youtube.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Youtube video';
$object_lc 				= 	'Youtube video';
$plural					=	'Youtube video\'s';
$plural_lc 				= 	'Youtube video\'s';

// *** Message definitions
$msg_added				=	"$object toegevoegd.";
$msg_editted			=	"$object gegevens bijgewerkt.";
$msg_deleted			=	"$object verwijderd.";
$msg_deleted_m			=	"$plural verwijderd.";
$msg_delete_confirm		=	"Deze $object_lc zeker weten verwijderen?";
$msg_delete_confirm_m	=	"Deze $plural_lc zeker weten verwijderen?";
$msg_activated			=	"$object geactiveerd.";
$msg_activated_m		=	"$plural geactiveerd.";
$msg_deactivated		=	"$object op non-actief gezet.";
$msg_deactivated_m		=	"$plural op non-actief gezet.";

// *** Form and text elements
define('TXT_ADD_ITEM',						$object.' toevoegen');
define('TXT_EDIT_ITEM',						$object.' gegevens bijwerken');
define('TXT_VIEW_ITEM',						$object.' gegevens');
define('TXT_NEW_VIDEO',						'nieuwe video');
define('TXT_VIEW_ITEM_OPTION',				$object.' gegevens inzien');
define('TXT_VIEW_VIDEO',					'Video bekijken'); 
define('TXT_DEACTIVATE_ITEM_OPTION',		$object.' de-activeren');
define('TXT_ACTIVATE_ITEM_OPTION',			$object.' activeren');
define('TXT_MOVE_ITEM_UP',					$object.' omhoog verplaatsen');
define('TXT_MOVE_ITEM_DOWN',				$object.' omlaag verplaatsen');
define('TXT_EDIT_ITEM_OPTION',				$object.' gegevens bijwerken');
define('TXT_DELETE_ITEM_OPTION',			$object.' verwijderen');

define('TXT_YOUTUBE_LINK',					'Youtube link');
define('TXT_YOUTUBE_VIDEO_ID',				'Youtube video ID');
define('TXT_THUMBNAIL',						'Thumbnail');
define('TXT_HTML_CODE',						'HTML code');
?>