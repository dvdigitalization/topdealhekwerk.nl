<?php
/*
Title:		cms_links.php language definitions (NL)
File: 		inc/lang/NL_cms_links.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Link';
$object_lc 				= 	'link';
$plural					=	'Links';
$plural_lc				=	'links';

// *** Message definitions
$msg_added 				=	'De link is geplaatst.';
$msg_editted			=	'De link is gewijzigd.';
$msg_deleted			=	'De link is verwijderd.';
$msg_deleted_m			=	'De links zijn verwijderd.';
$msg_delete_confirm		=	'Deze link zeker weten verwijderen?';
$msg_delete_confirm_m	=	'Deze links zeker weten verwijderen?';
$msg_deactivated		=	'De link wordt niet langer op de website getoond.';
$msg_deactivated_m		=	'De links worden niet langer op de website getoond.';
$msg_activated			=	'De link wordt vanaf nu op de website getoond.';
$msg_activated_m		=	'De links worden vanaf nu op de website getoond.';

// *** Form and text elements
define('TXT_ADD_ITEM',					$object.' toevoegen');
define('TXT_EDIT_ITEM',					$object.' wijzigen');
define('TXT_VIEW_ITEM',					$object.' gegevens inzien');
define('TXT_DELETE_ITEM',				$object.' verwijderen');
define('TXT_ACTIVATE_ITEM',				$object.' activeren');
define('TXT_DEACTIVATE_ITEM',			$object.' de-activeren');
define('TXT_ITEM_UP',					$object.' omhoog verplaatsen');
define('TXT_ITEM_DOWN',					$object.' omlaag verplaatsen');
define('TXT_DELETE_ITEMS',				$plural.' verwijderen');
define('TXT_ACTIVATE_ITEMS',			$plural.' activeren');
define('TXT_DEACTIVATE_ITEMS',			$plural.' de-activeren');
define('TXT_NEW_ITEM',					'nieuwe link');
define('TXT_CONTENTS',					'Inhoud');
define('TXT_URL',						'URL');

?>