<?php
/*
Title:		cms_projects.php language definitions (EN)
File: 		inc/lang/EN_cms_projects.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Project';
$object_lc 				= 	'project';
$plural					=	'Projects';
$plural_lc				=	'projects';

// *** Form and text elements
define('TXT_DEADLINE_ALERT_MAIL',		"Dear [NAME],<br/><br/>The deadline for the following projects will expire within [DAYS] days:<br/><br/>[PROJECTS]");
define('TXT_DEADLINE_MAIL_SUBJECT',		'Projects deadline warning');

?>