<?php
/*
Title:		cms_pages.php language definitions (EN)
File: 		inc/lang/EN_cms_pages.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Page';
$object_lc 				= 	'page';
$plural					=	'Pages';
$plural_lc				=	'pages';

// *** Message definitions
$msg_added 				=	'The page has been created.';
$msg_editted			=	'The page has been editted.';
$msg_deleted			=	'The page has been deleted.';
$msg_deleted_m			=	'The pages have been deleted.';
$msg_delete_confirm		=	'Are you sure you want to delete this page?';
$msg_delete_confirm_m	=	'Are you sure you want to delete these pages?';
$msg_deactivated		=	'The page has been deactivated.';
$msg_deactivated_m		=	'The pages have been deactivated.';
$msg_activated			=	'The page has been activated.';
$msg_activated_m		=	'The pages have been activated.';
$msg_locked				=	'The page cannot be changed anymore from now on.';
$msg_locked_m			=	'The pages cannot be changed anymore from now on.';
$msg_unlocked			=	'The page can be changed again from now on.';
$msg_unlocked_m			=	'The pages can be changed again from now on.';
$msg_page_is_locked		=	'This page is locked.';
$msg_some_pages_locked	=	'Some pages were locked and have not been deleted.';
$msg_authed				=	'The page has been secured with a login.';
$msg_authed_m			=	'The pages have been secured with a login.';
$msg_unauthed			=	'The page has been made freely accessible again.';
$msg_unauthed_m			=	'The pages have been made freely accessible again.';
$msg_upload_error		=	'Something went wrong while uploading the image.';
$msg_resample_error		=	'Something went wrong while processing the image.';
$msg_directory_error	=	'The target directory [DIR] does not exist.';
$msg_urls_fixed			=	'The image URLs have been fixed.';

// *** Form and text elements
define('TXT_ADD_ITEM',					'Create '.$object_lc);
define('TXT_EDIT_ITEM',					'Edit '.$object_lc);
define('TXT_VIEW_ITEM',					'View '.$object_lc.' details');
define('TXT_DELETE_ITEM',				'Delete '.$object_lc);
define('TXT_ACTIVATE_ITEM',				'Activate '.$object_lc);
define('TXT_DEACTIVATE_ITEM',			'De-activate '.$object_lc);
define('TXT_ARCHIVE_ITEM',				'Archive '.$object_lc);
define('TXT_UNARCHIVE_ITEM',			'Unarchive '.$object_lc);
define('TXT_DELETE_ITEMS',				'Delete '.$plural_lc);
define('TXT_ACTIVATE_ITEMS',			'Activate '.$plural_lc);
define('TXT_DEACTIVATE_ITEMS',			'De-activate '.$plural_lc);
define('TXT_ARCHIVE_ITEMS',				'Archive '.$plural_lc);
define('TXT_UNARCHIVE_ITEMS',			'Unarchive '.$plural_lc);
define('TXT_NEW_ITEM',					'new page');
define('TXT_CONTENTS',					'Contents');
define('TXT_PAGE_ADDRESS',				'Page address');
define('TXT_AUTH',						'Auth.');
define('TXT_LOCKED',					'Lock');
define('TXT_HTML_ALIAS',				'HTML alias');
define('TXT_TEMPLATE',					'Template');
define('TXT_SECURED',					'Secured');
define('TXT_GROUPS',					'Groups');
define('TXT_META_DESCRIPTION',			'Meta description');
define('TXT_META_KEYWORDS',				'Meta keywords');
define('TXT_IMAGE_OR_FLASH',			'Image or flash file');
define('TXT_CURRENT_FILE',				'Current file');
define('TXT_ADD_TO_ALL_LANGUAGES',		'Add to all languages');
define('TXT_FIX_URLS',					'fix image urls');
define('TXT_FOR_EXAMPLE',				'for example');
define('TXT_FROM_URL',					'From URL');
define('TXT_TO_URL',					'To URL');
define('BUTTON_FIX',					'Fix');
define('TXT_PAGE_CATS',					'page categories');
define('TXT_FILTER_ON_CATEGORY',		'Filter on category');
define('TXT_NO_CATEGORY',				'No category');

define('TXT_LOCK',						'lock');
define('TXT_UNLOCK',					'unlock');
define('TXT_LOCK_ITEM',					'Lock page');
define('TXT_UNLOCK_ITEM',				'Unlock page');
define('TXT_LOCK_ITEMS',				'Lock pages');
define('TXT_UNLOCK_ITEMS',				'Unlock pages');
define('TXT_PROTECT_ITEM',				'Protect page with a login');
define('TXT_UNPROTECT_ITEM',			'Make page freely accessible');

define('LOG_LOCKED',					'locked');
define('LOG_UNLOCKED',					'unlocked');
define('LOG_PROTECTED',					'protected with login');
define('LOG_UNPROTECTED',				'made freely accessible');

define('EXPL_PAGE_TITLE',				'The title of this page');
define('EXPL_FILE_NAME',				'Does this page refer to a physical file?');
define('EXPL_HTML_NAME',				'HTML alias for this page (for example: my-page.html)');
define('EXPL_TEMPLATE',					'What template does this page use?');
define('EXPL_AUTH',						'Is this page secured with a login? If so, for what groups?');
define('EXPL_CATEGORY',					'You can categorize this page if you want.');

?>