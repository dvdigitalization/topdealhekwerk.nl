<?php
/*
Title:		Basic CMS language definitions (NL)
File: 		inc/lang/NL_cms.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Module names
define('MOD_NAME_SETTINGS',				'Algemene opties');
define('MOD_NAME_AUTH_USERS',			'Gebruikers beheer');
define('MOD_NAME_PAGE_CONTENTS',		'Tekst elementen');
define('MOD_NAME_FAQ',					'Help');
define('MOD_NAME_MAILER',				'Email mailer');
define('MOD_NAME_AUTO_UPDATE',			'Auto update');
define('MOD_NAME_BAD_WORDS',			'SPAM woorden');
define('MOD_NAME_BLOCKED_IPS',			'Geblokkeerde ip\'s');
define('MOD_NAME_BACKUP',				'Systeem backup');
define('MOD_NAME_PAGES',				'Website pagina\'s');
define('MOD_NAME_NEWS',					'Nieuws berichten');
define('MOD_NAME_MENU',					'Website menu');
define('MOD_NAME_FILES',				'Website bestanden');
define('MOD_NAME_POLLS',				'Polls');
define('MOD_NAME_PHOTO_ALBUMS',			'Foto album\'s');
define('MOD_NAME_PHOTO_ALBUM_CATS',		'Foto album categorieën');
define('MOD_NAME_GUESTBOOK',			'Gastenboek');
define('MOD_NAME_NEWSLETTER_SUBSCR',	'Aanmeldingen');
define('MOD_NAME_NEWSLETTERS',			'Nieuwsbrieven');
define('MOD_NAME_CUSTOMER_QUOTES',		'Klanten quotes');
define('MOD_NAME_AGENDA',				'Agenda');
define('MOD_NAME_CALENDAR',				'Kalender');
define('MOD_NAME_BANNERS',				'Banners');
define('MOD_NAME_CUSTOMERS_ADM',		'Klanten');
define('MOD_NAME_PROJECTS',				'Projecten');
define('MOD_NAME_EXTRA_PROJECTS',		'Extra projecten');
define('MOD_NAME_INVOICES',				'Facturen');
define('MOD_NAME_INVOICE_LISTS',		'Lijsten voor facturatie');
define('MOD_NAME_ARTICLES',				'Artikelen');
define('MOD_NAME_ARTICLE_CATS',			'Artikel categorieën');
define('MOD_NAME_ARTICLE_UNITS',		'Artikel eenheden');
define('MOD_NAME_HOURS',				'Uren');
define('MOD_NAME_HOURS_OVERVIEW',		'Uren overzicht');
define('MOD_NAME_HOURS_TOTALS',			'Uren totalen');
define('MOD_NAME_QUOTES',				'Offertes');
define('MOD_NAME_QUOTE_TEXTS',			'Offerte teksten');
define('MOD_NAME_STATISTICS',			'Statistieken');
define('MOD_NAME_STATS_PER_CUSTOMER',	'Statistieken per klant');
define('MOD_NAME_TICKETS',				'Memo\'s');
define('MOD_NAME_TICKET_TYPES',			'Memo types');
define('MOD_NAME_EXTRANET_USERS',		'Extranet gebruikers');
define('MOD_NAME_EXTRANET_USER_GROUPS',	'Extranet gebruikersgroepen');
define('MOD_NAME_EXTRANET_FILES',		'Extranet bestanden');
define('MOD_NAME_EXTRANET_FILE_CATS',	'Extranet bestands categorieën');
define('MOD_NAME_FORMS',				'Formulieren');
define('MOD_NAME_CUSTOMERS_SHOP',		'Webshop klanten');
define('MOD_NAME_PRODUCT_CATEGORIES',	'Collecties');
define('MOD_NAME_CATS_SIMPLE',			'Categorieen');
define('MOD_NAME_PRODUCTS',				'Producten');
define('MOD_NAME_PRODUCT_PROPERTIES',	'Eigenschappen');
define('MOD_NAME_DELIVERY_COSTS',		'Verzendkosten');
define('MOD_NAME_COUNTRIES',			'Landen');
define('MOD_NAME_ORDERS',				'Bestellingen');
define('MOD_NAME_ORDER_STATUSSES',		'Order statussen');
define('MOD_NAME_PDF_INVOICE',			'PDF factuur');
define('MOD_NAME_AFFILIATES',			'Affiliates');
define('MOD_NAME_DISCOUNT_CODES',		'Kortingscodes');
define('MOD_NAME_SITE_STATS',			'Site statistieken');
define('MOD_NAME_FILESHARING',			'Bestanden delen');
define('MOD_NAME_CF',					'Contact formulier');
define('MOD_NAME_TAF',					'Tell-a-friend');
define('MOD_NAME_SEARCH',				'Zoek formulier');
define('MOD_NAME_YOUTUBE',				'Youtube video\'s');
define('MOD_NAME_LEADS',				'Leads');
define('MOD_NAME_LEAD_CATEGORIES',		'Leads categorieen');
define('MOD_NAME_LEAD_ACTIONS',			'Leads acties');
define('MOD_NAME_GOOGLE_MAPS',			'Google maps');
define('MOD_NAME_GEOCODES',				'Geocodes');
define('MOD_NAME_GOOGLE_DIRECTIONS',	'Google routeplanner');
define('MOD_NAME_GOOGLE_ROUTES',		'Google routes');
define('MOD_NAME_VACANCIES',			'Vacatures');
define('MOD_NAME_LINKS',				'Links');
define('MOD_NAME_SITE_PROJECTS',		'Site projecten');
define('MOD_NAME_CONTACT_FORM_DATA',	'Contactformulier gegevens');
define('MOD_NAME_SITE_FAQ',				'Site FAQ');
define('MOD_NAME_SITEMAP',				'Sitemap');
define('MOD_NAME_PAGE_CATS',			'Pagina categorieen');

// *** Logging
define('LOG_ADDED',						'toegevoegd');
define('LOG_CREATED',					'aangemaakt');
define('LOG_EDITTED',					'gewijzigd');
define('LOG_DELETED',					'verwijderd');
define('LOG_RESTORED',					'teruggezet');
define('LOG_ACTIVATED',					'geactiveerd');
define('LOG_DEACTIVATED',				'ge-deactiveerd');
define('LOG_ARCHIVED',					'gearchiveerd');
define('LOG_UNARCHIVED',				'uit het archief gehaald');
define('LOG_IP_BLOCKED',				'IP adres geblokkeerd');
define('LOG_MULTIPLE',					'Meerdere');

// *** CMS menu labels
define('MENU_LABEL_MANAGEMENT',			'Beheer');
define('MENU_LABEL_WEBSHOP',			'Webshop');
define('MENU_LABEL_PRODUCTS',			'Product beheer');
define('MENU_LABEL_ADMIN',				'Administratie');
define('MENU_LABEL_WEBSITE',			'Website');
define('MENU_LABEL_EXTRANET',			'Extranet');
define('MENU_LABEL_MAILING',			'Mailing');
define('MENU_LABEL_TOOLS',				'Extra');

// *** Login related
define('LOGIN_REQUIRED_NOTICE',			'U moet inloggen voordat u het CMS kunt gebruiken');
define('LOGIN_PROBLEMS_NOTICE',			'Als u uw wachtwoord bent vergeten of problemen heeft met inloggen kunt u contact opnemen met');
define('LOGIN_USERNAME',				'Gebruikersnaam');
define('LOGIN_PASSWORD',				'Wachtwoord');
define('LOGIN_BUTTON',					'Inloggen');
define('LOGIN_REMEMBER_ME',				'Ingelogd blijven op deze computer');
define('LOGIN_OK_MSG',					'U bent ingelogd');
define('LOGIN_LAST_LOGIN',				'Laatste login was op');
define('LOGIN_INVALID_MSG',				'Ongeldige gebruikersnaam en wachtwoord combinatie.');
define('LOGIN_INVALID_BF_MSG',			'Ongeldige gebruikersnaam en wachtwoord combinatie.<br/>Neem contact op met [TECHNICAL] als u uw wachtwoord vergeten bent.');
define('LOGIN_IDLE_LOGOUT',				'U bent uitgelogd uit het systeem omdat u langer dan [TIME] minuten niet actief was.');
define('LOGIN_EMPTY_POST_FIELDS',		'U moet uw gebruikersnaam en wachtwoord opgeven.');
define('LOGIN_NO_LONGER_AUTHORIZED',	'U bent uitgelogd uit het systeem omdat u niet langer geauthoriseerd bent.');
define('LOGIN_NOT_ALLOWED',				'U bent momenteel niet geauthoriseerd om in te loggen.');
define('LOGIN_ATTEMPTS_LEFT',			'U heeft nog [ATTEMPTS] poging(en) over voordat uw account geblokkeerd wordt.');
define('LOGIN_MAX_ATTEMPTS',			'U heeft het maximaal aantal foute inlog mogelijkheden overschreden.<br/>Neem contact op met uw systeem beheerder of [TECHNICAL] om uw account te deblokkeren.');

// *** Footer texts
define('TXT_PHP_VERSION',				'PHP versie');
define('TXT_VERSION',					'Versie');
define('TXT_REVISION',					'Revisie');
define('TXT_SAFE_MODE_ON',				'safe_mode staat aan');
define('TXT_BENCHMARK',					'Pagina geladen in');
define('TXT_SECONDS',					'seconden');
define('TXT_DIGI_LINK_TITLE',			'Ontwerp en onderhoud van internet applicaties en database systemen');
define('TXT_DEVELOPED_BY',				'ontwikkeld door');
define('TXT_COPYRIGHT',					'copyright');

// *** Data table and Message items
define('TXT_WITH_MARKED_ITEMS',			'Met gemarkeerde items');
define('TXT_ALL',						'alle');
define('TXT_NONE',						'geen');
define('TXT_MARK_ALL',					'Markeer alle items');
define('TXT_MARK_NONE',					'Markeer geen items');
define('TXT_SORT_BY',					'Sorteren op');
define('TXT_SEARCH',					'Zoek');
define('TXT_TOTAL',						'Totaal');
define('TXT_NO_ITEMS',					'Geen');
define('TXT_OK',						'OK');
define('TXT_WARNING',					'Waarschuwing');
define('TXT_ERROR',						'Fout');
define('TXT_CONFIRMATION',				'Bevestiging');
define('BUTTON_CONFIRM',				'bevestigen');
define('BUTTON_CANCEL',					'annuleren');

// *** Top icons texts
define('TXT_NEW_VERSION',				'Nieuwe versie beschikbaar');
define('TXT_HELP_USING_THE_CMS',		'Hulp bij het gebruik van het CMS');
define('TXT_EMAIL_TECH_SUPPORT',		'Email de technische dienst van [TECH_NAME]');

// *** Resize constraints and text definitions
define('TXT_RESIZE_TO',					'Verklein op');
define('TXT_MAX_SIZE',					'Max. afmeting');
define('TXT_WIDTH',						'Breedte');
define('TXT_HEIGHT',					'Hoogte');
define('TXT_PIXELS',					'pixels');
define('CONSTRAINT_NAME_NONE',			'Niet verkleinen');
define('CONSTRAINT_NAME_SIZE',			'Langste zijde');
define('CONSTRAINT_NAME_WIDTH',			'Breedte');
define('CONSTRAINT_NAME_HEIGHT',		'Hoogte');
define('CONSTRAINT_NAME_CROP',			'Vaste afmeting');

// *** General texts
define('HOME',							'Home');
define('BUTTON_ADD',					'Toevoegen');
define('BUTTON_MAKE',					'Aanmaken');
define('BUTTON_CREATE',					'Aanmaken');
define('BUTTON_SAVE',					'Opslaan');
define('BUTTON_CHANGE',					'Wijzigen');
define('BUTTON_ADJUST',					'Bijwerken');
define('TXT_OVERVIEW',					'overzicht');
define('TXT_FIRST_NAME',				'Voornaam');
define('TXT_LAST_NAME',					'Achternaam');
define('TXT_NAME',						'Naam');
define('TXT_SEX',						'Aanhef');
define('TXT_TITLE',						'Titel');
define('TXT_EMAIL_ADDRESS',				'Email adres');
define('TXT_EMAIL',						'Email');
define('TXT_PHONE',						'Telefoon');
define('TXT_USERNAME',					'Gebruikersnaam');
define('TXT_PASSWORD',					'Wachtwoord');
define('TXT_NEW_PASS',					'Nieuw wachtwoord');
define('TXT_REPEAT_PASS',				'Herhaal wachtwoord');
define('TXT_ACTIVE',					'Actief');
define('TXT_ARCHIVED',					'Archief');
define('TXT_YES',						'ja');
define('TXT_NO',						'nee');
define('TXT_DELETE',					'verwijder');
define('TXT_DELETE_VERB',				'verwijderen');
define('TXT_DELETE_PERM',				'verwijder permanent');
define('TXT_RESTORE',					'zet terug');
define('TXT_ACTIVATE',					'activeer');
define('TXT_DEACTIVATE',				'de-activeer');
define('TXT_ARCHIVE',					'archiveer');
define('TXT_UNARCHIVE',					'uit archief');
define('TXT_NA',						'n.v.t.');
define('TXT_OPTIONS',					'Opties');
define('TXT_LOGOUT',					'Uitloggen');
define('TXT_DATE',						'Datum');
define('TXT_SINCE',						'Sinds');
define('TXT_IMAGE',						'Afbeelding');
define('TXT_MAX',						'Max.');
define('TXT_MINUTES',					'minuten');
define('TXT_LINK',						'Link');
define('TXT_DESCRIPTION',				'Omschrijving');
define('TXT_UNNAMED',					'Naamloos');
define('TXT_FILE',						'Bestand');
define('TXT_DOWNLOAD_FILE',				'Download bestand');
define('TXT_TYPE',						'Type');
define('TXT_CATEGORY',					'Categorie');

?>