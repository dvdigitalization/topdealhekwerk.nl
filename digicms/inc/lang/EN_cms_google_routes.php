<?php
/*
Title:		cms_template.php language definitions (EN)
File: 		inc/lang/EN_cms_template.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Object definition
$object					=	'Route';
$object_lc 				= 	'route';
$plural					=	'Routes';
$plural_lc				=	'routes';

// *** Message definitions
$msg_added 				=	'The route has been added.';
$msg_editted			=	'The route has been editted.';
$msg_deleted			=	'The route has been deleted.';
$msg_deleted_m			=	'The routes have been deleted.';
$msg_delete_confirm		=	'Are you sure you want to delete this route?';
$msg_delete_confirm_m	=	'Are you sure you want to delete these routes?';
$msg_deactivated		=	'The route will no longer be displayed on the website.';
$msg_deactivated_m		=	'The routes will no longer be displayed on the website.';
$msg_activated			=	'The route will be displayed on the website.';
$msg_activated_m		=	'The routes will be displayed on the website.';
$msg_wp_added 			=	'The location has been added to the route.';
$msg_wp_deleted 		=	'The location has been removed from the route.';
$msg_wp_delete_confirm	=	'Are you sure you want to remove this location from the route?';

// *** Form and text elements
define('TXT_ADD_ITEM',					'Add '.$object_lc);
define('TXT_EDIT_ITEM',					'Edit '.$object_lc);
define('TXT_VIEW_ITEM',					'View '.$object_lc.' details');
define('TXT_DELETE_ITEM',				'Delete '.$object_lc);
define('TXT_ACTIVATE_ITEM',				'Activate '.$object_lc);
define('TXT_DEACTIVATE_ITEM',			'De-activate '.$object_lc);
define('TXT_ARCHIVE_ITEM',				'Archive '.$object_lc);
define('TXT_UNARCHIVE_ITEM',			'Unarchive '.$object_lc);
define('TXT_ITEM_UP',					'Move location up');
define('TXT_ITEM_DOWN',					'Move location down');
define('TXT_DELETE_ITEMS',				'Delete '.$plural_lc);
define('TXT_ACTIVATE_ITEMS',			'Activate '.$plural_lc);
define('TXT_DEACTIVATE_ITEMS',			'De-activate '.$plural_lc);
define('TXT_ARCHIVE_ITEMS',				'Archive '.$plural_lc);
define('TXT_UNARCHIVE_ITEMS',			'Unarchive '.$plural_lc);
define('TXT_NEW_ITEM',					'new route');
define('TXT_CONTENTS',					'Contents');
define('TXT_MAP_TYPE',					'Map type');
define('TXT_MAP_TYPE_NORMAL',			'Normal map');
define('TXT_MAP_TYPE_SAT',				'Satellite map');
define('TXT_MAP_TYPE_HYBRID',			'Hybrid map');
define('TXT_LOCATION',					'Location');
define('TXT_LOCATIONS',					'Locations');
define('TXT_DEL_WP',					'Delete location');
?>