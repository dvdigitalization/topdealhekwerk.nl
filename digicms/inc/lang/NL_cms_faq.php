<?php
/*
Title:		cms_faq.php language definitions (NL)
File: 		inc/lang/NL_cms_faq.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Message definitions
$msg_cat_added			=	'Categorie aangemaakt.';
$msg_cat_editted		=	'Categorie bijgewerkt.';
$msg_cat_deleted		=	'Categorie verwijderd.';
$msg_added				=	'Help onderwerp toegevoegd.';
$msg_editted			=	'Help onderwerp bijgewerkt.';
$msg_deleted			=	'Help onderwerp verwijderd.';

// *** Form and text elements
define('TXT_NO_TOPICS',					'Geen onderwerpen in deze categorie');
define('TXT_ADD_CATEGORY',				'Categorie toevoegen');
define('TXT_EDIT_CATEGORY',				'Categorie bijwerken');
define('TXT_SUBCAT',					'Sub-categorie van');
define('TXT_ADD_TOPIC',					'Help onderwerp toevoegen');
define('TXT_EDIT_TOPIC',				'Help onderwerp bijwerken');
define('TXT_NEW_CAT',					'nieuwe categorie');
define('TXT_NEW_TOPIC',					'nieuw help onderwerp');
define('TXT_FAQ_NOTE',					'Als er onduidelijkheden zijn of u nog verdere vragen heeft kunt u contact opnemen met [TECH_NAME] via [TECH_EMAIL]');
define('TXT_BROWSE',					'Kies hiernaast een help onderwerp om weer te geven. Om te navigeren tussen de help onderwerpen en categorieen klikt u op de relevante namen.');

?>