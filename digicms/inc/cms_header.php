<?
/*
Title:		CMS header construct
File: 		inc/cms_header.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** All settings and start up procedures are located in the ajax header
require_once('inc/cms_ajax_header.php');

// *** First time password change required for admin account
if (!$cfg['FIRST_LOGIN_PASS_CHANGED'] and $s_id == 2) {
	$Message->set(DIGI_ERR_PASS_CHANGE_REQUIRED, WARNING);
}

// *** Timer
if (isset($cfg['TIMER_ENABLED']) and $cfg['TIMER_ENABLED'] and file_exists(CMS_PATH_PREFIX.'classes/class_Timer.php') and $s_id and !$s_admin) {
	require_once(CMS_PATH_PREFIX.'classes/class_Timer.php');
	$Timer 			= 	new Timer();
	$timer_js		=	$Timer->makeTimerJS();
	$timer_controls =	$img['clock']." <div id='running_time'>0h 00m 00s</div>";
	$timer_controls	.=	$Timer->makeTimerControls();
}
else {
	$timer_js = $timer_controls = '';
}

// *** Load menu definition (backwards compatible)
$menu_old_style = false;
if (file_exists('inc/config/cfg_menu.php')) {
	require_once('inc/config/cfg_menu.php');
	$menu_old_style =  true;
}
elseif (file_exists('inc/definitions/def_menu.php')) {
	require_once('inc/definitions/def_menu.php');
	$menu_old_style =  true;
}

// *** Make the menu HTML
$menu_sections 	= $menu_options = array();
$menu_counter 	= 0;
$menu_html 		= '';

foreach($menu as $section_name => $section_modules) {

	//Menu old or new style
	$section_name = $menu_old_style ? $section_name : constant($section_name);

	//If the name has a (2) or (3) appendix, assume it belongs to previous menu section
	$last_bit = substr($section_name,-4);
	if ($last_bit != ' (2)' and $last_bit != ' (3)') {

		//Title of menu section
		$menu_sections[$menu_counter]	= "\n\n<span class='menu_section_header'>$section_name</span>";
	}
	else {

		//Decrease counter, because in essence this is the same category
		$menu_counter--;
	}

	//All modules in this section
	if (!isset($menu_options[$menu_counter])) {
		$menu_options[$menu_counter] = array();
	}
	foreach ($section_modules as $module_id) {

		//Menu old or new style
		$module_id = $menu_old_style ? $module_id : constant($module_id);

		//Installation check
		if (!isset($installed_modules[$module_id])) {
			if (!isset($warned)) {
				$Message->set(DIGI_ERR_CFG_MENU_ELEMENTS, WARNING);
				$warned = true;
			}
			continue;
		}

		//Read module properties
		$menu_priv = key($modules_in_use[$module_id][2]);
		$menu_file = $modules_in_use[$module_id][1][0];
		$menu_icon = $modules_in_use[$module_id][6];
		$menu_name = $modules_in_use[$module_id][0];

		if (is_array($menu_name)) {
			$menu_name = $menu_name[0];
		}

		//Verify privillege and make HTML
		if (empty($menu_priv) or checkPriv($menu_priv)) {
			if ($active_module == $module_id) {
				$menu_class		=	'cms_menu_active';
			}
			else {
				$menu_class		=	'cms_menu_regular';
			}
			$menu_options[$menu_counter][] = "\n<a href='$menu_file' title='$menu_name' class='$menu_class'>$menu_name</a>";
		}
	}

	$menu_counter++;
}

foreach ($menu_sections as $key => $menu_section) {
	if (count($menu_options[$key])) {
		$menu_html .= "<div class='menu_section'>".$menu_section;
		foreach ($menu_options[$key] as $menu_option) {
			$menu_html .= $menu_option;
		}
		$menu_html .= '</div>';
	}
}

if ($s_id) {
	$menu_html .= "
		<div class='menu_section'>
			<span class='menu_section_header'>".TXT_LOGOUT."</span>
    		<a href='$_file?logout=1' class='cms_menu_regular'>".TXT_LOGOUT."</a>
 		</div>
	";
}

// *** Calendar support
$calendar_support = $calendar_support ? "
<script type='text/javascript' src='../js/calendar.js'></script>
<script type='text/javascript' src='../js/calendar-nl.js'></script>
<script type='text/javascript' src='../js/calendar-setup.js'></script>
<link href='../style/style_calendar.css' rel='stylesheet' type='text/css'>
" : '';

// *** JS
$javascript = "
<script type='text/javascript' src='../js/globals.js'></script>
<script type='text/javascript' src='../js/ajax_functionality.js'></script>
";

// *** FAQ and support
$top_icons = $s_id ? "
<a href='cms_faq.php' title='".TXT_HELP_USING_THE_CMS."'><img style='padding-top: 2px;' onmouseover='this.src=\"../gfx/cms/icons/help.png\"' onmouseout='this.src=\"../gfx/cms/icons/help_off.png\"' src='../gfx/cms/icons/help_off.png' border='0px' /></a>
&nbsp;
<a href='$mailer$cfg[TECH_EMAIL]' title='".str_replace('[TECH_NAME]', $cfg['TECH_NAME'], TXT_EMAIL_TECH_SUPPORT)."'><img onmouseover='this.src=\"../gfx/cms/icons/helpdesk.png\"' onmouseout='this.src=\"../gfx/cms/icons/helpdesk_off.png\"' src='../gfx/cms/icons/helpdesk_off.png' border='0px' /></a>
" : '';
?>