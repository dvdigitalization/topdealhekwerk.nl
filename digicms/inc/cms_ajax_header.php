<?php
/*
Title:		CMS AJAX header construct
File: 		inc/cms_ajax_header.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Error displaying
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_DEPRECATED);

// *** Page loading benchmark time
$m_time 	= explode(' ',microtime());
$start_time = $m_time[1] + $m_time[0];

// *** Testmode
$test_servers	=	array(	'dev.digitalization.nl','pub.digitalization.nl','archive.digitalization.nl','cms.digitalization.nl',
							'admin.digitalization.nl','192.168.10.1','194.171.50.215','localhost');
$testmode 		= 	in_array($_SERVER['HTTP_HOST'], $test_servers);

// *** Refuse to run with install directory present
if (!$testmode and file_exists('install')) {
	echo " *** DELETE THE DIRECTORY /install BEFORE USING THE CMS ***";
	exit;
}

// *** Output buffering (compression caused some problems in IE...)
ob_start();
register_shutdown_function('ob_end_flush');

// *** Database table prefix
define('DIGI_DB_PREFIX',	'digi_');

// *** CMS path prefix
define('CMS_PATH_PREFIX',	'../');

// *** Initial settings (backwards compatible with version 2.0x)
if (file_exists(CMS_PATH_PREFIX.'inc/config.php')) {
	require_once(CMS_PATH_PREFIX.'inc/config.php');

	//Overwrite the site name variable
	$site_name	=	$site_name_cms;
}
else {
	require_once('inc/config/cfg_global.php');
}

// *** CMS root folder definition (for backwards compatibility with version 2.0x)
if (file_exists(CMS_PATH_PREFIX.'inc/common/cms_root_definition.php')) {
	require_once(CMS_PATH_PREFIX.'inc/common/cms_root_definition.php');
}
elseif (!isset($cms_folder)) {
	$cms_folder	= 'digicms';
}

// *** Language backwards compatibiliyy
if (!isset($cms_lang) or !file_exists('inc/lang/'.$cms_lang.'_cms.php')) {
	$cms_lang = 'NL';
}

// *** Private folder
if (isset($private_folder) and !empty($private_folder)) {

	if (substr($private_folder, -1) != '/') {
		$private_folder .= '/';
	}
	if (!is_dir($private_folder)) {
		$private_folder = '';
	}
}
else {
	$private_folder = '';
}

// *** Session functionality
require_once(CMS_PATH_PREFIX.'classes/class_Session.php');

// *** Create new session object
$is_cms		=	true;
$Session 	= 	new Session();

// *** UTF8 / ISO-8859-1 character support?
header('Content-Type: text/html; charset='.$charset);

// *** Database settings
if ($private_folder and file_exists($private_folder.'db_settings.php')) {
	require_once($private_folder.'db_settings.php');
}
elseif (file_exists(CMS_PATH_PREFIX.'inc/db_settings.php')) {
	require_once(CMS_PATH_PREFIX.'inc/db_settings.php');
}
elseif (file_exists(CMS_PATH_PREFIX.'inc/style.php')) {
	require_once(CMS_PATH_PREFIX.'inc/style.php');
}
else {
	echo 'Cant find any database settings file @_@';
	exit;
}

// *** Required files for database functionality
require_once(CMS_PATH_PREFIX.'inc/db.php');
require_once(CMS_PATH_PREFIX.'classes/class_DB.php');

// *** Create database connection object and define the database table prefix
$DB = new DB(dbHost, dbUser, dbPass, dbName);

// *** Global and validation functions
require_once(CMS_PATH_PREFIX.'inc/validation.php');
require_once(CMS_PATH_PREFIX.'inc/globals.php');
require_once(CMS_PATH_PREFIX.'inc/globals_cms.php');

// *** UTF-8 MySQL support
if ($charset == 'utf-8') {
	eq("SET NAMES 'utf8';");
}

// *** Settings
$res = eq("SELECT setting, setting_value FROM ".DIGI_DB_PREFIX."settings;");
while ($myo = mfo($res)) {
	$cfg[$myo->setting] = $myo->setting_value;
}

// *** Version
$_version 	= 	$cfg['CMS_VERSION_MAJOR'].'.'.sprintf('%02d', $cfg['CMS_VERSION_MINOR']);
$_revision	=	'r'.sprintf('%03d', $cfg['CMS_VERSION_REVISION']);

// *** Digi error handling
require_once(CMS_PATH_PREFIX.'inc/error_handling.php');

// *** Filesystem vars
$_file 		= 	basename(htmlspecialchars($_SERVER['SCRIPT_NAME']));
$_path 		= 	substr(htmlspecialchars($_SERVER['SCRIPT_NAME']),0,strlen($_SERVER['SCRIPT_NAME'])-strlen($_file));
$_path		=	'/'.trim($_path,'/').'/';
$_server	= 	$_SERVER['SERVER_NAME'];
$_server	=	count(explode('.', $_server)) > 2 ? $_server : 'www.'.$_server;
$_link		=	'http://'.$_server.substr($_path,0,strlen($_path)-strlen($cms_folder)-1);
$_file_abs	=	$_server.$_path.$_file;
$_sep  		= 	DIRECTORY_SEPARATOR;
$_ip		= 	getUserIP();
$_lang 		= 	request('lang');

// *** Sha encoding hashes
$sha1_appendix	=	'xGk32j5037dSfI';
$sha1_hash		=	'UchYeW91Jzk4i0';

// *** Soap server and hash
$soap_server	=	'http://soap.digitalization.nl/';
$soap_appendix	=	'JKjeFH92Jed3H3';

// *** Upload directory for temporary files
$upload_dir				=	'uploads';

// *** Installed languages
$installed_languages	=	array();
foreach (explode(';', $cfg['INSTALLED_LANGUAGES']) as $lang_code) {
	$installed_languages[$lang_code] = $lang_code;
}

// *** Language selection handling
if (empty($_lang) or !in_array($_lang, $installed_languages)) {
	$_lang = current($installed_languages);
}

// *** License key generation
$license_key	=	base64_encode(
						strrev(
						$sha1_appendix.
						sha1($cfg['REGISTRATION_NAME'].$sha1_appendix.'').
						sha1($_server.$sha1_appendix).
						sha1($cfg['INSTALLED_MODULES'].$sha1_appendix).
						$sha1_hash
						)
					);

// *** License file reading
$license_file	=	file('license');
array_pop	($license_file);
array_shift	($license_file);
evalTrim	($license_file);
$license_file	=	trim(implode('',$license_file));

// *** License match check
if ($license_file != $license_key and !$testmode) {
	echo " *** THIS VERSION OF THE DIGITALIZATION CMS HAS NOT BEEN REGISTERED PROPERLY. ***
			<br/><br/>
			To register this copy of the Digitalization CMS, please contact Digitalization and provide the following credentials:
			<br/>
			<br/><b>Registrant:</b> $cfg[REGISTRATION_NAME]
			<br/><b>Server:</b> $_server
			<br/><b>Version:</b> $_version ($_revision)
			<br/><b>Modules:</b> ".implode(', ',explode(';', $cfg['INSTALLED_MODULES']))."
	";
	exit;
}

// *** Load language files
require_once('inc/lang/'.$cms_lang.'_cms.php');
require_once('inc/lang/'.$cms_lang.'_errors.php');
require_once('inc/lang/'.$cms_lang.'_customer.php');

// *** Array definitions
require_once(CMS_PATH_PREFIX.'inc/arrays_'.$cms_lang.'.php');

// *** Initialize message system
require_once(CMS_PATH_PREFIX.'classes/class_Message.php');
$Message = new Message();

// *** Login functionality
require_once(CMS_PATH_PREFIX.'classes/class_Login.php');
require_once(CMS_PATH_PREFIX.'classes/class_Login_CMS.php');

// *** CMS modules and icons, JS definitions
require_once('inc/definitions/def_modules_and_privs.php');
require_once('inc/definitions/def_modules_custom.php');
require_once('inc/definitions/def_cms_icons.php');
require_once('inc/definitions/def_digicons.php');
require_once('inc/definitions/def_js.php');

// *** Custom CMS icons override
if (file_exists('inc/definitions/def_cms_icons_custom.php')) {
	require_once('inc/definitions/def_cms_icons_custom.php');
}

// *** Custom digicons override
if (file_exists('inc/definitions/def_digicons_custom.php')) {
	require_once('inc/definitions/def_digicons_custom.php');
}

// *** Set the installed modules
$installed_modules 	= 	explode(';', $cfg['INSTALLED_MODULES']);
$installed_modules 	= 	array_flip($installed_modules);
$modules_in_use		=	$modules + $custom_modules;
$active_module		= 	0;
$this_page_priv		=	0;
$calendar_support 	= 	false;

// *** Load up the modules
foreach ($modules_in_use as $module_id => $module) {

	//Installation check
	if (!isset($installed_modules[$module_id])) {
		continue;
	}

	//Fetch module info
	list($module_name, $module_files, $module_privs, $module_sql, $module_calendar_support, $mdi, $module_icon) = $module;

	//Check module files against current file and set some module vars
	if (($key = array_search($_file, $module_files)) !== false) {

		if (is_array($module_name)) {
			$module_name = $module_name[$key];
		}

		$page_title 		= 	$module_name;
		$db_table			=	DIGI_DB_PREFIX.$module_sql;
		$calendar_support	=	$module_calendar_support;
		$active_module		=	$module_id;
		$this_page_priv		=	key($module_privs);
		$site_icon			=	isset($icon[$module_icon]) ? $icon[$module_icon] : '../gfx/cms/icons/'.$module_icon;
	}

	//Check if the module is really installed
	foreach ($module_files as $module_file) {
		if (!file_exists($module_file)) {
			if (is_array($module_name)) $module_name = $module_name[0];
			$Message->set("De module $module_name is niet correct geinstalleerd.<br/>Het bestand $module_file ontbreekt.", CRITICAL);
		}
	}

	//Define the privs
	foreach ($module_privs as $priv_id => $priv) {
		if (is_array($priv) and !empty($priv_id) and !defined($priv[0])) {
			define($priv[0], $priv_id);
			$privs_array[$priv_id] = $priv[1];
		}
		else if (!is_array($priv) and !empty($priv_id) and !defined($priv)) {
			define($priv, $priv_id);
			$privs_array[$priv_id] = $priv;
		}
	}
}

// *** Home page title
if ($_file == 'index.php' or !isset($page_title)) {
	$page_title				=	HOME;
}

// *** Timezone
date_default_timezone_set($cfg['TIMEZONE']);

// *** Site specific definitions
if (file_exists(CMS_PATH_PREFIX.'inc/common/def_site_specific.php')) {
	require_once(CMS_PATH_PREFIX.'inc/common/def_site_specific.php');
}
else {
	require_once(CMS_PATH_PREFIX.'inc/site_specific_definitions.php');
}

// *** Safe_mode() warning
if (ini_get('safe_mode') and !$cfg['SUPRESS_SAFE_MODE_WARNING']) {
	$Message->set(DIGI_ERR_SAFEMODE, WARNING);
}

// *** Register_globals() warning
if (ini_get('register_globals') and !$cfg['SUPRESS_REG_GLOB_WARNING']) {
	$Message->set(DIGI_ERR_REG_GLOBALS, WARNING);
}

// *** Handle email batch sending
if (isset($installed_modules[MOD_MAILER])) {
	require_once(CMS_PATH_PREFIX.'inc/send_mails2go.php');
}

// *** Page and sort inputs
$page_and_sort_inputs = "
    \n<input type='hidden' name='page' 		value='".request('page')."' />
    \n<input type='hidden' name='sort' 		value='".request('sort')."' />
    \n<input type='hidden' name='method' 	value='".request('method')."' />
";

// *** Digi IP's
$digi_ips_array	=	array(
	'145.94.72.56',
	'145.94.72.173',
	'194.171.50.215',
	'192.168.1.143',
	'192.168.1.145'
);

// *** Search query
$search_appendix = ($search_query = request('search_query')) ? '&search_query='.urlencode($search_query) : '';
evalAll($search_query);

// *** Generate random string and set default link appendix
$random_string 	= 	randomPass(12);
$link_appendix 	= 	'rs='.$random_string.$search_appendix;

// *** Valid import filetypes
$IMPORT_FORMATS = array(
						1 => 'xml',
						2 => 'csv'
);

// *** Authenticate at this point
$s_id = $s_admin = $s_privs = '';
auth();

// *** Uses mailer?
$mailer	= (isset($installed_modules[MOD_MAILER]) and isset($_SESSION['uses_mailer']) and $_SESSION['uses_mailer'] and checkPriv(PRIV_MAILER)) ? 'cms_mailer.php?email=' : 'mailto:';

// *** Overwrite page title if not logged in, redirect user to index page
if (!$s_id) {
	if ($_file != 'index.php') {
		header('Location: index.php');
		exit;
	}
	$page_title = 'Inloggen';
}

// *** Check this page priv, redirect to index page if not privillege
if ($s_id and isset($this_page_priv) and $this_page_priv and !checkPriv($this_page_priv)) {
	header('Location: index.php');
	exit;
}

// *** Garbage collection
require_once(CMS_PATH_PREFIX.'classes/class_GarbageCollector.php');
$GC = new GarbageCollector();

// *** Auto backup functionality
require_once(CMS_PATH_PREFIX.'classes/class_Backup.php');
$Backup	= new Backup(true);

// *** Standard variable initialization
$pre_html		=	'';
$data_rows 		= 	array();
$where			=	array();
$total_items	=	0;
$chk			=	"checked='checked'";
?>