<?php
/*
Title:		Configuration settings and setting set language definitions [EN]
File: 		inc/definitions/def_general_settings_EN.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Categories
$setting_set['_general']				=	'General';
$setting_set['CMS']						=	'CMS';
$setting_set['agenda']					=	'Agenda';
$setting_set['backup']					=	'Backup';
$setting_set['blog']					=	'Blog';
$setting_set['calendar']				=	'Calendar';
$setting_set['company']					=	'Company information';
$setting_set['contact']					=	'Contact form';
$setting_set['customers']				=	'Customers';
$setting_set['extranet']				=	'Extranet';
$setting_set['google_maps']				=	'Google Maps';
$setting_set['invoices']				=	'Invoices';
$setting_set['mailer']					=	'Mailing';
$setting_set['menu']					=	'Menu';
$setting_set['news']					=	'News topics';
$setting_set['orders']					=	'Orders';
$setting_set['pages']					=	'Website pages';
$setting_set['photos']					=	'Photo albums';
$setting_set['products']				=	'Products';
$setting_set['product_cats']			=	'Product categories';
$setting_set['projects']				=	'Projects';
$setting_set['site_stats']				=	'Website statistics';
$setting_set['timer']					=	'Timer';
$setting_set['user_files']				=	'Website files';
$setting_set['sitemap']					=	'Sitemap';

// *** General
$setting['ITEMS_PER_PAGE']				=	'The maximum number of elements shown per page';
$setting['MAX_PAGES_IN_LIST']			=	'The maximum number of pages in the list';
$setting['SHOW_FAQ_LINKS']				=	'Show FAQ links withing the CMS';
$setting['ADMIN_NAME']					=	'Website administrator name';
$setting['ADMIN_EMAIL']					=	'Website administrator email address';
$setting['SITE_EMAIL_ADDRESS']			=	'Sender email address for emails sent from the website';
$setting['SITE_EMAIL_NAME']				=	'Sender name for emails sent from the website';
$setting['DISK_SPACE']					=	'Total diskspace available (in MB)';
$setting['DISK_SPACE_WARN']				=	'Warn on low disk space';
$setting['DISK_SPACE_LAST_CHECKED']		=	'Last diskspace check timestamp';
$setting['CHECK_FOR_UPDATES']			=	'Check for new updates automatically';
$setting['UPDATE_CHECK_FREQUENCY']		=	'Check for new updates after this amount of days';
$setting['ACTIVITY_LOGGING']			=	'Keep track of activities of users within the CMS';
$setting['IN_MAINTENANCE']				=	'Website in maintenance';
$setting['MAINTENANCE_END_TIME']		=	'Website in maintenance until';

// *** CMS
$setting['GARBAGE_LAST_COLLECTED']		=	'Garbage collection timestamp';
$setting['GARBAGE_COLLECT_INTERVAL']	=	'Garbage collection interval';
$setting['FIRST_LOGIN_PASS_CHANGED']	=	'Password was changed after first login';
$setting['SUPRESS_SAFE_MODE_WARNING']	=	'Supress safe_mode() warning';
$setting['SUPRESS_REG_GLOB_WARNING']	=	'Supress register_globals() warning';
$setting['ALLOW_COOKIE_LOGIN']			=	'Cookie login allowed';
$setting['COOKIE_EXPIRE_TIME']			=	'Cookie expiry time in days';
$setting['INSTALLED_MODULES']			=	'Installed CMS modules';
$setting['INSTALLED_LANGUAGES']			=	'Installed languages';
$setting['REGISTRATION_NAME']			=	'CMS registration name';
$setting['CMS_REGISTRATION_ID']			=	'CMS registration ID';
$setting['TECH_NAME']					=	'Technical support name';
$setting['TECH_EMAIL']					=	'Technical support email address';
$setting['TIMEZONE']					=	'Timezone (default Europe/Amsterdam)';
$setting['DIGI_ERROR_HANDLING']			=	'Errors are handled with the Digi error handler';
$setting['SHOW_ERRORS']					=	'Error messages are displayed';
$setting['ERROR_MAILING']				=	'Errors are sent per email';
$setting['DIGI_ALERT_EMAIL']			=	'Errors sending email address';
$setting['ERROR_LOGGING']				=	'Errors are saved in a log file';
$setting['IGNORE_NOTICE_ERRORS']		=	'Error notices are ignored';
$setting['CMS_VERSION_MAJOR']			=	'Version (major)';
$setting['CMS_VERSION_MINOR']			=	'Version (minor)';
$setting['CMS_VERSION_REVISION']		=	'Version (revision)';
$setting['LAST_CHECK_IN']				=	'Last check-in';
$setting['LAST_UPDATE_CHECK']			=	'Update check timestamp';
$setting['UPDATE_AVAILABLE']			=	'Update available';
$setting['UPDATE_TITLE']				=	'Update notice/title';
$setting['USE_REWRITE_ENGINE']			=	'Rewrite Engine is used';
$setting['UPDATE_FTP_DEST_FOLDER']		=	'Auto-update FTP destination folder';
$setting['UPDATE_FTP_SERVER']			=	'Auto-update FTP server';
$setting['UPDATE_FTP_USER']				=	'Auto-update FTP username';
$setting['UPDATE_FTP_PASSWORD']			=	'Auto-update FTP password';
$setting['UPDATE_FTP_REMEMBER']			=	'Auto-update FTP password is saved';

// *** Agenda / calendar
$setting['AGENDA_PHOTO_SIZE']			=	'Maximum dimension for images (pixels)';
$setting['AGENDA_THUMB_SIZE']			=	'Thumbnail dimension (pixels)';
$setting['AGENDA_PHOTO_CONSTRAINT']		=	'Images constraint';
$setting['AGENDA_THUMB_CONSTRAINT']		=	'Thumbnail constraint';
$setting['AGENDA_SHOW_IMAGE']			=	'Show image option';
$setting['AGENDA_SHOW_PREVIEW']			=	'Show preview option';

// *** Backup
$setting['MAX_AMOUNT_OF_DUMPS']			=	'Maximum amount of restore points that is saved';
$setting['LAST_DUMP']					=	'Last restore point timestamp';
$setting['DUMP_INTERVAL']				=	'Automatic restore point is created after this amount of days';
$setting['MAKE_AUTO_BACKUPS']			=	'Automatic restore point creation';
$setting['EMAIL_BACKUP_FILES']			=	'Send backup files by email';
$setting['EMAIL_BACKUPS_MAX_SIZE']		=	'Maximum filesize for backup file mailing in bytes';
$setting['EMAIL_BACKUPS_ADDRESS']		=	'Email address to send backup files to';

// *** Blog
$setting['BLOG_PHOTO_SIZE']				=	'Maximale afmeting voor afbeeldingen (pixels)';
$setting['BLOG_THUMB_SIZE']				=	'Thumbnail afmeting (pixels)';
$setting['BLOG_PHOTO_CONSTRAINT']		=	'Afbeeldingen constraint';
$setting['BLOG_THUMB_CONSTRAINT']		=	'Thumbnails constraint';
$setting['BLOG_SHOW_IMAGE']				=	'Afbeelding optie tonen';
$setting['BLOG_SHOW_PREVIEW']			=	'Preview optie tonen';
$setting['BLOG_RSS_ENABLED']			=	'RSS nieuwsfeed actief';

// *** Company info
$setting['COMPANY_VAT_NO']				=	'VAT number';
$setting['COMPANY_KVK_NAME']			=	'Chamber of commerce region';
$setting['COMPANY_KVK_NO']				=	'Chamber of commerce number';
$setting['COMPANY_BANK_NAME']			=	'Bank name';
$setting['COMPANY_BANK_NO']				=	'Bank account number';
$setting['COMPANY_TEL']					=	'Phone number';
$setting['COMPANY_EMAIL']				=	'Email address';
$setting['COMPANY_WEB']					=	'Website';
$setting['COMPANY_NAME']				=	'Company name';
$setting['COMPANY_ADDRESS']				=	'Address and nr';
$setting['COMPANY_PC']					=	'Postal code';
$setting['COMPANY_CITY']				=	'City';

// *** Contact form
$setting['CONTACT_FORM_EMAIL']			=	'Email addres(ses) to send filled in contact forms to';

// *** Customers
$setting['CUSTOMER_PREFIX']				=	'Prefix for customer numbers';
$setting['CUSTOMER_L_ZEROES']			=	'Amount of digits for customer numbers';
$setting['CUSTOMER_CONST']				=	'Customer numbers constant';

// *** Extranet
$setting['SITE_ALLOW_COOKIE_LOGIN']		=	'Allow cookie login (remember users)';
$setting['SITE_COOKIE_EXPIRE_TIME']		=	'Cookies expire after this amount of days';
$setting['ALLOW_LOGIN_MAINSWITCH']		=	'Allow login on the website';
$setting['REG_AUTO_CLEANSE']			=	'Automatically remove not-validated registrations';
$setting['REG_CLEANSE_DAYS']			=	'Automatically remove after this amount of days';
$setting['REG_MIN_PW_LENGTH']			=	'Minimum password length';
$setting['EXTRANET_FILES_AT_ONCE']		=	'Maximum amount of files to be uploaded at once';
$setting['EXTRANET_NO_SPACE']			=	'Do not allow spaces in filenames';
$setting['EXTRANET_LOWERCASE']			=	'Transform filenames to lowercase';

// *** Google maps
$setting['GOOGLE_MAPS_KEY']				=	'Google maps key for this server';

// *** Invoices
$setting['INVOICE_NO_RESET_EACH_YEAR']	=	'Start invoice numbering at 1 each year';
$setting['INVOICE_NO_FORMAT_ADM']		=	'Invoice numbering format';
$setting['INVOICE_CONST']				=	'Invoice numbers constant';
$setting['INVOICE_ALERT_TIME']			=	'Invoice payment is late after this amount of days';
$setting['INVOICE_LOGO_URL']			=	'Invoice logo URL';

// *** Mailer / newsletters
$setting['MAILER_EMAIL_ADDRESSES']		=	'Email addresses for use with the mailer (separate with semi-colon)';
$setting['DEFAULT_BCC']					=	'Default BCC email address';
$setting['MAX_PER_BATCH']				=	'Maximum amount of emails per batch';
$setting['BATCH_INTERVAL']				=	'Batch interval in seconds';
$setting['LAST_BATCH_SENT']				=	'Last batch timestamp';
$setting['AUTO_SENDING']				=	'Automatically send email batches';
$setting['USE_SMTP']					=	'Use SMTP for email sending';
$setting['SMTP_SERVER']					=	'SMTP server';
$setting['NL_FROM_EMAIL']				=	'Newsletter send email address';
$setting['NL_DATE_FORMAT']				=	'Newsletter date format';
$setting['NL_TEST_ADDRESSES']			=	'Newsletter test email adres(ses)';
$setting['NL_SIGN_OFF_URL']				=	'Newsletter unsubscribe page';
$setting['NL_DB_TABLES']				=	'Newsletter subscribers tables (separate with semi-colon)';

// *** Menu
$setting['MENU_SECTION_OPTIONS']		=	'Show menu section options';
$setting['MENU_SUB_OPTIONS']			=	'Show menu sub-item options';
$setting['MENU_IGNORE_WRONG_LINKS']		=	'Ignore wrong links';

// *** News
$setting['NEWS_PHOTO_SIZE']				=	'Maximum dimension for images (pixels)';
$setting['NEWS_THUMB_SIZE']				=	'Thumbnail dimension (pixels)';
$setting['NEWS_PHOTO_CONSTRAINT']		=	'Images constraint';
$setting['NEWS_THUMB_CONSTRAINT']		=	'Thumbnail constraint';
$setting['NEWS_SHOW_IMAGE']				=	'Show image option';
$setting['NEWS_SHOW_PREVIEW']			=	'Show preview option';
$setting['NEWS_RSS_ENABLED']			=	'Activate RSS newsfeed';
$setting['NEWS_COMMENTS_ENABLED']		=	'Activate comments on news topics';
$setting['NEWS_REWRITE_PREFIX']			=	'Rewrite engine prefix';

// *** Orders
$setting['ORDER_ID_FORMAT']				=	'Orders ID format';
$setting['INVOICE_NO_FORMAT']			=	'Invoice numbers format';

// *** Pages
$setting['PAGES_SHOW_IMAGE']			=	'Show image/flash option';
$setting['PAGES_SHOW_META']				=	'Show meta tags option';
$setting['PAGES_SHOW_TEMPLATE']			=	'Show template option';
$setting['PAGES_SHOW_HTML_NAME']		=	'Show HTML name option';
$setting['PAGES_SHOW_FILE']				=	'Show file option';
$setting['PAGES_SHOW_AUTH']				=	'Show security options';
$setting['PAGES_SHOW_FREE']				=	'Show free field option';
$setting['PAGEIMAGE_SIZE']				=	'Maximum dimension for images (pixels) when resizing';
$setting['PAGEIMAGE_CROP_WIDTH']		=	'Fixed width for images (pixels) when cropping';
$setting['PAGEIMAGE_CROP_HEIGHT']		=	'Fixed height for images (pixels) when cropping';
$setting['FREE_FIELD_TITLE']			=	'Free field title';
$setting['FREE_FIELD_EXPLANATION']		=	'Free field explanation';
$setting['PAGES_HARD_LINK_ML']			=	'Link multi-language pages to each other';

// *** Photo albums
$setting['USE_EXIF_TIMESTAMP']			=	'Use EXIF date and time for photo\'s';
$setting['ALB_MAX_FILES_AT_ONCE']		=	'Maximum amount of photo\'s allowed to be uploaded at once';
$setting['ALB_NO_SPACE_IN_FILENAME']	=	'Do not allow spaces in filenames';
$setting['ALB_FILENAMES_TO_LOWERCASE']	=	'Transform filenames to lowercase';
$setting['ALB_PHOTO_SIZE']				=	'Default photo resize dimension (in pixels)';
$setting['ALB_THUMB_SIZE']				=	'Default thumbnail dimension (in pixels)';
$setting['ALB_MAKE_THUMBS']				=	'Make thumbnails of the photo\'s';
$setting['ALB_RESIZE_PHOTOS']			=	'Resize photo\'';
$setting['MAX_DUMP_AT_ONCE']			=	'Maximum amount of photo\'s to dump at once';
$setting['ALB_USE_WATERMARK']			=	'Use watermark';
$setting['ALB_WATERMARK']				=	'Watermark text';
$setting['ALB_WATERMARK_COLOR']			=	'Watermark color';
$setting['ALB_WATERMARK_SIZE']			=	'Watermark size';
$setting['SQUARE_THUMBS']				=	'Square thumbnails';

// *** Products
$setting['PROD_IMAGE_SIZE']				=	'Maximum dimension for images (pixels)';
$setting['PROD_THUMB_SIZE']				=	'Thumbnail dimension (pixels)';
$setting['PROD_IMAGE_CONSTRAINT']		=	'Images constraint';
$setting['PROD_THUMB_CONSTRAINT']		=	'Thumbnail constraint';
$setting['PROD_USE_IMAGES']				=	'Use images for products';
$setting['PROD_IMAGE_RESIZE']			=	'Resize images';
$setting['PRODUCTS_STANDARD_VAT']		=	'Default VAT rate for products (in %)';
$setting['PROD_USE_SEPARATE_PROPS']		=	'Show option for separate name/description per product';
$setting['PROD_USE_SEPARATE_PROD_NO']	=	'Show option for separate product number per product';
$setting['PROD_ALLOW_SAME_SPECS']		=	'Allow duplicate product properties within a product group';
$setting['DISCOUNT_ONLY_ON_NONSALE']	=	'Apply discount only on products that are not on sale';

// *** Product categories
$setting['PROD_CAT_IMAGE_SIZE']			=	'Maximum dimension for images (pixels)';
$setting['PROD_CAT_IMAGE_CONSTRAINT']	=	'Images constraint';
$setting['PROD_CAT_IMAGE_RESIZE']		=	'Resize images';
$setting['PROD_CAT_USE_IMAGES']			=	'Use images for product categories';
$setting['PROD_CAT_ALLOW_SUBCATS']		=	'Allow sub categories';

// *** Projects
$setting['PROJECT_PREFIX']				=	'Prefix for project numbers';
$setting['PROJECT_L_ZEROES']			=	'Amount of digits for project numbers';
$setting['PROJECT_CONST']				=	'Project numbers constant';
$setting['PROJECT_DEADLINE_TIME']		=	'Default project deadline time (in days)';
$setting['PROJECT_DEADLINE_ALERT_DAYS']	=	'Deadline warning email is sent this many days before';
$setting['PROJECT_ALERTS_TIMESTAMP']	=	'Last projects deadline warning check timestamp';

// *** Sitemap
$setting['SITEMAP_REFRESH_RATE']		=	'Sitemap refresh rate (in days)';
$setting['SITEMAP_LAST_UPDATED']		=	'Last sitemap refresh timestamp';

// *** Site statistics
$setting['STATS_SINCE']					=	'Website statistics are being tracked since this date';
$setting['PAGE_HITS_START_VALUE']		=	'Start value for the amount of page hits';
$setting['UNIQUE_VISITORS_START_VALUE']	=	'Start value for the amount of unique visitors';
$setting['STATS_UNIQUE_INTERVAL']		=	'The amount of hours after which a visitor is counted as unique again';

// *** Timer
$setting['TIMER_HOURS_PRECISION']		=	'Hours rounding precision';
$setting['TIMER_ENABLED']				=	'Use the timer';

// *** User files
$setting['MAX_FILES_AT_ONCE']			=	'Maximum files allowed to be uploaded at once';
$setting['NO_SPACE_IN_FILENAME']		=	'Do not allow spaces in filenames';
$setting['FILENAMES_TO_LOWERCASE']		=	'Transform filenames to lowercase';
$setting['DEFAULT_IMAGE_RESIZE_SIZE']	=	'Default image resize dimension (in pixels)';
?>