<?php
/*
Title:		Page set language definitions [NL]
File: 		inc/definitions/def_page_sets_NL.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Categories
$page_set_names['_general']				=	'Algemeen';
$page_set_names['agenda']				=	'Agenda';
$page_set_names['calendar']				=	'Kalender';
$page_set_names['blog']					=	'Blog';
$page_set_names['spam']					=	'SPAM filter';
$page_set_names['contact']				=	'Contactformulier';
$page_set_names['news']					=	'Nieuwsberichten';
$page_set_names['news_comments']		=	'Reacties op nieuwsberichten';
$page_set_names['newsletter']			=	'Nieuwsbrief';
$page_set_names['photos']				=	'Foto albums';
$page_set_names['polls']				=	'Polls';
$page_set_names['search']				=	'Zoeken';
$page_set_names['tell-a-friend']		=	'Tell-a-friend';
$page_set_names['cart']					= 	'Winkelwagen';
$page_set_names['checkout']				=	'Kassa';
$page_set_names['payment']				=	'Betaling';
$page_set_names['login']				=	'Inloggen';
$page_set_names['registration']			=	'Registratie';
$page_set_names['guestbook']			=	'Gastenboek';
$page_set_names['google_maps']			=	'Google maps';
$page_set_names['invoice']				=	'Factuur';
$page_set_names['vacancies']			=	'Vacatures';

?>