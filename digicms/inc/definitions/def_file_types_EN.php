<?php
/*
Title:		File type definitions [EN]
File: 		inc/definitions/def_file_types_EN.php
Version: 	v2.04
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// GENERIC
$f_type[0]		=	' bestand';

// WEBDEVELOPMENT
$f_type['css']	=	'Stylesheet';
$f_type['php']	=	'PHP code';
$f_type['js']	=	'Javascript code';
$f_type['sql']	=	'Database code';
$f_type['rss']	=	'News feed';
$f_type['htm']	=	'Website file';
$f_type['html']	=	'Website file';
$f_type['log']	=	'Log file';
$f_type['xml']	=	'XML file';

// OFFICE AND PDF
$f_type['pdf']	=	'PDF document';
$f_type['doc']	=	'Word document';
$f_type['docx']	=	'Word document';
$f_type['csv']	=	'Excel file';
$f_type['xls']	=	'Excel spreadsheet';
$f_type['xlsx']	=	'Excel spreadsheet';
$f_type['ppt']	=	'Powerpoint presentation';
$f_type['pptx']	=	'Powerpoint presentation';

// OPEN OFFICE
$f_type['svw']	=	'OO writer document';
$f_type['sxc']	=	'OO calc spreadsheet';
$f_type['sxm']	=	'OO math document';
$f_type['sxi']	=	'OO impress presentation';
$f_type['sxd']	=	'OO draw document';

// PLAIN TEXT
$f_type['txt']	=	'Text file';
$f_type['rtf']	=	'Text file';

// FLASH
$f_type['swf']	=	'Flash animation';
$f_type['fla']	=	'Flash animation';

// MOVIES
$f_type['avi']	=	'Video';
$f_type['wmv']	=	'Video';
$f_type['mpg']	=	'Video';
$f_type['mpeg']	=	'Video';
$f_type['rm']	=	'Video';
$f_type['flv']	=	'Flash video';
$f_type['mov']	=	'Quicktime video';

// IMAGES
$f_type['jpg']	=	'Web image';
$f_type['jpeg']	=	'Web image';
$f_type['gif']	=	'Web image';
$f_type['png']	=	'Web image';
$f_type['tif']	=	'Bitmap';
$f_type['tiff']	=	'Bitmap';
$f_type['bmp']	=	'Bitmap';
$f_type['svg']	=	'Vector image';
$f_type['psd']	=	'Photoshop image';
$f_type['ico']	=	'Icon';

// AUDIO
$f_type['mp3']	=	'Mp3 audio';
$f_type['mp4']	=	'Mp3 audio';
$f_type['wav']	=	'Audio';
$f_type['wma']	=	'Audio';
$f_type['au']	=	'Audio';
$f_type['ra']	=	'Real audio';
$f_type['mid']	=	'Midi audio';
$f_type['midi']	=	'Midi audio';

// ARCHIVE FILES
$f_type['zip']	=	'Archive';
$f_type['rar']	=	'Archive';
$f_type['arj']	=	'Archive';
$f_type['ace']	=	'Archive';
$f_type['tar']	=	'Archive';
$f_type['gz']	=	'Archive';
$f_type['bz2']	=	'Archive';
$f_type['lha']	=	'Archive';
$f_type['iso']	=	'Archive';
$f_type['gzip']	=	'Archive';
?>