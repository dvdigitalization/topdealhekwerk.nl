<?php
/*
Title:		CMS icon definitions
File: 		inc/definitions/def_cms_icons.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
Icons by:	http://dryicons.com/
*/

// Default cms icons
$icon['computer']				=	'../gfx/cms/icons/computer.png';
$icon['auth_users']				=	'../gfx/cms/icons/auth_users.png';
$icon['page_contents']			=	'../gfx/cms/icons/page_contents.png';
$icon['spam']					=	'../gfx/cms/icons/spam.png';
$icon['blocked']				=	'../gfx/cms/icons/blocked.png';
$icon['backup']					=	'../gfx/cms/icons/backup.png';
$icon['help']					=	'../gfx/cms/icons/help.png';
$icon['mailer']					=	'../gfx/cms/icons/mailer.png';
$icon['warning']				=	'../gfx/cms/icons/warning.png';
$icon['pages']					=	'../gfx/cms/icons/pages.png';
$icon['menu']					=	'../gfx/cms/icons/page_swap.png';
$icon['user_files']				=	'../gfx/cms/icons/user_files.png';
$icon['rss']					=	'../gfx/cms/icons/rss.png';
$icon['calendar']				=	'../gfx/cms/icons/calendar.png';
$icon['polls']					=	'../gfx/cms/icons/polls.png';
$icon['images']					=	'../gfx/cms/icons/images.png';
$icon['folder']					=	'../gfx/cms/icons/folder.png';
$icon['talk']					=	'../gfx/cms/icons/talk.png';
$icon['user_comment']			=	'../gfx/cms/icons/user_comment.png';
$icon['people']					=	'../gfx/cms/icons/people.png';
$icon['newsletter']				=	'../gfx/cms/icons/newsletter.png';
$icon['he_users']				=	'../gfx/cms/icons/he_users.png';
$icon['people']					=	'../gfx/cms/icons/people.png';
$icon['folder_full']			=	'../gfx/cms/icons/folder_full.png';
$icon['extranet_file_cats']		=	'../gfx/cms/icons/folder.png';
$icon['folder']					=	'../gfx/cms/icons/folder.png';
$icon['note_list']				=	'../gfx/cms/icons/note_list.png';
$icon['products']				=	'../gfx/cms/icons/products.png';
$icon['calculator']				=	'../gfx/cms/icons/calculator.png';
$icon['cart']					=	'../gfx/cms/icons/cart.png';
$icon['she_users']				=	'../gfx/cms/icons/she_users.png';
$icon['invoices']				=	'../gfx/cms/icons/invoices.png';
$icon['clock']					=	'../gfx/cms/icons/clock.png';
$icon['stats']					=	'../gfx/cms/icons/stats.png';
$icon['computers']				=	'../gfx/cms/icons/computers.png';
$icon['helpdesk']				=	'../gfx/cms/icons/helpdesk.png';
$icon['comment']				=	'../gfx/cms/icons/comment.png';
$icon['film']					=	'../gfx/cms/icons/film.png';
$icon['fav']					=	'../gfx/cms/icons/fav.png';
$icon['home']					=	'../gfx/cms/icons/home.png';
$icon['comments']				=	'../gfx/cms/icons/comments.png';
$icon['search']					=	'../gfx/cms/icons/search.png';
$icon['applications']			=	'../gfx/cms/icons/applications.png';
?>