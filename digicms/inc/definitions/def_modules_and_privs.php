<?php
/*
Title:		Module and privillege definitions
File: 		inc/definitions/def_modules_and_privs.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Install module id definitions
define('MOD_SETTINGS',			1);
define('MOD_AUTH_USERS',		2);
define('MOD_PAGE_CONTENTS',		3);
define('MOD_BAD_WORDS',			4);
define('MOD_BLOCKED_IPS',		5);
define('MOD_BACKUP',			6);
define('MOD_PAGES',				7);
define('MOD_NEWS',				8);
define('MOD_MENU',				9);
define('MOD_FILES',				10);
define('MOD_POLLS',				11);
define('MOD_PHOTO_ALBUM',		12);
define('MOD_GUESTBOOK',			13);
define('MOD_NEWSLETTER_SUBSCR',	14);
define('MOD_NEWSLETTERS',		15);
define('MOD_CUSTOMER_QUOTES',	16);
define('MOD_AGENDA',			17);
define('MOD_CALENDAR',			18);
define('MOD_BANNERS',			19);

define('MOD_CUSTOMERS_ADM',		20);
define('MOD_PROJECTS',			21);
define('MOD_INVOICES',			23);
define('MOD_ARTICLES',			24);
define('MOD_HOURS',				25);
define('MOD_QUOTES',			26);
define('MOD_LEADS',				27);
define('MOD_STATISTICS',		28);
define('MOD_TICKETS',			29);

define('MOD_CUSTOMERS_SHOP',	30);
define('MOD_PRODUCT_CATEGORIES',31);
define('MOD_PRODUCTS',			32);
define('MOD_ORDERS',			33);
define('MOD_DELIVERY_COSTS',	34);
define('MOD_AFFILIATES',		35);

define('MOD_YOUTUBE',			40);
define('MOD_GOOGLE_MAPS',		41);
define('MOD_BLOG',				42);
define('MOD_GOOGLE_ROUTES',		44);

define('MOD_EXTRANET',			50);
define('MOD_EXTRANET_FILES',	51);
define('MOD_EXTRANET_BASIC',	52);

define('MOD_DISCOUNT_CODES',	60);

define('MOD_VACANCIES',			75);
define('MOD_LINKS',				76);
define('MOD_SITE_FAQ',			78);

define('MOD_FAQ',				90);
define('MOD_FILESHARING',		91);
define('MOD_SITE_STATISTICS',	92);
define('MOD_MAILER',			98);
define('MOD_AUTO_UPDATE',		99);

define('MOD_CF',				100);
define('MOD_TAF',				101);
define('MOD_SEARCH',			102);
define('MOD_SITEMAP',			103);

$modules = array (
	MOD_SETTINGS		=>	array(
									MOD_NAME_SETTINGS, 				// Module name
									array('cms_settings.php'),		// Required files, first one is main file
									array(                          // Privs
											0	=> 0,
									        100	=> 'PRIV_CHANGE_USER',
											101	=> 'PRIV_SETTINGS'
									),
									'settings',                	 	// SQL file for installation
									false,							// Calender support
									1,								// Default install 1 or 0
									'computer'						// Icon
								),

	MOD_FAQ		 		=>	array(
									MOD_NAME_FAQ,
									array('cms_faq.php'),
									array(),
									'faq_topics',
									false,
									1,
									'help'
								),

	MOD_AUTH_USERS		=>	array(
									MOD_NAME_AUTH_USERS,
									array('cms_auth_users.php'),
									array(
											110 => 'PRIV_AUTH_USERS',
											111	=> 'PRIV_VIEW_LOGS',
											112 => 'PRIV_PURGE_LOGS'
									),
									'auth_users',
									false,
									1,
									'auth_users'
								),

	MOD_PAGE_CONTENTS	=>	array(
									MOD_NAME_PAGE_CONTENTS,
									array('cms_page_contents.php'),
									array(
									        220	=> 'PRIV_TEXTS'
									),
									'page_contents',
									false,
									1,
									'page_contents'
								),

	MOD_BAD_WORDS		=>	array(
									MOD_NAME_BAD_WORDS,
									array('cms_bad_words.php'),
									array(
											140	=> 'PRIV_SPAM_WORDS'
									),
									'bad_words',
									false,
									1,
									'spam'
								),

	MOD_BLOCKED_IPS		=>	array(
									MOD_NAME_BLOCKED_IPS,
									array('cms_blocked_ips.php'),
									array(
									        150	=> 'PRIV_BLOCKED_IPS'
									),
									'blocked_ips',
									false,
									1,
									'blocked'
								),

	MOD_BACKUP			=>	array(
									MOD_NAME_BACKUP,
									array('cms_backup.php'),
									array(
									        120	=> 'PRIV_BACKUP',
											121	=> 'PRIV_RESTORE'
									),
									'backup',
									false,
									1,
									'backup'
								),

	MOD_MAILER		 	=>	array(
									MOD_NAME_MAILER,
									array('cms_mailer.php'),
									array(
											990 => 'PRIV_MAILER',
											991 => 'PRIV_DELETE_EMAILS'
									),
									'mailer',
									false,
									1,
									'mailer'
								),

	MOD_AUTO_UPDATE => array(
	                              	MOD_NAME_AUTO_UPDATE,
	                              	array('cms_auto_update.php'),
	                              	array(
											995	=> 'PRIV_AUTO_UPDATE'
									),
									'',
									false,
									1,
									'warning'
								),

	MOD_PAGES			=>	array(
	                             	array(MOD_NAME_PAGES,MOD_NAME_PAGE_CATS),
	                             	array('cms_pages.php','cms_page_categories.php'),
	                             	array(
									        200	=> 'PRIV_PAGES',
									        201	=> 'PRIV_ADD_PAGES',
									        202	=> 'PRIV_EDIT_PAGES',
									        203	=> 'PRIV_DEL_PAGES',
									        204	=> 'PRIV_PUB_PAGES',
									        205	=> 'PRIV_LOCK_PAGES',
									        206 => 'PRIV_AUTH_PAGES',
									        207	=> 'PRIV_PAGE_CATS'
									),
									'pages',
									false,
									0,
									'pages'
								),

	MOD_MENU			=>	array(
	                                MOD_NAME_MENU,
	                                array('cms_menu.php'),
	                                array(
									        210	=> 'PRIV_MENU'
									),
									'menu',
									false,
									0,
									'menu'
								),

	MOD_FILES			=>	array(
	                              	MOD_NAME_FILES,
	                              	array('cms_upload_files.php'),
	                              	array(
									        230	=> 'PRIV_FILES'
									),
									'user_files',
									false,
									0,
									'user_files'
								),

	MOD_NEWS			=>	array(
									MOD_NAME_NEWS,
	                             	array('cms_news.php'),
	                             	array(
									        240	=> 'PRIV_NEWS',
									        241	=> 'PRIV_NEWS_RSS'
									),
									'news_topics',
									true,
									0,
									'rss'
								),


	MOD_BLOG			=>	array(
									'Blog artikelen',
	                             	array('cms_blog.php'),
	                             	array(
									        600	=> 'PRIV_BLOG',
									        601	=> 'PRIV_BLOG_RSS'
									),
									'blog_topics',
									true,
									0,
									'rss'
								),

	MOD_AGENDA			=>	array(
	                            	MOD_NAME_AGENDA,
	                            	array('cms_agenda.php'),
	                            	array(
											245	=> 'PRIV_AGENDA'
									),
									'agenda',
									true,
									0,
									'calendar'
								),

	MOD_CALENDAR		=>	array(
	                            	MOD_NAME_CALENDAR,
	                            	array('cms_agenda.php'),
	                            	array(
											246	=> 'PRIV_CALENDAR'
									),
									'calendar',
									true,
									0,
									'calendar'
								),

	MOD_POLLS				=>	array(
	                              	MOD_NAME_POLLS,
	                              	array('cms_polls.php'),
	                              	array(
									        250	=> 'PRIV_POLLS'
									),
									'polls',
									false,
									0,
									'polls'
								),

	MOD_PHOTO_ALBUM			=>	array(
	                              	array(MOD_NAME_PHOTO_ALBUMS, MOD_NAME_PHOTO_ALBUM_CATS),
	                              	array('cms_photo_albums.php','cms_photo_album_cats.php'),
	                              	array(
									        260	=> 'PRIV_PHOTO_ALBUMS'
									),
									'photo_albums',
									true,
									0,
									'images'
								),

	MOD_BANNERS				=>	array(
	                              	MOD_NAME_BANNERS,
	                              	array('cms_banners.php'),
	                              	array(
									        265	=> 'PRIV_BANNERS'
									),
									'banners',
									false,
									0,
									'images'
								),

	MOD_GUESTBOOK			=>	array(
	                            	MOD_NAME_GUESTBOOK,
	                            	array('cms_guestbook.php'),
	                            	array(
											270	=> 'PRIV_GUESTBOOK'
									),
									'guestbook',
									false,
									0,
									'talk'
								),

	MOD_CUSTOMER_QUOTES		=>	array(
									MOD_NAME_CUSTOMER_QUOTES,
									array('cms_customer_quotes.php'),
									array(
									        275	=> 'PRIV_CUSTOMER_QUOTES'
									),
									'customer_quotes',
									false,
									0,
									'user_comment'
								),

	MOD_NEWSLETTER_SUBSCR	=>	array(
									MOD_NAME_NEWSLETTER_SUBSCR,
									array('cms_newsletter_subscribers.php'),
									array(
											280 => 'PRIV_NB_SUBSCR'
									),
									'newsletter_subscribers',
									false,
									0,
									'people'
								),

	MOD_NEWSLETTERS	=>	array(
									MOD_NAME_NEWSLETTERS,
									array('cms_newsletters.php'),
									array(
											290 => 'PRIV_NB',
											291 => 'PRIV_NB_SEND'
									),
									'newsletters',
									false,
									0,
									'newsletter'
								),

	MOD_EXTRANET => array(
									array(MOD_NAME_EXTRANET_USERS, MOD_NAME_EXTRANET_USER_GROUPS),
									array('cms_extranet_users.php','cms_extranet_user_groups.php'),
									array(
											300 => 'PRIV_EXTRANET'
									),
									'extranet_users',
									true,
									0,
									'he_users'
								),

	MOD_EXTRANET_FILES => array(
									array(MOD_NAME_EXTRANET_FILES, MOD_NAME_EXTRANET_FILE_CATS),
									array('cms_extranet_files.php','cms_extranet_file_cats.php'),
									array(
											310 => 'PRIV_EXTRANET_FILES'
									),
									'extranet_files',
									false,
									0,
									'folder_full'
								),

	MOD_EXTRANET_BASIC => array(
									array(MOD_NAME_EXTRANET_USERS),
									array('cms_extranet_basic.php'),
									array(
											300 => 'PRIV_EXTRANET'
									),
									'extranet_basic',
									true,
									0,
									'he_users'
								),

	MOD_VACANCIES => array(
									array(MOD_NAME_VACANCIES),
									array('cms_vacancies.php'),
									array(
											620 => 'PRIV_VACANCIES'
									),
									'vacancies',
									false,
									0,
									'he_users'
								),

	MOD_LINKS => array(
									array(MOD_NAME_LINKS),
									array('cms_links.php'),
									array(
											640 => 'PRIV_LINKS'
									),
									'links',
									false,
									0,
									'fav'
								),

	MOD_SITE_FAQ => array(
									array(MOD_NAME_SITE_FAQ),
									array('cms_site_faq.php'),
									array(
											660 => 'PRIV_SITE_FAQ'
									),
									'site_faq',
									false,
									0,
									'help'
								),

	MOD_YOUTUBE => array(
									array(MOD_NAME_YOUTUBE),
									array('cms_youtube.php'),
									array(
											320 => 'PRIV_YOUTUBE'
									),
									'youtube',
									false,
									0,
									'film'
								),

	MOD_GOOGLE_MAPS	=>	array(
	                              	array(MOD_NAME_GOOGLE_MAPS),
	                              	array('cms_google_maps.php'),
	                              	array(
									        330	=> 'PRIV_GOOGLE_MAPS'
									),
									'google_maps',
									false,
									0,
									'search'
								),

	MOD_GOOGLE_ROUTES		=>	array(
	                              	array(MOD_NAME_GOOGLE_ROUTES),
	                              	array('cms_google_routes.php'),
	                              	array(
									        350	=> 'PRIV_GOOGLE_ROUTES'
									),
									'google_routes',
									false,
									0,
									'search'
								),

	MOD_CUSTOMERS_SHOP	=>	array(
	                                MOD_NAME_CUSTOMERS_SHOP,
	                                array('cms_customers_shop.php'),
	                                array(
									        400	=> 'PRIV_CUSTOMERS',
									        401 => 'PRIV_ADD_CUSTOMERS',
									        402 => 'PRIV_EDIT_CUSTOMERS',
									        403 => 'PRIV_DELETE_CUSTOMERS'
									),
									'customers_shop',
									true,
									0,
									'people'
								),

	MOD_PRODUCT_CATEGORIES 	=>	array(
	                            	MOD_NAME_CATS_SIMPLE,
	                            	array('cms_product_categories.php'),
	                            	array(
									      	410	=> 'PRIV_PRODUCT_CATS',
									      	411 => 'PRIV_DELETE_PRODUCT_CATS'
									),
									'product_categories',
									false,
									0,
									'folder'
								),

	MOD_PRODUCTS 	=> 	array (
	                            	MOD_NAME_PRODUCTS,
	                            	array('cms_products.php'),
	                            	array(
									      	420	=> 'PRIV_PRODUCTS',
									      	421	=> 'PRIV_ADD_PRODUCTS',
									      	422 => 'PRIV_EDIT_PRODUCTS',
									      	423 => 'PRIV_DELETE_PRODUCTS'
									),
									'products_simple',
									false,
									0,
									'products'
								),

	MOD_ORDERS => array(
	                            	array(MOD_NAME_ORDERS, MOD_NAME_ORDER_STATUSSES, MOD_NAME_PDF_INVOICE),
	                            	array('cms_orders.php','cms_order_statusses.php','cms_invoice_shop_pdf.php'),
	                            	array(
									      	440	=> 'PRIV_ORDERS'
									),
									'orders',
									false,
									0,
									'cart'
								),

	MOD_DELIVERY_COSTS =>	array(
	                            	array(MOD_NAME_DELIVERY_COSTS, MOD_NAME_COUNTRIES),
	                            	array('cms_delivery_costs.php','cms_countries.php'),
	                            	array(
									      	430	=> 'PRIV_DELIVERY_COSTS'
									),
									'delivery_costs',
									false,
									0,
									'calculator'
								),

	MOD_DISCOUNT_CODES 	=> 	array (
                            	MOD_NAME_DISCOUNT_CODES,
                            	array('cms_discount_codes.php'),
                            	array(
								      	600	=> 'PRIV_DISCOUNT_CODES'
								),
								'discount_codes',
								true,
								0,
								'invoices'
							),

	MOD_AFFILIATES =>	array(
	                            	MOD_NAME_AFFILIATES,
	                            	array('cms_affiliates.php'),
	                            	array(
									      	450	=> 'PRIV_AFFILIATES',
									      	451 => 'PRIV_ADD_AFFILIATES',
									        452 => 'PRIV_EDIT_AFFILIATES',
									        453 => 'PRIV_DELETE_AFFILIATES'
									),
									'affiliates',
									false,
									0,
									'she_users'
								),

	MOD_CUSTOMERS_ADM	=>	array(
	                                MOD_NAME_CUSTOMERS_ADM,
	                                array('cms_customers_adm.php','cms_rates.php'),
	                                array(
									        400	=> 'PRIV_CUSTOMERS',
									        401 => 'PRIV_ADD_CUSTOMERS',
									        402 => 'PRIV_EDIT_CUSTOMERS',
									        403 => 'PRIV_DELETE_CUSTOMERS',
									        405 => 'PRIV_RATES',
									        406	=> 'PRIV_DELETE_CUSTOMER_EVENTS'
									),
									'customers_adm',
									true,
									0,
									'people'
								),

	MOD_LEADS	=>	array(
	                                MOD_NAME_LEADS,
	                                array('cms_leads.php'),
	                                array(
									        570	=> 'PRIV_LEADS',
									        571 => 'PRIV_ADD_LEADS',
									        572 => 'PRIV_EDIT_LEADS',
									        573 => 'PRIV_DELETE_LEADS'
									),
									'leads',
									true,
									0,
									'people'
								),

	MOD_PROJECTS		=>	array(
	                                array(MOD_NAME_PROJECTS, MOD_NAME_EXTRA_PROJECTS),
	                                array('cms_projects.php','cms_extra_projects.php'),
	                                array(
									        500	=> 'PRIV_PROJECTS',
									        501 => 'PRIV_ADD_PROJECTS',
									        502 => 'PRIV_EDIT_PROJECTS',
									        503 => 'PRIV_DELETE_PROJECTS',
									        504 => 'PRIV_CLOSE_PROJECTS',
									        505 => 'PRIV_EXTRA_PROJECTS',
									        506	=> 'PRIV_ALL_PROJECTS'
									),
									'projects',
									true,
									0,
									'folder_full'
								),

	MOD_INVOICES		=>	array(
	                                array(MOD_NAME_INVOICES, MOD_NAME_INVOICE_LISTS, MOD_NAME_PDF_INVOICE),
	                                array('cms_invoices.php','cms_invoice_lists.php','cms_invoice_pdf.php'),
	                                array(
									        510	=> 'PRIV_INVOICES',
											511 => 'PRIV_ADD_INVOICES',
											512 => 'PRIV_EDIT_INVOICES',
									        513 => 'PRIV_DELETE_INVOICES',
									        514 => 'PRIV_SET_PAID_INVOICES',
									        515	=> 'PRIV_INVOICE_LISTS'
									),
									'invoices',
									true,
									0,
									'invoices'
								),

	MOD_ARTICLES		=>	array(
	                                array(MOD_NAME_ARTICLES, MOD_NAME_ARTICLE_CATS, MOD_NAME_ARTICLE_UNITS),
	                                array('cms_articles.php', 'cms_article_categories.php', 'cms_article_units.php'),
	                                array(
									        520	=> 'PRIV_ARTICLES',
									        525 => 'PRIV_ARTICLE_UNITS',
									        526 => 'PRIV_ARTICLE_CATS'
									),
									'articles',
									true,
									0,
									'products'
								),

	MOD_HOURS			=>	array(
	                                array(MOD_NAME_HOURS, MOD_NAME_HOURS_OVERVIEW, MOD_NAME_HOURS_TOTALS),
	                                array('cms_hours.php', 'cms_hours_summary.php', 'cms_hours_totals.php'),
	                                array(
									        530	=> 'PRIV_HOURS',
									        531 => 'PRIV_EDIT_OWN_HOURS',
									        532 => 'PRIV_EDIT_ALL_HOURS',
									        533 => 'PRIV_VIEW_ALL_HOURS',
									        534 => 'PRIV_CHARGED_HOURS'
									),
									'hours',
									true,
									0,
									'clock'
								),

	MOD_QUOTES			=>	array(
	                                array(MOD_NAME_QUOTES, MOD_NAME_QUOTE_TEXTS),
	                                array('cms_quotes.php','cms_quote_texts.php'),
	                                array(
									        540	=> 'PRIV_QUOTES',
											541 => 'PRIV_ADD_QUOTES',
											542 => 'PRIV_EDIT_QUOTES',
									        543 => 'PRIV_DELETE_QUOTES',
									        545	=> 'PRIV_QUOTE_TEXTS'
									),
									'quotes',
									false,
									0,
									'invoices'
								),

	MOD_STATISTICS   	=>	array(
									array(MOD_NAME_STATISTICS,MOD_NAME_STATS_PER_CUSTOMER),
									array('cms_statistics.php', 'cms_statistics_customer.php'),
									array(
											560 => 'PRIV_STATS'
									),
									'',
									true,
									0,
									'stats'
								),

	MOD_TICKETS   	=>	array(
									array(MOD_NAME_TICKETS, MOD_NAME_TICKET_TYPES),
									array('cms_tickets.php', 'cms_ticket_types.php'),
									array(
											550	=> 'PRIV_TICKETS',
											551 => 'PRIV_ADD_TICKETS',
											552 => 'PRIV_EDIT_TICKETS',
									        553 => 'PRIV_DELETE_TICKETS'
									),
									'tickets',
									true,
									0,
									'note_list'
								),

	MOD_SITE_STATISTICS =>	array(
	                              	MOD_NAME_SITE_STATS,
	                              	array('cms_site_statistics.php'),
	                              	array(
											800	=> 'PRIV_SITE_STATS'
									),
									'site_statistics',
									false,
									0,
									'stats'
								),

	MOD_CF =>	array(
	                              	MOD_NAME_CF,
	                              	array(),
	                              	array(),
									'contact_form',
									false,
									0,
									''
								),

	MOD_TAF =>	array(
	                              	MOD_NAME_TAF,
	                              	array(),
	                              	array(),
									'tell_a_friend',
									false,
									0,
									''
								),

	MOD_SEARCH =>	array(
	                              	MOD_NAME_SEARCH,
	                              	array(),
	                              	array(),
									'search',
									false,
									0,
									''
								),

	MOD_SITEMAP =>	array(
	                              	MOD_NAME_SITEMAP,
	                              	array(),
	                              	array(),
									'sitemap',
									false,
									0,
									''
								)
				);

?>