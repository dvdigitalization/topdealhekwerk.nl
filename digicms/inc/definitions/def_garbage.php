<?php
/*
Title:		Garbage collection settings
File: 		inc/definitions/def_garbage.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Tables to check for garbage collections (they are dependent on the installed modules)
$gc_tables 		= array();
$gc_tables[] 	= 'auth_users';

if (isset($installed_modules[MOD_TICKETS]))	{
	$gc_tables[]	=	'ticket_types';
	$gc_tables[]	=	'customers_adm';
}		
if (isset($installed_modules[MOD_HOURS])) {			
	$gc_tables[]	=	'extra_projects';
	$gc_tables[]	=	'projects';
}
if (isset($installed_modules[MOD_ARTICLES])) {
	$gc_tables[]	=	'article_categories';
	$gc_tables[]	=	'article_units';
	$gc_tables[]	=	'vat_levels';
}
if (isset($installed_modules[MOD_INVOICES])) {
	$gc_tables[]	=	'projects';
	$gc_tables[]	=	'customers_adm';	
}
if (isset($installed_modules[MOD_PROJECTS])) {
	$gc_tables[]	=	'customers_adm';	
}

$gc_tables = array_unique($gc_tables);

// *** Dependent tables and their fields, for each of the above tables
$gc_dependencies = array(
	'ticket_types'			=>	array(	'tickets'		=>	'ticket_type'),
	'extra_projects'		=>	array(	'hours'			=>	'extra_project_id'),
	'article_categories'	=>	array(	'articles'		=>	'category_id'),
	'article_units'			=>	array(	'articles'		=>	'unit'),
	'vat_levels'			=>	array(	'articles'		=>	'vat_level'),
	'projects'				=>	array(	'hours'			=>	'project_id',
										'administrative_relations'	=>	'project_id'),
	'customers_adm'			=>	array(	'projects'		=>	'customer_id',
										'invoices'		=>	'customer_id',
										'tickets'		=>	'customer_id'),
);

if (isset($installed_modules[MOD_HOURS])) {
	$gc_dependencies['auth_users'] = array(	'hours'		=>	'employee_id');
}

?>