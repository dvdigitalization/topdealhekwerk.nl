<?php
/*
Title:		Generate language selectors
File: 		inc/definitions/def_lang_selectors.php
Version: 	v2.04
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

$lang_selectors = array();
foreach ($installed_languages as $language) {
	$lang_selectors[] = ($language == $_lang) ? $language : "<a href='$_file?lang=$language'>$language</a>";
}
$lang_selectors = '&nbsp; ('.implode(' - ', $lang_selectors).')';

?>