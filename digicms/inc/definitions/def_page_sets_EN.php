<?php
/*
Title:		Page set language definitions [EN]
File: 		inc/definitions/def_page_sets_EN.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Categories
$page_set_names['_general']				=	'General';
$page_set_names['agenda']				=	'Agenda';
$page_set_names['calendar']				=	'Calendar';
$page_set_names['blog']					=	'Blog';
$page_set_names['spam']					=	'SPAM filter';
$page_set_names['contact']				=	'Contactform';
$page_set_names['news']					=	'News';
$page_set_names['news_comments']		=	'Comments on news topics';
$page_set_names['newsletter']			=	'Newsletter';
$page_set_names['photos']				=	'Photo albums';
$page_set_names['polls']				=	'Polls';
$page_set_names['search']				=	'Search';
$page_set_names['tell-a-friend']		=	'Tell-a-friend';
$page_set_names['cart']					= 	'Shoppingcart';
$page_set_names['checkout']				=	'Checkout';
$page_set_names['payment']				=	'Payment';
$page_set_names['login']				=	'Login';
$page_set_names['registration']			=	'Registration';
$page_set_names['guestbook']			=	'Guestbook';
$page_set_names['google_maps']			=	'Google maps';
$page_set_names['invoice']				=	'Invoice';
$page_set_names['vacancies']			=	'Vacancies';

?>