<?php
/*
Title:		Configuration settings and setting set language definitions [NL]
File: 		inc/definitions/def_general_settings_NL.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Categories
$setting_set['_general']				=	'Algemeen';
$setting_set['CMS']						=	'CMS';
$setting_set['agenda']					=	'Agenda';
$setting_set['backup']					=	'Backup';
$setting_set['blog']					=	'Blog';
$setting_set['calendar']				=	'Kalender';
$setting_set['company']					=	'Bedrijfsgegevens';
$setting_set['contact']					=	'Contactformulier';
$setting_set['customers']				=	'Klanten';
$setting_set['extranet']				=	'Extranet';
$setting_set['google_maps']				=	'Google Maps';
$setting_set['invoices']				=	'Facturen';
$setting_set['mailer']					=	'Mailing';
$setting_set['menu']					=	'Menu';
$setting_set['news']					=	'Nieuwsberichten';
$setting_set['orders']					=	'Bestellingen';
$setting_set['pages']					=	'Website pagina\'s';
$setting_set['photos']					=	'Foto albums';
$setting_set['products']				=	'Producten';
$setting_set['product_cats']			=	'Product categorieen';
$setting_set['projects']				=	'Projecten';
$setting_set['site_stats']				=	'Website statistieken';
$setting_set['timer']					=	'Timer';
$setting_set['user_files']				=	'Website bestanden';
$setting_set['sitemap']					=	'Sitemap';

// *** General
$setting['ITEMS_PER_PAGE']				=	'Het aantal elementen wat per pagina wordt weergegeven';
$setting['MAX_PAGES_IN_LIST']			=	'Het maximaal aantal paginas in de lijst';
$setting['SHOW_FAQ_LINKS']				=	'Help links tonen binnen het CMS';
$setting['ADMIN_NAME']					=	'Website beheerder naam';
$setting['ADMIN_EMAIL']					=	'Website beheerder email adres';
$setting['SITE_EMAIL_ADDRESS']			=	'Afzender adres voor emails vanaf de website';
$setting['SITE_EMAIL_NAME']				=	'Afzender naam voor email vanaf de website';
$setting['DISK_SPACE']					=	'Totale schijfruimte beschikbaar (in MB)';
$setting['DISK_SPACE_WARN']				=	'Waarschuwen bij weinig schijfruimte';
$setting['DISK_SPACE_LAST_CHECKED']		=	'Laatste schijfruimte check timestamp';
$setting['CHECK_FOR_UPDATES']			=	'Automatisch checken voor nieuwe updates';
$setting['UPDATE_CHECK_FREQUENCY']		=	'Check voor updates om de zoveel dagen';
$setting['ACTIVITY_LOGGING']			=	'Activiteiten van gebruikers binnen het CMS bijhouden';
$setting['IN_MAINTENANCE']				=	'Website in onderhoud';
$setting['MAINTENANCE_END_TIME']		=	'Website in onderhoud tot';

// *** CMS
$setting['GARBAGE_LAST_COLLECTED']		=	'Garbage collection timestamp';
$setting['GARBAGE_COLLECT_INTERVAL']	=	'Garbage collection interval';
$setting['FIRST_LOGIN_PASS_CHANGED']	=	'Wachtwoord gewijzigd na eerste login';
$setting['SUPRESS_SAFE_MODE_WARNING']	=	'Safe_mode() waarschuwing onderdrukken';
$setting['SUPRESS_REG_GLOB_WARNING']	=	'Register_globals() waarschuwing onderdrukken';
$setting['ALLOW_COOKIE_LOGIN']			=	'Cookie login toestaan';
$setting['COOKIE_EXPIRE_TIME']			=	'Cookie verlooptijd in dagen';
$setting['INSTALLED_MODULES']			=	'Geinstalleerde CMS modules';
$setting['INSTALLED_LANGUAGES']			=	'Geinstalleerde talen';
$setting['REGISTRATION_NAME']			=	'CMS registratie naam';
$setting['CMS_REGISTRATION_ID']			=	'CMS registratie ID';
$setting['TECH_NAME']					=	'Technische dienst naam';
$setting['TECH_EMAIL']					=	'Technische dienst email adres';
$setting['TIMEZONE']					=	'Tijdzone (standaard Europe/Amsterdam)';
$setting['DIGI_ERROR_HANDLING']			=	'Foutmeldingen afhandelen met de Digi error handler';
$setting['SHOW_ERRORS']					=	'Foutmelding berichten tonen';
$setting['ERROR_MAILING']				=	'Foutmeldingen per email sturen';
$setting['DIGI_ALERT_EMAIL']			=	'Foutmeldingen email adres';
$setting['ERROR_LOGGING']				=	'Foutmeldingen in een log bestand opslaan';
$setting['IGNORE_NOTICE_ERRORS']		=	'Foutmeldingen van het type notices negeren';
$setting['CMS_VERSION_MAJOR']			=	'Versie (major)';
$setting['CMS_VERSION_MINOR']			=	'Versie (minor)';
$setting['CMS_VERSION_REVISION']		=	'Versie (revisie)';
$setting['LAST_CHECK_IN']				=	'Laatste check-in';
$setting['LAST_UPDATE_CHECK']			=	'Update check timestamp';
$setting['UPDATE_AVAILABLE']			=	'Update beschikbaar';
$setting['UPDATE_TITLE']				=	'Update notice/titel';
$setting['USE_REWRITE_ENGINE']			=	'Rewrite Engine gebruiken';
$setting['UPDATE_FTP_DEST_FOLDER']		=	'Auto-update FTP bestemmings map';
$setting['UPDATE_FTP_SERVER']			=	'Auto-update FTP server';
$setting['UPDATE_FTP_USER']				=	'Auto-update FTP gebruikersnaam';
$setting['UPDATE_FTP_PASSWORD']			=	'Auto-update FTP wachtwoord';
$setting['UPDATE_FTP_REMEMBER']			=	'Auto-update FTP wachtwoord opslaan';

// *** Agenda / calendar
$setting['AGENDA_PHOTO_SIZE']			=	'Maximale afmeting voor afbeeldingen (pixels)';
$setting['AGENDA_THUMB_SIZE']			=	'Thumbnail afmeting (pixels)';
$setting['AGENDA_PHOTO_CONSTRAINT']		=	'Afbeeldingen constraint';
$setting['AGENDA_THUMB_CONSTRAINT']		=	'Thumbnails constraint';
$setting['AGENDA_SHOW_IMAGE']			=	'Afbeelding optie tonen';
$setting['AGENDA_SHOW_PREVIEW']			=	'Preview optie tonen';

// *** Backup
$setting['MAX_AMOUNT_OF_DUMPS']			=	'Maximaal aantal backup punten dat er bewaard worden';
$setting['LAST_DUMP']					=	'Laatste backup punt timestamp';
$setting['DUMP_INTERVAL']				=	'Automatische backup wordt gemaakt om de zoveel dagen';
$setting['MAKE_AUTO_BACKUPS']			=	'Automatisch backup punten aanmaken';
$setting['EMAIL_BACKUP_FILES']			=	'Backup bestanden per email verzenden';
$setting['EMAIL_BACKUPS_MAX_SIZE']		=	'Maximale grootte bij backup bestanden mailen in bytes';
$setting['EMAIL_BACKUPS_ADDRESS']		=	'Email adres om backups naartoe te zenden';

// *** Blog
$setting['BLOG_PHOTO_SIZE']				=	'Maximale afmeting voor afbeeldingen (pixels)';
$setting['BLOG_THUMB_SIZE']				=	'Thumbnail afmeting (pixels)';
$setting['BLOG_PHOTO_CONSTRAINT']		=	'Afbeeldingen constraint';
$setting['BLOG_THUMB_CONSTRAINT']		=	'Thumbnails constraint';
$setting['BLOG_SHOW_IMAGE']				=	'Afbeelding optie tonen';
$setting['BLOG_SHOW_PREVIEW']			=	'Preview optie tonen';
$setting['BLOG_RSS_ENABLED']			=	'RSS nieuwsfeed actief';

// *** Company info
$setting['COMPANY_VAT_NO']				=	'BTW nummer';
$setting['COMPANY_KVK_NAME']			=	'KvK regio';
$setting['COMPANY_KVK_NO']				=	'KvK nummer';
$setting['COMPANY_BANK_NAME']			=	'Bank naam';
$setting['COMPANY_BANK_NO']				=	'Bankrekening nummer';
$setting['COMPANY_TEL']					=	'Telefoon nummer';
$setting['COMPANY_EMAIL']				=	'Email adres';
$setting['COMPANY_WEB']					=	'Website';
$setting['COMPANY_NAME']				=	'Bedrijfsnaam';
$setting['COMPANY_ADDRESS']				=	'Straat en huisnummer';
$setting['COMPANY_PC']					=	'Postcode';
$setting['COMPANY_CITY']				=	'Plaats';

// *** Contact form
$setting['CONTACT_FORM_EMAIL']			=	'Email adres(sen) om ingevulde formulieren naar toe te sturen';

// *** Customers
$setting['CUSTOMER_PREFIX']				=	'Voorvoegsel voor klant nummers';
$setting['CUSTOMER_L_ZEROES']			=	'Aantal cijfers voor klant nummer';
$setting['CUSTOMER_CONST']				=	'Klant nummer constante';

// *** Extranet
$setting['SITE_ALLOW_COOKIE_LOGIN']		=	'Cookie login toestaan (gebruikers onthouden)';
$setting['SITE_COOKIE_EXPIRE_TIME']		=	'Cookies vervallen na zoveel dagen';
$setting['ALLOW_LOGIN_MAINSWITCH']		=	'Inloggen op de site toestaan';
$setting['REG_AUTO_CLEANSE']			=	'Niet gevalideerde registraties automatisch verwijderen';
$setting['REG_CLEANSE_DAYS']			=	'Niet gevalideerde registraties verwijderen na zoveel dagen';
$setting['REG_MIN_PW_LENGTH']			=	'Minimale lengte wachtwoord';
$setting['EXTRANET_FILES_AT_ONCE']		=	'Maximaal aantal bestanden tegelijk up te loaden';
$setting['EXTRANET_NO_SPACE']			=	'Geen spaties in bestandsnamen toestaan';
$setting['EXTRANET_LOWERCASE']			=	'Bestandsnamen naar lower case omzetten';

// *** Google maps
$setting['GOOGLE_MAPS_KEY']				=	'Google maps sleutel voor deze server';

// *** Invoices
$setting['INVOICE_NO_RESET_EACH_YEAR']	=	'Factuur nummers elk jaar vanaf 1 nummeren';
$setting['INVOICE_NO_FORMAT_ADM']		=	'Factuur nummering formaat';
$setting['INVOICE_CONST']				=	'Factuur nummering constante';
$setting['INVOICE_ALERT_TIME']			=	'Factuur betaling is te laat na zoveel dagen';
$setting['INVOICE_LOGO_URL']			=	'Factuur logo URL';

// *** Mailer / newsletters
$setting['MAILER_EMAIL_ADDRESSES']		=	'Email adressen voor de mailer (scheiden met puntkomma)';
$setting['DEFAULT_BCC']					=	'Standaard BCC email adres';
$setting['MAX_PER_BATCH']				=	'Maximaal aantal mailtjes per batch';
$setting['BATCH_INTERVAL']				=	'Batch interval in seconden';
$setting['LAST_BATCH_SENT']				=	'Laatste batch timestamp';
$setting['AUTO_SENDING']				=	'Automatisch email batches sturen';
$setting['USE_SMTP']					=	'SMTP gebruiken voor email verzending';
$setting['SMTP_SERVER']					=	'SMTP server';
$setting['NL_FROM_EMAIL']				=	'Nieuwsbrief afzender email adres';
$setting['NL_DATE_FORMAT']				=	'Nieuwsbrief datum formaat';
$setting['NL_TEST_ADDRESSES']			=	'Nieuwsbrief test email adres(sen)';
$setting['NL_SIGN_OFF_URL']				=	'Nieuwsbrief afmeldpagina';
$setting['NL_DB_TABLES']				=	'Nieuwsbrief aanmeldingen tabellen (scheiden met puntkomma)';

// *** Menu
$setting['MENU_SECTION_OPTIONS']		=	'Menu sectie opties tonen';
$setting['MENU_SUB_OPTIONS']			=	'Menu sub-item opties tonen';
$setting['MENU_IGNORE_WRONG_LINKS']		=	'Incorrecte links negeren';

// *** News
$setting['NEWS_PHOTO_SIZE']				=	'Maximale afmeting voor afbeeldingen (pixels)';
$setting['NEWS_THUMB_SIZE']				=	'Thumbnail afmeting (pixels)';
$setting['NEWS_PHOTO_CONSTRAINT']		=	'Afbeeldingen constraint';
$setting['NEWS_THUMB_CONSTRAINT']		=	'Thumbnails constraint';
$setting['NEWS_SHOW_IMAGE']				=	'Afbeelding optie tonen';
$setting['NEWS_SHOW_PREVIEW']			=	'Preview optie tonen';
$setting['NEWS_RSS_ENABLED']			=	'RSS nieuwsfeed actief';
$setting['NEWS_COMMENTS_ENABLED']		=	'Reacties op nieuwsberichten actief';
$setting['NEWS_REWRITE_PREFIX']			=	'Rewrite engine prefix';

// *** Orders
$setting['ORDER_ID_FORMAT']				=	'Bestellingen ID formaat';
$setting['INVOICE_NO_FORMAT']			=	'Factuur nummers formaat';

// *** Pages
$setting['PAGES_SHOW_IMAGE']			=	'Afbeelding/flash optie tonen';
$setting['PAGES_SHOW_META']				=	'Meta tags optie tonen';
$setting['PAGES_SHOW_TEMPLATE']			=	'Template optie tonen';
$setting['PAGES_SHOW_HTML_NAME']		=	'HTML naam optie tonen';
$setting['PAGES_SHOW_FILE']				=	'Bestand optie tonen';
$setting['PAGES_SHOW_AUTH']				=	'Beveiligings opties tonen';
$setting['PAGES_SHOW_FREE']				=	'Vrij veld optie tonen';
$setting['PAGEIMAGE_SIZE']				=	'Maximale afmeting voor afbeeldingen (pixels) i.c.m. verkleinen';
$setting['PAGEIMAGE_CROP_WIDTH']		=	'Vaste breedte voor afbeeldingen (pixels) i.c.m. knippen';
$setting['PAGEIMAGE_CROP_HEIGHT']		=	'Vaste hoogte voor afbeeldingen (pixels) i.c.m. knippen';
$setting['FREE_FIELD_TITLE']			=	'Vrij veld titel';
$setting['FREE_FIELD_EXPLANATION']		=	'Vrij veld uitleg';
$setting['PAGES_HARD_LINK_ML']			=	'Meertalige pagina\'s aan elkaar vastkoppelen';

// *** Photo albums
$setting['USE_EXIF_TIMESTAMP']			=	'EXIF datum en tijd gebruiken voor foto\'s';
$setting['ALB_MAX_FILES_AT_ONCE']		=	'Maximaal aantal foto\'s wat tegelijk kan worden geupload';
$setting['ALB_NO_SPACE_IN_FILENAME']	=	'Geen spaties in bestandsnamen toestaan';
$setting['ALB_FILENAMES_TO_LOWERCASE']	=	'Bestandsnamen naar lowercase omzetten';
$setting['ALB_PHOTO_SIZE']				=	'Standaard foto verklein afmeting (in pixels)';
$setting['ALB_THUMB_SIZE']				=	'Standaard thumbnail afmeting (in pixels)';
$setting['ALB_MAKE_THUMBS']				=	'Thumbnails maken van de foto\'s';
$setting['ALB_RESIZE_PHOTOS']			=	'Foto\'s verkleinen';
$setting['MAX_DUMP_AT_ONCE']			=	'Maximaal aantal foto\'s tegelijk te dumpen';
$setting['ALB_USE_WATERMARK']			=	'Watermerk gebruiken';
$setting['ALB_WATERMARK']				=	'Watermerk text';
$setting['ALB_WATERMARK_COLOR']			=	'Watermerk kleur';
$setting['ALB_WATERMARK_SIZE']			=	'Watermerk formaat';
$setting['SQUARE_THUMBS']				=	'Vierkante thumbnails';

// *** Products
$setting['PROD_IMAGE_SIZE']				=	'Maximale afmeting voor afbeeldingen (pixels)';
$setting['PROD_THUMB_SIZE']				=	'Thumbnail afmeting (pixels)';
$setting['PROD_IMAGE_CONSTRAINT']		=	'Afbeeldingen constraint';
$setting['PROD_THUMB_CONSTRAINT']		=	'Thumbnails constraint';
$setting['PROD_USE_IMAGES']				=	'Afbeeldingen voor producten gebruiken';
$setting['PROD_IMAGE_RESIZE']			=	'Afbeeldingen verkleinen';
$setting['PRODUCTS_STANDARD_VAT']		=	'Standaard BTW tarief voor producten (in %)';
$setting['PROD_USE_SEPARATE_PROPS']		=	'Optie voor aparte naam/beschrijving per product';
$setting['PROD_USE_SEPARATE_PROD_NO']	=	'Optie voor apart product nummer per product';
$setting['PROD_ALLOW_SAME_SPECS']		=	'Zelfde product eigenschappen toestaan binnen een productgroep';
$setting['DISCOUNT_ONLY_ON_NONSALE']	=	'Korting alleen op non-sale artikelen';

// *** Product categories
$setting['PROD_CAT_IMAGE_SIZE']			=	'Maximale afmeting voor afbeeldingen (pixels)';
$setting['PROD_CAT_IMAGE_CONSTRAINT']	=	'Afbeeldingen constraint';
$setting['PROD_CAT_IMAGE_RESIZE']		=	'Afbeeldingen verkleinen';
$setting['PROD_CAT_USE_IMAGES']			=	'Afbeeldingen voor categorieen gebruiken';
$setting['PROD_CAT_ALLOW_SUBCATS']		=	'Subcategorieen toestaan';

// *** Projects
$setting['PROJECT_PREFIX']				=	'Voorvoegsel voor project nummers';
$setting['PROJECT_L_ZEROES']			=	'Aantal cijfers voor project nummer';
$setting['PROJECT_CONST']				=	'Project nummer constante';
$setting['PROJECT_DEADLINE_TIME']		=	'Standaard project deadline tijd (in dagen)';
$setting['PROJECT_DEADLINE_ALERT_DAYS']	=	'Deadline email wordt zoveel dagen voor verlopen verstuurd';
$setting['PROJECT_ALERTS_TIMESTAMP']	=	'Laatste projecten deadline waarschuwing check timestamp';

// *** Sitemap
$setting['SITEMAP_REFRESH_RATE']		=	'Sitemap wordt ververst om de zoveel dagen';
$setting['SITEMAP_LAST_UPDATED']		=	'Laatste sitemap ververs timestamp';

// *** Site statistics
$setting['STATS_SINCE']					=	'Website statistieken worden bijgehouden vanaf deze datum';
$setting['PAGE_HITS_START_VALUE']		=	'Startwaarde voor het aantal page hits';
$setting['UNIQUE_VISITORS_START_VALUE']	=	'Startwaarde voor het aantal unieke bezoekers';
$setting['STATS_UNIQUE_INTERVAL']		=	'Het aantal uur waarna een bezoeker weer als uniek wordt geteld';

// *** Timer
$setting['TIMER_HOURS_PRECISION']		=	'Uren afrondings precisie';
$setting['TIMER_ENABLED']				=	'Timer gebruiken';

// *** User files
$setting['MAX_FILES_AT_ONCE']			=	'Maximaal aantal bestanden tegelijk te uploaden';
$setting['NO_SPACE_IN_FILENAME']		=	'Geen spaties in bestandsnamen toestaan';
$setting['FILENAMES_TO_LOWERCASE']		=	'Bestandsnamen naar lowercase omzetten';
$setting['DEFAULT_IMAGE_RESIZE_SIZE']	=	'Standaard afbeeldingen verklein afmeting (in pixels)';
?>