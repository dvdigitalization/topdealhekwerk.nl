<?php
/*
Title:		Privillege definitions [NL]
File: 		inc/definitions/def_privs_NL.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Settings
$priv_def['PRIV_CHANGE_USER']			=	'Eigen gebruikersnaam en wachtwoord wijzigen';
$priv_def['PRIV_SETTINGS']				=	'Instellingen bewerken';

// *** Auth users
$priv_def['PRIV_AUTH_USERS']			=	'Gebruikers beheren';
$priv_def['PRIV_VIEW_LOGS'] 			=	'Activiteiten log bekijken';
$priv_def['PRIV_PURGE_LOGS']			= 	'Activiteiten log legen';

// *** Page Contents
$priv_def['PRIV_TEXTS']					=	'Website teksten beheren';

// *** Bad words
$priv_def['PRIV_SPAM_WORDS']			=	'SPAM woorden beheren';

// *** Blocked IP's
$priv_def['PRIV_BLOCKED_IPS']			=	'Geblokkeerde IPs beheren';

// *** Backup
$priv_def['PRIV_BACKUP']				=	'Systeem backups maken';
$priv_def['PRIV_RESTORE']				=	'Systeem backups herstellen';

// *** FAQ
$priv_def['PRIV_SITE_FAQ']				=	'FAQ beheren';

// *** Mailer
$priv_def['PRIV_MAILER']				=	'Mailer gebruiken';
$priv_def['PRIV_DELETE_EMAILS']			=	'Emails verwijderen';

// *** Auto-update
$priv_def['PRIV_AUTO_UPDATE']			=	'Auto updater gebruiken';

// *** Pages
$priv_def['PRIV_PAGES']					=	'Website pagina\'s beheren';
$priv_def['PRIV_ADD_PAGES']				=	'Website pagina\'s aanmaken';
$priv_def['PRIV_EDIT_PAGES']			=	'Website pagina\'s bewerken';
$priv_def['PRIV_DEL_PAGES']				=	'Website pagina\'s verwijderen';
$priv_def['PRIV_PUB_PAGES']				=	'Website pagina\'s publiceren';
$priv_def['PRIV_LOCK_PAGES']			=	'Website pagina\'s ver- en ontgrendelen';
$priv_def['PRIV_AUTH_PAGES']			=	'Website pagina\'s met login beveiligen';
$priv_def['PRIV_PAGE_CATS']				=	'Website pagina categorieen beheren';

// *** Menu
$priv_def['PRIV_MENU']					=	'Website menu beheren';

// *** Files
$priv_def['PRIV_FILES']					=	'Website bestanden beheren';

// *** News
$priv_def['PRIV_NEWS']					=	'Nieuwsberichten beheren';
$priv_def['PRIV_NEWS_RSS']				=	'Nieuwsberichten RSS feed beheren';

// *** Blog
$priv_def['PRIV_BLOG']					=	'Blog beheren';
$priv_def['PRIV_BLOG_RSS']				=	'Blog RSS nieuwsfeed beheren';

// *** Agenda
$priv_def['PRIV_AGENDA']				=	'Agenda beheren';

// *** Calendar
$priv_def['PRIV_CALENDAR']				=	'Kalender beheren';

// *** Polls
$priv_def['PRIV_POLLS']					=	'Polls beheren';

// *** Photo album
$priv_def['PRIV_PHOTO_ALBUMS']			=	'Foto albums beheren';

// *** Banners
$priv_def['PRIV_BANNERS']				=	'Banners beheren';

// *** Guestbook
$priv_def['PRIV_GUESTBOOK']				=	'Gastenboek beheren';

// *** Customer quotes
$priv_def['PRIV_CUSTOMER_QUOTES']		=	'Klanten quotes beheren';

// *** Newsletter subscriber
$priv_def['PRIV_NB_SUBSCR']				=	'Nieuwsbrief aanmeldingen beheren';

// *** Newsletter
$priv_def['PRIV_NB']					=	'Nieuwsbrieven beheren';
$priv_def['PRIV_NB_SEND']				=	'Nieuwsbrieven versturen';

// *** Extranet
$priv_def['PRIV_EXTRANET']				=	'Extranet gebruikers beheren';

// *** Extranet files
$priv_def['PRIV_EXTRANET_FILES']		=	'Extranet bestanden beheren';

// *** Extranet basic
$priv_def['PRIV_EXTRANET']				=	'Extranet gebruikers beheren';

// *** Forms
$priv_def['PRIV_FORMS']					=	'Formulieren beheren';

// *** Youtube
$priv_def['PRIV_YOUTUBE']				=	'Youtube video\'s beheren';

// *** Google maps
$priv_def['PRIV_GOOGLE_MAPS']			=	'Google maps beheren';
$priv_def['PRIV_GOOGLE_DIRECTIONS']		=	'Google routebeschrijvingen beheren';
$priv_def['PRIV_GOOGLE_ROUTES'] 		=	'Google routes beheren';

// *** Customer shop
$priv_def['PRIV_CUSTOMERS']				=	'Klanten beheren';
$priv_def['PRIV_ADD_CUSTOMERS']			=	'Klanten toevoegen';
$priv_def['PRIV_EDIT_CUSTOMERS']		=	'Klanten wijzigen';
$priv_def['PRIV_DELETE_CUSTOMERS']		=	'Klanten verwijderen';

// *** Collections
$priv_def['PRIV_COLLECTIONS']			=	'Collecties beheren';
$priv_def['PRIV_DELETE_COLLECTIONS']	=	'Collecties verwijderen';

// *** Products
$priv_def['PRIV_PRODUCTS']				=	'Producten beheren';
$priv_def['PRIV_ADD_PRODUCTS']			=	'Producten toevoegen';
$priv_def['PRIV_EDIT_PRODUCTS']			=	'Producten wijzigen';
$priv_def['PRIV_DELETE_PRODUCTS']		=	'Producten verwijderen';

// *** Product properties
$priv_def['PRIV_PRODUCT_PROPS']			= 	'Product eigenschappen beheren';

// *** Delivery Costs
$priv_def['PRIV_DELIVERY_COSTS']		=	'Verzendkosten beheren';

// *** Orders
$priv_def['PRIV_ORDERS']				=	'Bestellingen beheren';

// *** Affiliates
$priv_def['PRIV_AFFILIATES']			=	'Affiliates beheren';
$priv_def['PRIV_ADD_AFFILIATES']		=	'Affiliates toevoegen';
$priv_def['PRIV_EDIT_AFFILIATES']		=	'Affiliates wijzigen';
$priv_def['PRIV_DELETE_AFFILIATES']		=	'Affiliates verwijderen';

// *** Prod cats simple
$priv_def['PRIV_PRODUCT_CATS']			=	'Productcategorieen beheren';
$priv_def['PRIV_DELETE_PRODUCT_CATS']	=	'Productcategorieen verwijderen';

// *** Products simple
$priv_def['PRIV_PRODUCTS']				=	'Producten beheren';
$priv_def['PRIV_ADD_PRODUCTS']			=	'Producten toevoegen';
$priv_def['PRIV_EDIT_PRODUCTS']			=	'Producten wijzigen';
$priv_def['PRIV_DELETE_PRODUCTS']		=	'Producten verwijderen';

// *** Customer admin
$priv_def['PRIV_CUSTOMERS']				=	'Klanten module gebruiken';
$priv_def['PRIV_ADD_CUSTOMERS']			=	'Klanten toevoegen';
$priv_def['PRIV_EDIT_CUSTOMERS']		=	'Klanten wijzigen';
$priv_def['PRIV_DELETE_CUSTOMERS']		=	'Klanten verwijderen';
$priv_def['PRIV_RATES']					=	'Tarieven beheren';
$priv_def['PRIV_DELETE_CUSTOMER_EVENTS']=	'Tickets bij klanten verwijderen';

// *** Leads
$priv_def['PRIV_LEADS']					=	'Leads module gebruiken';

// *** Vacancies
$priv_def['PRIV_VACANCIES']				=	'Vacatures beheren';

// *** Links
$priv_def['PRIV_LINKS']					= 	'Links beheren';

// *** Contact form data
$priv_def['PRIV_CONTACT_FORM_DATA']		=	'Contactformulieren beheren';

// *** Projects
$priv_def['PRIV_PROJECTS']				=	'Projecten module gebruiken';
$priv_def['PRIV_ADD_PROJECTS']			=	'Projecten aanmaken';
$priv_def['PRIV_EDIT_PROJECTS']			=	'Projecten wijzigen';
$priv_def['PRIV_DELETE_PROJECTS']		=	'Projecten verwijderen';
$priv_def['PRIV_CLOSE_PROJECTS'] 		=	'Projecten afsluiten of heropenen';
$priv_def['PRIV_EXTRA_PROJECTS']		=	'Extra projecten beheren';
$priv_def['PRIV_ALL_PROJECTS']			=	'Alle projecten beheren, verantwoordelijkheid beheren';

// *** Invoices
$priv_def['PRIV_INVOICES']				=	'Facturen module gebruiken';
$priv_def['PRIV_ADD_INVOICES']			=	'Nieuwe factuur aanmaken';
$priv_def['PRIV_EDIT_INVOICES']			=	'Facturen wijzigen';
$priv_def['PRIV_DELETE_INVOICES']		=	'Facturen verwijderen';
$priv_def['PRIV_SET_PAID_INVOICES']		=	'Facturen op betaald zetten';
$priv_def['PRIV_INVOICE_LISTS']			=	'Lijsten voor facturatie beheren';

// *** Articles
$priv_def['PRIV_ARTICLES']				=	'Artikelen beheren';
$priv_def['PRIV_ARTICLE_UNITS']			=	'Artikel eenheden beheren';
$priv_def['PRIV_ARTICLE_CATS']			=	'Artikel categorieen beheren';

// *** Hours
$priv_def['PRIV_HOURS']					=	'Eigen uren inzien';
$priv_def['PRIV_EDIT_OWN_HOURS']		=	'Eigen uren boeken';
$priv_def['PRIV_EDIT_ALL_HOURS']		=	'Andermans uren boeken';
$priv_def['PRIV_VIEW_ALL_HOURS']		=	'Andermans uren inzien';
$priv_def['PRIV_CHARGED_HOURS']			=	'Berekende uren boeken/inzien';

// *** Quotes
$priv_def['PRIV_QUOTES']				=	'Offertes module gebruiken';
$priv_def['PRIV_ADD_QUOTES']			=	'Nieuwe offerte aanmaken';
$priv_def['PRIV_EDIT_QUOTES']			=	'Offertes wijzigen';
$priv_def['PRIV_DELETE_QUOTES']			=	'Offertes verwijderen';
$priv_def['PRIV_QUOTE_TEXTS']			=	'Offerte teksten aanpassen';

// *** Statistics
$priv_def['PRIV_STATS']					=	'Statistieken inzien';

// *** Tickets
$priv_def['PRIV_TICKETS']				=	'Gebruik maken van het tickets systeem';
$priv_def['PRIV_ADD_TICKETS']			=	'Nieuwe tickets aanmaken';
$priv_def['PRIV_EDIT_TICKETS']			=	'Tickets wijzigen';
$priv_def['PRIV_DELETE_TICKETS']		=	'Tickets verwijderen';

// *** Site statistics
$priv_def['PRIV_SITE_STATS']			=	'Site statistieken bekijken';

?>