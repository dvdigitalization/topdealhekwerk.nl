<?php
/*
Title:		Common invoice and article defintions
File: 		inc/definitions/def_invoice_and_article.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

$db_table_articles		= 	DIGI_DB_PREFIX.'articles';
$db_table_cats			=	DIGI_DB_PREFIX.'article_categories';
$db_table_units			=	DIGI_DB_PREFIX.'article_units';
$db_table_vat			=	DIGI_DB_PREFIX.'vat_levels';
$db_table_invoices		=	DIGI_DB_PREFIX.'invoices';
$db_table_inv_art		=	DIGI_DB_PREFIX.'invoice_articles';
$db_table_notes			=	DIGI_DB_PREFIX.'invoice_notes';

define('EXCL',	0);
define('INCL',	1);

$incl_excl_types = array(EXCL => 'excl. BTW', INCL => 'incl. BTW');

$invoice_notes = array();
$res = eq("SELECT id, name FROM $db_table_notes ORDER BY name ASC;");
while ($myo = mfo($res)) {
	$invoice_notes[$myo->id] = $myo->name;
}

$vat_levels = array();
$res = eq("SELECT id, name, level FROM $db_table_vat ORDER BY level DESC;");
while ($myo = mfo($res)) {
	$vat_levels[$myo->id] 	= ($myo->level/100).'% '.$myo->name;
	$vat_array[$myo->id]	= $myo->level;
}

$article_units = array();
$res = eq("SELECT id, name FROM $db_table_units WHERE deleted = 0 ORDER BY name ASC;");
while ($myo = mfo($res)) {
	$article_units[$myo->id] = $myo->name;
}

$article_categories = array();
$res = eq("SELECT id, name FROM $db_table_cats WHERE deleted = 0 ORDER BY name ASC;");
while ($myo = mfo($res)) {
	$article_categories[$myo->id] = $myo->name;
}

function calcExclValue($amount, $type, $vat_level) {
		
	//Incl
	if ($type == INCL) {
		$amount = round($amount / ((100+($vat_level/100))/100));
	}

	return $amount;
}

function calcInclValue($amount, $type, $vat_level) {
		
	//Excl
	if ($type == EXCL) {
		$amount = round($amount * ((100+($vat_level/100))/100));
	}

	return $amount;
}

?>