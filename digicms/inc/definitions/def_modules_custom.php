<?php
/*
Title:		Custom module and privillege definitions
File: 		inc/definitions/def_modules_custom.php
Version: 	v2.03
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Install module id definitions
define('MOD_OFFERTES',			1000);
define('MOD_TOPDEAL_CATS',		1010);
define('MOD_TOPDEAL_PRODS',		1020);
define('MOD_TOPDEAL_CUSTOMERS',	1030);
define('MOD_TOPDEAL_DISCOUNTS',	1040);
define('MOD_TOPDEAL_SUBSCR',	1050);
define('MOD_HOLIDAYS',			1060);

$custom_modules = array (
	MOD_OFFERTES			=>	array(
								'Offerte aanvragen',
                             	array('cms_topdeal_offertes.php'),
                             	array(
								        1000	=> array('PRIV_OFFERTES',			'Offerte aanvragen inzien en beheren')
								),
								'topdeal_offertes',
								false,
								0,
								'invoices'
							),
	MOD_TOPDEAL_CATS			=>	array(
								'Categorieen',
                             	array('cms_topdeal_categories.php'),
                             	array(
								        1010	=> array('PRIV_TOPDEAL_CATS',			'Product categorieen beheren')
								),
								'topdeal_categories',
								false,
								0,
								'folder_full'
							),
	MOD_TOPDEAL_PRODS			=>	array(
								array('Producten','Kleuren'),
                             	array('cms_topdeal_products.php','cms_topdeal_colors.php'),
                             	array(
								        1020	=> array('PRIV_TOPDEAL_PRODS',			'Producten beheren')
								),
								'topdeal_products',
								false,
								0,
								'products'
							),
	MOD_TOPDEAL_CUSTOMERS	=>	array(
	                                'Klanten',
	                                array('cms_topdeal_customers.php'),
	                                array(
									        400	=> 'PRIV_CUSTOMERS',
									        401 => 'PRIV_ADD_CUSTOMERS',
									        402 => 'PRIV_EDIT_CUSTOMERS',
									        403 => 'PRIV_DELETE_CUSTOMERS'
									),
									'topdeal_customers',
									true,
									0,
									'people'
								),
	MOD_TOPDEAL_DISCOUNTS	=>	array(
	                                'Staffelkortingen',
	                            	array('cms_topdeal_discounts.php'),
	                            	array(
									      	600	=> 'PRIV_DISCOUNT_CODES'
									),
									'topdeal_discounts',
									false,
									0,
									'invoices'
								),
	MOD_TOPDEAL_SUBSCR	=>	array(
									MOD_NAME_NEWSLETTER_SUBSCR,
									array('cms_topdeal_subscribers.php'),
									array(
											280 => 'PRIV_NB_SUBSCR'
									),
									'newsletter_subscribers',
									false,
									0,
									'people'
								),
	MOD_HOLIDAYS	=>	array(
									"Feestdagen",
									array('cms_holidays.php'),
									array(
								        1000	=> array('PRIV_HOLIDAYS',			'Holidays administration')
									),
									'holidays',
									true,
									0,
									'calendar'
								)								
);
?>