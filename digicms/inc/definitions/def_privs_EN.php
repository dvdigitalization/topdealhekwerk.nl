<?php
/*
Title:		Privillege definitions [EN]
File: 		inc/definitions/def_privs_EN.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Settings
$priv_def['PRIV_CHANGE_USER']			=	'Edit own username and password';
$priv_def['PRIV_SETTINGS']				=	'Edit settings';

// *** Auth users
$priv_def['PRIV_AUTH_USERS']			=	'Manage users';
$priv_def['PRIV_VIEW_LOGS'] 			=	'View activities log';
$priv_def['PRIV_PURGE_LOGS']			= 	'Empty activities log';

// *** Page Contents
$priv_def['PRIV_TEXTS']					=	'Manage website text';

// *** Bad words
$priv_def['PRIV_SPAM_WORDS']			=	'Manage SPAM words';

// *** Blocked IP's
$priv_def['PRIV_BLOCKED_IPS']			=	'Manage blocked IP\'s';

// *** Backup
$priv_def['PRIV_BACKUP']				=	'Create system backup';
$priv_def['PRIV_RESTORE']				=	'Restore system backup';

// *** FAQ
$priv_def['PRIV_SITE_FAQ']				=	'Manage FAQ';

// *** Mailer
$priv_def['PRIV_MAILER']				=	'Use mailer';
$priv_def['PRIV_DELETE_EMAILS']			=	'Delete emails';

// *** Auto-update
$priv_def['PRIV_AUTO_UPDATE']			=	'Use Auto updater';

// *** Pages
$priv_def['PRIV_PAGES']					=	'Manage website pages';
$priv_def['PRIV_ADD_PAGES']				=	'Add website pages';
$priv_def['PRIV_EDIT_PAGES']			=	'Edit website pages';
$priv_def['PRIV_DEL_PAGES']				=	'Delete website pages';
$priv_def['PRIV_PUB_PAGES']				=	'Publish website pages';
$priv_def['PRIV_LOCK_PAGES']			=	'Lock and unlock website pages';
$priv_def['PRIV_AUTH_PAGES']			=	'Secure website pages';
$priv_def['PRIV_PAGE_CATS']				=	'Manage website page categories';

// *** Menu
$priv_def['PRIV_MENU']					=	'Manage website menu';

// *** Files
$priv_def['PRIV_FILES']					=	'Manage website files';

// *** News
$priv_def['PRIV_NEWS']					=	'Manage news';
$priv_def['PRIV_NEWS_RSS']				=	'Manage news RSS feed';

// *** Blog
$priv_def['PRIV_BLOG']					=	'Manage blog';
$priv_def['PRIV_BLOG_RSS']				=	'Manage blog RSS newsfeed';

// *** Agenda
$priv_def['PRIV_AGENDA']				=	'Manage agenda';

// *** Calendar
$priv_def['PRIV_CALENDAR']				=	'Manage calendar';

// *** Polls
$priv_def['PRIV_POLLS']					=	'Manage polls';

// *** Photo album
$priv_def['PRIV_PHOTO_ALBUMS']			=	'Manage photo albums';

// *** Banners
$priv_def['PRIV_BANNERS']				=	'Manage banners';

// *** Guestbook
$priv_def['PRIV_GUESTBOOK']				=	'Manage guestbook';

// *** Customer quotes
$priv_def['PRIV_CUSTOMER_QUOTES']		=	'Manage customer quotes';

// *** Newsletter subscriber
$priv_def['PRIV_NB_SUBSCR']				=	'Manage newsletter subscribers';

// *** Newsletter
$priv_def['PRIV_NB']					=	'Manage newsletters';
$priv_def['PRIV_NB_SEND']				=	'Send newsletters';

// *** Extranet
$priv_def['PRIV_EXTRANET']				=	'Manage extranet users';

// *** Extranet files
$priv_def['PRIV_EXTRANET_FILES']		=	'Manage extranet files';

// *** Extranet basic
$priv_def['PRIV_EXTRANET']				=	'Manage extranet users';

// *** Forms
$priv_def['PRIV_FORMS']					=	'Manage forms';

// *** Youtube
$priv_def['PRIV_YOUTUBE']				=	'Manage youtube video\'s';

// *** Google maps
$priv_def['PRIV_GOOGLE_MAPS']			=	'Manage google maps';
$priv_def['PRIV_GOOGLE_DIRECTIONS']		=	'Manage Google directions';
$priv_def['PRIV_GOOGLE_ROUTES'] 		=	'Manage Google routes';

// *** Customer shop
$priv_def['PRIV_CUSTOMERS']				=	'Manage customers';
$priv_def['PRIV_ADD_CUSTOMERS']			=	'Add customers';
$priv_def['PRIV_EDIT_CUSTOMERS']		=	'Edit customers';
$priv_def['PRIV_DELETE_CUSTOMERS']		=	'Delete customers';

// *** Collections
$priv_def['PRIV_COLLECTIONS']			=	'Manage collections';
$priv_def['PRIV_DELETE_COLLECTIONS']	=	'Delete collections';

// *** Products
$priv_def['PRIV_PRODUCTS']				=	'Manage products';
$priv_def['PRIV_ADD_PRODUCTS']			=	'Add products';
$priv_def['PRIV_EDIT_PRODUCTS']			=	'Edit products';
$priv_def['PRIV_DELETE_PRODUCTS']		=	'Delete products';

// *** Product properties
$priv_def['PRIV_PRODUCT_PROPS']			= 	'Manage products properties';

// *** Delivery Costs
$priv_def['PRIV_DELIVERY_COSTS']		=	'Manage delivery costs';

// *** Orders
$priv_def['PRIV_ORDERS']				=	'Manage orders';

// *** Affiliates
$priv_def['PRIV_AFFILIATES']			=	'Manage affiliates';
$priv_def['PRIV_ADD_AFFILIATES']		=	'Add affiliates';
$priv_def['PRIV_EDIT_AFFILIATES']		=	'Edit affiliates';
$priv_def['PRIV_DELETE_AFFILIATES']		=	'Delete affiliates';

// *** Prod cats simple
$priv_def['PRIV_PRODUCT_CATS']			=	'Manage product categories';
$priv_def['PRIV_DELETE_PRODUCT_CATS']	=	'Delete product categories';

// *** Products simple
$priv_def['PRIV_PRODUCTS']				=	'Manage products';
$priv_def['PRIV_ADD_PRODUCTS']			=	'Add products';
$priv_def['PRIV_EDIT_PRODUCTS']			=	'Edit products';
$priv_def['PRIV_DELETE_PRODUCTS']		=	'Delete Products';

// *** Customer admin
$priv_def['PRIV_CUSTOMERS']				=	'Use customers module';
$priv_def['PRIV_ADD_CUSTOMERS']			=	'Add customers';
$priv_def['PRIV_EDIT_CUSTOMERS']		=	'Edit customers';
$priv_def['PRIV_DELETE_CUSTOMERS']		=	'Delete customers';
$priv_def['PRIV_RATES']					=	'Manage rates';
$priv_def['PRIV_DELETE_CUSTOMER_EVENTS']=	'Delete tickets for customers';

// *** Leads
$priv_def['PRIV_LEADS']					=	'Use leads modules';

// *** Vacancies
$priv_def['PRIV_VACANCIES']				=	'Manage vacancies';

// *** Links
$priv_def['PRIV_LINKS']					= 	'Manage links';

// *** Contact form data
$priv_def['PRIV_CONTACT_FORM_DATA']		=	'Manage contact forms';

// *** Projects
$priv_def['PRIV_PROJECTS']				=	'Use projects module';
$priv_def['PRIV_ADD_PROJECTS']			=	'Add projects';
$priv_def['PRIV_EDIT_PROJECTS']			=	'Edit projects';
$priv_def['PRIV_DELETE_PROJECTS']		=	'Delete projects';
$priv_def['PRIV_CLOSE_PROJECTS'] 		=	'Close or re-open projects';
$priv_def['PRIV_EXTRA_PROJECTS']		=	'Manage extra projects';
$priv_def['PRIV_ALL_PROJECTS']			=	'Manage all projects, manage responibility';

// *** Invoices
$priv_def['PRIV_INVOICES']				=	'Use invoices module';
$priv_def['PRIV_ADD_INVOICES']			=	'Add invoices';
$priv_def['PRIV_EDIT_INVOICES']			=	'Edit invoices';
$priv_def['PRIV_DELETE_INVOICES']		=	'Delete invoices';
$priv_def['PRIV_SET_PAID_INVOICES']		=	'Set paid invoices';
$priv_def['PRIV_INVOICE_LISTS']			=	'Lists to manage invoice';

// *** Articles
$priv_def['PRIV_ARTICLES']				=	'Manage articles';
$priv_def['PRIV_ARTICLE_UNITS']			=	'Manage article units';
$priv_def['PRIV_ARTICLE_CATS']			=	'Manage article categories';

// *** Hours
$priv_def['PRIV_HOURS']					=	'View own hours';
$priv_def['PRIV_EDIT_OWN_HOURS']		=	'Edit own hours';
$priv_def['PRIV_EDIT_ALL_HOURS']		=	'Edit all hours';
$priv_def['PRIV_VIEW_ALL_HOURS']		=	'View all hours';
$priv_def['PRIV_CHARGED_HOURS']			=	'View/edit charged hours';

// *** Quotes
$priv_def['PRIV_QUOTES']				=	'Use quotes module';
$priv_def['PRIV_ADD_QUOTES']			=	'Add quote';
$priv_def['PRIV_EDIT_QUOTES']			=	'Edit quote';
$priv_def['PRIV_DELETE_QUOTES']			=	'Delete quote';
$priv_def['PRIV_QUOTE_TEXTS']			=	'Edit quote text';

// *** Statistics
$priv_def['PRIV_STATS']					=	'View statistics';

// *** Tickets
$priv_def['PRIV_TICKETS']				=	'Use ticket system';
$priv_def['PRIV_ADD_TICKETS']			=	'Add tickets';
$priv_def['PRIV_EDIT_TICKETS']			=	'Edit tickets';
$priv_def['PRIV_DELETE_TICKETS']		=	'Delete tickets';

// *** Site statistics
$priv_def['PRIV_SITE_STATS']			=	'View site statistics';

?>