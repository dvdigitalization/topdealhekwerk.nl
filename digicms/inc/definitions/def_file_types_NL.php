<?php
/*
Title:		File type definitions [NL]
File: 		inc/definitions/def_file_types_NL.php
Version: 	v2.04
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// GENERIC
$f_type[0]		=	' bestand';

// WEBDEVELOPMENT
$f_type['css']	=	'Stylesheet';
$f_type['php']	=	'PHP code';
$f_type['js']	=	'Javascript code';
$f_type['sql']	=	'Database code';
$f_type['rss']	=	'Nieuws feed';
$f_type['htm']	=	'Website bestand';
$f_type['html']	=	'Website bestand';
$f_type['log']	=	'Log bestand';
$f_type['xml']	=	'XML bestand';

// OFFICE AND PDF
$f_type['pdf']	=	'PDF document';
$f_type['doc']	=	'Word document';
$f_type['docx']	=	'Word document';
$f_type['csv']	=	'Excel bestand';
$f_type['xls']	=	'Excel spreadsheet';
$f_type['xlsx']	=	'Excel spreadsheet';
$f_type['ppt']	=	'Powerpoint presentatie';
$f_type['pptx']	=	'Powerpoint presentatie';

// OPEN OFFICE
$f_type['svw']	=	'OO writer document';
$f_type['sxc']	=	'OO calc spreadsheet';
$f_type['sxm']	=	'OO math document';
$f_type['sxi']	=	'OO impress presentatie';
$f_type['sxd']	=	'OO draw document';

// PLAIN TEXT
$f_type['txt']	=	'Tekstbestand';
$f_type['rtf']	=	'Tekstbestand';

// FLASH
$f_type['swf']	=	'Flash animatie';
$f_type['fla']	=	'Flash animatie';

// MOVIES
$f_type['avi']	=	'Video';
$f_type['wmv']	=	'Video';
$f_type['mpg']	=	'Video';
$f_type['mpeg']	=	'Video';
$f_type['rm']	=	'Video';
$f_type['flv']	=	'Flash video';
$f_type['mov']	=	'Quicktime video';

// IMAGES
$f_type['jpg']	=	'Webafbeelding';
$f_type['jpeg']	=	'Webafbeelding';
$f_type['gif']	=	'Webafbeelding';
$f_type['png']	=	'Webafbeelding';
$f_type['tif']	=	'Bitmap';
$f_type['tiff']	=	'Bitmap';
$f_type['bmp']	=	'Bitmap';
$f_type['svg']	=	'Vector afbeelding';
$f_type['psd']	=	'Photoshop afbeelding';
$f_type['ico']	=	'Icoontje';

// AUDIO
$f_type['mp3']	=	'Mp3 audio';
$f_type['mp4']	=	'Mp3 audio';
$f_type['wav']	=	'Audio';
$f_type['wma']	=	'Audio';
$f_type['au']	=	'Audio';
$f_type['ra']	=	'Real audio';
$f_type['mid']	=	'Midi audio';
$f_type['midi']	=	'Midi audio';

// ARCHIVE FILES
$f_type['zip']	=	'Archief';
$f_type['rar']	=	'Archief';
$f_type['arj']	=	'Archief';
$f_type['ace']	=	'Archief';
$f_type['tar']	=	'Archief';
$f_type['gz']	=	'Archief';
$f_type['bz2']	=	'Archief';
$f_type['lha']	=	'Archief';
$f_type['iso']	=	'Archief';
$f_type['gzip']	=	'Archief';
?>