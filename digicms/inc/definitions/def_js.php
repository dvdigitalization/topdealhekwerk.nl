<?php
/*
Title:		Javascript definitions
File: 		inc/common/def_js.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

$js['NaN']	=	"onchange='force_number(this);'";
$js['int']	=	"onchange='force_positive_integer(this, 0);'";

?>