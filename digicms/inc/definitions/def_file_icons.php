<?php
/*
Title:		File icon definitions
File: 		inc/definitions/def_file_icons.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

if (defined('CMS_PATH_PREFIX')) 	$pr = CMS_PATH_PREFIX;
if (defined('SITE_PATH_PREFIX')) 	$pr = SITE_PATH_PREFIX;

// GENERIC
$f_icon[0]		=	"<img src='{$pr}gfx/filetypes/icon_generic.gif' border='0px' style='margin-bottom: -2px;'/>";

// WEBDEVELOPMENT
$f_icon['css']	=	"<img src='{$pr}gfx/filetypes/icon_css.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['php']	=	"<img src='{$pr}gfx/filetypes/icon_php.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['js']	=	"<img src='{$pr}gfx/filetypes/icon_js.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['sql']	=	"<img src='{$pr}gfx/filetypes/icon_sql.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['rss']	=	"<img src='{$pr}gfx/filetypes/icon_feed.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['htm']	=	"<img src='{$pr}gfx/filetypes/icon_html.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['html']	=	"<img src='{$pr}gfx/filetypes/icon_html.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['log']	=	"<img src='{$pr}gfx/filetypes/icon_log.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['xml']	=	"<img src='{$pr}gfx/filetypes/icon_xml.gif' border='0px' style='margin-bottom: -2px;'/>";

// OFFICE AND PDF
$f_icon['pdf']	=	"<img src='{$pr}gfx/filetypes/icon_pdf.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['doc']	=	"<img src='{$pr}gfx/filetypes/icon_doc.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['docx']	=	"<img src='{$pr}gfx/filetypes/icon_doc.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['csv']	=	"<img src='{$pr}gfx/filetypes/icon_xls.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['xls']	=	"<img src='{$pr}gfx/filetypes/icon_xls.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['xlsx']	=	"<img src='{$pr}gfx/filetypes/icon_xls.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['ppt']	=	"<img src='{$pr}gfx/filetypes/icon_ppt.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['pptx']	=	"<img src='{$pr}gfx/filetypes/icon_ppt.gif' border='0px' style='margin-bottom: -2px;'/>";

// OPEN OFFICE
$f_type['svw']	=	"<img src='{$pr}gfx/filetypes/icon_doc.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_type['sxc']	=	"<img src='{$pr}gfx/filetypes/icon_xls.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_type['sxm']	=	"<img src='{$pr}gfx/filetypes/icon_xls.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_type['sxi']	=	"<img src='{$pr}gfx/filetypes/icon_ppt.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_type['sxd']	=	"<img src='{$pr}gfx/filetypes/icon_image.gif' border='0px' style='margin-bottom: -2px;'/>";

// PLAIN TEXT
$f_icon['txt']	=	"<img src='{$pr}gfx/filetypes/icon_txt.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['rtf']	=	"<img src='{$pr}gfx/filetypes/icon_txt.gif' border='0px' style='margin-bottom: -2px;'/>";

// FLASH
$f_icon['swf']	=	"<img src='{$pr}gfx/filetypes/icon_swf.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['fla']	=	"<img src='{$pr}gfx/filetypes/icon_fla.gif' border='0px' style='margin-bottom: -2px;'/>";

// MOVIES
$f_icon['avi']	=	"<img src='{$pr}gfx/filetypes/icon_film.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['wmv']	=	"<img src='{$pr}gfx/filetypes/icon_film.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['mpg']	=	"<img src='{$pr}gfx/filetypes/icon_film.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['mpeg']	=	"<img src='{$pr}gfx/filetypes/icon_film.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['rm']	=	"<img src='{$pr}gfx/filetypes/icon_film.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['flv']	=	"<img src='{$pr}gfx/filetypes/icon_film.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['mov']	=	"<img src='{$pr}gfx/filetypes/icon_mov.gif' border='0px' style='margin-bottom: -2px;'/>";

// IMAGES
$f_icon['jpg']	=	"<img src='{$pr}gfx/filetypes/icon_image.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['jpeg']	=	"<img src='{$pr}gfx/filetypes/icon_image.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['gif']	=	"<img src='{$pr}gfx/filetypes/icon_image.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['png']	=	"<img src='{$pr}gfx/filetypes/icon_image.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['tiff']	=	"<img src='{$pr}gfx/filetypes/icon_image.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['tif']	=	"<img src='{$pr}gfx/filetypes/icon_image.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['bmp']	=	"<img src='{$pr}gfx/filetypes/icon_image.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['svg']	=	"<img src='{$pr}gfx/filetypes/icon_vector.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['psd']	=	"<img src='{$pr}gfx/filetypes/icon_photoshop.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_type['ico']	=	"<img src='{$pr}gfx/filetypes/icon_image.gif' border='0px' style='margin-bottom: -2px;'/>";

// AUDIO
$f_icon['mp3']	=	"<img src='{$pr}gfx/filetypes/icon_audio.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['mp4']	=	"<img src='{$pr}gfx/filetypes/icon_audio.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['wav']	=	"<img src='{$pr}gfx/filetypes/icon_audio.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['wma']	=	"<img src='{$pr}gfx/filetypes/icon_audio.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['au']	=	"<img src='{$pr}gfx/filetypes/icon_audio.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['ra']	=	"<img src='{$pr}gfx/filetypes/icon_audio.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['mid']	=	"<img src='{$pr}gfx/filetypes/icon_audio.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['midi']	=	"<img src='{$pr}gfx/filetypes/icon_audio.gif' border='0px' style='margin-bottom: -2px;'/>";

// ARCHIVE FILES
$f_icon['zip']	=	"<img src='{$pr}gfx/filetypes/icon_zip.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['gzip']	=	"<img src='{$pr}gfx/filetypes/icon_zip.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['rar']	=	"<img src='{$pr}gfx/filetypes/icon_zip.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['arj']	=	"<img src='{$pr}gfx/filetypes/icon_zip.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['ace']	=	"<img src='{$pr}gfx/filetypes/icon_zip.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['tar']	=	"<img src='{$pr}gfx/filetypes/icon_zip.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['gz']	=	"<img src='{$pr}gfx/filetypes/icon_zip.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['bz2']	=	"<img src='{$pr}gfx/filetypes/icon_zip.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['lha']	=	"<img src='{$pr}gfx/filetypes/icon_zip.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['iso']	=	"<img src='{$pr}gfx/filetypes/icon_zip.gif' border='0px' style='margin-bottom: -2px;'/>";

// SOME OTHER ICONS
$f_icon['script']	=	"<img src='{$pr}gfx/filetypes/icon_script.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['locked']	=	"<img src='{$pr}gfx/filetypes/icon_file_locked.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['generic']	=	"<img src='{$pr}gfx/filetypes/icon_generic.gif' border='0px' style='margin-bottom: -2px;'/>";
$f_icon['plugin']	=	"<img src='{$pr}gfx/filetypes/icon_plugin.gif' border='0px' style='margin-bottom: -2px;'/>";
?>