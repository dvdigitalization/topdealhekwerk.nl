<?php
/*
Title:		Digicon definitions
File: 		inc/common/def_digicons.php
Version: 	v2.04
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
Icons by:	http://dryicons.com/
*/

if (defined('CMS_PATH_PREFIX')) 	$pr = '../';
if (defined('SITE_PATH_PREFIX')) 	$pr = '';

//Some pixel gif urls which could be useful
$img['trans']					=	"{$pr}gfx/cms/system/transparent.gif'";
$img['black']					=	"{$pr}gfx/cms/system/black.gif'";

//Datatable
$img['arrow_mark']				=	"<img src='{$pr}gfx/cms/system/arrow_mark.gif' border='0px' />";
$img['asc']						=	"<img src='{$pr}gfx/cms/system/sort_asc.gif' border='0px' alt='Oplopend'/>";
$img['desc']					=	"<img src='{$pr}gfx/cms/system/sort_desc.gif' border='0px' alt='Aflopend'/>";

//Ok / notok
$img['ok']						=	"<img src='{$pr}gfx/digicons/ok.png' border='0px' class='digicon'/>";
$img['notok']					=	"<img src='{$pr}gfx/digicons/notok.png' border='0px' class='digicon'/>";
$img['halfok']					=	"<img src='{$pr}gfx/digicons/halfok.png' border='0px' class='digicon'/>";

//Generic
$img['add']						=	"<img src='{$pr}gfx/digicons/add.png' border='0px' class='digicon'/>";
$img['remove']					=	"<img src='{$pr}gfx/digicons/remove.png' border='0px' class='digicon'/>";
$img['del']						=	"<img src='{$pr}gfx/digicons/del.png' border='0px' class='digicon'/>";
$img['edit']					=	"<img src='{$pr}gfx/digicons/edit.png' border='0px' class='digicon'/>";

//Signs
$img['info']					=	"<img src='{$pr}gfx/digicons/info.png' border='0px' class='digicon'/>";
$img['help']					=	"<img src='{$pr}gfx/digicons/help.png' border='0px' class='digicon'/>";
$img['warning']					=	"<img src='{$pr}gfx/digicons/warning.png' border='0px' class='digicon'/>";

//Arrows
$img['forward']					=	"<img src='{$pr}gfx/digicons/next.png' border='0px' class='digicon'/>";
$img['backward']				=	"<img src='{$pr}gfx/digicons/prev.png' border='0px' class='digicon'/>";
$img['up']						=	"<img src='{$pr}gfx/digicons/up.png' border='0px' class='digicon'/>";
$img['down']					=	"<img src='{$pr}gfx/digicons/down.png' border='0px' class='digicon'/>";

//Some specific icons
$img['print']					=	"<img src='{$pr}gfx/digicons/print.png' border='0px' class='digicon'/>";
$img['print_search']			=	"<img src='{$pr}gfx/digicons/print_search.png' border='0px' class='digicon'/>";
$img['phone']					=	"<img src='{$pr}gfx/digicons/phone.png' border='0px' class='digicon'/>";
$img['draft']					=	"<img src='{$pr}gfx/digicons/draft.png' border='0px' class='digicon'/>";
$img['windows']					=	"<img src='{$pr}gfx/digicons/windows.png' border='0px' class='digicon'/>";

//Specific objects
$objects = array('app','backup','calc','calendar','cart','cd','clock','comment','comments','computer','db','fav','film','folder','folder_full','image','invoice','mail','note','page','process','product','rss','she_user','she_users','stats','text','user','users','window','home','search','clip');

foreach ($objects as $object) {
	$img[$object]				=	"<img src='{$pr}gfx/digicons/{$object}.png' border='0px' class='digicon'/>";
	$img[$object.'_add']		=	"<img src='{$pr}gfx/digicons/{$object}_add.png' border='0px' class='digicon'/>";
	$img[$object.'_del']		=	"<img src='{$pr}gfx/digicons/{$object}_del.png' border='0px' class='digicon'/>";
	$img[$object.'_ok']			=	"<img src='{$pr}gfx/digicons/{$object}_ok.png' border='0px' class='digicon'/>";
	$img[$object.'_up']			=	"<img src='{$pr}gfx/digicons/{$object}_up.png' border='0px' class='digicon'/>";
	$img[$object.'_down']		=	"<img src='{$pr}gfx/digicons/{$object}_down.png' border='0px' class='digicon'/>";
	$img[$object.'_prev']		=	"<img src='{$pr}gfx/digicons/{$object}_prev.png' border='0px' class='digicon'/>";
	$img[$object.'_next']		=	"<img src='{$pr}gfx/digicons/{$object}_next.png' border='0px' class='digicon'/>";
	$img[$object.'_edit']		=	"<img src='{$pr}gfx/digicons/{$object}_edit.png' border='0px' class='digicon'/>";
}

//Extra
$img['mail_search']				=	"<img src='{$pr}gfx/digicons/mail_search.png' border='0px' class='digicon'/>";
$img['mail_new_forward']		=	"<img src='{$pr}gfx/digicons/mail_new_forward.png' border='0px' class='digicon'/>";
$img['calendar_empty']			=	"<img src='{$pr}gfx/digicons/calendar_empty.png' border='0px' class='digicon'/>";
$img['page_search']				=	"<img src='{$pr}gfx/digicons/page_search.png' border='0px' class='digicon'/>";
$img['page_swap']				=	"<img src='{$pr}gfx/digicons/page_swap.png' border='0px' class='digicon'/>";
$img['page_lock']				=	"<img src='{$pr}gfx/digicons/page_lock.png' border='0px' class='digicon'/>";
$img['page_process']			=	"<img src='{$pr}gfx/digicons/page_process.png' border='0px' class='digicon'/>";
$img['film_fav']				=	"<img src='{$pr}gfx/digicons/film_fav.png' border='0px' class='digicon'/>";
$img['image_lock']				=	"<img src='{$pr}gfx/digicons/image_lock.png' border='0px' class='digicon'/>";
$img['image_multi']				=	"<img src='{$pr}gfx/digicons/image_multi.png' border='0px' class='digicon'/>";
$img['db_search']				=	"<img src='{$pr}gfx/digicons/db_search.png' border='0px' class='digicon'/>";
$img['db_process']				=	"<img src='{$pr}gfx/digicons/db_process.png' border='0px' class='digicon'/>";
$img['db_lock']					=	"<img src='{$pr}gfx/digicons/db_lock.png' border='0px' class='digicon'/>";
$img['note_list']				=	"<img src='{$pr}gfx/digicons/note_list.png' border='0px' class='digicon'/>";
$img['people']					=	"<img src='{$pr}gfx/digicons/people.png' border='0px' class='digicon'/>";
$img['talk']					=	"<img src='{$pr}gfx/digicons/talk.png' border='0px' class='digicon'/>";
$img['user_comment']			=	"<img src='{$pr}gfx/digicons/user_comment.png' border='0px' class='digicon'/>";
$img['she_user_comment']		=	"<img src='{$pr}gfx/digicons/she_user_comment.png' border='0px' class='digicon'/>";
$img['notfav']					=	"<img src='{$pr}gfx/digicons/notfav.png' border='0px' class='digicon'/>";

//Sound
$img['sound']					=	"<img src='{$pr}gfx/digicons/sound.png' border='0px' class='digicon'/>";
$img['sound_on']				=	"<img src='{$pr}gfx/digicons/sound_on.png' border='0px' class='digicon'/>";
$img['sound_off']				=	"<img src='{$pr}gfx/digicons/sound_off.png' border='0px' class='digicon'/>";

//Locks
$img['lock']					=	"<img src='{$pr}gfx/digicons/lock.png' border='0px' class='digicon'/>";
$img['lock_disabled']			=	"<img src='{$pr}gfx/digicons/lock_disabled.png' border='0px' class='digicon'/>";
$img['lock_off']				=	"<img src='{$pr}gfx/digicons/lock_off.png' border='0px' class='digicon'/>";
$img['lock_off_disabled']		=	"<img src='{$pr}gfx/digicons/lock_off_disabled.png' border='0px' class='digicon'/>";

//Placeholder
$img['placeholder']				=	"<img src='{$pr}gfx/digicons/transparent.gif' border='0px' class='digicon'/>";

?>