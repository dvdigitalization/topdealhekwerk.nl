<?php
/*
Title:		CMS footer construct
File: 		inc/cms_footer.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// *** Copyright years
$years = array();
for ($year = 2008; $year <= date('Y'); $year++) {
	$years[] = $year;	
}

// *** Determine footer data
$years 				= 	implode(', ', $years);
$version_tag 		= 	"<span title='".TXT_VERSION." $_version - ".TXT_REVISION." $_revision'>v".$_version."</span>";
$phpversion 		= 	TXT_PHP_VERSION.' '.substr(phpversion(),0,5);
$safe_mode_notice 	= 	ini_get('safe_mode') ? '<b>'.TXT_SAFE_MODE_ON.'</b>' : '';
$tech_mail_link 	= 	"<a href='mailto:$cfg[TECH_EMAIL]'>$cfg[TECH_NAME]</a>";
$username			=	$_SESSION['login_user'];

// *** Benchmark time
$m_time 			= 	explode(' ',microtime()); 
$end_time 			= 	$m_time[1] + $m_time[0]; 
$benchmark 			= 	TXT_BENCHMARK.' '.substr($end_time - $start_time,0,5).' '.TXT_SECONDS;

// *** Footer data
$footer_data	=	array($phpversion, $safe_mode_notice, $benchmark);
foreach ($footer_data as $key => $data) {
	if (empty($data))	unset($footer_data[$key]);
}
$footer_data	=	$s_id == 1 ? ' - '.implode(' - ', $footer_data) : '';

// *** Backwards compatibility
$cms_title		=	(isset($cms_title) and !empty($cms_title)) 	? $cms_title 	: 'Digitalization CMS';
$cms_name		=	(isset($cms_name) and !empty($cms_name)) 	? $cms_name 	: 'Digitalization CMS';
$cms_title		=	$s_id ? $cms_title.' v'.$_version : $cms_title;

// *** Not logged in
if (!$s_id) {
	
	//Change template, with backwards compatibility
	$template_cms = isset($template_login) ? $template_login : 'digi_cms_20x_login.html';

	//Get layout template
	$html = file_get_contents('template/'.$template_cms);
	
	//Make replacements
	$html = str_replace('[MENU_HTML]',				'',							$html);
	$html = str_replace('[TECH_LINK]',				$tech_mail_link,			$html);
	$html = str_replace('[LOGIN_REQUIRED_NOTICE]',	LOGIN_REQUIRED_NOTICE,		$html);
	$html = str_replace('[LOGIN_PROBLEMS_NOTICE]',	LOGIN_PROBLEMS_NOTICE,		$html);
}

// *** Logged in
else {

	//Backwards compatible
	$template_cms = isset($template_cms) ? $template_cms : $template;

	//Get layout template
	$html = file_get_contents('template/'.$template_cms);

	//AJAX JS
	$ajax_js = (isset($load_ajax) and $load_ajax) ?	"\n".$Xajax->printJavascript() : '';
	
	//Make replacements
	$html = str_replace('[MENU_HTML]',			$menu_html,					$html);
	$html = str_replace('[TOP_ICONS]',			$top_icons,					$html);
	$html = str_replace('[JAVASCRIPT]',			$javascript.$ajax_js,		$html);
	$html = str_replace('[TIMER_CONTROLS]',		$timer_controls.$timer_js,	$html);
	$html = str_replace('[CALENDAR_SUPPORT]',	$calendar_support,			$html);
	$html = str_replace('[PAGE_TITLE]',			$page_title,				$html);
	$html = str_replace('[VERSION]',			$version_tag,				$html);
	$html = str_replace('[FOOTER_DATA]',		$footer_data,				$html);
	$html = str_replace('[USERNAME]',			$username,					$html);
	$html = str_replace('[COPYRIGHT_YEARS]',	$years,						$html);
}

// *** Make some more replacements
$html = str_replace('[CMS_PATH_PREFIX]',		CMS_PATH_PREFIX,			$html);
$html = str_replace('[CMS_TITLE]',				$cms_title,					$html);
$html = str_replace('[CMS_NAME]',				$cms_name,					$html);
$html = str_replace('[SITE_NAME]',				$site_name,					$html);
$html = str_replace('[CHARSET]',				$charset,					$html);
$html = str_replace('[TXT_DIGI_LINK_TITLE]',	TXT_DIGI_LINK_TITLE,		$html);
$html = str_replace('[TXT_DEVELOPED_BY]',		TXT_DEVELOPED_BY,			$html);
$html = str_replace('[TXT_COPYRIGHT]',			TXT_COPYRIGHT,				$html);

// *** Determine what contents to output
if ($error_occured) {
	ob_clean();
	$html = str_replace('[CONTENT]',			$error_html,			$html);	
}
elseif ($s_id) {
	$html = str_replace('[CONTENT]',			ob_get_contents(),		$html);
	ob_clean();
}
else {
	ob_clean();
	$html = str_replace('[CONTENT]',			$login_box,				$html);
}

// *** Gold mode
if ((isset($_GET['goldmode']) or isset($_SESSION['goldmode'])) and file_exists('../gfx/cms/system/gold.gif')) {
	$_SESSION['goldmode'] = 1;
	$html = str_replace('&euro;',	"<img src='../gfx/cms/system/gold.gif' class='digicon'/> ",		$html);
}

// *** Output
echo $html;
?>