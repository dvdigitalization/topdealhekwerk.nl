<?php
/*
Title:		FAQ file
File: 		cms_faq.php
Version: 	v2.04
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization			
*/

/******************************************************************************************
 * Settings
 ***/
 
// *** Required files
require_once('inc/cms_header.php');
require_once('inc/lang/'.$cms_lang.'_'.$_file);
require_once(CMS_PATH_PREFIX.'classes/class_FaqEngine.php');

$db_table_categories 	= 	DIGI_DB_PREFIX.'faq_categories';
$is_digi				=	(($s_id == 1 and $s_admin) or ($testmode and $s_admin)) ? true : false;
$FaqEngine 				= 	new FaqEngine(); 

// *** Only ajax?
if (isset($_POST['ajax'])) {
	require_once('inc/cms_ajax_header.php');
}
else {
	require_once('inc/cms_header.php');	
}

/******************************************************************************************
 * Functions
 ***/

function makeTopicHTML($id, $title, $text) {
 
 	global $cfg, $mailer;
 
 	//Make topic HTML
 	$faq_note	=	str_replace('[TECH_NAME]',	$cfg['TECH_NAME'],	TXT_FAQ_NOTE);
 	$faq_note	=	str_replace('[TECH_EMAIL]',	"<a href='$mailer$cfg[TECH_EMAIL]'>".$cfg['TECH_EMAIL']."</a>",	$faq_note);
 	
	$html  = "	<div class='faqtopic'>
				<h6>#$id</h6><br/><b>$title</b><br/><br/>
				$text
				</div>
				<br/><i>$faq_note</i>
	";
	
	return $html;
}

/******************************************************************************************
 * AJAX requests
 ***/

if (isset($_POST['get_subcat_topics']) and $subcat_id = (int) $_POST['subcat_id']) {
	
	//Use global object
	global $FaqEngine;
	
	//Get array with topics
	$topics_array = $FaqEngine->getFaqTopics($subcat_id);
	
	//Loop and make HTML
	foreach ($topics_array as $id => $title) {
		$topics[] = "<tr><td valign='top' width='20px' align='right'><span class='grey'>&raquo;</span>&nbsp;</td><td>".($is_digi ? "<a href='$_file?edit_topic=$id'>$img[edit]</a> <a href='$_file?delete_topic=$id'>$img[fav_del]</a> - " : '')." <a href='JavaScript: void(0);' onclick='display_topic($id);'>$title</a></td></tr>";
	}
	
	//Set empty category HTML
	if (!count($topics)) {
		$topics[] = "<tr><td width='20px' align='right'><span class='grey'>&raquo;</span>&nbsp;</td><td>".TXT_NO_TOPICS."</td></tr>";
	}
	
	$html = "<table cellspacing='0px' cellpadding='0px' style='display: inline;'>".implode("\n", $topics)."</table>";
	
	echo $_POST['row'].'||'.$html;
	exit;
}

if (isset($_POST['get_topic']) and $FaqEngine->setTopic($_POST['topic_id'])) {
	$html = makeTopicHTML($_POST['topic_id'],$FaqEngine->getTopicTitle(),$FaqEngine->getTopicText());
	
	echo $html;
	exit;
}

/******************************************************************************************
 * POST and GET Cases
 ***/

if (isset($_GET['topic']) and $FaqEngine->setTopic($_GET['topic'])) {
 	$topic_id 			= (int) $_GET['topic'];
 	$subcat_to_expand 	= $FaqEngine->getCatFromTopic($topic_id);
	$topic_html 		= makeTopicHTML($topic_id,$FaqEngine->getTopicTitle(),$FaqEngine->getTopicText());
}

if (isset($_POST['add_category_finish']) and $is_digi) {
	evalAll($_POST['name']);
	insRec($db_table_categories,array('name','subcat_of'),array($_POST['name'],$_POST['subcat_of']));
	$Message->set($msg_cat_added);
}

if (isset($_POST['edit_category_finish']) and $db_id = (int) $_POST['id'] and $is_digi) {
	evalAll($_POST['name']);
	upRec($db_table_categories,$db_id,array('name','subcat_of'),array($_POST['name'],$_POST['subcat_of']));
	$Message->set($msg_cat_editted);
}

if (isset($_GET['delete_category']) and $db_id = (int) $_GET['delete_category']) {
	delRec($db_table_categories,$db_id);
	delRec($db_table_categories,"subcat_of = '$db_id'");
	delRec($db_table,"faq_cat = '$db_id'");
	$Message->set($msg_cat_deleted);	
}

if (isset($_POST['add_topic_finish']) and $is_digi) {
	evalAll($_POST['descr']);
	evalAll($_POST['title']);
	insRec($db_table,array('title','faq_cat','descr'),array($_POST['title'],$_POST['faq_cat'],$_POST['descr']));
	$Message->set($msg_added);
}

if (isset($_POST['edit_topic_finish']) and $db_id = (int) $_POST['id'] and $is_digi) {
	evalAll($_POST['descr']);
	evalAll($_POST['title']);
	upRec($db_table,$db_id,array('title','faq_cat','descr'),array($_POST['title'],$_POST['faq_cat'],$_POST['descr']));
	$Message->set($msg_editted);
}

if (isset($_GET['delete_topic']) and $db_id = (int) $_GET['delete_topic']) {
	delRec($db_table,$db_id);
	$Message->set($msg_deleted);	
}

/******************************************************************************************
 * Add / Edit / View options
 ***/
 
if (isset($_GET['add_category']) and $is_digi) {
	
	$faq_cats = array(0 => '') + makeArray($db_table_categories,'id','name');
	
	$pre_html = "
	
		<h3>".TXT_ADD_CATEGORY.":</h3><hr/><br/>
		<form name='add_category' method='post' action='$_file'>
		<table border='0px' cellspacing='0px' cellpadding='1px'>
		<tr>
			<td width='150px'>".TXT_NAME.":</td>
			<td><input type='text' name='name' class='input_regular'/></td>
		</tr>
		<tr>
			<td>".TXT_SUBCAT.":</td>
			<td>".makeSelectbox('subcat_of',$faq_cats,0)."</td>
		</tr>
		<tr>
			<td>&nbsp;</td><td><br/><input type='submit' class='input_submit' name='add_category_finish' value='".BUTTON_ADD."' /></td>
		</tr>
		</table>			
		</form><br/><br/>	
	";
}

if (isset($_GET['edit_category']) and $db_id = (int) $_GET['edit_category'] and $is_digi) {
	
	$faq_cats = array(0 => '') + makeArray($db_table_categories,'id','name');
	
	$res = eq("SELECT name, subcat_of FROM $db_table_categories WHERE id = '$db_id';");
	$myo = mfo($res);
	
	$pre_html = "
	
		<h3>".TXT_EDIT_CATEGORY.":</h3><hr/><br/>
		<form name='edit_category' method='post' action='$_file'>
		<input type='hidden' name='id' value='$db_id' />
		<table border='0px' cellspacing='0px' cellpadding='1px'>
		<tr>
			<td width='150px'>".TXT_NAME.":</td>
			<td><input type='text' name='name' class='input_regular' value='$myo->name'/></td>
		</tr>
		<tr>
			<td>".TXT_SUBCAT.":</td>
			<td>".makeSelectbox('subcat_of',$faq_cats,$myo->subcat_of)."</td>
		</tr>
		<tr>
			<td>&nbsp;</td><td><br/><input type='submit' class='input_submit' name='edit_category_finish' value='".BUTTON_SAVE."' /></td>
		</tr>
		</table>			
		</form><br/><br/>	
	";
}

if (isset($_GET['add_topic']) and $is_digi) {
	
	$faq_cats = makeArray($db_table_categories,'id','name');
	
	$pre_html = "
	
		<h3>".TXT_ADD_TOPIC.":</h3><hr/><br/>
		<form name='add_faq_topic' method='post' action='$_file'>
		<table border='0px' cellspacing='0px' cellpadding='1px'>
		<tr>
			<td width='150px'>".TXT_TITLE.":</td>
			<td><input type='text' name='title' class='input_regular'/></td>
		</tr>
		<tr>
			<td>".TXT_CATEGORY.":</td>
			<td>".makeSelectbox('faq_cat',$faq_cats,0)."</td>
		</tr>
		<tr>
			<td valign='top' colspan='2'><br/>".TXT_DESCRIPTION.":
			<br/><textarea name='descr'></textarea></td>
		</tr>
		<tr>
			<td colspan='2'><br/><input type='submit' class='input_submit' name='add_topic_finish' value='".BUTTON_ADD."' /></td>
		</tr>
		</table>			
		</form><br/><br/>
	";
}

if (isset($_GET['edit_topic']) and $db_id = (int) $_GET['edit_topic'] and $is_digi) {
	
	$faq_cats = makeArray($db_table_categories,'id','name');
	
	$res = eq("SELECT title, faq_cat, descr FROM $db_table WHERE id = '$db_id';");
	$myo = mfo($res);
	
	$pre_html = "
	
		<h3>".TXT_EDIT_TOPIC.":</h3><hr/><br/>
		<form name='edit_faq_topic' method='post' action='$_file'>
		<input type='hidden' name='id' value='$db_id' />
		<table border='0px' cellspacing='0px' cellpadding='1px'>
		<tr>
			<td width='150px'>".TXT_TITLE.":</td>
			<td><input type='text' name='title' class='input_regular' value='$myo->title'/></td>
		</tr>
		<tr>
			<td>".TXT_CATEGORY.":</td>
			<td>".makeSelectbox('faq_cat',$faq_cats,$myo->faq_cat)."</td>
		</tr>
		<tr>
			<td valign='top' colspan='2'><br/>".TXT_DESCRIPTION.":
			<br/><textarea name='descr'>$myo->descr</textarea></td>
		</tr>
		<tr>
			<td colspan='2'><br/><input type='submit' class='input_submit' name='edit_topic_finish' value='".BUTTON_SAVE."' /></td>
		</tr>
		</table>			
		</form><br/><br/>
	";
}


/******************************************************************************************
 * Page contents
 ***/

//Page title HTML
$html = "
	<a href='$_file'><img src='$site_icon' border='0px' class='cms_icon'/></a>
	<h6>
		<a href='$_file'>".TXT_OVERVIEW."</a>
		".(($is_digi) ? " &nbsp; $img[folder_add] <a href='$_file?add_category=1'>".TXT_NEW_CAT."</a>" : '')."
		".(($is_digi) ? " &nbsp; $img[fav_add] <a href='$_file?add_topic=1'>".TXT_NEW_TOPIC."</a>" : '')."
	</h6>
	<br/>".$Message->get()."
	<br/><br/>$pre_html
";

//Obtain array of main categories
$main_cats_array = $FaqEngine->getMainCats();
$cats_html = array();
   
//Loop through all main cats and build the html
$x = 0;
foreach($main_cats_array as $cat_id => $cat_title) {
	
	$cats_html[$cat_id] = '';
	
	$sub_cats_array = $FaqEngine->getSubCats($cat_id);
	if (count($sub_cats_array)) {
		$cats_html[$cat_id] .= "<h3 style='display: inline;'>$cat_title</h3>";

		//Loop through all sub cats
		foreach($sub_cats_array as $subcat_id => $subcat_title) {
			$x++;
			
			//If we get a GET request for a topic, we would want to expand the relevant subcategory
			if (isset($subcat_to_expand) and $subcat_id == $subcat_to_expand) $row_to_expand = $x;
			
			$cats_html[$cat_id] .= "<br/><span class='grey'>&raquo;</span>".($is_digi ? " <a href='$_file?edit_category=$subcat_id'>$img[edit]</a> <a href='$_file?delete_category=$subcat_id'>$img[folder_del]</a> - " : '')." <a href='JavaScript: void(0);' onclick='expand_subcat($subcat_id,$x)'><span id='subcat_title_$x'>$subcat_title</span></a><div id='subcat_expand_div_$x' style='display: inline;'></div>";
		}
	}
}

$html .= "
		<script type='text/javascript'>

			var expanded_rows = new Array();
			var subcat_titles = new Array();

			function expand_subcat(id, row) {
				if (expanded_rows[row] == 1) {
				 
				 	expanded_rows[row] = 0;
					innerHTML('subcat_expand_div_'+row,'');
					innerHTML('subcat_title_'+row, subcat_titles[row]);					
				}
				else {
				 
					ajax_query = 'get_subcat_topics=1&subcat_id='+id+'&row='+row;
					sendHttpRequest('$_file','update_page',0,'expand_subcat');
					
					expanded_rows[row] = 1;
					subcat_titles[row] = document.getElementById('subcat_title_'+row).innerHTML;					
				}				
			}
			
			function display_topic(id) {
				
				ajax_query = 'get_topic=1&topic_id='+id;
				sendHttpRequest('$_file','update_page',0,'show_topic');
			}

			function update_page(res, task) {
			 	
				if (task == 'expand_subcat') {
		 			var data = res.split('||');
					
					innerHTML('subcat_expand_div_'+data[0], '<br/>'+data[1]);
					innerHTML('subcat_title_'+data[0], '<b>'+subcat_titles[data[0]]+'</b>');
				}
				else if (task == 'show_topic') {
					
					innerHTML('topic_div', res);
				}
			}
		</script>
";

if (!isset($topic_html)) {

 	$faq_note	=	str_replace('[TECH_NAME]',	$cfg['TECH_NAME'],	TXT_FAQ_NOTE);
 	$faq_note	=	str_replace('[TECH_EMAIL]',	"<a href='$mailer$cfg[TECH_EMAIL]'>".$cfg['TECH_EMAIL']."</a>",	$faq_note);
	$topic_html = "	<i>".TXT_BROWSE."<br/><br/>$faq_note</i>.";
}

$html .= "	<table cellspacing='0px' cellpadding='1px'>
			<tr>
				<td valign='top' width='450px' id='topic_div'>$topic_html</td>
				<td valign='top' style='padding-left: 25px;'>".implode("\n\n<br/><br/>",$cats_html)."</td>
			</tr>
			</table>
";

echo $html;		

require_once('inc/cms_footer.php');
?>