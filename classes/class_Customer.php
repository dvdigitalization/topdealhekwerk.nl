<?php
/*
Title:		Main customer class
File: 		classes/class_Customer.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

class Customer {

    protected $id;

    function readFromDB() {
        $res = eq("SELECT * FROM $this->db WHERE id = '$this->id';");
        $mya = mfa($res);

        foreach ($mya as $key => $value) {
			$this->$key = $mya[$key];
		}
    }

    function getId() {
		return $this->id;
	}

	function getSince($formatted = true) {
		return $formatted ? time2date($this->timestamp) : $this->timestamp;
	}

    function getName($with_title = false) {

    	global $SEX_ARRAY;

    	if (!empty($this->company_name)) {
			return $this->company_name;
		}
        else {

        	$name = $this->last_name.
					(!empty($this->first_name) ? ', '.$this->first_name : (!empty($this->initials) ? ', '.$this->initials : '')).
					(!empty($this->prefix) ? ' '.$this->prefix : '');
        	return $with_title ? $SEX_ARRAY[$this->title].$name : $name;
		}
    }

    function getLabelName() {
		return $this->getName(true);
	}

    function getTitle($full = false) {
    	global $SEX_ARRAY;
        return $full ? $SEX_ARRAY[$this->title] : $this->title;
    }

    function getAddress() {
        return $this->address;
    }

	function getAddressNr() {
		return $this->nr;
	}

	function getPostalCode() {
		return $this->postal_code;
	}

	function getCity() {
		return $this->city;
	}

	function getCountry($full = false) {
		global $COUNTRY_ARRAY;
		if ($this->country == 0) {
			$this->country = 124;
		}
		return $full ? $COUNTRY_ARRAY[$this->country] : $this->country;
	}

	function getAddressLabel($br = '<br/>') {
		$label = 	$this->getLabelName();
		$label .=	($address = trim($this->getAddress().' '.$this->getAddressNr())) ? $br.$address : '';
		$label .=	($pc_city = trim($this->getPostalCode().' '.$this->getCity())) ? $br.$pc_city : '';
		$label .=	($country = $this->getCountry(true)) ? $br.$country : '';

		return $label;
	}

	function getPhone() {
		return $this->phone;
	}

	function getEmail() {
		return $this->email;
	}

	function getEmailLink() {
		if (empty($this->email)) return null;
		global $mailer;
		return "<a href='$mailer$this->email' title='Stuur deze klant een email'>$this->email</a>";
	}

	function getRemarks() {
		return $this->remarks ? nl2br($this->remarks) : 'Geen';
	}

	function isDeleted() {
		return $this->deleted ? true : false;
	}
}
?>