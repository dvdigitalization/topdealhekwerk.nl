<?php 
/*
Title:		RSS parser class
File: 		classes/class_RSS.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

class RSS {

	private $file,$cached_file;
	private $rss;
	
	private $output = array();
	
	
	function RSS() {
		$this->rss = xml_parser_create();
		
		xml_set_object($this->rss, $this);  
		xml_parser_set_option($this->rss, XML_OPTION_CASE_FOLDING, false); 
		xml_parser_set_option($this->rss, XML_OPTION_SKIP_WHITE, true); 
		xml_set_element_handler($this->rss, "startelement", "endelement");
		xml_set_character_data_handler($this->rss, "characterdata"); 		
	}	
	
	function getFile($rss_url) {
		
	}	
}

?>