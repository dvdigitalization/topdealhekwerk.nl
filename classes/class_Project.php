<?php
/*
Title:		Project class
File: 		classes/class_Project.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

class Project {

    private $id;

    function Project($id) {
    	
    	global $db_table, $db_table_hours;
    	
        $this->id 		= 	$id;
        $this->db 		= 	$db_table;
        $this->db_hours	=	$db_table_hours;
        
        $this->getInfoFromDB();
    }
    
    function getInfoFromDB() {
        $res = eq("SELECT *,UNIX_TIMESTAMP(start_date) AS start_date,UNIX_TIMESTAMP(finish_date) AS finish_date,UNIX_TIMESTAMP(deadline) AS deadline FROM $this->db WHERE id = '$this->id';");
        $mya = mfa($res);
        
        foreach ($mya as $key => $value) {
			$this->$key = $mya[$key];
		}
    }
    
    function getId() {
		return $this->id;
	}

	static function getNiceProjectId($id) {
		
		global $cfg;
		return sprintf($cfg['PROJECT_PREFIX'].'%0'.($cfg['PROJECT_L_ZEROES']).'d', ($id + $cfg['PROJECT_CONST']));	
	} 
	
	function getStartDate($formatted = true) {
		return $formatted ? time2date($this->start_date) : $this->start_date;
	}

	function getEndingDate($formatted = true) {
		return $this->isFinished() ? $this->getFinishDate($formatted) : $this->getDeadline($formatted);
	}

	function getFinishDate($formatted = true) {
		return $formatted ? time2date($this->finish_date) : $this->finish_date;
	}
	
	function getDeadline($formatted = true) {
		return $formatted ? time2date($this->deadline) : $this->deadline;
	}
	
	function getEstimatedTime() {
		return $this->estimated_time;
	}
	
	function getActualTime() {
		
		$hours_booked_total = 0;
		
		$res = eq("SELECT hours_booked FROM $this->db_hours WHERE project_id = '$this->id';");
		while ($myo = mfo($res)) {
			$hours_booked_total 	+= $myo->hours_booked;
		}
		
		return $hours_booked_total;
	}

	function getChargedTime() {
		
		$hours_charged_total = 0;
		
		$res = eq("SELECT hours_charged FROM $this->db_hours WHERE project_id = '$this->id';");
		while ($myo = mfo($res)) {
			$hours_charged_total 	+= $myo->hours_charged;
		}
		
		return $hours_charged_total;
	}
	    
    function getName() { 
        return $this->name;
    }
    
    function getCustomer($full = true) {
    	global $CUSTOMERS_ALL_ARRAY;
		return $full ? $CUSTOMERS_ALL_ARRAY[$this->customer_id] : $this->customer_id;
	}

    function getPriority($full = true) {
    	global $PRIORITIES_ARRAY;
		return $full ? $PRIORITIES_ARRAY[$this->priority] : $this->priority;
	}
	
	function getInvoiceId() {
		global $db_table_relations;
		return getMyo($db_table_relations, "project_id = '$this->id' AND invoice_id > 0", 'invoice_id');
	}
	
	function getInvoiceNo($with_link = true) {
		global $db_table_invoices, $db_table_relations, $file_invoices, $cfg;
		$invoice_id	=	getMyo($db_table_relations, "project_id = '$this->id'", 'invoice_id');
		$invoice 	= 	$invoice_id ? getMyo($db_table_invoices, $invoice_id, array('id','invoice_no','invoice_date')) : 0;
		
		if ($invoice) {
			
			$invoice_year 	= 	date('Y', date2time(time2date($invoice->invoice_date)));
			$invoice_no		=	$invoice->invoice_no;
			
			if ($cfg['INVOICE_YEAR_PREFIX']) {
				$prefix = $invoice_year.$cfg['INVOICE_YEAR_SUFFIX'];
			}
			$inv = sprintf($cfg['INVOICE_PREFIX'].$prefix.'%0'.($cfg['INVOICE_L_ZEROES']).'d', ($invoice_no + $cfg['INVOICE_CONST']));
			return $with_link ? "<a href='$file_invoices?view=$invoice->id' title='Factuur gegevens inzien'>$inv</a>" : $inv;
				
		}
		return 'geen';
	}
	
	function getRevenue($parsed = true, $return_zero = true) {
		
		global $db_table_invoice_articles;
		$excl = 0;
		
		$res = eq("SELECT amount, price FROM $db_table_invoice_articles WHERE invoice_id = '".$this->getInvoiceId()."';");
		while ($myo = mfo($res)) {
			$excl +=	$myo->amount * $myo->price;	
		}
		if (!$return_zero and $excl == 0)	return '';
		return $parsed ? parseAmount($excl,1) : $excl;
	}
	
	function getRevenuePerHourBooked($parsed = true) {
		if ($hours_booked = $this->getActualTime()) {
			$revenue = round($this->getRevenue(false) / $hours_booked);
		}
		else $revenue = 0;
		return $parsed ? parseAmount($revenue,1) : $revenue;
	}

	function getRevenuePerHourCharged($parsed = true) {
		if ($hours_charged = $this->getChargedTime()) {
			$revenue = round($this->getRevenue(false) / $hours_charged);
		}
		else $revenue = 0;
		return $parsed ? parseAmount($revenue,1) : $revenue;
	}
    
	function getDescription() {
		return $this->description ? nl2br($this->description) : 'Geen';
	}
	
	function isFinished() {
		return $this->finish_date ? true : false;
	}
	
	function isDeleted() {
		return $this->deleted ? true : false;
	}
	
	function hasInvoice() {
		return $this->getInvoiceId();
	}
}
?>