<?php
/*
Title:		Image handler class
File: 		classes/class_ImageHandler.php
Version: 	v2.04
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

define ('CONSTRAINT_SIZE',		1);
define ('CONSTRAINT_WIDTH',		2);
define ('CONSTRAINT_HEIGHT',	3);
define ('CONSTRAINT_CROP',		4);

class ImageHandler {

	private $source_image, $source_type, $target_image, $target_type;
	private $source_width, $source_height, $source_ratio, $target_width, $target_height, $target_ratio;
	private $error, $crop;
	
	private $use_watermark, $watermark, $watermark_color, $watermark_size;

	function ImageHandler($source_image = '', $target_image = '', $target_type = IMAGETYPE_JPEG) {
		
		$this->use_watermark = false;
		
		$this->setCrop(false);
		$this->setSource($source_image);
		$this->setTarget($target_image);
		$this->setTargetType($target_type);
	}
	
	function setSource($source_image) {
		
		$this->source_image	=	$source_image;
		
		if (!empty($this->source_image)) {
			if (!file_exists($this->source_image)) {
				$this->error	=	DIGI_ERR_IMGHANDLER_NOFILE;
			}
			else {
				$this->determineSourceInfo();
			}
		}
	}
	
	function setTarget($target_image) {
		$this->target_image	=	$target_image;
	}
	
	function setTargetType($target_type = IMAGETYPE_JPEG) {
		$this->target_type	=	$target_type;
	}
	
	function setTargetSize($width, $height, $crop = false) {
		$this->target_width		=	$width;
		$this->target_height	=	$height;
		$this->crop				= 	$crop;
	}

	function setCrop($crop = true) {
		$this->crop	= $crop;
	}
	
	function setTargetSizeConstraint($max_size, $constraint_type = CONSTRAINT_SIZE) {
		
		//Check for a valid source height/width
		if (!$this->source_width or !$this->source_height) {
			return false;
		}
		
		//Max height constraint
		if ($constraint_type == CONSTRAINT_HEIGHT 	or ($constraint_type == CONSTRAINT_SIZE
													and $this->source_width < $this->source_height)){
		    $this->target_height 	= 	$max_size;
			$this->target_width 	= 	$this->source_width * $max_size / $this->source_height;			
		}
		
		//Max width constraint
		elseif ($constraint_type == CONSTRAINT_WIDTH  or ($constraint_type == CONSTRAINT_SIZE
													  and $this->source_width > $this->source_height)){
			$this->target_width 	= 	$max_size;
			$this->target_height 	= 	$this->source_height * $max_size / $this->source_width;
		}
		
		//Square constraint
		else {
			$this->target_width 	= 	$max_size;
			$this->target_height 	= 	$max_size;
		}
	}
	
	function setWatermark($watermark, $color='255,255,255', $size=2) {
		$this->use_watermark = true;
		$this->watermark = $watermark;
		$this->watermark_color = explode(',',$color);
		$this->watermark_size = $size;				
	}

	function getTargetSize() {
		return array($this->target_width, $this->target_height);
	}
	
	function determineSourceInfo() {
		
		if ($info_array = getimagesize($this->source_image)) {
			list ($source_width, $source_height, $source_type) = $info_array;
			if ($source_width == 0 or $source_height == 0) {
				$this->error	=	DIGI_ERR_IMGHANDLER_NOSIZE;
			}
			elseif ($source_type == IMAGETYPE_JPEG or $source_type == IMAGETYPE_GIF or $source_type == IMAGETYPE_PNG) {
				$this->source_width			=	$source_width;
				$this->source_height		=	$source_height;
				$this->source_type			=	$source_type;
				$this->source_ratio			=	$source_width/$source_height;
				return true;
			}
		}
		$this->error	=	DIGI_ERR_IMGHANDLER_WRONGTYPE;
	}
	
	function outputTarget() {

		global $chmod;

		//Check for a valid source height/width
		if (!$this->source_width or !$this->source_height) {
			return false;
		}

		//Set target image size if not set
		if (!$this->target_width or !$this->target_height) {
			$this->target_width		= 	$this->source_width;
			$this->target_height	= 	$this->source_height;
		}
		
		//Set target ratio
		$this->target_ratio		=	$this->target_width/$this->target_height;
		
		//Set the target type if not set
		if (!$this->target_type) $this->target_type = IMAGETYPE_JPEG;
		
		//Make the source image
		switch ($this->source_type) {
			case IMAGETYPE_JPEG:
				$src_image 		= 	@imagecreatefromjpeg($this->source_image);
				break;
			case IMAGETYPE_GIF:
				$src_image 		= 	@imagecreatefromgif($this->source_image);
				break;
			case IMAGETYPE_PNG:
				$src_image 		= 	@imagecreatefrompng($this->source_image);
				break;
			default:
				$this->error	=	DIGI_ERR_IMGHANDLER_WRONGTYPE;
				return false;				
		}
		
		//Error?
		if (!$src_image) {
			$this->error	=	DIGI_ERR_IMGHANDLER_SOURCE_FAIL;
			return false;
		}
		
		//Make the target image
		if ($this->target_type == IMAGETYPE_GIF) {
			if (!$trg_image = imagecreate($this->target_width, $this->target_height)) {
				$this->error	=	DIGI_ERR_IMGHANDLER_CREATE_FAIL;
				return false;
			}
		}
		else {
			if (!$trg_image = imagecreatetruecolor($this->target_width, $this->target_height)) {
				$this->error	=	DIGI_ERR_IMGHANDLER_CREATE_FAIL;
				return false;
			}
		}
		
		//Default, for no cropping
		$src_x = $src_y = 0;
		$src_w = $this->source_width;
		$src_h = $this->source_height;
		
		//Cropping required?
		if ($this->crop) {
			
			//We need to crop the width
			if ($this->source_ratio > $this->target_ratio) {
				$src_w 	=	round($this->target_width * ($this->source_height/$this->target_height));
				$src_x 	= 	round(($this->source_width - $src_w) / 2);
			}
			//We need to crop the height
			elseif ($this->source_ratio < $this->target_ratio) {
				$src_h 	=	round($this->target_height * ($this->source_width/$this->target_width));
				$src_y 	= 	round(($this->source_height - $src_h) / 2);
			}
		}

		//Background fill for GIF, to make transparent colors white instead of black
		if ($this->source_type == IMAGETYPE_GIF and $this->target_type == IMAGETYPE_JPEG) {
			imagefill($trg_image,0,0,imagecolorallocate ($trg_image, 255, 255, 255));
		}

		//Copy the source to the target
		if (!imagecopyresampled(	$trg_image, $src_image, 0, 0, $src_x, $src_y, 
									$this->target_width, $this->target_height, $src_w, $src_h)) {
			$this->error 	=	DIGI_ERR_IMGHANDLER_COPY_FAIL;
			return false;								
		}
		
		//Put a watermark on the picture if required
		if ($this->use_watermark) {
			
			$xpos = 5;
			$ypos = $this->target_height - 8*$this->watermark_size;
			
			$string_color = imagecolorallocate($trg_image,$this->watermark_color[0],$this->watermark_color[1],$this->watermark_color[2]);
			imagestring($trg_image,2,$xpos,$ypos,$this->watermark,$string_color);
		}
		
		//Output the target according to type
		switch ($this->target_type) {
			case IMAGETYPE_JPEG:
				if (@imagejpeg($trg_image, $this->target_image, 100)) {
					if ($chmod) @chmod($this->target_image, $chmod);
					return true;
				}
				break;
			case IMAGETYPE_GIF:
				if (@imagegif($trg_image, $this->target_image)) {
					if ($chmod) @chmod($this->target_image, $chmod);
					return true;
				}
				break;
			case IMAGETYPE_PNG:
				if (@imagepng($trg_image, $this->target_image, 100)) {
					if ($chmod) @chmod($this->target_image, $chmod);
					return true;
				}
				break;
			default:
				$this->error	=	DIGI_ERR_IMGHANDLER_WRONGTGTYPE;
				return false;				
		}
		
		//Something went wrong!
		$this->error 	=	DIGI_ERR_IMGHANDLER_OUTPUT_FAIL;
	}
	
	function output() {
		
		$this->outputTarget();
		
		global $_error;
		if (!$_error = $this->getError()) {
			return true;
		}
		return false;
	}
	
	function getError() {
		return $this->error;
	}
}
?>