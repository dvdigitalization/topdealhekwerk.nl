<?php
/*
Title:		Newsletter class
File: 		classes/class_Newsletter.php
Version: 	v2.20
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

define('NO_RECIPIENTS_FOUND',	1);
define('NL_TEST_SENT',			2);
define('NL_REAL_SENT',			3);
define('ALREADY_SENT',			4);

class Newsletter {

	//Constructor
	function Newsletter($id, $test = false) {

		global $cfg, $date_format, $from_email, $test_addresses, $template_nl_layout, $_lang;

		//Settings
		$this->db_table 			= 	DIGI_DB_PREFIX.'newsletters';
		$this->db_table_m2g			=	DIGI_DB_PREFIX.'mails2go';
		$this->from_email			=	$from_email;
		$this->date_format			=	$date_format;
		$this->test_addresses		=	explode_recipients($test_addresses);

		//Set id and test parameter
		$this->id 		= 	(int) $id;
		$this->test 	=	$test;

		//Initialize vars and arrays
		$this->subscribers_sent		=	0;
		$this->recipients			=	array();
		$this->names				=	array();
		$this->subscribers_tables	=	array();
		$this->email_fields			=	array();
		$this->newsletter_fields	=	array();
		$this->language_fields		=	array();
		$this->language				=	$_lang;

		//Load newsletter from DB if id is given
		if ($this->id) {
			$this->readFromDB();
		}
	}

	//Read from database
	function readFromDB() {

		//Load data from DB
		$res = eq("	SELECT 	edition, language, subject, message, subscribers_sent, UNIX_TIMESTAMP(timestamp) AS timestamp
					FROM 	$this->db_table WHERE id = '$this->id';");
		$myo = mfo($res);

		//Set variables
		$this->edition			=	$myo->edition;
		$this->language			=	$myo->language;
		$this->subject			=	$myo->subject;
		$this->message			=	$myo->message;
		$this->timestamp		=	$myo->timestamp;
		$this->subscribers_sent	=	$myo->subscribers_sent;
	}

	//Save to database method
	function saveToDB() {

	 	//Data cleansing
	 	evalAll($_POST['subject']);
	 	evalAll($_POST['message'], false);

		//Set variables
		$this->subject		=	$_POST['subject'];
		$this->message		=	$_POST['message'];

		//Insert
		if (!$this->id) {

			//Get the language
			$this->language		=	$_POST['language'];

			//Set the edition, insert and set id
			$this->edition 	= getMyo($this->db_table, "language = '$this->language'",'edition','edition','DESC',1) + 1;
			$this->id 		= insRec($this->db_table,
									array('edition','language','subject','message'),
									array($this->edition,$this->language,$this->subject,$this->message)
								);

			return $this->id;
		}

		//Update
		else {

			$uprec = upRec($this->db_table, $this->id,
					array('subject','message'),
					array($this->subject, $this->message)
			);

			//Return true if no change was made (0 rows affected) or if a change was made (1 row affected).
			if ($uprec === 0 or $uprec) return $this->id;
		}
	}

	//Set subscriber tables
	function setSubscribers($subscribers) {

		//Go over each subscriber set
		foreach ($subscribers as $subscribers_data) {

			//Set the variables
			list ($db_table, $email_field, $newsletter_field, $language_field) = $subscribers_data;

			//Add to our arrays
			$this->subscribers_tables[]	=	$db_table;
			$this->email_fields[]		=	$email_field;
			$this->newsletter_fields[]	=	$newsletter_field;
			$this->language_fields[]	=	$language_field;
		}
	}

	//Method to get a summary of how many recipients will be mailed
	function howManyRecipients() {

		//Initialize counter
		$total_recipients 	= 	0;
		$recipients			=	array();

		//Loop over the tables
		foreach ($this->subscribers_tables as $key=> $subscribers_table) {

			//Set the relevant database fields
			$email_field		=	$this->email_fields[$key];
			$newsletter_field	=	$this->newsletter_fields[$key];
			$language_field		=	$this->language_fields[$key];

			//Perform the query
			$res = eq("	SELECT 		$email_field AS email, $language_field AS lang
						FROM 		$subscribers_table
						WHERE 		$newsletter_field = '1' AND $email_field <> ''
						ORDER BY 	$email_field ASC
			;");

			//Loop the results
			while ($myo = mfo($res)) {

				//Trim the email
				$email = trim($myo->email);

				//If not found yet in array, add as recipient
				if (!in_array($email, $this->recipients)) {
					if (!isset($recipients[$subscribers_table.' ('.$myo->lang.')'])) {
						$recipients[$subscribers_table.' ('.$myo->lang.')'] = 0;
					}
					$recipients[$subscribers_table.' ('.$myo->lang.')']++;
					$total_recipients++;
				}
			}
		}
		$recipients['digi_total'] = $total_recipients;

		//Return recipients
		return $recipients;
	}

	//Method to prepare all valid recipients
	function prepareRecipients() {

		//Initialize counter
		$total_recipients = 0;

		//Loop over the tables
		foreach ($this->subscribers_tables as $key=> $subscribers_table) {

			//Name field(s)
			if ($subscribers_table == DIGI_DB_PREFIX.'newsletter_subscribers') {
				$name_fields = ", name";
			}
			elseif ($subscribers_table == DIGI_DB_PREFIX.'customers_shop') {
				$name_fields = ", first_name, last_name";
			}
			else {
				$name_fields = '';
			}

			//Set the relevant database fields
			$email_field		=	$this->email_fields[$key];
			$newsletter_field	=	$this->newsletter_fields[$key];
			$language_field		=	$this->language_fields[$key];

			//Perform the query
			$res = eq("	SELECT 		$email_field AS email $name_fields
						FROM 		$subscribers_table
						WHERE 		$newsletter_field = '1' AND $email_field <> '' AND $language_field = '$this->language'
						ORDER BY 	$email_field ASC
			;");

			//Loop the results
			while ($myo = mfo($res)) {

				//Trim the email
				$email = trim($myo->email);

				//If not found yet in array, add as recipient
				if (!in_array($email, $this->recipients)) {
					$total_recipients++;
					$this->recipients[] = $email;

					//Set name
					if (isset($myo->name)) {
						$name = $myo->name;
					}
					elseif (isset($myo->first_name) and isset($myo->last_name)) {
						$name = $myo->first_name.' '.$myo->last_name;
					}
					else {
						$name = '';
					}

					$this->names[$email] = $name;
				}
			}
		}

		//Return total amount of recipients
		return $total_recipients;
	}

	//The core sending method
	function send() {

		//Do not send if already sent! -.-
		if ($this->subscribers_sent) {
			return ALREADY_SENT;
		}

		//Update the timestamp at this point, if none set already
		if (!$this->timestamp) {
			$this->timestamp = time();
		}

		//Was this a test?
		if ($this->test) {

			//Extra check for the test addresses
			if (count($this->test_addresses)) {
				sendMail($this->test_addresses, $this->getSubject(), $this->getHTML(), $this->from_email, '','',true);
				return NL_TEST_SENT;
			}
			else {
				return NO_RECIPIENTS_FOUND;
			}
		}

		//Prepare recipients, fail if none found
		if (!$this->prepareRecipients()) {
			return NO_RECIPIENTS_FOUND;
		}

		//Get the base HTML and title
		$newsletter_html 	= 	$this->getHTML();
		$newsletter_subject	=	$this->getSubject();

		//Start going through the recipients
		foreach ($this->recipients as $recipient) {

			//Make the recipient specific HTML
			$html =  $this->getSpecificHTML($newsletter_html, $recipient);

			//Put all emails in the mails2go system
			insRec($this->db_table_m2g, 	array('sender', 'recipient', 'subject', 'body', 'is_html', 'newsletter_id'),
											array($this->from_email, $recipient, $newsletter_subject, $html, 1, $this->id)
			);

			//Update counter
			$this->subscribers_sent++;
		}

		//Update the newsletter entry and return success code
		upRec($this->db_table, $this->id,
			array('subscribers_sent','timestamp'),
			array($this->subscribers_sent,mysql_time($this->timestamp))
		);

		return NL_REAL_SENT;
	}

	//Get recipient specific HTML
	function getSpecificHTML($newsletter_html, $recipient) {

		global $_link;

		//Transparent pixel to count times openend
		$pixel 	= 	"<img src='".$_link."inc/times_opened_counter.php?newsletter=$this->id&hash=".
					sha1($this->id.'timesopenedhash123')."&r=$recipient&rv=".sha1($recipient.'recipienthash456')."' />";

		//Replace tag
		$newsletter_html = str_replace('</body>', $pixel.'</body>', $newsletter_html);

		//Name
		if (isset($this->names[$recipient])) {
			$newsletter_html = str_replace('[NAAM]', $this->names[$recipient], $newsletter_html);
		}
		else {
			$newsletter_html = str_replace('[NAAM]', '', $newsletter_html);
		}

		//Return HTML
		return $newsletter_html;
	}

	//Function to get the HTML of the newsletter
	function getHTML() {

		global $sign_off_url, $_sep, $_link;

		//The newsletter layout template
		$template_nl_layout	= CMS_PATH_PREFIX.'template'.$_sep.'nieuwsbrief'.$_sep.'nieuwsbrief_layout_'.$this->language.'.html';

		//Get layout from template
		$nl_html = file_get_contents($template_nl_layout);

		//Set the date
		$date = date($this->date_format,($this->timestamp ? $this->timestamp : time()));

		//Make the replacements
		$nl_html = str_replace('[SUBJECT]',			$this->subject,		$nl_html);
		$nl_html = str_replace('[EDITION]',			$this->edition,		$nl_html);
		$nl_html = str_replace('[CONTENTS]',		$this->message,		$nl_html);
		$nl_html = str_replace('[DATE]',			$date,				$nl_html);
		$nl_html = str_replace('[SIGN_OFF_URL]',	$sign_off_url,		$nl_html);
		$nl_html = str_replace('[LINK]',			$_link, 			$nl_html);

		return $nl_html;
	}

	//Get the subject
	function getSubject() {

		if ($this->test) 	return '*TEST* '.$this->subject.' *TEST*';
		else 				return $this->subject;
	}
}
?>