<?php
/*
Title:		Invoice class
File: 		classes/class_Invoice.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

class Invoice {
    
    static function addArticleToInvoice($invoice_id, $article_text, $amount, $unit, $price, $vat_level) {
	
		global $db_table_inv_art;
	
		if ($inv_art = getMyo(	$db_table_inv_art, "invoice_id = '$invoice_id' AND article_text = '$article_text' AND price = '$price'",
							 	array('id','amount'))) {
			upRec($db_table_inv_art, $inv_art->id, 'amount', $inv_art->amount + $amount);
			$inv_id = $inv_art->id;
		}
		else {
			$inv_id = insRec($db_table_inv_art,
				array('invoice_id','article_text','amount','unit','price','vat_level'),
				array($invoice_id, $article_text, $amount, $unit, $price, $vat_level)
			);
		}
		return $inv_id;
	}

	static function deleteArticleFromInvoice($article_id) {

		global $db_table_inv_art;
		
		return delRec($db_table_inv_art,$article_id);
	}
	

	static function getNextInvoiceNo() {			
		
		global $cfg, $db_table; 
		 
		//If we want to reset the invoice number each year, we look just for this year
	 	if ($cfg['INVOICE_NO_RESET_EACH_YEAR']) {
		   $this_year = mktime(0,0,0,1,1,date('Y',time()));
   		   $res = eq("SELECT invoice_no FROM $db_table WHERE invoice_date > FROM_UNIXTIME($this_year) ORDER BY invoice_no DESC LIMIT 1;");
		}
		
		//Otherwise, just take the last number
		else {
			$res = eq("SELECT invoice_no FROM $db_table ORDER BY invoice_no DESC LIMIT 1;");			
		}
		
		$myo = mfo($res);
		if (is_object($myo)) {
			return $myo->invoice_no + 1;
		}
		return 1;
	}
	
	static function getNiceInvoiceID($invoice_id, $invoice_year) {
	
		global $cfg;
	
		if ($invoice_id == 0) return 'geen';
		return sprintf(str_replace('[YEAR]',$invoice_year, $cfg['INVOICE_NO_FORMAT_ADM']), $invoice_id + $cfg['INVOICE_CONST']);
	}
	
	static function getInvoiceTotalAmount($invoice_id) {
		
		global $vat_array, $db_table_inv_art;
		
		$articles = array();
		$excl = $incl = 0;
		
		$res = eq("SELECT amount, price, vat_level FROM $db_table_inv_art WHERE invoice_id = '$invoice_id';");
		while ($myo = mfo($res)) {
			$excl +=	$myo->amount * $myo->price;	
			$incl +=	calcInclValue(($myo->amount * $myo->price),0,$myo->vat_level); 
		}
		return array($excl, $incl);
	}
	
	static function paidInvoice($invoice_id, $paid_date) {
		
		global $db_table;
		upRec($db_table, $invoice_id, 'paid_date', $paid_date);
	}
}
?>