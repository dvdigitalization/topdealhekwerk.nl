<?php
/*
Title:		Filesharing class
File: 		classes/class_FileSharing.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

define('ROOT_FOLDER',	0);
define('TRASH_FOLDER',	1);

class FileSharing {
	
	
	function FileSharing($db_table, $files_folder) {
		
		$this->db_table			=	$db_table;
		$this->files_folder		=	$files_folder;
	}
	
	// *** Get files folder
	function getFilesFolder() {
		return $this->files_folder;
	}
	
	// *** Make a new folder
	function makeNewFolder($folder_name, $location) {

		evalTrim($folder_name);
		
		$res = eq("SELECT id FROM $this->db_table WHERE filename = '$folder_name' AND in_folder = '$location';");
		if (mysql_num_rows($res)) {
			return false;	
		}
		
		$f_id = insRec($this->db_table, array('filename', 'url', 'timestamp', 'in_folder'),
										array($folder_name, '', time(), $location));
		return $f_id;		
	}
	
	// *** Add a new file
	function addNewFile($file_name, $file_url, $to_location) {
		
		evalTrim($file_name);
		$file_name = filenameSafe($file_name, false, false);
		
		$res = eq("SELECT id FROM $this->db_table WHERE filename = '$file_name' AND in_folder = '$to_location';");
		if (mysql_num_rows($res)) {
			return false;	
		}
		
		insRec($this->db_table, array('filename', 'url', 'timestamp', 'in_folder'),
								array($file_name, $file_url, time(), $to_location));
		return true;	
	}
	
	// *** Get a new file url
	function getNewFileUrl() {
		
		$new_url = 'df_'.time().'_'.substr(microtime(),2,3);
		
		while (file_exists($this->files_folder.'/'.$new_url)) {
			$new_url = 'df_'.time().'_'.substr(microtime(),2,3);
		}
		
		return $new_url;
	}
	
	// *** Get new available filename
	function getNewFilename($old_name, $folder) {
		
		$new_name	=	$old_name;
		$x			=	1;
		
		$res = eq("SELECT id FROM $this->db_table WHERE filename = '$old_name' AND in_folder = '$folder' LIMIT 1;");
		
		while (mysql_num_rows($res)) {
			$x++;
			
			$dot 		= strrpos($old_name, '.');
			$new_name 	= substr($old_name, 0, $dot)." ($x)".getFileExtension($old_name,true);
			
			$res = eq("SELECT id FROM $this->db_table WHERE filename = '$new_name' AND in_folder = '$folder' LIMIT 1;");
			
			if ($x > 100) {
				$new_name = substr(microtime(),2,3).'_'.$old_name;
			}
		}
		
		return $new_name;
	}
	
	// *** Rename a file
	function renameFile($file, $new_name) {
		
		evalTrim($new_name);
		$new_name = filenameSafe($new_name, false, false);
		
		$res = eq("SELECT in_folder FROM $this->db_table WHERE id = '$file';");
		$myo = mfo($res);
		
		$res = eq("SELECT id FROM $this->db_table WHERE filename = '$new_name' AND in_folder = '$myo->in_folder';");
		if (mysql_num_rows($res)) {
			return false;	
		}
		
		upRec($this->db_table, $file, 'filename', $new_name);
		return true;
	}

	// *** Copy a file
	function copyFile($file, $new_location, $chmod) {

		$res = eq("SELECT * FROM $this->db_table WHERE id = '$file';");
		$myo = mfo($res);
		
		//File
		if ($myo->url) {
			$new_name 	=	$this->getNewFilename($myo->filename, $new_location);
			$new_url 	= 	$this->getNewFileUrl();
			
			if (@copy($this->files_folder.'/'.$myo->url, $this->files_folder.'/'.$new_url)) {
				if ($chmod) @chmod($this->files_folder.'/'.$new_url, $chmod);
			
				insRec($this->db_table, array('filename', 'url', 'timestamp', 'in_folder'),
										array($new_name, $new_url, time(), $new_location));
				return true;		
			}
			else {
				return false;
			}	
		}
		//Folder
		else {
			$new_name 	=	$this->getNewFilename($myo->filename, $new_location);

			//The folder itself
			$new_folder = insRec($this->db_table, 	array('filename', 'url', 'timestamp', 'in_folder'),
													array($new_name, '', time(), $new_location));
									
			//And all the files
			$res = eq("SELECT * FROM $this->db_table WHERE in_folder = '$file';");
			while($myo = mfo($res)) {
				
				//Since this folder is new, there will be no name conflicts. All we need is a new file url.
				$new_name	=	$myo->filename;
				$new_url 	= 	$this->getNewFileUrl();
				
				//Copy the files
				if (@copy($this->files_folder.'/'.$myo->url, $this->files_folder.'/'.$new_url)) {
					if ($chmod) @chmod($this->files_folder.'/'.$new_url, $chmod);
				
					insRec($this->db_table, array('filename', 'url', 'timestamp', 'in_folder'),
											array($new_name, $new_url, time(), $new_folder));		
				}
			}
			
			return true;		
		}
	}
	
	// *** Move a file
	function moveFile($file, $new_location) {
		
		$res = eq("SELECT url,filename FROM $this->db_table WHERE id = '$file';");
		$myo = mfo($res);
		
		//Check if file/folder with the same name already exists in target location
		$res = eq("SELECT id FROM $this->db_table WHERE filename = '$myo->file_name' AND in_folder = '$new_location';");
		if (mysql_num_rows($res)) {
			return false;
		}
		
		//Update the file location			
		upRec($this->db_table, $file, 'in_folder', $new_location);
		return true;	
	}
	
	// *** Trash a file/folder
	function trashFile($file) {
		
		upRec($this->db_table, $file,'deleted',1);
		upRec($this->db_table, "in_folder = '$file'",'deleted',1);
		
		return true;
	}

	// *** Restore a file/folder
	function restoreFile($file) {
		
		upRec($this->db_table, $file,'deleted',0);
		upRec($this->db_table, "in_folder = '$file'",'deleted',0);
		
		return true;
	}
	
	// *** Empty the entire trashcan
	function emptyTrash() {
		
		//First delete the actual files
		$res = eq("SELECT url FROM $this->db_table WHERE deleted = 1 AND url <> '';");
		while ($myo = mfo($res)) {
			if (file_exists($this->files_folder.'/'.$myo->url)) {
				@unlink($this->files_folder.'/'.$myo->url);
			}
		}
		
		//Now all the entries in the database
		delRec($this->db_table,"deleted = '1'");
		return true;
	}	

	// *** Restore the entire trashcan
	function restoreAll() {
		
		upRec($this->db_table,"deleted = '1'", 'deleted', 0);
		return true;
	}	

	// *** Delete a file permanently
	function deleteFile($file) {
		
		$res = eq("SELECT url FROM $this->db_table WHERE id = '$file';");
		$myo = mfo($res);
		
		//A file
		if ($myo->url) {
			@unlink($this->files_folder.'/'.$myo->url);
			delRec($this->db_table, $file);
		}
		//A folder
		else {
			
			$res = eq("SELECT id FROM $this->db_table WHERE in_folder = '$file';");
			while ($myo = mfo($res)) {
				$this->deleteFile($myo->id);
			}
		}
		
		return true;
	}
	
	// *** Offer a file for download
	function downloadFile($file) {
		
		$res = eq("SELECT url, filename FROM $this->db_table WHERE id = '$file';");
		$myo = mfo($res);
		
		if (file_exists($this->files_folder.'/'.$myo->url)) {
				
				return $this->files_folder.'/'.$myo->url.'||'.$myo->filename;
		}
		else {
			return false;
		}
	}
	
	// *** Determine if a folder has subfolders
	function hasSubfolders($dir) {
		
		$res = eq("SELECT id FROM $this->db_table WHERE url = '' AND in_folder = '$dir' LIMIT 1;");
		if (mysql_num_rows($res)) {
			return true;
		}
		return false;
	}
	
	// *** Get the parent folder id
	function getParent($id) {

		$res = eq("SELECT in_folder FROM $this->db_table WHERE id = '$id';");
		if (mysql_num_rows($res)) {
			$myo = mfo($res);
			return $myo->in_folder;
		}
		return 0;
	}
	
	// *** Get the name
	function getName($id) {
		$res = eq("SELECT filename FROM $this->db_table WHERE id = '$id';");
		if (mysql_num_rows($res)) {
			$myo = mfo($res);
			return $myo->filename;
		}
		return '';		
	}
	
	// *** Find extension in list of known extensions
	function getFileType($extension) {
		
		switch ($extension) {
			case '':
				return 'Typeloos bestand';
			case 'jpg':
			case 'jpeg':
				return 'JPEG afbeelding';
			case 'gif':
				return 'GIF afbeelding';
			case 'png':
				return 'PNG afbeelding';
			case 'bmp':
				return 'Bitmap afbeelding';
			case 'doc':
			case 'docx':
				return 'Word document';
			case 'xls':
			case 'xlsx':
				return 'Excel document';
			case 'rtf':
			case 'txt':
				return 'Textbestand';
			case 'pdf':
				return 'PDF document';
			case 'css':
				return 'Stylesheet';
			case 'html':
			case 'htm':
				return 'HTML bestand';
			case 'php':
				return 'PHP code';
			case 'js':
				return 'JavaScript code';
			default:
				return strtoupper($extension).' bestand';
		}
	}
	
	// *** Find extension in list of known extensions
	function getFileIcon($extension) {
		
		global $img;
		
		switch ($extension) {
			case 'jpg':
			case 'jpeg':
			case 'gif':
			case 'bmp':
			case 'png':
				return $img['image'];
			case 'doc':
			case 'docx':
			case 'rtf':
			case 'txt':			
				return $img['doc'];
			case 'xls':
			case 'xlsx':
				return $img['excel'];
			case 'pdf':
				return $img['pdf'];
			case 'css':
			case 'html':
			case 'htm':
			case 'js':
				return $img['html'];
			case 'php':
				return $img['php'];
			default:
				return $img['blank'];
		}
	}	
	// *** Get the filesize
	function getFileSize($url) {
		
		$url = $this->files_folder.'/'.$url;
		
		if (file_exists($url)) {
			$fs = filesize($url);
			return byte2kb($fs);
		}
		else {
			return byte2kb(0);
		}
	}
	
	// *** Get the size of a folder
	function getFolderSize($folder) {
		
		$res = eq("SELECT id,url FROM $this->db_table WHERE in_folder = '$folder';");
		while ($myo = mfo($res)) {
			
			//A file, add to total
			if ($myo->url) {
				$url = $this->files_folder.'/'.$myo->url;
				if (file_exists($url)) {
					$folder_size += filesize($url);
				}
			}
			//A folder, add filesize recursively
			else {
				$folder_size += $this->getFolderSize($myo->id);
			}
		}
		
		return byte2kb($folder_size);
	}
	
	// *** Get the total disk usage
	function getTotalDU() {
		return diskUsage($this->files_folder, 1, true);
	}
	
	// *** Get the location 'url' of a file
	function getFileLocation($id) {
		
		if ($id == TRASH_FOLDER) return 'Prullenbak';
		
		$parent = $id;
		
		while ($parent != 0) {
			
			$location = $location ? $this->getName($parent).' / '.$location : $this->getName($parent);
			$parent = $this->getParent($parent);
		}
		
		return '// '.$location;
	}
	
	// *** Get the folders inside a folder
	function getFolders($dir, $recursive = false, $deleted = false) {
	
		$folders = array();
		
		if ($deleted) $del_query = "AND deleted = 1";
		else		  $del_query = "AND deleted = 0";
		
		$res = eq("SELECT id,filename,timestamp FROM $this->db_table WHERE url = '' AND in_folder = '$dir' $del_query ORDER BY filename ASC;");
		while ($myo = mfo($res)) {
			$folders[$myo->id]	=	array($myo->filename, $myo->timestamp);
		}
		
		if ($recursive) {
			foreach (array_keys($folders) as $id) {
				$more_folders 	= $this->getFolders($id, true);
				$folders 		= array_merge($folders, $more_folders);
			}
		}
	
		return $folders;	
	}
	
	// *** Get the files inside a folder
	function getFiles($dir, $recursive = false, $deleted = false) {

		$files = array();
	
		if ($deleted) $del_query = "AND deleted = 1";
		else		  $del_query = "AND deleted = 0";
			
		$res = eq("SELECT id,filename,url,timestamp FROM $this->db_table WHERE url <> '' AND in_folder = '$dir' $del_query ORDER BY filename ASC;");
		while ($myo = mfo($res)) {
			$files[$myo->id]	=	array($myo->filename, $myo->url, $myo->timestamp);
		}
	
		if ($recursive) {
			
			$folders = $this->getFolders($dir,false);
			
			foreach (array_keys($folders) as $id) {
				$more_files = $this->getFiles($id, true);
				$files 		= array_merge($files, $more_files);
			}
		}
		
		return $files;		
	}
	
	// *** Display only the files
	function displayFiles($folder) {
		
		global $img;

		//Trashed files
		if ($folder == TRASH_FOLDER) {
			
			$files = $trashed_folders = array();
			
			$res = eq("SELECT id,filename,url,timestamp,in_folder FROM $this->db_table WHERE deleted = 1 ORDER BY filename ASC;");
			while ($myo = mfo($res)) {
				if (!$myo->url) {
					$trashed_folders[$myo->id]	=	true;
				}
				else {
					$files[$myo->id]	=	array($myo->filename, $myo->url, $myo->timestamp, $myo->in_folder);	
				}
				
			}
		}
		//Regular files
		else {
			$files = $this->getFiles($folder,false);
		}		
		
		foreach ($files as $id => $file) {
			
			list($filename, $url, $timestamp, $in_folder) = $file;
			
			//Don't show the file if it's parent folder is also trashed
			if ($folder == TRASH_FOLDER) {
				if (in_array($in_folder, array_keys($trashed_folders))) continue;	
			}
			
			$extension = getFileExtension($filename);
			$file_type = $this->getFileType($extension);
			$file_icon = $this->getFileIcon($extension);
			$file_size = $this->getFileSize($url);
			$file_date = date('d/m/Y H:i', $timestamp);
			$file_show = (strlen($filename) > 35) ? substr($filename,0,20).' ... '.substr($filename,-9) : $filename;
			$file_url  = $this->getFileLocation($id);
			$real_url  = $this->files_folder.'/'.$url;
			
			if (!file_exists($real_url)) {
				$img_link = "<a href='JavaScript: void(0);'
						onmouseover='setStatus(\"Het bestand <b>$filename</b> staat niet fysiek op de schijf!\")'>$file_icon</a>";
				$nonexisting = "style='color: grey;'";
			}
			else {
				$img_link = "<a href='inc/download.php?file=$real_url&filename=$filename'
						onmouseover='setStatus(\"Download het bestand <b>$filename</b>\")'>$file_icon</a>";
			}
			
			$html .= "
				\n<br/><span style='float:left; width:18px;'>
					$img_link	
				</span>
				<span style='float:left; width:200px;'>
					<a href='JavaScript: void(0);'	$nonexisting onclick='setSelected($id, \"$filename\", \"$file_url\", 1);'
													onmouseover='setStatus(\"Selecteer het bestand <b>$filename</b>\")'>$file_show</a>
				</span>
				<span style='float:left; width:120px;'>$file_type</span>
				<span style='float:left; width:110px;'>$file_date</span>
				<span style='float:left; width:50px; text-align: right;'>$file_size</span>
			";
		}
		
		return substr(trim($html),5);
	}
	
	// *** Display only the folders
	function displayFolders($folder) {
		
		global $img;
		
		//Trashed folders
		if ($folder == TRASH_FOLDER) {
			$folders = array();
			$res = eq("SELECT id,filename,in_folder, timestamp FROM $this->db_table WHERE url = '' AND deleted = 1 ORDER BY filename ASC;");
			while ($myo = mfo($res)) {
				$folders[$myo->id]	=	array($myo->filename, $myo->timestamp, $myo->in_folder);
			}
		}
		//Regular folders
		else {
			$folders = $this->getFolders($folder,false);	
		}
		
		
		foreach ($folders as $id => $file) {
			
			list($filename, $timestamp, $in_folder) = $file;

			//Don't show the folder if it's parent folder is also trashed
			if ($folder == TRASH_FOLDER) {
				if (in_array($in_folder, array_keys($folders))) continue;	
			}
			
			//Skip the trash can
			if ($id == 1) continue;
			
			//Determine open folder image (minus sign when has subfolders, blank when doesn't)
			if ($this->hasSubfolders($id)) {
				$has_sf		=	1;
			}
			else {
				$has_sf		=	0;
			}
			
			$file_size = $this->getFolderSize($id);
			$file_date = date('d/m/Y H:i', $timestamp);
			$file_url  = $this->getFileLocation($id);
			
			if ($folder == TRASH_FOLDER) {
				$img_link =	"$img[folder]";
			}
			else {
				$img_link =	"<a href='JavaScript: void(0);' 	onclick='showContents($id, \"$filename\", $has_sf);'
													onmouseover='setStatus(\"Toon inhoud van de map <b>$filename</b>\");'>$img[folder]</a>";
			}
			
			$html .= "
				\n<br/><span style='float:left; width:18px;'>
					$img_link
				</span>
				<span style='float:left; width:200px;'>
					<a href='JavaScript: void(0);' 	onclick='setSelected($id, \"$filename\", \"$file_url\", 0);'
													onmouseover='setStatus(\"Selecteer de map <b>$filename</b>\")'>$filename</a>
				</span>
				<span style='float:left; width:120px;'>Map</span>
				<span style='float:left; width:110px;'>$file_date</span>
				<span style='float:left; width:50px; text-align: right;'>$file_size</span>
			";
		}
		
		return substr(trim($html),5);
	}
	
	// *** Display the folder contents on the right hand side
	function displayContents($folder) {
		
		$folders 	= $this->displayFolders($folder);
		$files		= $this->displayFiles($folder);
		
		if ($folders and $files) {
			$folders.='<br/>';
		}
		
		if (!$folders and !$files) {
			if ($folder == ROOT_FOLDER) {
				$folders = '<i>Er zijn nog geen bestanden toegevoegd</i>';	
			}
			else {
				$folders = '<i>Deze map is leeg</i>';	
			}
		}
		
		return $folders.$files;
	}

	// *** Display the trashed contents	
	function displayTrashedContents() {
		
		$folders 	= $this->displayFolders(TRASH_FOLDER);
		$files		= $this->displayFiles(TRASH_FOLDER);
		
		if ($folders and $files) {
			$folders.='<br/>';
		}
		
		if (!$folders and !$files) {
			$folders = '<i>De prullenbak is leeg</i>';
		}
		
		return $folders.$files;	
	}
	
	// *** Display the tree contents
	function displayTree($folders, $level = 0, $expanded = array(), $folder_showing) {
		
		global $img;
		
		//Zero level is the main disk image
		if ($level == 0) {
			$has_sf	= $this->hasSubfolders(0) ? 1 : 0;
			$html 	= " <a href='JavaScript: void(0);' 	onclick='showContents(0, \"Mijn bestanden\", $has_sf)'
														onmouseover='setStatus(\"Toon mijn bestanden\")'>$img[disk]</a>
						Mijn bestanden";
		}
	
		//Determine the spacing, based on the level
		$spacing = $level * 5;
		for($x = 0; $x < $spacing; $x++) {
			$spacer .= '&nbsp;';
		}
		
		//Loop through each folder
		foreach ($folders as $id => $file) {
			
			list($filename, $timestamp) = $file;
			
			//Skip the trashcan for now
			if ($id == TRASH_FOLDER) continue;
			
			//Get the parent folder
			$parent_folder = $this->getParent($id);
			
			//Check if this is an expanded folder (parent must also be expanded!)
			if (in_array($id, $expanded) and (in_array($parent_folder, $expanded) or $parent_folder == 0)) {
				
				//Get sub folders for this folder
				$sub_folders = $this->getFolders($id,false);
				
				//Determine expander image (minus sign when has subfolders, blank when doesn't)
				if (count($sub_folders)) {
					$expander 	= 	"<a href='JavaScript: void(0);' onclick='contractFolder($id)'>$img[min]</a>";
					$has_sf		=	1;
				}
				else {
					$expander	=	"<img src='gfx/digicons/filesharing/fs_transparent.gif' width='9px' border='0px' />";
					$has_sf		=	0;
				}
				
				//Determine the file url
				$file_url  = $this->getFileLocation($id);
				
				//Make the HTML
				$html .= "
					<br/>$spacer$expander
				 	<a href='JavaScript: void(0);' 	onclick='showContents($id, \"$filename\", $has_sf)'
					 								onmouseover='setStatus(\"Toon inhoud van de map <b>$filename</b>\")'>$img[folder_open]</a>
					<a href='JavaScript: void(0);' 	onclick='setSelected($id, \"$filename\", \"$file_url\", 0);'
													onmouseover='setStatus(\"Selecteer de map <b>$filename</b>\")'>$filename</a>";
				
				//Add the subfolders HTML, increase the level
				$html .= $this->displayTree($sub_folders, $level + 1, $expanded, $folder_showing);
			}
			else {
				
				//Determine open folder image (minus sign when has subfolders, blank when doesn't)
				if ($this->hasSubfolders($id)) {
					$expander 	= 	"<a href='JavaScript: void(0);' onclick='expandFolder($id)'>$img[plus]</a>";
					$has_sf		=	1;
				}
				else {
					$expander	=	"<img src='gfx/digicons/filesharing/fs_transparent.gif' width='9px' border='0px' />";
					$has_sf		=	0;
				}

				//Determine the file url
				$file_url  = $this->getFileLocation($id);

				//Determine folder image (open when showing)
				if ($id == $folder_showing) {
					$folder_img	=	$img['folder_open'];
				}
				else {
					$folder_img	=	$img['folder'];
				}
				
				//Make the HTML
				$html .= "
					<br/>$spacer$expander
					<a href='JavaScript: void(0);' 	onclick='showContents($id, \"$filename\", $has_sf)'
													onmouseover='setStatus(\"Toon inhoud van de map <b>$filename</b>\")'>$folder_img</a>
					<a href='JavaScript: void(0);' 	onclick='setSelected($id, \"$filename\", \"$file_url\", 0);'
													onmouseover='setStatus(\"Selecteer de map <b>$filename</b>\")'>$filename</a>";
			}
			
		}
		
		//Trashcan goes last, and has special image
		if ($level == 0) {
			
			$html 	.= "<br/><a href='JavaScript: void(0);' onclick='showTrash()'
															onmouseover='setStatus(\"Toon de inhoud van de prullenbak\")'>$img[trash]</a>
						Prullenbak";
		}					
	
		//Return the HTML
		return $html;
	}

	// *** Display the command buttons	
	function displayCommands() {
		
		$commands = array(	
							"<button id='cmd_download'	onclick='downloadFile();'
														onmouseover='setStatus(\"Download <b>\"+selected_name+\"</b>\")'>
								Download
							 </button>",
							"<button id='cmd_rename'	onclick='renameFile();'
														onmouseover='setStatus(\"Wijzig de naam van <b>\"+selected_name+\"</b>\")'>
								Wijzig naam
							 </button>",
							"<button id='cmd_copy'		onclick='copyFile();'	
														onmouseover='setStatus(\"Kopieer <b>\"+selected_name+\"</b> naar <b>\"+folder_showing_name+\"</b>\")'>
								Kopieer
							 </button>",
							"<button id='cmd_cut'		onclick='moveFile();'
														onmouseover='setStatus(\"Verplaats <b>\"+selected_name+\"</b> naar <b>\"+folder_showing_name+\"</b>\")'>
								Verplaats
							 </button>",
							"<button id='cmd_delete'	onclick='trashFile();'
														onmouseover='setStatus(\"Verplaats <b>\"+selected_name+\"</b> naar de prullenbak\")'>
								Naar prullenbak
							 </button>",
							 "<button id='cmd_restore'	onclick='restoreFile();'
														onmouseover='setStatus(\"Zet <b>\"+selected_name+\"</b> terug\")'>
								Zet terug
							 </button>"
		);
		
		$html = implode(' ', $commands);
		
		//Return the HTML
		return $html;		
	}
	
	// *** Display the upload form and new folder button
	function displayFolderControls() {
		
		global $img;
		
		$html = "<form name='upload_form' method='post' action='$file' enctype='multipart/form-data' style='display: inline;'
				onsubmit='this.to_location.value = folder_showing; this.expand_folders.value = expanded_folders;'>
				<input type='hidden' name='to_location' value='0' />
				<input type='hidden' name='expand_folders' value='' />
			$img[arrowup] Nieuw bestand: <input type='file' name='uploaded_file' class='input_file' onmouseover='setStatus(\"Kies een bestand om te uploaden\")'/> <input type='submit' name='upload_file' value='Uploaden' onmouseover='setStatus(\"Upload bestand naar deze locatie\")'/>
		</form>
		<button id='cmd_new_folder'	onclick='newFolder();'
									onmouseover='setStatus(\"Nieuwe map aanmaken op deze locatie\")'>Nieuwe map</button>
		";
		
		return $html;
	}

	// *** Display the upload form and new folder button
	function displayTrashControls() {
		
		global $img;
		
		$html = "
		<button id='cmd_empty'	onclick='emptyTrash();'
								onmouseover='setStatus(\"Maak de prullenbak leeg\")'>Prullenbak legen</button>
		<button id='cmd_empty'	onclick='restoreAllTrash();'
								onmouseover='setStatus(\"Alle bestanden uit de prullenbak terug zetten\")'>Alles terugzetten</button>		
		";
		
		return $html;
	}
}
?>