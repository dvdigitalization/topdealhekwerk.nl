<?php
/*
Title:		Order class
File: 		classes/class_Order.php
Version: 	v2.20
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2009 by Digitalization
*/

// *** Definitions
define('STATUS_SUCCESS',	1);
define('STATUS_FAILED',		2);

define('PM_VOID',			0);
define('PM_TRANSFER',		1);
define('PM_INCASSO',		2);
define('PM_REMBOURS',		3);
define('PM_IKASSA',			4);
define('PM_IDEAL',			5);
define('PM_PAYPAL',			6);
define('PM_CASH',			7);
define('PM_CC',				8);
define('PM_IKASSA_CC',		9);
define('PM_VOORAF',		10);
define('DELIVERY_ARTICLE',	-1);
define('VAT_RETURN_ARTICLE',-2);

// *** Redefined sex arrays
$SEX_ARRAY['EN']	=	array (
							0 	=> '',
							1 	=> 'Mr. ',
							2	=> 'Ms. '
						);
$SEX_ARRAY['NL']	=	array (
							0 	=> '',
							1 	=> 'Dhr. ',
							2	=> 'Mevr. '
						);
$SEX_ARRAY['FR']	=	array (
							0	=>	'',
							1 	=> 'Mr. ',
							2	=> 'Mme. '
						);

// *** Transaction arrays
$OK_STATUSSES[PM_TRANSFER]		= array(1);
$PAYMENT_STATUS[PM_TRANSFER]	= array(
									0	=>	'Nog niet overgeschreven',
									1	=>	'Bedrag overgeschreven'
);

$OK_STATUSSES[PM_INCASSO]		= array(1);
$PAYMENT_STATUS[PM_INCASSO]		= array(
									0	=>	'Nog niet geincasseerd',
									1	=>	'Geincasseerd'
);

$OK_STATUSSES[PM_REMBOURS]		= array(1);
$PAYMENT_STATUS[PM_REMBOURS]	= array(
									0	=>	'Nog niet betaald',
									1	=>	'Betaald per rembours'
);

$OK_STATUSSES[PM_VOORAF]		= array(1);
$PAYMENT_STATUS[PM_VOORAF]	= array(
									0	=>	'Nog niet betaald',
									1	=>	'Vooraf betaald'
);

$OK_STATUSSES[PM_CASH]			= array(1);
$PAYMENT_STATUS[PM_CASH]		= array(
									0	=>	'Nog niet betaald',
									1	=>	'Contant of met PIN afgerekend'
);

$OK_STATUSSES[PM_CC]			= array(1);
$PAYMENT_STATUS[PM_CC]			= array(
									0	=>	'Nog niet betaald',
									1	=>	'Betaald per creditcard'
);

$OK_STATUSSES[PM_PAYPAL]		= array(1, 3);
$PAYMENT_STATUS[PM_PAYPAL]		= array(
									0	=>	'Niet betaald',
									1	=>	'Betaald',
									3	=> 	'Handmatig op betaald gezet'
);

$OK_STATUSSES[PM_IDEAL]			= array(1, 3);
$PAYMENT_STATUS[PM_IDEAL]		= array(
									0	=>	'Niet betaald',
									1	=>	'Betaald',
									3	=> 	'Handmatig op betaald gezet'
);

$OK_STATUSSES[PM_IKASSA]		= array(3, 5, 51, 9 ,91);
$PAYMENT_STATUS[PM_IKASSA]		= array(
									0  	=> 'Invalid or incomplete',
									1 	=> 'Cancelled by client',
									2 	=> 'Authorization refused',
									3	=> 'Handmatig op betaald gezet',
									4 	=> 'Order stored',
									41 	=> 'Waiting client payment',
									5 	=> 'Authorized',
									51 	=> 'Authorization waiting',
									52 	=> 'Authorization not known',
									55 	=> 'Stand-by',
									59 	=> 'Author. to get manually',
									6 	=> 'Authorized and canceled',
									61 	=> 'Author. deletion waiting',
									62 	=> 'Author. deletion uncertain',
									63 	=> 'Author. deletion refused',
									64 	=> 'Authorized and canceled',
									7 	=> 'Payment deleted',
									71 	=> 'Payment deletion pending',
									72 	=> 'Payment deletion uncertain',
									73 	=> 'Payment deletion refused',
									74 	=> 'Payment deleted',
									75 	=> 'Deletion handled by merchant',
									8 	=> 'Refund',
									81 	=> 'Refund pending',
									82 	=> 'Refund uncertain',
									83 	=> 'Refund refused',
									84 	=> 'Refund',
									85 	=> 'Refund handled by merchant',
									9 	=> 'Payment requested',
									91 	=> 'Payment processing',
									92 	=> 'Payment uncertain',
									93 	=> 'Payment refused',
									94 	=> 'Payment requested',
									95 	=> 'Payment handled by merchant',
									99 	=> 'Being processed'
);
$OK_STATUSSES[PM_IKASSA_CC]		= $OK_STATUSSES[PM_IKASSA];
$PAYMENT_STATUS[PM_IKASSA_CC]		= $PAYMENT_STATUS[PM_IKASSA];

// *** The order class
class Order {

	private	$OrderArticles 	= array();
	private	$OrderStatusses = array();

	private $order_id, $total_value, $payment_method, $transaction_id, $transaction_status, $order_status_id,
			$finalized, $closed, $credited, $customer_id, $affiliate_id, $timestamp, $invoice_date, $invoice_nr, $invoice_sent,
			$delivery_id, $remarks, $lang, $discount_code, $discount_percentage, $discount_amount, $discount;

	private $ikassa_payment_id, $ikassa_payment_method, $ikassa_acceptance, $ikassa_cc_brand, $ikassa_cc_no, $ikassa_nc_error,
	 		$ideal_transaction_id;

	//Initiate Order class
	function Order($existing_order_id = null, $unique_string = null) {

		//Set arrays
		$this->OrderArticles 		= 	array();									//Holds the order articles
		$this->OrderStatusses 		= 	array();									//Hold the order statusses

		//Database tables
		$this->db_table				=	DIGI_DB_PREFIX.'orders';					//The database table with the orders
		$this->db_table_articles 	=	DIGI_DB_PREFIX.'order_articles';			//Order articles table
		$this->db_table_statusses 	=	DIGI_DB_PREFIX.'order_statusses';			//Order statusses
		$this->db_table_status_defs	=	DIGI_DB_PREFIX.'order_status_definitions_';	//Order status definitions
		$this->db_table_customers	=	DIGI_DB_PREFIX.'customers_shop';			//The database table with customers
		$this->db_table_affiliates	=	DIGI_DB_PREFIX.'affiliates';				//The database table with affiliates
		$this->db_table_delivery	=	DIGI_DB_PREFIX.'delivery_addresses';		//The database table with delivery addresses
		$this->db_table_transaction	=	'';											//The script will set this

		//Transaction tables
		$this->db_table_incasso		=	DIGI_DB_PREFIX.'incasso_transactions';		//The database table with incasso transactions
		$this->db_table_ideal		=	DIGI_DB_PREFIX.'ideal_transactions';		//The database table with ideal transactions
		$this->db_table_transfer	=	DIGI_DB_PREFIX.'transfer_transactions';		//The database table with overboekings transactions
		$this->db_table_rembours 	=	DIGI_DB_PREFIX.'rembours_transactions'; 	//The database table with rembours transactions
		$this->db_table_vooraf 	=	DIGI_DB_PREFIX.'vooraf_transactions'; 	//The database table with vooraf transactions
		$this->db_table_ikassa 		=	DIGI_DB_PREFIX.'ikassa_transactions'; 		//The database table with ikassa transactions
		$this->db_table_paypal 		= 	DIGI_DB_PREFIX.'paypal_transactions'; 		//The database table with paypal transactions
		$this->db_table_cash		=	DIGI_DB_PREFIX.'cash_transactions';			//The database table with contacte transactions
		$this->db_table_cc			=	DIGI_DB_PREFIX.'cc_transactions';			//The database table with contacte transactions

		//Existing order by id
		if ($existing_order_id) {
			$this->order_id 		= 	$existing_order_id;
			$this->unique_string	=	0;
			$this->readFromDB();
		}
		//Existing order by unique string
		else if ($unique_string) {
			$this->order_id 		= 	0;
			$this->unique_string	=	validateSimpleString($unique_string);
			$this->readFromDB();
		}
		//New order
		else {
			$this->order_id				=	0;
			$this->unique_string		=	'';
			$this->timestamp			= 	time();
			$this->order_status			=	0;
			$this->deleted				=	0;
			$this->finalized			=	0;
			$this->discount_code		=	'';
			$this->discount_percentage	=	0;
			$this->discount_amount		=	0;
			$this->discount				=	0;
		}
	}

	//Read all order data from DB
	function readFromDB() {

		if ($this->order_id) {
			$where = "id = '$this->order_id'";
		}
		elseif ($this->unique_string) {
			$where = "unique_string = '$this->unique_string'";
		}
		else {
			return null;
		}

		//Get data from database
		$res = eq("SELECT * FROM $this->db_table WHERE $where;");
		$myo = mfo($res);

		//No data?
		if (!$myo) return null;

		//Set id and unique string
		$this->unique_string			=	$myo->us;
		$this->order_id					=	$myo->id;

		//Set payment method
		$this->setPaymentMethod($myo->payment_method);

		//Initialize transaction status and id
		$this->transaction_id		=	0;
		$this->transaction_status 	= 	0;

		//Set transaction status and id if all ok
		if ($this->db_table_transaction) {

			$res_status = eq("SELECT * FROM $this->db_table_transaction WHERE order_id = '$this->order_id';");
			$myo_status = mfo($res_status);

			if ($myo_status) {

				//Set core vars
				$this->transaction_id		=	$myo_status->id;
				$this->transaction_status 	= 	$myo_status->status;

				//Set some extra vars, depending on payment method
				switch ($this->payment_method) {

					case PM_INCASSO:
						$this->setIncassoVars(	$myo_status->account_nr, $myo_status->account_name);
						break;
					case PM_IDEAL:
						$this->setIdealVars(	$myo_status->transaction_id);
						break;
					case PM_IKASSA:
					case PM_IKASSA_CC:
						$this->setIkassaVars(	$myo_status->payment_id, $myo_status->payment_method, $myo_status->acceptance,
												$myo_status->cc_brand, $myo_status->cc_no, $myo_status->nc_error);
						break;
				}
			}
		}

		//Set invoice data
		$this->invoice_date			=	$myo->invoice_date;
		$this->invoice_nr			=	$myo->invoice_nr;
		$this->invoice_sent			=	$myo->invoice_sent;

		//Set discount data
		$this->discount_code		=	$myo->discount_code;
		$this->discount_percentage	=	$myo->discount_percentage;
		$this->discount_amount		=	$myo->discount_amount;
		$this->discount				=	$myo->discount;

		//Set all other vars
		$this->timestamp			=	unix_time($myo->timestamp);
		$this->customer_id			=	$myo->customer_id;
		$this->affiliate_id			=	$myo->affiliate_id;
		$this->total_value			=	$myo->total_value;
		$this->remarks				=	$myo->remarks;
		$this->lang					=	$myo->lang;
		$this->deleted				=	$myo->deleted;
		$this->finalized			=	$myo->finalized;
		$this->closed				=	$myo->closed;
		$this->credited 			= 	$myo->credited;

		//Delivery id
		$this->delivery_id 			=	getMyo($this->db_table_delivery, "order_id = '$this->order_id'", 'id');

		//Customer data
		$this->customer				=	getMyo($this->db_table_customers, $this->customer_id,
										array(	'first_name','last_name','title','email','phone','address','nr',
												'postal_code','city','country'));

		//Read order articles
		$this->readOrderArticlesFromDB();

		//Read order statusses
		$this->readOrderStatussesFromDB();
	}

	//Save an order to the database
	function saveOrderToDB() {

		if ($this->order_id) {
			upRec($this->db_table, $this->order_id,
					array(	'total_value', 'remarks', 'lang', 'payment_method', 'total_value',
							'discount_code','discount_amount','discount_percentage','discount'),
					array(	$this->total_value, $this->remarks, $this->lang, $this->payment_method, $this->total_value,
							$this->discount_code, $this->discount_amount, $this->discount_percentage, $this->discount)
			);
		}
		else {
			$this->order_id = insRec($this->db_table,
					array(	'us','customer_id', 'affiliate_id', 'total_value', 'remarks', 'payment_method', 'lang',
							'discount_code','discount_amount','discount_percentage','discount'),
					array(	$this->unique_string, $this->customer_id, $this->affiliate_id, $this->total_value, $this->remarks,
							$this->payment_method, $this->lang, $this->discount_code, $this->discount_amount, $this->discount_percentage,
							$this->discount)
			);

			//Update the delivery address entry with the right order id
			if ($this->order_id and $this->delivery_id) {
				upRec($this->db_table_delivery, $this->delivery_id,	'order_id', $this->order_id);
			}
		}

		//Save the order articles to database
		$this->saveOrderArticlesToDB();

		//Save the transaction state to database. Critical!
		$this->saveTransactionStateToDB();

		//If we dont manage to get a transaction ID, something is wrong and we cancel the order
		if (!$this->transaction_id) {
			$this->deleteOrder(true);
		}
	}

	//Save the transaction (insert or update)
	function saveTransactionStateToDB() {

		switch ($this->payment_method) {

			case PM_INCASSO:
				if ($this->transaction_id) {
					upRec($this->db_table_transaction, $this->transaction_id,
								array('status','account_nr','account_name'),
								array($this->transaction_status, $this->incasso_account_nr, $this->incasso_account_name)
					);
				}
				else {
					$this->transaction_id = insRec($this->db_table_transaction,
								array('order_id','status','account_nr','account_name'),
								array($this->order_id, $this->transaction_status,
									  $this->incasso_account_nr, $this->incasso_account_name)
					);
				}
				break;

			case PM_IDEAL:
				if ($this->transaction_id) {
					upRec($this->db_table_transaction, $this->transaction_id,
								array('status','transaction_id'),
								array($this->transaction_status, $this->ideal_transaction_id)
					);
				}
				else {
					$this->transaction_id = insRec($this->db_table_transaction,
								array('order_id','status','transaction_id'),
								array($this->order_id, $this->transaction_status,
									  $this->ideal_transaction_id)
					);
				}
				break;

			case PM_IKASSA:
			case PM_IKASSA_CC:
				if ($this->transaction_id) {
					upRec($this->db_table_transaction, $this->transaction_id,
								array(	'status','payment_id','payment_method','acceptance','cc_brand','cc_no','nc_error'),
								array(	$this->transaction_status, $this->ikassa_payment_id, $this->ikassa_payment_method,
										$this->ikassa_acceptance, $this->ikassa_cc_brand, $this->ikassa_cc_no, $this->ikassa_nc_error)
					);
				}
				else {
					$this->transaction_id = insRec($this->db_table_transaction,
								array(	'order_id','status','payment_id','payment_method','acceptance','cc_brand','cc_no','nc_error'),
								array(	$this->order_id,$this->transaction_status,$this->ikassa_payment_id,$this->ikassa_payment_method,
										$this->ikassa_acceptance,$this->ikassa_cc_brand,$this->ikassa_cc_no,$this->ikassa_nc_error)
					);
				}
				break;

			case PM_TRANSFER:
			case PM_REMBOURS:
			case PM_VOORAF:
			case PM_CASH:
			case PM_PAYPAL:
				if ($this->transaction_id) {
					upRec($this->db_table_transaction, $this->transaction_id,
								array('status'),
								array($this->transaction_status)
					);
				}
				else {
					$this->transaction_id = insRec($this->db_table_transaction,
								array('order_id','status'),
								array($this->order_id, $this->transaction_status)
					);
				}
				break;
		}
	}

	//Read all order statusses from database
	function readOrderStatussesFromDB() {

		//Set clean array
		$this->OrderStatusses = array();

		//Read articles from DB
		$res = eq("	SELECT id, UNIX_TIMESTAMP(timestamp) AS timestamp, status_id, remarks
					FROM $this->db_table_statusses WHERE order_id = '$this->order_id' ORDER BY timestamp ASC;");

		//Status definitions table
		$st_def_table = $this->db_table_status_defs.$this->lang;

		//Add to array
		while ($myo = mfo($res)) {
			$status = getMyo($st_def_table, $myo->status_id, array('name','description'));
			$this->OrderStatusses[$myo->id] = array($myo->timestamp, $status->name, $status->description, $myo->remarks, $myo->status_id);
		}
	}

	//Read all order articles from database
	function readOrderArticlesFromDB() {

		//Set clean array
		$this->OrderArticles = array();

		//Read articles from DB
		$res = eq("SELECT id FROM $this->db_table_articles WHERE order_id = '$this->order_id';");

		//Add to array
		while ($myo = mfo($res)) {
			$this->OrderArticles[$myo->id] = new OrderArticle($myo->id);
		}
	}

	//Save order articles to database
	function saveOrderArticlesToDB() {

		//Delete the old ones first
		delRec($this->db_table_articles, "order_id = '$this->order_id'");

		//Insert the new ones
		foreach ($this->OrderArticles as $OrderArticle) {
		 	insRec($this->db_table_articles,
			 		array(	'order_id','product_id','product_text','amount','price_at_ordering','vat_at_ordering'),
		 			array(	$this->order_id, $OrderArticle->getProductID(), $OrderArticle->getProductText(),
					 		$OrderArticle->getAmount(), $OrderArticle->getProductPrice(), $OrderArticle->getProductVatPercentage())
		 	);
		}
	}

	//Clear the current order articles
	function clearOrderArticles() {
		$this->OrderArticles = array();
	}

	//Delete the order articles
	function deleteOrderArticles() {
		$this->OrderArticles = array();
		delRec($this->db_table_articles, "order_id = '$this->order_id'");
	}

	//Delete this order
	function deleteOrder($permanently = false, $delete_delivery = true) {

		if ($permanently and $this->order_id) {
			$this->deleteTransaction();
			$this->deleteDelivery($delete_delivery);
			$this->deleteStatusses();
			$this->deleteOrderArticles();
			delRec($this->db_table, $this->order_id);
		}
		elseif ($this->order_id) {
			upRec($this->db_table, $this->order_id, 'deleted', 1);
		}
		$this->deleted 	= 1;
	}

	//Restore an order
	function restoreOrder() {
		upRec($this->db_table, $this->order_id, 'deleted', 0);
		$this->deleted 	= 0;
	}

	//Delete transactions
	function deleteTransaction() {
		if ($this->transaction_id and $this->db_table_transaction) {
			delRec($this->db_table_transaction, $this->transaction_id);
		}
	}

	//Delete delivery address
	function deleteDelivery($permanently = true) {

		//Set the proper where query
		$where = $this->delivery_id ? $this->delivery_id : "order_id='$this->order_id'";

		//Permanently or not?
		if ($permanently) {
			delRec($this->db_table_delivery, $where);
		}
		else {
			upRec($this->db_table_delivery, $this->delivery_id, 'order_id', 0);
		}
	}

	//Delete order statusses
	function deleteStatusses() {
		delRec($this->db_table_statusses, "order_id='$this->order_id'");
	}

	//Make an invoice
	function makeInvoice($override = false) {

		//Refuse to make an invoice if invoice already exists, unless you override it.
		if (!$this->invoice_nr or $override) {

			$Invoice = new Invoice($this->order_id, $this->customer_id);

			$this->invoice_nr 	= 	$Invoice->getInvoiceNr();
			$this->invoice_date	= 	time();

			upRec($this->db_table, $this->order_id,
					array('invoice_nr', 'invoice_date'),
					array($this->invoice_nr, mysql_time($this->invoice_date)));
		}
	}

	// *** SET functions

	//Set the payment method and relevant transactions table
	function setPaymentMethod($method) {

		//Set the method
		$this->payment_method = $method;

		//Set the proper table for the payment method
		switch($this->payment_method) {

			case PM_TRANSFER:
				$this->db_table_transaction = $this->db_table_transfer;
				break;
			case PM_INCASSO:
				$this->db_table_transaction = $this->db_table_incasso;
				break;
			case PM_IKASSA:
			case PM_IKASSA_CC:
				$this->db_table_transaction = $this->db_table_ikassa;
				break;
			case PM_IDEAL:
				$this->db_table_transaction = $this->db_table_ideal;
				break;
			case PM_REMBOURS:
				$this->db_table_transaction = $this->db_table_rembours;
				break;
			case PM_VOORAF:
				$this->db_table_transaction = $this->db_table_vooraf;
				break;
			case PM_PAYPAL:
				$this->db_table_transaction = $this->db_table_paypal;
				break;
			case PM_CASH:
				$this->db_table_transaction = $this->db_table_cash;
				break;
			case PM_CC:
				$this->db_table_transaction = $this->db_table_cc;
				break;
		}
	}

	//Set the incasso transaction variables
	function setIncassoVars($account_name, $account_nr) {
		$this->incasso_account_nr		=	$account_nr;
		$this->incasso_account_name		=	$account_name;
	}

	//Set the ideal transaction variables
	function setIdealVars($transaction_id) {
		$this->ideal_transaction_id		=	$transaction_id;
	}

	//Set the ikassa transaction variables
	function setIkassaVars($payment_id, $payment_method, $acceptance, $cc_brand, $cc_no, $nc_error) {
		$this->ikassa_payment_id		=	$payment_id;
		$this->ikassa_payment_method	=	$payment_method;
		$this->ikassa_acceptance		=	$acceptance;
		$this->ikassa_cc_brand			=	$cc_brand;
		$this->ikassa_cc_no				=	$cc_no;
		$this->ikassa_nc_error			=	$nc_error;
	}

	//Set the order to paid, if allowed by method
	function setPaid($override = false) {

		switch ($this->payment_method) {
			case PM_TRANSFER:
			case PM_INCASSO:
			case PM_REMBOURS:
			case PM_VOORAF:
			case PM_CASH:
			case PM_CC:

				$this->setTransactionStatus(1);
				$this->saveTransactionStateToDB();
				return true;
				break;

			case PM_IKASSA:
			case PM_IKASSA_CC:
			case PM_IDEAL:
			case PM_PAYPAL:

				if ($override) {
					$this->setTransactionStatus(3);
					$this->saveTransactionStateToDB();
					return true;
				}
				return false;
				break;
		}
	}

	//Set the transaction status
	function setTransactionStatus($status) {
		$this->transaction_status = $status;
	}

	//Set the unique string
	function setUniqueString($unique_string) {
		$this->unique_string = $unique_string;
	}

	//Set the customer
	function setCustomerId($customer_id) {
		$this->customer_id = $customer_id;
	}

	//Set the affiliate
	function setAffiliateId($affiliate_id) {
		$this->affiliate_id = $affiliate_id;
	}

	//Set the id of an existing delivery address
	function setDeliveryId($delivery_id) {
		$this->delivery_id = $delivery_id;
	}

	//Set the total value of this order
	function setTotalValue($total_value) {
		$this->total_value = $total_value;
	}

	//Set the total value of this order
	function setRemarks($remarks) {
		$this->remarks = $remarks;
	}

	//Set the language
	function setLang($lang) {
		$this->lang = $lang;
	}

	//Set the discount
	function setDiscount($discount_code, $discount_percentage, $discount_amount, $discount) {
		$this->discount_code		= 	$discount_code;
		$this->discount_percentage	=	$discount_percentage;
		$this->discount_amount		= 	$discount_amount;
		$this->discount				=	$discount;
	}

	//Re-calculate the discount
	function recalculateDiscount() {

		//Set to zero
		$this->discount = 0;

		//Discount percentage
		if ($this->discount_percentage) {
			foreach ($this->OrderArticles as $OrderArticle) {
				if (!$OrderArticle->isVatReturn() and !$OrderArticle->isDelivery()) {
					$this->discount += $this->discount_percentage/100 * $OrderArticle->getArticlePrice();
				}
			}
			$this->discount = round($this->discount);
		}

		//Fixed amount
		elseif ($this->discount_amount) {
			$sub_total = 0;
			foreach ($this->OrderArticles as $OrderArticle) {
				if (!$OrderArticle->isVatReturn() and !$OrderArticle->isDelivery()) {
					$sub_total += $OrderArticle->getArticlePrice();
				}
			}
			$this->discount = $this->discount_amount > $sub_total ? $sub_total : $this->discount_amount;
		}
	}

	//Recalculate the total value of this order (also recalculates the discount)
	function recalculateTotalValue() {

		global $cfg;

		//Set to zero
		$this->total_value = $this->discount = $sub_total = $vat = 0;

		//Loop the articles
		foreach ($this->OrderArticles as $article_id => $OrderArticle) {

			//Get article price
			$price = $OrderArticle->getArticlePrice();

			//Subtotal count
			if (!$OrderArticle->isVatReturn() and !$OrderArticle->isDelivery()) {
				$sub_total 			+= $price;
				$this->total_value 	+= $price;
				$vat 				+= $OrderArticle->getArticleVatAmount();
			}

			//Set the VAT article aside
			elseif ($OrderArticle->isVatReturn()) {
				$VatArticle = $article_id;
			}

			//Delivery article
			elseif ($OrderArticle->isDelivery()) {
				$this->total_value 	+= $price;
				$vat 				+= $OrderArticle->getArticleVatAmount();
			}
		}

		//Discount amount or percentage
		if ($this->discount_percentage) {
			$this->discount = round($this->discount_percentage/100 * $sub_total);
			$vat -= ($this->discount/($cfg['PRODUCTS_STANDARD_VAT']+100))*$cfg['PRODUCTS_STANDARD_VAT'];
		}
		elseif ($this->discount_amount) {
			$this->discount = $this->discount_amount > $sub_total ? $sub_total : $this->discount_amount;
			$vat -= ($this->discount/($cfg['PRODUCTS_STANDARD_VAT']+100))*$cfg['PRODUCTS_STANDARD_VAT'];
		}

		//Subtract the discount from the total value
		$this->total_value -= $this->discount;

		//Vat article update
		if (isset($VatArticle) and isset($this->OrderArticles[$VatArticle])) {
			$VatArticle = $this->OrderArticles[$VatArticle];
			$VatArticle->changePrice($vat);
			$this->total_value -= $vat;
		}

		//Save
		$this->saveOrderToDB();
	}

	//Add an article
	function addOrderArticle($OrderArticle) {
		$this->OrderArticles[] 	= 	$OrderArticle;
		$this->recalculateTotalValue();
	}

	//Remove an article
	function removeOrderArticle($article_id) {
		if (!isset($this->OrderArticles[$article_id])) {
			return null;
		}
		delRec($this->db_table_articles, $article_id);
		unset($this->OrderArticles[$article_id]);
		$this->recalculateTotalValue();
	}

	//Finalize the order, blocking stock updates and confirmation mail sending after a refresh
	function finalizeOrder() {
		$this->finalized	=	1;
		upRec($this->db_table, $this->order_id, 'finalized', 1);
		return true;
	}

	//Close the order
	function closeOrder() {
		$this->closed	=	1;
		upRec($this->db_table, $this->order_id, 'closed', 1);
	}

	//Re-open the order
	function reOpenOrder() {
		$this->closed	=	0;
		upRec($this->db_table, $this->order_id, 'closed', 0);
	}

	// *** GET functions

	function getOrderId() {
		return $this->order_id;
	}

	function getNiceOrderId($id_format) {
		return sprintf(str_replace('[YEAR]',date('Y',$this->timestamp),$id_format), $this->order_id);
	}

	function getAffiliateId() {
		return $this->affiliate_id;
	}

	function getAffiliateName() {
		if ($this->affiliate_id) {
			return getMyo($this->db_table_affiliates,$this->affiliate_id,'name');
		}
		return '';
	}

	function getCustomerId() {
		return $this->customer_id;
	}

	function getCustomerName($with_sex = false) {
		global $SEX_ARRAY;
		if ($with_sex)	return $SEX_ARRAY[$this->lang][$this->customer->title].$this->customer->first_name.' '.$this->customer->last_name;
		else			return $this->customer->first_name.' '.$this->customer->last_name;
	}

	function getCustomerEmail() {
		return $this->customer->email;
	}

	function getCustomerPhone() {
		return $this->customer->phone;
	}

	function getCustomerLabel($br = '<br/>') {

		global $COUNTRY_ARRAY, $SEX_ARRAY;

		//Postal code cleansing
		$pc = strtoupper($this->customer->postal_code);

		//Special formatting for dutch postal codes
		if ($this->customer->country == 124) {
			$pc = trim(str_replace(' ', '', $pc));
			$pc = substr($pc,0,4).' '.substr($pc,4);
		}

		//Return label
		return $SEX_ARRAY[$this->lang][$this->customer->title].$this->customer->first_name.' '.$this->customer->last_name.$br.$this->customer->address.' '.$this->customer->nr.$br.$pc.' &nbsp;'.$this->customer->city.$br.$COUNTRY_ARRAY[$this->customer->country];
	}

	function getDeliveryLabel($br = '<br/>') {

		global $COUNTRY_ARRAY, $SEX_ARRAY;

		//Get delivery details
		$myo = getMyo(	$this->db_table_delivery, $this->delivery_id,
						array('first_name','last_name','title','address','nr','postal_code','city','country'));

		//Postal code cleansing
		$pc = strtoupper($myo->postal_code);

		//Special formatting for dutch postal codes
		if ($myo->country == 124) {
			$pc = trim(str_replace(' ', '', $pc));
			$pc = substr($pc,0,4).' '.substr($pc,4);
		}

		//Return label
		return $SEX_ARRAY[$this->lang][$myo->title].$myo->first_name.' '.$myo->last_name.$br.$myo->address.' '.$myo->nr.$br.$pc.' &nbsp;'.$myo->city.$br.$COUNTRY_ARRAY[$myo->country];
	}

	function getDeliveryCountry() {
		return getMyo($this->db_table_delivery, $this->delivery_id, 'country');
	}

	function getDeliveryMessage() {
		return getMyo($this->db_table_delivery, $this->delivery_id, 'message');
	}

	function getRemarks() {
		return $this->remarks ? nl2br($this->remarks) : 'geen';
	}

	function getTimestamp($parsed = true) {
		return $parsed ? date('d/m/Y',$this->timestamp) : $this->timestamp;
	}

	function getPaymentMethod() {
		return $this->payment_method;
	}

	function getTransactionId() {
		return $this->transaction_id;
	}

	function getTransactionStatus($full = false) {
		global $PAYMENT_STATUS;
		if ($full)	return	$PAYMENT_STATUS[$this->payment_method][$this->transaction_status];
		else		return $this->transaction_status;
	}

	function getOrderStatus() {
		if ($key = count($this->OrderStatusses)) {
			return $this->OrderStatusses[$key-1];
		}
	}

	function getOrderStatusses() {
		return $this->OrderStatusses;
	}

	function getInvoiceNo($format) {
		return sprintf(str_replace('[YEAR]',date('Y',unix_time($this->invoice_date)), $format), $this->invoice_nr);
	}

	function getInvoiceDate($parsed = false) {
		if ($parsed)	return time2date($this->invoice_date);
		else			return $this->invoice_date;
	}

	function getInvoiceYear() {
		return date('Y',$this->invoice_date);
	}

	function getTotalValue($parsed = false, $sign = 0) {
		if ($parsed)	return parseAmount($this->total_value, $sign);
		else			return $this->total_value;
	}

	function getLang() {
		return $this->lang;
	}

	function getOrderArticles() {
		return $this->OrderArticles;
	}

	function getOrderArticle($article_id) {
		if (isset($this->OrderArticles[$article_id])) {
			return $this->OrderArticles[$article_id];
		}
		return null;
	}

	function getPaymentMethodString() {

		switch ($this->payment_method) {

			case PM_INCASSO:
				return 'incasso';
			case PM_TRANSFER:
				return 'overboeking';
			case PM_IDEAL:
				return 'iDeal';
			case PM_VOID:
				return 'afgebroken';
			case PM_IKASSA:
			case PM_IKASSA_CC:
				return 'i-kassa';
			case PM_REMBOURS:
				return 'rembours';
			case PM_VOORAF:
				return 'vooraf';
			case PM_PAYPAL:
				return 'paypal';
			case PM_CASH:
				return 'contant';
			case PM_CC:
				return 'creditcard';
		}
	}

	function getDeliveryString() {
		return 'verzending';
	}

	function getVatReturnString() {
		return 'btw verrekening';
	}

	function getDiscountString() {
		if ($this->discount_percentage) {
			return 'Korting '.$this->discount_code.' '.$this->discount_percentage.'%';
		}
		elseif ($this->discount_amount) {
			return 'Korting '.$this->discount_code;
		}
		else {
			return 'Korting';
		}
	}

	function getIkassaVars() {
		return array(	$this->ikassa_payment_id, $this->ikassa_payment_method, $this->ikassa_acceptance,
						$this->ikassa_cc_brand,	$this->ikassa_cc_no, $this->ikassa_nc_error);
	}

	function getIncassoVars() {
		return array($this->incasso_account_nr, $this->incasso_account_name);
	}

	function getIdealVars() {
		return array($this->ideal_transaction_id);
	}

	function getDiscountCode() {
		return $this->discount_code;
	}

	function getDiscountPercentage() {
		return $this->discount_percentage;
	}

	function getDiscountAmount() {
		return $this->discount_amount;
	}

	function getDiscount() {
		return $this->discount;
	}

	function getVat() {

		global $cfg;

		//Initialize
		$vat = 0;

		//Loop the articles
		foreach ($this->OrderArticles as $article_id => $OrderArticle) {

			//Count the vat
			if (!$OrderArticle->isVatReturn()) {
				$vat 				+= $OrderArticle->getArticleVatAmount();
			}

			//Vat return article? So there is no vat...
			else {
				return 0;
			}
		}

		//Discount amount or percentage
		if ($this->discount) {
			$vat -= ($this->discount/($cfg['PRODUCTS_STANDARD_VAT']+100))*$cfg['PRODUCTS_STANDARD_VAT'];
		}

		return $vat;
	}

	function invoiceExists() {
		if ($this->invoice_nr) {
			return true;
		}
		return false;
	}

	function invoiceSent() {
		return $this->invoice_sent;
	}

	function isFinalized() {
		return $this->finalized;
	}

	function isClosed() {
		return $this->closed;
	}

	function isCredited() {
		return $this->credited;
	}

	function isDeleted() {
		return $this->deleted;
	}
}


// *** CLASS FOR THE ORDER ARTICLES
class OrderArticle {

	private $id, $product_id, $product_text, $amount, $price_at_ordering, $vat_at_ordering;

	function OrderArticle($article_id = null) {

		if (!defined('PRODUCTS_TABLE')) {
			define('PRODUCTS_TABLE',	'products');
		}

		$this->db_table_articles 			=	DIGI_DB_PREFIX.'order_articles';
		$this->db_table_products			=	DIGI_DB_PREFIX.PRODUCTS_TABLE;

		if ($article_id) {
			$this->id = $article_id;
			$this->readFromDB();
		}
	}

	function readFromDB() {

		$res = eq("SELECT * FROM $this->db_table_articles WHERE id='$this->id';");
		$myo = mfo($res);

		$this->amount 				= 	$myo->amount;
		$this->price_at_ordering 	= 	$myo->price_at_ordering;
		$this->vat_at_ordering 		= 	$myo->vat_at_ordering;
		$this->product_id 			= 	$myo->product_id;
		$this->product_text			=	$myo->product_text;
	}

	function readFromItem($Item) {
		$this->amount				=	$Item->getAmount();
		$this->price_at_ordering	=	$Item->getProductPrice(true);
		$this->vat_at_ordering		=	$Item->getProductVatPercentage();
		$this->product_id			=	$Item->getProductId();
		$this->product_text			=	$Item->getProductName();
	}

	function setVarsManually($amount, $price_at_ordering, $vat_at_ordering, $product_id, $product_text) {
		$this->amount 				= 	$amount;
		$this->price_at_ordering 	= 	$price_at_ordering;
		$this->vat_at_ordering		=	$vat_at_ordering;
		$this->product_id			=	$product_id;
		$this->product_text			=	$product_text;
	}

	function changePrice($new_price) {
		$this->price_at_ordering 	=	$new_price;
		upRec($this->db_table_articles, $this->id, 'price_at_ordering', $new_price);
	}

	function getId() {
		return $this->id;
	}

	function getAmount() {
		return $this->amount;
	}

	function getProductID() {
		return $this->product_id;
	}

	function getProductText() {
		return $this->product_text;
	}

	function getProductPrice() {
		return $this->price_at_ordering;
	}

	function getProductVatPercentage() {
		return $this->vat_at_ordering;
	}

	function getProductGroup() {
		return getMyo($this->db_table_products, $this->product_id, 'group_id');
	}

	function getArticlePrice() {
		return $this->amount * $this->price_at_ordering;
	}

	function getArticleVatAmount() {
		return round($this->amount * $this->vat_at_ordering * $this->price_at_ordering / (100 + $this->vat_at_ordering));
	}

	function isDelivery() {
		return ($this->product_id == DELIVERY_ARTICLE);
	}

	function isVatReturn() {
		return ($this->product_id == VAT_RETURN_ARTICLE);
	}
}
?>