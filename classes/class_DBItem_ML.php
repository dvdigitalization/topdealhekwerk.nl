<?php
/*
Title:		DB Item multilanguage
File: 		classes/class_DBItem_ML.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

class DBItem_ML {
	
	private $db_table, $id;
	
	// *** Instantiate a new DbItem
	function DBItem_ML($id = 0) {
		
		global $db_table, $db_table_langless, $name_field, $extra_props;
		
		$this->id 			= 	(int) $id;
		$this->db_table 	= 	$db_table;
		$this->db_table_ll	=	$db_table_langless;
		$this->props		=	array_keys($extra_props) + array(99 => $name_field);
		
		// Build this item
		if ($this->id) {
			$this->readFromDb();
		}	  			
	}

	// *** Read values from Db
	function readFromDb() {

		$res = eq("SELECT * FROM $this->db_table WHERE id='$this->id';");
		$mya = mfa($res);
        
        foreach ($this->props as $key) {
        	$this->vars[$key] = $mya[$key];
		}
	}
	
	// *** Read post vars, if there are any
	function readPostVars() {
		foreach ($this->props as $key) {
			$this->vars[$key] = $_POST[$key];
		}
	}
	
	// *** Get the name of this item
	function getVar($var) {
		return $this->vars[$var];	
	}
	
	// *** Save this item
	function save() {
		
		global $extra_prop_functions, $installed_languages;
		
		foreach ($this->vars as $key => $value) {
			
			if (isset($extra_prop_functions[$key])) {
				$value = $extra_prop_functions[$key][1]($value);
			}	
			
			$fields[]	=	$key;
			$values[]	=	$value;
		}
		
		if ($this->id) {
			upRec($this->db_table,$this->id,$fields,$values);
		}
		else {
		
			//Always maintain sortability, just in case we want to change the table later
			$fields[]	=	'item_order';
			$values[]	=	getMyo($this->db_table, 'id > 0', 'item_order', 'item_order', 'DESC', 1) + 1;
		
			foreach($installed_languages as $lang) {
				$this->id =	insRec($this->db_table_ll.$lang,$fields,$values);	
			}
		}
		return $this->id;
	}
	
	// *** Delete this item
	function delete($permanently = false, $clear_dependencies = false) {
		
		global $dependent_fields, $dependent_tables, $installed_languages;
		
		if ($permanently) {
			
			foreach ($dependent_tables as $key => $table) {
				$field = $dependent_fields[$key];

				//Dolf check
				if (substr($table,0, strlen(DIGI_DB_PREFIX)) != DIGI_DB_PREFIX) {
					$table = DIGI_DB_PREFIX.$table;
				}
				
				if ($clear_dependencies) {
					upRec($table,"$field = '$this->id'",$field,'');
				}
				else {
					$res = eq("SELECT id FROM $table WHERE $field = '$this->id';");
					if (mnr($res)) {
						return false;	
					}
				}
			}
			
			foreach($installed_languages as $lang) {
				$item_order = getMyo($this->db_table_ll.$lang, $this->id, 'item_order');
				delRec($this->db_table_ll.$lang, $this->id);
				fixSortOrder($this->db_table_ll.$lang, 'item_order', $item_order);
			}
			return true;
		}
		else {
			foreach($installed_languages as $lang) {
				upRec($this->db_table_ll.$lang, $this->id, 'deleted', 1);
			}
			return true;	
		}
	}

	// *** Move item up
	function move_up() {
	
		global $installed_languages;
	
		$old_order = getMyo($this->db_table, $this->id, 'item_order');
		$new_order = $old_order - 1;
		$change_id = getMyo($this->db_table, "item_order = '$new_order'", 'id');
	
		if (!empty($change_id)) {	
			foreach($installed_languages as $lang) {			
				upRec($this->db_table_ll.$lang, $this->id, 'item_order', $new_order);
				upRec($this->db_table_ll.$lang, $change_id,'item_order', $old_order);
			}
		}
	}
	
	// *** Move item down
	function move_down() {

		global $installed_languages;

		$old_order = getMyo($this->db_table, $this->id, 'item_order');
		$new_order = $old_order + 1;
		$change_id = getMyo($this->db_table, "item_order = '$new_order'", 'id');
		
		if (!empty($change_id)) {
			foreach($installed_languages as $lang) {		
				upRec($this->db_table_ll.$lang, $this->id, 'item_order', $new_order);
				upRec($this->db_table_ll.$lang, $change_id,'item_order', $old_order);
			}
		}
	}

	// *** Activate an item
	function activate() {
		global $installed_languages;
		foreach($installed_languages as $lang) {
			upRec($this->db_table_ll.$lang, $this->id, 'active', 1);
		}
	}
	
	// *** De-activate an item
	function de_activate() {
		global $installed_languages;
		foreach($installed_languages as $lang) {
			upRec($this->db_table_ll.$lang, $this->id, 'active', 0);
		}
	}
}    
?>