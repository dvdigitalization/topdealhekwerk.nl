<?php

/**
 * Export engine
 *
 * @package 	DigiCMS v3.x
 * @file		system/libraries/export.php
 * @version		3.00
 * @author		Digitalization
 * @email		info@digitalization.nl
 * @link		http://www.digitalization.nl/
 * @copyright	All code copyright 2008,2009 by Digitalization
 */

 /**
 * Generate a random string/number/hex value of a certain amount of characters
 *
 * @param 	int 	The amount of characters to return
 * @param 	string 	The seed (predefined or custom)
 * @return 	string	The random string
 */
function random($characters = 6, $seed = 'alphanum') {

	//Possible pre-defined seeds
	$seeds['alpha']    	= 	'abcdefghijklmnopqrstuvwqyz';
    $seeds['num']  		= 	'0123456789';
    $seeds['alphanum'] 	= 	'abcdefghijklmnopqrstuvwqyz0123456789';
    $seeds['hexidec']  	= 	'0123456789abcdef';
    $seeds['pass']		=	'abcdefghijkmnpqrstuvzxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789';

    //Set the seed
    if (isset($seeds[$seed])) {
		$seed = $seeds[$seed];
	}

	//Generate the return string
	$return_string 	= '';
	$seed_max 		= strlen($seed) - 1;

	for ($s = 0; $s < $characters; $s++) {
		$return_string .= $seed[mt_rand(0, $seed_max)];
	}
	return $return_string;
}

class Export {

	private	$csv_delimiter	=	',';			//The CSV delimiter character
	private $csv_encase		=	'"';			//The CSV encase character
	private $csv_legend		=	true;			//The first CSV line has the field names in it or not
	private $xml_root		=	'objects';		//The XML root node
	private $xml_object		=	'object';		//The XML object identifier

	private $fields			=	array();
	private $values			=	array();

	/**
	 * Initializes the export engine
	 *
	 */
	public function __construct() {

	}

	/**
	 * Method to directly force-download a CSV file with the export data
	 *
	 * @param 	mixed 	Filename to use
	 * @return 	void
	 */
	public function download_csv($filename) {

		//Check if we can use a temporary file (this prevents memory issues with large amounts of data)
		$this->use_temp_file($filename, 'csv', 'text/comma-separated-values');

		//If we get here, we couldn't use a temporary file, so we just output the content on the fly.
		//Putting everything in a variable first to determine the "filesize" could lead to memory problems with large amounts of data.
		$this->output_header($filename, 'text/comma-separated-values');

		//Output UTF-8 indicator
		echo "\xEF\xBB\xBF";

		//Output the first row of field names
		if ($this->csv_legend) {
			echo implode($this->csv_delimiter, $this->csv_prepare($this->fields));
		}

		//Loop the values now
		foreach ($this->values as $values) {

			//Replace " by double "" to preserve them, and remove \r characters
			foreach ($values as $key => $value) {
				$values[$key] = str_replace('"','""', $value);
			}

			//Output the line of data
			echo "\r\n".implode($this->csv_delimiter, $this->csv_prepare($values));
		}
		exit;
	}

	/**
	 * Writes the export content to a CSV file
	 *
	 * @param 	string 	The file location (full path)
	 * @param 	bool 	Overwrite if file exists?
	 * @return 	int 	The filesize
	 */
	public function write_csv($filename, $overwrite = true) {

		//Already exists?
		if (!$overwrite and file_exists($filename)) {
			return 0;
		}

		//Open handle
		if ($file_handle = @fopen($filename, 'wb')) {

			//Write UTF-8 indicator
			@fwrite($file_handle, "\xEF\xBB\xBF");

			//Write the first row of field names
			if ($this->csv_legend) {
				@fwrite($file_handle, implode($this->csv_delimiter, $this->csv_prepare($this->fields)));
			}

			//Loop the values now
			foreach ($this->values as $values) {

				//Replace " by double "" to preserve them, and remove \r characters
				foreach ($values as $key => $value) {
					$values[$key] = str_replace('"', '""', $value);
				}

				//Write the line of data
				@fwrite($file_handle, "\r\n".implode($this->csv_delimiter, $this->csv_prepare($values)));
			}

			//Close the filehandle and return the filesize
			@fclose($file_handle);
			return filesize($filename);
		}
		return 0;
	}

	/**
	 * Method to directly force-download an XML file with the export data
	 *
	 * @param 	mixed 	Filename to use
	 * @return 	void
	 */
	public function download_xml($filename) {

		//Check if we can use a temporary file (this prevents memory issues with large amounts of data)
		$this->use_temp_file($filename, 'xml', 'text/xml');

		//If we get here, we couldn't use a temporary file, so we just output the content on the fly.
		//Putting everything in a variable first to determine the "filesize" could lead to memory problems with large amounts of data.
		$this->output_header($filename, 'text/xml');

		//Output the XML header and root node
		echo "<?xml version=\"1.0\"?>\n<".$this->xml_root.">";

		//Loop the values now
		foreach ($this->values as $values) {

			//Start the object node
			echo "\n\t<".$this->xml_object.">";

			//Output the keys and values
			foreach ($values as $key => $value) {

				//Fix field name
				$field = str_replace(array(" ","\n","\r", "&", "<", ">"), '', $this->fields[$key]);

				//Remove \r
				$value = str_replace("\r", '', $value);

				//CDATA needed?
				if (mb_strstr($value, "\n") !== false or mb_strstr($value, "&") !== false or mb_strstr($value, "<") !== false) {
					$value = "<![CDATA[".$value."]]>";
				}

				//Output
				echo "\n\t\t<".$field.">".$value."</".$field.">";
			}

			//Close the object node
			echo "\n\t</".$this->xml_object.">";
		}

		//Close the root node
		echo "\n</".$this->xml_root.">";
		exit;
	}

	/**
	 * Writes the export content to an XML file
	 *
	 * @param 	string 	The file location (full path)
	 * @param 	bool 	Overwrite if file exists?
	 * @return 	int 	The filesize
	 */
	public function write_xml($filename, $overwrite = true) {

		//Already exists?
		if (!$overwrite and file_exists($filename)) {
			return 0;
		}

		//Open handle
		if ($file_handle = @fopen($filename, 'wb')) {

			//Write the first xml header and root node
			@fwrite($file_handle, "<?xml version=\"1.0\"?>\n<".$this->xml_root.">");

			//Loop the values now
			foreach ($this->values as $values) {

				//Start the object node
				@fwrite($file_handle, "\n\t<".$this->xml_object.">");

				//Write the keys and values
				foreach ($values as $key => $value) {

					//Fix field name
					$field = str_replace(array(" ","\n","\r", "&", "<", ">"), '', $this->fields[$key]);

					//Remove \r
					$value = str_replace("\r", '', $value);

					//CDATA needed?
					if (mb_strstr($value, "\n") !== false or mb_strstr($value, "&") !== false or mb_strstr($value, "<") !== false) {
						$value = "<![CDATA[".$value."]]>";
					}

					//Write
					@fwrite($file_handle, "\n\t\t<".$field.">".$value."</".$field.">");
				}

				//Close the object node
				@fwrite($file_handle, "\n\t</".$this->xml_object.">");
			}

			//Close the root node
			@fwrite($file_handle, "\n</".$this->xml_root.">");

			//Close the filehandle and return the filesize
			@fclose($file_handle);
			return filesize($filename);
		}
		return 0;
	}

	/**
	 * Method to directly force-download an XLS file with the export data
	 *
	 * @param 	mixed 	Filename to use
	 * @return 	void
	 */
	public function download_xls($filename) {

		//Check if we can use a temporary file (this prevents memory issues with large amounts of data)
		$this->use_temp_file($filename, 'xls', 'application/vnd.ms-excel');

		//If we get here, we couldn't use a temporary file, so we just output the content on the fly.
		//Putting everything in a variable first to determine the "filesize" could lead to memory problems with large amounts of data.
		$this->output_header($filename, 'application/vnd.ms-excel');

		//Output the XLS BOF
		echo $this->xls_bof();

		//Output the field names
		echo $this->xls_row($this->fields);

		//Loop the values now
		foreach ($this->values as $values) {
			echo $this->xls_row($values);
		}

		//Output the XLS EOF
		echo $this->xls_eof();
		exit;
	}

	/**
	 * Writes the export content to an XLS file
	 *
	 * @param 	string 	The file location (full path)
	 * @param 	bool 	Overwrite if file exists?
	 * @return 	int 	The filesize
	 */
	public function write_xls($filename, $overwrite = true) {

		//Already exists?
		if (!$overwrite and file_exists($filename)) {
			return 0;
		}

		//Open handle
		if ($file_handle = @fopen($filename, 'wb')) {

			//Write the first XLS BOF
			@fwrite($file_handle, $this->xls_bof());

			//Write the field names
			@fwrite($file_handle, $this->xls_row($this->fields));

			//Loop the values now
			foreach ($this->values as $values) {
				@fwrite($file_handle, $this->xls_row($values));
			}

			//Write the XLS EOF
			@fwrite($file_handle, $this->xls_eof());

			//Close the filehandle and return the filesize
			@fclose($file_handle);
			return filesize($filename);
		}
		return 0;
	}

	/**
	 * Generate an XLS row
	 *
	 * @param 	array 	An array of data/values
	 * @return 	string	Binary data
	 */
	private function xls_row($data) {

		//Set row counter if not set
		if (!isset($this->xls_row)) {
			$this->xls_row = 0;
		}

		//Initialization
		$col = 0;
		$row = $this->xls_row;
		$xls = '';

		//Loop the data and generate cells
		foreach ($data as $value) {
			$xls .= $this->xls_cell($row, $col, $value);
			$col++;
		}

		//Increment the row counter
		$this->xls_row++;

		//Return the row data
		return $xls;
	}

	/**
	 * Prepare an array of values for CSV exporting
	 *
	 * @param 	array  	The data
	 * @return 	array  	The rpepared data
	 */
	private function csv_prepare($data) {

		//Loop the data
		foreach ($data as $key => $value) {

			//If we find any of the special characters...
			if (	strpos($value, $this->csv_delimiter) !== false or
		          	strpos($value, $this->csv_encase) !== false or
		          	strpos($value, "\n") !== false or
		          	strpos($value, "\r") !== false or
		          	strpos($value, "\t") !== false or
		          	strpos($value, ' ') !== false) {

 				//...we must encase the string and replace encasement characters
 				$data[$key] = 	$this->csv_encase.
				 				str_replace($this->csv_encase, $this->csv_encase.$this->csv_encase, $value).
				 				$this->csv_encase;
			 }
		}

		//Return prepared data
		return $data;
	}

	/**
	 * Generates XLS format start of file
	 *
	 * @return 	string	Binary data
	 */
	private function xls_bof() {
		return pack("ssssss", 0x809, 0x8, 0x0, 0x10, 0x0, 0x0);
	}

	/**
	 * Generates XLS format end of file
	 *
	 * @return 	string	Binary data
	 */
	private function xls_eof() {
		return pack("ss", 0x0A, 0x00);
	}

	/**
	 * Generate a cell for XLS format
	 *
	 * @param 	int 	The row
	 * @param 	int 	The column
	 * @param 	mixed 	The data
	 * @return 	string	Binary data
	 */
	private function xls_cell($row, $col, $data) {
		if (is_numeric($data)) {
			return $this->xls_number($row, $col, $data);
		}
		else {
			return $this->xls_string($row, $col, $data);
		}
	}

	/**
	 * Generates XLS format string entry
	 *
	 * @param 	int 	The row
	 * @param 	int 	The column
	 * @param 	string 	The string
	 * @return 	string	Binary data
	 */
	private function xls_string($row, $col, $string) {
		$length = mb_strlen($string);
		return pack("ssssss", 0x204, 8 + $length, $row, $col, 0x0, $length).$string;
	}

	/**
	 * Generates XLS format number entry
	 *
	 * @param 	int 	The row
	 * @param 	int 	The column
	 * @param 	mixed 	The number
	 * @return 	string	Binary data
	 */
	private function xls_number($row, $col, $number) {
		return 	pack("sssss", 0x203, 14, $row, $col, 0x0).
				pack("d", $number);
	}

	/**
	 * Helper method to write to a temporary file first, instead of directly ouputting
	 *
	 * @param 	string 	String of the filename to use
	 * @param 	string 	The type of output desired
	 * @return 	void
	 */
	private function use_temp_file($filename, $type, $mime_type) {

		//Must have valid type
		if ($type != 'csv' and $type != 'xls' and $type != 'xml') {
			$type = 'csv';
		}

		//Check if we can use the temporary folder
		if (file_exists(TEMP_FOLDER) and is_dir(TEMP_FOLDER) and is_writable(TEMP_FOLDER)) {

			//Generate temp filename
			$temp_file = TEMP_FOLDER.'export_'.random(8).'.'.$type;

			//Try to write the temporary file
			$func_name = 'write_'.$type;
			if ($file_size = $this->$func_name($temp_file)) {

				//We succeeded, so all we need now is to output the headers
				$this->output_header($filename, $mime_type);
				header("Content-Length: ".$file_size);

				//Output the file contents
				readfile($temp_file);

				//Delete the temp file and exit
				@unlink($temp_file);
				exit;
			}
		}
	}

	/**
	 * Output force-download headers (excluding filesize)
	 *
	 * @param 	mixed 	The filename
	 * @param 	mixed 	The mime-type
	 * @return 	void
	 */
	private function output_header($filename, $mime_type) {

		//Generate the server headers
		if (strstr($_SERVER['HTTP_USER_AGENT'], "MSIE")) {
			header('Content-Type: "'.$mime_type.'"');
			header('Content-Disposition: attachment; filename="'.$filename.'"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header("Content-Transfer-Encoding: binary");
			header('Pragma: public');
		}
		else {
			header('Content-Type: "'.$mime_type.'"');
			header('Content-Disposition: attachment; filename="'.$filename.'"');
			header("Content-Transfer-Encoding: binary");
			header('Expires: 0');
			header('Pragma: no-cache');
		}
	}

	/**
	 * Set the fields
	 *
	 * @param 	mixed 	String or array of fields
	 * @return 	void
	 */
	public function set_fields($fields) {

		//Verify fields
		if (!is_array($fields)) {
			$fields = explode(',',$fields);
		}
		$this->fields = $fields;
	}

	/**
	 * Set the values to export
	 *
	 * @param 	array 	The values
	 * @return 	void
	 */
	public function set_values($values) {

		//Verify values
		if (!is_array($values)) {
			$values	= array();
		}
		$this->values = $values;
	}

	/**
	 * Clear fields and values
	 *
	 * @return 	void
	 */
	public function clear() {
		$this->fields	=	array();
		$this->values	=	array();
	}

	/**
	 * Set the XML root node
	 *
	 * @param 	string 	The XML root node name
	 * @return 	void
	 */
	public function set_xml_root($root) {
		$this->xml_root = $root;
	}

	/**
	 * Set the XML object name
	 *
	 * @param 	string 	The XML object name
	 * @return 	void
	 */
	public function set_xml_object($object) {
		$this->xml_object = $object;
	}

	/**
	 * Set the CSV delimiter
	 *
	 * @param 	string 	The desired delimiter
	 * @return 	void
	 */
	public function set_csv_delimiter($delimiter) {
		$this->csv_delimiter = $delimiter;
	}

	/**
	 * Set the CSV encase character
	 *
	 * @param 	string 	The desired encase character
	 * @return 	void
	 */
	public function set_csv_encase($encase) {
		$this->csv_encase = $encase;
	}

	/**
	 * Set the CSV legend
	 *
	 * @param 	bool 	Show fields legend on first CSV line or not
	 * @return 	void
	 */
	public function set_csv_legend($legend = true) {
		$this->csv_legend = $legend;
	}

	/**
	 * Get the CSV delimiter used
	 *
	 * @return 	string 	The delimiter
	 */
	public function get_csv_delimiter() {
		return $this->csv_delimiter;
	}

	/**
	 * Get the CSV encase character used
	 *
	 * @return 	string 	The encase character
	 */
	public function get_csv_encase() {
		return $this->csv_encase;
	}
}

?>