<?php
/*
Title:		Database class
File: 		classes/class_DB.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

class DB {

	private $link;
	private $server, $username, $password, $database;

	function DB($server, $username, $password, $database) {
		$this->server 	= $server;
		$this->username = $username;
		$this->password = $password;
		$this->database = $database;
		$this->connect();
	}

	function connect() {
		if (!$this->link = @mysql_connect($this->server, $this->username, $this->password)) {
			echo "This website is experiencing some database connectivity problems. Please try again in a short while.";
			exit;
		}
		if (!@mysql_select_db($this->database, $this->link)) {
			echo "This website is experiencing some database selection problems. Please try again in a short while.";
			exit;
		}
	}
	
	function disconnect() {
		@mysql_close($this->link);
	}

	function eq($query) {
		global $_file;
	 	if ($this->link) {
			if ($result = 	@mysql_query($query, $this->link)) {
				return $result;			
			}
			else {
				$error = mysql_error();
				if ($error == 'Lost connection to MySQL server during query' or 
					$error == 'MySQL server has gone away') {
					echo "This website is experiencing some database problems. Please try again in a short while.";
					exit;									
				}
				else {
					trigger_error(mysql_error()."<br/><br/>The complete query was: <b>$query</b>
						<br/><br/>Error originated from <b>$_file</b><br/><br/>", E_USER_WARNING.', originated from '.$_file);
				}										
			}
		}
	}

	function __sleep() {
		$this->disconnect();
	}

	function __wakeup() {
		$this->connect();
	}
}
?>