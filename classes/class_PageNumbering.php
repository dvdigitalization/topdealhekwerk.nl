<?php
/*
Title:		Page numbering generation class
File: 		classes/class_PageNumbering.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
*/

class PageNumbering {

	public function PageNumbering(	$db_table, $where_clause = '', $link_appendix, $items_per_page = 10, $max_pages_in_list = 20,
									$show_when_empty = false, $hide_empty_prev_next = false) {

		//Set data
		$this->db_table				=	$db_table;
		$this->items_per_page		=	$items_per_page;
		$this->max_pages_in_list	=	$max_pages_in_list;
		$this->where_clause			=	$where_clause;
		$this->link_appendix		=	$link_appendix;
		$this->show_when_empty		=	$show_when_empty;
		$this->hide_empty_prev_next	=	$hide_empty_prev_next;

		//Set default templates
		$this->link_template		=	"<a href='[LINK]' title='[X]'>[X]</a>";
		$this->current_template		=	"[X]";
		$this->imploder				=	' ';
		$this->prev_next_sep		=	' ';
		$this->dots					=	'..';
		$this->prev_template		=	'&laquo;';
		$this->next_template		=	'&raquo;';
		$this->count_column			=	1;

		//Vars
		$this->page					=	1;
		$this->pages_needed			=	0;
		$this->total_items			=	0;
		$this->calculated			=	false;

		//Set standard stuff
		$this->page_trigger			=	'page';
	}

	public function setCountColumn($column) {
		$this->count_column		=	$column;
	}

	public function setLinkTemplate($template) {
		$this->link_template	=	$template;
	}

	public function setCurrentTemplate($template) {
		$this->current_template	=	$template;
	}

	public function setPrevTemplate($template) {
		$this->prev_template	=	$template;
	}

	public function setNextTemplate($template) {
		$this->next_template	=	$template;
	}

	public function setImploder($imploder) {
		$this->imploder			= 	$imploder;
	}

	public function setPrevNextSeparator($separator) {
		$this->prev_next_sep	=	$separator;
	}

	public function setDots($dots) {
		$this->dots				= 	$dots;
	}

	public function setPageTrigger($trigger) {
		$this->page_trigger		= 	$trigger;
	}

	private function makePageItem($x, $page) {

		global $_file;

		if ($x != $page) {
			$item = str_replace('[X]', $x, $this->link_template);
			$item = str_replace('[LINK]', $_file.'?'.$this->page_trigger.'='.$x.'&'.$this->link_appendix, $item);
		}
		else {
			$item = str_replace('[X]', $x, $this->current_template);
			$item = str_replace('[LINK]', $_file.'?'.$this->page_trigger.'='.$x.'&'.$this->link_appendix, $item);
		}
		return $item;
	}

	private function makePrevAndNext($page, $pages_needed) {

		global $_file;

		if ($page != $pages_needed) $next = $page+1;
		if ($page != 1) 			$prev = $page-1;

		if (isset($next)) {
			$next = "<a href='".$_file.'?'.$this->page_trigger.'='.$next.'&'.$this->link_appendix."'>".$this->next_template."</a>";
		}
		else {
			$next = $this->hide_empty_prev_next ? '' : $this->next_template;
		}

		if (isset($prev)) {
			$prev = "<a href='".$_file.'?'.$this->page_trigger.'='.$prev.'&'.$this->link_appendix."'>".$this->prev_template."</a>";
		}
		else {
			$prev = $this->hide_empty_prev_next ? '' : $this->prev_template;
		}

		return array($prev, $next);
	}

	private function calculateDetails() {

		//What page are we viewing
		$this->page = isset($_GET[$this->page_trigger]) ? (int) $_GET[$this->page_trigger] : 1;
		$this->page = $this->page ? $this->page : 1;

		//Calculate some stuff and initialize pages array
		$this->total_items 		= 	countResults($this->db_table, $this->where_clause, '', $this->count_column);
		$this->pages_needed 	= 	ceil($this->total_items / $this->items_per_page);
		$this->pages_needed		=	$this->pages_needed == 0 ? 1 : $this->pages_needed;
		$this->my_limit 		= 	($this->page - 1) * $this->items_per_page;

		// If the page number is too high and all items will be hidden, then reset the 'my_limit' so items starting
		// from zero will be shown
		if($this->my_limit > $this->total_items) {
			$this->my_limit = 0;
		}

		$this->calculated		=	true;
	}

	public function getPage() {
		return $this->page;
	}

	public function getPagesNeeded() {
		return $this->pages_needed;
	}

	public function getMysqlLimitStart() {
		return $this->my_limit;
	}

	public function getTotalItems() {
		return $this->total_items;
	}

	public function getPrevNextHTML() {

		if (!$this->calculated) {
			$this->calculateDetails();
		}
		return $this->makePrevAndNext($this->page, $this->pages_needed);
	}

	public function getPagesHTML($include_prev_next = true) {

		if (!$this->calculated) {
			$this->calculateDetails();
		}

		$page 				= 	$this->page;
		$pages				=	array();

		//If the total amount of pages exceeds the max amount of pages to show in the list...
		if ($this->pages_needed > $this->max_pages_in_list) {

			//Define lower and upper limits
			$lower_limit = ceil($this->max_pages_in_list / 2);
		    $upper_limit = $this->pages_needed - floor($this->max_pages_in_list / 2);

		    //If the page we are on is between the limits...
			if ($page > $lower_limit and $page < $upper_limit) {
		        $pages[] = $this->dots;
		        for ($x = ($page - floor($this->max_pages_in_list/2)); $x <= ($page + floor($this->max_pages_in_list/2)); $x++) {
		        	$pages[] = $this->makePageItem($x, $page);
		        }
		        $pages[] = $this->dots;
		    }

		    //If it is below or equal to the lower limit...
		    elseif ($page <= $lower_limit) {
		        for ($x = 1; $x <= $this->max_pages_in_list; $x++) {
		            $pages[] = $this->makePageItem($x, $page);
		        }
		        $pages[] = $this->dots;
		    }

		    //If it is above or equal to the upper limit...
		    elseif ($page >= $upper_limit) {
		        $pages[] = $this->dots;
		        for ($x = ($this->pages_needed - $this->max_pages_in_list + 1); $x <= $this->pages_needed; $x++) {
		            $pages[] = $this->makePageItem($x, $page);
		        }
		    }
		}

		//If it doesn't exceed the max amount of pages...
		else {

			//Fill array with links to the required pages
			for ($x = 1; $x <= $this->pages_needed; $x++) {
			    $pages[] = $this->makePageItem($x, $page);
			}
		}

		//Now, if there are more than one pages, or when we want to override default behavior, we show the html
		if ($this->pages_needed > 1 or $this->show_when_empty) {

			if ($include_prev_next) {
				list($prev, $next) = $this->makePrevAndNext($page, $this->pages_needed);
				return $prev.$this->prev_next_sep.implode($this->imploder,$pages).$this->prev_next_sep.$next;
			}
			else {
				return implode($this->imploder,$pages);
			}
		}

		//Otherwise return blank
		else {
			return '';
		}
	}
}

?>