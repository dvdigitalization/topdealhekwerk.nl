<?php
/*
Title:		Garbage collector class
File: 		classes/class_GarbageCollector.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

class GarbageCollector {
	
	function GarbageCollector() {
		
		if ($this->hasToCollect()) {
			
			require_once('inc/definitions/def_garbage.php');
			
			$this->gc_tables		=	$gc_tables;
			$this->gc_dependencies	=	$gc_dependencies;
			
			$this->collectNow();
			$this->updateCollectedTime();
		}
	}
	
	function collectNow() {
		foreach ($this->gc_tables as $table) {
			$this->collectTable($table);
		}
	}
	
	function collectTable($table) {
		
		$res = eq("SELECT id FROM ".DIGI_DB_PREFIX.$table." WHERE deleted > 0;");
		while ($myo = mfo($res)) {
			
			//Find dependencies if needed
			if (isset($this->gc_dependencies[$table])) {
				
				//If a dependency is found, skip the item for now
				if ($this->findDependencies($myo->id, $this->gc_dependencies[$table])) {
					//echo "Dependency found for item #$myo->id from $table.<br/>";
					continue;
				}
			}
			
			//Delete the item
			//echo "Deleting item #$myo->id from $table now.<br/>";
			delRec(DIGI_DB_PREFIX.$table, $myo->id);
		}
	}
	
	function findDependencies($db_id, $dependencies) {
		foreach ($dependencies as $dependent_table => $dependent_field) {
			if (getMyo(DIGI_DB_PREFIX.$dependent_table, "$dependent_field = '$db_id'", 'id')) {
				return true;
			}
		}
		return false;
	}
	
	function updateCollectedTime() {
		changeSetting('GARBAGE_LAST_COLLECTED',	time());
	}
	
	function hasToCollect() {
		
		if (isset($_GET['gc_now'])) return true;
		global $cfg, $s_id;
		if ($s_id and (time() - $cfg['GARBAGE_LAST_COLLECTED'] > $cfg['GARBAGE_COLLECT_INTERVAL']*86400)) {
			return true;
		}
		return false;
	}
}
?>