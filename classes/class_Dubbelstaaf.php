<?php

/**
 * @author Digitalization
 * @copyright 2009
 */

require_once('class_Paneel.php');

class Dubbelstaaf extends Paneel {

	public function __construct($category_id) {

		parent::__construct($category_id);
	}
	
	public function get_poles_html() {

		global $db_table_products, $db_table_cats, $poles_options, $pole_categories, $pole_height_map, $vat_multiplier;

		if (!$this->height) {
			return "<p>Kies eerst de hoogte van het hekwerk.</p>";
		}

		$html = "<table cellpadding='0' cellspacing='0' border='0'>";

		//Get poles
		$pole_cat	=	$pole_categories[$this->category_id];		//Beton palen 1
		$prices[0]	=	'';
		
		//Get matching height
		$height = isset($pole_height_map[$pole_cat][$this->height]) ? $pole_height_map[$pole_cat][$this->height] : 0;

		//Get matching prices for polecat
		$res	 = eq("	SELECT 	normal_price, price, sale_price, on_sale, price_unit
						FROM 	$db_table_products
						LEFT JOIN $db_table_cats ON (category_id = $db_table_cats.id)
						WHERE 	category_id = '$pole_cat' AND height='$height' AND color_id='$this->color'
		");

		if (!mnr($res)) {
			unset($poles_options[1]);
		}
		else {
			$pole = mfo($res);
			$pole->price =	parseAmount($pole->on_sale ? $pole->sale_price*$vat_multiplier : $pole->price*$vat_multiplier, '&euro; ');
			$prices[1]	 =	'Normaal: '.parseAmount($pole->normal_price*$vat_multiplier, '&euro; ').' p/st, voor: '.$pole->price.' p/st';
		}

		//Loop the pole options
		foreach ($poles_options as $key => $value) {

			$html .= "
				<tr>
					<td width='25' height='22' align='left' valign='top'><input type='radio' value='$key' onclick='changed_poles();' name='poles' ".($key == 0 ? "checked='checked'" : '')." /></td>
					<td align='left' valign='middle'>$value ".$prices[$key]."</td>
				</tr>
			";
		}

		$html .= "</table>";
		return $html;
	}
}

?>