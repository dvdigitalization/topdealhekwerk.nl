<?php
/*
Title:		Site login management class
File: 		classes/class_Login_Site.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization

Extends:	classes/class_Login.php

*/

class Login_Site extends Login {

	// *** Constructor, takes the database table to use as a parameter
	function Login_Site($db_table, $extra_db_fields = '') {

		global $site_name;

		//Set the database table
		$this->db_table 			=	DIGI_DB_PREFIX.$db_table;
		$this->db_table_groups 		=	DIGI_DB_PREFIX.'extranet_user_groups';

		//Set default max idle time in seconds
		$this->max_idle_time		=	1200;

		//Set the max number of failed login attempts
		$this->max_attempts			=	4;

		//Redirect after succesful login?
		$this->after_login_page		=	'';

		//Set the username and password fields (in the login form)
		$this->username_form_field	=	'login_field_user';
		$this->password_form_field	=	'login_field_password';
		$this->remember_field		=	'remember_me';

		//Group id
		$this->group_id				=	'';

		//Set the cookie name
		$c_name = '';
	    for ($i=0; $i<strlen($site_name); $i++) {
	        if (preg_match('([0-9a-z])', $site_name[$i])) {
	            $c_name .= $site_name[$i];
	        }
	    }
		$this->cookie_name			=	$c_name.'COOKIE';

		//Set the standard fields  to fetch
		$this->info_fields_db		=	array(	'id','email','email_validated','active',
												'username','password','cookie_id','login_attempts','group_id');

		//Default extra fields
		if (!$extra_db_fields or !is_array($extra_db_fields)) {
			$extra_db_fields		=	array(	'first_name','last_name');
		}

		//Add extra fields
		foreach ($extra_db_fields as $db_field) {
			$this->info_fields_db[]	=	$db_field;
		}

		//Call the base class login function
		$this->Login();

		//Catch login and logout cases
		$this->catchLoginAndLogout();
	}

	// *** Catch all possible login and logout cases
	function catchLoginAndLogout() {

		global $cfg;

		//Login not allowed?
		if (!$cfg['ALLOW_LOGIN_MAINSWITCH']) {
			return null;
		}

		//Logout by link
		if (!empty($_GET['logout'])) {
			$this->logout();
			return null;
		}

		//Login by form
		if (!empty($_POST['login'])) {
			$remember_me = isset($_POST[$this->remember_field]) ? true : false;
			$this->loginByForm($_POST[$this->username_form_field],$_POST[$this->password_form_field],$remember_me);
			return null;
		}

		//Login by link (for example in an email, but only allowed on non-cms)
		if (!empty($_GET['login']) and !empty($_GET['validate'])) {
			$this->loginByLink($_GET['login'], $_GET['validate']);
			return null;
		}

		//Login by cookie if allowed and if not already logged in by cookie
		if ($cfg['SITE_ALLOW_COOKIE_LOGIN'] and isset($_COOKIE[$this->cookie_name]) and empty($_SESSION['login_cookie'])) {
			$this->loginByCookie();
			return null;
		}
	}

	// *** Login process by form
	function loginByForm($username, $password, $remember_me) {

		global $cfg;

		//Check for empty data
		if (empty($username) or empty($password)) {
			$this->login_msg = LOGIN_ALL_FIELDS;
			return null;
		}

		//Set the data to validate
		$password		=	dbSaltSite($password);
		$email			=	validateEmail($username);
		$username		=	validateSimpleString($username);
		$remember_me	=	(int) $remember_me;

		//Set the fields to fetch from the database
		$fields = implode(',', $this->info_fields_db);

		//Match against database
		$res = eq("	SELECT $fields FROM $this->db_table
					WHERE (email = '$email' AND email <> '')
					OR (username = '$username' AND username <> '');");

		//No results?
		if (!mnr($res)) {
			$this->login_msg = LOGIN_INVALID;
			return null;
		}

		//Validate result
		else {

			//Get the result
			$myo = mfo($res);

			//Max failed login attempts exceeded?
			if ($myo->login_attempts >= $this->max_attempts) {

				$this->login_msg = str_replace(	'[ADMIN]',
												"<a href='mailto:$cfg[ADMIN_EMAIL]'>$cfg[ADMIN_NAME]</a>",MAX_ATTEMPTS_REACHED);
				return null;
			}

			//Compare validation string against password hash
			else if ($password != $myo->password) {

				//Update the login attempts amount
				upRec($this->db_table, $myo->id, 'login_attempts', $myo->login_attempts + 1);

				$attempts_left 	 = $this->max_attempts - $myo->login_attempts - 1;
				if ($attempts_left) {
					$this->login_msg = LOGIN_INVALID.' '.str_replace('[ATTEMPTS]',$attempts_left,ATTEMPTS_LEFT);
				}
				else {
					$this->login_msg = LOGIN_INVALID.' '.str_replace(	'[ADMIN]',
												"<a href='mailto:$cfg[ADMIN_EMAIL]'>$cfg[ADMIN_NAME]</a>",MAX_ATTEMPTS_REACHED);
				}
				return null;
			}

			//Email validation check
			else if (!$myo->email_validated) {
				$this->login_msg = str_replace('[ADMIN]',
								   "<a href='mailto:$cfg[ADMIN_EMAIL]'>$cfg[ADMIN_NAME]</a>", LOGIN_NOT_VALIDATED);
				return null;
			}

			//Account disabled check
			else if (!$myo->active) {
				$this->login_msg = str_replace('[ADMIN]',
								   "<a href='mailto:$cfg[ADMIN_EMAIL]'>$cfg[ADMIN_NAME]</a>", LOGIN_NOT_ALLOWED);
				return null;
			}

			//Group disabled check
			else if ($this->db_table_groups and !getMyo($this->db_table_groups, $myo->group_id, 'active')) {
				$this->login_msg = str_replace('[ADMIN]',
								   "<a href='mailto:$cfg[ADMIN_EMAIL]'>$cfg[ADMIN_NAME]</a>", LOGIN_NOT_ALLOWED);
				return null;
			}

			//Validation succeeded
			else {

				//Pass the database result object to the login ok function
				$this->loginSucceeded($myo, $remember_me);
			}
		}
	}

	// *** Login process by link
	function loginByLink($login_id, $validation_string) {

		//Use global vars
		global $sha1_appendix, $admin_email;

		//Set data to validate
		$login_id				=	(int) $login_id;
		$validation_string		=	$validation_string;		//STRING VALIDATION!!

		//Set the fields to fetch from the database
		$fields = implode(',', $this->info_fields_db);

		//Match against database
		$res = eq("SELECT $fields FROM $this->db_table WHERE id = '$login_id';");

		//No results?
		if (!mnr($res)) {
			$this->login_msg = LOGIN_LINK_INVALID;
			return null;
		}

		//Validate result
		else {

			//Get the result
			$myo = mfo($res);

			//Compare validation string against password hash
			if ($validation_string != sha1($myo->password.$sha1_appendix)) {
				$this->login_msg = LOGIN_LINK_INVALID;
				return null;
			}

			//Max failed login attempts exceeded?
			else if ($myo->login_attempts >= $this->max_attempts) {

				$this->login_msg = str_replace(	'[ADMIN]',
												"<a href='mailto:$cfg[ADMIN_EMAIL]'>$cfg[ADMIN_NAME]</a>",MAX_ATTEMPTS_REACHED);
				return null;
			}

			//Email validation check
			else if (!$myo->email_validated) {
				$this->login_msg = str_replace('[ADMIN]',
								   "<a href='mailto:$cfg[ADMIN_EMAIL]'>$cfg[ADMIN_NAME]</a>", LOGIN_NOT_VALIDATED);
				return null;
			}

			//Account disabled check
			else if (!$myo->active) {
				$this->login_msg = str_replace('[ADMIN]',
								   "<a href='mailto:$cfg[ADMIN_EMAIL]'>$cfg[ADMIN_NAME]</a>", LOGIN_NOT_ALLOWED);
				return null;
			}

			//Group disabled check
			else if ($this->db_table_groups and !getMyo($this->db_table_groups, $myo->group_id, 'active')) {
				$this->login_msg = str_replace('[ADMIN]',
								   "<a href='mailto:$cfg[ADMIN_EMAIL]'>$cfg[ADMIN_NAME]</a>", LOGIN_NOT_ALLOWED);
				return null;
			}

			//Validation succeeded
			else {

				//Cancel the redirect
				$this->after_login_page = '';

				//Pass the database result object to the login ok function
				$this->loginSucceeded($myo, false);
			}
		}
	}

	// *** Login by cookie
	function loginByCookie() {

		//Use global vars
		global $sha1_hash, $_ip;

		//Get cookie vars
		list($email, $cookie, $login_ip) = unserializeHashed(stripslashes($_COOKIE[$this->cookie_name]), $sha1_hash);

		//Invalid vars?
		if (!$email or !$cookie or !$login_ip or ($login_ip != sha1($_ip))) {

		 	//Delete the cookie
		 	setcookie($this->cookie_name);
		 	return null;
		}
		else {

			//Set the vars
			$this->cookie_id		=	$cookie;

			//Set the fields to fetch from the database
			$fields = implode(',', $this->info_fields_db);

			//Validate against database
			$res = eq("	SELECT $fields FROM $this->db_table	WHERE cookie_id = '$this->cookie_id';");
			$myo = mfo($res);

			//No results, email not validated, inactive, invalid credentials or max login attempts failed
			if (!mnr($res) or !$myo->email_validated or !$myo->active or $myo->email != $email or
				$myo->login_attempts >= $this->max_attempts or
				($this->db_table_groups and !getMyo($this->db_table_groups, $myo->group_id, 'active'))) {

			 	//Delete the cookie
			 	setcookie($this->cookie_name);
				return null;
			}

			//Validation succeeded
			else {

				//Cancel the redirect
				$this->after_login_page = '';

				//Pass the database result object to the login ok function
				$this->loginSucceeded($myo, true);
			}
		}
	}

	// *** Logout function
	function logout() {

		global $cfg;

		//Remove session info from database
		if (!empty($this->login_id)) {
			upRec($this->db_table, $this->login_id, 'session_id', '');
		}

		//Set session defaults
		$this->setSessionDefaults();

		//Delete the cookie if we use them
		if ($cfg['SITE_ALLOW_COOKIE_LOGIN']) {
		 	setcookie($this->cookie_name);
		}

		//Not authorized
		$this->authorized = false;
	}

	// *** Login succeeded function
	function loginSucceeded($myo, $remember_me) {

		global $cfg, $sha1_appendix;

		//Determine the name
		if (isset($myo->first_name) and $myo->first_name and isset($myo->last_name) and $myo->last_name) {
			$name	=	$myo->first_name.' '.$myo->last_name;
		}
		elseif (isset($myo->company_name) and $myo->company_name) {
			$name	=	$myo->company_name;
		}
		else {
			$name	=	$myo->username;
		}

		//Set login variables
		$_SESSION['login_id']			=	$this->login_id				=	$myo->id;
		$_SESSION['login_name']			=	$this->login_name			=	$name;
		$_SESSION['last_active']		=	$this->last_active			=	time();
		$_SESSION['login_validation']	=	$this->login_validation		=	sha1($myo->password.$sha1_appendix);

		//Generate a fresh session id, delete old session (php 5)
		session_regenerate_id(true);

		//Set a cookie, if allowed and if chosen
		if ($remember_me and $cfg['SITE_ALLOW_COOKIE_LOGIN']) {
			$this->setLoginCookie($myo->email);
		}

		//Otherwise delete the cookie
		else {
		 	setcookie($this->cookie_name);
		}

		//Save login session to DB
		$this->saveLoginToDb();

		//Authorized
		$this->authorized 	= 	true;

		//Group ID
		$this->group_id		=	$myo->group_id;

		//Redirect if needed
		if ($this->after_login_page) {
			header('Location: '.$this->after_login_page);
			exit;
		}
	}

	// *** Function to set a cookie for automated login
	function setLoginCookie($email) {

		global $cfg, $sha1_hash, $sha1_appendix, $_ip;

		//Make a new unique cookie identification sha1 code
	 	$this->cookie_id = sha1(time().$email.$sha1_appendix);

	 	//Create the cookie
		$actual_cookie = serializeHashed(array($email, $this->cookie_id, sha1($_ip)), $sha1_hash);

		//Set the cookie
		$_SESSION['login_cookie']	=	$this->cookie_id;
		setcookie($this->cookie_name, $actual_cookie, time() + (86400*$cfg['SITE_COOKIE_EXPIRE_TIME']));
	}

	// *** Save a login state to the database
	function saveLoginToDb() {

		upRec($this->db_table, $this->login_id, array('session_id','cookie_id','login_attempts','last_login'),
												array(session_id(),$this->cookie_id,0,'[NOW()]'));
	}

	// *** Authentication function
	function authenticate() {

		//Global variables
		global $cfg, $sha1_appendix;

		//Login not allowed?
		if (!$cfg['ALLOW_LOGIN_MAINSWITCH']) {
			$this->logout();
			$this->authorized 	= false;
			return false;
		}

		//Set the fields to fetch from the database
		$fields = implode(',', $this->info_fields_db);

		//Query the database, matching login id and session id
		$res = eq("	SELECT $fields FROM $this->db_table
					WHERE id = '$this->login_id' AND session_id = '".session_id()."' AND email_validated = '1' AND active = '1';");
		$myo = mfo($res);

		//No results?
		if (!mnr($res)) {
			$this->logout();
			$this->authorized 	= false;
		}

		//Password validation string
		else if (sha1($myo->password.$sha1_appendix) != $this->login_validation) {
			$this->logout();
			$this->authorized 	= false;
		}

		//Timeout and no cookie?
		else if ((time() - $this->last_active) > $this->max_idle_time and !$this->cookie_id) {
			$this->logout();
			$time_notification	= $this->max_idle_time/60;
			$this->login_msg 	= str_replace('[TIME]', $time_notification, IDLE_LOGOUT);
			$this->authorized 	= false;
		}

		//Cookie but corrupt?
		else if ($this->cookie_id and $myo->cookie_id != $this->cookie_id) {
			$this->logout();
			$this->authorized 	= false;
		}

		//Group inactive?
		else if ($this->db_table_groups and !getMyo($this->db_table_groups, $myo->group_id, 'active')) {
			$this->logout();
			$this->authorized 	= false;
		}

		//Authentication succeeded
		else {

			//Re-set last active time and set group id
			$this->group_id		= $myo->group_id;
			$this->last_active 	= $_SESSION['last_active'] = time();
			$this->authorized 	= true;
		}

		return $this->authorized;
	}

	// *** Make login HTML
	function makeLoginBoxHTML() {

		global $cfg, $_file, $_sep;

		//Lost pass link
		$file_lost_pass = 	'lost-password.php';
		$link_lost_pass	=	$file_lost_pass.'?return_to='.$_file;

		//Error and remember me html
		$error 			= 	($msg = $this->getLoginMsg()) ? 	"<p class='error_message'>$msg</p>" : '';
		$remember		=	$cfg['SITE_ALLOW_COOKIE_LOGIN'] ? 	"<input type='checkbox' name='remember_me' value='1' />&nbsp;"
																.LOGIN_FIELD_REMEMBER : '';

		//Get template contents
		$html			=	file_get_contents('template'.$_sep.'login.html');

		//Make replacements
		$html	=	str_replace('[LOGIN_TITLE]', 		LOGIN_TITLE,		$html);
		$html	=	str_replace('[LOGIN_TEXT]', 		LOGIN_TEXT,			$html);
		$html	=	str_replace('[ERROR]', 				$error,				$html);
		$html	=	str_replace('[FILE]', 				$_file,				$html);
		$html	=	str_replace('[LOGIN_FIELD_USER]', 	LOGIN_FIELD_USER,	$html);
		$html	=	str_replace('[LOGIN_FIELD_PASS]', 	LOGIN_FIELD_PASS,	$html);
		$html	=	str_replace('[REMEMBER_ME]', 		$remember,			$html);
		$html	=	str_replace('[LOST_PASS_URL]', 		$link_lost_pass,	$html);
		$html	=	str_replace('[LOST_PASS_LINK]', 	LOST_PASS_LINK,		$html);
		$html	=	str_replace('[BUTTON_LOGIN]',		BUTTON_LOGIN,		$html);
		$html	=	str_replace('[LF_USER]', 			$this->username_form_field,	$html);
		$html	=	str_replace('[LF_PASS]', 			$this->password_form_field,	$html);

		//Return HTML
		return $html;
	}

	// *** Get group id
	function getGroupId() {
		return $this->group_id;
	}
}

// *** Authentication function
function auth($needs) {

	global $s_id, $g_id, $login_box, $logout_link, $is_authorized, $login_table, $page_allowed_groups, $extra_db_fields;

	if (!isset($login_table) or empty($login_table)) {
		$login_table = 'extranet_users';
	}

	$Login = new Login_Site($login_table, $extra_db_fields);
	$Login->authenticate();

	if (!$Login->isAuthorized()) {
		$s_id			=	'';
		$g_id			=	'';
		$is_authorized	=	false;

		if ($needs) {
		    $login_box 		= 	$Login->makeLoginBoxHTML();
		    $_POST			=	array();
		    $_GET			=	array();
		    $_REQUEST		=	array();
		}
	    return false;
	}
	else {
		$s_id 			= 	$Login->getLoginID();
		$g_id			=	$Login->getGroupId();

		//This page is not restricted, or this page is restricted to certain groups, and the user belongs to these groups
		if (!count($page_allowed_groups) or in_array($g_id, $page_allowed_groups)) {
			$is_authorized	=	true;
			$login_box		=	'';
		}
		else {
			$is_authorized	=	false;
			$login_box		=	"<h1>".NOT_AUTHORIZED_TITLE."</h1><p>".NOT_AUTHORIZED_TEXT."</p>";
		}
		return true;
	}
}
?>