<?php
/*
Title:		Shopping cart class for simple products
File: 		classes/class_ShoppingcartSimple.php
Version: 	v2.20
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

class Shoppingcart {

    private $Items 		= 	array();
    private $strings 	=	array();

    private $has_promo, $promo_code, $promo_code_id, $promo_amount, $promo_percentage, $promo_one_time;
    private $db_delivery_costs, $db_countries, $db_orders, $db_promo;

    public function __construct() {

		$this->has_promo			=	false;
		$this->promo_code			=	'';
		$this->promo_code_id		=	0;
		$this->promo_amount			=	0;
		$this->promo_percentage		=	0;
		$this->promo_one_time		=	0;

		$this->db_delivery_costs	=	DIGI_DB_PREFIX.'delivery_costs';
		$this->db_countries			=	DIGI_DB_PREFIX.'countries';
		$this->db_orders			=	DIGI_DB_PREFIX.'orders';
		$this->db_promo				=	DIGI_DB_PREFIX.'discount_codes';
    }

    function refresh() {
		foreach($this->Items as $key => $Item) {
			if (is_object($Item->Product)) {
				$Item->Product->readFromDB();
			}
			else {
				unset($this->Items[$key]);
			}
		}
	}

    function addItem($id, $amount, $string) {

        if (!in_array($string, $this->strings)) {

        	$this->strings[] = $string;

			if (isset($this->Items[$id])) {
				$this->Items[$id]->addAmount($amount);
			}
	        else {
				$this->Items[$id] = new Item($id,$amount);
			}
		}
    }

    function removeItem($id, $amount, $string) {

        if (!in_array($string, $this->strings)) {

        	$this->strings[] = $string;

			if (isset($this->Items[$id])) {
	            $this->Items[$id]->removeAmount($amount);
	            if ($this->Items[$id]->getAmount() < 1) {
					unset($this->Items[$id]);
				}
	        }
	    }
    }

    function deleteItem($id, $string) {

	    if (!in_array($string, $this->strings)) {

        	$this->strings[] = $string;

			if (isset($this->Items[$id])) {
				unset($this->Items[$id]);
			}
		}
    }

    function emptyCart($string) {

	    if (!in_array($string, $this->strings)) {

        	$this->strings[] 	= $string;
			$this->Items 		= array();
		}
    }

    function getItems() {
        return $this->Items;
    }

    function getItemTotalPrice($id) {
        return $this->Items[$id]->getTotalPrice();
    }

    function getAmountOfItems() {
		return count($this->Items);
	}

    function getTotalAmount() {
    	$totalamount = 0;
        foreach($this->Items as $Item) {
            $totalamount += $Item->getAmount();
        }
        return $totalamount;
    }

    function getSubTotal() {
    	$subtotal = 0;
        foreach($this->Items as $Item) {
            $subtotal += $Item->getAmount() * $Item->getProductPrice();
        }
        return $subtotal;
    }

    function getNonSaleSubTotal() {
     	$subtotal = 0;
        foreach($this->Items as $Item) {
        	if (!$Item->onSale()) {
        		  $subtotal += $Item->getAmount() * $Item->getProductPrice();
        	}
        }
        return $subtotal;
    }

    function getSubTotalWithDiscount() {
		return $this->getSubTotal()-$this->getDiscount();
	}

	function getDiscount() {

		global $cfg;

		if (!$this->hasPromoCode()) {
			return 0;
		}
		if ($percentage = $this->getPromoPercentage()) {
			$total_price = $cfg['DISCOUNT_ONLY_ON_NONSALE'] ? $this->getNonSaleSubTotal() : $this->getSubTotal();
			return round(($percentage/100)*$total_price);
		}
		elseif ($amount = $this->getPromoFixedAmount()) {
			$total_price = $cfg['DISCOUNT_ONLY_ON_NONSALE'] ? $this->getNonSaleSubTotal() : $this->getSubTotal();
			return $amount > $total_price ? 0 : $amount;
		}
		else {
			return 0;
		}
	}

    function getTotalVat($country) {

		global $cfg;

    	//Get the products VAT first
    	$totalvat = 0;
        foreach($this->Items as $Item) {
            $totalvat += $Item->getItemVat();
        }

		//Add delivery costs VAT and remove discount VAT
        $totalvat += ($this->getDeliveryCosts($country)/($cfg['PRODUCTS_STANDARD_VAT']+100))*$cfg['PRODUCTS_STANDARD_VAT'];
		$totalvat -= ($this->getDiscount()/($cfg['PRODUCTS_STANDARD_VAT']+100))*$cfg['PRODUCTS_STANDARD_VAT'];

        return round($totalvat);
	}

    function getTotalDeliveryPoints() {
    	$totalpoints = 0;
		foreach($this->Items as $Item) {
			$totalpoints += $Item->getDeliveryPoints();
		}
		return $totalpoints;
	}

    function getDeliveryCosts($country) {

		//Calculate the delivery points total
		$delivery_points = $this->getTotalDeliveryPoints();

		//Check if we have data for this specific country
		if (getMyo($this->db_delivery_costs, "country = '$country'",'id')) {
			$where	= "country = '$country'";
		}
		//If not, obtain region first
		else {
			$region = getMyo($this->db_countries,$country,'region');
			$where	= "region = '$region'";
		}

		//Get price and repeat points
		$delivery = getMyo(	$this->db_delivery_costs, "points <= '$delivery_points' AND $where",
							array('points','price','repeat_points'),'points','DESC',1);

		//No result?
		if (!$delivery) {
			return 0;
		}

		//Do these delivery costs repeat?
		if ($delivery->repeat_points) {
			$times_to_repeat 	= 1 + floor(($delivery_points - $delivery->points) / $delivery->repeat_points);
			$delivery_costs 	= $delivery->price * $times_to_repeat;
		}

		//If not, delivery costs are simply the price
		else {
			$delivery_costs 	= $delivery->price;
		}

		//Return the delivery costs
		return $delivery_costs;
	}

	function setPromoCode($code) {

		evalAll($code);
		$code = strtoupper($code);

		$res = eq("	SELECT id, active, percentage, one_time_use, fixed_amount, UNIX_TIMESTAMP(valid_till) AS valid_till
					FROM $this->db_promo WHERE code = '$code' AND active = '1';");
		if (!mnr($res)) {
			return PROMOCODE_INVALID;
		}

		$myo = mfo($res);
		if (!$myo->active or ($myo->valid_till - time() < 0)) {
			return PROMOCODE_EXPIRED;
		}

		$this->has_promo		=	true;
		$this->promo_code		=	$code;
		$this->promo_code_id	=	$myo->id;
		$this->promo_percentage	=	$myo->percentage;
		$this->promo_amount		=	$myo->fixed_amount;
		$this->promo_one_time	=	$myo->one_time_use;
		return true;
	}

	function validatePromoCode($customer_id) {

		if ($this->has_promo and $this->promo_code_id and $this->promo_one_time and
			countResults($this->db_orders, "customer_id = '$customer_id' AND discount_code = '$this->promo_code'")) {

			$this->clearPromoCode();
			return false;
		}
		return true;
	}

	function clearPromoCode() {
		$this->has_promo		=	false;
		$this->promo_code		=	'';
		$this->promo_percentage	=	0;
		$this->promo_amount		=	0;
	}

	function hasPromoCode() {
		return $this->has_promo;
	}
	function getPromoCode() {
		return $this->promo_code;
	}
	function getPromoCodeId() {
		return $this->promo_code_id;
	}
	function getPromoPercentage() {
		return $this->promo_percentage;
	}
	function getPromoFixedAmount() {
		return $this->promo_amount;
	}
	function getPromoString() {
		if ($this->has_promo) {
			return $this->promo_code.($this->promo_percentage ? ' '.$this->promo_percentage.'%' : '');
		}
		return '';
	}
}

class Item {

    public 	$Product;
	private	$amount;

    function Item($id,$amount) {
        $this->Product 		= 	new Product($id);
        $this->step_size 	= 	$this->Product->getStepsize();
       	$amount = $amount < $this->step_size ? $this->step_size : ($amount - ($amount % $this->step_size));
        $this->amount 		= 	$amount;
    }

    function addAmount($amount) {
    	$amount = $amount < $this->step_size ? $this->step_size : ($amount - ($amount % $this->step_size));
        $this->amount 	+= 	$amount;
    }

    function removeAmount($amount) {
    	$amount = $amount < $this->step_size ? $this->step_size : ($amount - ($amount % $this->step_size));
        $this->amount 	-= 	$amount;
        $this->amount	=	$this->amount < 0 ? 0 : $this->amount;
    }

    function getAmount() {
        return $this->amount;
    }

    function getProductId() {
        return $this->Product->getId();
    }

    function getProductName() {
        return $this->Product->getName();
    }

    function getProductPrice($force_vat_incl = false) {
        return $this->Product->getPrice($force_vat_incl);
    }

	function getProductOnSale() {
		return $this->Product->onSale();
	}

	function getItemVat() {
		return $this->amount * $this->getProductVat();
	}

	function getProductVat() {
		if ($this->Product->vatIncluded()) {
			$vat_percentage	=	$this->Product->getVatPercentage();
			$vat_on_product	=	$vat_percentage * $this->Product->getPrice() / (100 + $vat_percentage);
			return round($vat_on_product);
		}
		else {
			return 0;
		}
	}

	function getProductVatPercentage() {
		return $this->Product->getVatPercentage();
	}

    function getTotalPrice() {
        return $this->amount * $this->Product->getPrice();
    }

	function getDeliveryPoints() {
		return $this->amount * $this->Product->getDeliveryPoints();
	}

	function getProductThumbUrl($gfx_folder) {
		return $this->Product->getThumbUrl($gfx_folder);
	}

	function getProductProp($prop) {
		return $this->Product->getProp($prop);
	}

	function onSale() {
		return $this->Product->onSale();
	}
}

class Product {

    private $id;

    function Product($id) {

        $this->id 					= 	$id;
        $this->db_products			= 	DIGI_DB_PREFIX.'products_simple';
        $this->db_product_props		=	DIGI_DB_PREFIX.'product_data';
        $this->readFromDB();
    }

    function readFromDB() {

    	global $_lang, $cfg;
    	$this->db_products			= 	DIGI_DB_PREFIX.'products_simple';

		//Read main product data
        $res 		= eq("SELECT * FROM $this->db_products WHERE id = '$this->id';");
        $product 	= mfo($res);

		//Set main product properties
		foreach (get_object_vars($product) as $key => $value) {
			$this->$key	= $value;
		}

		//Read name and description, language dependent
		$props = getMyo($this->db_product_props, "lang = '$_lang' AND product_id = '$this->id'", array('name','description'));

		//Set name and description (language dependant)
		$this->name				=	$props->name;
		$this->description		=	$props->description;

        //Price
        if ($product->on_sale) {
			$this->on_sale		=	true;
			$this->price		=	$product->sale_price;
		}
		else {
			$this->on_sale		=	false;
			$this->price		=	$product->price;
		}
    }

    function getId() {
		return $this->id;
	}

    function getName() {
        return $this->name;
    }

	function getDescription() {
		return $this->description;
	}

    function getProductNo() {
        return $this->product_no;
    }

    function getProductType() {
		return $this->product_type;
	}

    function getPrice($force_vat_incl = false) {

		if ($force_vat_incl) {

			//Vat is included
			if ($this->vatIncluded()) {
				return $this->price;
			}

			//Not yet included
			else {
				return round($this->price * (100+$this->vat_percentage)/100);
			}
		}
		else {
			return $this->price;
		}
    }

    function getVatPercentage() {
        return $this->vat_percentage;
    }

	function getStepsize() {
		return $this->step_size;
	}

	function getDeliveryPoints() {
		return $this->delivery_points;
	}

	function getProp($prop) {
		if (isset($this->$prop)) {
			return $this->$prop;
		}
		else return '';
	}

	function getThumbUrl($gfx_folder) {
		global $_sep;
		return $gfx_folder.$_sep.'thumb_'.$this->id.'.jpg';
	}

	function onSale() {
		return $this->on_sale;
	}

	function vatIncluded() {
		return $this->vat_included;
	}
}
?>