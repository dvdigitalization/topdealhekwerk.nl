<?php
/*
Title:		DB Item
File: 		classes/class_DBItem.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

class DBItem {
	
	private $db_table, $id;
	
	// *** Instantiate a new DbItem
	function DBItem($id = 0) {
		
		global $db_table, $name_field, $extra_props;
		
		$this->id 			= 	(int) $id;
		$this->db_table 	= 	$db_table;
		$this->props		=	array_keys($extra_props) + array(99 => $name_field);
		
		// Build this item
		if ($this->id) {
			$this->readFromDb();
		}	  			
	}
	
	// *** Read values from Db
	function readFromDb() {

		$res = eq("SELECT * FROM $this->db_table WHERE id='$this->id';");
		$mya = mfa($res);
        
        foreach ($this->props as $key) {
        	$this->vars[$key] = $mya[$key];
		}
	}
	
	// *** Read post vars, if there are any
	function readPostVars() {
		foreach ($this->props as $key) {
			$this->vars[$key] = $_POST[$key];
		}
	}
	
	// *** Get the name of this item
	function getVar($var) {
		return $this->vars[$var];	
	}
	
	// *** Save this item
	function save() {
		
		global $extra_prop_functions;
		
		foreach ($this->vars as $key => $value) {
			
			if (isset($extra_prop_functions[$key])) {
				$value = $extra_prop_functions[$key][1]($value);
			}	
			
			$fields[]	=	$key;
			$values[]	=	$value;
		}
		
		if ($this->id) {
			upRec($this->db_table,$this->id,$fields,$values);
		}
		else {
		
			//Always maintain sortability, just in case we want to change the table later
			$fields[]	=	'item_order';
			$values[]	=	getMyo($this->db_table, 'id > 0', 'item_order', 'item_order', 'DESC', 1) + 1;
		
			$this->id =	insRec($this->db_table,$fields,$values);	
		}
		return $this->id;
	}
	
	// *** Delete this item
	function delete($permanently = false, $clear_dependencies = false) {
		
		global $dependent_fields, $dependent_tables;
		
		if ($permanently) {
			
			foreach ($dependent_tables as $key => $table) {
				$field = $dependent_fields[$key];

				//Dolf check
				if (substr($table,0, strlen(DIGI_DB_PREFIX)) != DIGI_DB_PREFIX) {
					$table = DIGI_DB_PREFIX.$table;
				}
				
				if ($clear_dependencies) {
					upRec($table,"$field = '$this->id'",$field,'');
				}
				else {
					$res = eq("SELECT id FROM $table WHERE $field = '$this->id';");
					if (mnr($res)) {
						return false;	
					}
				}
			}
			
			$item_order = getMyo($this->db_table, $this->id, 'item_order');
			delRec($this->db_table, $this->id);
			fixSortOrder($this->db_table, 'item_order', $item_order);
			return true;
		}
		else {
			upRec($this->db_table, $this->id, 'deleted', 1);
			return true;	
		}
	}
	
	// *** Move item up
	function move_up() {
	
		$old_order = getMyo($this->db_table, $this->id, 'item_order');
		$new_order = $old_order - 1;
		$change_id = getMyo($this->db_table, "item_order = '$new_order'", 'id');
	
		if (!empty($change_id)) {		
			upRec($this->db_table, $this->id, 'item_order', $new_order);
			upRec($this->db_table, $change_id,'item_order', $old_order);
		}
	}
	
	// *** Move item down
	function move_down() {

		$old_order = getMyo($this->db_table, $this->id, 'item_order');
		$new_order = $old_order + 1;
		$change_id = getMyo($this->db_table, "item_order = '$new_order'", 'id');
		
		if (!empty($change_id)) {		
			upRec($this->db_table, $this->id, 'item_order', $new_order);
			upRec($this->db_table, $change_id,'item_order', $old_order);
		}
	}
	
	// *** Activate an item
	function activate() {
		upRec($this->db_table,$this->id,'active',1);
	}
	
	// *** De-activate an item
	function de_activate() {
		upRec($this->db_table,$this->id,'active',0);
	}
}    
?>