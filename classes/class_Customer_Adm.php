<?php
/*
Title:		Administrative customer class
File: 		classes/class_Customer_Adm.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

require_once(CMS_PATH_PREFIX.'classes/class_Customer.php');

class Customer_Adm extends Customer {

    protected $id;

    function Customer_Adm($id) {
    	
        $this->id 		= 	$id;
        $this->db 		= 	DIGI_DB_PREFIX.'customers_adm';
        
        $this->readFromDB();
    }
    
	static function getNiceCustomerId($id) {
		global $cfg;
		return sprintf($cfg['CUSTOMER_PREFIX'].'%0'.($cfg['CUSTOMER_L_ZEROES']).'d', ($id + $cfg['CUSTOMER_CONST']));	
	}                        
    
    function getContactName($with_title = false) {
    	global $SEX_ARRAY;
		$name = $this->last_name.
				(!empty($this->first_name) ? ', '.$this->first_name : '').
				(!empty($this->prefix) ? ' '.$this->prefix : '');
		return $with_title ? $SEX_ARRAY[$this->title].$name : $name;		
	}

	function getMobile() {
		return $this->mobile;
	}
	
	function getFax() {
		return $this->fax;
	}	
	
	function getEmail2() {
		return $this->email_2;
	}

	function getEmailLink2() {
		if (empty($this->email_2)) return null;
		global $mailer;
		return "<a href='$mailer$this->email_2' title='Stuur deze klant een email'>$this->email_2</a>";
	}

	function getIM() {
		return $this->instant_messenger;
	}
	
	function getWebsite($full = false) {
		require_once(CMS_PATH_PREFIX.'inc/functions/split_website.php');
		if (empty($this->website)) return null;
		if (!$full) return $this->website;
		list ($url, $name) = split_website($this->website);
		return "<a href='$url' target='_blank' title='Open website in nieuw venster'>$name</a>";
	}
	
	function getBTWnr() {
		return $this->btw_nr;
	}

	function getKVKnr() {
		return $this->kvk_nr;
	}
	
	function getBankName() {
		return $this->bank_name;
	}
	
	function getBankNr() {
		return $this->bank_nr;
	}
	
	function getBICnr() {
		return $this->bic_nr;
	}
	
	function getIBANnr() {
		return $this->iban_nr;
	}
	
	function getRate1($full = true) {
		global $PROJECT_RATES;
		return $full ? (isset($PROJECT_RATES[$this->rate_1]) ? $PROJECT_RATES[$this->rate_1] : '') : $this->rate_1;
	}		
	
	function getRate2($full = true) {
		global $PROJECT_RATES;
		return $full ? $PROJECT_RATES[$this->rate_2] : $this->rate_2;
	}
	
	function getRatesValidFrom($formatted = true) {
		return $formatted ? time2date($this->rates_valid_from) : $this->rates_valid_from;
	}

	function getRatesValidUntil($formatted = true) {
		return $formatted ? time2date($this->rates_valid_until) : $this->rates_valid_until;
	}
	
	function getAssignedEmployee($full = true) {
		if (empty($this->assign_to_employee)) return $full ? 'geen' : '';
		global $EMPLOYEES_ALL_ARRAY;
		return $full ? $EMPLOYEES_ALL_ARRAY[$this->assign_to_employee] : $this->assign_to_employee;
	}
	
	function isCompany() {
		return !empty($this->company_name);
	}
	
	function hasFinancialInfo() {
		if (!empty($this->btw_nr) or !empty($this->kvk_nr) or !empty($this->bank_name) or !empty($this->bank_nr) or 
			!empty($this->bic_nr) or !empty($this->iban_nr)) {
			return true;
		}
		return false;
	}
	
	function hasContactInfo() {
		if (!empty($this->phone) or !empty($this->mobile) or !empty($this->fax) or !empty($this->website) or 
			!empty($this->email) or !empty($this->email_2) or !empty($this->instant_messenger)) {
			return true;
		}
		return false;		
	}
}
?>