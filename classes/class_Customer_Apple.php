<?php
/*
Title:		Shop customer class
File: 		classes/class_Customer_Shop.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

require_once(CMS_PATH_PREFIX.'classes/class_Customer.php');

class Customer_Shop extends Customer {

    protected $id;

    private $props = array(	'timestamp','title','initials','first_name','prefix','last_name','company_name','display_name',
						'address','nr','postal_code','city','country','phone','email','newsletter','language','remarks','password');

    function Customer_Shop($id) {

        $this->id 		= 	(int) $id;
        $this->db 		= 	DIGI_DB_PREFIX.'topdeal_customers';

        if ($this->id) {
	        $this->readFromDB();
	    }
    }

    function readPostVars() {
		foreach ($props as $prop) {
			evalAll($_POST[$prop]);
			$this->$prop = $_POST[$prop];
		}

	}

    function saveToDB() {

	}

	function getLanguage() {
		return $this->language;
	}

	function hasNewsletter() {
		return $this->newsletter ? true : false;
	}
}
?>