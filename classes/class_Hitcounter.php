<?php
/*
Title:		Hitcounter class
File: 		classes/class_Hitcounter.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

class Hitcounter {
	
	private $ip, $user_agent, $user_os, $user_browser, $user_browser_version;
	private $year, $month, $day;
	
	function Hitcounter($db_tables = '') {
		
		global $cfg, $_ip;
		
		if (!is_array($db_tables)) {
			$db_tables = array(	DIGI_DB_PREFIX.'site_recent_hits',
								DIGI_DB_PREFIX.'site_statistics_daily',
								DIGI_DB_PREFIX.'site_statistics_os',
								DIGI_DB_PREFIX.'site_statistics_browser',
								DIGI_DB_PREFIX.'site_statistics_ref'
								);
		}
		
		list (	$this->db_table_recent,
				$this->db_table_daily,
				$this->db_table_os,
				$this->db_table_browser,
				$this->db_table_ref
			) = $db_tables;
		
		//Set user vars
		$this->ip					=	$_ip;
		$this->user_agent			=	isset($_SERVER['HTTP_USER_AGENT']) 	? $_SERVER['HTTP_USER_AGENT'] : '';
		$this->referrer				=	isset($_SERVER['HTTP_REFERER']) 	? parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) : '';
		$this->hit_interval			=	$cfg['STATS_UNIQUE_INTERVAL'] * 3600;
		
		//Set time vars
		$this->year		=	date('Y');
		$this->month	=	date('n');
		$this->day		=	date('j');
						
		//If the ip is not in the database, or if long time passed, treat as a new unique visitor
		if ($this->checkIpRecent()) {
			$this->readBrowserInfo();
			$this->uniqueVisitor();
		}
		
		//Do a regular hit
		$this->hit();
		
		//Clear old entries
		$clear_time = mysql_time(time() - $this->hit_interval);
		delRec($this->db_table_recent,"last_activity < '$clear_time'");
	}
	
	function uniqueVisitor() {

		// *** OS stats ***
		$res = eq("	SELECT id,hits
					FROM $this->db_table_os
					WHERE os = '$this->user_os'
				;");
		
		if (mysql_num_rows($res)) {
			$myo = mfo($res);
			upRec($this->db_table_os, $myo->id, 'hits', $myo->hits + 1);
		}
		else {
			insRec($this->db_table_os, array('os','hits'), array($this->user_os, 1));
		}
		
		
		// *** Browser stats ***
		$res = eq("	SELECT id,hits
					FROM $this->db_table_browser
					WHERE browser = '$this->user_browser'
					AND version = '$this->user_browser_version'
				;");
		
		if (mysql_num_rows($res)) {
			$myo = mfo($res);
			upRec($this->db_table_browser, $myo->id, 'hits', $myo->hits + 1);
		}
		else {
			insRec($this->db_table_browser, array('browser','version','hits'), array($this->user_browser, $this->user_browser_version, 1));
		}
		
		// *** Referrer stats
		$res = eq("	SELECT id,hits
					FROM $this->db_table_ref
					WHERE referrer = '$this->referrer'
				;");
		
		if (mysql_num_rows($res)) {
			$myo = mfo($res);
			upRec($this->db_table_ref, $myo->id, 'hits', $myo->hits + 1);
		}
		else {
			insRec($this->db_table_ref, array('referrer','hits'), array($this->referrer, 1));
		}		
		
		// *** Daily stats ***
		$res = eq("	SELECT id,unique_visitors
					FROM $this->db_table_daily
					WHERE year = '$this->year'
					AND month = '$this->month'
					AND day = '$this->day'
				;");
		
		if (mysql_num_rows($res)) {
			$myo = mfo($res);
			upRec($this->db_table_daily, $myo->id, 'unique_visitors', $myo->unique_visitors + 1);
		}
		else {
			insRec($this->db_table_daily, array('year','month','day','unique_visitors'), array($this->year, $this->month, $this->day, 1));
		}
		
	}
	
	function hit() {
		
		// *** Daily stats ***
		$res = eq("	SELECT id,page_hits
					FROM $this->db_table_daily
					WHERE year = '$this->year'
					AND month = '$this->month'
					AND day = '$this->day'
				;");
		
		if (mysql_num_rows($res)) {
			$myo = mfo($res);
			upRec($this->db_table_daily, $myo->id, 'page_hits', $myo->page_hits + 1);
		}
		else {
			insRec($this->db_table_daily, array('year','month','day','page_hits'), array($this->year, $this->month, $this->day, 1));
		}		
	}
	
	function checkIpRecent() {

		//Get the last visited timestamp for this IP
		$res = eq("SELECT UNIX_TIMESTAMP(last_activity) AS last_activity FROM $this->db_table_recent WHERE ip = '$this->ip';");
		
		//If we have an result...
		if (mysql_num_rows($res)) {
			$myo = mfo($res);
			
			//...check if the time of the visit was more than $this->hit_interval ago
			if ( (time() - $myo->last_activity) > $this->hit_interval ) {
				
				//If so, update the time of last visited and return true
				$this->updateRecentHit($this->ip);
				return true;
			}
			
			//If not, update the time of last visited and return false
			else {
				$this->updateRecentHit($this->ip);
				return false;
			}
		}
		
		//If we don't have a result for this IP, insert into last visited and return true
		else {
			$this->insertRecentHit($this->ip);
			return true;
		}		
	}

	function insertRecentHit($ip) {
		insRec($this->db_table_recent, array('ip','last_activity'), array($ip,mysql_time()));
	}
	
	function updateRecentHit($ip) {
		upRec($this->db_table_recent, "ip = '$ip'", 'last_activity', mysql_time());
	}
	
	function readBrowserInfo() {
		
		//Platform
        if (strstr($this->user_agent, 'Win')) {
            $this->user_os = 'Windows';
        }
		elseif (strstr($this->user_agent, 'Mac')) {
            $this->user_os = 'Mac';
        }
		elseif (strstr($this->user_agent, 'Linux')) {
            $this->user_os = 'Linux';
        }
		elseif (strstr($this->user_agent, 'Unix')) {
            $this->user_os = 'Unix';
        }
		elseif (strstr($this->user_agent, 'OS/2')) {
            $this->user_os = 'OS/2';
        }
		else {
            $this->user_os = 'Other';
        }

        //Browser and version (must check everything else before Mozilla)
        if (preg_match('@Opera(/| )([0-9].[0-9]{1,2})@', $this->user_agent, $log_version)) {
            $this->user_browser_version = $log_version[2];
            $this->user_browser =  'Opera';
        }
		elseif (preg_match('@MSIE ([0-9].[0-9]{1,2})@', $this->user_agent, $log_version)) {
            $this->user_browser_version = $log_version[1];
            $this->user_browser =  'IE';
        }
		elseif (preg_match('@OmniWeb/([0-9].[0-9]{1,2})@', $this->user_agent, $log_version)) {
            $this->user_browser_version = $log_version[1];
            $this->user_browser =  'Omniweb';
        }
		elseif (preg_match('@(Konqueror/)(.*)(;)@', $this->user_agent, $log_version)) {
            $this->user_browser_version = substr($log_version[2],0,3);
            $this->user_browser =  'Konqueror';
        }
		elseif (preg_match('@Mozilla/([0-9].[0-9]{1,2})@', $this->user_agent, $log_version)
                   && preg_match('@Safari/([0-9]*)@', $this->user_agent, $log_version2)) {
            $this->user_browser_version = $log_version[1];
            $this->user_browser =  'Safari';
        }
		elseif (preg_match('@Mozilla/([0-9].[0-9]{1,2})@', $this->user_agent, $log_version)
                   && preg_match('@Firefox/([0-9].[0-9]*)@', $this->user_agent, $log_version2)) {
            $this->user_browser_version = $log_version2[1];
            $this->user_browser =  'Firefox';
        }        
		elseif (preg_match('@Mozilla/([0-9].[0-9]{1,2})@', $this->user_agent, $log_version)
                   && preg_match('@Netscape/([0-9].[0-9]*)@', $this->user_agent, $log_version2)) {
            $this->user_browser_version = $log_version2[1];
            $this->user_browser =  'Netscape';
        }   
		elseif (preg_match('@Mozilla/([0-9].[0-9]{1,2})@', $this->user_agent, $log_version)) {
            $this->user_browser_version = $log_version[1];
            $this->user_browser =  'Mozilla';
        }
		else {
            $this->user_browser_version = 0;
            $this->user_browser =  'Other';
        }		
	}
}
?>