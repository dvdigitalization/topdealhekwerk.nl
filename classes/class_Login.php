<?php
/*
Title:		Login base class
File: 		classes/class_Login.php
Version: 	v2.00
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization

Comments:	This login class is the base for the CMS and Site login classes
			
*/

class Login {
	
	protected $max_idle_time;		//The maximum idle time after which a session expires
	protected $max_attempts;		//The maximum number of failed login attempts
	protected $db_table;			//The database table to authenticate against
	protected $login_msg;			//Holds the latest login message
	protected $login_msg_type;		//Holds the message type
	
	protected $username_form_field, $password_form_field, $username_db_field, $password_db_field;
	
	protected $last_active;			//The timestamp when the user was last active
	protected $login_id;			//Users login ID (corresponds to database ID)
	protected $login_name;			//Users name (not username)
	protected $login_validation;	//An extra validation check, based on the users password hash (but not identical)
	protected $cookie_id;
		
	protected $info_fields_db;		//Holds some standard info fields we want to obtain from database after login
	
	
	// *** Constructor of the base login class
	function Login() {
		
		//Check if we have valid login session variables
		if (!isset($_SESSION['login_id']) or !isset($_SESSION['last_active']) or !isset($_SESSION['login_validation']) or !isset($_SESSION['login_cookie']) or !isset($_SESSION['login_name'])) {
			
			//If not, set the default values
			$this->setSessionDefaults();
		}
		
		//Set the session variables to the session values
		$this->login_id				=	(int) $_SESSION['login_id'];
		$this->last_active			=	(int) $_SESSION['last_active'];
		$this->login_name			=	$_SESSION['login_name'];
		$this->login_validation		=	$_SESSION['login_validation'];
		$this->cookie_id			=	$_SESSION['login_cookie'];
		
		//Default value
		$this->authorized			=	false;
	}
	
	// *** Set default values for our session
	function setSessionDefaults() {
		
		$this->login_id					=	0;
		$this->last_active				=	0;
		$this->login_validation			=	'';
		$this->cookie_id				=	'';
		$this->login_user				=	'';
		$this->login_name				=	'';
		
		$_SESSION['login_id']			=	0;
		$_SESSION['last_active']		=	0;
		$_SESSION['login_name']			=	'';
		$_SESSION['login_user']			=	'';
		$_SESSION['login_validation']	=	'';
		$_SESSION['login_cookie']		=	'';
	}
	
	// *** Get the latest login message
	function getLoginMsg() {
		return $this->login_msg;
	}

	// *** Get the login message type
	function getMsgType() {
		return $this->login_msg_type;
	}
	
	// *** Get the login id
	function getLoginID() {
		return $this->login_id;
	}

	// *** Get the login username
	function getLoginUser() {
		return $this->login_user;
	}

	// *** Simple check to see if a user is authorized
	function isAuthorized() {
		return ($this->authorized === true);
	}
}
?>