<?php
/*
Title:		Quote class
File: 		classes/class_Quote.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

class Quote {
    
    static function addArticleToQuote($quote_id, $article_text, $price) {
	
		global $db_table_quote_art;
	
		if (!isDoublePost($db_table_quote_art, array('quote_id','article_text','price'), array($quote_id, $article_text, $price))) {
			insRec($db_table_quote_art,
				array('quote_id','article_text','price'),
				array($quote_id, $article_text, $price)
			);
		}
	}

	static function deleteArticleFromQuote($article_id) {

		global $db_table_quote_art;
		return delRec($db_table_quote_art,$article_id);
	}
	
	static function getQuoteTotalAmount($quote_id) {
		
		global $db_table_quote_art;
		
		$articles 	= array();
		$total 		= 0;
		
		$res = eq("SELECT price FROM $db_table_quote_art WHERE quote_id = '$quote_id';");
		while ($myo = mfo($res)) {
			$total += $myo->price;
		}
		return $total;
	}
}
?>