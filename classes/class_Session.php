<?php
/*
Title:		Session initalization and management class
File: 		classes/class_Session.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

class Session {

	private $session_name, $session_signature;
	
	function Session() {

		//Initialize a new session
		$this->initializeSession();		
	}
	
	// *** Initialize the session functionality
	function initializeSession() {
		
		//Utilize global variables set in our ajax headers
		global $site_name, $sha1_appendix, $is_cms;
	
		//Make a clean session name	
		$s_name = '';
	    for ($i=0; $i<strlen($site_name); $i++) {
	        if (preg_match('([0-9a-zA-Z])', $site_name[$i])) {
	            $s_name .= $site_name[$i];
	        }    
	    }
		
		//Create a session name based on the site name, as defined in our ajax headers
		$this->session_name			=	$s_name.((isset($is_cms) and $is_cms) ? 'CMS' : '');
		
		//Generate a session signature (fingerprint) based on the clients remote address, user agent and server protocol
		$this->session_signature 	=	$_SERVER['REMOTE_ADDR'].
										(isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '').
										$_SERVER['SERVER_PROTOCOL'];
		
		//Configure the session
		ini_set('session.use_only_cookies', 1);
		session_cache_limiter('no-cache');
		session_name($this->session_name);
		
		//Start the session
		session_start();
		
		//Validate the session signature				
		if (empty($_SESSION['signature'])) {
			
			//If empty, generate a new session id and set the signature
			session_destroy();
			session_start();
			session_regenerate_id(true);
			$_SESSION['signature'] = sha1($this->session_signature.$sha1_appendix);
		}
		else if ($_SESSION['signature'] != sha1($this->session_signature.$sha1_appendix)) {
			
			//If invalid, destroy the session and start a new one
			session_destroy();
			session_start();
		}
	}
}
?>