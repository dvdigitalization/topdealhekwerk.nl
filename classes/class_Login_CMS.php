<?php
/*
Title:		CMS Login management class
File: 		classes/class_Login_CMS.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization

Extends:	classes/class_Login.php
		
*/

class Login_CMS extends Login {
	
	// *** Constructor, takes the database table to use as a parameter
	function Login_CMS($db_table = '') {
		
		global $cfg, $site_name;
		
		//Default db table
		if (empty($db_table)) $db_table = DIGI_DB_PREFIX.'auth_users';
		
		//Set the database table
		$this->db_table 			=	$db_table;
		
		//Set default max idle time in seconds and max brute force login attempts
		$this->max_idle_time		=	1800;
		$this->max_brute_attempts	=	13;
		
		//Set the max number of failed login attempts
		$this->max_attempts			=	4;
		
		//Set some CMS specific variables
		$this->privs				=	'';
		$this->is_admin				=	0;

		//Set the cookie name
		$c_name = '';
	    for ($i=0; $i<strlen($site_name); $i++) {
	        if (preg_match('([0-9a-zA-Z])', $site_name[$i])) {
	            $c_name .= $site_name[$i];
	        }    
	    }
		$this->cookie_name			=	$c_name.'CMSCOOKIE';
		
		//Set the username and password fields (in the login form)
		$this->username_form_field	=	'username';
		$this->password_form_field	=	'password';
		$this->remember_field		=	'remember_me';
		
		//Set the standard fields to fetch
		$this->info_fields_db		=	array(	'id','UNIX_TIMESTAMP(last_login) AS last_login','last_login_ip',
												'uses_mailer','login_attempts','cookie_id','first_name','last_name',
												'allow_login','is_admin','privs','password','username');
		
		//Call the base class login function
		$this->Login();
		
		//Catch login and logout cases
		$this->catchLoginAndLogout();		
	}
	
	// *** Catch all possible login and logout cases
	function catchLoginAndLogout() {
		
		global $cfg;
			
		//Logout by link
		if (!empty($_GET['logout'])) {
			$this->logout();
			cmsLog("Gebruiker is uitgelogd uit het CMS.");
			return null;
		}
		
		//Login by form
		if (!empty($_POST['login'])) {
			$remember_me = isset($_POST[$this->remember_field]) ? true : false;
			$this->loginByForm($_POST[$this->username_form_field], $_POST[$this->password_form_field], $remember_me);
			return null;
		}

		//Login by cookie if allowed and if not already logged in by cookie
		if ($cfg['ALLOW_COOKIE_LOGIN'] and isset($_COOKIE[$this->cookie_name]) and empty($_SESSION['login_cookie'])) {
			$this->loginByCookie();
			return null;
		}
		
		//Login by IP address
		if (!isset($_SESSION['ip_checked']) and !$this->authorized) {
			$this->loginByIP();
			return null;
		}
	}
	
	// *** Login process by IP address
	function loginByIP() {
		
		global $cfg, $_ip;
		
		//Do this only once per session
		$_SESSION['ip_checked']	= true;
		
		//Validate IP
		if (!$user_ip = validateIP($_ip)) {
			return null;
		}
		
		//Set the fields to fetch from the database
		$fields = implode(',', $this->info_fields_db);
		
		//Match against database		
		$res = eq("SELECT $fields FROM $this->db_table WHERE ip_login_1 = '$_ip' OR ip_login_2 = '$_ip' LIMIT 1;");
		$myo = mfo($res);
		
		//No results or inactive?
		if (!mnr($res) or !$myo->allow_login) {
			
			//No logging in by IP today
			return null;			
		}
		
		//Validation succeeded
		else {
			
			//Pass the database result object to the login ok function
			$this->loginSucceeded($myo, true);
		}
	}
	
	// *** Login process by form
	function loginByForm($username, $password, $remember_me) {
		
		global $cfg, $_ip;
		
		//Check for empty data
		if (empty($username) or empty($password)) {
			$this->login_msg 		= 	LOGIN_EMPTY_POST_FIELDS;
			$this->login_msg_type	=	CRITICAL;
			return null;
		}
		
		//Check for brute force limit (masquerade as a failed login attempt)
		if (isset($_SESSION['cms_id']) and $_SESSION['cms_id'] > $this->max_brute_attempts) {
			$this->login_msg 		= str_replace(	'[TECHNICAL]',
													"<a href='mailto:$cfg[TECH_EMAIL]'>$cfg[TECH_NAME]</a>", LOGIN_INVALID_BF_MSG);
			$this->login_msg_type	=	CRITICAL;
			return null;	
		}
		
		//Set the data to validate
		$password		=	dbSalt($password);
		$username		=	validateSimpleString($username);
		
		//Set the fields to fetch from the database
		$fields = implode(',', $this->info_fields_db);
		
		//Match against database
		$res = eq("SELECT $fields FROM $this->db_table WHERE username = '$username';");	
		
		//No results?
		if (!mnr($res)) {
			
			//Brute force login check
			if (isset($_SESSION['cms_id'])) {
				$_SESSION['cms_id']++;
			}
			else {
				$_SESSION['cms_id'] = 1;
			}
			
			$this->login_msg 		= 	LOGIN_INVALID_MSG;
			$this->login_msg_type	=	CRITICAL;
			return null;
		}
		
		//Validate result		
		else {
			
			//Get the result
			$myo = mfo($res);
			
			//Max failed login attempts exceeded?
			if ($myo->login_attempts >= $this->max_attempts) {
				
				$this->login_msg = str_replace(	'[TECHNICAL]',
												"<a href='mailto:$cfg[TECH_EMAIL]'>$cfg[TECH_NAME]</a>",LOGIN_MAX_ATTEMPTS);
				return null;				
			}
			
			//Compare validation string against password hash
			else if ($password != $myo->password) {
				
				//Update the login attempts amount
				upRec($this->db_table, $myo->id, 'login_attempts', $myo->login_attempts + 1);
				
				$attempts_left 	 = $this->max_attempts - $myo->login_attempts - 1;
				cmsLog("Gebruiker gaf verkeerde wachtwoord op vanaf $_ip. Nog $attempts_left pogingen over.", false, $myo->id);
				
				if ($attempts_left) {
					$this->login_msg 		= 	LOGIN_INVALID_MSG.' '.str_replace('[ATTEMPTS]',$attempts_left,LOGIN_ATTEMPTS_LEFT);
					$this->login_msg_type	=	WARNING;
				}
				else {
					$this->login_msg 		= 	LOGIN_INVALID_MSG.' '.str_replace(	'[TECHNICAL]',
												"<a href='mailto:$cfg[TECH_EMAIL]'>$cfg[TECH_NAME]</a>",LOGIN_MAX_ATTEMPTS);
					$this->login_msg_type	=	CRITICAL;
				}
				return null;				
			}

			//Check if user is allowed to login
			else if (!$myo->allow_login) {
				$this->login_msg 		=	LOGIN_NOT_ALLOWED;
				$this->login_msg_type	=	CRITICAL;
				return null;				
			}
			
			//Validation succeeded
			else {
				
				//Pass the database result object to the login ok function
				$this->loginSucceeded($myo, $remember_me);
			}			
		}		
	}
	
	// *** Login by cookie
	function loginByCookie() {
		
		//Use global vars
		global $sha1_hash, $_ip;
		
		//Get cookie vars
		list($username, $cookie, $login_ip) = unserializeHashed(stripslashes($_COOKIE[$this->cookie_name]), $sha1_hash);
		
		//Invalid vars?
		if (!$username or !$cookie or !$login_ip or ($login_ip != sha1($_ip))) {
	
		 	//Delete the cookie
		 	setcookie($this->cookie_name);
		 	return null;
		}
		else {
	
			//Set the vars
			$this->cookie_id		=	$cookie;

			//Set the fields to fetch from the database
			$fields = implode(',', $this->info_fields_db);
			
			//Validate against database
			$res = eq("	SELECT $fields FROM $this->db_table	WHERE cookie_id = '$this->cookie_id';");
			$myo = mfo($res);
			
			//No results, inactive, invalid credentials or max login attempts failed
			if (!mnr($res) or !$myo->allow_login or $myo->username != $username or 
				$myo->login_attempts >= $this->max_attempts) {
				
			 	//Delete the cookie
			 	setcookie($this->cookie_name);
				return null;			
			}
			
			//Validation succeeded
			else {

				//Pass the database result object to the login ok function
				$this->loginSucceeded($myo, true);
			}
		}
	}
	
	// *** Logout function
	function logout() {
		
		//Remove session info from database, update last active time
		if (!empty($this->login_id)) {
			upRec($this->db_table, $this->login_id, array('session_id','cookie_id','last_online'), array('','','0000-00-00 00:00:00'));
		}
		
		//Set session defaults
		$this->setSessionDefaults();
		
		//Clear cookie
		setcookie($this->cookie_name);
		
		//Not authorized
		$this->authorized = false;		
	}

	// *** Login succeeded function
	function loginSucceeded($myo, $remember_me) {
		
		global $sha1_appendix, $_ip, $cfg;
		
		//Set login variables
		$_SESSION['login_id']			=	$this->login_id				=	$myo->id;
		$_SESSION['login_user']			=	$this->login_user			=	$myo->first_name.' '.$myo->last_name.' ('.$myo->username.')';
		$_SESSION['last_active']		=	$this->last_active			=	time();
		$_SESSION['login_validation']	=	$this->login_validation		=	sha1($myo->password.$sha1_appendix.'7H3ki');

		//CMS related variables
		$this->last_login				= 	$myo->last_login;
		$this->last_login_ip			= 	$myo->last_login_ip;
		$this->is_admin					= 	$myo->is_admin;
		$this->privs					= 	$myo->privs;

		//Construct the login message, if not logged in by cookie
		if (!$this->cookie_id) {
			$this->login_msg 		= 	LOGIN_OK_MSG;
			if ($this->last_login)		$this->login_msg .=	'. '.LOGIN_LAST_LOGIN.' '.date('d/m/Y, h:i',$this->last_login).'.';
		}
		else {
			$this->login_msg 	= 	'';
		}
		$this->login_msg_type	=	'';

		//Log event
		cmsLog("Gebruiker is ingelogd in het CMS vanaf $_ip.");

		//Does this user use our internal mailer (and does the mailer file exist)?
		$_SESSION['uses_mailer'] = file_exists('cms_mailer.php') ? $myo->uses_mailer : 0;
		
		//Reset the brute force login amount
		$_SESSION['cms_id']	=	0;
		
		//Generate a fresh session id, delete old session (php 5)
		@session_regenerate_id(true);

		//Set a cookie, if allowed and if chosen
		if ($remember_me and $cfg['ALLOW_COOKIE_LOGIN']) {
			$this->setLoginCookie($myo->username);
		}

		//Save login session to DB
		$this->saveLoginToDb();

		//Authorized
		$this->authorized = true;
	}

	// *** Function to set a cookie for automated login
	function setLoginCookie($username) {
		
		global $cfg, $sha1_hash, $sha1_appendix, $_ip;
		
		//Make a new unique cookie identification sha1 code
	 	$this->cookie_id = sha1(time().$username.$sha1_appendix);
	 	
	 	//Create the cookie
		$actual_cookie = serializeHashed(array($username, $this->cookie_id, sha1($_ip)), $sha1_hash);
		
		//Set the cookie
		$_SESSION['login_cookie']	=	$this->cookie_id;
		setcookie($this->cookie_name, $actual_cookie, time() + (86400*$cfg['COOKIE_EXPIRE_TIME']));
	}

	// *** Save a login state to the database
	function saveLoginToDb() {
		
		global $_ip;
		
		upRec($this->db_table, $this->login_id, array('last_login','last_login_ip','session_id','cookie_id','login_attempts'),
												array('[NOW()]',$_ip,session_id(),$this->cookie_id,0));
	}
	
	// *** Authentication function
	function authenticate() {

		//Global variables
		global $sha1_appendix;

		//Set the fields to fetch from the database and get the login message
		$fields = implode(',', $this->info_fields_db);
		
		//Query the database, matching login id and session id
		$res = eq("	SELECT $fields FROM $this->db_table WHERE id = '$this->login_id' AND session_id = '".session_id()."';");
		$myo = mfo($res);
		
		//No results?
		if (!mnr($res)) {
			$this->authorized 	= false;
		}
		
		//Password validation string
		else if (sha1($myo->password.$sha1_appendix.'7H3ki') != $this->login_validation) {
			$this->authorized 	= false;
		}
		
		//Timeout?
		else if ((time() - $this->last_active) > $this->max_idle_time and !$this->cookie_id) {
			cmsLog("Gebruiker is uitgelogd uit het CMS door inactiviteit.");
			$this->logout();
			$time_notification		=	$this->max_idle_time/60;
			$this->login_msg 		= 	str_replace('[TIME]', $time_notification, LOGIN_IDLE_LOGOUT);
			$this->login_msg_type	=	WARNING;			
			$this->authorized 		= 	false;
		}
		
		//No longer allowed to login
		else if (!$myo->allow_login) {
			$this->logout();
			$this->login_msg 		= 	LOGIN_NO_LONGER_AUTHORIZED;
			$this->login_msg_type	=	WARNING;
			$this->authorized 		= 	false;
		}

		//Cookie but corrupt?
		else if ($this->cookie_id and $myo->cookie_id != $this->cookie_id) {
			$this->logout();
			$this->authorized 	= false;
		}
		
		//Authentication succeeded
		else {
			
			//Re-set privs
			$this->is_admin		= $myo->is_admin;
			$this->privs		= $myo->privs;

			//Re-set last active time
			$this->last_active 	= $_SESSION['last_active'] = time();
			$this->authorized 	= true;
			
			//Update last active in DB
			upRec($this->db_table, $this->login_id, 'last_online', '[NOW()]');
		}

		return $this->authorized;
	}

	// *** Simple check to see if a user is an administrator
	function isAdmin() {
		return $this->is_admin;
	}
	
	// *** Function to get an array of user privilleges
	function getPrivs() {
		return array_flip(explode(';',$this->privs));
	}

	// *** Make login HTML
	function makeLoginBoxHTML() {
		
		global $cfg, $_file;
		
		$html = "
				<form name='login_form' method='post' action='$_file'>
				<table border='0px' cellspacing='3px' cellpadding='0px'>
					<tr>
						<td width='150px'>".LOGIN_USERNAME.":</td>
						<td><input type='text' name='username' class='input_medium'/></td>
					</tr>
					<tr>
						<td>".LOGIN_PASSWORD.":</td>
						<td><input type='password' name='password' class='input_medium'/></td>
					</tr>
					<tr>
						<td colspan='2'><br/><input type='submit' class='input_submit' name='login' value='".LOGIN_BUTTON."' />
						".($cfg['ALLOW_COOKIE_LOGIN'] ? "
						&nbsp; <input type='checkbox' name='remember_me' value='1' /> ".LOGIN_REMEMBER_ME."
						" : '')."
						</td>
					</tr>
				</table>
				</form>
				".(($msg = $this->getLoginMsg()) ? "<div id='login_error'>$msg</div>" : '')."
		";

		return $html;
	}				
}

// *** Function to return a salted password
function dbSalt($password) {
	$salt			=	'He03Kje2UekEkmPa3MeRbKntw8T9Ons5';
	$salt_prefix	=	'7JjUe83k';
	return sha1($salt_prefix.$password.$salt);
}

// *** Authentication function
function auth() {
	
	global $Message, $s_id, $s_admin, $s_privs;
	global $login_box, $logout_link;
	
	$Login = new Login_CMS();
	$Login->authenticate();
	
	$Message->add($Login->getLoginMsg(), $Login->getMsgType());
	
	if (!$Login->isAuthorized()) {
		$s_id			=	'';
		$s_admin		=	'';
		$s_privs		=	'';
	    $login_box 		= 	$Login->makeLoginBoxHTML();
	    $_POST			=	array();
	    $_GET			=	array();
	    $_REQUEST		=	array();
	    return false;
	}
	else {
		$s_id 			= 	$Login->getLoginID();
		$s_privs		= 	$Login->getPrivs();
		$s_admin 		= 	$Login->isAdmin();
		$login_box		=	'';
		return true;
	}
}
?>