<?php
/*
Title:		Activity logging class
File: 		classes/class_Activity.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

class Activity {
	
	public $auth_user_id, $display_page, $max_per_page, $db_table;
	
	function Activity($db_table, $auth_user_id = 0, $display_page = 1, $max_per_page = 40) {
		
		if (!$display_page) {
			$display_page = 1;
		}
		
		$this->db_table			=	$db_table;
		$this->auth_user_id		=	$auth_user_id;
		$this->display_page		=	$display_page;
		$this->max_per_page		=	$max_per_page;
	}
	
	function readLog() {
		
		$start_limit 	= 	$this->max_per_page * ($this->display_page - 1);
		$end_limit		=	$this->max_per_page;
		$log 			= 	array();
		
		$res = eq("	SELECT 		UNIX_TIMESTAMP(timestamp) AS timestamp, ip_address, activity_text
				 	FROM 		$this->db_table
					WHERE 		auth_user = '$this->auth_user_id'
					ORDER BY 	timestamp DESC
					LIMIT 		$start_limit, $end_limit;");
    	
		while ($myo = mfo($res)) {
			$log[$myo->timestamp]	=	array($myo->ip_address, $myo->activity_text);
		}
		
		return $log;
	}
	
	function purgeLog() {
		
		if ($this->auth_user_id) {
			delRec($this->db_table, "auth_user = '$this->auth_user_id'");
		}
		else {
			eq("TRUNCATE TABLE $this->db_table;");
		}
		
		return true;
	}
	
	function getPageLinks() {
		
		global $_file;
		
		$page			= $this->display_page;
		$total 			= countResults($this->db_table, "auth_user = '$this->auth_user_id'");
		$pages_needed 	= ceil($total / $this->max_per_page);
		
		if ($pages_needed > 1) {
			
			//Fill array with links to the required pages
			for ($x = 1; $x <= $pages_needed; $x++) {
			    if ($x != $page) 	$pages[] = "<a href='$_file?show_log=$this->auth_user_id&page=$x'>$x</a>";
			    else 				$pages[] = "<u>$x</u>";
			}
			
			//Next
			if ($page != $pages_needed) 	$next = $page+1;
			else							$next = 0;
			
			if ($next) 	$next = "<a href='$_file?show_log=$this->auth_user_id&page=$next'>&raquo;</a>";
			else 		$next = "<span class='grey'>&raquo;</span>";
			
			//Prev
			if ($page != 1) 				$prev = $page-1;
			else							$prev = 0;
			
			if ($prev) 	$prev = "<a href='$_file?show_log=$this->auth_user_id&page=$prev'>&laquo;</a>";
			else 		$prev = "<span class='grey'>&laquo;</span>";
			
			//Last
			if ($page != $pages_needed)	$last	=	"<a href='$_file?show_log=$this->auth_user_id&page=$pages_needed'>&raquo;&raquo;</a>";
			else						$last	=	"<span class='grey'>&raquo;&raquo;</span>";
			
			//First
			if ($page != 1)				$first	=	"<a href='$_file?show_log=$this->auth_user_id&page=1'>&laquo;&laquo</a>";
			else						$first	=	"<span class='grey'>&laquo;&laquo</span>";
			
			//HTML to use these pages
			$pages_html = "<h6>$first&nbsp; $prev&nbsp; ".implode(" ",$pages)."&nbsp; $next &nbsp;$last</h6>";
		}
		else {
			$pages_html = '';	
		}
		
		return $pages_html;
	}
}
?>