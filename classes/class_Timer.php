<?php
/*
Title:		Timer class
File: 		classes/class_Timer.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization		
*/

define('TIMER_MODE_RUNNING',	1);
define('TIMER_MODE_STOPPED',	2);
define('TIMER_MODE_PAUSED',		3);

define('TIME_MINUTES',	1);
define('TIME_HOURS',	2);
define('TIME_FRIENDLY',	3);
define('TIME_TIMER',	4);
define('TIME_UNIX',		5);

$pr = CMS_PATH_PREFIX;

$img['play_on']		=	"<img src='{$pr}gfx/cms/timer/play_on.gif' border='0px' style='margin-bottom: -2px;'/>";
$img['play_off']	=	"<img src='{$pr}gfx/cms/timer/play_off.gif' border='0px' style='margin-bottom: -2px;'/>";
$img['pause_on']	=	"<img src='{$pr}gfx/cms/timer/pause_on.gif' border='0px' style='margin-bottom: -2px;'/>";
$img['pause_off']	=	"<img src='{$pr}gfx/cms/timer/pause_off.gif' border='0px' style='margin-bottom: -2px;'/>";
$img['stop_on']		=	"<img src='{$pr}gfx/cms/timer/stop_on.gif' border='0px' style='margin-bottom: -2px;'/>";
$img['stop_off']	=	"<img src='{$pr}gfx/cms/timer/stop_off.gif' border='0px' style='margin-bottom: -2px;'/>";

class Timer {

    private $timer_running, $timer_paused;
    private $start_time, $accumulated_time;
		
	function Timer() {
	
		global $s_admin;
	
		$this->db_table			=	DIGI_DB_PREFIX.'auth_users';
		$this->timer_running	=	0;
		$this->timer_paused		=	0;
		$this->start_time		=	0;
		$this->accumulated_time	=	0;
		
		//Check if we have a timer-valid employee
		if (!$s_admin) {
			
			//Read timer status from database
			$this->readFromDatabase();
			
			//Reset accumulated time if timer is stopped and not paused
			if (!$this->timer_running and !$this->timer_paused) {
				$this->accumulated_time = 0;
				$this->saveToDatabase();
			}

			//Catch controls
			$this->catchControls();
		}
	}

	function readFromDatabase() {
		
		global $s_id;
		
        $res = eq("	SELECT 	timer_running, timer_paused, timer_timestamp, timer_accumulated 
					FROM 	$this->db_table
					WHERE id = '$s_id';");
        $myo = mfo($res);
        
        $this->timer_running 	= 	$myo->timer_running;
        $this->timer_paused		= 	$myo->timer_paused;
        $this->start_time 		= 	$myo->timer_timestamp;
        $this->accumulated_time = 	$myo->timer_accumulated;
    }
    
    function saveToDatabase() {
        
        global $s_id;
        
		upRec($this->db_table, $s_id,
				array('timer_running','timer_paused','timer_timestamp','timer_accumulated'),
				array($this->timer_running,$this->timer_paused,$this->start_time,$this->accumulated_time));
    }
    
    function catchControls() {
		
		global $Message;
		
		if (isset($_GET['start_timer']) and !$this->isRunning()) {
			$this->startTimer();
			$Message->set('Timer gestart');
		}	
		if ((isset($_GET['reset_timer']) or (isset($_POST['reset_timer']) and $_POST['reset_timer'] == 1)) and $this->isRunning()) {
			$this->stopTimer();
			$Message->set('Timer herstart. Verlopen tijd was '.$this->getElapsedTime(TIME_FRIENDLY).'.');
			$this->startTimer();
		}
		if (isset($_GET['stop_timer']) and $this->isRunning())	{
			$this->stopTimer();
			$Message->set('Timer gestopt. Verlopen tijd is '.$this->getElapsedTime(TIME_FRIENDLY).'.');			
		}
		if (isset($_GET['pause_timer'])) {
			$Message->set('Timer gepauzeerd. Verlopen tijd is '.$this->getElapsedTime(TIME_FRIENDLY).'.');
			$this->pauseTimer();	
		}
		if (isset($_GET['resume_timer'])) {
			$Message->set('Timer hervat.');
			$this->pauseTimer();
		}
	}
	
	function startTimer() {

        $this->timer_running 	= 	1;
        $this->timer_paused 	= 	0;
        $this->accumulated_time = 	0;
        $this->start_time 		= 	time();

        $this->saveToDatabase();
    }
    
    function stopTimer() {

		//If stopped during running, we update acumulated time. (If stopped during a pause, we shouldn't update accumulated time.)
		if (!$this->timer_paused) {	
	        $this->accumulated_time = time() - $this->start_time;     
		}
	
        $this->timer_running 	= 	0;
        $this->timer_paused 	= 	0;
		$this->start_time 		= 	0;
		
        $this->saveToDatabase();
    }
    
    function pauseTimer() {
		
		//Unpause
		if ($this->timer_paused) {

			$this->timer_paused 	= 	0;
	        $this->timer_running 	= 	1;
		 	$this->start_time 		= 	time() - $this->accumulated_time;
		 	$this->accumulated_time = 	0;
		}
		
		//Pause
		else {
		 
			$this->timer_paused 	= 	1;
	        $this->timer_running 	= 	1;
		 	$this->accumulated_time = 	time() - $this->start_time;
		 	$this->start_time		= 	0;
		}

	 	$this->saveToDatabase();
	}

	function getMode() {
		if ($this->timer_paused)		return TIMER_MODE_PAUSED;
		elseif ($this->timer_running)	return TIMER_MODE_RUNNING;
		else							return TIMER_MODE_STOPPED;
	}
	
	function getElapsedTime($mode = TIME_UNIX) {
     
     	global $cfg;
     
     	//When paused or stopped, return the accumulated time
     	if ($this->timer_paused or !$this->timer_running)	$elapsed = $this->accumulated_time;
     	
     	//When running, the difference between the current time and the start time.
     	else												$elapsed = time() - $this->start_time;
     
		//Return elapsed time formatted according to chosen mode
		switch ($mode) {
			case TIME_MINUTES:	
				return 	round($elapsed/60);
			
			case TIME_HOURS:
				return 	round($elapsed/3600,$cfg['TIMER_HOURS_PRECISION']);
			
			case TIME_FRIENDLY:
	            $hours 	= 	floor($elapsed / 3600);
    	        $mins 	= 	floor((($elapsed / 3600) - $hours) * 60);
        	    $secs 	= 	$elapsed - ($mins * 60) - ($hours * 3600);
            
            	if ($hours) 	return $hours.' uur '.$mins.' minuten';
            	if ($mins) 		return $mins.' minuten '.$secs.' seconden';
            	else 			return $secs.' seconden';
			
			case TIME_TIMER:
				$hours 	= 	floor($elapsed / 3600);
    	        $mins 	= 	floor((($elapsed / 3600) - $hours) * 60);
        	    $secs 	= 	$elapsed - ($mins * 60) - ($hours * 3600);
        	    
        	    if (strlen($mins) == 1) $mins = '0'.$mins;
        	    if (strlen($secs) == 1) $secs = '0'.$secs;
        	    
				return 	$hours.'h '.$mins.'m '.$secs.'s';
			
			case TIME_UNIX:
			default:
				return 	$elapsed;
		}
    }
    
    function isRunning() {
        return $this->timer_running;
    }
    
    function isPaused() {
        return $this->timer_paused;
    }
 
	function makeTimerControls() {
		
		global $img, $_file;
		
		switch($this->getMode()) {
							
		 	case TIMER_MODE_RUNNING:
				$html = "	<a href='$_file?stop_timer=1' title='Timer stoppen'>$img[stop_on]</a>
							<a href='$_file?reset_timer=1' title='Timer herstarten'>$img[play_on]</a>
							<a href='$_file?pause_timer=1' title='Timer pauzeren'>$img[pause_on]</a>";	
				break;
				
			case TIMER_MODE_PAUSED:
				$html = "	<a href='$_file?stop_timer=1' title='Timer stoppen'>$img[stop_on]</a>
							$img[play_off]
							<a href='$_file?resume_timer=1' title='Timer hervatten'>$img[pause_on]</a>";
				break;
				
			case TIMER_MODE_STOPPED:
			default:
				$html = "	$img[stop_off]
							<a href='$_file?start_timer=1' title='Timer starten'>$img[play_on]</a>
							$img[pause_off]";
				break;
		}
	
		return $html;
	} 
	
	function makeTimerJS() {
		
	 	$html = "
			<script type='text/javascript'>

				var isRunning;
				var elapsed;
				
				if (".( ($this->isRunning() and !$this->isPaused()) ? 'true' : 'false').") {
					isRunning 		= true;
					elapsed 		= ".$this->getElapsedTime().";
					elapsed_hours 	= ".$this->getElapsedTime(TIME_HOURS).";
					timeRun();
				}
				else {
					isRunning 		= false;
					elapsed 		= ".(($this->isRunning() and !$this->isPaused()) ? '0' : $this->getElapsedTime()).";
					elapsed_hours 	= ".(($this->isRunning() and !$this->isPaused()) ? '0' : $this->getElapsedTime(TIME_HOURS)).";
					setTimerTime();
				}
				
				function getTimerTime() {
					var hours = Math.floor(elapsed / 3600);
				 	var minutes = Math.floor( (elapsed - hours * 3600) / 60);
				 	var seconds = elapsed - (hours * 3600) - (minutes * 60);
				 	
				 	minutes = ''+minutes;
				 	seconds = ''+seconds;
				 	
				 	if (minutes.length == 1) minutes = '0'+minutes;
				 	if (seconds.length == 1) seconds = '0'+seconds;
				 	
				 	return hours+'h '+minutes+'m '+seconds+'s';
				}
				
				function setTimerTime() {
					innerHTML('running_time',getTimerTime());
				}
				
				function timeRun() {
					setTimerTime();
					elapsed++;
					if (isRunning) setTimeout('timeRun();',1000);
				}
			</script>
		";
		
		return $html;		
	}
}
?>