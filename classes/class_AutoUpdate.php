<?php
/*
Title:		Auto update class
File: 		classes/class_AutoUpdate.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

define('UPDATE_STANDARD',	1);
define('UPDATE_FTP',		2);
define('UPDATE_MANUAL',		3);

//Try to override setting
if (!ini_get('allow_url_fopen')) {
	ini_set('allow_url_fopen', 1);
}

class AutoUpdate {
	
	// *** Constructor
	function AutoUpdate() {
		
		//return true;
		global $cfg, $_server, $soap_server, $soap_appendix;
		
		//Set vars
		$this->no_soap				=	false;
		$this->checked_for_update	=	false;
		$this->soap_server			=	$soap_server;
		$this->soap_updater			=	'auto_update_r047.php';
		$this->soap_app				=	sha1($soap_appendix);
		$this->update_available		=	false;
		$this->multiple 			= 	false;
		$this->version_data 		= 	array(	
						  				'registrant_id'		=>	(int) $cfg['CMS_REGISTRATION_ID'],
						  				'version_major'		=>	(int) $cfg['CMS_VERSION_MAJOR'],
						  				'version_revision'	=>	(int) $cfg['CMS_VERSION_REVISION'],
						  				'installed_modules'	=>	$cfg['INSTALLED_MODULES'],
						  				'server'			=>	$_server
										);
		$this->repository			=	$this->soap_server.'update_repository/';
		$this->test_file			=	'test.txt';
		$this->update_dir			=	'update';
		$this->sql_queries 			= 	array();
		$this->error				=	array();
		
		//Do we need to check for an update?
		if ($cfg['CHECK_FOR_UPDATES'] and (time() - $cfg['LAST_UPDATE_CHECK']) > $cfg['UPDATE_CHECK_FREQUENCY']*86400) {
			
			//Change update checked time
			changeSetting('LAST_UPDATE_CHECK', time());
			
			//Check for update
			$this->checkForUpdate();	
		}
	}
	
	// *** Connect the soap client
	function connectSoap() {
		
		//Basic socket connection possible?
		if (!canConnect()) {
			$this->no_soap = true;
			return false;
		}

		//Soap functionality
		require_once(CMS_PATH_PREFIX.'apps/nusoap/nusoap.php');

		//Make client
		$this->soap_client 	=	new nusoap_client($this->soap_server.$this->soap_updater.'?wsdl', true);
		
		//Check for soap client error
		$soap_result 		= 	$this->soap_client->call('isConnected');
		$soap_error 		= 	$this->soap_client->getError();
		
		//No Soap available at this time
		if ($soap_error or !$soap_result) {
			$this->error[] = 'SOAP error: '.$soap_error;
			$this->no_soap = true;
			return false;
		}
		
		//Connect OK
		return true;
	}
	
	// *** Check if there is an update, and if so, read the versions information
	function checkForUpdate() {
		
		global $cfg;
		
		//Check only once in the lifespan of this class
		if ($this->checked_for_update) 	return false;
		else							$this->checked_for_update	=	true;
		
		//Connect to SOAP
		if (!isset($this->soap_client)) {
			if (!$this->connectSoap()) {
				return false;
			}
		}
		
		//Soap not working?
		if ($this->no_soap) 			return false;
		
		//Check for update
		$soap_result = $this->soap_client->call('updateAvailable', array($this->version_data));

		if ($soap_error = $this->soap_client->getError()) {
			changeSetting('UPDATE_AVAILABLE',	0);
			$this->error[] = 'SOAP error: '.$soap_error;
			return false;
		}

		if ($soap_result) {
			changeSetting('UPDATE_AVAILABLE',	1);
			changeSetting('UPDATE_TITLE',		$soap_result);
			$cfg['UPDATE_TITLE']			=	$soap_result;
		}
		else {
			changeSetting('UPDATE_AVAILABLE',	0);
			changeSetting('UPDATE_TITLE',		'');
		}
	}
	
	// *** Fetch versions information
	function fetchNewVersions() {

		//Connect to SOAP
		if (!isset($this->soap_client)) {
			if (!$this->connectSoap()) {
				return false;
			}
		}
		
		//Soap not working?
		if ($this->no_soap) return false;

		//Fetch new versions
		$soap_result = $this->soap_client->call('getLatestVersions',array($this->version_data));

		//Error handling
		if ($soap_error = $this->soap_client->getError()) {
			$this->error[] = 'SOAP error: '.$soap_error;
			return false;
		}
		
		$this->new_versions = unserialize($soap_result);
		if (count($this->new_versions) > 1) {
			$this->multiple = true;
		}
	}
	
	// *** Clean the update directory
	function cleanUpdateDirectory() {
		
		//Remove all files, keep the directory
		rmdir_files($this->update_dir, true);
	}
	
	// *** Check if the udpate directory works and is ok for usage
	function checkUpdateDirectory() {
		
		global $chmod, $_sep;
		
		//Attempt to create if doesn't exist
		if (!file_exists($this->update_dir)) {
			if (!@mkdir($this->update_dir)) {
				$this->error[] = str_replace('[DIR]', $this->update_dir.$_sep, DIGI_ERR_AU_DIR_CREATE_FAIL);
				return false;
			}
			else {
				if ($chmod) chmod($this->update_dir, $chmod);
			}
		}
		
		//Check if it is a directory, if it exists, and clean it
		else {
			if (!is_dir($this->update_dir)) {
				$this->error[] = str_replace('[DIR]', $this->update_dir.$_sep, DIGI_ERR_AU_DIR_NOT_A_DIR);
				return false;
			}
			$this->cleanUpdateDirectory();
		}
		
		//Now check if we can perform write operation to this folder
		if ($fh = @fopen($this->update_dir.$_sep.$this->test_file, 'w')) {
			if (@fwrite($fh, 'test')) {
				@fclose($fh);
				if (@unlink($this->update_dir.$_sep.$this->test_file)) {
					return true;
				}
				$this->error[] = str_replace('[DIR]', $this->update_dir.$_sep, DIGI_ERR_AU_UNLINK_FAIL);
				return false;
			}
		}
		$this->error[] = str_replace('[DIR]', $this->update_dir.$_sep, DIGI_ERR_AU_CANT_WRITE);
		return false;
	}
	
	// *** Determine if we can get files
	function canGetFiles() {
		
		global $_sep;
		
		if (ini_get('allow_url_fopen') and @copy($this->repository.$this->test_file, $this->update_dir.$_sep.$this->test_file)) {
			@unlink($this->update_dir.$_sep.$this->test_file);
			return true;
		}
		else {
			require_once(CMS_PATH_PREFIX.'apps/snoopy/snoopy.class.php');
			$Snoopy = new Snoopy();
			if ($Snoopy->fetch($this->repository.$this->test_file)) {
				if ($fh = @fopen($this->update_dir.$_sep.$this->test_file,'wb')) {
					if (@fwrite($fh, $Snoopy->results)) {
						@unlink($this->update_dir.$_sep.$this->test_file);
						fclose($fh);
						return true;
					}
					fclose($fh);
				}	
			}	
		}
		return false;
	}
	
	// *** Determine if we can write files in the root of the CMS install
	function canWriteFiles() {
		if ($fh = @fopen($this->test_file, 'w')) {
			if (@fwrite($fh, 'test')) {
				@fclose($fh);
				if (@unlink($this->test_file)) {
					return true;
				}
			}
		}
		return false;
	}
	
	// *** Determine what update mode is possible for this install
	function determineUpdateMode() {
		
		if ($this->canGetFiles()) {
			if ($this->canWriteFiles()) {
				$this->update_mode = UPDATE_STANDARD;
			}
			else {
				$this->update_mode = UPDATE_FTP;
			}
		}
		else {
			$this->update_mode = UPDATE_MANUAL;
		}
	}
	
	// *** Prepare the update package on the server
	function createUpdatePackage() {

		global $cms_folder;

		//Connect to SOAP
		if (!isset($this->soap_client)) {
			if (!$this->connectSoap()) {
				return false;
			}
		}

		//Soap not working?
		if ($this->no_soap) return false;

		//Send instructions to make the update package
		$soap_result = $this->soap_client->call('createUpdatePackage',array($this->version_data, $cms_folder, $this->soap_app));

		//Error handling
		if ($soap_error = $this->soap_client->getError()) {
			$this->error[] = DIGI_ERR_AU_PKG_CREATE_FAIL;
			$this->error[] = 'SOAP error: '.$soap_error;
			return false;
		}
		
		$pkg = @unserialize($soap_result);
		$this->update_package 		= 	$pkg[0];
		$this->update_package_hash	=	$pkg[1];
		
		if ($this->update_package) {
			return true;
		}
		$this->error[] = DIGI_ERR_AU_PKG_CREATE_FAIL;
		return false;
	}
	
	// *** Set the update package
	function setPackage($package) {
		$this->update_package = $package;
	}

	// *** Get full package url
	function getPackageDownloadURL() {
		return $this->repository.$this->update_package;
	}

	// *** Get partial package url
	function getPackageURL() {
		return $this->update_package;
	}
	
	// *** Set the package hash
	function setPackageHash($hash) {
		$this->update_package_hash = $hash;
	}
	
	// *** Get the package hash
	function getPackageHash() {
		return $this->update_package_hash;
	}

	// *** Get the update package and copy it to the update directory
	function fetchUpdatePackage() {
		
		global $_sep;
		
		if (ini_get('allow_url_fopen') and @copy($this->repository.$this->update_package, $this->update_dir.$_sep.'update.zip')) {
			if (sha1_file($this->update_dir.$_sep.'update.zip') == $this->update_package_hash) {
				return true;	
			}
			$this->error[] = DIGI_ERR_AU_PACKAGE_HASH_FAIL;
			return false;
		}
		
		else {
			require_once(CMS_PATH_PREFIX.'apps/snoopy/snoopy.class.php');
			$Snoopy = new Snoopy();
			if ($Snoopy->fetch($this->repository.$this->update_package)) {
				if ($fh = @fopen($this->update_dir.$_sep.'update.zip','wb')) {
					if (@fwrite($fh, $Snoopy->results)) {
						fclose($fh);
						if (sha1_file($this->update_dir.$_sep.'update.zip') == $this->update_package_hash) {
							return true;	
						}
						$this->error[] = DIGI_ERR_AU_PACKAGE_HASH_FAIL;
						return false;
					}
					fclose($fh);
				}	
			}
		}
		
		$this->error[] = str_replace('[DIR]', $this->update_dir.$_sep, DIGI_ERR_AU_PACKAGE_RETRIEVE);
		return false;
	}
	
	// *** Extract the update package
	function extractPackage() {
		
		global $_sep;
		
		//Get the zip handler
		require_once(CMS_PATH_PREFIX.'apps/pclzip/pclzip.lib.php');
		
		//Initiate zip object
		$PclZip = new PclZip($this->update_dir.$_sep.'update.zip');
		
		//Extract the zip file to final destination
		if ($PclZip->extract(CMS_PATH_PREFIX)) {
			unlink($this->update_dir.$_sep.'update.zip');
			return true;
		}
		else {
			$this->error[]	=	DIGI_ERR_AU_PACKAGE_UNPACK;
			return false;
		}
	}
	
	// *** Extract the update package by FTP
	function extractPackageByFTP() {
		
		global $_sep;
		
		//Get the zip handler
		require_once(CMS_PATH_PREFIX.'apps/pclzip/pclzip.lib.php');
		
		//Initiate zip object
		$PclZip = new PclZip($this->update_dir.$_sep.'update.zip');
		
		//Extract the zip file to temporary location
		if (!$PclZip->extract($this->update_dir)) {
			$this->error[]	=	DIGI_ERR_AU_PACKAGE_UNPACK;
			return false;
		}	
		else {
			
			//Delete the archive
			unlink($this->update_dir.$_sep.'update.zip');
			
			//Connect through FTP
			$ftp_conn 	= @ftp_connect($this->FTP_server);
			
			//Login with username and password
			$ftp_login 	= @ftp_login($ftp_conn, $this->FTP_user, $this->FTP_pass);
			
			//Check if connection was ok
			if (!$ftp_conn or !$ftp_login) {
				$this->error[]	=	DIGI_ERR_AU_FTP_CONNECT;
				return false;	
			} 

			//Trailing slash
			if (substr($this->FTP_folder, -1) != $_sep) {
				$this->FTP_folder .= $_sep;
			}
			
			//Leading slash
			if (substr($this->FTP_folder, 0, 1) != $_sep) {
				$this->FTP_folder = $_sep.$this->FTP_folder;
			}

			//Remember these two settings always
			changeSetting('UPDATE_FTP_SERVER', 		$this->FTP_server);
			changeSetting('UPDATE_FTP_DEST_FOLDER', $this->FTP_folder);
			
			//Remember username/password only if marked
			if ($this->FTP_remember) {
				changeSetting('UPDATE_FTP_USER', 		u_encode($this->FTP_user));
				changeSetting('UPDATE_FTP_PASSWORD', 	p_encode($this->FTP_pass));
				changeSetting('UPDATE_FTP_REMEMBER', 	1);					
			}
			else {
				changeSetting('UPDATE_FTP_USER', 		'');
				changeSetting('UPDATE_FTP_PASSWORD', 	'');
				changeSetting('UPDATE_FTP_REMEMBER', 	0);			
			}

			//Read all update files into array (recurse into subfolders)
			$update_files = readDirectory($this->update_dir);
			$failed_files = array();
			
			//Upload all files
			foreach ($update_files as $source_file) {
				
				//Upload file
				$destination_file	=	$this->FTP_folder.$source_file;
				$ftp_upload 		= 	@ftp_put($ftp_conn, $destination_file, $this->update_dir.$_sep.$source_file, FTP_BINARY);
				
				//Failed?
				if (!$ftp_upload) {
					$failed_files[] 	= 	$destination_file;
				}
			}
			
			//Failed files?
			if (count($failed_files)) {
				$this->error[] 	=	DIGI_ERR_AU_FTP_UPLOAD;
				$this->alertDigi($failed_files, 'files');
			}
			
			//Close connection and return true
			$this->cleanUpdateDirectory();
			@ftp_close($ftp_conn);
			return true;
		}
	}
	
	// *** Test the ftp connection
	function testFTP() {
		
		//Connect through FTP
		$ftp_conn 	= @ftp_connect($this->FTP_server);
		
		//Login with username and password
		$ftp_login 	= @ftp_login($ftp_conn, $this->FTP_user, $this->FTP_pass);
		
		//Check if connection was ok
		if (!$ftp_conn or !$ftp_login) {
			$this->error[]	=	DIGI_ERR_AU_FTP_CONNECT;
			return false;	
		} 
		return true;
	}
	
	// *** Get the SQL queries to perform
	function fetchSQLqueries() {

		//Connect to SOAP
		if (!isset($this->soap_client)) {
			if (!$this->connectSoap()) {
				return false;
			}
		}

		//Soap not working?
		if ($this->no_soap) return false;

		//Send instructions to make the update package
		$soap_result = $this->soap_client->call('fetchSQLqueries',array($this->version_data, $this->soap_app));

		//Error handling
		if ($soap_error = $this->soap_client->getError()) {
			$this->error[] = 'SOAP error: '.$soap_error;
			return false;
		}

		if ($this->sql_queries = @unserialize($soap_result)) {
			if (is_array($this->sql_queries)) {
				foreach ($this->sql_queries as $key => $query) {
					$this->sql_queries[$key] = implode("\r\n", $query);
				}
			}
			else {
				$this->sql_queries = array();
			}
			return true;
		}
		$this->error[]	=	DIGI_ERR_AU_SQL_FETCH;
		return false;
	}
	
	// *** Perform the SQL queries
	function performSQLqueries() {
		
		global $installed_languages, $testmode;
		
		$queries_ok 	= 	0;
		$failed_queries	=	array();
		
		foreach ($this->sql_queries as $qkey => $query) {
			
			evalTrim($query);
			if (empty($query)) {
				unset($this->sql_queries[$qkey]);
			}
			else {
				
				//Multi language query?
				if (substr($query,0,4) == '[ML]') { 
					$query 			= 	substr($query,4);
					$ml_queries_ok	=	0;

					foreach ($installed_languages as $lang) {
						$ml_query = str_replace('[ML]',$lang, $query);

						if (@eq($ml_query)) {
							$ml_queries_ok++;
						}
						else {
							$failed_queries[] = $ml_query;
						}
					}
					
					if ($ml_queries_ok == count($installed_languages)) {
						$queries_ok++;
					}
				}
				
				//Normal query
				else if (@eq($query)) {
					$queries_ok++;				
				}
				else {
					$failed_queries[] = $query;
				}
			} 
		}
		
		if ($queries_ok == count($this->sql_queries)) {
			return true;
		}
		else {
			
			if ($testmode) {
				$this->error[] = DIGI_ERR_AU_QUERIES_FAILED.implode("<br/><br/>",$failed_queries);
			}
			else {
				if ($queries_ok == 0) {
					$this->error[] = DIGI_ERR_AU_ALL_QUERIES_FAILED;
				}
				else {
					$this->error[] = DIGI_ERR_AU_SOME_QUERIES_FAILED;
				}
				$this->alertDigi($failed_queries, 'SQL queries');
			}		
			return false;
		}
	}
	
	// *** Alert Digi
	function alertDigi($failed_elements, $element_type) {
		
		global $cfg, $_version, $_revision;
		require_once(CMS_PATH_PREFIX.'inc/sendmail.php');
		
		$subject	=	'[Update Error] '.$_SERVER['SERVER_NAME'].'(CMS v'.$_version.', '.$_revision.')';
		$message	=	"<u>The following $element_type have failed while updating to the latest version:</u>
						 <br/><br/>".implode("<br/><br/>",$failed_elements);
						 
		sendMail($cfg['DIGI_ALERT_EMAIL'],$subject,$message);
	}
	
	// *** Prepare the update process
	function prepareToUpdate() {
		
		//Connect to SOAP
		if (!isset($this->soap_client)) {
			if (!$this->connectSoap()) {
				return false;
			}
		}
		
		//Get the new versions
		$this->fetchNewVersions();
		
		//Check if the update directory is ok
		if (!$this->checkUpdateDirectory()) {
			return false;	
		}

		//Let the server prepare the update package
		if (!$this->createUpdatePackage()) {
			return false;
		}

		//Determine the update mode (STANDARD / FTP / MANUAL)
		$this->determineUpdateMode();
		
		return true;
	}
	
	// *** Do the actual update
	function doUpdate() {
		
		switch($this->update_mode) {
			
			case UPDATE_STANDARD:
				if ($this->fetchUpdatePackage()) {
					if ($this->fetchSQLqueries()) {
						if ($this->extractPackage()) {
							$this->performSQLqueries();
							$this->updateCurrentVersion();
							return true;
						}
					}
				}
				return false;
				
			case UPDATE_FTP:
				if ($this->testFTP()) {
					if ($this->fetchUpdatePackage()) {
						if ($this->fetchSQLqueries()) {
							if ($this->extractPackageByFTP()) {
								$this->performSQLqueries();
								$this->updateCurrentVersion();
								return true;
							}
						}
					}
				}
				return false;
				
			case UPDATE_MANUAL:
				if ($this->fetchSQLqueries()) {
					$this->performSQLqueries();
					$this->updateCurrentVersion();
					return true;
				}
				return false;
		}
	}
	
	// *** Update this install to the new version
	function updateCurrentVersion() {

		//Connect to SOAP
		if (!isset($this->soap_client)) {
			if (!$this->connectSoap()) {
				return false;
			}
		}

		//Soap not working?
		if ($this->no_soap) return false;
		
		//Config
		global $cfg;
		
		//Get the new versions
		$this->fetchNewVersions();
		
		list ($version_major, $version_minor, $version_revision, $release_date, $release_notes) = $this->new_versions[0];
		
		changeSetting('CMS_VERSION_MAJOR',		$version_major);
		changeSetting('CMS_VERSION_MINOR',		$version_minor);
		changeSetting('CMS_VERSION_REVISION',	$version_revision);
		changeSetting('UPDATE_AVAILABLE',		0);
		changeSetting('UPDATE_TITLE',			0);
		
		//Send instructions to update this version
		$soap_result = $this->soap_client->call('updateCompleted', array($this->version_data,
												$version_major, $version_minor, $version_revision, $this->soap_app));

		//Error handling
		if ($soap_error = $this->soap_client->getError()) {
			$this->error[] = 'SOAP error: '.$soap_error;
		}
	}
	
	// *** Sets the update mode
	function setUpdateMode($mode) {
		$this->update_mode = (int) $mode;
		if ($this->update_mode > UPDATE_MANUAL) {
			$this->update_mode = UPDATE_MANUAL;
		} 
	}
	
	// *** Set the FTP vars
	function setFTPvars($server, $user, $pass, $folder, $remember) {
		$this->FTP_server	=	$server;
		$this->FTP_user		=	$user;
		$this->FTP_pass		=	$pass;
		$this->FTP_folder	=	$folder;
		$this->FTP_remember	=	$remember;
	}
	
	// *** Returns the update mode
	function getUpdateMode() {
		return $this->update_mode;
	}	
	
	// *** Get the error
	function getError() {
		return $this->error;
	}
	
	// *** Get the no-soap indicator
	function noSoap() {
		return $this->no_soap;
	}
	
	// *** Other get functions
	function updateAvailable() {
		global $cfg;
		return $cfg['UPDATE_AVAILABLE'];
	}
	
	function getNewVersions() {
		return $this->new_versions;
	}
	
	function getLatestVersionString() {
		list ($version_major, $version_minor, $version_revision, $release_date, $release_notes, $version_notice) = $this->new_versions[0];
		$version_string = 'v'.$version_major.'.'.sprintf('%02d',$version_minor).' (r'.sprintf('%03d', $version_revision).')';
		return $version_string;
	}
	
	function multipleUpdates() {
		return $this->multiple;
	}
}
?>