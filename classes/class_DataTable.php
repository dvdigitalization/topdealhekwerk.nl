<?php
/*
Title:		Data table class
File: 		classes/class_DataTable.php
Version: 	v2.03
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

class DataTable {
	
	private $colspan, $width, $title;
	private $mark_all_options;
	private $show_search;
	
	//Constructor
	function DataTable($custom_header_title = '', $alternate_width = 0, $hidden_rows = false) {

		$this->title					=	$custom_header_title;
		$this->width					=	$alternate_width;
		$this->use_hidden_rows			=	$hidden_rows;
		$this->colspan 					= 	0;
		$this->mark_all_options 		= 	0;
		$this->total_items				=	0;
		$this->show_search				=	false;
		$this->hidden_inputs			=	array();
	}
	
	//Set the legend row properties
	function setLegendRow($names, $widths, $sort_options) {
		
		$this->legend_names			=	$names;
		$this->legend_widths		=	$widths;
		$this->legend_sort_options	=	$sort_options;
	}
	
	//Set the data rows
	function setDataRows($data_rows) {
		$this->data_rows 	=	$data_rows;
	}

	//Set the hidden rows
	function setHiddenRows($hidden_rows) {
		$this->hidden_rows 	=	$hidden_rows;
	}
	
	//Set the mark all options
	function setMarkAllOptions() {
		$this->mark_all_options 		= 	func_get_args();
	}
	
	//Set the search options
	function showSearch($extra_hidden_inputs = '') {	
		$this->show_search				=	true;
		if ($extra_hidden_inputs) {
			$this->hidden_inputs		=	$extra_hidden_inputs;
		}
	}
	
	//Output the table including header and searchbox
	function getHTML() {
		
		//Used global vars
		global $_file, $_app, $object_lc, $plural_lc, $img, $search_query, $db_ids;
		global $page_appendix, $sort_appendix, $link_appendix, $new_method, $sort, $method, $pages_html;
		global $db_table, $where_clause, $group_clause;

		//Initial variables
		$m 				= 	0;
		$eu				=	'e';
		$data_rows		=	array();
		$mark_row 		= 	'';
		$search_form	=	'';
		$checked		=	'';
		
		//Pages HTML
		if (!empty($pages_html)) {
			$pages_html = '<br/>'.$pages_html;
		}
		
		//Db id's
		if (isset($_GET['multiple']) and isset($db_ids) and is_array($db_ids)) {
			$db_ids = array_flip($db_ids);	
		}
		else $db_ids = array();
		
		//Loop over the data rows
		foreach ($this->data_rows as $key => $data_row_array) {
			
			//The id
			$db_id = $data_row_array[0];
			
			//Make the checkbox TD for mark all functionality, if set
			if (!empty($this->mark_all_options)) {
				$m++;
				
				//Did we perform a mark-all operation? If so, mark those items again
				$checked = isset($db_ids[$db_id]) ? "checked='checked'" : '';				
				$data_row_array[0] = "<input type='checkbox' $checked id='m_$m' value='$db_id'/>";
			}
			else {
				unset($data_row_array[0]);
			}
			
			//Alternate row colors
			$eu = $eu == 'u' ? 'e' : 'u';
			
			//Start the data row HTML
			$data_row = "<tr class='data_row_$eu'>";
			
			foreach ($data_row_array as $field_value) {
				$data_row .= "\n<td>$field_value</td>";
			}
			
			//End the data row HTML
			$data_row .= '</tr>';
			$data_rows[]	=	$data_row;
			
			//Increment the total count
			$this->current_items++;
			
			if ($this->use_hidden_rows) {
				$data_rows[] = "<tr class='data_row_$eu' style='display: none;' id='hidden_row_$db_id'>".$this->hidden_rows[$key]."</tr>";
			}
		}

		//Count the amount of items again
		$join_clause = !isset($join_clause) ? '' : $join_clause;
		$total_items = 	countResults($db_table, $where_clause, $group_clause, '*', $join_clause);

		//Return just a header if we don't have items
		if ($total_items == 0)	return '<h3>'.TXT_NO_ITEMS.' '.$plural_lc.'</h3>';

		//If we have a custom title specified, use it. Otherwise, use standard.
		if ($this->title) {
			$this->title = str_replace('[AMOUNT]', $total_items, $this->title);
		}
		else {
			$this->title = TXT_TOTAL.' '.$total_items.' '.($total_items == 1 ? $object_lc : $plural_lc);
		}

		//Search box
		if ($this->show_search) {
			
			$hidden_inputs = '';
			foreach ($this->hidden_inputs as $input_name => $input_value) {
				$hidden_inputs .= "<input type='hidden' name='$input_name' value='$input_value'/>";
			}
			
			//Build the search box
			$search_form	=	"
				<div class='search'>
					<form name='search_form' action='$_file' method='post'>
					$hidden_inputs
					<input type='text' name='search_query' value='$search_query' class='input_medium'/>
			   		<input type='submit' class='input_submit' name='search' value='".TXT_SEARCH."' />
					</form>
				</div>
			";
		}

		//Start building the HTML
		$html = "
			\n$search_form
			\n<h3>$this->title</h3><hr/>
		";
		
		//Do we need to show the mark all options?
		if (!empty($this->mark_all_options)) {
			
			$mark_buttons = array();
			
			//Go over the mark all options
			foreach($this->mark_all_options as $option) {
				list ($button, $action, $title) = $option;
				$mark_buttons[] =  "<a href='javascript:void(0);' onclick='sendWithMarked(\"$_file\", \"$_app\", \"$action\");'
									title='$title'>$button</a>";
			}
			
			//Make the mark row
			$mark_row = " 
					<div id='mark_all'>$img[arrow_mark] &nbsp; 
					<a href='javascript:void(0);' onclick='markAll();' title='".TXT_MARK_ALL."'>".TXT_ALL."</a> / 
					<a href='javascript:void(0);' onclick='markNone();' title='".TXT_MARK_NONE."'>".TXT_NONE."</a> |
					".TXT_WITH_MARKED_ITEMS.": ".implode(' | ',$mark_buttons)."</div>
			";
		}
		
		//If we have an alternate width specified, set it here
		if ($this->width) {
			$this->width =  "style='width: {$this->width}px;'";
		}

		//Make the legend row
		$this->legend_row = "<tr class='legend_row'>";
		
		//Checkbox for mark all
		if (!empty($this->mark_all_options)) {
			$this->colspan++;
			$this->legend_row .= "<td width='25px'>&nbsp;</td>";
		}
		
		//All legend options
		foreach ($this->legend_names as $key => $name) {
			
			//Increase colspan and set the width
			$this->colspan++;
			$width 			= $this->legend_widths[$key] ? " width='".$this->legend_widths[$key]."px'" : '';
			
			//Is this a sortable column?
			if (!empty($this->legend_sort_options[$key])) {
				
				//Set sorting parameters
				$option 		= $this->legend_sort_options[$key];
				$sort_indicator = $sort == $option ? $img[strtolower($method)] : '';

				//Change the name to a sorting link
				$name = "	<a href='$_file?sort=$option&method=$new_method&$page_appendix&$link_appendix'
							title='".TXT_SORT_BY." ".strtolower($name)."'>$name</a> $sort_indicator";
			}
			
			//Append to the legend row
			$this->legend_row	.=	"<td$width>$name</td>";
		}
		
		//Close the legend row
		$this->legend_row .= "</tr>";
	
		//Make the table HTML		
		$html .= "
			\n<br/>
			\n<table cellpadding='0px' cellspacing='0px' border='0px' class='data_table'".$this->width.">
		    \n".$this->legend_row."
			".implode("\n", $data_rows)."
			\n</table>
			\n$mark_row
			$pages_html
		";
		
		return $html;
	}
}
?>