<?php
/*
Title:		FAQ file
File: 		classes/class_FaqEngine.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization		
*/

class FaqEngine {

	//General vars
	private $Page;

	//Arrays we use
	private $main_cats_array 	= 	array();
	private $sub_cats_array 	= 	array();
	private $topics_array		= 	array();
	
	//Vars for topic content
	private $topic_title, $topic_text;

	function FaqEngine() {
	 	
		//Set DB tables
		$this->DB_faq_topics 	= 	DIGI_DB_PREFIX.'faq_topics';
		$this->DB_faq_cats 		=	DIGI_DB_PREFIX.'faq_categories';
		
	}

	//***
	//* Lees categorieen en subcategorieen uit de DB en zet ze in arrays
	//***********************************************************************************************************
	function readCatArraysFromDB() {
	 
	 	//Uit de database de FAQ categorieen lezen
		$res = eq("SELECT id, name, subcat_of FROM $this->DB_faq_cats ORDER BY id ASC;");
		while ($myo = mfo($res)) {
		 
		 		//Het is een subcat
				if ($myo->subcat_of) $this->sub_cats_array[$myo->subcat_of][$myo->id] = $myo->name;
				
				//Het is een main cat
				else {
					$this->sub_cats_array[$myo->id] = array();
					$this->main_cats_array[$myo->id] = $myo->name;
				}
		}
	}

	//***
	//* Leest de topics voor gegeven cat uit de DB
	//***********************************************************************************************************
	function readTopicsFromDB($cat) {
		
		$res = eq("SELECT id, title FROM $this->DB_faq_topics WHERE faq_cat = '$cat' ORDER BY id ASC;");
		while ($myo = mfo($res)) {
			$this->topics_array[$myo->id] = $myo->title;
		}

		return $this->topics_array;		
	}

	//***
	//* Geeft je een array van de main cats
	//***********************************************************************************************************
	function getMainCats() {
		
		//Lees eerst de categories uit de DB naar arrays als dit nog niet gedaan is
		if (!count($this->main_cats_array)) $this->readCatArraysFromDB();
		
		//Pass de array
		return $this->main_cats_array;
	}

	//***
	//* Geeft je een array van de sub cats bijbehorend bij de gegeven main cat id
	//***********************************************************************************************************
	function getSubCats($main_cat) {
		
		//Lees eerst de categories uit de DB naar arrays als dit nog niet gedaan is
		if (!count($this->main_cats_array)) $this->readCatArraysFromDB();
		
		//Pass de array
		return $this->sub_cats_array[$main_cat];
	}	

	//***
	//* Geeft je een array van de topics bijbehorend bij een gegeven cat id
	//***********************************************************************************************************
	function getFaqTopics($cat_id) {
		
		//Must be integer
		$cat_id = (int) $cat_id;
		
		//Error if no valid category id
		if (!$cat_id) {
			trigger_error('No faq category id was given.',E_USER_WARNING);
			return array();
		}
		
		//Lees eerst de categories uit de DB naar arrays als dit nog niet gedaan is
		$this->readTopicsFromDB($cat_id);
		
		//Pass de topics array
		return $this->topics_array;
	}	

	/***
	 * Sets the active topic and reads topic information from DB
	 *
	 * @param	int		$topic_id	The topic id
	 * @return	boolean				Success
	 *
	 ************************************************************************************************************************/
	function setTopic($topic_id) {
		
		global $mailer, $cfg;
		
		//Has to be integer
		$topic_id = (int) $topic_id;
		
		//Error when no valid topic id given
		if (!$topic_id) {
			trigger_error('No faq topic id was given.', E_USER_WARNING);
			return false;
		}
		
		//Read topic information from DB
		$res = eq("SELECT title, descr FROM $this->DB_faq_topics WHERE id = '$topic_id';");
		$myo = mfo($res);
		
		//If no topic found, trigger error
		if (mysql_num_rows($res) == 0) {
			trigger_error("No data found in the database for faq topic $topic_id.", E_USER_WARNING);
			return false;
		}
		
		//Topic gegevens zetten
		$this->topic_title	=	$myo->title;
		$this->topic_text	=	nl2br($myo->descr);
		
		//Search for italics or link color modifiers
		$this->topic_text	=	str_ireplace('[COLOR]',"<span style='color: #880000;'>", $this->topic_text);
		$this->topic_text	=	str_ireplace('[/COLOR]',"</span>", $this->topic_text);
		$this->topic_text	=	str_ireplace('[I]',"<i>", $this->topic_text);
		$this->topic_text	=	str_ireplace('[/I]',"</i>", $this->topic_text);
		$this->topic_text	=	str_ireplace('[B]',"<b>", $this->topic_text);
		$this->topic_text	=	str_ireplace('[/B]',"</b>", $this->topic_text);
		$this->topic_text	=	str_ireplace('[U]',"<u>", $this->topic_text);
		$this->topic_text	=	str_ireplace('[/U]',"</u>", $this->topic_text);
		
		//Search for technical
		$this->topic_text	=	str_replace('[TECHNICAL]',"<a href='$mailer".$cfg['TECH_EMAIL']."'>".$cfg['TECH_NAME']."</a>", $this->topic_text);
												
		$this->searchForImages($this->topic_text, 0);
		$this->searchForInternalLinks($this->topic_text, 0);
		$this->searchForExternalLinks($this->topic_text, 0);
		
		return true;
	}
	
	function searchForImages(&$text, $offset) {
			
		global $img;
		
		if ($start_pos 	= stripos($text, '[IMG]', $offset)) {
			$end_pos 	= stripos($text, '[/IMG]', $start_pos);
			
			$image 	= substr($text, $start_pos+5, ($end_pos-$start_pos-5));
			$text 	= substr_replace($text, $img[$image], $start_pos, ($end_pos-$start_pos+6));
			
			$this->searchForImages($text, $end_pos);
		}
	}
	
	function searchForInternalLinks(&$text, $offset) {
			
		global $files, $img;
		
		if ($start_pos 	= stripos($text, '[FILE]', $offset)) {
			$end_pos 	= stripos($text, '[/FILE]', $start_pos);
			
			list ($file, $title) = explode('|',substr($text, $start_pos+6, ($end_pos-$start_pos-6)));
			$text = substr_replace($text,"<a href='".$files[$file]."'>$title  $img[link]</a>", $start_pos, ($end_pos-$start_pos+7));
			
			$this->searchForInternalLinks($text, $end_pos);
		}
	}

	function searchForExternalLinks(&$text, $offset) {
		
		if ($start_pos = stripos($text, '[link', $offset)) {
			$end_pos 		= stripos($text, '[/link]', $start_pos);
			$end_of_link 	= strpos($text, ']', $start_pos);
			
			$link 	= substr($text, $start_pos + 6, ($end_of_link - $start_pos -6));
			$title	= substr($text, $end_of_link + 1, ($end_pos - $end_of_link - 1));
			
			$text 	= substr_replace($text, "<a href='$link' title='$title'>$title</a>", $start_pos, ($end_pos-$start_pos+7));
			
			$this->searchForExternalLinks($text, $end_pos);
		}
	}
	
	// *** Get functions
	
	function getTopicTitle() {
		return $this->topic_title;
	}
	
	function getTopicText() {
		return $this->topic_text;
	}
	
	function getCatFromTopic($topic_id) {

		//Has to be integer
		$topic_id = (int) $topic_id;
				
		//Query database and return
		$res = eq("SELECT faq_cat AS cat FROM $this->DB_faq_topics WHERE id = '$topic_id';");
		$myo = mfo($res);
		
		return $myo->cat;
	}
}
?>