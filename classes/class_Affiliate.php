<?php
/*
Title:		Affiliate class
File: 		classes/class_Affiliate.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

class Affiliate {
	
	function Affiliate() {
		
		$this->db_table			=	DIGI_DB_PREFIX.'affiliates';
		
		//Does this visitor already have an affiliate associated with him?
		if (isset($_SESSION['affiliate'])) {
			return null;
		}
		
		//If not, do we have affiliate variables coming in?
		else if (isset($_GET['ref']) and $db_id = (int) $_GET['ref'] and isset($_GET['code'])) {
			
			//If so, are they valid and is the affiliate active?
			if (getMyo($this->db_table,"id = '$db_id' AND active = '1'",'affiliate_code') == $_GET['code']) {
				
				//Associate this affiliate with this visitor
				$_SESSION['affiliate']	=	$db_id.$_GET['code'];
				
				//Update the amount of page hits for this affiliate
				eq("UPDATE $this->db_table SET page_hits = page_hits + 1 WHERE id = '$db_id';");
			}
			
			//Set to blank if invalid
			else {
				$_SESSION['affiliate']	=	'';
			}
		}
	}
	
	function getId() {
		
		if (isset($_SESSION['affiliate'])) {
			
			//Read vars from session
			$db_id 	= 	(int) substr($_SESSION['affiliate'],0,1);
			$code	=	substr($_SESSION['affiliate'],1);
			
			//Validate again
			if (getMyo($this->db_table,"id = '$db_id' AND active = '1'",'affiliate_code') == $code) {
				return $db_id;	
			}
		}
		else {
			return 0;
		}
	}
}
?>