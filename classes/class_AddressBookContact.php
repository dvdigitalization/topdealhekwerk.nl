<?php
/*
Title:		Addressbook contact class
File: 		classes/class_AddressBookContact.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

class AddressBookContact {

    private $id;

    function AddressBookContact($id) {
    	
        $this->id 		= 	$id;
        $this->db 		= 	'address_book';
        
        $this->getInfoFromDB();
    }
    
    function getInfoFromDB() {
        $res = eq("SELECT * FROM $this->db WHERE id = '$this->id';");
        $mya = mfa($res);
        
        foreach ($mya as $key => $value) {
			$this->$key = $mya[$key];
		}
    }
    
    function getId() {
		return $this->id;
	}
	
	function getSince($formatted = true) {
		return $formatted ? time2date($this->timestamp) : $this->timestamp;
	}
    
    function getName() {
        return ucwords(strtolower($this->name));
    }
    
    function getTitle() {
        return $this->title;
    }
        
    function getTitleText() {
    	global $SEX_ARRAY;
        return $SEX_ARRAY[$this->title];
    }
    
    function getNameAndTitle() {
		return $this->getTitleText().$this->getName();
	}
    
    function getAddress() {
        return ucwords(strtolower($this->address));
    }
	
	function getAddressNr() {
		return strtoupper(str_replace(' ','',$this->nr));
	}
	
	function getPostalCode() {
		return strtoupper(str_replace(' ','',$this->postal_code));
	}
	
	function getCity() {
		return ucwords(strtolower($this->city));
	}
	
	function getCountry() {
		return ucwords(strtolower($this->country));
	}
	
	function getAddressLabel() {
		
		$label = '';
		
		if ($address = $this->getAddress()) {
			$label .= $address.' '.$this->getAddressNr();
		}
		if ($postal_code = $this->getPostalCode()) {
			if ($label) $label.= '<br/>';
			$label .= $postal_code;
			if ($city = $this->getCity()) {
				$label .= ' '.$city;
			}
		}
		if (!$this->getPostalCode() and $city = $this->getCity()) {
			if ($label) $label.= '<br/>';
			$label .= $city;
		}
		if ($country = $this->getCountry()) {
			if ($label) $label.= '<br/>';
			$label .= $country;
		}
					
		return $label;
	}
	
	function getPhone() {
		return $this->phone;
	}

	function getMobile() {
		return $this->mobile;
	}

	function getFax() {
		return $this->fax;
	}	
	
	function getEmail() {
		return strtolower($this->email);
	}

	function getWebsite() {
		return strtolower($this->website);
	}
	
	function getEmailLink() {
		global $mailer;
		$email = $this->getEmail();
		return "<a href='$mailer$email' title='Stuur deze contactpersoon een email'>$email</a>";
	}

	function getWebsiteLink() {
		require_once(CMS_PATH_PREFIX.'inc/functions/split_website.php');
		list ($site_url, $site_name) = split_website($this->getWebsite());
		return "<a href='$site_url' title='Open deze website in een nieuw venster' target='_blank'>$site_name</a>";
	}

	function getRemarks() {
		return $this->remarks ? nl2br($this->remarks) : 'Geen';
	}
	
	function hasAddressInfo() {
		if ($this->getAddress() or $this->getCity() or $this->getCountry()) {
			return true;
		}
		return false;
	}
	
	function hasContactInfo() {
		if ($this->getEmail() or $this->getFax() or $this->getPhone() or $this->getMobile() or $this->getWebsite()) {
			return true;
		}
		return false;
	}

	function isPrivate() {
		return $this->private ? true : false;
	}
	
	function isDeleted() {
		return $this->deleted ? true : false;
	}
}
?>