<?php
/*
Title:		Shopping cart class
File: 		classes/class_Shoppingcart.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

class Shoppingcart {

    private $Items 		= 	array();
    private $strings 	=	array();
    
    private $has_promo, $promo_code, $promo_code_id, $promo_discount;

    function Shoppingcart() {

		$this->has_promo			=	false;
		$this->promo_code			=	'';
		$this->promo_discount		=	0;
		$this->promo_code_id		=	0;
		
		$this->db_delivery_costs	=	DIGI_DB_PREFIX.'delivery_costs';
		$this->db_countries			=	DIGI_DB_PREFIX.'countries';
    }
    
    function refresh() {
		foreach($this->Items as $key => $Item) {
			if (is_object($Item->Product)) {
				$Item->Product->readFromDB();
			}
			else {
				unset($this->Items[$key]);
			}
		}
	}

    function addItem($id, $amount, $string) {
        
        if (!in_array($string, $this->strings)) {
        	
        	$this->strings[] = $string;
        	
			if (isset($this->Items[$id])) {
				$this->Items[$id]->addAmount($amount);	
			} 
	        else {
				$this->Items[$id] = new Item($id,$amount);
			}
		}
    }
    
    function removeItem($id, $amount, $string) {
        
        if (!in_array($string, $this->strings)) {
        	
        	$this->strings[] = $string;
			
			if (isset($this->Items[$id])) {
	            $this->Items[$id]->removeAmount($amount);
	            if ($this->Items[$id]->getAmount() < 1) {
					unset($this->Items[$id]);	
				}
	        }
	    }
    }
    
    function deleteItem($id, $string) {
    
	    if (!in_array($string, $this->strings)) {
        	
        	$this->strings[] = $string;
        	
			if (isset($this->Items[$id])) {
				unset($this->Items[$id]);
			}
		}
    }

    function emptyCart($string) {
    
	    if (!in_array($string, $this->strings)) {
        	
        	$this->strings[] 	= $string;
			$this->Items 		= array();
		}
    }
    
    function getItems() {
        return $this->Items;
    }
    
    function getItemTotalPrice($id) {
        return $this->Items[$id]->getTotalPrice();
    }

    function getAmountOfItems() {
		return count($this->Items);
	}

    function getTotalPrice() {
    	$totalprice = 0;
        foreach($this->Items as $Item) {
            $totalprice += $Item->getAmount() * $Item->getProductPrice();
        }
        return $totalprice;
    }
    
    function getTotalAmount() {
    	$totalamount = 0;
        foreach($this->Items as $Item) {
            $totalamount += $Item->getAmount();
        }
        return $totalamount;
    }
    
    function getTotalVat() {
    	$totalvat = 0;
        foreach($this->Items as $Item) {
            $totalvat += $Item->getItemVat();
        }
        return $totalvat;		
	}
    
    function getTotalDeliveryPoints() {
    	$totalpoints = 0;
		foreach($this->Items as $Item) {
			$totalpoints += $Item->getDeliveryPoints();
		}
		return $totalpoints;
	}
    
    function getDeliveryCosts($country) {
		
		//Calculate the delivery points total
		$delivery_points = $this->getTotalDeliveryPoints();
		
		//Check if we have data for this specific country
		if (getMyo($this->db_delivery_costs, "country = '$country'",'id')) {
			$where	= "country = '$country'";
		}
		//If not, obtain region first
		else {
			$region = getMyo($this->db_countries,$country,'region');
			$where	= "region = '$region'";
		}
		
		//Get price and repeat points
		$delivery = getMyo(	$this->db_delivery_costs, "points <= '$delivery_points' AND $where",
							array('points','price','repeat_points'),'points','DESC',1);

		//No result?
		if (!$delivery) {
			return 0;
		}

		//Do these delivery costs repeat?
		if ($delivery->repeat_points) {
			$times_to_repeat 	= 1 + floor(($delivery_points - $delivery->points) / $delivery->repeat_points);
			$delivery_costs 	= $delivery->price * $times_to_repeat;
		}
		
		//If not, delivery costs are simply the price
		else {
			$delivery_costs 	= $delivery->price;
		}
		
		//Return the delivery costs
		return $delivery_costs;
	}
	
	function setPromoCode($code) {
		
		evalAll($code);
		$code = strtoupper($code);
		
		$res = eq("	SELECT id, active, percentage, one_time_use FROM ".DIGI_DB_PREFIX."discount_codes
					WHERE code = '$code' AND active = '1';");
		if (!mysql_num_rows($res)) {
			return PROMOCODE_INVALID;
		}
		
		$myo = mfo($res);
		if (!$myo->active) {
			return PROMOCODE_EXPIRED;
		}
		
		$this->has_promo		=	true;
		$this->promo_code		=	$code;
		$this->promo_code_id	=	$myo->id;
		$this->promo_percentage	=	$myo->percentage;
		$this->promo_one_time	=	$myo->one_time_use;
		return true;
	}
	
	function validatePromoCode($customer_id) {
		
		if ($this->promo_one_time and 
			countResults(DIGI_DB_PREFIX.'orders', "customer_id = '$customer_id' AND discount_id = '$this->promo_code_id'")) {
			
			$this->clearPromoCode();
			return false;
		}
		return true;
	}
	
	function clearPromoCode() {
		$this->has_promo		=	false;
		$this->promo_code		=	'';
		$this->promo_discount	=	0;
	}
	
	function hasPromoCode() {
		return $this->has_promo;
	}
	function getPromoCode() {
		return $this->promo_code;
	}
	function getPromoCodeId() {
		return $this->promo_code_id;
	}
	function getPromoPercentage() {
		return $this->promo_percentage;
	}	
}

/*
Title:		Shopping cart item class
File: 		classes/class_Item.php
Version: 	v2.00
Author:		Digitalization
Contact:	info@digitalization.nl
*/
class Item {

    public 	$Product;
	private	$amount;

    function Item($id,$amount) {
        $this->Product 		= 	new Product($id);
        $this->step_size 	= 	$this->Product->getStepsize();
        $this->amount 		= 	$amount;
    }
    
    function addAmount($amount) {
    	$amount = $amount < $this->step_size ? $this->step_size : ($amount - ($amount % $this->step_size));
        $this->amount 	+= 	$amount;
    }
    
    function removeAmount($amount) {
    	$amount = $amount < $this->step_size ? $this->step_size : ($amount - ($amount % $this->step_size));
        $this->amount 	-= 	$amount;
        $this->amount	=	$this->amount < 0 ? 0 : $this->amount;
    }
    
    function getAmount() {
        return $this->amount;
    }

    function getProductId() {
        return $this->Product->getId();
    }
    
    function getProductName() {
        return $this->Product->getName();
    }

    function getProductPrice($force_vat_incl = false) {
        return $this->Product->getPrice($force_vat_incl);
    }
    
	function getProductOnSale() {
		return $this->Product->onSale();
	} 

	function getItemVat() {
		return $this->amount * $this->getProductVat();
	}

	function getProductVat() {
		if ($this->Product->vatIncluded()) {
			$vat_percentage	=	$this->Product->getVatPercentage();
			$vat_on_product	=	$vat_percentage * $this->Product->getPrice() / (100 + $vat_percentage);
			return round($vat_on_product);
		}
		else {
			return 0;
		}
	}

	function getProductVatPercentage() {
		return $this->Product->getVatPercentage();
	}
    
    function getTotalPrice() {
        return $this->amount * $this->Product->getPrice();
    }

	function getDeliveryPoints() {
		return $this->amount * $this->Product->getDeliveryPoints();
	}
	
	function getProductThumbUrl($gfx_folder) {
		return $this->Product->getThumbUrl($gfx_folder);
	}
	
	function getProductProp($prop) {
		return $this->Product->getProp($prop);
	}
}

/*
Title:		Product class
File: 		classes/class_Product.php
Version: 	v2.00
Author:		Digitalization
Contact:	info@digitalization.nl
*/
class Product {

    private $id;

    function Product($id) {
    	
        $this->id 					= 	$id;
        $this->db_products			= 	DIGI_DB_PREFIX.'products';
        $this->db_groups 			= 	DIGI_DB_PREFIX.'product_groups';
        $this->db_product_props		=	DIGI_DB_PREFIX.'product_data';
        $this->db_group_props		=	DIGI_DB_PREFIX.'product_group_data';
        
        $this->readFromDB();
    }
    
    function readFromDB() {
    	
    	global $_lang, $cfg;

		//Read main product data    	
        $res 		= eq("SELECT * FROM $this->db_products WHERE id = '$this->id';");
        $product 	= mfo($res);

		//Set main product properties
		foreach (get_object_vars($product) as $key => $value) {
			$this->$key	= $value;
		}

		//Read group product data    	
        $res 		= eq("SELECT * FROM $this->db_groups WHERE id = '$this->group_id';");
        $group 		= mfo($res);

		//Read name and description, language dependent
		if ($cfg['PROD_USE_SEPARATE_PROPS']) {  
			$props = getMyo($this->db_product_props, "lang = '$_lang' AND product_id = '$this->id'", array('name','description'));
		}
		else {             
			$props = getMyo($this->db_group_props, "lang = '$_lang' AND group_id = '$this->group_id'", array('name','description'));
		}
		      
		//Set name and description (language dependant)
		$this->name				=	$props->name;
		$this->description		=	$props->description;

		//Product no
		if ($cfg['PROD_USE_SEPARATE_PROD_NO']) {
			$this->product_no	=	$product->product_no;
		}
		else {
			$this->product_no	=	$group->product_no;
		}

		//Some group props
		$this->step_size		=	$group->step_size;
		$this->delivery_points	=	$group->delivery_points;
		$this->product_type		=	$group->product_type;
		$this->vat_included		=	$group->vat_included;
		$this->vat_percentage	=	$group->vat_percentage;
        
        //Price
        if ($product->on_sale) {
			$this->on_sale		=	true;
			$this->price		=	$product->sale_price;
		}
		else {
			$this->on_sale		=	false;
			$this->price		=	$product->price;
		}
    }
    
    function getId() {
		return $this->id;
	}
    
    function getName() {
        return $this->name;
    }
	 
	function getDescription() {
		return $this->description;
	} 
	   
    function getProductNo() {
        return $this->product_no;
    }
    
    function getProductType() {
		return $this->product_type;
	}
        
    function getPrice($force_vat_incl = false) {
    	if ($force_vat_incl) {
			
			//Vat is included
			if ($this->vatIncluded()) {
				return $this->price;
			}
			
			//Not yet included
			else {
				return round($this->price * (100+$this->vat_percentage)/100);
			}
		}
		else {
			return $this->price;	
		}
    }

    function getVatPercentage() {
        return $this->vat_percentage;
    }
	
	function getStepsize() {
		return $this->step_size;
	}

	function getDeliveryPoints() {
		return $this->delivery_points;
	}
	
	function getProp($prop) {
		if (isset($this->$prop)) {
			return $this->$prop;
		}
		else return '';
	}

	function getThumbUrl($gfx_folder) {
		global $_sep;
		return $gfx_folder.$_sep.$this->group_id.$_sep.'thumb_'.$this->id.'.jpg';
	}
		
	function onSale() {
		return $this->on_sale;
	}
	
	function vatIncluded() {
		return $this->vat_included;
	}
}
?>