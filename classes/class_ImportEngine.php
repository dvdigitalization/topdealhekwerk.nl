<?php
/*
Title:		Import engine class
File: 		classes/class_ImportEngine.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

class ImportEngine {
	
	function ImportEngine($file, $file_type = 'XML', $item_definition, $props) {
		
		$this->file		=	$file;
		$this->type		=	$file_type;
		$this->item		=	$item_definition;
		$this->props	=	$props;
		$this->data		=	array();		
	}
	
	function process() {
		switch ($this->type) {
			case 'XML':
				return $this->process_XML();
				break;
				
			case 'CSV':
				return $this->process_CSV();
				break;
				
			default:
				return false;
		}
	}
	
	function process_XML() {
		
		//Create new XML object
		$xml 	= 	new SimpleXMLElement(file_get_contents($this->file));			
		
		//Run over each item
		foreach ($xml->${$this->item} as ${$this->item}) {
			
			$values = array();
			
			//Run over each prop
			foreach ($this->props as $key=> $prop) {
				if (isset(${$this->item}->$$prop)) {
					$values[$key]	=	$values[$prop]	=	${$this->item}->$$prop;
				}
				else {
					$values[$key]	=	$values[$prop]	=	'';
				}
			}

			//Append to data array
			$this->data[]	=	$values;
		}
		
		return true;
	}
	
	function process_CSV() {
		
		//Process CSV file
		$fh = fopen($this->file, 'r');
		while (($item_values = fgetcsv($fh)) !== false) {
		    
		    //Clear array
			$values 		= 	array();
			
		    //Go over the props
			foreach ($this->props as $key => $prop) {
				if (isset($item_values[$key])) {
					$value 			= 	trim($item_values[$key]);
					$values[$key]	=	$values[$prop]	=	$value;
				}
				else {
					$values[$key]	=	$values[$prop]	=	'';
				}
			}
			
			//Append to data array
			$this->data[]	=	$values;
		}
		fclose($fh);

		return true;
	}
	
	function reset_data() {
		$this->data = array();
	}
	
	function get_data() {
		return $this->data;	
	}
	
	function get_element() {
		if (count($this->data)) {
			return array_shift($this->data);
		}
		return false;
	}
		
	function get_count() {
		return count($this->data);
	}
}
?>