<?php

/**
 * @author Digitalization
 * @copyright 2009
 */

class Gaas {

	public function __construct($category_id) {

		global $vk_height, $vk_length;

		$this->category_id	=	(int) 	$category_id;
		$this->height		=	(int) 	request('height', $vk_height);
		$this->color		=	(int) 	request('color', 0);
		$this->corners		=	(int) 	request('corners', 0);
		$this->sides		=	(int) 	request('sides', 1);
		$this->mural		=	(int) 	request('mural', 0);
		$this->poles		=	(bool) 	request('poles', 0);
		$this->vk_length	=	(int)	$vk_length;
	}

	//Get the available heights HTML
	public function get_heights_html() {

		//Globals
		global$db_table_products;

		//Read available heights
		$available_heights = array(0 => 'Maak uw keuze');
		$res_h = eq("SELECT height FROM $db_table_products WHERE category_id = '$this->category_id' GROUP BY height;");
		while ($height = mfo($res_h)) {
			$available_heights[$height->height] = (int) $height->height;
		}
		asort($available_heights);

		//Height selector
		return makeSelectBox(	'height', $available_heights, $this->height, 0,
								"class='selectMed' onchange='changed_height(this.value);'");
	}

	//Get the corners HTML (always a selectbox)
 	public function get_corners_html() {
 		$corners_array	=	array(0,1,2,3,4);
		return	makeSelectBox(	'corners', $corners_array, $this->corners, 0,
								"class='selectMed' onchange='changed_corners(this.value);update_plasitor();'");
 	}

	//Get the sides HTML (either a selectbox, or a hidden input, depending on the amount of corners)
 	public function get_sides_chooser_html() {

 		switch ($this->corners) {
 			case 4:
 				return	makeSelectBox(	'sides', array(4 => 4, 5 => 5), $this->sides, 0,
										"class='selectMed' onchange='changed_sides(this.value);'");
 				break;
 			case 3:
 				return	makeSelectBox(	'sides', array(3 => 3, 4 => 4), $this->sides, 0,
										"class='selectMed' onchange='changed_sides(this.value);'");
 				break;
 			default:
 				return $this->sides."<input type='hidden' id='sides' name='sides' value='".$this->sides."' />";
 				break;
 		}
 	}

	//Get the mural HTML (a selectbox)
 	public function get_mural_html() {
		return	makeSelectBox(	'sides', array(0,1,2,3,4), $this->mural, 0,
								"class='selectMed' onchange='calc_price(\"Changed mural\");'");
 	}

 	//Get available poles HTML
 	public function get_poles_html() {

		//No height? Choose first
		if (!$this->height) {
			return "<p>Kies eerst een hoogte</p>";
		}

		//Globals
		global $db_table_products, $db_table_cats, $gfx_folder, $_sep, $cat_prefix, $pole_categories, $pole_height_map, $vat_multiplier;

		//Read poles
		$poles_html = "";
		$p = 0;
		if (isset ($pole_categories[$this->category_id]) and count($pole_categories[$this->category_id])) {
			$res_p = eq("	SELECT 	$db_table_products.id, name, menu_name, description, $db_table_cats.id AS cat_id,
									normal_price, price, sale_price, on_sale, price_unit, height
							FROM 	$db_table_products
							LEFT JOIN $db_table_cats ON (category_id = $db_table_cats.id)
							WHERE   category_id IN (".implode(',',$pole_categories[$this->category_id]).")
								AND color_id = '$this->color'
			");
			while ($pole = mfo($res_p)) {

				//Height must match
				if (empty($pole_height_map[$pole->cat_id][$this->height]) or $pole_height_map[$pole->cat_id][$this->height] != $pole->height) {
					continue;
				}

				//Set some stuff
				$p++;
				$pole->price 	=	parseAmount($pole->on_sale ? $pole->sale_price*$vat_multiplier : $pole->price*$vat_multiplier, '&euro; ');
				$pole->normal_price = parseAmount($pole->normal_price*$vat_multiplier, '&euro; ');
				$pole->name 	= 	$pole->menu_name ? $pole->menu_name : $pole->name;
				$pole_image_url	=	$gfx_folder.$_sep.$cat_prefix.$pole->cat_id.'.jpg';

				//No request? Just use the default pole
				if (!$req_pole = request('pole')) {
					$checked 		= 	isset($checked) ? '' : "checked='checked'";
				}

				//Otherwise match the request
				else {
					$checked 		= 	$req_pole != $pole->id ? '' : "checked='checked'";
				}

				//Append the HTML
				$poles_html .= "
					<div class='paal'>
						<label for='paal_$p'>
							<img src='$pole_image_url' alt='$pole->name' width='125px'/>
							<input type='radio' class='pole' name='pole' value='$pole->id' onclick='calc_price(\"Changed pole type\");' id='paal_$p' $checked /> <span>$pole->name<br/>Normaal: $pole->normal_price p/st<br/>Voor: $pole->price p/st</span>
							<div class='info' id='infoPaal_$p' onmouseover=\"displayDiv('infoPaal_$p', 1);\" onmouseout=\"displayDiv('infoPaal_$p', 0);\" style='display:none;'>
								<h2>$pole->name</h2>
								$pole->description
							</div>
							<a onmouseover=\"displayDiv('infoPaal_$p', 1);\" onmouseout=\"displayDiv('infoPaal_$p', 0);\" class='infoIcon'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:3px 0 0;' /></a>
						</label>
					</div>
				";
			}
		}

		if (!$p) {
			return "<p>Geen palen gevonden voor de gekozen gaas hoogte</p>";
		}

		return $poles_html;
 	}

	//Get HTML for all sides
	public function get_sides_html() {

		//Initialize sides HTML
		$sides_html = '';

		//Loop from 1 to 5 and get side HTML
		for ($s = 1; $s <= 5; $s++) {
			$sides_html .= $this->get_side_html($s);
		}

		//Return
		return $sides_html;
	}

	//Get HTML for a specific side
	public function get_side_html($side) {

		//Has gate or not
		$has_gate = request('has_gate_'.$side);

		return "
			<div class='blok' id='side_$side' style='".($side > $this->sides ? "display: none;" : '')."'>
				<h2>Zijde $side</h2>
				<h3>Zijde $side: Toegangscontrole</h3>
				<div class='info' id='toegangscontroleZijde_$side' onmouseover=\"displayDiv('toegangscontroleZijde_$side', 1);\" onmouseout=\"displayDiv('toegangscontroleZijde_$side', 0);\" style='display:none;'>
					".INFO_GAAS_ACCESS_CONTROL."
				</div>
				<a onmouseover=\"displayDiv('toegangscontroleZijde_$side', 1);\" onmouseout=\"displayDiv('toegangscontroleZijde_$side', 0);\" class='infoIcon'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:1px 0 0 5px;' /></a>
				<div class='table'>
					<table cellpadding='0' cellspacing='0' border='0'>
						<tr>
							<td width='25' height='22' align='left' valign='top'><input type='radio' name='has_gate_$side' value='0' onclick='changed_has_gate($side, this.value);' ".($has_gate ? "" : "checked='checked'")." /></td>
							<td align='left' valign='middle'>Geen toegangspoort</td>
						</tr>
						<tr>
							<td width='25' height='22' align='left' valign='top'><input type='radio' name='has_gate_$side' value='1' onclick='changed_has_gate($side, this.value);' ".(!$has_gate ? "" : "checked='checked'")." /></td>
							<td align='left' valign='middle'>Toegangspoort(en)</td>
						</tr>
					</table>
				</div>
				<div class='subtableDivider'>&nbsp;</div>
			</div>
			<div id='gate_color_chooser_$side' class='kleurKiezenLayer' style='display: none;'>
				<div>
					<div class='kleurKiezenLayerSluiten'><img src='gfx/layout/nav/close.gif' width='16' height='16' alt='Sluiten' onclick=\"displayDiv('gate_color_chooser_$side', 0);\" /></div>
					<h2>Kies uw kleur</h2>
				</div>
				<div id='gate_colors_available_$side'>

				</div>
			</div>
			<div class='blok' id='side_gates_$side' ".(($has_gate and $side <= $this->sides) ? '' : "style='display: none;'").">".($has_gate ? $this->get_gates_html($side) : "")."</div>
			<div class='blok' id='side_lengths_$side' style='".($side > $this->sides ? "display: none;" : '')."'>
				<h3>Zijde $side: Bepaal de lengte</h3>
				<div class='table' id='lengths_table_$side'>
					<table cellpadding='0' cellspacing='0' border='0' id='lengths_table_$side'>
						".$this->get_side_lengths_html($side)."
					</table>
				</div>
				<div class='subtableDivider'>&nbsp;</div>
				<div class='info' id='zijdeLengte_$side' onmouseover=\"displayDiv('zijdeLengte_$side', 1);\" onmouseout=\"displayDiv('zijdeLengte_$side', 0);\" style='display:none;'>
					".INFO_GAAS_LENGTH."
				</div>
				<a onmouseover=\"displayDiv('zijdeLengte_$side', 1);\" onmouseout=\"displayDiv('zijdeLengte_$side', 0);\" class='infoIcon'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:1px 0 0 5px;' /></a>
			</div>
		";
	}

	//Get side lengths HTML
	public function get_side_lengths_html($side) {

		$lengths = request('side_lengths['.$side.']');
		if ($lengths and is_array($lengths)) {
			$nr_lengths = count($lengths);
		}
		else {
			$nr_lengths = 1;
		}

		//VK length
		if ($this->vk_length and $side == 1 and $nr_lengths = 1) {
			$lengths[0] = $this->vk_length;
		}

		$lengths_html = '';
		for ($l = 1; $l <= $nr_lengths; $l++) {
			$val = isset($lengths[$l-1]) ? $lengths[$l-1] : '';
			$lengths_html .= "
				<tr>
					<td width='140' height='22' align='left' valign='middle'>Lengte".($l > 1 ? ' '.$l : '').":</td>
					<td width='15' align='left' valign='middle'>*</td>
					<td width='180' align='left' valign='middle'><input type='text' class='side_lengths formMed' name='side_lengths[$side][]' value ='$val' onkeyup='update_plasitor();calc_price(\"Changed side length\");'/></td>
					<td align='left' valign='top'><p style='padding:4px 10px 0 0; float:left;'>m</p></td>
				</tr>
			";
		}

		return $lengths_html;
	}

	//Get color selection HTML
	public function get_colors_html() {

		//Globals
		global $db_table_colors, $db_table_products;

		//Get available colors
		$available_colors = array();
		$res_c = eq("	SELECT $db_table_colors.name, $db_table_colors.id FROM $db_table_colors
						LEFT JOIN $db_table_products ON ($db_table_colors.id = color_id)
						WHERE $db_table_products.active = '1' AND category_id = '$this->category_id' AND height = '$this->height'
						GROUP BY color_id
						ORDER BY $db_table_colors.name ASC
		");
		while ($color = mfo($res_c)) {
			$available_colors[$color->id] = $color->name;
		}

		//Request?
		$color_req = request('color');

		//Colors HTML
		$color_selector = '';
		if (count($available_colors)) {

			//More than one color
			$colors = array();
			$c = 0;
			foreach ($available_colors as $color_id => $color_name) {

				if ($color_req) {
					$checked = $color_id != $color_req ? '' : "checked='checked'";
				}
				else {
					$checked = isset($checked) ? '' : "checked='checked'";
				}

				$c++;
				$available_colors[$color_id] = "
					<div class='kleurstaal'>
						<label for='kleur_$c'>
							<img src='gfx/colors/color_$color_id.jpg' alt='$color_name' title='$color_name' />
							<input type='radio' class='color' name='color' id='kleur_$c' value='$color_id' $checked onclick='calc_price(\"Changed colors\");'/> <span>$color_name</span>
						</label>
					</div>
				";
			}

			$color_selector = implode("\n",$available_colors);
		}
		/*elseif (count($available_colors)) {

			//Just one color
			$color_id 	= 	key($available_colors);
			$color_name	=	array_pop($available_colors);

			$color_selector = "
				<div class='blok'>
					<h2>Kleur van het gaas</h2>
					<div class='table'>
						<div class='kleurstaal'>
							<img src='gfx/colors/color_$color_id.jpg' alt='$color_name' title='$color_name' onload='javascript: calc_price();'/>
							<input type='hidden' id='color' name='color' value='$color_id' /> <span>$color_name</span>
						</div>
					</div>
					<div class='subtableDivider'>&nbsp;</div>
				</div>
			";
		}*/
		else {

			//No colors
			$color_selector = "Geen kleuren gevonden.";
		}

		return $color_selector;
	}

	//Get gates HTML for a certain side
	public function get_gates_html($side) {

		//Gate categories
		global $gate_categories, $db_table_products, $db_table_cats;
		$gate_categories = isset($gate_categories[$this->category_id]) ? $gate_categories[$this->category_id] : array();

		//Get available gates
		$found_gates = 0;
		$gates_html = "<h3>Zijde $side: Kies een poort</h3><div class='table'>";
		foreach($gate_categories as $gate_category_id) {

			//Add the HTML
			$gate = $this->get_gate_html($side, $gate_category_id);
			if ($gate) {
				$found_gates++;
				$gates_html .= $gate;
			}
		}

		if (!$found_gates) {
			$gates_html .= "Er zijn geen poorten gevonden voor de door u opgegeven hoogte. ";
		}

		$gates_html .= "</div><div class='subtableDivider'></div>";
		return $gates_html;
	}

	public function get_gate_html($side, $gate_category_id, $width = 0, $with_div = true) {

		global $db_table_products, $db_table_cats, $_sep, $gfx_folder, $cat_prefix, $gate_height_map, $vat_multiplier, $db_table_colors;

		//Must have a height, and matching gate heights
		if (!$this->height or empty($gate_height_map[$gate_category_id][$this->height])) {
			return '';
		}

		$res_g = eq("	SELECT 	$db_table_products.id, name, menu_name, description, width, height,
								normal_price, price, sale_price, on_sale, price_unit, color_id
						FROM 	$db_table_products
						LEFT JOIN $db_table_cats ON (category_id = $db_table_cats.id)
						WHERE 	category_id = '$gate_category_id' AND $db_table_products.active='1'
		");

		if (!mnr($res_g)) {
			return '';
		}

		//Width requested instead?
		if (!$width) {
			$width 	= 	request('side_gates_widths['.$side.']',0);
			$val	=	request('side_gates_amounts['.$side.']');
		}
		else {
			$val = '';
		}

		$widths = array();
		$gate_id = 0;
		$gates = 0;
		$color_gate = 0;
		$last_color = 0;
		while ($gate = mfo($res_g)) {

			//Height must match
			if ($gate_height_map[$gate_category_id][$this->height] != $gate->height) {
				continue;
			}

			$gates++;

			//Add to widths selectbox
			$widths[$gate->width] = $gate->width.' mm';

			//No specific width specified? Use the first gate as default
			if (!$width) {
				$width = $gate->width;
			}

			//No color specified yet? Try to find a match
			if (!$color_gate and $gate->color_id == $this->color) {
				$color_gate = $gate->color_id;
			}
			$last_color = $gate->color_id;

			//Set some props
			if ($width == $gate->width) {
				$gate_name 			= 	$gate->menu_name ? $gate->menu_name : $gate->name;
				$gate_image_url		=	$gfx_folder.$_sep.$cat_prefix.$gate_category_id.'.jpg';
				$gate_id			=	$gate->id;
				$gate_description	=	$gate->description;
				$gate_price 		=	parseAmount($gate->on_sale ? $gate->sale_price*$vat_multiplier : $gate->price*$vat_multiplier, '&euro; ');
				$gate_normal_price 	= 	parseAmount($gate->normal_price*$vat_multiplier, '&euro; ');
				$gate_price_unit	=	$gate->price_unit;
				$gate_height		=	$gate->height;
			}
		}

		//No matching color found? Use last color
		if (!$color_gate) {
			$color_gate = $last_color;
		}

		//Get color name
		$color_name = getMyo($db_table_colors, $color_gate, 'name');

		//No matching gates?
		if (!$gates) {
			return '';
		}

		$gate_html =  $with_div ? "<div class='poort' id='gate_{$side}_$gate_category_id'>" : '';
		$gate_html .= "
					<input type='hidden' class='side_gates' name='side_gates[$side][]' value='$gate_id' />
					<div class='imgLeft'><img src='$gate_image_url' alt='$gate_name' width='125px'/></div>
					<div class='poortInfo'>
						<strong>$gate_name</strong>
						<div class='info' id='infoPoort_{$side}_$gate_id' onmouseover=\"displayDiv('infoPoort_{$side}_$gate_id', 1);\" onmouseout=\"displayDiv('infoPoort_{$side}_$gate_id', 0);\" style='display:none;'>
							<h2>$gate_name</h2>
							$gate_description
						</div>
						<a onmouseover=\"displayDiv('infoPoort_{$side}_$gate_id', 1);\" onmouseout=\"displayDiv('infoPoort_{$side}_$gate_id', 0);\" class='infoIcon'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:3px 0 0;' /></a>
						<br /><br />
						<input type='hidden' name='side_gates_colors[$side][]' class='side_gates_colors' id='gate_color_{$side}_$gate_id' value='$color_gate'/>
						<table cellpadding='0' cellspacing='0' border='0'>
							<tr>
								<td width='86' height='22' align='left' valign='middle'>Aantal:</td>
								<td width='60' align='left' valign='middle'><input type='text' name='side_gates_amounts[$side][]' class='formSmall gate_amount_{$side} side_gates_amounts' onkeyup='changed_gate_amount($side);' value='$val'/></td>
							</tr>
							<tr>
								<td height='22' align='left' valign='middle'>Breedte:</td>
								<td width='60' align='left' valign='middle'>".makeSelectBox('side_gates_widths['.$side.'][]', $widths, $width, 0,
																			"class='selectSmall side_gates_widths' onchange='changed_gate_width($side, $gate_category_id, this.value);'")."</td>
							</tr>
							<tr>
								<td height='22' align='left' valign='middle'>Kleur:</td>
								<td width='60' align='left' valign='middle'><a href='javascript: void(0);' onclick='choose_gate_color($side, $gate_category_id, $gate_id);' id='color_link_{$side}_$gate_id'>$color_name</a></td>
							</tr>
							<tr>
								<td height='22' align='left' valign='middle'>Hoogte:</td>
								<td width='60' align='left' valign='middle'>$gate_height mm</td>
							</tr>
							<tr>
								<td height='22' align='left' valign='middle'>Normaal:</td>
								<td width='60' align='left' valign='middle'>$gate_normal_price p/st</td>
							</tr>
							<tr>
								<td height='22' align='left' valign='middle'>Voor:</td>
								<td width='60' align='left' valign='middle'>$gate_price p/st</td>
							</tr>
						</table>
					</div>
		";
		$gate_html .=  $with_div ? "</div>" : '';
		return $gate_html;
	}

	public function update_price($height, $color, $poles, $nr_corners, $nr_sides, $return_xml = false) {

		global $db_table_products, $db_table_colors;

		//Get the required parameters
		$height				=	(int) $height;
		$color				=	(int) $color;
		$poles 				= 	$poles;
		$nr_corners 		= 	(int) $nr_corners;
		$nr_sides 			= 	(int) $nr_sides;

		//Identifier (based on the unique properties combination)
		$identifier = md5($category_id.$height.$color);

		//Price determination
		$res_p = eq("	SELECT price,sale_price,on_sale,step_size FROM $db_table_products
						WHERE height='$height'  AND color_id='$color'  AND active = '1' AND category_id = '$category_id' LIMIT 1;");
		if ($product = mfo($res_p)) {
			$price 	= 	$product->on_sale ? $product->sale_price : $product->price;
			$step	=	$product->step_size;
		}

		else {
			$dont_add = true;
			return;
		}

		//For each side, get the number of gates, or just the length
		$side_lengths 			= request('side_lengths');
		$side_gates 			= request('side_gates');
		$side_gates_amounts		= request('side_gates_amounts');
		$side_gates_widths 		= request('side_gates_widths');
		$side_gates_colors 		= request('side_gates_colors');
		$price_gate 			= 0;
		$total_length 			= 0;

		for ($i = 1; $i <= $nr_sides; $i++) {

			//Did we select gates
			if (isset($side_gates[$i]) and count($side_gates[$i]) > 1) {

				//Algoritm to get the data of each gate
				$gates 		= array();
				$lengths 	= array();
				for ($j = 0; $j < count($side_gates[$i]); $j++) {

					//No amount selected for this gate?
					if ($side_gates_amounts[$i][$j] < 1) {
						continue;
					}

					$gate_id 	= $side_gates[$i][$j]; 				//This is actually the id of the gate category
					$amount 	= $side_gates_amounts[$i][$j];
					$width 		= $side_gates_widths[$i][$j];
					$color 		= $side_gates_colors[$i][$j];

					$res_g = eq("	SELECT 		price, sale_price, on_sale FROM $db_table_products
									WHERE 		width='$width' AND color_id='$color' AND category_id='$gate_id' AND active='1'
					;");

					if (mnr($res_g)) {
						$gate = mfo($res_g);
						$price_gate += ($gate->on_sale ? $gate->sale_price : $gate->price) *$amount;
					}
				}

				for ($k = 0; $k < count($side_lengths[$i]); $k++) {

					//Update total length
					$total_length += $side_lengths[$i][$k];
				}
			}

			//No gates selected, just take the length of this side
			else {

				//Update total length
				$total_length += $side_lengths[$i][0];
			}
		}

		//Determine the number of rolls needed
		$rolls = $total_length / $step;

		//If a pole has been selected
		$price_pole = 0;
		if ($poles) {
			$pole_id = (int) request('pole');
			$res_p = eq("	SELECT 		price, sale_price, on_sale FROM $db_table_products
							WHERE 		id='$pole_id' AND active='1'
			;");

			if (mnr($res_p)) {
				$pole = mfo($res_p);
				$price_pole = $pole->on_sale ? $pole->sale_price : $pole->price;
			}

			//Poles have been selected so we also need to include the cliptang :')
			if ($cliptang = getMyo($db_table_products, CLIPTANG_CATEGORY_ID, array('price','sale_price','on_sale'))) {

				//Update the price
				$price_pole += ($cliptang->on_sale ? $cliptang->sale_price : $cliptang->price);
			}

			//Calculate needed poles
			$needed_poles = ceil(($total_length / 2.5) + 1);

			//Amount of needed clips
			$needed_clips = $needed_poles * CLIPS_PER_POLE;

			//Add the price of the clips to the pole price
			if ($clips = getMyo($db_table_products, CLIPS_CATEGORY_ID, array('price','sale_price','on_sale'))) {

				//How many bags do we need?
				$needed_bags 	= ceil($needed_clips / CLIPS_PER_BAG);

				//Price of each bag
				$price_per_bag 	= $clips->on_sale ? $clips->sale_price : $clips->price;

				//Update price
				$price_pole += $price_per_bag * $needed_bags;
			}
		}

		//Price is per roll, so determine the amount of rolls needed based on the length input
		$price = $price * ($rolls = ceil($total_length/$step));

		$price = $price + $price_pole + $price_gate;

		if ($return_xml) {
			return "";
		}
	}

	//Get the colors HTML
	public function get_gate_colors_html($side, $gate_category_id, $gate_id) {

		//Globals
		global $db_table_colors, $db_table_products;

		//Get available colors
		$available_colors = array();
		$res_c = eq("	SELECT $db_table_colors.name, $db_table_colors.id FROM $db_table_colors
						LEFT JOIN $db_table_products ON ($db_table_colors.id = color_id)
						WHERE $db_table_products.category_id = '$gate_category_id'
						ORDER BY $db_table_colors.name ASC
		");
		while ($color = mfo($res_c)) {
			$available_colors[$color->id] = $color->name;
		}

		//Colors HTML
		$color_selector = '';
		unset($checked);
		if (count($available_colors)) {

			$colors = array();
			$c = 0;
			foreach ($available_colors as $color_id => $color_name) {
				$checked = isset($checked) ? '' : "checked='checked'";
				$c++;
				$available_colors[$color_id] = "
					<div class='kleurstaal'>
						<a href='javascript: void(0);' onclick='pick_color($side, $gate_id, $color_id, \"$color_name\");'>
							<img src='gfx/colors/color_$color_id.jpg' alt='$color_name' title='$color_name' />
							<p>$color_name</p>
						</a>
					</div>
				";
			}

			$color_selector = implode("\n",$available_colors);
		}
		else {
			$color_selector = "Geen kleuren gevonden.";
		}

		return $color_selector;
	}

	public function set_height($height) {
		$this->height = (int) $height;
	}

	public function set_color($color) {
		$this->color = (int) $color;
	}

	public function set_poles($poles) {
		$this->poles = (bool) $poles;
	}

	public function set_mural($mural) {
		$this->mural = (int) $mural;
	}

	public function set_corners($corners) {

		//Validate the amount of corners
		$this->corners = (int) $corners;
		if ($this->corners < 0) {
			$this->corners = 0;
		}
		if ($this->corners > 4) {
			$this->corners = 4;
		}

		//Set the default amount of sides for this amount of corners
		$this->set_sides();
	}

	public function set_sides($sides = 1) {

		//Validate the amount of sides
		$this->sides = (int) $sides;
		if ($this->sides < 1) {
			$this->sides = 1;
		}
		if ($this->sides > 5) {
			$this->sides = 5;
		}

		//Validate against amount of corners
		if ($this->corners < 3) {
			$this->sides = $this->corners + 1;
		}
		elseif (($this->corners == 3) and $this->sides != 3 and $this->sides != 4) {
			$this->sides = 4;
		}
		elseif (($this->corners == 4) and $this->sides != 4 and $this->sides != 5) {
			$this->sides = 4;
		}
	}

	public function get_height() {
		return $this->height;
	}

	public function get_color() {
		return $this->color;
	}

	public function get_poles() {
		return $this->poles;
	}

	public function get_corners() {
		return $this->corners;
	}

	public function get_sides() {
		return $this->sides;
	}

	public function get_mural() {
		return $this->mural;
	}
}

?>