<?php
/*
Title:		Message handling class
File: 		classes/class_Message.php
Version: 	v2.03
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

define('MSG',			0);
define('WARNING',		1);
define('CRITICAL',		2);
define('CONFIRM',		3);
define('CONFIRM_DATE',	4);
define('CONFIRM_TEXT',	5);

class Message {
	
	private $msg, $level;
	private $type, $db_id, $db_ids, $confirm_name, $confirm_date, $link_app;
	
	function Message() {
		$this->msg 		= 	array();
		$this->level 	= 	array();		
		$this->db_id	=	0;
		$this->ts		=	time();
		$this->text		=	'';
	}
	
	function clear() {
		$this->msg 		= 	array();
		$this->level 	= 	array();		
	}
	
	function set($msg = '', $level = MSG) {
		$this->add($msg, $level);
	}

	function add($msg = '', $level = MSG) {
		$this->msg[]	=	$msg;
		$this->level[]	=	$level;
	}
	
	function setConfirm($msg, $type, $db_id, $db_name_field = '', $manual_name = '', $link_appendix = '') {
		
		if (empty($link_appendix)) {
			global $_app;
			$link_appendix = $_app;
		}
		
		if (!empty($db_name_field) and is_int($db_id)) {
			global $db_table;
			$this->confirm_name = ' &raquo; '.getMyo($db_table, $db_id, $db_name_field);
		}
		elseif (!empty($manual_name)) {
			$this->confirm_name = $manual_name;
		}
		else {
			$this->confirm_name = $db_id;
		}
		
		$this->msg 			= 	$msg;
		$this->level		=	CONFIRM;		
		$this->type			=	$type;
		$this->db_id		=	$db_id;
		$this->link_app		=	$link_appendix;
	} 
	
	function setConfirmMultiple($msg, $type, $db_ids_array, $db_name_field = '', $manual_names = '', $link_appendix = '') {
		
		if (empty($link_appendix)) {
			global $_app;
			$link_appendix = $_app;
		}
		
		//Reset to string
		$this->db_id = '1';
		
		//Loopo array elements
		foreach($db_ids_array as $db_id) {
			if (!empty($db_name_field)) {
				global $db_table;
				$this->confirm_name[] = ' &raquo; '.getMyo($db_table, $db_id, $db_name_field);	
			}
			elseif (isset($manual_names[$db_id])) {
				$this->confirm_name[] = $manual_names[$db_id];
			}
			$this->db_id	.=	'&'.$type.'_confirm[]='.$db_id;
		}
		
		$this->msg 		= 	$msg;
		$this->type		=	'multiple';
		$this->level	=	CONFIRM;
		$this->link_app	=	$link_appendix;		
	}
	
	function setConfirmDate($msg, $type, $db_id, $link_appendix = '', $timestamp = 0) {
		
		global $_file;
		if (empty($link_appendix)) {
			global $_app;
			$link_appendix = $_app;
		}		

		$this->msg 		= 	$msg;
		$this->type		=	$type;
		$this->db_id	=	$db_id;
		$this->level	=	CONFIRM_DATE;
		$this->link_app	=	$link_appendix;
		$this->ts		=	$timestamp ? $timestamp : time();
	}
	
	function setConfirmText($msg, $type, $db_id, $link_appendix = '', $text = '') {
		
		global $_file;
		if (empty($link_appendix)) {
			global $_app;
			$link_appendix = $_app;
		}		

		$this->msg 		= 	$msg;
		$this->type		=	$type;
		$this->db_id	=	$db_id;
		$this->level	=	CONFIRM_TEXT;
		$this->link_app	=	$link_appendix;
		$this->text		=	$text;
	}

	function get() {
		
		if (empty($this->msg)) {
			return '';
		}
		
		if (is_array($this->msg)) {
			$html = array();
			foreach ($this->msg as $key => $msg) {
				$html[]	=	$this->getMessageHTML($msg, $this->level[$key]);	
			}
			$html = implode("\n",$html);
		}
		else {
			$html = $this->getMessageHTML($this->msg, $this->level);
		}
		
		return $html;
	}
	
	function getMessageHTML($msg, $level) {
		
		switch($level) {
			case CONFIRM_TEXT:
				return $this->confirmText($msg);
				exit;
			case CONFIRM_DATE:
				return $this->confirmDate($msg);
				exit;
			case CONFIRM:
				return $this->confirmMessage($msg);
				exit;
			case CRITICAL:
				return $this->errorMessage($msg);
				break;
			case WARNING:
				return $this->warningMessage($msg);
				break;
			case MSG:
				return $this->successMessage($msg);
				break;
		}		
	}
	
	function successMessage($msg) {
		
		if ($msg)
		return "
			<div class='message'>
				<b>".TXT_OK."</b>
				<br/><br/>
				$msg
			</div>
		";
		return '';
	}
	
	function warningMessage($msg) {
		
		if ($msg)
		return "
			<div class='warning'>
				<b>".TXT_WARNING.":</b>
				<br/><br/>
				$msg
			</div>
		";
		return '';		
	}
	
	function errorMessage($msg) {
		
		if ($msg)
		return "
			<div class='error'>
				<b>".TXT_ERROR.":</b>
				<br/><br/>
				$msg
			</div>
		";
		return '';
	}
	
	function confirmMessage($msg) {
		
		global $_file;
		
		if (!empty($this->confirm_name)) {
			if (is_array($this->confirm_name)) {
				$this->confirm_name = implode("\n<br/>", $this->confirm_name);
			}
			$this->confirm_name = "<br/><br/><b>$this->confirm_name</b>";
		}
		
		if ($msg)
		return "
			<div class='warning'>
				<b>".TXT_CONFIRMATION.":</b>
				<br/><br/>
				$this->msg
				$this->confirm_name
				<br/><br/><a href='$_file?{$this->type}_confirm=$this->db_id&$this->link_app'>".BUTTON_CONFIRM."</a>
				- <a href='$_file?$this->link_app'>".BUTTON_CANCEL."</a>
			</div>
		";
		return '';		
	}
	
	function confirmDate($msg) {
		
		global $_file, $page_and_sort_inputs;
		
		if (!empty($this->confirm_name)) {
			if (is_array($this->confirm_name)) {
				$this->confirm_name = implode("\n<br/>", $this->confirm_name);
			}
			$this->confirm_name = "<br/><br/><b>$this->confirm_name</b>";
		}
		
		if ($msg)
		return "
			<div class='warning'>
				<b>".TXT_CONFIRMATION.":</b>
				<br/><br/>
				$this->msg
				<br/><br/>
				<form name='confirm_date_form' method='post' action='$_file'>
				$page_and_sort_inputs
				<input type='hidden' name='confirm_$this->type' value='$this->db_id'/>
	<input type='text' readonly='readonly' name='date_value' id='date_value' class='input_calendar' value='".date('d/m/Y', $this->ts)."' />
	<button class='calendar_button' type='reset' id='date_value_trigger'>...</button>
				</form>
				
				<script type='text/javascript'>
					Calendar.setup({
				        inputField     :    'date_value',      		// id of the input field
				        button         :    'date_value_trigger'   	// trigger for the calendar (button ID)
				    });	
		        </script>
				
				<br/><br/><a href='JavaScript: void(0);' onclick='document.confirm_date_form.submit();'>".BUTTON_CONFIRM."</a>
				- <a href='$_file?$this->link_app'>".BUTTON_CANCEL."</a>
			</div>
		";
		return '';		
	}
	
	function confirmText($msg) {
		
		global $_file, $page_and_sort_inputs;
		
		if (!empty($this->confirm_name)) {
			if (is_array($this->confirm_name)) {
				$this->confirm_name = implode("\n<br/>", $this->confirm_name);
			}
			$this->confirm_name = "<br/><br/><b>$this->confirm_name</b>";
		}
		
		if ($msg)
		return "
			<div class='warning'>
				<b>".TXT_CONFIRMATION.":</b>
				<br/><br/>
				$this->msg
				<br/><br/>
				<form name='confirm_text_form' method='post' action='$_file'>
				$page_and_sort_inputs
				<input type='hidden' name='confirm_$this->type' value='$this->db_id'/>
				<input type='text' name='text_value' class='input_regular' value='$this->text' />
				</form>
								
				<br/><br/><a href='JavaScript: void(0);' onclick='document.confirm_text_form.submit();'>".BUTTON_CONFIRM."</a>
				- <a href='$_file?$this->link_app'>".BUTTON_CANCEL."</a>
			</div>
		";
		return '';		
	}
}
?>