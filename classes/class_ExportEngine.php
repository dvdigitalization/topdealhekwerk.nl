<?php
/*
Title:		Export engine class
File: 		classes/class_ExportEngine.php
Version: 	v2.02
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

// define some messafes
define ('NO_ARRAY_GIVEN','De tweede input van ExportEgine moet een array zijn');

class ExportEngine {

	// the database table that will be exported
	private $db_table;

	// the fields of the database table that need to be included in the export document
	private $fields;

	// the type of export file we will be using, such as XML or CSV
	private $file_type;

	// placeholder for all the data
	private $data;

	// error message placeholder
	private $error;

	// xml root, like 'customers'
	private $xml_root;

	// data name, like 'customer'
	private $data_name;

	// xml output
	private $xml_output;

	// csv output
	private $csv_output;

	// csv delimiter
	private $csv_delimiter;

	// if specific rows have to be exported
	private $rows;

	// constructor
	function ExportEngine($db_table, $fields, $file_type = 'XML', $xml_root = '', $data_name = 'no_name', $csv_delimiter = ';', $rows = '') {

		// make sure that $fields is an array
		if (!is_array($fields)) {
			$error = NO_ARRAY_GIVEN;
			return;
		}

		$this->db_table 		= $db_table;
		$this->fields 			= $fields;
		$this->file_type 		= $file_type;
		$this->xml_root 		= $xml_root;
		$this->data_name 		= $data_name;
		$this->csv_delimiter	= $csv_delimiter;
		$this->rows 			= $rows;

		$this->data 			= array();
	}

	// put all fields in an array
	function fetch_data() {
		$i=0;
		$res = eq("SELECT ".implode(',',$this->fields)." FROM $this->db_table;");
		while ($myr = mfa($res)) {
			foreach ($myr as $key=>$value) {
				if (!is_numeric($key) and $key != '') {
					$this->data[$i][$key] = $value;
				}
			}
			$i++;
		}
	}

	// make an XML output file
	function create_xml_file() {

		// xml declaration and xml root identifier
		$this->xml_output = "<?xml version=\"1.0\"?>\n<$this->xml_root>\n";

		// each data object is now read and injected
		foreach ($this->data as $row=>$data) {

			// data object name
			$this->xml_output .= "\t<$this->data_name>\n";

			// each field
			foreach ($data as $key=>$value) {
				$this->xml_output .= "\t\t<$key>$value</$key>\n";
			}

			// data object close
			$this->xml_output .= "\t</customer>\n";
		}

		// close xml root
		$this->xml_output .= "</$this->xml_root>";
	}

	// make a CSV output file
	function create_csv_file() {

		// initialize csv output
		$this->csv_output = implode($this->csv_delimiter,$this->fields)."\n";

		// read all data rows
		foreach ($this->data as $row=>$data) {
			$this->csv_output .= implode($this->csv_delimiter,$data)."\n";
		}
	}

	// get the xml data
	function get_xml_output() {
		return $this->xml_output;
	}

	// get the csv data
	function get_csv_output() {
		return $this->csv_output;
	}

	// TEMP FUNCTION
	function print2() {
		return $this->xml_output;
	}
}


?>