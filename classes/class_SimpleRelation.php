<?php
/*
Title:		Simple relation
File: 		classes/class_SimpleRelation.php
Version: 	v1.75
Author:		Digitalization
Contact:	info@digitalization.nl
*/

class SimpleRelation {
	
	private $db_table, $id, $item_a_id, $item_b_id;
	
	// *** Instantiate a new SimpleRelation
	function SimpleRelation($id = 0) {
		
		global $db_table;
		
		$this->id 			= 	(int) $id;
		$this->db_table 	= 	$db_table;
		
		// Build this item
		if ($this->id) {
			$this->readFromDb();
		}	  			
	}
	
	// *** Read values from Db
	function readFromDb() {

		global $field_a, $field_b;

		$res = eq("SELECT $field_a AS item_a_id, $field_b AS item_b_id FROM $this->db_table WHERE id='$this->id';");
		$myo = mfo($res);
		$this->item_a_id 	= $myo->item_a_id;
		$this->item_b_id  	= $myo->item_b_id;
	}
	
	// *** Read post vars, if there are any
	function readPostVars() {
		$this->item_a_id 	= $_POST['item_a_id'];
		$this->item_b_id  	= $_POST['item_b_id'];
	}
	
	// *** Get the items
	function getItemA() {
		return $this->item_a_id;	
	}
	
	function getItemB() {
		return $this->item_b_id;	
	}
		
	// *** Save this item
	function save() {
		
		global $field_a, $field_b;
		
		//Check for duplicates
		$res = eq("	SELECT id FROM $this->db_table
					WHERE $field_a = '$this->item_a_id' AND $field_b = '$this->item_b_id' AND id <> '$this->id'
		;");
		if (mnr($res)) {
			return false;
		}
		
		if ($this->id) {
			upRec($this->db_table,$this->id, 	array($field_a,$field_b), array($this->item_a_id, $this->item_b_id));
		}
		else {
			$this->id =	insRec($this->db_table, array($field_a,$field_b), array($this->item_a_id, $this->item_b_id));	
		}
		return true;
	}
	
	// *** Delete this item
	function delete() {
		
		global $dependent_items, $dependent_tables;
		
		delRec($this->db_table, $this->id);
		return true;
	}
}    
?>