<?php
/*
Title:		AJAX total price getter
File: 		ajaxhond.php
Version: 	v2.20
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2009 by Digitalization
*/

/******************************************************************************************
 * Settings
 ***/

//Required files
require_once('inc/site_ajax_header.php');

//The categories we'll display, provide their ID's
define('CATEGORY_PANEEL',			1);
define('CATEGORY_GAAS',				13);
define('CATEGORY_PLASITOR_GAAS',	25);
define('CATEGORY_DIER_TUIN_GAAS',	20);
define('CATEGORY_POORTEN',			27);
define('CATEGORY_ACCESSOIRES',		36);
define('CATEGORY_DUBBELSTAAF',		97);

//Cart files
$cart_files				=	array(	CATEGORY_PANEEL			=>	'cart_paneelsystemen.php',
									CATEGORY_GAAS			=>	'cart_gaas.php',
									CATEGORY_POORTEN		=>	'cart_toegangspoorten.php',
									CATEGORY_PLASITOR_GAAS	=>	'cart_plasitor_gaas.php',
									CATEGORY_DIER_TUIN_GAAS	=>	'cart_dier_tuin_gaas.php',
									CATEGORY_ACCESSOIRES	=>	'cart_accessoires.php',
									CATEGORY_DUBBELSTAAF	=>	'cart_dubbelstaaf.php');

/******************************************************************************************
 * Request processing
 ***/

if (isset($_GET['ajax_calc_price']) and isset($_GET['category_id']) and $category_id = (int) $_GET['category_id']) {

	//Define invalid price
	define('INVALID_PRICE',		999999);

	//Read some info from database
	$category = getMyo($db_table_cats,$category_id,
				array('active','menu_name','name','description','sub_cat_of','price_unit','manual','delivery_days'));

	//Validate category
	if ($category->active and $category->sub_cat_of and isset($cart_files[$category->sub_cat_of])) {

		//Data cleansing
		foreach (array_keys($_POST) as $key) {
			evalAll($_POST[$key]);
		}

		//Get item name
		$c_name	= $category->menu_name ? $category->menu_name : $category->name;

		//Cart file
		include('inc/common/'.$cart_files[$category->sub_cat_of]);
	}
}
?>