<?php
/*
Title:		Newsletter subscribe / unsubscribe page
File: 		newsletter.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

/******************************************************************************************
 * Settings
 ***/

//Page sets and HTML name
$page_set 		= 	array('newsletter','spam');
$html_name		=	'nieuwsbrief.html';

//Required files
require_once('inc/site_header.php');

//Crumb
$crumbs[$html_name] = 'Nieuwsbrief';

//Database table
$db_table 				= 	DIGI_DB_PREFIX.'newsletter_subscribers';

//Initial values
$show_form				=	true;
$fields_missing			=	false;
$form_html				=	'';

/******************************************************************************************
 * POST and GET cases
 ***/

if (isset($_POST['signup']) or isset($_POST['signoff'])) {

	//Clean contents
	foreach(array_keys($_POST) as $key) {
		evalAll($_POST[$key]);
	}
}

if (isset($_POST['signup'])) {

	//Load spam shields
	$spam_check_string = $_POST['first_name'];
	require(SITE_PATH_PREFIX.'inc/spamshields.php');

	//IP blocked
	if ($ip_blocked) {
		$error 		= IP_IS_BLOCKED;
		$show_form 	= false;
	}

	//Very bad word found
	elseif ($very_bad_word_found) {
	 	$error 		= str_replace('[BAD_WORD]',$very_bad_word_found, BAD_WORD).'<br/><br/>'.NO_TOLERANCE_WORD;
		$show_form 	= false;
	}

	//Bad word found
	elseif ($bad_word_found) {
		$error 		= str_replace('[BAD_WORD]',$bad_word_found, BAD_WORD);
		$show_form 	= true;
	}

	//Check for required fields
	elseif (empty($_POST['first_name']) or empty($_POST['last_name']) or empty($_POST['signup_email'])) {
		$error 		= NL_FIELDS_MISSING;
		$show_form 	= true;
	}

	//Email validation failed
	elseif (!$email = validateEmail($_POST['signup_email'])) {
		$error 		= NL_INVALID_EMAIL;
		$show_form 	= true;
	}

	//Success
	else {

		$res = eq("SELECT id,newsletter FROM $db_table WHERE email = '$email';");
	    if (mysql_num_rows($res)) {

	        //Email address exists
	        $myo = mfo($res);

	        //Already receives newsletter
	        if ($myo->newsletter) {
				$msg = NL_ALREADY_RECEIVES;
			}

			//Activate
	        else {
	            upRec($db_table,$myo->id,'newsletter',1);
	            $msg = NL_SUBSCRIBED;
	        }
	    }
	    else {

	    	//Insert into database
	        insRec($db_table,	array('first_name','last_name','language','email','newsletter'),
								array($_POST['first_name'],$_POST['last_name'],$_lang,$email,1));
			$msg = NL_SUBSCRIBED;
			
			//Sign up via Muskito mail
			require_once('inc/muskito.php');
			muskito_api('optin', array('first_name' => $_POST['first_name'],'last_name' => $_POST['last_name'], 'email' => $email));
	    }

	    $show_form 	= false;
	    $_POST 		= array();
	}
}

if (isset($_POST['signoff'])) {

	//Need to fill in email
	if (empty($_POST['signoff_email'])) {
		$error 	= NL_FIELDS_MISSING;
	}

	//Email validation failed
	elseif (!$email = validateEmail($_POST['signoff_email'])) {
		$error 	= NL_INVALID_EMAIL;
	}

	//All ok
	else {
		
		//Sign out via Muskito mail
		require_once('inc/muskito.php');
		$xml = muskito_api('optout', array('email' => $email));
		
		//Fout?
		$non_exist = false;
		if (empty($xml['status'])) {
			if (!empty($xml['error']) and $xml['error'] == 121) {
				$non_exist = true;
			}
		}

		$res = eq("SELECT id FROM $db_table WHERE email = '$email';");
		if (mysql_num_rows($res)) {
			$myo = mfo($res);
	        upRec($db_table,$myo->id,'newsletter',0);
		}
		
		//Doesn't exist?
	    if ($non_exist or !mysql_num_rows($res)) {
			$msg = NL_EMAIL_NOT_FOUND;
		}

	    else {
	        $msg = NL_UNSUBSCRIBED;	        
	        $_POST = array();
	        $show_form = false;
	    }
	}
}

/******************************************************************************************
 * Page contents
 ***/

//Error or message?
$error 	= isset($error) ? "<p>&nbsp;</p><p class='error_message'>$error</p>" : '';
$msg    = isset($msg) 	? "<p>&nbsp;</p><p class='message'>$msg</p>" : '';

//Do we show the form?
if ($show_form)  {

	$form_html = "
	<p>&nbsp;</p>
	<p>".NL_SIGNUP."</p>
	<p>&nbsp;</p>
	<div class='table'>
		<form name='newsletter_subscribe' method='post' action='".$_file."'>
		<table width='100%' cellpadding='0' cellspacing='0' border='0'>
			<tr>
				<td width='140' height='22' align='left' valign='middle'>Voornaam:</td>
				<td width='15' align='left' valign='middle'>*</td>
				<td align='left' valign='middle'><input type='text' name='first_name' value='".request('first_name')."' class='formMed' /></td>
			</tr>
			<tr>
				<td width='140' height='22' align='left' valign='middle'>Achternaam:</td>
				<td width='15' align='left' valign='middle'>*</td>
				<td align='left' valign='middle'><input type='text' name='last_name' value='".request('last_name')."' class='formMed' /></td>
			</tr>
			<tr>
				<td width='140' height='22' align='left' valign='middle'>E-mail:</td>
				<td width='15' align='left' valign='middle'>*</td>
				<td align='left' valign='middle'><input type='text' name='signup_email' value='".request('signup_email')."' class='formMed' /></td>
			</tr>
			<tr>
				<td width='140' height='22' align='left' valign='middle'></td>
				<td width='15' align='left' valign='middle'>&nbsp;</td>
				<td align='left' valign='middle'>* Verplichte velden</td>
			</tr>
			<tr>
				<td height='22' align='left' valign='bottom' colspan='3'>
	     			<div class='divider'>
						<div class='verstuurBtn'><input type='submit' name='signup' class='verstuurBtn' value=''/></div>
	   				</div>
				</td>
			</tr>
		</table>
		</form>
	</div>";

	$form_html .= "
	<p>&nbsp;</p>
	<p>".NL_SIGNOFF."</p>
	<p>&nbsp;</p>
	<div class='table'>
		<form name='newsletter_unsubscribe' method='post' action='".$_file."'>
		<table width='100%' cellpadding='0' cellspacing='0' border='0'>
			<tr>
				<td width='140' height='22' align='left' valign='middle'>E-mail:</td>
				<td width='15' align='left' valign='middle'>*</td>
				<td align='left' valign='middle'><input type='text' name='signoff_email' value='".request('signoff_email')."' class='formMed' /></td>
			</tr>
			<tr>
				<td width='140' height='22' align='left' valign='middle'></td>
				<td width='15' align='left' valign='middle'>&nbsp;</td>
				<td align='left' valign='middle'>* Verplichte velden</td>
			</tr>
			<tr>
				<td height='22' align='left' valign='bottom' colspan='3'>
					<div class='divider'>
						<div class='verstuurBtn'><input type='submit' name='signoff' class='verstuurBtn' value=''/></div>
					</div>
				</td>
			</tr>
		</table>
		</form>
	</div>
	";
}

//Main HTML contents
$page_contents .= "
	$msg
	$error
	$form_html
";

//Output
echo $page_contents;

require_once('inc/site_footer.php');
?>