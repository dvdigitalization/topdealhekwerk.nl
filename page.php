<?php
/*
Title:		Standard page contents displayer
File: 		page.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

require_once('inc/site_header.php');

$crumbs[$_file] = $page_title;
echo $page_contents;

require_once('inc/site_footer.php');
?>