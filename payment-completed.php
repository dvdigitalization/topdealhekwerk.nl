<?php
/*
Title:		Payment completed handler
File: 		payment-completed.php
Version: 	v2.20
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2009 by Digitalization
*/

/******************************************************************************************
 * Settings
 ***/

//Page set and title
$page_set 	=  	array('Kassa');

//Required files
require_once('inc/site_header.php');

//Database tables
$db_table_customers		=	DIGI_DB_PREFIX.'topdeal_customers';
$db_table_orders		=	DIGI_DB_PREFIX.'topdeal_orders';

//File definitions
$file_shop				=	$cfg['USE_REWRITE_ENGINE'] ? 'shop.html' 	: 'shop.php';
$file_checkout			=	$cfg['USE_REWRITE_ENGINE'] ? 'kassa.html' 	: 'checkout.php';

//Settings
$id_format				=	'%05d';

//Payment settings
$bpe_merchant			=	'3krKTuAvPQ';
$bpe_secret_key			=	'TDhek2014SKapple';

//Payment methods
$PAYMENT_METHODS		=	array(	'50'		=>	'iDEAL (50% vooraf betaald en 50% overmaken na levering)',
									'100'		=>	'iDEAL (100% vooraf betaald)',
									'rembours'	=>	'Rembours');

/******************************************************************************************
 * Functions
 ***/

if (!function_exists('render_currency')) {
	function render_currency($cents, $decimal = '.') {
		$cents = round($cents);
		$euros = floor($cents / 100);
		$cents = $cents % 100;
		if ($cents < 10) $cents = '0'.$cents;
		return "$euros$decimal$cents";
	}
}

if (isset($_GET['send_manually']) and $order_id = (int) $_GET['send_manually']) {
	$order  		=	getMyo($db_table_orders, $order_id, 'order_serialized');
	if ($order and $order = unserialize($order) and is_array($order)) {
		send_order_mails($order_id, $order);
		echo "Mails verstuurd";
		exit;
	}
}

function send_order_mails($order_id, $order) {

	//Globals
	global $db_table_orders, $id_format, $PAYMENT_METHODS, $_link, $cfg, $COUNTRY_ARRAY, $_sep, $SEX_ARRAY;

	$gfx_folder				=	'gfx'.$_sep.'categories';
	$cat_prefix				=	'cat_';

	//Has the mail been sent already?
	if (getMyo($db_table_orders, $order_id, 'mail_sent')) {
		return false;
	}

	//Get the mail contents
	$customer_mail	=	file_get_contents('template/mail/mailtemplate_bestelling_klant.html');
	$topdeal_mail	=	file_get_contents('template/mail/mailtemplate_bestelling.html');

	//Get the order / cart
	$order 	= 	unserialize(getMyo($db_table_orders, $order_id, 'order_serialized'));
	$cart	=	$order['cart'];

	//Totals and amounts
	$_SESSION['cart'] = $cart;
	list ($sub_total, $delivery, $discount, $vat, $total, $discount_code, $discount_info, $factor, $item_prices) = get_cart_amounts();

	//Rembours costs if paying by rembours
	if ($order['payment_method'] == 'rembours') {
		$total += (int) $cfg['REMBOURS_COSTS'];
	}

	//50/50 costs if paying 50/50
	if ($order['payment_method'] == 50) {
		$total += (int) $cfg['5050_COSTS'];
	}

	//1% discount if subscribing to newsletter
	if (!empty($order['newsletter'])) {
		$new_total 	 = round(99*$total/100);
		$nl_discount = $total - $new_total;
		$total		 = $new_total;
	}

	//Recalculate VAT
	$vat = round($total*21/121);

	//Generate the order summary
	$rows = array();
	foreach ($cart as $identifier => $item) {

		//Get some info
		$amount 		=& 	$item['amount'];
		$category_id	=&	$item['id'];
		$name			=&	$item['name'];
		$price			=&	$item['price'];
		$unit			=&	$item['unit'];
		$description	=&	$item['description'];

		//Item price
		$item_price = 	$item_prices[$identifier];

		//Set image HTML
		$image_url	=	$gfx_folder.$_sep.$cat_prefix.$category_id.'.jpg';
		$image		=	file_exists($image_url) ? "<img src='$_link$image_url' alt='$name' width='35px' height='35px' />" : '';

		//Fix description if needed
		if (is_array($description)) {

			foreach ($description as $label => &$contents) {

				//If the contents are an array as well, we have the italic properties list
				if (is_array($contents)) {
					foreach ($contents as &$content) {

						//These are the actual property -> value pairs, that are displayed in italic
						foreach ($content as $item_key => &$item_val) {
							$item_val = "<em>$item_key:</em> $item_val";
						}

						//We implode them now with spaces
						$content = implode("<br/>&nbsp;&nbsp;&nbsp;", $content);
					}

					//Prepend with a dash and implode
					$contents = "<br/>&ndash; ".implode("\n<br/>&ndash; ", $contents);
				}

				//And make the description item row
				$contents = "<strong>$label:</strong> $contents";
			}

			//Implode the rows
			$description = implode("\n<br/>", $description);
		}

		//Product row
		$rows[] = "
		<tr>
			<td height='5' align='left' valign='top' colspan='4'></td>
		</tr>
		<tr>
			<td height='5' align='left' valign='top' style='border-top:1px solid #d7d7d7;' colspan='5'></td>
		</tr>
		<tr>
			<td width='50' height='18' align='left' valign='top'>$image</td>
			<td width='347' align='left' valign='top' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; padding:3px 0 0; color:#696868;'><strong style='color:#000;'>$name</strong><br>$description
			</td>
			<td width='88' align='left' valign='top' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; padding:3px 0 0; color:#696868;'>$amount</td>
			<td width='95' align='left' valign='top' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt; padding:3px 0 0; color:#696868;'>".parseAmount($item_price, '&euro; ')."</td>
		</tr>
		";
	}
	$order_summary = implode("\n", $rows);

	//Totals
	$customer_mail	=	str_replace('[DISCOUNT_CODE_INFO]', $discount_code, $customer_mail);
	$topdeal_mail   =	str_replace('[DISCOUNT_CODE_INFO]', $discount_code, $topdeal_mail);
	$customer_mail	=	str_replace('[DISCOUNT_AMOUNT]', 	parseAmount($discount, '&euro; '), $customer_mail);
	$topdeal_mail   =	str_replace('[DISCOUNT_AMOUNT]', 	parseAmount($discount, '&euro; '), $topdeal_mail);
	$customer_mail	=	str_replace('[SUBTOTAL]', 	parseAmount($sub_total, '&euro; '), $customer_mail);
	$topdeal_mail   =	str_replace('[SUBTOTAL]', 	parseAmount($sub_total, '&euro; '), $topdeal_mail);
	$customer_mail	=	str_replace('[VAT]', 	parseAmount($vat, '&euro; '), $customer_mail);
	$topdeal_mail   =	str_replace('[VAT]', 	parseAmount($vat, '&euro; '), $topdeal_mail);
	$customer_mail	=	str_replace('[TOTAL]', 	parseAmount($total, '&euro; '), $customer_mail);
	$topdeal_mail   =	str_replace('[TOTAL]', 	parseAmount($total, '&euro; '), $topdeal_mail);
	$customer_mail	=	str_replace('[DELIVERY]', 	parseAmount($delivery, '&euro; '), $customer_mail);
	$topdeal_mail   =	str_replace('[DELIVERY]', 	parseAmount($delivery, '&euro; '), $topdeal_mail);

	//Make the personal info
	$customer_mail	=	str_replace('[TITLE]', 		$SEX_ARRAY[$order['customer']['title']], $customer_mail);
	$topdeal_mail   =	str_replace('[TITLE]', 		$SEX_ARRAY[$order['customer']['title']], $topdeal_mail);
	$customer_mail	=	str_replace('[PREFIX]', 	$order['customer']['prefix'], $customer_mail);
	$topdeal_mail   =	str_replace('[PREFIX]', 	$order['customer']['prefix'], $topdeal_mail);
	$customer_mail	=	str_replace('[FIRST_NAME]', $order['customer']['first_name'], $customer_mail);
	$topdeal_mail   =	str_replace('[FIRST_NAME]', $order['customer']['first_name'], $topdeal_mail);
	$customer_mail	=	str_replace('[LAST_NAME]',  $order['customer']['last_name'], $customer_mail);
	$topdeal_mail   =	str_replace('[LAST_NAME]',  $order['customer']['last_name'], $topdeal_mail);
	$customer_mail	=	str_replace('[ADDRESS]',    $order['customer']['address'], $customer_mail);
	$topdeal_mail   =	str_replace('[ADDRESS]',    $order['customer']['address'], $topdeal_mail);
	$customer_mail	=	str_replace('[POSTCODE]',   $order['customer']['postal_code'], $customer_mail);
	$topdeal_mail   =	str_replace('[POSTCODE]',   $order['customer']['postal_code'], $topdeal_mail);
	$customer_mail	=	str_replace('[CITY]',       $order['customer']['city'], $customer_mail);
	$topdeal_mail   =	str_replace('[CITY]',      	$order['customer']['city'], $topdeal_mail);
	$customer_mail	=	str_replace('[COUNTRY]', 	$COUNTRY_ARRAY[$order['customer']['country']], $customer_mail);
	$topdeal_mail   =	str_replace('[COUNTRY]', 	$COUNTRY_ARRAY[$order['customer']['country']], $topdeal_mail);
	$customer_mail	=	str_replace('[PHONE]', 		$order['customer']['phone'], $customer_mail);
	$topdeal_mail   =	str_replace('[PHONE]', 		$order['customer']['phone'], $topdeal_mail);
	$customer_mail	=	str_replace('[MOBILE]', 	$order['customer']['mobile'], $customer_mail);
	$topdeal_mail   =	str_replace('[MOBILE]', 	$order['customer']['mobile'], $topdeal_mail);
	$customer_mail	=	str_replace('[EMAIL]', 		$order['customer']['email'], $customer_mail);
	$topdeal_mail   =	str_replace('[EMAIL]', 		$order['customer']['email'], $topdeal_mail);

	//Shipping address
	$customer_mail	=	str_replace('[SHIPPING_ADDRESS]',    $order['delivery']['address'], $customer_mail);
	$topdeal_mail   =	str_replace('[SHIPPING_ADDRESS]',    $order['delivery']['address'], $topdeal_mail);
	$customer_mail	=	str_replace('[SHIPPING_POSTCODE]',   $order['delivery']['postal_code'], $customer_mail);
	$topdeal_mail   =	str_replace('[SHIPPING_POSTCODE]',   $order['delivery']['postal_code'], $topdeal_mail);
	$customer_mail	=	str_replace('[SHIPPING_CITY]',       $order['delivery']['city'], $customer_mail);
	$topdeal_mail   =	str_replace('[SHIPPING_CITY]',       $order['delivery']['city'], $topdeal_mail);
	$customer_mail	=	str_replace('[SHIPPING_COUNTRY]', 	 $COUNTRY_ARRAY[$order['delivery']['country']], $customer_mail);
	$topdeal_mail   =	str_replace('[SHIPPING_COUNTRY]', 	 $COUNTRY_ARRAY[$order['delivery']['country']], $topdeal_mail);

	//Order details
	$customer_mail  =	str_replace('[DELIVERY_DATE]', 		 $order['delivery_date'], $customer_mail);
	$topdeal_mail   =	str_replace('[DELIVERY_DATE]', 		 $order['delivery_date'], $topdeal_mail);
	$customer_mail  =	str_replace('[PAYMENT_METHOD]', 	 $PAYMENT_METHODS[$order['payment_method']], $customer_mail);
	$topdeal_mail   =	str_replace('[PAYMENT_METHOD]', 	 $PAYMENT_METHODS[$order['payment_method']], $topdeal_mail);
	$customer_mail	=	str_replace('[SUMMARY]', $order_summary, $customer_mail);
	$topdeal_mail	=	str_replace('[SUMMARY]', $order_summary, $topdeal_mail);

	//Replace the rembours tag, if rembours was chosen
	$rembours = '';
	if ($order['payment_method'] == 'rembours') {
		$rembours = "
			<tr>
				<td width='50' height='18' align='left' valign='middle'></td>
				<td width='347' align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt;'>Rembourskosten</td>
				<td width='88' align='left' valign='middle'></td>
				<td width='95' align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt;'>".parseAmount($cfg['REMBOURS_COSTS'], '&euro; ')."</td>
			</tr>
		";
	}
	else if ($order['payment_method'] == 50) {
		$rembours = "
			<tr>
				<td width='50' height='18' align='left' valign='middle'></td>
				<td width='347' align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt;'>50% betaling achteraf kosten</td>
				<td width='88' align='left' valign='middle'></td>
				<td width='95' align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt;'>".parseAmount($cfg['5050_COSTS'], '&euro; ')."</td>
			</tr>
		";
	}

	if (!empty($nl_discount)) {
		$rembours .= "
			<tr>
				<td width='50' height='18' align='left' valign='middle'></td>
				<td width='347' align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt;'>1% korting i.v.m. aanmelden nieuwsbrief</td>
				<td width='88' align='left' valign='middle'></td>
				<td width='95' align='left' valign='middle' style='font-family:Arial, Verdana, Helvetica, sans-serif; font-size:9pt;'>- ".parseAmount($nl_discount, '&euro; ')."</td>
			</tr>
		";
	}

	$customer_mail	=	str_replace('[REMBOURS]', 	$rembours, $customer_mail);
	$topdeal_mail   =	str_replace('[REMBOURS]', 	$rembours, $topdeal_mail);

	//Replace tags
	$customer_mail	=	str_replace('[ORDER_ID]', sprintf($id_format, $order_id), $customer_mail);
	$topdeal_mail	=	str_replace('[ORDER_ID]', sprintf($id_format, $order_id), $topdeal_mail);
	$customer_mail	=	str_replace('[COMMENTS]', nl2br($order['remarks']), $customer_mail);
	$topdeal_mail	=	str_replace('[COMMENTS]', nl2br($order['remarks']), $topdeal_mail);
	$customer_mail	=	str_replace('[LINK]', $_link, $customer_mail);
	$topdeal_mail	=	str_replace('[LINK]', $_link, $topdeal_mail);

	

	//Send the emails
	require_once(SITE_PATH_PREFIX.'inc/sendmail.php');

	sendMail(	$order['customer']['email'], 	ORDER_EMAIL_TO_CUSTOMER, 	$customer_mail, '', '', '', true);
	
	//Email string	
	$email_string = $cfg['ORDER_FORM_EMAIL'];
	$emails = explode('; ', $email_string);	
	foreach($emails as $email) {
		sendMail(	$email, 		ORDER_EMAIL_TO_TOPDEAL, 	$topdeal_mail,
				$order['customer']['first_name'].' '.$order['customer']['last_name'].' <'.$order['customer']['email'].'>','','',true,false,true);
	}

	//Mark as sent
	upRec($db_table_orders, $order_id, 'mail_sent', 1);
}

/******************************************************************************************
 * Determine the order
 ***/

//Order ID given?
$error = '';

if ($order_id = (int) request('bpe_reference') and $signature = request('bpe_signature2')) {

	//Invalid signature
	if ($signature != md5(				
			request('bpe_trx').
			request('bpe_timestamp').
			$bpe_merchant.
			request('bpe_invoice').
			request('bpe_reference').
			request('bpe_currency').
			request('bpe_amount').
			request('bpe_result').
			request('bpe_mode').$bpe_secret_key)
		) {
		$order_id 	= 	0;
	}

	//Get order info
	$order  		=	getMyo($db_table_orders, $order_id, 'order_serialized');

	//Failed?
	$result = request('bpe_result');
	if ($result != 801 and $result != 800) {
		$_GET['failed'] = 1;
	}

	//Order not found / malformed
	if (!$order or !($order = unserialize($order)) or !is_array($order)) {
		$order_id 	=	0;
	}
}

//BRQ nieuwe payment engine
elseif ($order_id = (int) request('brq_invoicenumber') and $signature = request('brq_signature')) {
	
	$shaString = '';

	function iksort($a, $b)
	{
		return strcasecmp(strtolower($a), strtolower($b));
	}		

	uksort($_POST, 'iksort');
	foreach($_POST as $name => $value){
		if ($name != 'brq_signature'){
			$shaString .= $name.'='.urldecode($value);
		}
	}	
	$shaString .= $bpe_secret_key;

	if(sha1($shaString) != $_POST['brq_signature']){
		$order_id = 0;
	}

	//Failed?
	$result = request('brq_statuscode');
	if ($result != 190 and $result != 790 and $result != 791 and $result != 792 and $result != 793) {
		$_GET['failed'] = 1;
	}

	//Get order info
	$order  		=	getMyo($db_table_orders, $order_id, 'order_serialized');

	//Order not found / malformed
	if (!$order or !($order = unserialize($order)) or !is_array($order)) {
		$order_id 	=	0;
	}
}

//Rembours
elseif (isset($_POST['order_id']) and $order_id = (int) $_POST['order_id']) {

	//Get order info
	$order  		=	getMyo($db_table_orders, $order_id, 'order_serialized');

	//Order not found / malformed
	if (!$order or !($order = unserialize($order)) or !is_array($order)) {
		$order_id 	=	0;
	}
}

//Order still in session? Redirect to checkout
elseif (isset($_SESSION['order_id']) and $order_id = (int) $_SESSION['order_id'] and isset($_SESSION['order_ver']) and
		$_SESSION['order_ver'] == sha1($order_id.'ErdfEH83Hj#k5k%')) {
	header('Location: '.$file_checkout);
	exit;
}

//No order, nothing to do, redirect to shop
else {
	header('Location: '.$file_shop);
	exit;
}

/******************************************************************************************
 * Payment failed handler
 ***/

if (!$order_id or isset($_GET['failed'])) {

	//Output notification
	$_file		=	$cfg['USE_REWRITE_ENGINE'] ? 'betaling-mislukt.html' : $_file;
	$page_title = 	'Bestelling mislukt';
	$page_contents		=	PAYMENT_FAILED;
}

/******************************************************************************************
 * Payment succeeded handler
 ***/

elseif ($order_id) {

	//Send confirmation mail
	send_order_mails($order_id, $order);

	//Output notification
	$_file		=	$cfg['USE_REWRITE_ENGINE'] ? 'betaling-voltooid.html' : $_file;
	$page_title = 	'Bestelling voltooid';

	if ($order['payment_method'] == 100) {
		$page_contents = str_replace(	array('[ORDER_NR]','[EMAIL]'),
										array(sprintf($id_format, $order_id), $order['customer']['email']), PAY_OK_100);
	}
	elseif ($order['payment_method'] == 50) {
		$page_contents = str_replace(	array('[ORDER_NR]','[EMAIL]'),
										array(sprintf($id_format, $order_id), $order['customer']['email']), PAY_OK_50);
	}
	elseif ($order['payment_method'] == 'rembours') {
		$page_contents = str_replace(	array('[ORDER_NR]','[EMAIL]'),
										array(sprintf($id_format, $order_id), $order['customer']['email']), PAY_OK_REMBOURS);
	}
	else {
		$page_contents = '@___@';
	}

	// ?
	$ga_conversion = '
<!-- Google Code for Conversie bestelling Hekwerk Conversion Page -->
<script type="text/javascript">
<!--
var google_conversion_id = 1031884269;
var google_conversion_language = "nl";
var google_conversion_format = "1";
var google_conversion_color = "ffffff";
var google_conversion_label = "jaztCI2VlgEQ7ZuF7AM";
//-->
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1031884269/?label=jaztCI2VlgEQ7ZuF7AM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
	';
	
	/* Prepare Google Analytics data */
	
	$account = json_encode('UA-7408791-4');
	
	//Get the order / cart
	$order	= 	unserialize(getMyo($db_table_orders, $order_id, 'order_serialized'));
	$delivery	=	$order['delivery'];
	$cart		=	$order['cart'];
	$customer	=	$order['customer'];

	//Totals and amounts
	list ($sub_total, $delivery, $discount, $vat, $total, $discount_code, $discount_info, $factor, $item_prices) = get_cart_amounts();

	$ignored_ref = 'buckaroo.nl';
	
	$transaction = array(
		'transaction_id'	=> $order_id,
		'store'				=> 'Topdeal',
		'total'				=> render_currency($total),
		'tax'				=> render_currency($vat),
		'shipping'			=> render_currency($delivery),
		'city'				=> $customer['city'],
		'state_province'	=> '',
		'country'			=> getMyo(DIGI_DB_PREFIX.'countries', $customer['country'], 'country_code')
	);
	
	//Build items
	$items = array();
	foreach ($cart as $identifier => $item) {

		$items[] = array(
			'id'		=> $order_id,
			'code'		=> $item['id'],
			'name'		=> $item['name'],
			'category'	=> '',
			'price'		=> render_currency($item['price']),
			'quantity'	=> $item['amount']
		);
	}
	
	/* Pre-process Google Analytics data */
	
	$ignored_ref = json_encode($ignored_ref);
	foreach($transaction as &$v) {
		$v = json_encode((string)$v);
	}
	foreach($items as &$item) {
		foreach ($item as &$v) {
			$v = json_encode((string)$v);
		}
	}
	unset($v);
	
	/* Render Google Analytics script */
	
	$page_contents .= <<<JS
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', $account]);
_gaq.push(['_setSiteSpeedSampleRate', 50]);
_gaq.push(['_addIgnoredRef', $ignored_ref]);
_gaq.push(['_trackPageview']);
_gaq.push([
	'_addTrans',
	{$transaction['transaction_id']},	// transaction ID - required
	{$transaction['store']},			// affiliation or store name
	{$transaction['total']},			// total - required
	{$transaction['tax']},				// tax
	{$transaction['shipping']},			// shipping
	{$transaction['city']},				// city
	{$transaction['state_province']},	// state or province
	{$transaction['country']}			// country
]);
JS;
	foreach($items as $item) {
		$page_contents .= <<<JS
_gaq.push([
	'_addItem',
	{$item['id']},			// transaction ID - required
	{$item['code']},		// SKU/code - required
	{$item['name']},		// product name
	{$item['category']},	// category or variation
	{$item['price']},		// unit price - required
	{$item['quantity']}		// quantity - required
]);
JS;
	}
	
	$page_contents .= <<<JS
_gaq.push(['_trackTrans']); //submits transaction to the Analytics servers
</script>
JS;

	//Clear cart and order from session
	if (isset($_SESSION['cart'])) {
		unset($_SESSION['cart']);
	}
	if (isset($_SESSION['order'])) {
		unset($_SESSION['order']);
	}
}


/******************************************************************************************
 * Page contents
 ***/

echo $page_contents;

require_once('inc/site_footer.php');
?>