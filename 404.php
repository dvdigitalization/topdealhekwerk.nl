<?php
/*
Title:		Standard index page
File: 		index.php
Version: 	v2.10
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

//Set HTML name
$html_name	=	'index.html';

header("HTTP/1.0 404 Not Found");

require_once('inc/site_header.php');

echo $page_contents;

require_once('inc/site_footer.php');
?>