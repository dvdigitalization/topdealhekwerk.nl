<?php
/*
Title:		Shopping cart
File: 		shopping-cart.php
Version: 	v2.20
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2008 by Digitalization
*/

/******************************************************************************************
 * Settings
 ***/

//Page set
$page_set 	= 	array('Winkelwagen');

//Required files
require_once('inc/site_header.php');

//File definitions
$_file					=	$cfg['USE_REWRITE_ENGINE'] ? 'winkelwagen.html' : $_file;
$file_checkout			=	$cfg['USE_REWRITE_ENGINE'] ? 'kassa.html' 		: 'checkout.php';
$ajax_page				=	'inc/common/ajx_cart.php';

//Settings
$gfx_folder				=	'gfx'.$_sep.'categories';
$cat_prefix				=	'cat_';
$return_url				=	request('return', 'index.html');
$page_title				=	'Uw winkelwagen';
$cat_re_prefix			=	'categorie';
$datepicker_min_date	=	3;

//Random string
$random_string			=	randomPass(12);

/******************************************************************************************
 * Load cart contents from session
 ***/

if (!isset($_SESSION['cart']) or !$_SESSION['cart'] or !is_array($_SESSION['cart']) or isset($_GET['clear_cart'])) {
	$_SESSION['cart'] = array();
}
$cart =& $_SESSION['cart'];

/******************************************************************************************
 * Cart content modifications
 ***/

// *** Remove items
if (isset($_GET['remove']) and $identifier = $_GET['remove'] and isset($cart[$identifier])) {
	unset($cart[$identifier]);
	unset($_SESSION['cart'][$identifier]);
}

/******************************************************************************************
 * Page contents
 ***/

//If no items, display empty cart notice and quit script
if (!count($cart)) {

	//Empty cart error
	$error			=	"Uw winkelwagen is leeg.";
	$cart_contents	=	'';
}
else {

	//Get cart amounts and factor
	list ($sub_total, $delivery_price, $discount, $vat, $total, $discount_code, $discount_info, $factor, $item_prices) = get_cart_amounts();

	//Loop items
	$rows = array();
	$identifiers = array();
	foreach ($cart as $identifier => $item) {

		//Save identifier
		$identifiers[] = $identifier;

		//Get some info
		$amount 		=& 	$item['amount'];
		$category_id	=&	$item['id'];
		$name			=&	$item['name'];
		$price			=&	$item['price'];
		$unit			=&	$item['unit'];
		$description	=&	$item['description'];
		$delivery		=&	$item['delivery'];

		//Delivery date
		if ($delivery = (int) $delivery and $delivery > $datepicker_min_date) {
			$datepicker_min_date = $delivery;
		}

		//Product URL
		$cat_url		=	$cfg['USE_REWRITE_ENGINE'] ? 	$cat_re_prefix.'-'.$category_id.'-'.rewriteSafe($name).'.html' :
															'shop.php?category_id='.$category_id;

		//Item price
		$item_price = 	$item_prices[$identifier];

		//Set image HTML
		$image_url	=	$gfx_folder.$_sep.$cat_prefix.$category_id.'.jpg';
		$image		=	file_exists($image_url) ? "<img src='$image_url' alt='$name' width='35px' height='35px' />" : '';

		//Fix description if needed
		if (is_array($description)) {
/*
			$description = array(	'descr 1'	=>	'heers',
									'descr 2'	=>	'faal',
									'descr 3'	=>	array(
										array(	'subprop'	=>	'subvalue',
												'subprop 2'	=>	'subvalue 2',),

										//Volgende streepje
										array(	'subprop 3'	=>	'subvalue',
												'subprop 4'	=>	'subvalue 2',)
									));
*/
			foreach ($description as $label => &$contents) {

				//If the contents are an array as well, we have the italic properties list
				if (is_array($contents)) {
					foreach ($contents as &$content) {

						//These are the actual property -> value pairs, that are displayed in italic
						foreach ($content as $item_key => &$item_val) {
							$item_val = "<em>$item_key:</em> $item_val";
						}

						//We implode them now with spaces
						$content = implode("<br/>&nbsp;&nbsp;&nbsp;", $content);
					}

					//Prepend with a dash and implode
					$contents = "<br/>&ndash; ".implode("\n<br/>&ndash; ", $contents);
				}

				//And make the description item row
				$contents = "
				<tr>
					<td width='47' align='left' valign='middle'>&nbsp;</td>
					<td align='left' valign='middle' class='cartDescr'><strong>$label:</strong> $contents</td>
					<td align='left' valign='middle' colspan='3'>&nbsp;</td>

				</tr>

				";
			}

			//Implode the rows
			$description = implode("\n", $description);
		}

		//Product row
		$rows[] = "
		<div class='winkelwagenRow'>
			<table width='100%' cellspacing='2' cellpadding='0' border='0'>
			<tbody>
			<tr>
				<td width='47px' valign='middle' align='left'>
				$image
				</td>
				<td valign='middle' align='left'>
					<strong>$name</strong>
				</td>
				<td width='78px' valign='middle' align='left'>
					<input class='formSmall' type='text' name='aantal' value='$amount' onkeyup='update_price(\"$identifier\", this.value)'/>
				</td>
				<td width='100px' valign='middle' align='left'>
					<strong><span id='$identifier'>".parseAmount($item_price, '&euro; ')."</span></strong>
				</td>
				<td width='70px' class='verwijderBtn' valign='middle' align='left'>
					<a href='$_file?remove=$identifier&return=$return_url'></a>
				</td>
			</tr>
			$description
			</tbody>
			</table>
			<div class='divider'>&nbsp;</div>
		</div>
		";
	}

	//Delivery date picker
	$datepicker_min_date = '+'.$datepicker_min_date.'d';

	//Set cart contents
	$cart_contents	=	"
	<script type='text/javascript'>

		var item_identifiers = new Array('".implode("','", $identifiers)."');

		function update_price(id, amount) {

			if (amount) {
				$.get('$ajax_page?update_price='+id+'&amount='+amount, function(xml) {
					if (xml) {

						//Get the new vales
						var sub_total 	= $(xml).find('sub_total').text();
						var discount 	= $(xml).find('discount').text();
						var vat 		= $(xml).find('vat').text();
						var delivery 	= $(xml).find('delivery').text();
						var total 		= $(xml).find('total').text();

						//Update the HTML
						$('span#sub_total').html(sub_total);
						$('span#discount').html(discount);
						$('span#vat').html(vat);
						$('span#delivery').html(delivery);
						$('span#total').html(total);

						//Update all the item prices
						for (i = 0; i <= item_identifiers.length; i++) {
							var identifier = item_identifiers[i];
							$('span#'+identifier).html($(xml).find(identifier).text());
						}
					}
					else {

						//Something went wrong, refresh the cart by force
						document.location.href = '$_file';
					}
				});
			}
		}

		function eval_discount_code(code) {
			$.get('$ajax_page?discount_code='+code, function(xml) {
				if (xml) {

					//Get the new vales
					var code 		= $(xml).find('discount_code').text();
					var sub_total 	= $(xml).find('sub_total').text();
					var discount 	= $(xml).find('discount').text();
					var vat 		= $(xml).find('vat').text();
					var delivery 	= $(xml).find('delivery').text();
					var total 		= $(xml).find('total').text();
					var discount_info = $(xml).find('discount_info').text();

					//Update the HTML
					$('input#discount_code').val(code);
					$('span#discount_info').html(discount_info);
					$('span#sub_total').html(sub_total);
					$('span#discount').html(discount);
					$('span#vat').html(vat);
					$('span#delivery').html(delivery);
					$('span#total').html(total);
				}
				else {

					//Something went wrong, refresh the cart by force
					document.location.href = '$_file';
				}
			});
		}
	</script>
	<form name='cart_form' method='post' action='$_file' />
	<input type='hidden' name='return' value='$return_url' />
	<div class='winkelwagenRow'>
		<table width='100%' cellspacing='2' cellpadding='0' border='0'>
		<tbody>
		<tr>
			<td height='22px' width='47px' valign='middle' align='left'>&nbsp;</td>
			<td valign='middle' align='left'>
				<strong>Productnaam</strong>
			</td>
   			<td width='78px' valign='middle' align='left'>
				<strong>Aantal</strong>
			</td>
			<td width='100px' valign='middle' align='left'>
				<strong>Topdealprijs</strong>
			</td>
			<td width='70px' valign='middle' align='left'>
				<strong>Verwijder</strong>
			</td>
		</tr>
		</tbody>
		</table>
		<div class='divider'>&nbsp;</div>
	</div>
	".implode("\n", $rows)."
	<div class='winkelwagenRow'>
		<table width='100%' cellpadding='0' cellspacing='2' border='0'>
			<tr>
				<td width='47px' height='22' align='left' valign='middle'></td>
				<td align='left' valign='middle'><strong>Heeft u een actiecode?</strong>
					<div class='info' id='infoActie' onmouseover='displayDiv(\"infoActie\", 1);' onmouseout='displayDiv(\"infoActie\", 0);' style='display:none;'>
						".INFO_DISCOUNT_CODE."
					</div>
					<a onmouseover='displayDiv(\"infoActie\", 1);' onmouseout='displayDiv(\"infoActie\", 0);' name='infoIcon'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:4px 0 0; vertical-align: text-bottom' /></a> &nbsp; <span id='discount_info'>$discount_info</span>
				</td>
				<td width='78px' align='left' valign='middle'><input type='text' id='discount_code' onchange='eval_discount_code(this.value);' class='formSmall' value='$discount_code'/></td>
				<td width='172px' align='left' valign='middle'><strong><span id='discount'>- ".parseAmount($discount, '&euro; ')."</span></strong></td>
			</tr>
		</table>
		<div class='divider'>&nbsp;</div>
	</div>
	<div class='winkelwagenRow'>
		<table width='100%' cellpadding='0' cellspacing='2' border='0'>
			<tr>
				<td height='22' align='right' valign='middle'>Topdealprijs&nbsp;</td>
				<td width='10px'>&nbsp;</td>
				<td width='172px' align='left' valign='middle'><strong><span id='sub_total'>".parseAmount($sub_total, '&euro; ')."</span></strong></td>
			</tr>
		</table>
		<div class='divider'>&nbsp;</div>
	</div>
	<div class='winkelwagenRow'>
		<table width='100%' cellpadding='0' cellspacing='2' border='0'>
			<tr>
				<td height='22' align='right' valign='top'>Verzendkosten <a onmouseover='displayDiv(\"infoVerzend\", 1);' onmouseout='displayDiv(\"infoVerzend\", 0);' name='infoIcon2'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:4px 0 0; vertical-align: text-bottom' /></a></td>
				<td width='10px' align='left' valign='middle'>
					<div class='info' id='infoVerzend' onmouseover='displayDiv(\"infoVerzend\", 1);' onmouseout='displayDiv(\"infoVerzend\", 0);' style='display:none;'>
						".INFO_DELIVERY_COSTS."
					</div>
				</td>
				<td width='172px' align='left' valign='middle'><strong><span id='delivery'>".parseAmount($delivery_price, '&euro; ')."</span></strong></td>
			</tr>
		</table>
		<div class='divider'>&nbsp;</div>
	</div>
	<div class='winkelwagenRow'>
		<table width='100%' cellpadding='0' cellspacing='2' border='0'>
			<tr>
				<td height='22' align='right' valign='middle'>Totaal&nbsp;</td>
				<td width='10px'>&nbsp;</td>
				<td width='172px' align='left' valign='middle'><strong><span id='total'>".parseAmount($total, '&euro; ')."</span></strong></td>
			</tr>
		</table>
		<div class='divider'>&nbsp;</div>
	</div>
	<div class='winkelwagenRow'>
		<table width='100%' cellpadding='0' cellspacing='2' border='0'>
			<tr>
				<td height='22' align='right' valign='middle'>Waarvan 21% BTW&nbsp;</td>
				<td width='10px'>&nbsp;</td>
				<td width='172px' align='left' valign='middle'><strong><span id='vat'>".parseAmount($vat, '&euro; ')."</span></strong></td>
			</tr>
		</table>
	</div>
	</form>";

	//We don't want to interfere with the navigation...
	$category_id = 0;
}

//Error
$error = isset($error) ? $error : '';

//Page contents
$page_contents 	= "
	<div class='blok'>
		<h1>Uw winkelwagen</h1>
		<hr/>
	</div>
	<div class='blok'>
		<p class='error_message'>$error</p>
	</div>
	<div class='blok'>
		$cart_contents
	</div>
	<br/><br/>

	<div class='winkelwagenRowBtn'>
		<div class='divider'>
			<div class='naarKassaBtn'>
				<a href='$file_checkout'><span>Naar de kassa</span></a>
			</div>
			<div class='winkelVerderBtn'>
				<a href='$return_url'><span>Winkel verder</span></a>
			</div>
		</div>
	</div>
";

echo $page_contents;

require_once('inc/site_footer.php');
?>