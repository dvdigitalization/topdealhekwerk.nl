<?php
/*
Title:		Topdeal shop template
File: 		shop.php
Version: 	v2.20
Author:		Digitalization
Contact:	info@digitalization.nl
Copyright:	All code copyright 2009 by Digitalization
*/

/******************************************************************************************
 * Settings
 ***/

//Page set and title
$page_set 	= 	array('Paneelsystemen','Gaas','Toegangspoorten','Accessoires','Offertes');

//Required files
require_once('inc/site_header.php');

//File definitions
$_file					=	$cfg['USE_REWRITE_ENGINE'] ? 'shop.html' 		: $_file;
$file_cart				=	$cfg['USE_REWRITE_ENGINE'] ? 'winkelwagen.html' : 'shopping-cart.php';

//Random string
$random_string			=	randomPass(12);
$template				=	'static/shop.html';

//Get lowest factor to use for prices
$factor					=	getMyo('digi_topdeal_discounts', "", 'multiplier', 'multiplier', 'ASC', 1);

//Settings
$gfx_folder				=	'gfx'.$_sep.'categories';
$cat_prefix				=	'cat_';
$cat_re_prefix			=	'categorie';
$show_ruler				=	false;

/******************************************************************************************
 * Category definitions / set-up
 ***/

//Items per page (for pagination)
$items_per_page  = 5;

//The categories we'll display, provide their ID's
define('CATEGORY_PANEEL',			1);
define('CATEGORY_GAAS',				13);
define('CATEGORY_PLASITOR_GAAS',	25);
define('CATEGORY_DIER_TUIN_GAAS',	20);
define('CATEGORY_POORTEN',			27);
define('CATEGORY_ACCESSOIRES',		36);
define('CATEGORY_DUBBELSTAAF',		97);

//The offerte form files
$form_files				=	array(	CATEGORY_PANEEL			=>	'frm_paneelsystemen.php',
									CATEGORY_GAAS			=>	'frm_gaas.php',
									CATEGORY_POORTEN		=>	'frm_toegangspoorten.php',
									CATEGORY_PLASITOR_GAAS	=>	'frm_plasitor_gaas.php',
									CATEGORY_DIER_TUIN_GAAS	=>	'frm_dier_tuin_gaas.php',
									CATEGORY_ACCESSOIRES	=>	'frm_accessoires.php',
									CATEGORY_DUBBELSTAAF	=>	'frm_dubbelstaaf.php');

//Additional required fields check files
$required_files			=	array(	CATEGORY_PANEEL			=>	'req_paneelsystemen.php',
									CATEGORY_GAAS			=>	'req_gaas.php',
									CATEGORY_POORTEN		=>	'req_toegangspoorten.php',
									CATEGORY_PLASITOR_GAAS	=>	'req_plasitor_gaas.php',
									CATEGORY_DIER_TUIN_GAAS	=>	'req_dier_tuin_gaas.php',
									CATEGORY_ACCESSOIRES	=>	'req_accessoires.php',
									CATEGORY_DUBBELSTAAF	=>	'req_dubbelstaaf.php');

//Cart files
$cart_files				=	array(	CATEGORY_PANEEL			=>	'cart_paneelsystemen.php',
									CATEGORY_GAAS			=>	'cart_gaas.php',
									CATEGORY_POORTEN		=>	'cart_toegangspoorten.php',
									CATEGORY_PLASITOR_GAAS	=>	'cart_plasitor_gaas.php',
									CATEGORY_DIER_TUIN_GAAS	=>	'cart_dier_tuin_gaas.php',
									CATEGORY_ACCESSOIRES	=>	'cart_accessoires.php',
									CATEGORY_DUBBELSTAAF	=>	'cart_dubbelstaaf.php');

//Required fields
$required_fields 		= 	array();
$fields_missing			=	false;
$missing				=	array();

/******************************************************************************************
 * Form processing
 ***/

if (isset($_POST['send_form']) and $category_id) {

	//Define invalid price
	define('INVALID_PRICE',		999999);

	//Read some info from database
	$category = getMyo($db_table_cats,$category_id,
				array('active','menu_name','name','description','sub_cat_of','price_unit','manual','delivery_days'));

	//Validate category
	if ($category and $category->active and $category->sub_cat_of and isset($form_files[$category->sub_cat_of])) {

		//Data cleansing
		foreach (array_keys($_POST) as $key) {
			evalAll($_POST[$key]);
		}

		//Additional required fields/validation
		@include('inc/common/'.$required_files[$category->sub_cat_of]);

		//Missing fields check
		foreach ($required_fields as $required_field) {
			if (empty($_POST[$required_field])) {
				$fields_missing = true;
				$missing[$required_field] = true;
			}
		}

		if ($fields_missing) {
			$error = OFF_FIELDS_MISSING;
		}

		//All ok
		else {

			//Prepare cart array if not set
			if (!isset($_SESSION['cart']) or !$_SESSION['cart'] or !is_array($_SESSION['cart']) or isset($_GET['clear_cart'])) {
				$_SESSION['cart'] = array();
			}
			$cart =& $_SESSION['cart'];

			//Get item name
			$c_name	= $category->menu_name ? $category->menu_name : $category->name;

			//Cart file
			include('inc/common/'.$cart_files[$category->sub_cat_of]);

			//Dont add?
			if (!empty($dont_add)) {
				echo "<br/><p style='color: red;'>Foute product keuze</p><br/>";
			}
			else {

				//Check if identifier was set. If not, make one now.
				//$identifier	= isset($identifier) ? 'i'.$identifier : 'i'.md5($category_id);
				$identifier = md5(time());

				//Item exists in cart?
				if (isset($cart[$identifier])) {
					$cart[$identifier]['amount']++;
				}

				//Add item
				else {

					//Initialize the array, or use $item if it exists
					$cart[$identifier] = isset($item) ? $item : array();

					//The ID and name are always standard
					$cart[$identifier]['id']		=	$category_id;
					$cart[$identifier]['name']		=	$c_name;
					$cart[$identifier]['unit']		=	$category->price_unit;
					$cart[$identifier]['delivery']	=	$category->delivery_days;

					//Check if these three were set
					if (!isset($cart[$identifier]['amount'])) {
						$cart[$identifier]['amount'] = 1;
					}
					if (!isset($cart[$identifier]['price'])) {
						$cart[$identifier]['price'] = INVALID_PRICE;
					}
					if (!isset($cart[$identifier]['description'])) {
						$cart[$identifier]['description'] = '';
					}
				}

				//Redirect to cart, with a returl URL
				$c_url		=	$cfg['USE_REWRITE_ENGINE'] ? 	$cat_re_prefix.'-'.$category_id.'-'.rewriteSafe($c_name).'.html' :
																$_file.'?category_id='.$category_id;
				header('Location: '.$file_cart.'?return='.$c_url);
				exit;
			}
		}
	}
}

/******************************************************************************************
 * Category view
 ***/

if ($category_id) {

	//Read from database (if not done already)
	if (!isset($_POST['send_form'])) {
		$category = getMyo($db_table_cats,$category_id,
					array('active','menu_name','name','description','sub_cat_of','price_unit','manual'));
	}

	//Active and main cat? Set-up correctly?
	if ($category and $category->active and !$category->sub_cat_of) {

		//Set vars
		$c_name			=	$category->menu_name ? $category->menu_name : $category->name;
		$c_description 	= 	nl2br($category->description);
		$c_image_url	=	$gfx_folder.$_sep.$cat_prefix.$category_id.'.jpg';
		$page_image		=	$c_image_url;
		$page_title		=	$c_name;

		//Category URL
		$c_url			 =	$cfg['USE_REWRITE_ENGINE'] ? 	$cat_re_prefix.'-'.$category_id.'-'.rewriteSafe($c_name).'.html' :
															$_file.'?category_id='.$category_id;

		//Bread crumb
		$crumbs[$c_url] 	= 	$c_name;
		$active_crumb		=	$c_url;

		//Pagination
		require_once('classes/class_PageNumbering.php');
		$PN = new PageNumbering($db_table_cats, "active='1' AND menu_name <> '' AND sub_cat_of='$category_id'",
								"category_id=".$category_id,$items_per_page,100);
		$PN->setImploder("<div class='gbPage'><span>|</span></div>");
		$PN->setCurrentTemplate("<div class='gbPage'><a href='[LINK]' title='[X]' class='selected'>[X]</a></div>");
		$PN->setLinkTemplate("<div class='gbPage'><a href='[LINK]' title='[X]'>[X]</a></div>");
		$PN->setNextTemplate('');
		$PN->setPrevTemplate('');
		$PN->setPrevNextSeparator('');

		//Pagination HTML and mysql limit start
		$pages_html			=	$PN->getPagesHTML();
		$pages_html 		= 	"<div class='paginering'>
									<div class='gbPage'><span>pagina ".$PN->getPage()." van ".$PN->getPagesNeeded()."&nbsp;</span></div>
									".$pages_html."
								</div>";
		$mysql_limit_start	=	$PN->getMysqlLimitStart();

		//Get sub-categories
		$items = array();
		$res = eq("	SELECT id,menu_name,name,description,description_overview,price_unit
					FROM $db_table_cats WHERE active = '1' AND menu_name <> ''
					AND sub_cat_of = '$category_id' ORDER BY category_order ASC
					LIMIT $mysql_limit_start,$items_per_page;");

		//Voorkeuren (available)
		$heights = $widths = array(0 => '');

		//Reset voorkeuren on category change
		if (isset($_SESSION['vk_cat_id']) and $category_id != $_SESSION['vk_cat_id']) {
			$_SESSION['vk_height']	=	0;
			$_SESSION['vk_width']	=	0;
			$_SESSION['vk_length']	=	0;
		}

		//Voorkeuren (selected)
		if (!isset($_SESSION['vk_height'])) 	$_SESSION['vk_height']	=	0;
		if (!isset($_SESSION['vk_width'])) 		$_SESSION['vk_width']	=	0;
		if (!isset($_SESSION['vk_length'])) 	$_SESSION['vk_length']	=	0;

		$vk_height 	= 	(int) request('vk_height', 	$_SESSION['vk_height']);
		$vk_width 	= 	(int) request('vk_width', 	$_SESSION['vk_width']);
		$vk_length 	= 	(int) request('vk_length',	$_SESSION['vk_length']);
		if (!$vk_length) {
			$vk_length = '';
		}

		$_SESSION['vk_height']	=	$vk_height;
		$_SESSION['vk_width']	=	$vk_width;
		$_SESSION['vk_length']	=	$vk_length;
		$_SESSION['vk_cat_id'] 	= 	$category_id;

		//Process list
		while ($sub = mfo($res)) {

			//Initialize voorkeur yes/no
			$has_width = $has_height = false;

			//Available heights/widths of all products
			$res_vk = eq("SELECT height,width FROM $db_table_products WHERE category_id = $sub->id;");
			while ($vk = mfo($res_vk)) {

				//Populate the available voorkeuren arrays
				if (!isset($heights[$vk->height])) 	$heights[$vk->height] = $vk->height;
				if (!isset($widths[$vk->width])) 	$widths[$vk->width]   = $vk->width;

				//Check if a height/width is selected, if this subcat has this widht/height
				if ($vk->height == $vk_height) 		$has_height	=	true;
				if ($vk->width == $vk_width) 		$has_width	=	true;
			}

			//Fine, so now if we selected a voorkeur, and we DON'T have that widht/height in this sub cat, skip it!
			if ($vk_height and !$has_height) {
				continue;
			}
			if ($vk_width and !$has_width) {
				continue;
			}

			//Double check
			if ($vk_width and $vk_height and !getMyo($db_table_products, "category_id = $sub->id AND width='$vk_width' AND height = '$vk_height'", 'id')) {
				continue;
			}

			//Sub cat props
			$s_name			=	$sub->menu_name ? $sub->menu_name : $sub->name;
			$s_description 	= 	$sub->description_overview ? $sub->description_overview : $sub->description;
			$s_image_url	=	$gfx_folder.$_sep.$cat_prefix.$sub->id.'.jpg';
			$s_price_unit	=	$sub->price_unit;
			$s_image		=	file_exists($s_image_url) ? "<img src='$s_image_url' alt='$s_name' width='171px' height='171px'/>" : '';

			//Category URL
			$s_url			 =	$cfg['USE_REWRITE_ENGINE'] ? 	$cat_re_prefix.'-'.$sub->id.'-'.rewriteSafe($s_name).'.html' :
																$_file.'?category_id='.$sub->id;

			//Price props (also based on voorkeur)
			$res_p = eq("	SELECT price,normal_price,sale_price,on_sale,step_size FROM $db_table_products
							WHERE active = '1' AND category_id = '$sub->id'
							".($vk_height ? " AND height='$vk_height'" : '')."
							".($vk_width ? " AND width='$vk_width'" : '')."
							ORDER BY price ASC LIMIT 1;");
			$product = mfo($res_p);
			if ($product) {
				$p_price	 =	($product->on_sale ? $product->sale_price : $product->price)*$vat_multiplier*$factor;
				$p_voordeel	 =	parseAmount($product->normal_price*$vat_multiplier-$p_price, '&euro; ');
				$p_price	 =	parseAmount($p_price, '&euro; ');
				$p_normal	 =	parseAmount($product->normal_price*$vat_multiplier, '&euro; ');

				//If a length is available, also calculate the total price
				if ($vk_length) {

					$step 				= $product->step_size;
					$rolls 				= ceil($vk_length / $step);

					$pl_price 			= ($product->on_sale ? $product->sale_price : $product->price)*$vat_multiplier*$factor*$rolls;
					$pl_normal_price 	= $product->normal_price*$vat_multiplier*$rolls;
					$pl_voordeel 		= parseAmount($pl_normal_price - $pl_price, '&euro; ');
					$pl_price 			= parseAmount($pl_price, '&euro; ');
					$pl_normal_price 	= parseAmount($pl_normal_price, '&euro; ');
				}
			}
			else {
				$p_price = $p_voordeel = $p_normal = '';
			}

			//If a length is available, show the actual price kokokokokokoko
			if ($vk_length) {
				$price_offer = "
					<div class='priceNormal'>Normaal vanaf ".$pl_normal_price."</div>
					<h4>Topdeal vanaf</h4>
					<h5>$pl_price</h5>
					<div class='priceAdv'>Uw voordeel: $pl_voordeel</div>
				";
			}
			else {
				$price_offer = "
					<div class='priceNormal'>Normaal vanaf $p_normal $s_price_unit</div>
					<h4>Topdeal vanaf</h4>
					<h5>$p_price $s_price_unit</h5>
					<div class='priceAdv'>Uw voordeel: $p_voordeel $s_price_unit</div>
				";
			}

			//Add to list
			$items[]		=	"
				<div class='prodOverzHorz'>
					<div class='prodHorz'>
						<div class='prodHorzImg'><a href='$s_url'>$s_image</a></div>
						<div class='prodHorzInfo'>
							<h3><a href='$s_url'>$s_name</a></h3>
							<div class='prodHorzDesc'>$s_description</div>
							<div class='prodHorzExtra'>&nbsp;</div>
							<div class='divider'>
								<div class='berekenprijsBtn'><a href='$s_url'><span>Bereken uw prijs</span></a></div>
							</div>
						</div>
						<div class='prodHorzPrice'>
							$price_offer
						</div>
					</div>
				</div>
			";
		}

		//Sort available voorkeuren
		ksort($heights);
		ksort($widths);

		//Voorkeuren selection
		if ($category_id == CATEGORY_PANEEL or $category_id == CATEGORY_GAAS /*or $category_id == CATEGORY_PLASITOR_GAAS or
			$category_id == CATEGORY_DIER_TUIN_GAAS*/) {

			$voorkeur = "
				<tr>
					<td width='140' valign='middle' height='22' align='left'>Kies een lengte (m):</td>
					<td width='140' valign='middle' align='left'><input type='text' class='' name='vk_length' id='vk_length' value='$vk_length' class='formMed' onchange='refresh_overview();'/></td>
					<td valign='middle' align='left'>
						<div id='Lengte' class='info' style='display: none;' onmouseout='displayDiv(\"Lengte\", 0); return false;'>".VOORKEUR_LENGTE."</div>
						<a class='infoIcon' onmouseover='displayDiv(\"Lengte\", 1); return false;'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:3px 0 0;' /></a>
					</td>
				</tr>
				<tr>
					<td width='140' valign='middle' height='22' align='left'>Kies een hoogte (mm):</td>
					<td width='140' valign='middle' align='left'>".makeSelectBox('vk_height', $heights, $vk_height, 129, "onchange='refresh_overview();'")."</td>
					<td valign='middle' align='left' colspan='3'>
						<div id='Hoogte' class='info' style='display: none;' onmouseout='displayDiv(\"Hoogte\", 0); return false;'>".VOORKEUR_HOOGTE."</div>
						<a class='infoIcon' onmouseover='displayDiv(\"Hoogte\", 1); return false;'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:3px 0 0;' /></a>
					</td>
				</tr>";
		}
		elseif ($category_id == CATEGORY_POORTEN) {
			$voorkeur = "<tr>
					<td width='140' valign='middle' height='22' align='left'>Kies een breedte (mm):</td>
					<td width='140' valign='middle' align='left'>".makeSelectBox('vk_width', $widths, $vk_width, 129, "onchange='refresh_overview();'")."</td>
					<td valign='middle' align='left'>
						<div id='Width' class='info' style='display: none;' onmouseout='displayDiv(\"Width\", 0); return false;'>".VOORKEUR_BREEDTE."</div>
						<a class='infoIcon' onmouseover='displayDiv(\"Width\", 1); return false;'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:3px 0 0;' /></a>
					</td>
				</tr>
				<tr>
					<td width='140' valign='middle' height='22' align='left'>Kies een hoogte (mm):</td>
					<td width='140' valign='middle' align='left'>".makeSelectBox('vk_height', $heights, $vk_height, 129, "onchange='refresh_overview();'")."</td>
					<td valign='middle' align='left' colspan='3'>
						<div id='Hoogte' class='info' style='display: none;' onmouseout='displayDiv(\"Hoogte\", 0); return false;'>".VOORKEUR_HOOGTE."</div>
						<a class='infoIcon' onmouseover='displayDiv(\"Hoogte\", 1); return false;'><img src='gfx/layout/nav/info.gif' alt='' title='' style='margin:3px 0 0;' /></a>
					</td>
				</tr>";
		}
		else {
			$voorkeur = '';
		}

		//Page contents
		$page_contents = "

			<div class='blok'>
				<h2>$c_name</h2>
				<p>$c_description</p>
			</div>
			".($voorkeur ? "
			<script type='text/javascript'>
				function refresh_overview() {

					var width	=	$('#vk_width').val();
					var height	=	$('#vk_height').val();
					var length	=	$('#vk_length').val();
					var	vk_uri	=	'';

					if (width != undefined) {
						vk_uri += 'vk_width=' + width + '&';
					}
					if (height != undefined) {
						vk_uri += 'vk_height=' + height + '&';
					}
					if (length != undefined) {
						vk_uri += 'vk_length=' + length + '&';
					}

					document.location.href = '$c_url?' + vk_uri;
				}
			</script>
			<div class='blok'>
				<h2>Uw voorkeur</h2>
				<table width='100%' cellspacing='0' cellpadding='0' border='0' style=''>
				<tbody>
					$voorkeur
				</tbody>
				</table>
			</div>
			" : '')."
			<div class='blok'>
				$pages_html
			</div>
   			<div class='blok'>
   				".(count($items) ? implode("\n", $items) : VOORKEUR_NO_PRODUCTS)."
			</div>
		";
	}

	// ************************************************************************************************************************************

	//Active and sub cat?
	elseif ($category and $category->active and $category->sub_cat_of and isset($form_files[$category->sub_cat_of])) {

		//Different template
		$template		=	'static/shop.html';

		//Voorkeuren (selected)
		if (!isset($_SESSION['vk_height'])) 	$_SESSION['vk_height']	=	0;
		if (!isset($_SESSION['vk_width'])) 		$_SESSION['vk_width']	=	0;
		if (!isset($_SESSION['vk_length'])) 	$_SESSION['vk_length']	=	0;

		$vk_height 	= 	(int) $_SESSION['vk_height'];
		$vk_width 	= 	(int) $_SESSION['vk_width'];
		$vk_length 	= 	(int) $_SESSION['vk_length'];
		if (!$vk_length) {
			$vk_length = '';
		}

		//Set vars
		$c_name			=	$category->menu_name ? $category->menu_name : $category->name;
		$c_description 	= 	$category->description;
		$c_image_url	=	$gfx_folder.$_sep.$cat_prefix.$category_id.'.jpg';
		$page_image		=	$gfx_folder.$_sep.$cat_prefix.$category->sub_cat_of.'.jpg';
		$c_image		=	file_exists($c_image_url) ? "<img src='$c_image_url' alt='$c_name' width='171px' height='171px' />" : '';
		$c_price_unit	=	$category->price_unit;

		//Category URL
		$c_url			=	$cfg['USE_REWRITE_ENGINE'] ? 	$cat_re_prefix.'-'.$category_id.'-'.rewriteSafe($c_name).'.html' :
															$_file.'?category_id='.$category_id;

		//Parent URL and name
		$pc				=	getMyo($db_table_cats, $category->sub_cat_of, array('name','menu_name'));
		$pc_name		=	$pc->menu_name ? $pc->menu_name : $pc->name;
		$pc_url			=	$cfg['USE_REWRITE_ENGINE'] ? 	$cat_re_prefix.'-'.$category->sub_cat_of.'-'.rewriteSafe($pc_name).'.html' :
															$_file.'?category_id='.$category->sub_cat_of;
		$page_title		=	$pc_name.' - '.$c_name;

		//Bread crumbs
		$crumbs[$pc_url] 	= 	$pc_name;
		$crumbs[$c_url] 	= 	$c_name;
		$active_crumb		=	$c_url;

		//Price props
		$res_p = eq("	SELECT price,normal_price,sale_price,on_sale FROM $db_table_products
						WHERE active = '1' AND category_id = '$category_id' ORDER BY price ASC LIMIT 1;");
		$product = mfo($res_p);
		if ($product) {
			$p_price	 =	($product->on_sale ? $product->sale_price : $product->price)*$vat_multiplier*$factor;
			$p_voordeel	 =	parseAmount($product->normal_price*$vat_multiplier-$p_price, '&euro; ');
			$p_price	 =	parseAmount($p_price, '&euro; ');
			$p_normal	 =	parseAmount($product->normal_price*$vat_multiplier, '&euro; ');
		}
		else {
			$p_price = $p_voordeel = $p_normal = '';
		}

		//Manual
		$manual = 	getMyo(DIGI_DB_PREFIX.'pages_NL', $category->manual, 'html_name');
		$manual = 	$manual ? 	"<a href='$manual' title='Montage handleiding'>Montage handleiding</a>
					 			 <div class='divider'>&nbsp;</div>" : '';

		//Available colors
		$available_colors = array();
		$res_c = eq("	SELECT $db_table_colors.name, $db_table_colors.id FROM $db_table_colors
						LEFT JOIN $db_table_products ON ($db_table_colors.id = color_id)
						WHERE $db_table_products.active = '1' AND category_id = '$category_id'
						GROUP BY color_id
						ORDER BY $db_table_colors.name ASC
		");

		while ($color = mfo($res_c)) {
			$available_colors[$color->id] = $color->name;
		}

		//Error?
		$error = isset($error) ? "<div class='blok'><p class='error_message'>$error</p></div>" : '';

		//Form contents, load the category specific file
		$form_contents = '';
		include('inc/common/'.$form_files[$category->sub_cat_of]);

		//Amount field
		$amount_field = '';
		if (CATEGORY_ACCESSOIRES == $category->sub_cat_of) {
			$amount_field = "<p>Aantal: <input type='text' id='amount' name='amount' style='width: 60px;' value='1' maxlength='4' onkeyup='calc_price(\"Amount changed\");'/> stuk(s)</p>";
		}

		//Page contents
		$page_contents = "
			<form name='offerte' id='offerte' method='post' action='$_file'>
			<input type='hidden' name='category_id' value='$category_id' />
			<div class='blok'>
				<h2>$c_name</h2>
				<hr />
				<div class='addWinkelTop'><input type='submit' name='send_form' value='' /></div>
			</div>
			<div class='blok'>
				<div class='prodOverzHorz'>
					<div class='prodHorz'>
						<div class='prodHorzImg'>$c_image</div>
						<div class='prodHorzInfo'>
							<div class='prodHorzDescExt'>$c_description</div>
							$amount_field
						</div>
						<div class='prodHorzPriceProd' id='price_block'>
							".get_price_block($p_normal, $p_price, $p_voordeel, $c_price_unit)."
						</div>

						".(($manual or $show_ruler) ? "
						<div class='prodHorzExtraInfo'>
							$manual
						</div>
						" : '')."

						".(($category->sub_cat_of != CATEGORY_ACCESSOIRES) ? "
						<div class='prodHorzPriceProd' id='price_block_2'>
							".get_price_block($p_normal, $p_price, $p_voordeel, $c_price_unit)."
						</div>
						" : '')."
					</div>
				</div>
			</div>

			$form_contents

			<br/><br/>
			<div class='blok'>
				<div class='divider'>
					<div class='addWinkelBtm'><input type='submit' name='send_form' value=''/></div>
				</div>
			</div>
			</form>
		";
	}

	//Inactive
	else {
		$page_contents = "<h2>Fout</h2><p>Deze categorie kan niet gevonden worden of is momenteel niet actief.</p>";
	}
}

/******************************************************************************************
 * Re-direct
 ***/

else {

	header('Location: index.html');
	exit;
}

/******************************************************************************************
 * Output
 ***/

echo $page_contents;

require_once('inc/site_footer.php');
?>